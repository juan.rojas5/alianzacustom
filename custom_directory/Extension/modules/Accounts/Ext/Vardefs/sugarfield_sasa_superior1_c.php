<?php
 // created: 2022-10-25 23:17:02
$dictionary['Account']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Account']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_superior1_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Account']['fields']['sasa_superior1_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_superior1_c']['dependency']='';

 ?>