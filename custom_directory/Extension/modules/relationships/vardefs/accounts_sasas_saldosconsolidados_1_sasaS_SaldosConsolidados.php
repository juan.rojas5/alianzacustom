<?php
// created: 2019-01-04 22:23:30
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'accounts_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1_name"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link' => 'accounts_sasas_saldosconsolidados_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1accounts_ida"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE_ID',
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link' => 'accounts_sasas_saldosconsolidados_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
