<?php
// created: 2019-01-04 22:23:30
$dictionary["Account"]["fields"]["accounts_sasas_saldosconsolidados_1"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'accounts_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'sasaS_SaldosConsolidados',
  'bean_name' => 'sasaS_SaldosConsolidados',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
