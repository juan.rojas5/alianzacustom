<?php
$dictionary['Campaign']['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'len' => 255,
);
$dictionary['Campaign']['fields']['clicks'] = array(
    'required' => false,
    'name' => 'clicks',
    'vname' => 'LBL_CC_CLICKS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['opens'] = array(
    'required' => false,
    'name' => 'opens',
    'vname' => 'LBL_CC_OPENS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['forwards'] = array(
    'required' => false,
    'name' => 'forwards',
    'vname' => 'LBL_CC_FORWARDS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['bounces'] = array(
    'required' => false,
    'name' => 'bounces',
    'vname' => 'LBL_CC_BOUNCES',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['optouts'] = array(
    'required' => false,
    'name' => 'optouts',
    'vname' => 'LBL_CC_OPTOUTS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['spamreports'] = array(
    'required' => false,
    'name' => 'spamreports',
    'vname' => 'LBL_CC_SPAMREPORTS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['last_run_completed'] = array(
    'name' => 'last_run_completed',
    'type' => 'bool',
    'default' => 0,
);
