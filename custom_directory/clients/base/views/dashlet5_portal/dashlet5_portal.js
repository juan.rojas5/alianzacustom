({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,	
	url:'**PAI**',
	ws_auth:'**PAI**',
	authorization: "**PAI**",
	xmlns_wsdl:'**PAI**',
	xmlns_xsd:'**PAI**',	
	wsdl:'**PAI**',
	Tipo_Doc:'**PAI**',
	sub_Doc:'**PAI**',
	initialize: function(options) {
		var self = this;		
		if (typeof app === 'undefined') app = App;			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet5_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet5_id_cliente").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet5_id_cliente").css("border", "none"); 
						$("#dashlet5_id_cliente").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet5_id_cliente").attr('readonly', true);
					}
					$("#dashlet5_consultar").click( function(){
						self.dashlet_siged();	
					});
					$("#dashlet5_consultar").show();
					$("#dashlet5_descargar").click( function(){
						self.dashlet5_descargar_siged();	
					});		
					$("#dahslet5_encargos_list").change( function(){
						self.dahslet6_encargos_list_change();	
					});			
					$("#dashlet5_consultar").click( function(){
						self.dashlet_siged();	
					});			
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dahslet6_encargos_list_change: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		if (!_.isEmpty( self.respuesta )){ 		
			$("#dahslet5_radicado").empty();//limiar lista primero
			var i=$("#dahslet5_encargos_list").val();
			var infoCasos =self.respuesta.infoCasos[i];
			if(infoCasos.encargo == null || infoCasos.encargo == 'null') infoCasos.encargo = 'No disponible!';
			$("#dahslet5_radicado").html(infoCasos.encargo);
		}
	},
	dashlet_siged: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet_siged_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		$("#dahslet5_encargos_list").empty();//limiar lista primero
		$("#dashlet5_nombre").html("--");//limiar lista primero
		self.respuesta=null;//limiar lista primero
		
		self.dashlet_siged_call();		
		console.log("Response dashlet_siged_call", self.respuesta);
		
		if(self.respuesta.codigo == 'OK'){			
			if(!$.isArray(self.respuesta.infoCasos)){//cuando solo retorna un elemnto, se obtiene un objeto en lugar de un array multinivel
				var temp = self.respuesta.infoCasos;
				self.respuesta.infoCasos = [];
				self.respuesta.infoCasos.push(temp);				
			}
			/*
			Obteniendo cuenta
			*/
			app.api.call(
				'GET', 
				app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet5_id_cliente").val()), 
					{
						"sasa_nroidentificacion_c": $("#dashlet5_id_cliente").val(),
					}, 
					{
					success: function (data) {
                        			if(!_.isEmpty(data.records)){
							$("#dashlet5_nombre").html(data.records[0].name);
                       				}
						else{
							$("#dashlet5_nombre").html('--<b>No Existe en sugar</b>--');
						}
					},
					error: function (e) {						
						app.alert.show('error_procesando_cuenta', {
							level: 'error',
							title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
							autoClose: false,
						});
					}
				}, {async: false});
			
			$.each(self.respuesta.infoCasos, function( i, encargo ) {
				$("#dahslet5_encargos_list").append('<option value="'+i+'">'+encargo.radicado+'</option>');				
			});
			self.dahslet6_encargos_list_change();	
		}
		else{
			app.alert.show('dashlet_siged_error'+self.respuesta.codigo, {
				level: 'error',
				title: self.respuesta.mensaje,
				autoClose: false,
			});
		}	
	},
	dashlet5_descargar_siged: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;		
		
		i = $("#dahslet5_encargos_list").val();
		if($("#dahslet5_encargos_list").val() != null){
			if (!_.isEmpty( self.respuesta )){ 		
				window.open(self.respuesta.infoCasos[i].urldocumento, self.encargo, '');
			}
		}		
	},
	dashlet_siged_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		console.log('Consultando Siged...');
		var ID_Cliente = $("#dashlet5_id_cliente").val();	
		var method  = "dashlet_siged";		
		var data = [
			{
				'url':self.url,
				'ws_auth':self.ws_auth,
				'authorization':self.authorization,
			},
			{
				'xmlns_wsdl': self.xmlns_wsdl,
				'xmlns_xsd':self.xmlns_xsd,
				'wsdl':self.wsdl,
				'Encargo': '',
				'ID_Cliente': ID_Cliente,
				'Tipo_Doc': self.Tipo_Doc,
				'sub_Doc': self.sub_Doc,
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_siged_procesando');
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_siged_procesando');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + e ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	}
})
