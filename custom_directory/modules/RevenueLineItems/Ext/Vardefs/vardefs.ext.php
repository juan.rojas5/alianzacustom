<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_quantity.php

 // created: 2018-06-20 17:26:31
$dictionary['RevenueLineItem']['fields']['quantity']['default']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['len']='12';
$dictionary['RevenueLineItem']['fields']['quantity']['audited']=true;
$dictionary['RevenueLineItem']['fields']['quantity']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['comments']='Quantity in use';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quantity']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_usdollar.php

 // created: 2018-06-20 17:29:05
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['audited']=true;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['comments']='Cost expressed in USD';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['importable']='false';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['cost_usdollar']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_description.php

 // created: 2018-06-20 17:33:55
$dictionary['RevenueLineItem']['fields']['description']['audited']=false;
$dictionary['RevenueLineItem']['fields']['description']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['description']['comments']='Full text of the note';
$dictionary['RevenueLineItem']['fields']['description']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['description']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['description']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.47',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['description']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['description']['rows']='6';
$dictionary['RevenueLineItem']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-06-20 17:34:50
$dictionary['RevenueLineItem']['fields']['date_entered']['audited']=true;
$dictionary['RevenueLineItem']['fields']['date_entered']['comments']='Date record created';
$dictionary['RevenueLineItem']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_entered']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-06-20 17:35:32
$dictionary['RevenueLineItem']['fields']['date_modified']['audited']=true;
$dictionary['RevenueLineItem']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['RevenueLineItem']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_modified']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_discount_price.php

 // created: 2018-07-07 16:31:00
$dictionary['RevenueLineItem']['fields']['discount_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['discount_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_name.php

 // created: 2018-07-09 14:49:04
$dictionary['RevenueLineItem']['fields']['name']['required']=false;
$dictionary['RevenueLineItem']['fields']['name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['name']['comments']='Name of the product';
$dictionary['RevenueLineItem']['fields']['name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.57',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['name']['calculated']='true';
$dictionary['RevenueLineItem']['fields']['name']['formula']='related($rli_templates_link,"name")';
$dictionary['RevenueLineItem']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_cost_price.php

 // created: 2018-07-14 20:14:15
$dictionary['RevenueLineItem']['fields']['cost_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['cost_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['cost_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['comments']='Product cost ("Cost" in Quote)';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['cost_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['cost_price']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['cost_price']['enable_range_search']=false;
$dictionary['RevenueLineItem']['fields']['cost_price']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_likely_case.php

 // created: 2018-07-14 20:15:42
$dictionary['RevenueLineItem']['fields']['likely_case']['default']='';
$dictionary['RevenueLineItem']['fields']['likely_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['likely_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['likely_case']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sasa_motivoperdida_c.php

 // created: 2018-08-04 17:03:59
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['labelValue']='Motivo de Pérdida';
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['enforced']='';
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['dependency']='equal($sales_stage,"Closed Lost")';

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/rli_vardef.ext.php

$dictionary['RevenueLineItem']['importable'] = true;
$dictionary['RevenueLineItem']['unified_search'] = true;
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/sugarfield_sales_stage.php

 // created: 2018-08-08 18:18:29
$dictionary['RevenueLineItem']['fields']['sales_stage']['default']='Perception Analysis';
$dictionary['RevenueLineItem']['fields']['sales_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['sales_stage']['required']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['massupdate']=true;
$dictionary['RevenueLineItem']['fields']['sales_stage']['options']='sasa_etapaventas_c_list';
$dictionary['RevenueLineItem']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['RevenueLineItem']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['RevenueLineItem']['fields']['account_name']['is_denormalized'] = true;
$dictionary['RevenueLineItem']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['len'] = '150';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['audited'] = true;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['required'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['importable'] = 'required';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_merge'] = 'enabled';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = '1';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['calculated'] = false;
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['RevenueLineItem']['fields']['denorm_account_name']['studio'] = false;

?>
