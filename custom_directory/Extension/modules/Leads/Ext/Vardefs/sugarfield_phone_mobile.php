<?php
 // created: 2018-08-04 15:46:55
$dictionary['Lead']['fields']['phone_mobile']['len']='10';
$dictionary['Lead']['fields']['phone_mobile']['required']=false;
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']=false;
$dictionary['Lead']['fields']['phone_mobile']['help']='Formato: (Número de celular de diez dígitos)';

 ?>