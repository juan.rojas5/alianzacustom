<?php
 // created: 2022-11-18 20:05:52
$dictionary['Case']['fields']['primary_contact_name']['len']=255;
$dictionary['Case']['fields']['primary_contact_name']['massupdate']=true;
$dictionary['Case']['fields']['primary_contact_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['primary_contact_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['primary_contact_name']['reportable']=false;
$dictionary['Case']['fields']['primary_contact_name']['calculated']=false;
$dictionary['Case']['fields']['primary_contact_name']['related_fields']=array (
  0 => 'primary_contact_id',
);

 ?>