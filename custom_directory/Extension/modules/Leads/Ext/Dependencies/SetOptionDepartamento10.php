<?php
$dependencies['Leads']['SetOptionDepartamento10'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Caqueta")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Albania","Belen de Los Andaquies","Cartagena del Chaira","Curillo","El Doncello","El Paujil","Florencia","La Montanita","Milan","Morelia","Puerto Rico","San Jose del Fragua","San Vicente del Caguan","Solano","Solita","Valparaiso")',
           'labels' => 'createList("","Albania","Belén de Los Andaquies","Cartagena del Chairá","Curillo","El Doncello","El Paujil","Florencia","La Montañita","Milán","Morelia","Puerto Rico","San José del Fragua","San Vicente del Caguán","Solano","Solita","Valparaíso")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
