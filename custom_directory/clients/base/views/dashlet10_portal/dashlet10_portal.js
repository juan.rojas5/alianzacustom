({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	token: null,
	autenticarse: null,
	ws: null,
	data: null,
	tipo: null,
	cuenta2:[],
	certificado:[],
	ruta:'',
	url: '',
	tipoIdentificacion:'',
	numeroIdentificacionConDigito:'',
	initialize: function(options) {
		var self = this;	
		dashlet10 = self;	
		if (typeof app === 'undefined') app = App;			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet10_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet10_id_cliente").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet10_id_cliente").css("border", "none"); 
						$("#dashlet10_id_cliente").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet10_id_cliente").attr('readonly', true);
						self.numeroIdentificacionConDigito=self.bean.sasa_nroidentificacion_c;
					}
					
					$("#dashlet10_Tipo_Certificado").change( function(){
						self.dashlet10_portal_Tipo_Certificado_change();	
					});
					
					$("#dashlet10_consultar").click( function(){
						self.dashlet10_portal();	
					});
					
					$("#dashlet10_consultar").show();
					
					$("#dashlet10_descargar1").click( function(){
						self.dashlet_portal_obtener_descargar20(true,false);	
					});
					
					$("#dashlet10_enviar1").click( function(){
						self.dashlet_portal_obtener_enviar20(true,true);	
					});
					
					$("#dashlet10_descargar2").click( function(){
						self.dashlet10_portal_obtener_descargar2(true,false);	
					});
					
					$("#dashlet10_enviar2").click( function(){
						self.dashlet10_portal_obtener_descargar2(true,true);	
					});
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet10_portal: function(){
		var self = dashlet10;
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet10_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet10_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){		
			if (!_.isEmpty( $("#dashlet10_id_cliente").val() )){	
				//cuenta/correo $("#dashlet10_correo").html("<option value='daniel.montoya@sasaconsultoria.com'>me</option>");
				self.dashlet10_portal_dashlet10_get_cuenta();	
				
				//extracto	
				self.dashlet10_portal_obtener_cuenta1(false);	
			 	self.dashlet10_portal_obtener_periodo(false);			
				
				//certificado
				self.dashlet10_portal_obtener_Tipo_Certificado(true);
				self.dashlet10_portal_obtener_cuenta2(false);
			}
			else{
				app.alert.show('error_procesando_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet10_portal_procesando');		
			document.getElementById('dashlet10_skm_LockPane').className = "LockOff"; 	
		}, 200 );		
	},
	dashlet10_portal_dashlet10_get_cuenta: function(){
		var self = dashlet10;
		if (typeof app === 'undefined') app = App;
	
		/*
		Obteniendo cuenta
		*/
		$("#dashlet10_nombre").html('');
		$("#dashlet10_correo").html('');
		self.tipoIdentificacion="";
		self.numeroIdentificacionConDigito = $("#dashlet10_id_cliente").val();
		app.api.call(
			'GET', 
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet10_id_cliente").val()), 
				{
					"sasa_nroidentificacion_c": $("#dashlet10_id_cliente").val(),
				}, 
				{
				success: function (data) {
					if(!_.isEmpty(data.records)){
						$("#dashlet10_nombre").html(data.records[0].name);
						self.tipoIdentificacion = data.records[0].sasa_tipoidentificacion_c;					
						
						if(self.tipoIdentificacion.trim() != ""){
						
							if(self.tipoIdentificacion=="Cedula de Ciudadania") self.tipoIdentificacion ="CC";
							if(self.tipoIdentificacion=="Cedula de Extranjeria") self.tipoIdentificacion ="CE";
							if(self.tipoIdentificacion=="Nit") self.tipoIdentificacion ="NIT";	
							
							if(!_.isEmpty(data.records[0].email)){						
								$.each(data.records[0].email, function( k, mail ) {
									var valor = mail.email_address;
									var texto = mail.email_address;
									var selected = "";
									if(mail.primary_address) selected ='selected="true"';
									if(mail.invalid_email){
										texto += " (Invalido)";
									}
									else if(mail.primary_address){
										texto += " (Principal)";
									} 
									else if(mail.opt_out){
										texto += " (Rehusado)";
									} 
									else if(mail.invalid_email){
										texto += " (No Valido)";
									}
									else{
										texto += " (Opcional)";
									}
									if (!_.isEmpty( valor )){ 
										$("#dashlet10_correo").append('<option '+selected+' value="'+valor+'">'+texto+'</option>');	
									}
								});
							}		
						}
						else{					
							app.alert.show('error_dashlet10_portal_obtener_fondo', {
								level: 'warning',
								title: 'Tipo de documento vacio',
								autoClose: false,
							});
						}
					}
					else{
						$("#dashlet10_nombre").html('--<b>No Existe en sugar</b>--');
						$("#dashlet10_correo").html('<option>--</option>');
						
									
						app.alert.show('error_dashlet10_portal_obtener_fondo', {
							level: 'warning',
							title: '<p><br> No exite en Sugar, por tanto no se puede leer el tipo de documento causando fallas al Generar el PDF!',
							autoClose: false,
						});
					}									
							
					if(self.tipoIdentificacion != "CC" && self.tipoIdentificacion != "CE"){
						
						/*
						Digito verificacion
						*/
						self.ws = "/extractos-v1/api_fiducia/consultas/digito-verificacion";
						self.tipo = "POST";
						self.data = {
							"nrold": self.numeroIdentificacionConDigito*1,
							"modo": "M11NIT",
						};
						self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
						if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
							console.log("Digito verificacion: ",self.respuesta);
							
							if(typeof self.respuesta.operacionExitosa !== 'undefined') {
								if(self.respuesta.operacionExitosa){
									self.numeroIdentificacionConDigito = $("#dashlet10_id_cliente").val()+self.respuesta.digito;
								}
							}
							
							if(typeof self.respuesta.error !== 'undefined') {	
								if(!_.isEmpty(self.respuesta.error)){						
									app.alert.show('error_dashlet10_portal_obtener_fondo', {
										level: 'error',
										title: '<p><br>Error al obtener digito verificacion nit',
										messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
										autoClose: false,
									});
								}
							}								
						}
						else {							
							app.alert.show('error_dashlet10_portal_obtener_fondo', {
								level: 'error',
								title: '<p><br>Error al obtener digito verificacion nit',
								messages:' <p>No se encontro el digito ingresado o se dio un error en el sistema remoto',
								autoClose: false,
							});
						}
					}
				},
				error: function (e) {		
					app.alert.dismiss('dashlet10_portal_procesando');				
					app.alert.show('error_procesando_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
						autoClose: false,
					});
				}
			}, {async: false}
		);		
	},
	dashlet10_portal_obtener_periodo: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		$("#dashlet10_Periodo").empty();//limpiando		
		var date = new Date();		
		for (i = 0; i < 6; i++) { 
			var option =  date.toLocaleString("es", {year: "numeric"}) + '/'+ date.toLocaleString("es", {month:"2-digit"});//formato requerido	
			var value = date.toLocaleString("es", { month: "long" }).replace(/(\b\w)/gi,function(m){return m.toUpperCase();})+" "+date.getFullYear();//formato requerido			
			date.setMonth(date.getMonth()-1);//un mes menos
			$("#dashlet10_Periodo").append('<option value="'+option+'">'+value+'</option>');	
		}
		
		if ( _.isEmpty( $("#dashlet10_Periodo").html().trim() ) ){ 
			$("#dashlet10_Periodo").append('<option value="">--</option>');
		}
	},	
	dashlet10_portal_obtener_cuenta: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;	
		
		self.autenticarse = autenticarse;
		self.ws = "/api_fiducia/comercial/consultas/obtenerClientes";
		self.tipo = "POST";
		var docuemnto_con_digito = $("#dashlet10_id_cliente").val();// + self.get_digito( $("#dashlet10_id_cliente").val() );
		self.data = '{\"nombre": \"\", \"id\": \"'+docuemnto_con_digito+'\", \"tipoId\": \"\", \"producto\": \"\", \"desde\": 1, \"hasta\": 5}';
		self.dashlet10_portal_call({});//autenticandose y consumo
	},	
	dashlet10_portal_obtener_descargar1: function(autenticarse,enviar){//9001077859
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet10_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet10_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){		
			if (   !_.isEmpty(    $("#dashlet10_cuenta2").val())  &&    !_.isEmpty(   self.ruta)     &&         !_.isEmpty(   $("#dashlet10_Vigencia").val())             ){ 
				self.autenticarse = autenticarse;
				self.ws = "/api_fiducia/certificaciones/valores/consultas/extractos/inversiones?oyd="+$("#dashlet10_cuenta2").val()+"&fechaDesde="+$("#dashlet10_Periodo").val();
				self.tipo = "GET";
				self.tipo = "file_body";
				self.data = [];
				var mail = {};
				var tipo = "Extracto Clientes Valores";
				if(enviar){	
					var Address = "";	
					if ( !_.isEmpty( $("#dashlet10_correo").val() )){
						if (typeof $("#dashlet10_correo").val()[0] !== 'undefined'){	
							if ( !_.isEmpty( $("#dashlet10_correo").val()[0] )){ 
								firma="Atentamente,\n___________\nAlianza";
								//Address = $("#dashlet10_correo").val().join("; ");
								Address = $("#dashlet10_correo").val();	
								Subject = tipo+" - "+$("#dashlet10_id_cliente").val()+" - "+Date();				
								nombre = $("#dashlet10_nombre").html();
								
								Body = "";
								Body += "Buen día,";
								Body += "\n\n";
								Body += "Estimado "+nombre+",";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Reciba un cordial saludo de parte de Alianza.";
								Body += "\n\n";
								Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Cordialmente,";
								Body += "\n\n";
								Body += "Alianza.";
								Body += "\n\n";
								Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
								
								mail = {
									"Subject": Subject,
									"Body": Body,
									"Address": Address,
									"AddressCC": "",
								};
							}
						}
					}
					if ( _.isEmpty( Address )){
						app.alert.show('error_procesando_obtener_descargar2', {
							level: 'error',
							title: "Por favor, seleccione una direccion de correo!!",
							autoClose: false,
						});
						return;	
					}
				}
				self.dashlet10_portal_call(mail);//autenticandose y consumo
				if (self.token != null){
					if (!_.isEmpty( self.respuesta )){ 
						if(enviar){									
							if(self.respuesta == "ok"){
								//var Address = $("#dashlet10_correo").val().join("; ");
								var Address = $("#dashlet10_correo").val();
								app.alert.show('success_procesando_obtener_descargar1', {
									level: 'success',
									messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
									autoClose: false,
								});	
							}
							else{
								app.alert.show('error_procesando_obtener_descargar1', {
									level: 'error',
									title: self.respuesta,
									autoClose: false,
								});	
							}									
						}
						else{
							let pdfWindow = window.open("","Extracto")
							pdfWindow.document.write("<iframe  height='98.5%' width='99%' style='position:absolute;' src='data:application/pdf;base64, " + encodeURI(self.respuesta)+"'></iframe>");
						}
					}
					else{				
						
						app.alert.show('error_procesando_obtener_descargar1', {
							level: 'error',
							title: 'Error al descargar PDF1',
							autoClose: false,
						});
					}
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_descargar1', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet10_portal_procesando');		
			document.getElementById('dashlet10_skm_LockPane').className = "LockOff"; 	
		}, 200 );
	},		
	dashlet10_portal_obtener_descargar2: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet10_skm_LockPane').className = "LockOn"; app.alert.dismissAll();
			app.alert.dismissAll();
			app.alert.show('dashlet10_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){		
			if (   !_.isEmpty(    $("#dashlet10_Tipo_Certificado").val())  &&     !_.isEmpty(    $("#dashlet10_cuenta2").val())  &&    !_.isEmpty(   self.ruta)     &&         !_.isEmpty(   $("#dashlet10_Vigencia").val())             ){ 
				self.autenticarse = autenticarse;
				//oyd=22352
				self.ws = "/api_fiducia/certificaciones/valores/consultas/certificaciones/generarCertificadoTributario?oyd="+$("#dashlet10_cuenta2").val()+"&ruta="+self.ruta+"&vigencia="+$("#dashlet10_Vigencia").val();
				self.tipo = "file_body";
				self.data = [];
				var mail = {};
				var tipo = "Certificado Tributario Clientes Solo Valores";
				if(enviar){	
					var Address = "";	
					if ( !_.isEmpty( $("#dashlet10_correo").val() )){
						if (typeof $("#dashlet10_correo").val()[0] !== 'undefined'){	
							if ( !_.isEmpty( $("#dashlet10_correo").val()[0] )){ 
								firma="Atentamente,\n___________\nAlianza";
								//Address = $("#dashlet10_correo").val().join("; ");
								Address = $("#dashlet10_correo").val();	
								Subject = tipo+" - "+$("#dashlet10_id_cliente").val()+" - "+Date();		
								nombre = $("#dashlet10_nombre").html();
								
								Body = "";
								Body += "Buen día,";
								Body += "\n\n";
								Body += "Estimado "+nombre+",";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Reciba un cordial saludo de parte de Alianza.";
								Body += "\n\n";
								Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Cordialmente,";
								Body += "\n\n";
								Body += "Alianza.";
								Body += "\n\n";
								Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
								
								mail = {
									"Subject": Subject,
									"Body": Body,
									"Address": Address,
									"AddressCC": "",
								};
							}
						}
					}
					if ( _.isEmpty( Address )){
						app.alert.show('error_procesando_obtener_descargar2', {
							level: 'error',
							title: "Por favor, seleccione una direccion de correo!!",
							autoClose: false,
						});
						return;	
					}
				}
				self.dashlet10_portal_call(mail);//autenticandose y consumo
				if (self.token != null){
					if (!_.isEmpty( self.respuesta )){ 
						if(enviar){						
							if(self.respuesta == "ok"){
								//var Address = $("#dashlet10_correo").val().join("; ");
								var Address = $("#dashlet10_correo").val();
								app.alert.show('success_procesando_obtener_descargar2', {
									level: 'success',
									messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
									autoClose: false,
								});	
							}
							else{
								app.alert.show('error_procesando_obtener_descargar2', {
									level: 'error',
									title: self.respuesta,
									autoClose: false,
								});	
							}									
						}
						else{
							//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
	                        var mapForm = document.createElement("form");
	                            mapForm.target = "Map";
	                            mapForm.method = "POST"; // or "post" if appropriate
	                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

	                            var mapInput = document.createElement("input");
	                            mapInput.type = "text";
	                            mapInput.name = "file";
	                            mapInput.value = self.respuesta;
	                            mapForm.appendChild(mapInput);

	                            document.body.appendChild(mapForm);

	                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

	                        if (map) {
	                            mapForm.submit();
	                        }
							else{
								app.alert.show('error_procesando_obtener_descargar1', {
									level: 'error',
									title: 'Error al descargar PDF',
									messages: "El navegador esta bloqueando la ventana!!!",
									autoClose: false,
								});
							}
						}
					}
					else{				
						
						app.alert.show('error_procesando_obtener_descargar2', {
							level: 'error',
							title: 'Error al descargar PDF2',
							autoClose: false,
						});
					}
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_descargar1', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet10_portal_procesando');		
			document.getElementById('dashlet10_skm_LockPane').className = "LockOff"; 	
		}, 200 );
	},	
	dashlet10_portal_obtener_cuenta_fecha: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;	
		
		self.autenticarse = autenticarse;
		self.ws = "/api_fiducia/certificaciones/valores/consultas/certificaciones/ultimoDiaHabil";
		self.tipo = "GET";
		self.data = [];
		self.dashlet10_portal_call({});//autenticandose y consumo
		console.log("dashlet10_portal_obtener_cuenta_fecha:",  self.respuesta);	
	},	
	get_digito: function(documento){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		fuente https://github.com/gabrielizalo/calculo-digito-de-verificacion-dian/blob/master/calcularDigitoVerificacion.js
		*/
		
		var digito  = "";
		var vpri,
		x,
		y,
		z;

		// Se limpia el Nit
		documento = documento.replace(/\s/g, ""); // Espacios
		documento = documento.replace(/,/g, ""); // Comas
		documento = documento.replace(/\./g, ""); // Puntos
		documento = documento.replace(/-/g, ""); // Guiones

		// Se valida el nit
		if (isNaN(documento)) {
			digito = "";
		}
		else{

			// Procedimiento
			vpri = new Array(16);
			z = documento.length;

			vpri[1] = 3;
			vpri[2] = 7;
			vpri[3] = 13;
			vpri[4] = 17;
			vpri[5] = 19;
			vpri[6] = 23;
			vpri[7] = 29;
			vpri[8] = 37;
			vpri[9] = 41;
			vpri[10] = 43;
			vpri[11] = 47;
			vpri[12] = 53;
			vpri[13] = 59;
			vpri[14] = 67;
			vpri[15] = 71;

			x = 0;
			y = 0;
			for (var i = 0; i < z; i++) {
			y = (documento.substr(i, 1));
			x += (y * vpri[z - i]);
			}

			y = x % 11;

			var digito = (y > 1) ? 11 - y : y;
		}
		return digito;
	},
	dashlet10_portal_obtener_cuenta1: function(autenticarse){
		var self = dashlet10;
		if(typeof app === 'undefined') app = App;

		var tipoIdentificacion = "";
		if(self.tipoIdentificacion == "CC") tipoIdentificacion = "C";
		if(self.tipoIdentificacion == "CE") tipoIdentificacion = "E";
		if(self.tipoIdentificacion == "NIT") tipoIdentificacion = "N";		
		
		self.ws = "/extractos-v1/api_fiducia/consultas/oyd";
		self.tipo = "POST";
		self.data = {
			"idUsuarioRecurso": self.numeroIdentificacionConDigito,
			"tipoId": tipoIdentificacion,
		};
		self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
		$("#dashlet10_cuenta1").empty(); //limpiando
		if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
			if(self.respuesta.operacionExitosa) {
				console.log("dashlet10_portal_obtener_cuenta1: ", self.respuesta);
				$.each(self.respuesta.listaProductos, function(i, respuesta) {
					if(typeof respuesta.cuentasOyD !== 'undefined') {
						$("#dashlet10_cuenta1").append('<option value="' + respuesta.cuentasOyD + '">' + respuesta.cuentasOyD + '</option>');
					}
				});
			} 
			else {
				if(typeof self.respuesta.error !== 'undefined') {					
					app.alert.show('error_dashlet10_portal_obtener_fondo', {
						level: 'error',
						title: '<p><br>Error al obtener cuenta Extractos',
						messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
						autoClose: false,
					});
				}
				else{
					app.alert.show('error_dashlet10_portal_obtener_fondo', {
						level: 'error',
						title: 'Error al obtener cuenta Extractos: ' + self.respuesta.descripcionError,
						autoClose: false,
					});
				}
			}	
		}		
				
		if(_.isEmpty($("#dashlet10_cuenta1").html().trim())) {
			$("#dashlet10_cuenta1").append('<option value="">--</option>');
		}
	},
	dashlet10_portal_obtener_cuenta2: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.dashlet10_portal_obtener_cuenta_fecha(true);
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){
				var fecha = 	self.respuesta; //var date = new Date();var fecha = date.getFullYear()+'/'+(date.getUTCMonth()+1)+'/'+date.getDay();
				self.dashlet10_portal_obtener_cuenta(false);//poblando cuenta
				if (self.token != null){
					if (!_.isEmpty( self.respuesta )){		
						/*
						Filtrando cliente por N (Valores) y A (Fiduciaria)
						*/
						var negocio = "--";
						if (typeof self.respuesta.datos !== 'undefined') {	
							var cuenta= null;				
							$.each(self.respuesta.datos, function( i, cuentas ) {
								if(cuentas.pro_descripcion != "Todos Fiduciaria"){
									cuenta=cuentas;
									negocio = cuentas.cli_tipo_id;
								}	
							});
							self.respuesta.datos=[];
							self.respuesta.datos.push(cuenta);
						}
						console.log("cuenta_tipoIdentificacion_filtrada_por_"+negocio+":",  self.respuesta);
						
						if (typeof self.respuesta.datos[0] !== 'undefined' & self.respuesta.datos[0] !== null){
							if (typeof self.respuesta.datos[0].cli_tipo_id !== 'undefined'){
								var tipoIdentificacion = self.respuesta.datos[0].cli_tipo_id;	
								if(tipoIdentificacion != null){
									self.autenticarse = false;
									$("#dashlet10_cuenta2").empty();//limpiando
									if(tipoIdentificacion == "N"){
										var docuemnto_con_digito = $("#dashlet10_id_cliente").val() + self.get_digito( $("#dashlet10_id_cliente").val() );
									}
									else{
										var docuemnto_con_digito = $("#dashlet10_id_cliente").val();
									}
									self.ws = "/api_fiducia/consultas/valores/inversiones?desde="+fecha+"&hasta="+fecha+"&idUsuarioRecurso="+docuemnto_con_digito+"&tipoIdentificacion="+tipoIdentificacion;
									self.tipo = "GET";
									self.data = [];
									self.dashlet10_portal_call({});//autenticandose y consumo	
									if (self.token != null){
										if (!_.isEmpty( self.respuesta )){ 		
											if(!$.isArray( self.respuesta )){			
												var temp = self.respuesta;
												self.respuesta = [];
												self.respuesta.push(temp);
											}
											console.log("dashlet10_portal_obtener_cuenta2: ",self.respuesta);
											$.each(self.respuesta, function( i, respuesta ) {
												if (typeof respuesta.codigo_oyd !== 'undefined' && respuesta.codigo_oyd != "") {
													var valor = respuesta.codigo_oyd;
													var texto = respuesta.codigo_oyd;
													$("#dashlet10_cuenta2").append('<option value="'+valor+'">'+texto+'</option>');		
												}
											});	
										}
									}
									
									if ( _.isEmpty( $("#dashlet10_cuenta2").html().trim() ) ){ 
										$("#dashlet10_cuenta2").append('<option value="">--</option>');
									}
								}
								else{				
									
									app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
										level: 'error',
										title: 'Error tipoIdentificacion vacio',
										autoClose: false,
									});
								}
							}
							else{				
								
								app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
									level: 'error',
									title: 'Error tipoIdentificacion vacio',
									autoClose: false,
								});
							}
						}
					}
				}
				
				
				if ( _.isEmpty( $("#dashlet10_cuenta2").html().trim() ) ){ 
					$("#dashlet10_cuenta2").append('<option value="">--</option>');
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
					level: 'error',
					title: 'Error último día habil vacio',
					autoClose: false,
				});
			}
		}
	},
	dashlet10_portal_Tipo_Certificado_change: function(autenticarse){	
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		console.log("dashlet10_portal_dashlet10_Tipo_Certificado_change",self.cuenta);
		$("#dashlet10_Vigencia").empty();//limpiando
		if (!_.isEmpty( self.certificado )){		
			var i = $("#dashlet10_Tipo_Certificado").val();
			var anioDesde = self.certificado[i].anioDesde;
			var anioHasta = self.certificado[i].anioHasta;		
			self.ruta = self.certificado[i].rutaCert;	
			for (j = anioDesde; j <= anioHasta; j++) { 
			  $("#dashlet10_Vigencia").append('<option value="'+j+'">'+j+'</option>');
			}
		}
		if ( _.isEmpty( $("#dashlet10_Vigencia").html().trim() ) ){ 
			$("#dashlet10_Vigencia").append('<option value="">--</option>');
		}
	},
	dashlet10_portal_obtener_Tipo_Certificado: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet10_Tipo_Certificado").empty();//limpiando
		self.ws = "/api_fiducia/certificaciones/valores/consultas/certificaciones/certificadosTributariosList";
		self.tipo = "GET";
		self.data = [];
		self.dashlet10_portal_call({});//autenticandose y consumo
		self.certificado = [];		
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("dashlet10_portal_obtener_Tipo_Certificado: ",self.respuesta);
				self.certificado = self.respuesta;
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.idCert !== 'undefined') {
						var valor = i;
						var texto = respuesta.nombreCert;
						$("#dashlet10_Tipo_Certificado").append('<option value="'+valor+'">'+texto+'</option>');			
					}
				});
				self.dashlet10_portal_Tipo_Certificado_change(false);
			}
			else{				
				self.certificado = [];		
				$("#dashlet10_Tipo_Certificado").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener Tipo_Certificado vacio',
					autoClose: false,
				});
			}
		}
		
		if ( _.isEmpty( $("#dashlet10_Tipo_Certificado").html().trim() ) ){ 
			$("#dashlet10_Tipo_Certificado").append('<option value="">--</option>');
		}	
	},
	dashlet_portal_obtener_enviar20: function(autenticarse, enviar) {	
		var self = dashlet10;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet10_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet10_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {			
			if( !_.isEmpty($("#dashlet10_id_cliente").val()) && !_.isEmpty($("#dashlet10_cuenta1").val()) && !_.isEmpty($("#dashlet10_Periodo").val()) ) {
				var Address = "";
				if(!_.isEmpty($("#dashlet10_correo").val())) {
					if(typeof $("#dashlet10_correo").val()[0] !== 'undefined') {
						if(!_.isEmpty($("#dashlet10_correo").val()[0])) {
							//Address = $("#dashlet10_correo").val().join("; ");
							Address = $("#dashlet10_correo").val();
						}
					}
				}
				
				if(_.isEmpty(Address)) {
					app.alert.show('error_dashlet_portal_obtener_enviar20', {
						level: 'error',
						title: "Por favor, seleccione una direccion de correo!!",
						autoClose: false,
					});
				}
				else{			
					self.ws = "/extractos-v1/api_alianza/docs/extractosPorCorreo";
					self.tipo = "POST";
					self.data = {
						"tipoProducto": "CI",
						"producto": $("#dashlet10_cuenta1").val(),
						"periodo":$("#dashlet10_Periodo").val(),
						"tipoIdentificacion": self.tipoIdentificacion,
						"numeroIdentificacion": $("#dashlet10_id_cliente").val(),
						"correo": Address,
					};
					
					self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
					
					if(self.respuesta == '' | self.respuesta == null){
						self.respuesta = "En proceso...";
						
						app.alert.show('dashlet10_dashlet_portal_obtener_enviar20', {
							level: 'success',
							title: 'Se ha enviado la solicitud al servidor!!',							
							messages:'<hr><p><b>Respuesta:</b><p><br><p>'+self.respuesta+"<p>",
							autoClose: false,
						});	
					}
					else{						
						if(typeof self.respuesta.error !== 'undefined') {
							app.alert.show('error_dashlet_portal_obtener_enviar20', {
								level: 'error',
								title: '<p><br>Error al enviar PDF Extractos',
								messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
								autoClose: false,
							});
						}
						else{	
							app.alert.show('dashlet10_dashlet_portal_obtener_enviar20', {
								level: 'error',
								title: '<p><br>Error al enviar PDF Extractos',						
								messages:"Fallo en el  WS de enviar PDF pro correo para Extractos",
								autoClose: false,
							});	
						}
					}
				}
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_enviar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet10_portal_procesando');
			document.getElementById('dashlet10_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet_portal_obtener_descargar20: function(autenticarse, enviar) {
		var self = dashlet10;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet10_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet10_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if( !_.isEmpty($("#dashlet10_id_cliente").val()) && !_.isEmpty($("#dashlet10_cuenta1").val()) && !_.isEmpty($("#dashlet10_Periodo").val()) ) {
				self.ws = "/extractos-v1/api_alianza/docs/extractos";
				self.tipo = "POST";
				self.data = {
					"tipoProducto": "CI",
					"producto": $("#dashlet10_cuenta1").val(),
					"periodo":$("#dashlet10_Periodo").val(),
					"tipoIdentificacion": self.tipoIdentificacion,
					"numeroIdentificacion": $("#dashlet10_id_cliente").val(), 
				};
				
				self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
				
				if(!_.isEmpty(self.respuesta)) {
					if(typeof self.respuesta.error !== 'undefined') {
						app.alert.show('error_dashlet_portal_obtener_descargar20', {
							level: 'error',
							title: '<p><br>Error al descargar PDF Extractos',
							messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
							autoClose: false,
						});
					}
					else{
						//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
                        var mapForm = document.createElement("form");
                            mapForm.target = "Map";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

                            var mapInput = document.createElement("input");
                            mapInput.type = "text";
                            mapInput.name = "file";
                            mapInput.value = self.respuesta;
                            mapForm.appendChild(mapInput);

                            document.body.appendChild(mapForm);

                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

                        if (map) {
                            mapForm.submit();
                        }
						else{
							app.alert.show('error_procesando_obtener_descargar1', {
								level: 'error',
								title: 'Error al descargar PDF',
								messages: "El navegador esta bloqueando la ventana!!!",
								autoClose: false,
							});
						}
					}
				} 
				else {
					app.alert.show('error_dashlet_portal_obtener_descargar20', {
						level: 'error',
						title: 'Error al descargar PDF Extractos',
						autoClose: false,
					});
				}
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_descargar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet10_portal_procesando');
			document.getElementById('dashlet10_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet_portal_call20: function(PAI, mail) {
		var self = this;
		if(typeof app === 'undefined') app = App;
		var method = "dashlet_portal";
		var data = [{
			'url': self.url,
			'tipo': self.tipo,
			'ws': self.ws,
			'data': self.data,
			'PAI': PAI,
		}];

		var url = app.api.buildURL(method, 'read', '', {
			data
		});

		app.api.call('read', url, null, {
			success: _.bind(function(response) {
				self.respuesta = response['respuesta'];
			}, this),
			error: _.bind(function(error) {
				self.token = null;
				self.respuesta = null;

				app.alert.show('error_dashlet_portal_call20', {
					level: 'error',
					title: 'Fallo al procesar ' + error,
					autoClose: false,
				});
			}, this),
		}, {
			async: false
		});
	},
	dashlet10_portal_call: function(mail){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Para el periodo
		Al parecer no hay ningún servicio que realice esta lógica, el portal con esa función lo que hace es calcular los últimos 6 meses a partir de la fecha para mostrar en el listado.

		*/
		if (typeof app === 'undefined') app = App;
		var method  = "dashlet_portal";		
		var data = [
			{
				'url':self.url,			
				'tipo':'POST',	
				'ws':'/auth_fiducia/comercial/autenticar',
				'data': "**PAI**",
				'autenticarse': self.autenticarse,
				'token': self.token,
			},
			{				
				//call
				'url':self.url,
				'tipo':self.tipo,
				'ws':self.ws,
				'data':self.data,
				'mail':mail,
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) { 
				self.token=response['token'];
				self.respuesta=response['respuesta'];	
				
				
				if(self.token.includes("Credenciales invalidas")){
					error = JSON.parse(self.token);				
					if(error["data"] == "Credenciales invalidas" ){						
						app.alert.show('error_procesando_obtener_fondo', {
							level: 'error',
							title: 'Error '+error["code"]+' '+error["data"],
							autoClose: false,
						});
						self.token=null;
					}
				}	
				
			}, this),  
			error: _.bind(function(error) {  
				self.token=null;
				self.respuesta=null;
				
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	}
})
