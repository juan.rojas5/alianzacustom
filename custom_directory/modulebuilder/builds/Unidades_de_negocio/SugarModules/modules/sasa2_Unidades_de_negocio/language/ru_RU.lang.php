<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Создано по названию',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio Список',
  'LBL_MODULE_NAME' => 'Unidades de negocio',
  'LBL_MODULE_TITLE' => 'Unidades de negocio',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidades de negocio',
  'LBL_HOMEPAGE_TITLE' => 'Моя Unidades de negocio',
  'LNK_NEW_RECORD' => 'Создать Unidades de negocio',
  'LNK_LIST' => 'Просмотр Unidades de negocio',
  'LNK_IMPORT_SASA2_UNIDADES_DE_NEGOCIO' => 'Importar Unidades de negocio',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Unidades de negocio',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_SUBPANEL_TITLE' => 'Unidades de negocio',
  'LBL_NEW_FORM_TITLE' => 'Новый Unidades de negocio',
  'LNK_IMPORT_VCARD' => 'Importar Unidades de negocio vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidades de negocio record by importing a vCard from your file system.',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio Panel de enfoque',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_RECORD_DASHBOARD' => 'Unidades de negocio Cuadro de mando del registro',
);