<?php
// created: 2022-09-09 19:35:12
$role_dropdown_filters['source_dom'] = array (
  '' => true,
  'Internal' => false,
  'Forum' => false,
  'Web' => false,
  'InboundEmail' => true,
  'Twitter' => false,
  'SFC' => false,
  'Defensor del Consumidor' => false,
  'Carta' => false,
  'portal publico' => true,
  'portal transaccional' => true,
  'Telefono' => true,
);