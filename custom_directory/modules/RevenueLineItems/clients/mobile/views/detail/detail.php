<?php
// created: 2023-02-03 09:39:47
$viewdefs['RevenueLineItems']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'fields' => 
      array (
        0 => 'name',
        1 => 'opportunity_name',
        2 => 'account_name',
        3 => 'product_template_name',
        4 => 'likely_case',
        5 => 'date_closed',
        6 => 'tag',
        7 => 'assigned_user_name',
        8 => 'team_name',
      ),
    ),
  ),
);