<?php
$module_name = 'sasa_sasa_tipificaciones';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_motivo_c',
                'label' => 'LBL_SASA_MOTIVO_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_proceso_c',
                'label' => 'LBL_SASA_PROCESO_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_detalle_c',
                'label' => 'LBL_SASA_DETALLE_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_wf_c',
                'label' => 'LBL_SASA_WF_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              6 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              8 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_anshorariolaboral_c',
                'label' => 'LBL_SASA_ANSHORARIOLABORAL_C',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'sasa_ansfuerahorariolaboral__c',
                'label' => 'LBL_SASA_ANSFUERAHORARIOLABORAL_',
                'enabled' => true,
                'readonly' => false,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
