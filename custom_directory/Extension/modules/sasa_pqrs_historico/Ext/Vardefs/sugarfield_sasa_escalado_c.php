<?php
 // created: 2022-11-18 20:48:38
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['labelValue']='Escalado';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['readonly_formula']='';

 ?>