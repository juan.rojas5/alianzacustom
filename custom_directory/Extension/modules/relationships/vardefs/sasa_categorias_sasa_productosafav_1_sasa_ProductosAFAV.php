<?php
// created: 2019-01-04 21:22:57
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_Categorias',
  'bean_name' => 'sasa_Categorias',
  'side' => 'right',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link-type' => 'one',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1_name"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_CATEGORIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasa_productosafav_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'name',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1sasa_categorias_ida"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE_ID',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasa_productosafav_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
