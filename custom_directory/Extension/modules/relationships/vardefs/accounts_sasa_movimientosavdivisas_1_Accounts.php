<?php
// created: 2019-01-04 21:41:00
$dictionary["Account"]["fields"]["accounts_sasa_movimientosavdivisas_1"] = array (
  'name' => 'accounts_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAVDivisas',
  'bean_name' => 'sasa_MovimientosAVDivisas',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
