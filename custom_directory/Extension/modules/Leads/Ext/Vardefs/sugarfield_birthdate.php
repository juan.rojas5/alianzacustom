<?php
 // created: 2021-03-09 20:30:32
$dictionary['Lead']['fields']['birthdate']['audited']=false;
$dictionary['Lead']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Lead']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Lead']['fields']['birthdate']['calculated']=false;
$dictionary['Lead']['fields']['birthdate']['pii']=false;
$dictionary['Lead']['fields']['birthdate']['enable_range_search']=false;

 ?>