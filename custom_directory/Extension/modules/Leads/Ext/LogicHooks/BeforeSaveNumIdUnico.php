<?php

$hook_array['before_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'BeforeSaveNumIdUnico',

  //The PHP file where your class is located.
  'custom/modules/Leads/BeforeSaveNumIdUnico.php',

  //The class the method is in.
  'BeforeSaveNumIdUnico',

  //The method to call.
  'before_save'
);

?>