<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Priradené používateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Priradené k',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenené používateľom',
  'LBL_MODIFIED_ID' => 'Upravené používateľom s ID',
  'LBL_MODIFIED_NAME' => 'Upravené používateľom s menom',
  'LBL_CREATED' => 'Vytvoril',
  'LBL_CREATED_ID' => 'Vytvorené používateľom s ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Obľúbení používatelia',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upravené používateľom s menom',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Vytvorené pod menom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'PQRs Histórico Zoznam',
  'LBL_MODULE_NAME' => 'PQRs Histórico',
  'LBL_MODULE_TITLE' => 'PQRs Histórico',
  'LBL_MODULE_NAME_SINGULAR' => 'PQRs Histórico',
  'LBL_HOMEPAGE_TITLE' => 'Moje PQRs Histórico',
  'LNK_NEW_RECORD' => 'Vytvoriť PQRs Histórico',
  'LNK_LIST' => 'Zobraziť PQRs Histórico',
  'LNK_IMPORT_SASA_PQRS_HISTORICO' => 'Importar PQRs Histórico',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie PQRs Histórico',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_SASA_PQRS_HISTORICO_SUBPANEL_TITLE' => 'PQRs Histórico',
  'LBL_NEW_FORM_TITLE' => 'Nové PQRs Histórico',
  'LNK_IMPORT_VCARD' => 'Importar PQRs Histórico vCard',
  'LBL_IMPORT' => 'Importar PQRs Histórico',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new PQRs Histórico record by importing a vCard from your file system.',
  'LBL_SASA_PQRS_HISTORICO_FOCUS_DRAWER_DASHBOARD' => 'PQRs Histórico Panel de enfoque',
  'LBL_SASA_PQRS_HISTORICO_RECORD_DASHBOARD' => 'PQRs Histórico Cuadro de mando del registro',
);