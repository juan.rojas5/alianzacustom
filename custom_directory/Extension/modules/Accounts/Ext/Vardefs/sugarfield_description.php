<?php
 // created: 2020-09-18 03:00:16
$dictionary['Account']['fields']['description']['audited']=false;
$dictionary['Account']['fields']['description']['massupdate']=false;
$dictionary['Account']['fields']['description']['comments']='Full text of the note';
$dictionary['Account']['fields']['description']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['description']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['description']['merge_filter']='disabled';
$dictionary['Account']['fields']['description']['reportable']=false;
$dictionary['Account']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.72',
  'searchable' => true,
);
$dictionary['Account']['fields']['description']['calculated']=false;
$dictionary['Account']['fields']['description']['rows']='6';
$dictionary['Account']['fields']['description']['cols']='80';
$dictionary['Account']['fields']['description']['hidemassupdate']=false;

 ?>