<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Layoutdefs/sasa_sasa_tipificaciones_cases_1_sasa_sasa_tipificaciones.php

 // created: 2022-04-29 13:46:19
$layout_defs["sasa_sasa_tipificaciones"]["subpanel_setup"]['sasa_sasa_tipificaciones_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'sasa_sasa_tipificaciones_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
