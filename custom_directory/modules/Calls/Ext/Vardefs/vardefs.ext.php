<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/audit.php

$dictionary['Call']['audited'] = true;
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_name.php

 // created: 2018-07-06 21:57:53
$dictionary['Call']['fields']['name']['audited']=true;
$dictionary['Call']['fields']['name']['massupdate']=false;
$dictionary['Call']['fields']['name']['comments']='Brief description of the call';
$dictionary['Call']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['name']['merge_filter']='disabled';
$dictionary['Call']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.41',
  'searchable' => true,
);
$dictionary['Call']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-07-06 21:58:47
$dictionary['Call']['fields']['date_entered']['audited']=true;
$dictionary['Call']['fields']['date_entered']['comments']='Date record created';
$dictionary['Call']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_entered']['calculated']=false;
$dictionary['Call']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_start.php

 // created: 2018-07-06 21:59:20
$dictionary['Call']['fields']['date_start']['audited']=true;
$dictionary['Call']['fields']['date_start']['comments']='Date in which call is schedule to (or did) start';
$dictionary['Call']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_start']['full_text_search']=array (
);
$dictionary['Call']['fields']['date_start']['calculated']=false;
$dictionary['Call']['fields']['date_start']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_reminder_time.php

 // created: 2018-07-06 21:59:35
$dictionary['Call']['fields']['reminder_time']['default']='-1';
$dictionary['Call']['fields']['reminder_time']['audited']=true;
$dictionary['Call']['fields']['reminder_time']['comments']='Specifies when a reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Call']['fields']['reminder_time']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['reminder_time']['merge_filter']='disabled';
$dictionary['Call']['fields']['reminder_time']['calculated']=false;
$dictionary['Call']['fields']['reminder_time']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_email_reminder_time.php

 // created: 2018-07-06 21:59:47
$dictionary['Call']['fields']['email_reminder_time']['default']='-1';
$dictionary['Call']['fields']['email_reminder_time']['audited']=true;
$dictionary['Call']['fields']['email_reminder_time']['comments']='Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Call']['fields']['email_reminder_time']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['email_reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['email_reminder_time']['merge_filter']='disabled';
$dictionary['Call']['fields']['email_reminder_time']['calculated']=false;
$dictionary['Call']['fields']['email_reminder_time']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-07-06 22:00:45
$dictionary['Call']['fields']['date_modified']['audited']=true;
$dictionary['Call']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Call']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_modified']['calculated']=false;
$dictionary['Call']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_vencida_c.php

 // created: 2018-07-21 21:03:05
$dictionary['Call']['fields']['sasa_vencida_c']['labelValue']='Vencida';
$dictionary['Call']['fields']['sasa_vencida_c']['dependency']='';
$dictionary['Call']['fields']['sasa_vencida_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2018-07-26 02:03:30
$dictionary['Call']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Call']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Call']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Call']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Call']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Call']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Call']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2018-07-26 02:05:19
$dictionary['Call']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Call']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Call']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Call']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Call']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['Call']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Call']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/rli_link_workflow.php

$dictionary['Call']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_direction.php

 // created: 2018-08-11 14:52:32
$dictionary['Call']['fields']['direction']['default']='Outbound';
$dictionary['Call']['fields']['direction']['audited']=true;
$dictionary['Call']['fields']['direction']['massupdate']=true;
$dictionary['Call']['fields']['direction']['comments']='Indicates whether call is inbound or outbound';
$dictionary['Call']['fields']['direction']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['direction']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['direction']['merge_filter']='disabled';
$dictionary['Call']['fields']['direction']['calculated']=false;
$dictionary['Call']['fields']['direction']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_status.php

 // created: 2018-08-11 21:48:10
$dictionary['Call']['fields']['status']['audited']=true;
$dictionary['Call']['fields']['status']['massupdate']=true;
$dictionary['Call']['fields']['status']['comments']='The status of the call (Held, Not Held, etc.)';
$dictionary['Call']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['status']['merge_filter']='disabled';
$dictionary['Call']['fields']['status']['calculated']=false;
$dictionary['Call']['fields']['status']['dependency']=false;
$dictionary['Call']['fields']['status']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_resultadollamada_c.php

 // created: 2018-08-21 01:27:07
$dictionary['Call']['fields']['sasa_resultadollamada_c']['labelValue']='Resultado de la Llamada';
$dictionary['Call']['fields']['sasa_resultadollamada_c']['dependency']='';
$dictionary['Call']['fields']['sasa_resultadollamada_c']['visibility_grid']=array (
  'trigger' => 'status',
  'values' => 
  array (
    'Planned' => 
    array (
    ),
    'Held' => 
    array (
      0 => '',
      1 => 'Interesado',
      2 => 'Efectivo',
      3 => 'Negocio en Tramite',
      4 => 'Descartado',
      5 => 'Postergado',
    ),
    'Not Held' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_objetivollamada_c.php

 // created: 2018-09-07 22:04:37
$dictionary['Call']['fields']['sasa_objetivollamada_c']['labelValue']='Objetivo de la Llamada';
$dictionary['Call']['fields']['sasa_objetivollamada_c']['dependency']='';
$dictionary['Call']['fields']['sasa_objetivollamada_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_date_end.php

 // created: 2018-09-10 22:12:07
$dictionary['Call']['fields']['date_end']['audited']=true;
$dictionary['Call']['fields']['date_end']['comments']='Date is which call is scheduled to (or did) end';
$dictionary['Call']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_end']['calculated']=false;
$dictionary['Call']['fields']['date_end']['enable_range_search']='1';
$dictionary['Call']['fields']['date_end']['required']=true;
$dictionary['Call']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Call']['fields']['date_end']['group_label']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_parent_name.php

 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_name']['len']=36;
$dictionary['Call']['fields']['parent_name']['required']=true;
$dictionary['Call']['fields']['parent_name']['audited']=false;
$dictionary['Call']['fields']['parent_name']['massupdate']=false;
$dictionary['Call']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_type']['len']=255;
$dictionary['Call']['fields']['parent_type']['audited']=false;
$dictionary['Call']['fields']['parent_type']['massupdate']=false;
$dictionary['Call']['fields']['parent_type']['comments']='';
$dictionary['Call']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_type']['calculated']=false;
$dictionary['Call']['fields']['parent_type']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_parent_id.php

 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_id']['len']=255;
$dictionary['Call']['fields']['parent_id']['audited']=false;
$dictionary['Call']['fields']['parent_id']['massupdate']=false;
$dictionary['Call']['fields']['parent_id']['comments']='';
$dictionary['Call']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_id']['calculated']=false;
$dictionary['Call']['fields']['parent_id']['reportable']=true;
$dictionary['Call']['fields']['parent_id']['unified_search']=false;
$dictionary['Call']['fields']['parent_id']['group']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_envio_compro_c.php

 // created: 2019-10-26 00:06:30
$dictionary['Call']['fields']['sasa_envio_compro_c']['duplicate_merge_dom_value']=0;
$dictionary['Call']['fields']['sasa_envio_compro_c']['calculated']='true';
$dictionary['Call']['fields']['sasa_envio_compro_c']['formula']='ifElse(not(equal($sasa_fechaproximallamada_c,$sasa_fechaproximallamada_c)),concat("SI"),concat(""))';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_llamadacompaniade_c.php

 // created: 2020-05-06 23:07:31
$dictionary['Call']['fields']['sasa_llamadacompaniade_c']['duplicate_merge_dom_value']=0;
$dictionary['Call']['fields']['sasa_llamadacompaniade_c']['labelValue']='Llamada en Compañía de:';
$dictionary['Call']['fields']['sasa_llamadacompaniade_c']['calculated']='';
$dictionary['Call']['fields']['sasa_llamadacompaniade_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_description.php

 // created: 2020-05-04 10:49:09
$dictionary['Call']['fields']['description']['audited']=true;
$dictionary['Call']['fields']['description']['massupdate']=false;
$dictionary['Call']['fields']['description']['comments']='Full text of the note';
$dictionary['Call']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['description']['merge_filter']='disabled';
$dictionary['Call']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.54',
  'searchable' => true,
);
$dictionary['Call']['fields']['description']['calculated']=false;
$dictionary['Call']['fields']['description']['rows']='6';
$dictionary['Call']['fields']['description']['cols']='80';
$dictionary['Call']['fields']['description']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_compromiso_c.php

 // created: 2019-10-10 14:25:29
$dictionary['Call']['fields']['sasa_compromiso_c']['labelValue']='Compromiso';
$dictionary['Call']['fields']['sasa_compromiso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Call']['fields']['sasa_compromiso_c']['enforced']='';
$dictionary['Call']['fields']['sasa_compromiso_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadollamada_c,"Descartado")))';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Call']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_productosofrecidos_c.php

 // created: 2021-11-30 14:06:19
$dictionary['Call']['fields']['sasa_productosofrecidos_c']['labelValue']='Productos Ofrecidos';
$dictionary['Call']['fields']['sasa_productosofrecidos_c']['dependency']='';
$dictionary['Call']['fields']['sasa_productosofrecidos_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_productosofrecidos_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_productosofrecidos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_responsableejecucion_c.php

 // created: 2022-06-25 19:16:08
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['labelValue']='Responsable de la Ejecución';
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['dependency']='';
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['visibility_grid']='';
$dictionary['Call']['fields']['sasa_responsableejecucion_c']['required']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_fechaproximallamada_c.php

 // created: 2022-06-25 14:58:09
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['labelValue']='Fecha de la Próxima Llamada';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['enforced']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadollamada_c,"Descartado")))';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['required']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_agendar_c.php

 // created: 2022-11-09 15:53:52
$dictionary['Call']['fields']['agendar_c']['labelValue']='Agendar';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_sasa_programarllamada_c.php

 // created: 2023-01-26 21:29:55
$dictionary['Call']['fields']['sasa_programarllamada_c']['labelValue']='¿Programar visita o llamada?';
$dictionary['Call']['fields']['sasa_programarllamada_c']['dependency']='and(equal($status,"Held"))';
$dictionary['Call']['fields']['sasa_programarllamada_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_programarllamada_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_programarllamada_c']['visibility_grid']=array (
);

 
?>
