<?php
$module_name = 'sasa_SaldosAF';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_nombrefondo_c',
                'label' => 'LBL_SASA_NOMBREFONDO_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_nombresubcuenta_c',
                'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'accounts_sasa_saldosaf_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_SASA_SALDOSAF_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_productosafav_sasa_saldosaf_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
                'enabled' => true,
                'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1SASA_PRODUCTOSAFAV_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_inversiones_c',
                'label' => 'LBL_SASA_INVERSIONES_C',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_digito_c',
                'label' => 'LBL_SASA_DIGITO_C',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_plan_c',
                'label' => 'LBL_SASA_PLAN_C',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_numerounid_c',
                'label' => 'LBL_SASA_NUMEROUNID_C',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_valorunid_c',
                'label' => 'LBL_SASA_VALORUNID_C',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_saldoactual_c',
                'label' => 'LBL_SASA_SALDOACTUAL_C',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'sasa_fechasaldo_c',
                'label' => 'LBL_SASA_FECHASALDO_C',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'sasa_ingresosdia_c',
                'label' => 'LBL_SASA_INGRESOSDIA_C',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'sasa_egresosdia_c',
                'label' => 'LBL_SASA_EGRESOSDIA_C',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'sasa_canje_c',
                'label' => 'LBL_SASA_CANJE_C',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'sasa_retcont_c',
                'label' => 'LBL_SASA_RETCONT_C',
                'enabled' => true,
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'sasa_retrend_c',
                'label' => 'LBL_SASA_RETREND_C',
                'enabled' => true,
                'default' => true,
              ),
              19 => 
              array (
                'name' => 'sasa_ctrlcancel_c',
                'label' => 'LBL_SASA_CTRLCANCEL_C',
                'enabled' => true,
                'default' => true,
              ),
              20 => 
              array (
                'name' => 'sasa_reservado_c',
                'label' => 'LBL_SASA_RESERVADO_C',
                'enabled' => true,
                'default' => true,
              ),
              21 => 
              array (
                'name' => 'sasa_4x1000_c',
                'label' => 'LBL_SASA_4X1000_C',
                'enabled' => true,
                'default' => true,
              ),
              22 => 
              array (
                'name' => 'sasa_sinconsol_c',
                'label' => 'LBL_SASA_SINCONSOL_C',
                'enabled' => true,
                'default' => true,
              ),
              23 => 
              array (
                'name' => 'sasa_maximoretiro_c',
                'label' => 'LBL_SASA_MAXIMORETIRO_C',
                'enabled' => true,
                'default' => true,
              ),
              24 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              25 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => true,
              ),
              26 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              27 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
              28 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              29 => 
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
              30 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              31 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              32 => 
              array (
                'name' => 'my_favorite',
                'label' => 'LBL_FAVORITE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
