<?php
 // created: 2018-07-04 16:57:57
$dictionary['Contact']['fields']['assistant']['audited']=false;
$dictionary['Contact']['fields']['assistant']['massupdate']=false;
$dictionary['Contact']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Contact']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant']['reportable']=false;
$dictionary['Contact']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant']['calculated']=false;
$dictionary['Contact']['fields']['assistant']['pii']=false;
$dictionary['Contact']['fields']['assistant']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>