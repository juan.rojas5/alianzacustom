<?php
 // created: 2019-01-04 21:37:35
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_saldosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_SaldosAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'get_subpanel_data' => 'accounts_sasa_saldosav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
