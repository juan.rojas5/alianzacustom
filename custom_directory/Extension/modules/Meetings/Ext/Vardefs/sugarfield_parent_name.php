<?php
 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_name']['len']=36;
$dictionary['Meeting']['fields']['parent_name']['required']=true;
$dictionary['Meeting']['fields']['parent_name']['audited']=false;
$dictionary['Meeting']['fields']['parent_name']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_name']['calculated']=false;

 ?>