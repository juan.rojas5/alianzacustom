<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sugarfield_sasa_deficit_c.php

 // created: 2019-03-08 18:37:57

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sugarfield_sasa_saldoconsolidado_c.php

 // created: 2019-03-08 18:39:10

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2019-03-08 18:54:05
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2019-03-08 18:54:06
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['sasaP_PresupuestoxProducto']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_sasaP_PresupuestoxProducto.php

// created: 2019-03-08 18:56:45
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestosxAsesor',
  'bean_name' => 'sasaP_PresupuestosxAsesor',
  'side' => 'right',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link-type' => 'one',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
  'save' => true,
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'table' => 'sasap_presupuestosxasesor',
  'module' => 'sasaP_PresupuestosxAsesor',
  'rname' => 'name',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_pres107bxasesor_ida"] = array (
  'name' => 'sasap_pres107bxasesor_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE_ID',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'table' => 'sasap_presupuestosxasesor',
  'module' => 'sasaP_PresupuestosxAsesor',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/sasa_productosafav_sasap_presupuestoxproducto_1_sasaP_PresupuestoxProducto.php

// created: 2019-03-08 18:57:52
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1_name"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_produff64tosafav_ida',
  'link' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_produff64tosafav_ida"] = array (
  'name' => 'sasa_produff64tosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE_ID',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestoxProducto/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasaP_PresupuestoxProducto']['full_text_search']=true;

?>
