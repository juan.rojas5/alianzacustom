<?php
 // created: 2018-07-14 15:07:34
$dictionary['Contact']['fields']['alt_address_state']['required']=false;
$dictionary['Contact']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_state']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_state']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>