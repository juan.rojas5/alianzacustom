<?php

$job_strings[] = 'ScheduleTaskVacations';
function ScheduleTaskVacations()
{
    global $db;
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio ScheduleTaskVacations. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    //Tarea 16425

    //Consulta para obtener quien está en vacaciones.
    $query="SELECT * FROM users_cstm WHERE users_cstm.sasa_envacaciones_c=1 and users_cstm.sasa_controlvacaciones_c=0";
    $result = $db->query($query);
    while($row = $db->fetchByAssoc($result)){
        if(!empty($row['id_c'])){
            //Validar que fecha sea igual a hoy
            $today = new DateTime(); // This object represents current date/time
            $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $row['sasa_fechainicio_c']);
            $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $diff = $today->diff( $match_date );
            $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

            if ($diffDays==0) {
                //Optener quien sale de vacaciones custom
                $Sale_Vacaciones = BeanFactory::getBean($users_cstm, $row['id_c']);
                //optener equipo privado de quien sale a vacaciones
                $queryTeam="SELECT * FROM `teams` WHERE associated_user_id='{$row['id_c']}'";
                $resultTeam = $db->query($queryTeam);
                while($rowTeam = $db->fetchByAssoc($resultTeam)){
                    if(!empty($rowTeam['id'])){
                        $PrivateTeam=$rowTeam['id'];
                    }
                }
                //Asignar equipo privado al reemplazante
                $Remplazante = BeanFactory::getBean($users, $row['user_id_c']);
                //Load the team relationship 
                $Remplazante->load_relationship('teams');
                //Add the teams
                $Remplazante->teams->add(
                    array(
                        $PrivateTeam
                    )
                );
                //guardar
                $Remplazante->save();
                //Marcacion para evitar leer dos veces
                $Sale_Vacaciones->sasa_controlvacaciones_c=1;
                $Sale_Vacaciones->save();
            }
        }
    }


    //Consulta para obtener quien vuelve de vacaciones con base al campo fecha fin
    $query="SELECT * FROM users_cstm WHERE users_cstm.sasa_envacaciones_c=1 and users_cstm.sasa_controlvacaciones_c=1";
    $result = $db->query($query);
    while($row = $db->fetchByAssoc($result)){
        if(!empty($row['id_c'])){
            //Validar que fecha sea igual a hoy
            $today = new DateTime(); // This object represents current date/time
            $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $row['sasa_fechafin_c']);
            $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $diff = $today->diff( $match_date );
            $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

            if ($diffDays==0) {
                //Optener quien sale a vacaciones
                $Sale_Vacaciones = BeanFactory::getBean($users_cstm, $row['id_c']);
                //optener equipo privado de quien sale a vacaciones
                $queryTeam="SELECT * FROM `teams` WHERE associated_user_id='{$row['id_c']}'";
                $resultTeam = $db->query($queryTeam);
                while($rowTeam = $db->fetchByAssoc($resultTeam)){
                    if(!empty($rowTeam['id'])){
                        $PrivateTeam=$rowTeam['id'];
                    }
                }
                //Optener el remplazante
                $Remplazante = BeanFactory::getBean($users, $row['user_id_c']);
                //Retirar equipo privado al remplazante del entrante
                //Load the team relationship 
                $Remplazante->load_relationship('teams');

                //Remove the teams
                $Remplazante->teams->remove(
                    array(
                        $PrivateTeam 
                    )
                );
                //guardar
                $Remplazante->save();
                //retirar marcacion para evitar leer dos veces
                $Sale_Vacaciones->sasa_controlvacaciones_c=0;
                $Sale_Vacaciones->save();   
            }
            
        }
    }



    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin ScheduleTaskVacations. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
}

