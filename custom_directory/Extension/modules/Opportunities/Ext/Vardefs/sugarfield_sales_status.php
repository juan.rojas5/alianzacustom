<?php
 // created: 2022-07-28 03:53:01
$dictionary['Opportunity']['fields']['sales_status']['audited']=false;
$dictionary['Opportunity']['fields']['sales_status']['massupdate']=true;
$dictionary['Opportunity']['fields']['sales_status']['importable']='true';
$dictionary['Opportunity']['fields']['sales_status']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_status']['reportable']=true;
$dictionary['Opportunity']['fields']['sales_status']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_status']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_status']['studio']=true;
$dictionary['Opportunity']['fields']['sales_status']['len']=100;
$dictionary['Opportunity']['fields']['sales_status']['default']='New';
$dictionary['Opportunity']['fields']['sales_status']['options']='sales_status_dom';
$dictionary['Opportunity']['fields']['sales_status']['hidemassupdate']=false;

 ?>