<?php
$dependencies['Leads']['SetOptionDepartamento28'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Santander")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Miguel","Santa Barbara","Villanueva","Albania","Guadalupe","El Penon","Cabrera","Chima","La Paz","Bolivar","Sucre","Aguada","Aratoca","Barbosa","Barichara","Barrancabermeja","Betulia","Bucaramanga","California","Capitanejo","Carcasi","Cepita","Cerrito","Charala","Charta","Chipata","Cimitarra","Concepcion","Confines","Contratacion","Coromoro","Curiti","El Carmen de Chucuri","El Guacamayo","El Playon","Encino","Enciso","Florian","Floridablanca","Galan","Gambita","Giron","Guaca","Guapota","Guavata","Güepsa","Hato","Jesus Maria","Jordan","La Belleza","Landazuri","Lebrija","Los Santos","Macaravita","Malaga","Matanza","Mogotes","Molagavita","Ocamonte","Oiba","Onzaga","Palmar","Palmas del Socorro","Paramo","Piedecuesta","Pinchote","Puente Nacional","Puerto Parra","Puerto Wilches","Rionegro","Sabana de Torres","San Andres","San Benito","San Gil","San Joaquin","San Jose de Miranda","San Vicente de Chucuri","Santa Helena del Opon","Simacota","Socorro","Suaita","Surata","Tona","Valle de San Jose","Velez","Vetas","Zapatoca")',
           'labels' => 'createList("","San Miguel","Santa Bárbara","Villanueva","Albania","Guadalupe","El Peñón","Cabrera","Chimá","La Paz","Bolívar","Sucre","Aguada","Aratoca","Barbosa","Barichara","Barrancabermeja","Betulia","Bucaramanga","California","Capitanejo","Carcasí","Cepitá","Cerrito","Charalá","Charta","Chipatá","Cimitarra","Concepción","Confines","Contratación","Coromoro","Curití","El Carmen de Chucurí","El Guacamayo","El Playón","Encino","Enciso","Florián","Floridablanca","Galán","Gambita","Girón","Guaca","Guapotá","Guavatá","Güepsa","Hato","Jesús María","Jordán","La Belleza","Landázuri","Lebríja","Los Santos","Macaravita","Málaga","Matanza","Mogotes","Molagavita","Ocamonte","Oiba","Onzaga","Palmar","Palmas del Socorro","Páramo","Piedecuesta","Pinchote","Puente Nacional","Puerto Parra","Puerto Wilches","Rionegro","Sabana de Torres","San Andrés","San Benito","San Gil","San Joaquín","San José de Miranda","San Vicente de Chucurí","Santa Helena del Opón","Simacota","Socorro","Suaita","Suratá","Tona","Valle de San José","Vélez","Vetas","Zapatoca")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
