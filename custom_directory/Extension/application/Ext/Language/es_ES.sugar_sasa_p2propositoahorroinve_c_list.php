<?php
 // created: 2018-06-02 00:10:15

$app_list_strings['sasa_p2propositoahorroinve_c_list']=array (
  '' => '',
  9 => 'a. Acumular capital a mediano plazo con un fin específico (estudios, viajes, etc.)',
  5 => 'b. Ahorrar para pagar gastos del corto plazo',
  1 => 'c. Contar con una inversión que me permita tener una renta en la vejez',
  13 => 'd. Especular en el mercado de capitales, aceptando las posibles pérdidas que tenga',
);