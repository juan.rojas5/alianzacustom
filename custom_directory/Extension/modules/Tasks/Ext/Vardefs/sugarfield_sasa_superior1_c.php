<?php
 // created: 2018-07-26 03:43:07
$dictionary['Task']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Task']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Task']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_superior1_c']['dependency']='';

 ?>