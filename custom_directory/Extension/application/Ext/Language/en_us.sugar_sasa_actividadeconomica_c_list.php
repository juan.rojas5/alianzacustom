<?php
 // created: 2018-08-24 14:04:44

$app_list_strings['sasa_actividadeconomica_c_list']=array (
  '' => '',
  'Comercial' => 'Comercial',
  'Industrial' => 'Industrial',
  'Transporte' => 'Transporte',
  'Construccion' => 'Construcción',
  'Servicios' => 'Servicios',
  'Financiero' => 'Financiero',
  'Otros' => 'Otros',
  'Agroindustria' => 'Agroindustria',
  'Comunicacion' => 'Comunicación',
  'Educacion' => 'Educación',
  'Salud' => 'Salud',
  'No Registra' => 'No Registra',
);