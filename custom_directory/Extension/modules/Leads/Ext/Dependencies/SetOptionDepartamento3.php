<?php
$dependencies['Leads']['SetOptionDepartamento3'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Arauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Arauca","Arauquita","Cravo Norte","Fortul","Puerto Rondon","Saravena","Tame")',
           'labels' => 'createList("","Arauca","Arauquita","Cravo Norte","Fortul","Puerto Rondón","Saravena","Tame")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
