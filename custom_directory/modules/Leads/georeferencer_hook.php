<?php
	if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
	class Georeferencer_Leads{
		function before_save($bean, $event, $arguments){
			try{
				if(!empty($bean->sasa_pais_c)){
					$bean->primary_address_country = $bean->sasa_pais_c;
				}
				if(!empty($bean->sasa_departamento_c)){
					$bean->primary_address_state = $bean->sasa_departamento_c;
				}
				if(!empty($bean->sasa_municipio_c)){
					$bean->primary_address_city = $bean->sasa_municipio_c;
				}
				if(!empty($bean->sasa_departamentointer_c)){
					$bean->primary_address_state = $bean->sasa_departamento_c;
				}
				if(!empty($bean->sasa_municipiointer_c)){
					$bean->primary_address_city = $bean->sasa_municipio_c;
				}
			}catch (Exception $e){
			    $GLOBALS['log']->security("ERROR: error mirror georeferencing ".$e->getMessage()); 
			}	
		}
	}
?>