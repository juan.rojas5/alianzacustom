<?php
// created: 2019-01-04 21:54:13
$viewdefs['sasa_SaldosAV']['base']['view']['subpanel-for-accounts-accounts_sasa_saldosav_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_productosafav_sasa_saldosav_1_name',
          'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
          'enabled' => true,
          'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1SASA_PRODUCTOSAFAV_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_nombreproducto_c',
          'label' => 'LBL_SASA_NOMBREPRODUCTO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_nemotecnico_c',
          'label' => 'LBL_SASA_NEMOTECNICO_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_emisor_c',
          'label' => 'LBL_SASA_EMISOR_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_isinespecie_c',
          'label' => 'LBL_SASA_ISINESPECIE_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_numeroacciones_c',
          'label' => 'LBL_SASA_NUMEROACCIONES_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_tipotitulo_c',
          'label' => 'LBL_SASA_TIPOTITULO_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_fechaemision_c',
          'label' => 'LBL_SASA_FECHAEMISION_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_fechavencimiento_c',
          'label' => 'LBL_SASA_FECHAVENCIMIENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'sasa_tasafacial_c',
          'label' => 'LBL_SASA_TASAFACIAL_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'sasa_tipooperacion_c',
          'label' => 'LBL_SASA_TIPOOPERACION_C',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'sasa_nominal_c',
          'label' => 'LBL_SASA_NOMINAL_C',
          'enabled' => true,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'sasa_valorcompra_c',
          'label' => 'LBL_SASA_VALORCOMPRA_C',
          'enabled' => true,
          'default' => true,
        ),
        15 => 
        array (
          'name' => 'sasa_tirprecio_c',
          'label' => 'LBL_SASA_TIRPRECIO_C',
          'enabled' => true,
          'default' => true,
        ),
        16 => 
        array (
          'name' => 'sasa_valormercado_c',
          'label' => 'LBL_SASA_VALORMERCADO_C',
          'enabled' => true,
          'default' => true,
        ),
        17 => 
        array (
          'name' => 'sasa_tirvpn_c',
          'label' => 'LBL_SASA_TIRVPN_C',
          'enabled' => true,
          'default' => true,
        ),
        18 => 
        array (
          'name' => 'sasa_preciovpn_c',
          'label' => 'LBL_SASA_PRECIOVPN_C',
          'enabled' => true,
          'default' => true,
        ),
        19 => 
        array (
          'name' => 'sasa_disponiblenominal_c',
          'label' => 'LBL_SASA_DISPONIBLENOMINAL_C',
          'enabled' => true,
          'default' => true,
        ),
        20 => 
        array (
          'name' => 'sasa_disponiblevlrmercado_c',
          'label' => 'LBL_SASA_DISPONIBLEVLRMERCADO_C',
          'enabled' => true,
          'default' => true,
        ),
        21 => 
        array (
          'name' => 'sasa_garantianominal_c',
          'label' => 'LBL_SASA_GARANTIANOMINAL_C',
          'enabled' => true,
          'default' => true,
        ),
        22 => 
        array (
          'name' => 'sasa_garantiavlrmercado_c',
          'label' => 'LBL_SASA_GARANTIAVLRMERCADO_C',
          'enabled' => true,
          'default' => true,
        ),
        23 => 
        array (
          'name' => 'sasa_valorinicial_c',
          'label' => 'LBL_SASA_VALORINICIAL_C',
          'enabled' => true,
          'default' => true,
        ),
        24 => 
        array (
          'name' => 'sasa_valorfinal_c',
          'label' => 'LBL_SASA_VALORFINAL_C',
          'enabled' => true,
          'default' => true,
        ),
        25 => 
        array (
          'name' => 'sasa_saldototal_c',
          'label' => 'LBL_SASA_SALDOTOTAL_C',
          'enabled' => true,
          'default' => true,
        ),
        26 => 
        array (
          'name' => 'sasa_fecharegistro_c',
          'label' => 'LBL_SASA_FECHAREGISTRO_C',
          'enabled' => true,
          'default' => true,
        ),
        27 => 
        array (
          'name' => 'sasa_fechacumplimiento_c',
          'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        28 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);