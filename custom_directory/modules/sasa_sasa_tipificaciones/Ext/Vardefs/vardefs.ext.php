<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_wf_c.php

 // created: 2022-03-30 12:02:08
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_wf_c']['labelValue']='SFC';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_wf_c']['enforced']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_wf_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_wf_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_detalle_c.php

 // created: 2022-04-06 20:42:57

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sasa_sasa_tipificaciones_cases_1_sasa_sasa_tipificaciones.php

// created: 2022-04-29 13:46:19
$dictionary["sasa_sasa_tipificaciones"]["fields"]["sasa_sasa_tipificaciones_cases_1"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_sasa_tipificaciones_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_name.php

 // created: 2022-10-19 15:53:51
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['len']='255';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['audited']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['massupdate']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['hidemassupdate']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['importable']='false';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['unified_search']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['calculated']='1';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['formula']='concat($sasa_motivo_c," - ",getDropdownValue("sasa_categoria_c_list",$sasa_categoria_c)," - ",getDropdownValue("sasa_motivo_c_list",$sasa_proceso_c)," - ",getDropdownValue("sasa_detalle_c_list",$sasa_detalle_c))';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_motivo_c.php

 // created: 2022-11-18 15:59:51
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_motivo_c']['labelValue']='Motivo';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_motivo_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_motivo_c']['required_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_motivo_c']['readonly_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_motivo_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_ans_c.php

 // created: 2022-12-09 21:10:44
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['labelValue']='ANS';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['enforced']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['required_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_ans_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_instructivo_c.php

 // created: 2022-12-16 15:25:01
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['labelValue']='Instructivo';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['enforced']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['required_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_instructivo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2023-05-17 15:40:54
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['labelValue']='Categoría';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['enforced']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['required_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['readonly_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_categoria_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/sasa_sasa_tipificaciones/Ext/Vardefs/sugarfield_sasa_proceso_c.php

 // created: 2023-05-17 15:44:11
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['labelValue']='Proceso';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['enforced']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['dependency']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['required_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['readonly_formula']='';
$dictionary['sasa_sasa_tipificaciones']['fields']['sasa_proceso_c']['visibility_grid']=array (
);

 
?>
