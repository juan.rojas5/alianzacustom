<?php
 // created: 2023-02-01 19:16:06
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['labelValue']='Nombre de negocio';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['readonly_formula']='';

 ?>