<?php
 // created: 2021-03-09 20:29:01
$dictionary['Lead']['fields']['department']['audited']=true;
$dictionary['Lead']['fields']['department']['massupdate']=false;
$dictionary['Lead']['fields']['department']['comments']='Department the lead belongs to';
$dictionary['Lead']['fields']['department']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['department']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['department']['merge_filter']='disabled';
$dictionary['Lead']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['department']['calculated']=false;

 ?>