<?php
 // created: 2018-06-25 14:02:27
$dictionary['Campaign']['fields']['name']['audited']=false;
$dictionary['Campaign']['fields']['name']['massupdate']=false;
$dictionary['Campaign']['fields']['name']['comments']='The name of the campaign';
$dictionary['Campaign']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Campaign']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Campaign']['fields']['name']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['name']['unified_search']=false;
$dictionary['Campaign']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.39',
  'searchable' => true,
);
$dictionary['Campaign']['fields']['name']['calculated']=false;

 ?>