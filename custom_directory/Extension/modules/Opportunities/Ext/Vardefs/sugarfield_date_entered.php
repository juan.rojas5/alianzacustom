<?php
 // created: 2018-06-20 13:31:29
$dictionary['Opportunity']['fields']['date_entered']['audited']=true;
$dictionary['Opportunity']['fields']['date_entered']['comments']='Date record created';
$dictionary['Opportunity']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_entered']['calculated']=false;
$dictionary['Opportunity']['fields']['date_entered']['enable_range_search']='1';

 ?>