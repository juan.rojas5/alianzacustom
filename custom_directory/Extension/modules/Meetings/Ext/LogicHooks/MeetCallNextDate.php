<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	2,

	//Label. A string value to identify the hook.
	'MeetCallNextDate',

	//The PHP file where your class is located.
	'custom/modules/Meetings/MeetCallNextDate.php',

	//The class the method is in.
	'before_save_class_MeetCallNextDate',

	//The method to call.
	'before_save_method_MeetCallNextDate'
);

?>
