<?php
// created: 2019-01-04 22:24:53
$dictionary["sasa_Categorias"]["fields"]["sasa_categorias_sasas_saldosconsolidados_1"] = array (
  'name' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'sasaS_SaldosConsolidados',
  'bean_name' => 'sasaS_SaldosConsolidados',
  'vname' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASA_CATEGORIAS_TITLE',
  'id_name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'link-type' => 'many',
  'side' => 'left',
);
