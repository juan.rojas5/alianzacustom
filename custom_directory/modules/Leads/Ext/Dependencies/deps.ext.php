<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/fields_setrequired.php


$dependencies['Leads']['fields_setrequired'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('sasa_tipopersona_c','sasa_estadoinfocp_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'email',
                'value' => 'and(equal($sasa_tipopersona_c,"Juridica"),equal($sasa_estadoinfocp_c,"Informacion Completa"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ingresosmensuales_c',
                'value' => 'and(equal($sasa_tipopersona_c,"Juridica"),equal($sasa_estadoinfocp_c,"Informacion Completa"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipoidentificacion_c',
                'value' => 'equal($sasa_tipopersona_c,"Juridica")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => 'equal($sasa_tipopersona_c,"Juridica")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'salutation',
                'value' => 'equal($sasa_tipopersona_c,"Natural")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'first_name',
                'value' => 'not(equal($sasa_tipopersona_c,"Juridica"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'last_name',
                'value' => 'not(equal($sasa_tipopersona_c,"Juridica"))',
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/set_value_nid.php


$dependencies['Leads']['set_value_nid'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'and(equal($sasa_tipopersona_c,"Juridica"),greaterThan(strlen($sasa_nroidentificacion_c),9))',
    'triggerFields' => array('sasa_tipopersona_c','sasa_nroidentificacion_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetValue',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => '',
            )
        )
    )
);




?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/readonly_primary_address_city.php


$dependencies['Leads']['readonly_primary_address_city'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_city'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/readonly_primary_address_country.php


$dependencies['Leads']['readonly_primary_address_country'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_country'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/readonly_primary_address_state.php


$dependencies['Leads']['readonly_primary_address_state'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_state'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_state',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/ReadOnlyConverted.php


$dependencies['Leads']['ReadOnlyConverted'] = array(
    'hooks' => array("all"),
    'trigger' => 'and(equal($converted,true),not(isUserAdmin("")))',
    'triggerFields' => array('sasa_nroidentificacion_c','sasa_tipopersona_c','account_name','sasa_sector_c','sasa_tipoidentificacion_c','sasa_ocupacion_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipopersona_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'birthdate',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'account_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_sector_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipoidentificacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'lead_source',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'refered_by',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'title',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_telefono_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'do_not_call',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_mobile',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'email',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_pais_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_departamentointer_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_municipiointer_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_departamento_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_municipio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'primary_address',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'description',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'business_center_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_actividadeconomica_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ocupacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'department',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ingresosventasanuales_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_segmento_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nombreproducto_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_valorinversion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'campaign_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'lead_source_description',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_home',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_telefono2_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assistant_phone',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_fax',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'facebook',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'twitter',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'googleplus',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'website',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assistant',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assigned_user_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_regional_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'team_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p1rangoedadcalidad_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p2propositoahorroinve_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p3horizontetiempoinve_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p4dondeprovienedinero_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p5porcentajeactivosah_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p6experienciainversio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p7oportunidadrentabil_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p8supongainversionesp_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_perfilriesgo_c',
                'value' => 'true',
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOption.php

$dependencies['Leads']['SetOption'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Natural")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("","Cedula de Ciudadania","Cedula de Extranjeria","Pasaporte","NUIP","Registro Civil","Tarjeta de identidad","Carne Diplomatico","Fideicomiso","Sociedad Extranjera","Otro")',
           'labels' => 'createList("","Cédula de Ciudadanía","Cédula de Extranjería","Pasaporte","NUIP","Registro Civil","Tarjeta de Identidad","Carné Diplomatico","Fideicomiso","Sociedad Extranjera","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_ocupacion_c',
           'keys' => 'createList("","Asalariado","Empleado","Estudiante","Hogar","Independiente","Militar","Pensionado","Religioso","Rentista","Socio","Otros")',
           'labels' => 'createList("","Asalariado","Empleado","Estudiante","Hogar","Independiente","Militar","Pensionado","Religioso","Rentista","Socio","Otros")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_ocupacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOption1.php

$dependencies['Leads']['SetOption1'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Juridica")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("Nit","Otro")',
           'labels' => 'createList("Nit","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_ocupacion_c',
           'keys' => 'createList("","Comercial","Construccion","Financiero","Industrial","Servicios","Transporte","Otros")',
           'labels' => 'createList("","Comercial","Construcción","Financiero","Industrial","Servicios","Transporte","Otros")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_ocupacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOption2.php

$dependencies['Leads']['SetOption2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Fideicomiso")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("Fideicomiso","Otro")',
           'labels' => 'createList("Fideicomiso","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento1.php

$dependencies['Leads']['SetOptionDepartamento1'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Amazonas")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","El Encanto","La Chorrera","La Pedrera","La Victoria","Leticia","Miriti Parana","Puerto Alegria","Puerto Arica","Puerto Narino","Puerto Santander","Tarapaca")',
           'labels' => 'createList("","El Encanto","La Chorrera","La Pedrera","La Victoria","Leticia","Miriti Paraná","Puerto Alegría","Puerto Arica","Puerto Nariño","Puerto Santander","Tarapacá")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento10.php

$dependencies['Leads']['SetOptionDepartamento10'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Caqueta")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Albania","Belen de Los Andaquies","Cartagena del Chaira","Curillo","El Doncello","El Paujil","Florencia","La Montanita","Milan","Morelia","Puerto Rico","San Jose del Fragua","San Vicente del Caguan","Solano","Solita","Valparaiso")',
           'labels' => 'createList("","Albania","Belén de Los Andaquies","Cartagena del Chairá","Curillo","El Doncello","El Paujil","Florencia","La Montañita","Milán","Morelia","Puerto Rico","San José del Fragua","San Vicente del Caguán","Solano","Solita","Valparaíso")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento11.php

$dependencies['Leads']['SetOptionDepartamento11'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Casanare")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Luis de Gaceno","Villanueva","Sabanalarga","Aguazul","Chameza","Hato Corozal","La Salina","Mani","Monterrey","Nunchia","Orocue","Paz de Ariporo","Pore","Recetor","Sacama","Tamara","Tauramena","Trinidad","Yopal")',
           'labels' => 'createList("","San Luis de Gaceno","Villanueva","Sabanalarga","Aguazul","Chámeza","Hato Corozal","La Salina","Maní","Monterrey","Nunchía","Orocué","Paz de Ariporo","Pore","Recetor","Sácama","Támara","Tauramena","Trinidad","Yopal")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento12.php

$dependencies['Leads']['SetOptionDepartamento12'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Florencia","Paez","Morales","Santa Rosa","Almaguer","Argelia","Balboa","Bolivar","Buenos Aires","Cajibio","Caldono","Caloto","Corinto","El Tambo","Guachene","Guapi","Inza","Jambalo","La Sierra","La Vega","Lopez","Mercaderes","Miranda","Padilla","Patia","Piamonte","Piendamo","Popayan","Puerto Tejada","Purace","Rosas","San Sebastian","Santander de Quilichao","Silvia","Sotara","Suarez","Sucre","Timbio","Timbiqui","Toribio","Totoro","Villa Rica")',
           'labels' => 'createList("","Florencia","Páez","Morales","Santa Rosa","Almaguer","Argelia","Balboa","Bolívar","Buenos Aires","Cajibío","Caldono","Caloto","Corinto","El Tambo","Guachené","Guapi","Inzá","Jambaló","La Sierra","La Vega","López","Mercaderes","Miranda","Padilla","Patía","Piamonte","Piendamó","Popayán","Puerto Tejada","Puracé","Rosas","San Sebastián","Santander de Quilichao","Silvia","Sotara","Suárez","Sucre","Timbío","Timbiquí","Toribio","Totoró","Villa Rica")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento23.php

$dependencies['Leads']['SetOptionDepartamento23'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Narino")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Alban","Mosquera","Narino","Ricaurte","San Bernardo","El Tambo","Belen","Cordoba","Aldana","Ancuya","Arboleda","Barbacoas","Buesaco","Chachagüi","Colon","Consaca","Contadero","Cuaspud","Cumbal","Cumbitara","El Charco","El Penol","El Rosario","El Tablon de Gomez","Francisco Pizarro","Funes","Guachucal","Guaitarilla","Gualmatan","Iles","Imues","Ipiales","La Cruz","La Florida","La Llanada","La Tola","La Union","Leiva","Linares","Los Andes","Magüi","Mallama","Olaya Herrera","Ospina","Pasto","Policarpa","Potosi","Providencia","Puerres","Pupiales","Roberto Payan","Samaniego","San Andres de Tumaco","San Lorenzo","San Pablo","San Pedro de Cartago","Sandona","Santa Barbara","Santacruz","Sapuyes","Taminango","Tangua","Tuquerres","Yacuanquer")',
           'labels' => 'createList("","Albán","Mosquera","Nariño","Ricaurte","San Bernardo","El Tambo","Belén","Córdoba","Aldana","Ancuyá","Arboleda","Barbacoas","Buesaco","Chachagüí","Colón","Consaca","Contadero","Cuaspud","Cumbal","Cumbitara","El Charco","El Peñol","El Rosario","El Tablón de Gómez","Francisco Pizarro","Funes","Guachucal","Guaitarilla","Gualmatán","Iles","Imués","Ipiales","La Cruz","La Florida","La Llanada","La Tola","La Unión","Leiva","Linares","Los Andes","Magüí","Mallama","Olaya Herrera","Ospina","Pasto","Policarpa","Potosí","Providencia","Puerres","Pupiales","Roberto Payán","Samaniego","San Andrés de Tumaco","San Lorenzo","San Pablo","San Pedro de Cartago","Sandoná","Santa Bárbara","Santacruz","Sapuyes","Taminango","Tangua","Túquerres","Yacuanquer")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento14.php

$dependencies['Leads']['SetOptionDepartamento14'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Choco")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Riosucio","Acandi","Alto Baudo","Atrato","Bagado","Bahia Solano","Bajo Baudo","Belen de Bajira","Bojaya","Carmen del Darien","Certegui","Condoto","El Canton del San Pablo","El Carmen de Atrato","El Litoral del San Juan","Istmina","Jurado","Lloro","Medio Atrato","Medio Baudo","Medio San Juan","Novita","Nuqui","Quibdo","Rio Iro","Rio Quito","San Jose del Palmar","Sipi","Tado","Unguia","Union Panamericana")',
           'labels' => 'createList("","Riosucio","Acandí","Alto Baudo","Atrato","Bagadó","Bahía Solano","Bajo Baudó","Belén de Bajira","Bojaya","Carmen del Darien","Cértegui","Condoto","El Cantón del San Pablo","El Carmen de Atrato","El Litoral del San Juan","Istmina","Juradó","Lloró","Medio Atrato","Medio Baudó","Medio San Juan","Nóvita","Nuquí","Quibdó","Río Iro","Río Quito","San José del Palmar","Sipí","Tadó","Unguía","Unión Panamericana")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento13.php

$dependencies['Leads']['SetOptionDepartamento13'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cesar")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Aguachica","Agustin Codazzi","Astrea","Becerril","Bosconia","Chimichagua","Chiriguana","Curumani","El Copey","El Paso","Gamarra","Gonzalez","La Gloria","La Jagua de Ibirico","La Paz","Manaure","Pailitas","Pelaya","Pueblo Bello","Rio de Oro","San Alberto","San Diego","San Martin","Tamalameque","Valledupar")',
           'labels' => 'createList("","Aguachica","Agustín Codazzi","Astrea","Becerril","Bosconia","Chimichagua","Chiriguaná","Curumaní","El Copey","El Paso","Gamarra","González","La Gloria","La Jagua de Ibirico","La Paz","Manaure","Pailitas","Pelaya","Pueblo Bello","Río de Oro","San Alberto","San Diego","San Martín","Tamalameque","Valledupar")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento16.php

$dependencies['Leads']['SetOptionDepartamento16'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cundinamarca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","La Vega","El Penon","Agua de Dios","Alban","Anapoima","Anolaima","Apulo","Arbelaez","Beltran","Bituima","Bojaca","Cabrera","Cachipay","Cajica","Caparrapi","Caqueza","Carmen de Carupa","Chaguani","Chia","Chipaque","Choachi","Choconta","Cogua","Cota","Cucunuba","El Colegio","El Rosal","Facatativa","Fomeque","Fosca","Funza","Fuquene","Fusagasuga","Gachala","Gachancipa","Gacheta","Gama","Girardot","Granada","Guacheta","Guaduas","Guasca","Guataqui","Guatavita","Guayabal de Siquima","Guayabetal","Gutierrez","Jerusalen","Junin","La Calera","La Mesa","La Palma","La Pena","Lenguazaque","Macheta","Madrid","Manta","Medina","Mosquera","Narino","Nemocon","Nilo","Nimaima","Nocaima","Pacho","Paime","Pandi","Paratebueno","Pasca","Puerto Salgar","Puli","Quebradanegra","Quetame","Quipile","Ricaurte","San Antonio del Tequendama","San Bernardo","San Cayetano","San Francisco","San Juan de Rio Seco","Sasaima","Sesquile","Sibate","Silvania","Simijaca","Soacha","Sopo","Subachoque","Suesca","Supata","Susa","Sutatausa","Tabio","Tausa","Tena","Tenjo","Tibacuy","Tibirita","Tocaima","Tocancipa","Topaipi","Ubala","Ubaque","Une","utica","Venecia","Vergara","Viani","Villa de San Diego de Ubate","Villagomez","Villapinzon","Villeta","Viota","Yacopi","Zipacon","Zipaquira")',
           'labels' => 'createList("","La Vega","El Peñón","Agua de Dios","Albán","Anapoima","Anolaima","Apulo","Arbeláez","Beltrán","Bituima","Bojacá","Cabrera","Cachipay","Cajicá","Caparrapí","Caqueza","Carmen de Carupa","Chaguaní","Chía","Chipaque","Choachí","Chocontá","Cogua","Cota","Cucunubá","El Colegio","El Rosal","Facatativá","Fomeque","Fosca","Funza","Fúquene","Fusagasugá","Gachala","Gachancipá","Gachetá","Gama","Girardot","Granada","Guachetá","Guaduas","Guasca","Guataquí","Guatavita","Guayabal de Siquima","Guayabetal","Gutiérrez","Jerusalén","Junín","La Calera","La Mesa","La Palma","La Peña","Lenguazaque","Macheta","Madrid","Manta","Medina","Mosquera","Nariño","Nemocón","Nilo","Nimaima","Nocaima","Pacho","Paime","Pandi","Paratebueno","Pasca","Puerto Salgar","Pulí","Quebradanegra","Quetame","Quipile","Ricaurte","San Antonio del Tequendama","San Bernardo","San Cayetano","San Francisco","San Juan de Río Seco","Sasaima","Sesquilé","Sibaté","Silvania","Simijaca","Soacha","Sopó","Subachoque","Suesca","Supatá","Susa","Sutatausa","Tabio","Tausa","Tena","Tenjo","Tibacuy","Tibirita","Tocaima","Tocancipá","Topaipí","Ubalá","Ubaque","Une","Útica","Venecia","Vergara","Vianí","Villa de San Diego de Ubate","Villagómez","Villapinzón","Villeta","Viotá","Yacopí","Zipacón","Zipaquirá")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento17.php

$dependencies['Leads']['SetOptionDepartamento17'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Guainia")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Puerto Colombia","Barranco Minas","Cacahual","Inirida","La Guadalupe","Mapiripana","Morichal","Pana Pana","San Felipe")',
           'labels' => 'createList("","Puerto Colombia","Barranco Minas","Cacahual","Inírida","La Guadalupe","Mapiripana","Morichal","Pana Pana","San Felipe")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento18.php

$dependencies['Leads']['SetOptionDepartamento18'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Guaviare")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Miraflores","Calamar","El Retorno","San Jose del Guaviare")',
           'labels' => 'createList("","Miraflores","Calamar","El Retorno","San José del Guaviare")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento19.php

$dependencies['Leads']['SetOptionDepartamento19'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Huila")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Palestina","Santa Maria","Acevedo","Agrado","Aipe","Algeciras","Altamira","Baraya","Campoalegre","Colombia","Elias","Garzon","Gigante","Guadalupe","Hobo","Iquira","Isnos","La Argentina","La Plata","Nataga","Neiva","Oporapa","Paicol","Palermo","Pital","Pitalito","Rivera","Saladoblanco","San Agustin","Suaza","Tarqui","Tello","Teruel","Tesalia","Timana","Villavieja","Yaguara")',
           'labels' => 'createList("","Palestina","Santa María","Acevedo","Agrado","Aipe","Algeciras","Altamira","Baraya","Campoalegre","Colombia","Elías","Garzón","Gigante","Guadalupe","Hobo","Iquira","Isnos","La Argentina","La Plata","Nátaga","Neiva","Oporapa","Paicol","Palermo","Pital","Pitalito","Rivera","Saladoblanco","San Agustín","Suaza","Tarqui","Tello","Teruel","Tesalia","Timaná","Villavieja","Yaguará")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento2.php

$dependencies['Leads']['SetOptionDepartamento2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Antioquia")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Abejorral","Abriaqui","Alejandria","Amaga","Amalfi","Andes","Angelopolis","Angostura","Anori","Anza","Apartado","Arboletes","Argelia","Armenia","Barbosa","Bello","Belmira","Betania","Betulia","Briceno","Buritica","Caceres","Caicedo","Caldas","Campamento","Canasgordas","Caracoli","Caramanta","Carepa","Carolina","Caucasia","Chigorodo","Cisneros","Ciudad Bolivar","Cocorna","Concepcion","Concordia","Copacabana","Dabeiba","Don Matias","Ebejico","El Bagre","El Carmen de Viboral","El Santuario","Entrerrios","Envigado","Fredonia","Frontino","Giraldo","Girardota","Gomez Plata","Granada","Guadalupe","Guarne","Guatape","Heliconia","Hispania","Itagui","Ituango","Jardin","Jerico","La Ceja","La Estrella","La Pintada","La Union","Liborina","Maceo","Marinilla","Medellin","Montebello","Murindo","Mutata","Narino","Nechi","Necocli","Olaya","Penol","Peque","Pueblorrico","Puerto Berrio","Puerto Nare","Puerto Triunfo","Remedios","Retiro","Rionegro","Sabanalarga","Sabaneta","Salgar","San Andres de Cuerquia","San Carlos","San Francisco","San Jeronimo","San Jose de La Montana","San Juan de Uraba","San Luis","San Pedro","San Pedro de Uraba","San Rafael","San Roque","San Vicente","Santa Barbara","Santa Rosa de Osos","Santafe de Antioquia","Santo Domingo","Segovia","Sonson","Sopetran","Tamesis","Taraza","Tarso","Titiribi","Toledo","Turbo","Uramita","Urrao","Valdivia","Valparaiso","Vegachi","Venecia","Vigia del Fuerte","Yali","Yarumal","Yolombo","Yondo","Zaragoza")',
           'labels' => 'createList("","Abejorral","Abriaquí","Alejandría","Amagá","Amalfi","Andes","Angelópolis","Angostura","Anorí","Anza","Apartadó","Arboletes","Argelia","Armenia","Barbosa","Bello","Belmira","Betania","Betulia","Briceño","Buriticá","Cáceres","Caicedo","Caldas","Campamento","Cañasgordas","Caracolí","Caramanta","Carepa","Carolina","Caucasia","Chigorodó","Cisneros","Ciudad Bolívar","Cocorná","Concepción","Concordia","Copacabana","Dabeiba","Don Matías","Ebéjico","El Bagre","El Carmen de Viboral","El Santuario","Entrerrios","Envigado","Fredonia","Frontino","Giraldo","Girardota","Gómez Plata","Granada","Guadalupe","Guarne","Guatapé","Heliconia","Hispania","Itagui","Ituango","Jardín","Jericó","La Ceja","La Estrella","La Pintada","La Unión","Liborina","Maceo","Marinilla","Medellín","Montebello","Murindó","Mutatá","Nariño","Nechí","Necoclí","Olaya","Peñol","Peque","Pueblorrico","Puerto Berrío","Puerto Nare","Puerto Triunfo","Remedios","Retiro","Rionegro","Sabanalarga","Sabaneta","Salgar","San Andrés de Cuerquía","San Carlos","San Francisco","San Jerónimo","San José de La Montaña","San Juan de Urabá","San Luis","San Pedro","San Pedro de Uraba","San Rafael","San Roque","San Vicente","Santa Bárbara","Santa Rosa de Osos","Santafé de Antioquia","Santo Domingo","Segovia","Sonsón","Sopetrán","Támesis","Tarazá","Tarso","Titiribí","Toledo","Turbo","Uramita","Urrao","Valdivia","Valparaíso","Vegachí","Venecia","Vigía del Fuerte","Yalí","Yarumal","Yolombó","Yondó","Zaragoza")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento20.php

$dependencies['Leads']['SetOptionDepartamento20'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"La Guajira")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Manaure","Villanueva","Albania","Barrancas","Dibula","Distraccion","El Molino","Fonseca","Hatonuevo","La Jagua del Pilar","Maicao","Riohacha","San Juan del Cesar","Uribia","Urumita")',
           'labels' => 'createList("","Manaure","Villanueva","Albania","Barrancas","Dibula","Distracción","El Molino","Fonseca","Hatonuevo","La Jagua del Pilar","Maicao","Riohacha","San Juan del Cesar","Uribia","Urumita")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento21.php

$dependencies['Leads']['SetOptionDepartamento21'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Magdalena")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Salamina","Algarrobo","Aracataca","Ariguani","Cerro San Antonio","Chivolo","Cienaga","Concordia","El Banco","El Pinon","El Reten","Fundacion","Guamal","Nueva Granada","Pedraza","Pijino del Carmen","Pivijay","Plato","Pueblo Viejo","Remolino","Sabanas de San Angel","San Sebastian de Buenavista","San Zenon","Santa Ana","Santa Barbara de Pinto","Santa Marta","Sitionuevo","Tenerife","Zapayan","Zona Bananera")',
           'labels' => 'createList("","Salamina","Algarrobo","Aracataca","Ariguaní","Cerro San Antonio","Chivolo","Ciénaga","Concordia","El Banco","El Piñon","El Retén","Fundación","Guamal","Nueva Granada","Pedraza","Pijiño del Carmen","Pivijay","Plato","Pueblo Viejo","Remolino","Sabanas de San Angel","San Sebastián de Buenavista","San Zenón","Santa Ana","Santa Bárbara de Pinto","Santa Marta","Sitionuevo","Tenerife","Zapayán","Zona Bananera")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento15.php

$dependencies['Leads']['SetOptionDepartamento15'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cordoba")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Ayapel","Buenavista","Canalete","Cerete","Chima","Chinu","Cienaga de Oro","Cotorra","La Apartada","Lorica","Los Cordobas","Momil","Montelibano","Monteria","Monitos","Planeta Rica","Pueblo Nuevo","Puerto Escondido","Puerto Libertador","Purisima","Sahagun","San Andres Sotavento","San Antero","San Bernardo del Viento","San Carlos","San Jose de Ure","San Pelayo","Tierralta","Tuchin","Valencia")',
           'labels' => 'createList("","Ayapel","Buenavista","Canalete","Cereté","Chimá","Chinú","Ciénaga de Oro","Cotorra","La Apartada","Lorica","Los Córdobas","Momil","Montelíbano","Montería","Moñitos","Planeta Rica","Pueblo Nuevo","Puerto Escondido","Puerto Libertador","Purísima","Sahagún","San Andrés Sotavento","San Antero","San Bernardo del Viento","San Carlos","San José de Uré","San Pelayo","Tierralta","Tuchín","Valencia")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento4.php

$dependencies['Leads']['SetOptionDepartamento4'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Archipielago de San Andres Providencia y Santa Catalina")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Providencia","San Andres")',
           'labels' => 'createList("","Providencia","San Andrés")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento24.php

$dependencies['Leads']['SetOptionDepartamento24'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Norte de Santander")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Cayetano","Puerto Santander","Abrego","Arboledas","Bochalema","Bucarasica","Cachira","Cacota","Chinacota","Chitaga","Convencion","Cucuta","Cucutilla","Durania","El Carmen","El Tarra","El Zulia","Gramalote","Hacari","Herran","La Esperanza","La Playa","Labateca","Los Patios","Lourdes","Mutiscua","Ocana","Pamplona","Pamplonita","Ragonvalia","Salazar","San Calixto","Santiago","Sardinata","Silos","Teorama","Tibu","Toledo","Villa Caro","Villa del Rosario")',
           'labels' => 'createList("","San Cayetano","Puerto Santander","Abrego","Arboledas","Bochalema","Bucarasica","Cachirá","Cácota","Chinácota","Chitagá","Convención","Cúcuta","Cucutilla","Durania","El Carmen","El Tarra","El Zulia","Gramalote","Hacarí","Herrán","La Esperanza","La Playa","Labateca","Los Patios","Lourdes","Mutiscua","Ocaña","Pamplona","Pamplonita","Ragonvalia","Salazar","San Calixto","Santiago","Sardinata","Silos","Teorama","Tibú","Toledo","Villa Caro","Villa del Rosario")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento34.php

$dependencies['Leads']['SetOptionDepartamento34'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"No Registra")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("No Registra")',
           'labels' => 'createList("No Registra")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento26.php

$dependencies['Leads']['SetOptionDepartamento26'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Quindio")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Cordoba","Buenavista","Armenia","Calarca","Circasia","Filandia","Genova","La Tebaida","Montenegro","Pijao","Quimbaya","Salento")',
           'labels' => 'createList("","Córdoba","Buenavista","Armenia","Calarcá","Circasia","Filandia","Génova","La Tebaida","Montenegro","Pijao","Quimbaya","Salento")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento27.php

$dependencies['Leads']['SetOptionDepartamento27'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Risaralda")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Balboa","Apia","Belen de Umbria","Dosquebradas","Guatica","La Celia","La Virginia","Marsella","Mistrato","Pereira","Pueblo Rico","Quinchia","Santa Rosa de Cabal","Santuario")',
           'labels' => 'createList("","Balboa","Apía","Belén de Umbría","Dosquebradas","Guática","La Celia","La Virginia","Marsella","Mistrató","Pereira","Pueblo Rico","Quinchía","Santa Rosa de Cabal","Santuario")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento28.php

$dependencies['Leads']['SetOptionDepartamento28'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Santander")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Miguel","Santa Barbara","Villanueva","Albania","Guadalupe","El Penon","Cabrera","Chima","La Paz","Bolivar","Sucre","Aguada","Aratoca","Barbosa","Barichara","Barrancabermeja","Betulia","Bucaramanga","California","Capitanejo","Carcasi","Cepita","Cerrito","Charala","Charta","Chipata","Cimitarra","Concepcion","Confines","Contratacion","Coromoro","Curiti","El Carmen de Chucuri","El Guacamayo","El Playon","Encino","Enciso","Florian","Floridablanca","Galan","Gambita","Giron","Guaca","Guapota","Guavata","Güepsa","Hato","Jesus Maria","Jordan","La Belleza","Landazuri","Lebrija","Los Santos","Macaravita","Malaga","Matanza","Mogotes","Molagavita","Ocamonte","Oiba","Onzaga","Palmar","Palmas del Socorro","Paramo","Piedecuesta","Pinchote","Puente Nacional","Puerto Parra","Puerto Wilches","Rionegro","Sabana de Torres","San Andres","San Benito","San Gil","San Joaquin","San Jose de Miranda","San Vicente de Chucuri","Santa Helena del Opon","Simacota","Socorro","Suaita","Surata","Tona","Valle de San Jose","Velez","Vetas","Zapatoca")',
           'labels' => 'createList("","San Miguel","Santa Bárbara","Villanueva","Albania","Guadalupe","El Peñón","Cabrera","Chimá","La Paz","Bolívar","Sucre","Aguada","Aratoca","Barbosa","Barichara","Barrancabermeja","Betulia","Bucaramanga","California","Capitanejo","Carcasí","Cepitá","Cerrito","Charalá","Charta","Chipatá","Cimitarra","Concepción","Confines","Contratación","Coromoro","Curití","El Carmen de Chucurí","El Guacamayo","El Playón","Encino","Enciso","Florián","Floridablanca","Galán","Gambita","Girón","Guaca","Guapotá","Guavatá","Güepsa","Hato","Jesús María","Jordán","La Belleza","Landázuri","Lebríja","Los Santos","Macaravita","Málaga","Matanza","Mogotes","Molagavita","Ocamonte","Oiba","Onzaga","Palmar","Palmas del Socorro","Páramo","Piedecuesta","Pinchote","Puente Nacional","Puerto Parra","Puerto Wilches","Rionegro","Sabana de Torres","San Andrés","San Benito","San Gil","San Joaquín","San José de Miranda","San Vicente de Chucurí","Santa Helena del Opón","Simacota","Socorro","Suaita","Suratá","Tona","Valle de San José","Vélez","Vetas","Zapatoca")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento29.php

$dependencies['Leads']['SetOptionDepartamento29'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Sucre")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Sucre","Buenavista","La Union","Caimito","Chalan","Coloso","Corozal","Covenas","El Roble","Galeras","Guaranda","Los Palmitos","Majagual","Morroa","Ovejas","Palmito","Sampues","San Benito Abad","San Juan de Betulia","San Luis de Since","San Marcos","San Onofre","San Pedro","Santiago de Tolu","Sincelejo","Tolu Viejo")',
           'labels' => 'createList("","Sucre","Buenavista","La Unión","Caimito","Chalán","Coloso","Corozal","Coveñas","El Roble","Galeras","Guaranda","Los Palmitos","Majagual","Morroa","Ovejas","Palmito","Sampués","San Benito Abad","San Juan de Betulia","San Luis de Sincé","San Marcos","San Onofre","San Pedro","Santiago de Tolú","Sincelejo","Tolú Viejo")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento3.php

$dependencies['Leads']['SetOptionDepartamento3'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Arauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Arauca","Arauquita","Cravo Norte","Fortul","Puerto Rondon","Saravena","Tame")',
           'labels' => 'createList("","Arauca","Arauquita","Cravo Norte","Fortul","Puerto Rondón","Saravena","Tame")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento30.php

$dependencies['Leads']['SetOptionDepartamento30'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Tolima")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Suarez","Alpujarra","Alvarado","Ambalema","Anzoategui","Armero","Ataco","Cajamarca","Carmen de Apicala","Casabianca","Chaparral","Coello","Coyaima","Cunday","Dolores","Espinal","Falan","Flandes","Fresno","Guamo","Herveo","Honda","Ibague","Icononzo","Lerida","Libano","Mariquita","Melgar","Murillo","Natagaima","Ortega","Palocabildo","Piedras","Planadas","Prado","Purificacion","Rio Blanco","Roncesvalles","Rovira","Saldana","San Antonio","San Luis","Santa Isabel","Valle de San Juan","Venadillo","Villahermosa","Villarrica")',
           'labels' => 'createList("","Suárez","Alpujarra","Alvarado","Ambalema","Anzoátegui","Armero","Ataco","Cajamarca","Carmen de Apicala","Casabianca","Chaparral","Coello","Coyaima","Cunday","Dolores","Espinal","Falan","Flandes","Fresno","Guamo","Herveo","Honda","Ibagué","Icononzo","Lérida","Líbano","Mariquita","Melgar","Murillo","Natagaima","Ortega","Palocabildo","Piedras","Planadas","Prado","Purificación","Rio Blanco","Roncesvalles","Rovira","Saldaña","San Antonio","San Luis","Santa Isabel","Valle de San Juan","Venadillo","Villahermosa","Villarrica")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento31.php

$dependencies['Leads']['SetOptionDepartamento31'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Valle del Cauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","La Union","San Pedro","Bolivar","Restrepo","Argelia","La Victoria","Candelaria","Alcala","Andalucia","Ansermanuevo","Buenaventura","Bugalagrande","Caicedonia","Cali","Calima","Cartago","Dagua","El aguila","El Cairo","El Cerrito","El Dovio","Florida","Ginebra","Guacari","Guadalajara de Buga","Jamundi","La Cumbre","Obando","Palmira","Pradera","Riofrio","Roldanillo","Sevilla","Toro","Trujillo","Tulua","Ulloa","Versalles","Vijes","Yotoco","Yumbo","Zarzal")',
           'labels' => 'createList("","La Unión","San Pedro","Bolívar","Restrepo","Argelia","La Victoria","Candelaria","Alcalá","Andalucía","Ansermanuevo","Buenaventura","Bugalagrande","Caicedonia","Cali","Calima","Cartago","Dagua","El Águila","El Cairo","El Cerrito","El Dovio","Florida","Ginebra","Guacarí","Guadalajara de Buga","Jamundí","La Cumbre","Obando","Palmira","Pradera","Riofrío","Roldanillo","Sevilla","Toro","Trujillo","Tuluá","Ulloa","Versalles","Vijes","Yotoco","Yumbo","Zarzal")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento32.php

$dependencies['Leads']['SetOptionDepartamento32'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Vaupes")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Caruru","Mitu","Pacoa","Papunaua","Taraira","Yavarate")',
           'labels' => 'createList("","Caruru","Mitú","Pacoa","Papunaua","Taraira","Yavaraté")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento33.php

$dependencies['Leads']['SetOptionDepartamento33'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Vichada")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Cumaribo","La Primavera","Puerto Carreno","Santa Rosalia")',
           'labels' => 'createList("","Cumaribo","La Primavera","Puerto Carreño","Santa Rosalía")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento22.php

$dependencies['Leads']['SetOptionDepartamento22'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Meta")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Guamal","Granada","San Martin","Puerto Rico","Acacias","Barranca de Upia","Cabuyaro","Castilla la Nueva","Cubarral","Cumaral","El Calvario","El Castillo","El Dorado","Fuente de Oro","La Macarena","Lejanias","Mapiripan","Mesetas","Puerto Concordia","Puerto Gaitan","Puerto Lleras","Puerto Lopez","Restrepo","San Carlos de Guaroa","San Juan de Arama","San Juanito","Uribe","Villavicencio","Vista Hermosa")',
           'labels' => 'createList("","Guamal","Granada","San Martín","Puerto Rico","Acacias","Barranca de Upía","Cabuyaro","Castilla la Nueva","Cubarral","Cumaral","El Calvario","El Castillo","El Dorado","Fuente de Oro","La Macarena","Lejanías","Mapiripán","Mesetas","Puerto Concordia","Puerto Gaitán","Puerto Lleras","Puerto López","Restrepo","San Carlos de Guaroa","San Juan de Arama","San Juanito","Uribe","Villavicencio","Vista Hermosa")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento25.php

$dependencies['Leads']['SetOptionDepartamento25'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Putumayo")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Santiago","Colon","San Francisco","Leguizamo","Mocoa","Orito","Puerto Asis","Puerto Caicedo","Puerto Guzman","San Miguel","Sibundoy","Valle de Guamez","Villagarzon")',
           'labels' => 'createList("","Santiago","Colón","San Francisco","Leguízamo","Mocoa","Orito","Puerto Asís","Puerto Caicedo","Puerto Guzmán","San Miguel","Sibundoy","Valle de Guamez","Villagarzón")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento5.php

$dependencies['Leads']['SetOptionDepartamento5'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Atlantico")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Baranoa","Barranquilla","Campo de La Cruz","Candelaria","Galapa","Juan de Acosta","Luruaco","Malambo","Manati","Palmar de Varela","Piojo","Polonuevo","Ponedera","Puerto Colombia","Repelon","Sabanagrande","Sabanalarga","Santa Lucia","Santo Tomas","Soledad","Suan","Tubara","Usiacuri")',
           'labels' => 'createList("","Baranoa","Barranquilla","Campo de La Cruz","Candelaria","Galapa","Juan de Acosta","Luruaco","Malambo","Manatí","Palmar de Varela","Piojó","Polonuevo","Ponedera","Puerto Colombia","Repelón","Sabanagrande","Sabanalarga","Santa Lucía","Santo Tomás","Soledad","Suan","Tubará","Usiacurí")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento6.php

$dependencies['Leads']['SetOptionDepartamento6'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Bogota D.C.")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Bogota D.C.")',
           'labels' => 'createList("","Bogotá D.C.")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento7.php

$dependencies['Leads']['SetOptionDepartamento7'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Bolivar")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Achi","Altos del Rosario","Arenal","Arjona","Arroyohondo","Barranco de Loba","Calamar","Cantagallo","Cartagena","Cicuco","Clemencia","Cordoba","El Carmen de Bolivar","El Guamo","El Penon","Hatillo de Loba","Magangue","Mahates","Margarita","Maria la Baja","Mompos","Montecristo","Morales","Norosi","Pinillos","Regidor","Rio Viejo","San Cristobal","San Estanislao","San Fernando","San Jacinto","San Jacinto del Cauca","San Juan Nepomuceno","San Martin de Loba","San Pablo de Borbur","Santa Catalina","Santa Rosa","Santa Rosa del Sur","Simiti","Soplaviento","Talaigua Nuevo","Tiquisio","Turbaco","Turbana","Villanueva","Zambrano")',
           'labels' => 'createList("","Achí","Altos del Rosario","Arenal","Arjona","Arroyohondo","Barranco de Loba","Calamar","Cantagallo","Cartagena","Cicuco","Clemencia","Córdoba","El Carmen de Bolívar","El Guamo","El Peñón","Hatillo de Loba","Magangué","Mahates","Margarita","María la Baja","Mompós","Montecristo","Morales","Norosí","Pinillos","Regidor","Río Viejo","San Cristóbal","San Estanislao","San Fernando","San Jacinto","San Jacinto del Cauca","San Juan Nepomuceno","San Martín de Loba","San Pablo de Borbur","Santa Catalina","Santa Rosa","Santa Rosa del Sur","Simití","Soplaviento","Talaigua Nuevo","Tiquisio","Turbaco","Turbaná","Villanueva","Zambrano")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento8.php

$dependencies['Leads']['SetOptionDepartamento8'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Boyaca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Pablo de Borbur","La Victoria","Almeida","Aquitania","Arcabuco","Belen","Berbeo","Beteitiva","Boavita","Boyaca","Briceno","Buena Vista","Busbanza","Caldas","Campohermoso","Cerinza","Chinavita","Chiquinquira","Chiquiza","Chiscas","Chita","Chitaraque","Chivata","Chivor","Cienega","Combita","Coper","Corrales","Covarachia","Cubara","Cucaita","Cuitiva","Duitama","El Cocuy","El Espino","Firavitoba","Floresta","Gachantiva","Gameza","Garagoa","Guacamayas","Guateque","Guayata","Güican","Iza","Jenesano","Jerico","La Capilla","La Uvita","Labranzagrande","Macanal","Maripi","Miraflores","Mongua","Mongui","Moniquira","Motavita","Muzo","Nobsa","Nuevo Colon","Oicata","Otanche","Pachavita","Paez","Paipa","Pajarito","Panqueba","Pauna","Paya","Paz de Rio","Pesca","Pisba","Puerto Boyaca","Quipama","Ramiriqui","Raquira","Rondon","Saboya","Sachica","Samaca","San Eduardo","San Jose de Pare","San Luis de Gaceno","San Mateo","San Miguel de Sema","Socha","Santa Maria","Santa Rosa de Viterbo","Santa Sofia","Santana","Sativanorte","Sativasur","Siachoque","Soata","Socota","Sogamoso","Somondoco","Sora","Soraca","Sotaquira","Susacon","Sutamarchan","Sutatenza","Tasco","Tenza","Tibana","Tibasosa","Tinjaca","Tipacoque","Toca","Togüi","Topaga","Tota","Tunja","Tunungua","Turmeque","Tuta","Tutaza","Umbita","Ventaquemada","Villa de Leyva","Viracacha","Zetaquira")',
           'labels' => 'createList("","San Pablo de Borbur","La Victoria","Almeida","Aquitania","Arcabuco","Belén","Berbeo","Betéitiva","Boavita","Boyacá","Briceño","Buena Vista","Busbanzá","Caldas","Campohermoso","Cerinza","Chinavita","Chiquinquirá","Chíquiza","Chiscas","Chita","Chitaraque","Chivatá","Chivor","Ciénega","Cómbita","Coper","Corrales","Covarachía","Cubará","Cucaita","Cuítiva","Duitama","El Cocuy","El Espino","Firavitoba","Floresta","Gachantivá","Gameza","Garagoa","Guacamayas","Guateque","Guayatá","Güicán","Iza","Jenesano","Jericó","La Capilla","La Uvita","Labranzagrande","Macanal","Maripí","Miraflores","Mongua","Monguí","Moniquirá","Motavita","Muzo","Nobsa","Nuevo Colón","Oicatá","Otanche","Pachavita","Páez","Paipa","Pajarito","Panqueba","Pauna","Paya","Paz de Río","Pesca","Pisba","Puerto Boyacá","Quípama","Ramiriquí","Ráquira","Rondón","Saboyá","Sáchica","Samacá","San Eduardo","San José de Pare","San Luis de Gaceno","San Mateo","San Miguel de Sema","Socha","Santa María","Santa Rosa de Viterbo","Santa Sofía","Santana","Sativanorte","Sativasur","Siachoque","Soatá","Socotá","Sogamoso","Somondoco","Sora","Soracá","Sotaquirá","Susacón","Sutamarchán","Sutatenza","Tasco","Tenza","Tibaná","Tibasosa","Tinjacá","Tipacoque","Toca","Togüí","Tópaga","Tota","Tunja","Tununguá","Turmequé","Tuta","Tutazá","Umbita","Ventaquemada","Villa de Leyva","Viracachá","Zetaquira")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionDepartamento9.php

$dependencies['Leads']['SetOptionDepartamento9'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Caldas")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Aguadas","Anserma","Aranzazu","Belalcazar","Chinchina","Filadelfia","La Dorada","La Merced","Manizales","Manzanares","Marmato","Marquetalia","Marulanda","Neira","Norcasia","Pacora","Palestina","Pensilvania","Riosucio","Risaralda","Salamina","Samana","San Jose","Supia","Victoria","Villamaria","Viterbo")',
           'labels' => 'createList("","Aguadas","Anserma","Aranzazu","Belalcázar","Chinchiná","Filadelfia","La Dorada","La Merced","Manizales","Manzanares","Marmato","Marquetalia","Marulanda","Neira","Norcasia","Pácora","Palestina","Pensilvania","Riosucio","Risaralda","Salamina","Samaná","San José","Supía","Victoria","Villamaría","Viterbo")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionPais.php

$dependencies['Leads']['SetOptionPais'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_pais_c,"Colombia")',
   'triggerFields' => array('sasa_pais_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_departamento_c',
           'keys' => 'createList("","Amazonas","Antioquia","Arauca","Archipielago de San Andres Providencia y Santa Catalina","Atlantico","Bogota D.C.","Bolivar","Boyaca","Caldas","Caqueta","Casanare","Cauca","Cesar","Choco","Cundinamarca","Cordoba","Guainia","Guaviare","Huila","La Guajira","Magdalena","Meta","Narino","Norte de Santander","Putumayo","Quindio","Risaralda","Santander","Sucre","Tolima","Valle del Cauca","Vaupes","Vichada")',
           'labels' => 'createList("","Amazonas","Antioquia","Arauca","Archipiélago de San Andrés, Providencia y Santa Catalina","Atlántico","Bogotá D.C.","Bolívar","Boyacá","Caldas","Caquetá","Casanare","Cauca","Cesar","Chocó","Cundinamarca","Córdoba","Guainía","Guaviare","Huila","La Guajira","Magdalena","Meta","Nariño","Norte de Santander","Putumayo","Quindío","Risaralda","Santander","Sucre","Tolima","Valle del Cauca","Vaupés","Vichada")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_departamento_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetOptionPais2.php

$dependencies['Leads']['SetOptionPais2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_pais_c,"No Registra")',
   'triggerFields' => array('sasa_pais_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_departamento_c',
           'keys' => 'createList("No Registra")',
           'labels' => 'createList("No Registra")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_departamento_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/SetVisibilityLeads.php


$dependencies['Leads']['SetVisibilityLeads'] = array(
        'hooks' => array("all"),
        'triggerFields' => array('sasa_sector_c','sasa_tipopersona_c','account_name','sasa_pais_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_sector_c',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_sector_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'account_name',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'account_name',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_actividadeconomica_c',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_actividadeconomica_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'department',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'department',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'birthdate',
                    'value' => 'equal($sasa_tipopersona_c,"Natural")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'birthdate',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'title',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'title',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'refered_by',
                    'value' => 'equal($lead_source,"Word of mouth")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'refered_by',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_departamento_c',
                    'value' => 'or(equal($sasa_pais_c,"Colombia"),equal($sasa_pais_c,"No Registra"))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_departamento_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_departamentointer_c',
                    'value' => 'and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_departamentointer_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_municipiointer_c',
                    'value' => 'and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_municipiointer_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_municipio_c',
                    'value' => 'not($equal($sasa_departamento_c,""))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_municipio_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'website',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'website',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
        ),
    );
?>
