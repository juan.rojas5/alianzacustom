<?php
 // created: 2018-05-23 16:05:07

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'Nuevo',
  'Assigned' => 'Asignado',
  'In Process' => 'En proceso',
  'Converted' => 'Convertido',
  'Recycled' => 'Reciclado',
  'Dead' => 'Libre',
);