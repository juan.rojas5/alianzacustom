<?php
// created: 2022-10-18 19:53:20
$dictionary["Case"]["fields"]["cases_contacts_1"] = array (
  'name' => 'cases_contacts_1',
  'type' => 'link',
  'relationship' => 'cases_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CASES_CONTACTS_1_FROM_CASES_TITLE',
  'id_name' => 'cases_contacts_1cases_ida',
  'link-type' => 'many',
  'side' => 'left',
);
