<?php
 // created: 2018-09-19 22:32:13

$app_list_strings['sasa_estadocivil_c_list']=array (
  '' => '',
  'Casado' => 'Casado',
  'Divorsiado' => 'Divorsiado',
  'Separado' => 'Separado',
  'Soltero' => 'Soltero',
  'Union libre' => 'Unión libre',
  'Viudo' => 'Viudo',
  'Otro' => 'Otro',
);