<?php
 // created: 2018-07-26 02:03:30
$dictionary['Call']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Call']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Call']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Call']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Call']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Call']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Call']['fields']['sasa_superior1_c']['dependency']='';

 ?>