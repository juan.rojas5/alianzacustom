<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class PAIEndpointsApi extends SugarApi
{
	public function registerApiRest()
	{
		return array(
			//GET
			'PAIGetEndpoint' => array(
				//request type
				'reqType' => array('POST'),

				//set authentication
				'noLoginRequired' => true,

				//endpoint path
				'path' => array('PAIEndpoint'),

				//endpoint variables
				'pathVars' => array(''),

				//method to call
				'method' => 'PAISetMethod',

				//short help string to be displayed in the help documentation
				'shortHelp' => 'An example of a GET endpoint',

				//long help to be displayed in the help documentation
				'longHelp' => 'custom/clients/base/api/help/PAIEndPoint_PAIGetEndPoint_help.html',
			),
		);
	}

	/**
	* Method to be used for my MyEndpoint/GetExample endpoint
	*/

	public function PAISetMethod($api, $args)
	{
		require_once 'custom/modules/ParametersAdvancedInstance/classes/ConfigClass.php';

		$configClass = new ConfigClass();

		switch ($args['method']) {
			case 'post':
				return $configClass->newConfig($args);
				break;

			case 'update':
				return $configClass->editConfig($args);
				break;

			case 'delete':
				return $configClass->deleteConfig($args);
				break;

			default:
				return array("status" => 204, "message" => "Mètodo no existe");
				break;
		}



		//newConfig($name, $value);
		
		//custom logic
		//return array("status" => 200, "message" => "Creaciòn objeto tabla admin correcto ". $args['name']);
		//return $args;
		//return {"sasa":"sasa"};
		//return $_SERVER['REQUEST_METHOD'];
		//return $args;
	}

}

?>
