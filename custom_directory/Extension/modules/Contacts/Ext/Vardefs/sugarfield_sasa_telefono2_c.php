<?php
 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_telefono2_c']['labelValue']='Teléfono 2';
$dictionary['Contact']['fields']['sasa_telefono2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_telefono2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_telefono2_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>