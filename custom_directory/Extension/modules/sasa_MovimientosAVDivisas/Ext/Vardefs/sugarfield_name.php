<?php
 // created: 2018-11-21 22:19:43
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['formula']='$sasa_tipo_c';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['enforced']=true;

 ?>