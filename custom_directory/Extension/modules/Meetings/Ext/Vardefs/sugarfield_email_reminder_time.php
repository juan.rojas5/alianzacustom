<?php
 // created: 2018-07-06 21:53:28
$dictionary['Meeting']['fields']['email_reminder_time']['default']='-1';
$dictionary['Meeting']['fields']['email_reminder_time']['audited']=true;
$dictionary['Meeting']['fields']['email_reminder_time']['comments']='Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['email_reminder_time']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['email_reminder_time']['calculated']=false;
$dictionary['Meeting']['fields']['email_reminder_time']['dependency']=false;

 ?>