<?php
// created: 2019-01-04 21:37:35
$dictionary["Account"]["fields"]["accounts_sasa_saldosav_1"] = array (
  'name' => 'accounts_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAV',
  'bean_name' => 'sasa_SaldosAV',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
