<?php
 // created: 2022-10-25 23:16:58
$dictionary['Account']['fields']['sasa_municipiointer_c']['labelValue']='Ciudad';
$dictionary['Account']['fields']['sasa_municipiointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_municipiointer_c']['enforced']='';
$dictionary['Account']['fields']['sasa_municipiointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 ?>