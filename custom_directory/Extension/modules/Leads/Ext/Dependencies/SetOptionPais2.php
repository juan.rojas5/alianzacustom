<?php
$dependencies['Leads']['SetOptionPais2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_pais_c,"No Registra")',
   'triggerFields' => array('sasa_pais_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_departamento_c',
           'keys' => 'createList("No Registra")',
           'labels' => 'createList("No Registra")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_departamento_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
