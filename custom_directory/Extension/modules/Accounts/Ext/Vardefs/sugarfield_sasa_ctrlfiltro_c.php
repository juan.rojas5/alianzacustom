<?php
 // created: 2022-10-25 23:16:55
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['labelValue']='Ctrl Filtro';
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['formula']='related($assigned_user_link,"id")';
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_ctrlfiltro_c']['dependency']='';

 ?>