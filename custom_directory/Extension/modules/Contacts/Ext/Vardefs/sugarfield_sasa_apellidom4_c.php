<?php
 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_apellidom4_c']['labelValue']='Cuenta (Apellido) - M4';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_apellidom4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['readonly_formula']='';

 ?>