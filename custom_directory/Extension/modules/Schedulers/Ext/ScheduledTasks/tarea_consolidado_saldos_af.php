<?php

$job_strings[] = 'tarea_consolidado_saldos_af';
function tarea_consolidado_saldos_af(){
	$GLOBALS['log']->security("\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_consolidado_saldos_AF. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query="
			SELECT
				tipo,
				cuenta,
				categoria,
				SUM(saldo_total) AS saldo_total,
				descripcion,
				cantidad,
				id_cuenta,
				id_categoria,
				id_producto
			FROM (
					SELECT
						'Saldos AF' AS tipo,
						accounts.name AS cuenta,
						sasa_categorias.name AS categoria,
						SUBSTRING_INDEX(
							GROUP_CONCAT(
								sasa_saldosaf.sasa_saldoactual_c
								ORDER BY
									sasa_saldosaf.sasa_fechasaldo_c
								DESC
							),
							',',
							1
						) AS saldo_total,
						'--' AS descripcion,
						count(0) AS cantidad,
						accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida AS id_cuenta,
						sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida AS id_categoria,
						sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida AS id_producto
					FROM
						sasa_saldosaf
					LEFT JOIN accounts_sasa_saldosaf_1_c ON accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
					LEFT JOIN sasa_productosafav_sasa_saldosaf_1_c ON sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
					LEFT JOIN sasa_categorias_sasa_productosafav_1_c ON sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_productosafav_idb = sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida
					LEFT JOIN sasa_categorias ON sasa_categorias.id = sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida
					LEFT JOIN accounts ON accounts.id = accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida
					WHERE
						sasa_saldosaf.deleted = 0 AND accounts_sasa_saldosaf_1_c.deleted = 0 AND sasa_productosafav_sasa_saldosaf_1_c.deleted = 0 AND sasa_categorias_sasa_productosafav_1_c.deleted = 0 AND sasa_categorias.deleted = 0 AND accounts.deleted = 0
					GROUP BY
						id_cuenta,
						id_categoria,
						id_producto
					ORDER BY
						id_cuenta,
						id_categoria

			) af
			GROUP BY
				id_cuenta,
				id_categoria,
				tipo
			ORDER BY
				tipo,
				id_cuenta,
				id_categoria,
				id_producto
		";
		$hoy = date("Y-m-d");
		global $db;
		$db->query("DELETE FROM sasas_saldosconsolidados WHERE name LIKE 'Saldos AF%'");//caso sasa 6430 Saldos - Visualización registro mas reciente |||| Se limina todo, ya que son muchos registros para mantener el historico de eliminados....
		$result = $db->query($query);
		while($row = $db->fetchByAssoc($result)){
			$sasaS_SaldosConsolidados = BeanFactory::newBean('sasaS_SaldosConsolidados', "", array('disable_row_level_security' => true));
			$sasaS_SaldosConsolidados->name = "{$row["tipo"]} de {$hoy} - {$row["categoria"]} ({$row["cantidad"]})";
			$sasaS_SaldosConsolidados->accounts_sasas_saldosconsolidados_1accounts_ida = $row["id_cuenta"];
			$sasaS_SaldosConsolidados->sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida = $row["id_categoria"];
			$sasaS_SaldosConsolidados->sasa_saldototal_c= $row["saldo_total"];
			$sasaS_SaldosConsolidados->description= $row["descripcion"];
			$sasaS_SaldosConsolidados->save();
			$GLOBALS['log']->security("\n{$sasaS_SaldosConsolidados->id} -> {$sasaS_SaldosConsolidados->name} = \${$sasaS_SaldosConsolidados->sasa_saldototal_c}");
		}
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_consolidado_saldos_AF. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n\n");
	return $return;
}
