<?php
 // created: 2018-07-13 21:24:04
$dictionary['Contact']['fields']['assistant_phone']['len']='20';
$dictionary['Contact']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Contact']['fields']['assistant_phone']['comments']='Phone number of the assistant of the contact';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant_phone']['calculated']=false;
$dictionary['Contact']['fields']['assistant_phone']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['assistant_phone']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 ?>