<?php
 // created: 2022-09-28 14:17:26
$dictionary['Case']['fields']['sasa_tipoproducto_c']['labelValue']='Tipo de producto';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['visibility_grid']=array (
  'trigger' => 'sasa_negocios_c',
  'values' => 
  array (
    'Inversiones' => 
    array (
      0 => '',
      1 => '1',
      2 => '2',
      3 => '5',
      4 => '6',
      5 => '7',
      6 => '9',
      7 => '8',
      8 => '3',
      9 => '10',
      10 => '4',
      11 => '13',
      12 => '15',
      13 => '16',
      14 => '14',
      15 => '12',
      16 => '11',
    ),
    'Fiduciaria' => 
    array (
      0 => '',
      1 => '17',
      2 => '18',
    ),
    'Valores' => 
    array (
      0 => '',
      1 => '19',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>