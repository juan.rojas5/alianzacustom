//https://drive.google.com/drive/u/1/folders/1EpZtxH359tCyhO769-O8njnoopwUH2P8
({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	token: null,
	autenticarse: null,
	ws: null,
	data: null,
	tipo: null,
	encargo: [],
	certificado: [],
	url: '',
	tipoIdentificacion:'',
	initialize: function(options) {
		var self = this;
		dashlet7 = self;
		if(typeof app === 'undefined') app = App;
		self._super('initialize', [options]);
		if(self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;

		var checkExist = setInterval(function() {
			if($('#dashlet7_consultar')
				.length) {
				if(self.boton) {
					self.boton = false; //solo añadir la funciona al boton una vez...
					if(typeof self.bean.sasa_nroidentificacion_c !== 'undefined') { //para la vista registro de cuenta
						$("#dashlet7_id_cliente")
							.val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet7_id_cliente")
							.css("border", "none");
						$("#dashlet7_id_cliente")
							.css("background", "rgba(0,0,0,0)");
						$("#dashlet7_id_cliente")
							.attr('readonly', true);
					}

					$("#dashlet7_Tipo_Fondo")
						.change(function() {
							self.dashlet7_portal_dashlet7_Tipo_Fondo_change();
						});

					$("#dashlet7_Tipo_Certificado")
						.change(function() {
							self.dashlet7_portal_dashlet7_Tipo_Certificado_change();
						});

					$("#dashlet7_consultar")
						.click(function() {
							self.dashlet7_portal();
						});

					$("#dashlet7_consultar")
						.show();

					$("#dashlet7_descargar1")
						.click(function() {
							self.dashlet_portal_obtener_descargar20();
						});

					$("#dashlet7_enviar1")
						.click(function() {
							dashlet7.dashlet_portal_obtener_enviar20();
						});

					$("#dashlet7_descargar2")
						.click(function() {
							self.dashlet7_portal_obtener_descargar2(true, false);
						});

					$("#dashlet7_enviar2")
						.click(function() {
							self.dashlet7_portal_obtener_descargar2(true, true);
						});
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet7_portal: function() {
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if(!_.isEmpty($("#dashlet7_id_cliente").val())) {
				self.dashlet7_portal_dashlet7_get_cuenta();
				
				//extracto	
				self.dashlet7_portal_obtener_fondo(false); //poblando fondo
				self.dashlet7_portal_dashlet7_Tipo_Fondo_change();//poblando encargo
				
				//certificado
				self.dashlet7_portal_obtener_tipo_certificado(false); //poblando lista tipo certificado
				self.dashlet7_portal_dashlet7_Tipo_Certificado_change();
			} else {

				app.alert.show('error_dashlet7_portal', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet7_portal_dashlet7_get_cuenta: function() {
		var self = this;
		if(typeof app === 'undefined') app = App;

		/*
		Obteniendo cuenta
		*/
		$("#dashlet7_nombre")
			.html('');
		$("#dashlet7_correo")
			.html('');
		self.tipoIdentificacion="";
		app.api.call(
			'GET',
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]=' + $("#dashlet7_id_cliente")
				.val()), {
				"sasa_nroidentificacion_c": $("#dashlet7_id_cliente")
					.val(),
			}, {
				success: function(data) {
					if(!_.isEmpty(data.records)) {
						$("#dashlet7_nombre").html(data.records[0].name);
						self.tipoIdentificacion = data.records[0].sasa_tipoidentificacion_c;
						if(self.tipoIdentificacion=="Cedula de Ciudadania") self.tipoIdentificacion ="CC";
						if(self.tipoIdentificacion=="Cedula de Extranjeria") self.tipoIdentificacion ="CE";
						if(self.tipoIdentificacion=="Nit") self.tipoIdentificacion ="NIT";
						if(!_.isEmpty(data.records[0].email)) {
							$.each(data.records[0].email, function(k, mail) {
								var valor = mail.email_address;
								var texto = mail.email_address;
								var selected = "";
								if(mail.primary_address) selected = 'selected="true"';
								if(mail.invalid_email) {
									texto += " (Invalido)";
								} else if(mail.primary_address) {
									texto += " (Principal)";
								} else if(mail.opt_out) {
									texto += " (Rehusado)";
								} else if(mail.invalid_email) {
									texto += " (No Valido)";
								} else {
									texto += " (Opcional)";
								}
								if(!_.isEmpty(valor)) {
									$("#dashlet7_correo")
										.append('<option ' + selected + ' value="' + valor + '">' + texto + '</option>');
								}
							});
						}
					} else {
						$("#dashlet7_nombre").html('--<b>No Existe en sugar</b>--');
						$("#dashlet7_correo").html('<option>--</option>');
					}
				},
				error: function(e) {
					app.alert.dismiss('dashlet7_dashlet7_portal_dashlet7_get_cuenta');
					app.alert.show('error_dashlet7_portal_dashlet7_get_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e,
						autoClose: false,
					});
				}
			}, {
				async: false
			}
		);
		if(self.tipoIdentificacion=="" | self.tipoIdentificacion==null) self.tipoIdentificacion ="CC";
	},
	dashlet7_portal_obtener_encargo2: function(autenticarse) {
		var self = dashlet7; 
		if(typeof app === 'undefined') app = App;

		if(!_.isEmpty(self.certificado)) {
			self.autenticarse = autenticarse;
			var i = $("#dashlet7_Tipo_Certificado")
				.val();
			var fondo = self.certificado[i].codigoFondo;
			self.ws = "/api_fiducia/consultas/fondos/" + fondo + "/encargos?idUsuarioRecurso=" + $("#dashlet7_id_cliente")
				.val();
			self.tipo = "GET";
			self.data = [];
			self.encargo = [];
			self.dashlet7_portal_call("dashlet7_portal", {}); //autenticandose y consumo
			if(self.token != null) {
				if(!_.isEmpty(self.respuesta)) {
					if(!$.isArray(self.respuesta)) {
						var temp = self.respuesta;
						self.respuesta = [];
						self.respuesta.push(temp);
					}
					console.log("respuesta_encargo2: ", self.respuesta);
					$.each(self.respuesta, function(i, respuesta) {
						var valor1 = respuesta.wspf_plan + respuesta.wspf_plan_digito;
						var valor2 = respuesta.wspf_plan;
						var texto1 = respuesta.wspf_plan + respuesta.wspf_plan_digito + ' ' + respuesta.wspf_plan_descri;
						var texto2 = respuesta.wspf_plan + ' ' + respuesta.wspf_plan_descri;
						if(typeof respuesta.wspf_plan !== 'undefined') {
							self.encargo[i] = [];
							self.encargo[i][respuesta.wspf_fond + '_valor1'] = valor1;
							self.encargo[i][respuesta.wspf_fond + '_valor2'] = valor2;
							self.encargo[i][respuesta.wspf_fond + '_texto1'] = texto1;
							self.encargo[i][respuesta.wspf_fond + '_texto2'] = texto2;
						}
					});
				}
			}
		}
	},
	dashlet7_portal_dashlet7_Tipo_Certificado_change: function() {
		var self = this;
		if(typeof app === 'undefined') app = App;

		console.log("dashlet7_portal_dashlet7_Tipo_Certificado_change", self.certificado);
		$("#dashlet7_Encargob").empty(); //limpiando
		$("#dashlet7_Periodob").empty(); //limpiando

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			self.dashlet7_portal_obtener_encargo2(true); //poblando lista encargo		
			if(!_.isEmpty(self.certificado)) {
				var i = $("#dashlet7_Tipo_Certificado").val();
				var fondo = self.certificado[i].codigoFondo;
				var anioDesde = self.certificado[i].anioDesde;
				var anioHasta = self.certificado[i].anioHasta;
				if(!_.isEmpty(self.encargo)) {
					$.each(self.encargo, function(k, encargo) {
						var encargo_valor = encargo[fondo + '_valor2'];
						var encargo_texto = encargo[fondo + '_texto2'];
						if(!_.isEmpty(encargo_valor)) {
							$("#dashlet7_Encargob").append('<option value="' + encargo_valor + '">' + encargo_texto + '</option>');
						}
					});
				} 

				for(i = anioHasta; i >= anioDesde; i--) {
					$("#dashlet7_Periodob").append('<option value="' + i + '">' + i + '</option>');
				}
				if(_.isEmpty($("#dashlet7_Periodob").html().trim())) {
					$("#dashlet7_Periodob").append('<option value="">--</option>');
				}
			}
			
			if(_.isEmpty($("#dashlet7_Encargob").html().trim())) {
				$("#dashlet7_Encargob").append('<option value="">--</option>');
			}
			
			if(_.isEmpty($("#dashlet7_Periodob").html().trim())) {
				$("#dashlet7_Periodob").append('<option value="">--</option>');
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet7_portal_obtener_cuenta: function(autenticarse) {
		var self = this;
		if(typeof app === 'undefined') app = App;

		self.autenticarse = autenticarse;
		self.ws = "/api_fiducia/comercial/consultas/obtenerClientes";
		self.tipo = "POST";
		self.data = '{\"nombre": \"\", \"id\": \"' + $("#dashlet7_id_cliente")
			.val() + '\", \"tipoId\": \"\", \"producto\": \"\", \"desde\": 1, \"hasta\": 1}';
		self.dashlet7_portal_call("dashlet7_portal", {}); //autenticandose y consumo
	},
	dashlet7_portal_obtener_tipo_tipoIdentificacion: function(autenticarse) {
		var self = this;
		if(typeof app === 'undefined') app = App;

		self.autenticarse = autenticarse;
		$("#dashlet7_tipo_tipoIdentificacion").empty(); //limpiando
		self.ws = "/auth_fiducia/enumeracion/tiposIdentificacionComercial";
		self.tipo = "GET";
		self.data = '';
		self.dashlet7_portal_call("dashlet7_portal", {}); //autenticandose y consumo	
		
		if(_.isEmpty($("#dashlet7_tipo_tipoIdentificacion").html().trim())) {
			$("#dashlet7_tipo_tipoIdentificacion").append('<option value="">--</option>');
		}
	},
	dashlet7_portal_obtener_tipo_certificado: function(autenticarse) {
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		self.autenticarse = autenticarse;
		$("#dashlet7_Tipo_Certificado").empty(); //limpiando
		self.ws = "/api_reportes/consultas/certificaciones/certificadosTributariosList";
		self.tipo = "GET";
		self.data = [];
		self.dashlet7_portal_call("dashlet7_portal", {}); //autenticandose y consumo
		if(self.token != null) {
			if(!_.isEmpty(self.respuesta)) {
				if(!$.isArray(self.respuesta)) {
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("respuesta_obtener_tipo_certificado: ", self.respuesta);
				self.certificado = self.respuesta;
				$.each(self.respuesta, function(i, respuesta) {
					if(typeof respuesta.nombreCert !== 'undefined') {
						$("#dashlet7_Tipo_Certificado")
							.append('<option value="' + i + '">' + respuesta.nombreCert + '</option>');
					} else {

						app.alert.show('error_dashlet7_portal_obtener_tipo_certificado', {
							level: 'error',
							title: 'Error al obtener_certificado diferente',
							autoClose: false,
						});
					}
				});
			} else {
				self.certificado = [];

				app.alert.show('error_dashlet7_portal_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener_certificado vacio',
					autoClose: false,
				});
			}
		}
		
		if(_.isEmpty($("#dashlet7_Tipo_Certificado").html().trim())) {
			$("#dashlet7_Tipo_Certificado").append('<option value="">--</option>');
		}
	},
	dashlet7_portal_obtener_descargar2: function(autenticarse, enviar) {
		var self = this;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if(!_.isEmpty($("#dashlet7_id_cliente").val())) {
				if ( !_.isEmpty($("#dashlet7_Tipo_Certificado").val())  &&  !_.isEmpty($("#dashlet7_Encargob").val())    &&  !_.isEmpty($("#dashlet7_Periodob").val())      ){
					self.dashlet7_portal_obtener_cuenta(true); //poblando cuenta
					if(self.token != null) {
						if(!_.isEmpty(self.respuesta)) {
							console.log("cuenta:", self.respuesta);
							if(typeof self.respuesta.datos !== 'undefined') {
								if(typeof self.respuesta.datos[0] !== 'undefined') {
									if(typeof self.respuesta.datos[0].codTipoIdentificacion !== 'undefined') {
										var tipoIdentificacion = self.respuesta.datos[0].codTipoIdentificacion;
										if(tipoIdentificacion != null) {
											self.autenticarse = false;
											self.tipo = "file_post";

											var ruta = self.certificado[$("#dashlet7_Tipo_Certificado")
												.val()].rutaCert;
											var encargo = $("#dashlet7_Encargob")
												.val();
											var vigencia = $("#dashlet7_Periodob")
												.val();
											self.ws = "/api_reportes/consultas/certificaciones/generarCertificadoTributario?idUsuarioRecurso=" + $("#dashlet7_id_cliente")
												.val() + "&tipoIdentificacion=" + tipoIdentificacion;
											self.data = '{\"ruta\":\"' + ruta + '\",\"encargo\":\"' + encargo + '\",\"vigencia\":' + vigencia + '}';
											var mail = {};
											var tipo = "Certificado Tributario Fondos";
											if(enviar) {
												var Address = "";
												if(!_.isEmpty($("#dashlet7_correo")
														.val())) {
													if(typeof $("#dashlet7_correo")
														.val()[0] !== 'undefined') {
														if(!_.isEmpty($("#dashlet7_correo")
																.val()[0])) {
															firma = "Atentamente,\n___________\nAlianza";
															//Address = $("#dashlet7_correo").val().join("; ");
															Address = $("#dashlet7_correo").val();
															Subject = tipo + " - " + $("#dashlet7_id_cliente")
																.val() + " - " + Date();
															nombre = $("#dashlet7_nombre")
																.html();

															Body = "";
															Body += "Buen día,";
															Body += "\n\n";
															Body += "Estimado " + nombre + ",";
															Body += "\n\n";
															Body += "\n\n";
															Body += "Reciba un cordial saludo de parte de Alianza.";
															Body += "\n\n";
															Body += "Adjunto a este correo encontrara el " + tipo + " en formato PDF.";
															Body += "\n\n";
															Body += "\n\n";
															Body += "Cordialmente,";
															Body += "\n\n";
															Body += "Alianza.";
															Body += "\n\n";
															Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";

															mail = {
																"Subject": Subject,
																"Body": Body,
																"Address": Address,
																"AddressCC": "",
															};
														}
													}
												}
												if(_.isEmpty(Address)) {
													app.alert.show('error_dashlet7_portal_obtener_descargar2', {
														level: 'error',
														title: "Por favor, seleccione una direccion de correo!!",
														autoClose: false,
													});
													return;
												}
											}
											self.dashlet7_portal_call("dashlet7_portal", mail); //autenticandose y consumo
											if(enviar) console.log("respuesta2", self.respuesta);
											if(!_.isEmpty(self.respuesta)) {
												if(enviar) {
													if(self.respuesta == "ok") {
														//var Address = $("#dashlet7_correo").val().join("; ");
														var Address = $("#dashlet7_correo").val();
														app.alert.show('success_dashlet7_portal_obtener_descargar2', {
															level: 'success',
															messages: "Correo " + tipo + " enviado!!<p> Correos: " + Address + "!",
															autoClose: false,
														});
													} else {
														app.alert.show('error_dashlet7_portal_obtener_descargar2', {
															level: 'error',
															title: self.respuesta,
															autoClose: false,
														});
													}
												} else {
													//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
							                        var mapForm = document.createElement("form");
							                            mapForm.target = "Map";
							                            mapForm.method = "POST"; // or "post" if appropriate
							                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

							                            var mapInput = document.createElement("input");
							                            mapInput.type = "text";
							                            mapInput.name = "file";
							                            mapInput.value = self.respuesta;
							                            mapForm.appendChild(mapInput);

							                            document.body.appendChild(mapForm);

							                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

							                        if (map) {
							                            mapForm.submit();
							                        }
													else{
														app.alert.show('error_procesando_obtener_descargar1', {
															level: 'error',
															title: 'Error al descargar PDF',
															messages: "El navegador esta bloqueando la ventana!!!",
															autoClose: false,
														});
													}
												}
											} else {

												app.alert.show('error_dashlet7_portal_obtener_descargar2', {
													level: 'error',
													title: 'Error al descargar PDF',
													autoClose: false,
												});
											}
										} else {

											app.alert.show('error_dashlet7_portal_obtener_descargar2', {
												level: 'error',
												title: 'Error tipo_tipoIdentificacion vacio',
												autoClose: false,
											});
										}
									} else {

										app.alert.show('error_dashlet7_portal_obtener_descargar2', {
											level: 'error',
											title: 'Error obtener_cuenta vacio',
											autoClose: false,
										});
									}
								} else {

									app.alert.show('error_dashlet7_portal_obtener_descargar2', {
										level: 'error',
										title: 'Error obtener_cuenta vacio',
										autoClose: false,
									});
								}
							} else {

								app.alert.show('error_dashlet7_portal_obtener_descargar2', {
									level: 'error',
									title: 'Error obtener_cuenta vacio',
									autoClose: false,
								});
							}
						} else {

							app.alert.show('error_dashlet7_portal_obtener_descargar2', {
								level: 'error',
								title: 'Error obtener_cuenta vacio',
								autoClose: false,
							});
						}
					}
				} else{
						
					app.alert.show('error_procesando_obtener_descargar2', {
						level: 'error',
						title: "Hay un campo sin seleccionar",
						autoClose: false,
					});
				}
			} else {

				app.alert.show('error_dashlet7_portal_obtener_descargar2', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet7_portal_obtener_periodo: function(autenticarse) {
		var self = this;
		if(typeof app === 'undefined') app = App;

		$("#dashlet7_Periodoa").empty(); //limpiando		
		var date = new Date();
		for(i = 0; i < 6; i++) {
			var option =  date.toLocaleString("es", {year: "numeric"}) + '/'+ date.toLocaleString("es", {month:"2-digit"}); //formato requerido	
			var value = date.toLocaleString("es", {month: "long"}).replace(/(\b\w)/gi, function(m) {
				return m.toUpperCase();
			}) + " " + date.getFullYear(); //formato requerido			
			date.setMonth(date.getMonth() - 1); //un mes menos
			$("#dashlet7_Periodoa").append('<option value="' + option + '">' + value + '</option>');
		}
		
		if(_.isEmpty($("#dashlet7_Periodoa").html().trim())) {
			$("#dashlet7_Periodoa").append('<option value="">--</option>');
		}
	},
	dashlet7_portal_obtener_fondo: function(autenticarse) {
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		self.ws = "/extractos-v1/api_fiducia/consultas/fondos";
		self.tipo = "POST";
		self.data = {
			"idUsuarioRecurso": $("#dashlet7_id_cliente").val(),
		};
		self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
		$("#dashlet7_Tipo_Fondo").empty(); //limpiando
		if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
			if(self.respuesta.operacionExitosa) {
				console.log("respuesta_fondo_extracto: ", self.respuesta);
				$.each(self.respuesta.listaProductos, function(i, respuesta) {
					if(typeof respuesta.identificacion !== 'undefined') {
						$("#dashlet7_Tipo_Fondo").append('<option value="' + respuesta.identificacion + '">' + respuesta.descripcion1 + '</option>');
					}
				});
			} else {
				if(typeof self.respuesta.error !== 'undefined') {					
					app.alert.show('error_dashlet7_portal_obtener_fondo', {
						level: 'error',
						title: '<p><br>Error al enviar PDF Extractos',
						messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
						autoClose: false,
					});
				}
				else{
					app.alert.show('error_dashlet7_portal_obtener_fondo', {
						level: 'error',
						title: 'Error al obtener_fondo extracto: ' + self.respuesta.descripcionError,
						autoClose: false,
					});
				}
			}	
		}		
				
		if(_.isEmpty($("#dashlet7_Tipo_Fondo").html().trim())) {
			$("#dashlet7_Tipo_Fondo").append('<option value="">--</option>');
		}
	},
	dashlet7_portal_dashlet7_Tipo_Fondo_change: function() {
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			self.ws = "/extractos-v1/api_fiducia/consultas/encargo";
			self.tipo = "POST";
			self.data = {
				"idUsuarioRecurso": $("#dashlet7_id_cliente").val(),
				"numFondo": $("#dashlet7_Tipo_Fondo").val(),
			};
			self.encargo = [];	
				
			self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
		
			$("#dashlet7_Encargoa").empty(); //limpiando
			
			if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
				if(self.respuesta.operacionExitosa) {
					console.log("respuesta_encarg_extracto: ", self.respuesta);
					$.each(self.respuesta.listaProductos, function(i, respuesta) {
						if(typeof respuesta.plan !== 'undefined') {
							$("#dashlet7_Encargoa").append('<option value="' + respuesta.plan + '">' + respuesta.plan + ' ' + respuesta.descripcion + '</option>');
						}
					});
				} 
				else {
					if(typeof self.respuesta.error !== 'undefined') {					
						app.alert.show('error_dashlet7_portal_dashlet7_Tipo_Fondo_change', {
							level: 'error',
							title: '<p><br>Error al enviar PDF Extractos',
							messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
							autoClose: false,
						});
					}
					else{
						app.alert.show('error_dashlet7_portal_dashlet7_Tipo_Fondo_change', {
							level: 'error',
							title: 'Error al obtener_fondo extracto: ' + self.respuesta.descripcionError,
							autoClose: false,
						});
					}
				}	
			}
				
			if(_.isEmpty($("#dashlet7_Encargoa").html().trim())) {
				$("#dashlet7_Encargoa").append('<option value="">--</option>');
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";			
		}, 200);
		self.dashlet7_portal_obtener_periodo(false); //poblando periodo
	},
	dashlet_portal_obtener_enviar20: function(autenticarse, enviar) {	
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if(!_.isEmpty($("#dashlet7_id_cliente").val()) && !_.isEmpty($("#dashlet7_Encargoa").val()) && !_.isEmpty($("#dashlet7_Periodoa").val()) && !_.isEmpty($("#dashlet7_Tipo_Fondo").val())) {				
				var Address = "";
				if(!_.isEmpty($("#dashlet7_correo").val())) {
					if(typeof $("#dashlet7_correo").val()[0] !== 'undefined') {
						if(!_.isEmpty($("#dashlet7_correo").val()[0])) {
							//Address = $("#dashlet7_correo").val().join("; ");
							Address = $("#dashlet7_correo").val();
						}
					}
				}
				
				if(_.isEmpty(Address)) {
					app.alert.show('error_dashlet_portal_obtener_enviar20', {
						level: 'error',
						title: "Por favor, seleccione una direccion de correo!!",
						autoClose: false,
					});
				}
				else{			
					self.ws = "/extractos-v1/api_alianza/docs/extractosPorCorreo";
					self.tipo = "POST";
					self.data = {
						"tipoProducto": "FIC",
						"producto": $("#dashlet7_Encargoa").val(),
						"periodo":$("#dashlet7_Periodoa").val(),
						"tipoIdentificacion": self.tipoIdentificacion,
						"numeroIdentificacion": $("#dashlet7_id_cliente").val(),
						"correo": Address,
					};
					
					self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
					
					if(self.respuesta == '' | self.respuesta == null){
						self.respuesta = "En proceso...";
						
						app.alert.show('dashlet7_dashlet_portal_obtener_enviar20', {
							level: 'success',
							title: 'Se ha enviado la solicitud al servidor!!',							
							messages:'<hr><p><b>Respuesta:</b><p><br><p>'+self.respuesta+"<p>",
							autoClose: false,
						});	
					}
					else{						
						if(typeof self.respuesta.error !== 'undefined') {
							app.alert.show('error_dashlet_portal_obtener_enviar20', {
								level: 'error',
								title: '<p><br>Error al enviar PDF Extractos',
								messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
								autoClose: false,
							});
						}
						else{	
							app.alert.show('dashlet7_dashlet_portal_obtener_enviar20', {
								level: 'error',
								title: '<p><br>Error al enviar PDF Extractos',						
								messages:"Fallo en el  WS de enviar PDF pro correo para Extractos",
								autoClose: false,
							});	
						}
					}
				}
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_enviar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet_portal_obtener_descargar20: function(autenticarse, enviar) {
		var self = dashlet7;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet7_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet7_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if(!_.isEmpty($("#dashlet7_id_cliente").val()) && !_.isEmpty($("#dashlet7_Encargoa").val()) && !_.isEmpty($("#dashlet7_Periodoa").val()) && !_.isEmpty($("#dashlet7_Tipo_Fondo").val())) {
				self.ws = "/extractos-v1/api_alianza/docs/extractos";
				self.tipo = "POST";
				self.data = {
					"tipoProducto": "FIC",
					"producto": $("#dashlet7_Encargoa").val(),
					"periodo":$("#dashlet7_Periodoa").val(),
					"tipoIdentificacion": self.tipoIdentificacion,
					"numeroIdentificacion": $("#dashlet7_id_cliente").val(),
				};
				
				self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
				
				if(!_.isEmpty(self.respuesta)) {
					if(typeof self.respuesta.error !== 'undefined') {
						app.alert.show('error_dashlet_portal_obtener_descargar20', {
							level: 'error',
							title: '<p><br>Error al descargar PDF Extractos',
							messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
							autoClose: false,
						});
					}
					else{
						//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
                        var mapForm = document.createElement("form");
                            mapForm.target = "Map";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

                            var mapInput = document.createElement("input");
                            mapInput.type = "text";
                            mapInput.name = "file";
                            mapInput.value = self.respuesta;
                            mapForm.appendChild(mapInput);

                            document.body.appendChild(mapForm);

                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

                        if (map) {
                            mapForm.submit();
                        } else {
                        	app.alert.show('error_procesando_obtener_descargar1', {
								level: 'error',
								title: 'Error al descargar PDF',
								messages: "Debes permitir las ventanas emergentes para que este visualizador funcione.",
								autoClose: false,
							});
                        }

                        //Funcion en version 10
						/*let pdfWindow = window.open("", "Extracto");	
                        let pdfWindow ="Hola";
						//console.log("URL Visualizar PDF "+self.respuesta);					
						if(pdfWindow != null){
							pdfWindow.document.write("<iframe  height='98.5%' width='99%' style='position:absolute;' src='data:application/pdf;base64, " + encodeURI(self.respuesta) + "'></iframe>");
						}
						else{
							app.alert.show('error_procesando_obtener_descargar1', {
								level: 'error',
								title: 'Error al descargar PDF',
								messages: "El navegador esta bloqueando la ventana!!!",
								autoClose: false,
							});
						}*/
					}
				} 
				else {
					app.alert.show('error_dashlet_portal_obtener_descargar20', {
						level: 'error',
						title: 'Error al descargar PDF Extractos',
						autoClose: false,
					});
				}
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_descargar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet7_portal_procesando');
			document.getElementById('dashlet7_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet_portal_call20: function(PAI, mail) {
		var self = this;
		if(typeof app === 'undefined') app = App;
		var method = "dashlet_portal";
		var data = [{
			'url': self.url,
			'tipo': self.tipo,
			'ws': self.ws,
			'data': self.data,
			'PAI': PAI,
		}];

		var url = app.api.buildURL(method, 'read', '', {
			data
		});

		app.api.call('read', url, null, {
			success: _.bind(function(response) {
				self.respuesta = response['respuesta'];
			}, this),
			error: _.bind(function(error) {
				self.token = null;
				self.respuesta = null;

				app.alert.show('error_dashlet_portal_call20', {
					level: 'error',
					title: 'Fallo al procesar ' + error,
					autoClose: false,
				});
			}, this),
		}, {
			async: false
		});
	},
	dashlet7_portal_call: function(PAI, mail) {
		var self = this;
		if(typeof app === 'undefined') app = App;

		/*
		Para el periodo
		Al parecer no hay ningún servicio que realice esta lógica, el portal con esa función lo que hace es calcular los últimos 6 meses a partir de la fecha para mostrar en el listado.

		*/
		if(typeof app === 'undefined') app = App;
		var method = "dashlet_portal";
		var data = [{
				'url': self.url,
				'tipo': 'POST',
				'ws': '/auth_fiducia/comercial/autenticar',
				'data': "**PAI**",
				'autenticarse': self.autenticarse,
				'token': self.token,
				'PAI': PAI,
			},
			{
				'url': self.url,
				'tipo': self.tipo,
				'ws': self.ws,
				'data': self.data,
				'mail': mail,
			},
		];

		var url = app.api.buildURL(method, 'read', '', {
			data
		});

		app.api.call('read', url, null, {
			success: _.bind(function(response) {
				self.token = response['token'];
				self.respuesta = response['respuesta'];


				if(self.token.includes("Credenciales invalidas")) {
					error = JSON.parse(self.token);
					if(error["data"] == "Credenciales invalidas") {
						app.alert.show('error_dashlet7_portal_call', {
							level: 'error',
							title: 'Portal7: Error ' + error["code"] + ' ' + error["data"],
							autoClose: false,
						});
						self.token = null;
					}
				}

			}, this),
			error: _.bind(function(error) {
				self.token = null;
				self.respuesta = null;

				app.alert.show('error_dashlet7_portal_call', {
					level: 'error',
					title: 'Fallo al procesar ' + error,
					autoClose: false,
				});
			}, this),
		}, {
			async: false
		});
	}
})
