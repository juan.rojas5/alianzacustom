<?php
 // created: 2018-07-18 15:00:35
$dictionary['Contact']['fields']['primary_address_street']['required']=false;
$dictionary['Contact']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.33',
  'searchable' => true,
);
$dictionary['Contact']['fields']['primary_address_street']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_street']['rows']='4';
$dictionary['Contact']['fields']['primary_address_street']['cols']='20';
$dictionary['Contact']['fields']['primary_address_street']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>