<?php
 // created: 2022-03-14 09:45:49
$dictionary['Case']['fields']['sasa_raddefensorc_c']['labelValue']='Rad Defensor del Consumidor ';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_raddefensorc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['readonly_formula']='';

 ?>