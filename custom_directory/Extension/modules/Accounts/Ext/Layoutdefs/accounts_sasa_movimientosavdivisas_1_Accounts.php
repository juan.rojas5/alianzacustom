<?php
 // created: 2019-01-04 21:41:00
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_movimientosavdivisas_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAVDivisas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'get_subpanel_data' => 'accounts_sasa_movimientosavdivisas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
