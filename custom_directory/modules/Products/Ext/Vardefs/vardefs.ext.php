<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/sugarfield_revenuelineitem_name.php

 // created: 2018-08-08 18:11:45
$dictionary['Product']['fields']['revenuelineitem_name']['audited']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['massupdate']=true;
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge']='enabled';
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge_dom_value']=1;
$dictionary['Product']['fields']['revenuelineitem_name']['merge_filter']='disabled';
$dictionary['Product']['fields']['revenuelineitem_name']['calculated']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['studio']='visible';

 
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/rli_link_workflow.php

$dictionary['Product']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Products/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Product']['full_text_search']=false;

?>
