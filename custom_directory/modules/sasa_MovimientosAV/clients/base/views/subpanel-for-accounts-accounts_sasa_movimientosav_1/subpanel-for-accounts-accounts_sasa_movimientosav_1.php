<?php
// created: 2019-01-04 21:54:58
$viewdefs['sasa_MovimientosAV']['base']['view']['subpanel-for-accounts-accounts_sasa_movimientosav_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_productosafav_sasa_movimientosav_1_name',
          'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
          'enabled' => true,
          'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1SASA_PRODUCTOSAFAV_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_nemotecnico_c',
          'label' => 'LBL_SASA_NEMOTECNICO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_emisor_c',
          'label' => 'LBL_SASA_EMISOR_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_fechacumplimiento_c',
          'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_fechaliquid_c',
          'label' => 'LBL_SASA_FECHALIQUID_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_documento_c',
          'label' => 'LBL_SASA_DOCUMENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_tipooperacion_c',
          'label' => 'LBL_SASA_TIPOOPERACION_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_detalle_c',
          'label' => 'LBL_SASA_DETALLE_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_nominal_c',
          'label' => 'LBL_SASA_NOMINAL_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'sasa_tirprecio_c',
          'label' => 'LBL_SASA_TIRPRECIO_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'sasa_valor_c',
          'label' => 'LBL_SASA_VALOR_C',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'sasa_comision_c',
          'label' => 'LBL_SASA_COMISION_C',
          'enabled' => true,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'sasa_iva_c',
          'label' => 'LBL_SASA_IVA_C',
          'enabled' => true,
          'default' => true,
        ),
        15 => 
        array (
          'name' => 'sasa_retencion_c',
          'label' => 'LBL_SASA_RETENCION_C',
          'enabled' => true,
          'default' => true,
        ),
        16 => 
        array (
          'name' => 'sasa_afavor_c',
          'label' => 'LBL_SASA_AFAVOR_C',
          'enabled' => true,
          'default' => true,
        ),
        17 => 
        array (
          'name' => 'sasa_encontra_c',
          'label' => 'LBL_SASA_ENCONTRA_C',
          'enabled' => true,
          'default' => true,
        ),
        18 => 
        array (
          'name' => 'sasa_saldo_c',
          'label' => 'LBL_SASA_SALDO_C',
          'enabled' => true,
          'default' => true,
        ),
        19 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);