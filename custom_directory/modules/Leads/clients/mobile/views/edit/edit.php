<?php
// created: 2023-02-03 09:39:47
$viewdefs['Leads']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_inforequerida_c',
          'studio' => 'visible',
          'label' => 'LBL_SASA_INFOREQUERIDA_C',
        ),
        1 => 
        array (
          'name' => 'salutation',
          'comment' => 'Contact salutation (e.g., Mr, Ms)',
          'label' => 'LBL_SALUTATION',
        ),
        2 => 
        array (
          'name' => 'first_name',
          'customCode' => '{html_options name="salutation" options=$fields.salutation.options selected=$fields.salutation.value}&nbsp;<input name="first_name" size="25" maxlength="25" type="text" value="{$fields.first_name.value}">',
          'displayParams' => 
          array (
            'wireless_edit_only' => true,
          ),
        ),
        3 => 
        array (
          'name' => 'last_name',
          'displayParams' => 
          array (
            'wireless_edit_only' => true,
          ),
        ),
        4 => 
        array (
          'name' => 'sasa_tipopersona_c',
          'label' => 'LBL_SASA_TIPOPERSONA_C',
        ),
        5 => 'account_name',
        6 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        7 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        8 => 'title',
        9 => 
        array (
          'name' => 'lead_source',
          'comment' => 'Lead source (ex: Web, print)',
          'label' => 'LBL_LEAD_SOURCE',
        ),
        10 => 
        array (
          'name' => 'refered_by',
          'comment' => 'Identifies who refered the lead',
          'label' => 'LBL_REFERED_BY',
        ),
        11 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        12 => 'phone_mobile',
        13 => 'email',
        14 => 
        array (
          'name' => 'sasa_pais_c',
          'label' => 'LBL_SASA_PAIS_C',
        ),
        15 => 
        array (
          'name' => 'sasa_departamento_c',
          'label' => 'LBL_SASA_DEPARTAMENTO_C',
        ),
        16 => 
        array (
          'name' => 'sasa_municipio_c',
          'label' => 'LBL_SASA_MUNICIPIO_C',
        ),
        17 => 'primary_address_street',
        18 => 'phone_home',
        19 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        20 => 
        array (
          'name' => 'assistant_phone',
          'comment' => 'Phone number of the assistant of the contact',
          'label' => 'LBL_ASSISTANT_PHONE',
        ),
        21 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        22 => 
        array (
          'name' => 'sasa_ocupacion_c',
          'label' => 'LBL_SASA_OCUPACION_C',
        ),
        23 => 
        array (
          'name' => 'department',
          'comment' => 'Department the lead belongs to',
          'label' => 'LBL_DEPARTMENT',
        ),
        24 => 
        array (
          'name' => 'sasa_actividadeconomica_c',
          'label' => 'LBL_SASA_ACTIVIDADECONOMICA_C',
        ),
        25 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
        ),
        26 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosmensuales_c',
          'label' => 'LBL_SASA_INGRESOSMENSUALES_C',
        ),
        27 => 
        array (
          'name' => 'website',
          'comment' => 'URL of website for the company',
          'label' => 'LBL_WEBSITE',
        ),
        28 => 
        array (
          'name' => 'sasa_nombreproducto_c',
          'label' => 'LBL_SASA_NOMBREPRODUCTO_C',
        ),
        29 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_valorinversion_c',
          'label' => 'LBL_SASA_VALORINVERSION_C',
        ),
        30 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        31 => 'status',
        32 => 'assigned_user_name',
        33 => 'tag',
      ),
    ),
  ),
);