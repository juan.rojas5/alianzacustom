<?php
// created: 2023-02-06 20:21:32
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_purchases_1"] = array (
  'name' => 'sasa_productosafav_purchases_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_purchases_1',
  'source' => 'non-db',
  'module' => 'Purchases',
  'bean_name' => 'Purchase',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
