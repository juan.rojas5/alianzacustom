<?php
// created: 2023-02-06 20:17:53
$dictionary["sasa2_Unidades_de_negocio"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);
