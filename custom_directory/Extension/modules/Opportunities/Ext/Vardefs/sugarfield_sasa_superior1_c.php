<?php
 // created: 2018-07-09 13:05:08
$dictionary['Opportunity']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Opportunity']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Opportunity']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Opportunity']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Opportunity']['fields']['sasa_superior1_c']['dependency']='';

 ?>