<?php
 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_municipiointer_c']['labelValue']='Ciudad';
$dictionary['Contact']['fields']['sasa_municipiointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_municipiointer_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_municipiointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 ?>