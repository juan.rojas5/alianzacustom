<?php
 // created: 2019-10-10 14:25:29
$dictionary['Call']['fields']['sasa_compromiso_c']['labelValue']='Compromiso';
$dictionary['Call']['fields']['sasa_compromiso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Call']['fields']['sasa_compromiso_c']['enforced']='';
$dictionary['Call']['fields']['sasa_compromiso_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadollamada_c,"Descartado")))';

 ?>