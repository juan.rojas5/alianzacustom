({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        var self = this;
        self._super('initialize', [options]);

        self.model.on('change', function (model, resp, options) 
        {
            self.mostrarValorCampoEspejo(model, resp, options, self);
        });

    },

    mostrarValorCampoEspejo: function (model, resp, options, self) 
    {
        // let changeProceso = model.changed.sasa_proceso_c;
        // let changeSubProceso = model.changed.sasa_subproceso_c;
        // let changeDetalle = model.changed.sasa_detalle_c;

        let proceso = self.model.get('sasa_proceso_c');
        let subproceso = self.model.get('sasa_subproceso_c');
        let detalle = self.model.get('sasa_detalle_c');
        let tipo_solicitud = self.model.get('sasa_tipodesolicitud_c');

        if (tipo_solicitud == "peticion") 
        {
            if ((proceso != null && proceso != '') && (subproceso != null && subproceso != '')) 
            {
                let detalleAlt = (detalle != null && detalle != '') ? detalle : 161;

                let base_url = "sasa_sasa_tipificaciones?filter[0][sasa_categoria_c]=" + proceso + "&filter[1][sasa_proceso_c]=" + subproceso + "&filter[2][sasa_detalle_c]=" + detalleAlt;

                app.api.call('read', app.api.buildURL(base_url), null, 
                {
                    success: function (response) 
                    {
                        let cantidadRegistros = response.records.length;
    
                        console.log({
                            model, resp, options, proceso, subproceso, detalle, tipo_solicitud, response, cantidadRegistros
                        });

                        if ( cantidadRegistros >= 1 ) 
                        {
                            let instructivo = ( response.records[0].sasa_instructivo_c != '' && response.records[0].sasa_instructivo_c != undefined ) ? response.records[0].sasa_instructivo_c : "";

                            self.model.set('sasa_instructivo_c', instructivo);   
                        }
                    },
                    error: function (error) 
                    {
                        console.log('ERROR: ', error);
                    }
                });
            }
        }
    }
})