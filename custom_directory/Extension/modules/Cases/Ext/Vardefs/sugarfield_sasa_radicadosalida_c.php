<?php
 // created: 2023-03-10 13:26:09
$dictionary['Case']['fields']['sasa_radicadosalida_c']['labelValue']='Radicado de salida';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radicadosalida_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['dependency']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['readonly_formula']='';

 ?>