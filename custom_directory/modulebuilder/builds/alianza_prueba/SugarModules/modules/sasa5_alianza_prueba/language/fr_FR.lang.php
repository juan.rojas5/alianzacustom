<?php
// created: 2023-04-25 03:53:11
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Créé par Nom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Alianza Prueba Catalogue',
  'LBL_MODULE_NAME' => 'Alianza Prueba',
  'LBL_MODULE_TITLE' => 'Alianza Prueba',
  'LBL_MODULE_NAME_SINGULAR' => 'Alianza Prueba',
  'LBL_HOMEPAGE_TITLE' => 'Mes Alianza Prueba',
  'LNK_NEW_RECORD' => 'Créer Alianza Prueba',
  'LNK_LIST' => 'Afficher Alianza Prueba',
  'LNK_IMPORT_SASA5_ALIANZA_PRUEBA' => 'Importar Alianza Prueba',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Alianza Prueba',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_SASA5_ALIANZA_PRUEBA_SUBPANEL_TITLE' => 'Alianza Prueba',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Alianza Prueba',
  'LNK_IMPORT_VCARD' => 'Importar Alianza Prueba vCard',
  'LBL_IMPORT' => 'Importar Alianza Prueba',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Alianza Prueba record by importing a vCard from your file system.',
  'LBL_SASA5_ALIANZA_PRUEBA_FOCUS_DRAWER_DASHBOARD' => 'Alianza Prueba Panel de enfoque',
  'LBL_SASA5_ALIANZA_PRUEBA_RECORD_DASHBOARD' => 'Alianza Prueba Cuadro de mando del registro',
);