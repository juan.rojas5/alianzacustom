<?php
$module_name = 'sasa_pqrs_historico';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_case_number_c',
                'label' => 'LBL_SASA_CASE_NUMBER_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_fecha_de_vencimiento_c',
                'label' => 'LBL_SASA_FECHA_DE_VENCIMIENTO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_nombres_cliente_c',
                'label' => 'LBL_SASA_NOMBRES_CLIENTE_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_apellidos_cliente_c',
                'label' => 'LBL_SASA_APELLIDOS_CLIENTE_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_numero_documento_client_c',
                'label' => 'LBL_SASA_NUMERO_DOCUMENTO_CLIENT',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_medio_de_recepcion_c',
                'label' => 'LBL_SASA_MEDIO_DE_RECEPCION_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_estado_de_caso_c',
                'label' => 'LBL_SASA_ESTADO_DE_CASO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_tipo_de_solicitud_c',
                'label' => 'LBL_SASA_TIPO_DE_SOLICITUD_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_tipificacion_1_c',
                'label' => 'LBL_SASA_TIPIFICACION_1_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_tipificacion_2_c',
                'label' => 'LBL_SASA_TIPIFICACION_2_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_tipificacion_3_c',
                'label' => 'LBL_SASA_TIPIFICACION_3_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_tipo_de_solicitud_codig_c',
                'label' => 'LBL_SASA_TIPO_DE_SOLICITUD_CODIG',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'sasa_tipificacion_1_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_1_CODIGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'sasa_tipificacion_2_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_2_CODIGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'sasa_tipificacion_3_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_3_CODIGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'sasa_estado_de_caso_codigo_c',
                'label' => 'LBL_SASA_ESTADO_DE_CASO_CODIGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'sasa_fecha_creacion_c',
                'label' => 'LBL_SASA_FECHA_CREACION_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'sasa_hora_c',
                'label' => 'LBL_SASA_HORA_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'sasa_solucionador_c',
                'label' => 'LBL_SASA_SOLUCIONADOR_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'sasa_vencido_c',
                'label' => 'LBL_SASA_VENCIDO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'sasa_usuario_asistente_encar_c',
                'label' => 'LBL_SASA_USUARIO_ASISTENTE_ENCAR',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              23 => 
              array (
                'name' => 'sasa_fecha_solucion_c',
                'label' => 'LBL_SASA_FECHA_SOLUCION_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              24 => 
              array (
                'name' => 'sasa_descripcion_de_la_solic_c',
                'label' => 'LBL_SASA_DESCRIPCION_DE_LA_SOLIC',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              25 => 
              array (
                'name' => 'sasa_categoria_codigo_c',
                'label' => 'LBL_SASA_CATEGORIA_CODIGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              26 => 
              array (
                'name' => 'sasa_escalado_c',
                'label' => 'LBL_SASA_ESCALADO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              27 => 
              array (
                'name' => 'sasa_solucionador_inmediato__c',
                'label' => 'LBL_SASA_SOLUCIONADOR_INMEDIATO_',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              28 => 
              array (
                'name' => 'sasa_documentado_c',
                'label' => 'LBL_SASA_DOCUMENTADO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              29 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              30 => 
              array (
                'name' => 'sasa_creado_por_nombre_compl_c',
                'label' => 'LBL_SASA_CREADO_POR_NOMBRE_COMPL',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
              31 => 
              array (
                'name' => 'sasa_segmento_datawarehouse__c',
                'label' => 'LBL_SASA_SEGMENTO_DATAWAREHOUSE_',
                'enabled' => true,
                'readonly' => '1',
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
