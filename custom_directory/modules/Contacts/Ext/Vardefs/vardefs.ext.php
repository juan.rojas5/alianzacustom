<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-05-30 20:13:32
$dictionary['Contact']['fields']['date_entered']['audited']=true;
$dictionary['Contact']['fields']['date_entered']['comments']='Date record created';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_entered']['calculated']=false;
$dictionary['Contact']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-05-30 20:33:59
$dictionary['Contact']['fields']['date_modified']['audited']=true;
$dictionary['Contact']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_modified']['calculated']=false;
$dictionary['Contact']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_first_name.php

 // created: 2018-05-30 20:36:48
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['massupdate']=false;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['first_name']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.99',
  'searchable' => true,
);
$dictionary['Contact']['fields']['first_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_birthdate.php

 // created: 2018-06-05 13:43:33
$dictionary['Contact']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Contact']['fields']['birthdate']['calculated']=false;
$dictionary['Contact']['fields']['birthdate']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_salutation.php

 // created: 2018-06-13 16:19:52
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['required']=true;
$dictionary['Contact']['fields']['salutation']['audited']=false;
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Contact']['fields']['salutation']['calculated']=false;
$dictionary['Contact']['fields']['salutation']['dependency']=false;
$dictionary['Contact']['fields']['salutation']['pii']=false;
$dictionary['Contact']['fields']['salutation']['options']='salutation_list';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/fbsg_cc_newvars.php

// $cc_module = 'Contact';
// include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/fbsg_vardefs_contacts.php

$dictionary['Contact']['fields']['fbsg_ccintegrationlog_contacts'] = array(
    'name' => 'fbsg_ccintegrationlog_contacts',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_contacts',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Contact"]["fields"]["prospect_list_contacts"] = array(
    'name' => 'prospect_list_contacts',
    'type' => 'link',
    'relationship' => 'prospect_list_contacts',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Contact';
include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2018-07-04 16:55:28
$dictionary['Contact']['fields']['phone_mobile']['len']='100';
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.09',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_mobile']['calculated']=false;
$dictionary['Contact']['fields']['phone_mobile']['required']=true;
$dictionary['Contact']['fields']['phone_mobile']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_do_not_call.php

 // created: 2018-07-04 16:57:34
$dictionary['Contact']['fields']['do_not_call']['default']=false;
$dictionary['Contact']['fields']['do_not_call']['audited']=false;
$dictionary['Contact']['fields']['do_not_call']['massupdate']=false;
$dictionary['Contact']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Contact']['fields']['do_not_call']['reportable']=false;
$dictionary['Contact']['fields']['do_not_call']['unified_search']=false;
$dictionary['Contact']['fields']['do_not_call']['calculated']=false;
$dictionary['Contact']['fields']['do_not_call']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_assistant.php

 // created: 2018-07-04 16:57:57
$dictionary['Contact']['fields']['assistant']['audited']=false;
$dictionary['Contact']['fields']['assistant']['massupdate']=false;
$dictionary['Contact']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Contact']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant']['reportable']=false;
$dictionary['Contact']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant']['calculated']=false;
$dictionary['Contact']['fields']['assistant']['pii']=false;
$dictionary['Contact']['fields']['assistant']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_email.php

 // created: 2018-07-04 16:58:53
$dictionary['Contact']['fields']['email']['len']='100';
$dictionary['Contact']['fields']['email']['required']=true;
$dictionary['Contact']['fields']['email']['massupdate']=true;
$dictionary['Contact']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['email']['merge_filter']='disabled';
$dictionary['Contact']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.95',
  'searchable' => true,
);
$dictionary['Contact']['fields']['email']['calculated']=false;
$dictionary['Contact']['fields']['email']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_fax.php

 // created: 2018-07-04 18:11:36
$dictionary['Contact']['fields']['phone_fax']['len']='100';
$dictionary['Contact']['fields']['phone_fax']['audited']=false;
$dictionary['Contact']['fields']['phone_fax']['massupdate']=false;
$dictionary['Contact']['fields']['phone_fax']['comments']='Contact fax number';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_fax']['reportable']=false;
$dictionary['Contact']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.06',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_fax']['calculated']=false;
$dictionary['Contact']['fields']['phone_fax']['pii']=false;
$dictionary['Contact']['fields']['phone_fax']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_preferred_language.php

 // created: 2018-07-04 18:30:31
$dictionary['Contact']['fields']['preferred_language']['default']='en_us';
$dictionary['Contact']['fields']['preferred_language']['audited']=false;
$dictionary['Contact']['fields']['preferred_language']['massupdate']=true;
$dictionary['Contact']['fields']['preferred_language']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['preferred_language']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['preferred_language']['merge_filter']='disabled';
$dictionary['Contact']['fields']['preferred_language']['calculated']=false;
$dictionary['Contact']['fields']['preferred_language']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_facebook.php

 // created: 2018-07-04 18:31:21
$dictionary['Contact']['fields']['facebook']['audited']=false;
$dictionary['Contact']['fields']['facebook']['massupdate']=false;
$dictionary['Contact']['fields']['facebook']['comments']='The facebook name of the user';
$dictionary['Contact']['fields']['facebook']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['facebook']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Contact']['fields']['facebook']['reportable']=false;
$dictionary['Contact']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['facebook']['calculated']=false;
$dictionary['Contact']['fields']['facebook']['pii']=false;
$dictionary['Contact']['fields']['facebook']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_twitter.php

 // created: 2018-07-04 18:31:44
$dictionary['Contact']['fields']['twitter']['audited']=false;
$dictionary['Contact']['fields']['twitter']['massupdate']=false;
$dictionary['Contact']['fields']['twitter']['comments']='The twitter name of the user';
$dictionary['Contact']['fields']['twitter']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['twitter']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Contact']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['twitter']['calculated']=false;
$dictionary['Contact']['fields']['twitter']['pii']=false;
$dictionary['Contact']['fields']['twitter']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_googleplus.php

 // created: 2018-07-04 18:32:34
$dictionary['Contact']['fields']['googleplus']['audited']=false;
$dictionary['Contact']['fields']['googleplus']['massupdate']=false;
$dictionary['Contact']['fields']['googleplus']['comments']='The google plus id of the user';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['googleplus']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Contact']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['googleplus']['calculated']=false;
$dictionary['Contact']['fields']['googleplus']['pii']=false;
$dictionary['Contact']['fields']['googleplus']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_department.php

 // created: 2018-07-10 21:27:22
$dictionary['Contact']['fields']['department']['audited']=true;
$dictionary['Contact']['fields']['department']['massupdate']=false;
$dictionary['Contact']['fields']['department']['comments']='The department of the contact';
$dictionary['Contact']['fields']['department']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['department']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['department']['merge_filter']='disabled';
$dictionary['Contact']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['department']['calculated']=false;
$dictionary['Contact']['fields']['department']['importable']='false';
$dictionary['Contact']['fields']['department']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2018-07-11 20:48:00
$dictionary['Contact']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_postalcode']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_postalcode.php

 // created: 2018-07-11 20:52:46
$dictionary['Contact']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2018-07-13 21:10:57
$dictionary['Contact']['fields']['phone_home']['len']='20';
$dictionary['Contact']['fields']['phone_home']['required']=false;
$dictionary['Contact']['fields']['phone_home']['massupdate']=false;
$dictionary['Contact']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_home']['calculated']=false;
$dictionary['Contact']['fields']['phone_home']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_assistant_phone.php

 // created: 2018-07-13 21:24:04
$dictionary['Contact']['fields']['assistant_phone']['len']='20';
$dictionary['Contact']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Contact']['fields']['assistant_phone']['comments']='Phone number of the assistant of the contact';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Contact']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['assistant_phone']['calculated']=false;
$dictionary['Contact']['fields']['assistant_phone']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['assistant_phone']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_city.php

 // created: 2018-07-14 15:06:42
$dictionary['Contact']['fields']['alt_address_city']['required']=false;
$dictionary['Contact']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_city']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_city']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_country.php

 // created: 2018-07-14 15:07:01
$dictionary['Contact']['fields']['alt_address_country']['len']='255';
$dictionary['Contact']['fields']['alt_address_country']['required']=false;
$dictionary['Contact']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_country']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_country']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2018-07-14 15:07:34
$dictionary['Contact']['fields']['alt_address_state']['required']=false;
$dictionary['Contact']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_state']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_state']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_alt_address_street.php

 // created: 2018-07-14 15:07:53
$dictionary['Contact']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.32',
  'searchable' => true,
);
$dictionary['Contact']['fields']['alt_address_street']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_street']['rows']='4';
$dictionary['Contact']['fields']['alt_address_street']['cols']='20';
$dictionary['Contact']['fields']['alt_address_street']['required']=false;
$dictionary['Contact']['fields']['alt_address_street']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2018-07-18 15:00:35
$dictionary['Contact']['fields']['primary_address_street']['required']=false;
$dictionary['Contact']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.33',
  'searchable' => true,
);
$dictionary['Contact']['fields']['primary_address_street']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_street']['rows']='4';
$dictionary['Contact']['fields']['primary_address_street']['cols']='20';
$dictionary['Contact']['fields']['primary_address_street']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2018-08-31 23:46:30
$dictionary['Contact']['fields']['lead_source']['len']=100;
$dictionary['Contact']['fields']['lead_source']['required']=false;
$dictionary['Contact']['fields']['lead_source']['audited']=true;
$dictionary['Contact']['fields']['lead_source']['massupdate']=true;
$dictionary['Contact']['fields']['lead_source']['comments']='How did the contact come about';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Contact']['fields']['lead_source']['reportable']=true;
$dictionary['Contact']['fields']['lead_source']['calculated']=false;
$dictionary['Contact']['fields']['lead_source']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2018-09-12 21:53:48
$dictionary['Contact']['fields']['phone_work']['len']='100';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.08',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_work']['calculated']=false;
$dictionary['Contact']['fields']['phone_work']['audited']=false;
$dictionary['Contact']['fields']['phone_work']['importable']='false';
$dictionary['Contact']['fields']['phone_work']['reportable']=false;
$dictionary['Contact']['fields']['phone_work']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_other.php

 // created: 2018-09-12 21:54:41
$dictionary['Contact']['fields']['phone_other']['len']='100';
$dictionary['Contact']['fields']['phone_other']['massupdate']=false;
$dictionary['Contact']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_other']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.07',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_other']['calculated']=false;
$dictionary['Contact']['fields']['phone_other']['audited']=false;
$dictionary['Contact']['fields']['phone_other']['importable']='false';
$dictionary['Contact']['fields']['phone_other']['reportable']=false;
$dictionary['Contact']['fields']['phone_other']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_title.php

 // created: 2018-09-12 21:56:24
$dictionary['Contact']['fields']['title']['massupdate']=false;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['title']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';
$dictionary['Contact']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['title']['calculated']=false;
$dictionary['Contact']['fields']['title']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['title']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/custom_import_index.php

$dictionary['Contact']['indices'][] = array(
     'name' => 'idx_sasa_nroidentificacion_c',
     'type' => 'index',
     'fields' => array(
         'sasa_nroidentificacion_c',
     ),
     'source' => 'non-db',
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2019-03-20 09:02:14
$dictionary['Contact']['fields']['primary_address_city']['required']=true;
$dictionary['Contact']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_city']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2019-03-20 09:02:34
$dictionary['Contact']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_state']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_state']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_primary_address_country.php

 // created: 2019-03-20 09:02:58
$dictionary['Contact']['fields']['primary_address_country']['len']='255';
$dictionary['Contact']['fields']['primary_address_country']['required']=true;
$dictionary['Contact']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_country']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-02-03 20:54:36
$dictionary['Contact']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_description.php

 // created: 2020-09-17 16:49:30
$dictionary['Contact']['fields']['description']['audited']=false;
$dictionary['Contact']['fields']['description']['massupdate']=false;
$dictionary['Contact']['fields']['description']['comments']='Full text of the note';
$dictionary['Contact']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['description']['merge_filter']='disabled';
$dictionary['Contact']['fields']['description']['reportable']=false;
$dictionary['Contact']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.71',
  'searchable' => true,
);
$dictionary['Contact']['fields']['description']['calculated']=false;
$dictionary['Contact']['fields']['description']['rows']='6';
$dictionary['Contact']['fields']['description']['cols']='80';
$dictionary['Contact']['fields']['description']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['description']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['Contact']['fields']['account_name']['is_denormalized'] = true;
$dictionary['Contact']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['Contact']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['Contact']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['Contact']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['Contact']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['Contact']['fields']['denorm_account_name']['len'] = '150';
$dictionary['Contact']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['Contact']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['Contact']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Contact']['fields']['denorm_account_name']['audited'] = true;
$dictionary['Contact']['fields']['denorm_account_name']['required'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['importable'] = 'required';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Contact']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['Contact']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_merge'] = 'enabled';
$dictionary['Contact']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = '1';
$dictionary['Contact']['fields']['denorm_account_name']['calculated'] = false;
$dictionary['Contact']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Contact']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_razonsocial_c.php

 // created: 2022-09-09 21:58:07
$dictionary['Contact']['fields']['sasa_razonsocial_c']['labelValue']='Razon Social';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_razonsocial_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['dependency']='equal($sasa_tipopersona_c,"Juridica")';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/cases_contacts_1_Contacts.php

// created: 2022-10-18 19:53:20
$dictionary["Contact"]["fields"]["cases_contacts_1"] = array (
  'name' => 'cases_contacts_1',
  'type' => 'link',
  'relationship' => 'cases_contacts_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_CASES_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'cases_contacts_1cases_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["cases_contacts_1_name"] = array (
  'name' => 'cases_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CONTACTS_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_contacts_1cases_ida',
  'link' => 'cases_contacts_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["cases_contacts_1cases_ida"] = array (
  'name' => 'cases_contacts_1cases_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'cases_contacts_1cases_ida',
  'link' => 'cases_contacts_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_descripcioncontacto_c.php

 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_descripcioncontacto_c']['labelValue']='Descripción (eliminar)';
$dictionary['Contact']['fields']['sasa_descripcioncontacto_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_descripcioncontacto_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_descripcioncontacto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_departamento_c.php

 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_departamento_c']['labelValue']='Departamento';
$dictionary['Contact']['fields']['sasa_departamento_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_departamento_c']['visibility_grid']=array (
  'trigger' => 'sasa_pais_c',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'No Registra' => 
    array (
      0 => 'No Registra',
    ),
    'Colombia' => 
    array (
      0 => '',
      1 => 'Amazonas',
      2 => 'Antioquia',
      3 => 'Arauca',
      4 => 'Archipielago de San Andres Providencia y Santa Catalina',
      5 => 'Atlantico',
      6 => 'Bogota D.C.',
      7 => 'Bolivar',
      8 => 'Boyaca',
      9 => 'Caldas',
      10 => 'Caqueta',
      11 => 'Casanare',
      12 => 'Cauca',
      13 => 'Cesar',
      14 => 'Choco',
      15 => 'Cundinamarca',
      16 => 'Cordoba',
      17 => 'Guainia',
      18 => 'Guaviare',
      19 => 'Huila',
      20 => 'La Guajira',
      21 => 'Magdalena',
      22 => 'Meta',
      23 => 'Narino',
      24 => 'Norte de Santander',
      25 => 'Putumayo',
      26 => 'Quindio',
      27 => 'Risaralda',
      28 => 'Santander',
      29 => 'Sucre',
      30 => 'Tolima',
      31 => 'Valle del Cauca',
      32 => 'Vaupes',
      33 => 'Vichada',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_departamentointer_c.php

 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_departamentointer_c']['labelValue']='Departamento';
$dictionary['Contact']['fields']['sasa_departamentointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_departamentointer_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_departamentointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_condicionespecial_c.php

 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_condicionespecial_c']['labelValue']='Condición especial';
$dictionary['Contact']['fields']['sasa_condicionespecial_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_condicionespecial_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_condicionespecial_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_condicionespecial_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fondocesantias_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['labelValue']='Fondo de Cesantías';
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fondopensiones_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['labelValue']='Fondo de Pensiones';
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_estadocivil_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_estadocivil_c']['labelValue']='Estado Civil';
$dictionary['Contact']['fields']['sasa_estadocivil_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_estadocivil_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fechadeactualizacionsc_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fechadeactualizacionsc_c']['labelValue']='Fecha de Actualización SC';
$dictionary['Contact']['fields']['sasa_fechadeactualizacionsc_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fechadeactualizacionsc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fechademodificacionsc_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fechademodificacionsc_c']['labelValue']='Fecha de Modificación SC';
$dictionary['Contact']['fields']['sasa_fechademodificacionsc_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fechademodificacionsc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fechadevinculacionsc_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fechadevinculacionsc_c']['labelValue']='Fecha de Vinculación SC';
$dictionary['Contact']['fields']['sasa_fechadevinculacionsc_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fechadevinculacionsc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_genero_c.php

 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_genero_c']['labelValue']='Género';
$dictionary['Contact']['fields']['sasa_genero_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_genero_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_genero_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_genero_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_hobbiesaficionesdepor_c.php

 // created: 2022-11-16 02:16:40
$dictionary['Contact']['fields']['sasa_hobbiesaficionesdepor_c']['labelValue']='Hobbies/Aficiones/Deportes';
$dictionary['Contact']['fields']['sasa_hobbiesaficionesdepor_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_hobbiesaficionesdepor_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_hobbiesaficionesdepor_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_noenviarinfocliente_c.php

 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_noenviarinfocliente_c']['labelValue']='No Enviar Información al Cliente';
$dictionary['Contact']['fields']['sasa_noenviarinfocliente_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_noenviarinfocliente_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_infoquiererecibir_c.php

 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_infoquiererecibir_c']['labelValue']='Información de interés que quiere recibir';
$dictionary['Contact']['fields']['sasa_infoquiererecibir_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_infoquiererecibir_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipio_c.php

 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_municipio_c']['labelValue']='Ciudad';
$dictionary['Contact']['fields']['sasa_municipio_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_municipio_c']['visibility_grid']=array (
  'trigger' => 'sasa_departamento_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Amazonas' => 
    array (
      0 => '',
      1 => 'El Encanto',
      2 => 'La Chorrera',
      3 => 'La Pedrera',
      4 => 'La Victoria',
      5 => 'Leticia',
      6 => 'Miriti Parana',
      7 => 'Puerto Alegria',
      8 => 'Puerto Arica',
      9 => 'Puerto Narino',
      10 => 'Puerto Santander',
      11 => 'Tarapaca',
    ),
    'Antioquia' => 
    array (
      0 => '',
      1 => 'Abejorral',
      2 => 'Abriaqui',
      3 => 'Alejandria',
      4 => 'Amaga',
      5 => 'Amalfi',
      6 => 'Andes',
      7 => 'Angelopolis',
      8 => 'Angostura',
      9 => 'Anori',
      10 => 'Anza',
      11 => 'Apartado',
      12 => 'Arboletes',
      13 => 'Argelia',
      14 => 'Armenia',
      15 => 'Barbosa',
      16 => 'Bello',
      17 => 'Belmira',
      18 => 'Betania',
      19 => 'Betulia',
      20 => 'Briceno',
      21 => 'Buritica',
      22 => 'Caceres',
      23 => 'Caicedo',
      24 => 'Caldas',
      25 => 'Campamento',
      26 => 'Canasgordas',
      27 => 'Caracoli',
      28 => 'Caramanta',
      29 => 'Carepa',
      30 => 'Carolina',
      31 => 'Caucasia',
      32 => 'Chigorodo',
      33 => 'Cisneros',
      34 => 'Ciudad Bolivar',
      35 => 'Cocorna',
      36 => 'Concepcion',
      37 => 'Concordia',
      38 => 'Copacabana',
      39 => 'Dabeiba',
      40 => 'Don Matias',
      41 => 'Ebejico',
      42 => 'El Bagre',
      43 => 'El Carmen de Viboral',
      44 => 'El Santuario',
      45 => 'Entrerrios',
      46 => 'Envigado',
      47 => 'Fredonia',
      48 => 'Frontino',
      49 => 'Giraldo',
      50 => 'Girardota',
      51 => 'Gomez Plata',
      52 => 'Granada',
      53 => 'Guadalupe',
      54 => 'Guarne',
      55 => 'Guatape',
      56 => 'Heliconia',
      57 => 'Hispania',
      58 => 'Itagui',
      59 => 'Ituango',
      60 => 'Jardin',
      61 => 'Jerico',
      62 => 'La Ceja',
      63 => 'La Estrella',
      64 => 'La Pintada',
      65 => 'La Union',
      66 => 'Liborina',
      67 => 'Maceo',
      68 => 'Marinilla',
      69 => 'Medellin',
      70 => 'Montebello',
      71 => 'Murindo',
      72 => 'Mutata',
      73 => 'Narino',
      74 => 'Nechi',
      75 => 'Necocli',
      76 => 'Olaya',
      77 => 'Penol',
      78 => 'Peque',
      79 => 'Pueblorrico',
      80 => 'Puerto Berrio',
      81 => 'Puerto Nare',
      82 => 'Puerto Triunfo',
      83 => 'Remedios',
      84 => 'Retiro',
      85 => 'Rionegro',
      86 => 'Sabanalarga',
      87 => 'Sabaneta',
      88 => 'Salgar',
      89 => 'San Andres de Cuerquia',
      90 => 'San Carlos',
      91 => 'San Francisco',
      92 => 'San Jeronimo',
      93 => 'San Jose de La Montana',
      94 => 'San Juan de Uraba',
      95 => 'San Luis',
      96 => 'San Pedro',
      97 => 'San Pedro de Uraba',
      98 => 'San Rafael',
      99 => 'San Roque',
      100 => 'San Vicente',
      101 => 'Santa Barbara',
      102 => 'Santa Rosa de Osos',
      103 => 'Santafe de Antioquia',
      104 => 'Santo Domingo',
      105 => 'Segovia',
      106 => 'Sonson',
      107 => 'Sopetran',
      108 => 'Tamesis',
      109 => 'Taraza',
      110 => 'Tarso',
      111 => 'Titiribi',
      112 => 'Toledo',
      113 => 'Turbo',
      114 => 'Uramita',
      115 => 'Urrao',
      116 => 'Valdivia',
      117 => 'Valparaiso',
      118 => 'Vegachi',
      119 => 'Venecia',
      120 => 'Vigia del Fuerte',
      121 => 'Yali',
      122 => 'Yarumal',
      123 => 'Yolombo',
      124 => 'Yondo',
      125 => 'Zaragoza',
    ),
    'Arauca' => 
    array (
      0 => '',
      1 => 'Arauca',
      2 => 'Arauquita',
      3 => 'Cravo Norte',
      4 => 'Fortul',
      5 => 'Puerto Rondon',
      6 => 'Saravena',
      7 => 'Tame',
    ),
    'Archipielago de San Andres Providencia y Santa Catalina' => 
    array (
      0 => '',
      1 => 'Providencia',
      2 => 'San Andres',
    ),
    'Atlantico' => 
    array (
      0 => '',
      1 => 'Baranoa',
      2 => 'Barranquilla',
      3 => 'Campo de La Cruz',
      4 => 'Candelaria',
      5 => 'Galapa',
      6 => 'Juan de Acosta',
      7 => 'Luruaco',
      8 => 'Malambo',
      9 => 'Manati',
      10 => 'Palmar de Varela',
      11 => 'Piojo',
      12 => 'Polonuevo',
      13 => 'Ponedera',
      14 => 'Puerto Colombia',
      15 => 'Repelon',
      16 => 'Sabanagrande',
      17 => 'Sabanalarga',
      18 => 'Santa Lucia',
      19 => 'Santo Tomas',
      20 => 'Soledad',
      21 => 'Suan',
      22 => 'Tubara',
      23 => 'Usiacuri',
    ),
    'Bogota D.C.' => 
    array (
      0 => 'Bogota D.C.',
    ),
    'Bolivar' => 
    array (
      0 => '',
      1 => 'Achi',
      2 => 'Altos del Rosario',
      3 => 'Arenal',
      4 => 'Arjona',
      5 => 'Arroyohondo',
      6 => 'Barranco de Loba',
      7 => 'Calamar',
      8 => 'Cantagallo',
      9 => 'Cartagena',
      10 => 'Cicuco',
      11 => 'Clemencia',
      12 => 'Cordoba',
      13 => 'El Carmen de Bolivar',
      14 => 'El Guamo',
      15 => 'El Penon',
      16 => 'Hatillo de Loba',
      17 => 'Magangue',
      18 => 'Mahates',
      19 => 'Margarita',
      20 => 'Maria la Baja',
      21 => 'Mompos',
      22 => 'Montecristo',
      23 => 'Morales',
      24 => 'Norosi',
      25 => 'Pinillos',
      26 => 'Regidor',
      27 => 'Rio Viejo',
      28 => 'San Cristobal',
      29 => 'San Estanislao',
      30 => 'San Fernando',
      31 => 'San Jacinto',
      32 => 'San Jacinto del Cauca',
      33 => 'San Juan Nepomuceno',
      34 => 'San Martin de Loba',
      35 => 'San Pablo de Borbur',
      36 => 'Santa Catalina',
      37 => 'Santa Rosa',
      38 => 'Santa Rosa del Sur',
      39 => 'Simiti',
      40 => 'Soplaviento',
      41 => 'Talaigua Nuevo',
      42 => 'Tiquisio',
      43 => 'Turbaco',
      44 => 'Turbana',
      45 => 'Villanueva',
      46 => 'Zambrano',
    ),
    'Boyaca' => 
    array (
      0 => '',
      1 => 'Almeida',
      2 => 'Aquitania',
      3 => 'Arcabuco',
      4 => 'Belen',
      5 => 'Berbeo',
      6 => 'Beteitiva',
      7 => 'Boavita',
      8 => 'Boyaca',
      9 => 'Briceno',
      10 => 'Buena Vista',
      11 => 'Busbanza',
      12 => 'Caldas',
      13 => 'Campohermoso',
      14 => 'Cerinza',
      15 => 'Chinavita',
      16 => 'Chiquinquira',
      17 => 'Chiquiza',
      18 => 'Chiscas',
      19 => 'Chita',
      20 => 'Chitaraque',
      21 => 'Chivata',
      22 => 'Chivor',
      23 => 'Cienega',
      24 => 'Combita',
      25 => 'Coper',
      26 => 'Corrales',
      27 => 'Covarachia',
      28 => 'Cubara',
      29 => 'Cucaita',
      30 => 'Cuitiva',
      31 => 'Duitama',
      32 => 'El Cocuy',
      33 => 'El Espino',
      34 => 'Firavitoba',
      35 => 'Floresta',
      36 => 'Gachantiva',
      37 => 'Gameza',
      38 => 'Garagoa',
      39 => 'Guacamayas',
      40 => 'Guateque',
      41 => 'Guayata',
      42 => 'Güican',
      43 => 'Iza',
      44 => 'Jenesano',
      45 => 'Jerico',
      46 => 'La Capilla',
      47 => 'La Uvita',
      48 => 'La Victoria',
      49 => 'Labranzagrande',
      50 => 'Macanal',
      51 => 'Maripi',
      52 => 'Miraflores',
      53 => 'Mongua',
      54 => 'Mongui',
      55 => 'Moniquira',
      56 => 'Motavita',
      57 => 'Muzo',
      58 => 'Nobsa',
      59 => 'Nuevo Colon',
      60 => 'Oicata',
      61 => 'Otanche',
      62 => 'Pachavita',
      63 => 'Paez',
      64 => 'Paipa',
      65 => 'Pajarito',
      66 => 'Panqueba',
      67 => 'Pauna',
      68 => 'Paya',
      69 => 'Paz de Rio',
      70 => 'Pesca',
      71 => 'Pisba',
      72 => 'Puerto Boyaca',
      73 => 'Quipama',
      74 => 'Ramiriqui',
      75 => 'Raquira',
      76 => 'Rondon',
      77 => 'Saboya',
      78 => 'Sachica',
      79 => 'Samaca',
      80 => 'San Eduardo',
      81 => 'San Jose de Pare',
      82 => 'San Luis de Gaceno',
      83 => 'San Mateo',
      84 => 'San Miguel de Sema',
      85 => 'San Pablo de Borbur',
      86 => 'Santa Maria',
      87 => 'Santa Rosa de Viterbo',
      88 => 'Santa Sofia',
      89 => 'Santana',
      90 => 'Sativanorte',
      91 => 'Sativasur',
      92 => 'Siachoque',
      93 => 'Soata',
      94 => 'Socha',
      95 => 'Socota',
      96 => 'Sogamoso',
      97 => 'Somondoco',
      98 => 'Sora',
      99 => 'Soraca',
      100 => 'Sotaquira',
      101 => 'Susacon',
      102 => 'Sutamarchan',
      103 => 'Sutatenza',
      104 => 'Tasco',
      105 => 'Tenza',
      106 => 'Tibana',
      107 => 'Tibasosa',
      108 => 'Tinjaca',
      109 => 'Tipacoque',
      110 => 'Toca',
      111 => 'Togüi',
      112 => 'Topaga',
      113 => 'Tota',
      114 => 'Tunja',
      115 => 'Tunungua',
      116 => 'Turmeque',
      117 => 'Tuta',
      118 => 'Tutaza',
      119 => 'Umbita',
      120 => 'Ventaquemada',
      121 => 'Villa de Leyva',
      122 => 'Viracacha',
      123 => 'Zetaquira',
    ),
    'Caldas' => 
    array (
      0 => '',
      1 => 'Aguadas',
      2 => 'Anserma',
      3 => 'Aranzazu',
      4 => 'Belalcazar',
      5 => 'Chinchina',
      6 => 'Filadelfia',
      7 => 'La Dorada',
      8 => 'La Merced',
      9 => 'Manizales',
      10 => 'Manzanares',
      11 => 'Marmato',
      12 => 'Marquetalia',
      13 => 'Marulanda',
      14 => 'Neira',
      15 => 'Norcasia',
      16 => 'Pacora',
      17 => 'Palestina',
      18 => 'Pensilvania',
      19 => 'Riosucio',
      20 => 'Risaralda',
      21 => 'Salamina',
      22 => 'Samana',
      23 => 'San Jose',
      24 => 'Supia',
      25 => 'Victoria',
      26 => 'Villamaria',
      27 => 'Viterbo',
    ),
    'Caqueta' => 
    array (
      0 => '',
      1 => 'Albania',
      2 => 'Belen de Los Andaquies',
      3 => 'Cartagena del Chaira',
      4 => 'Curillo',
      5 => 'El Doncello',
      6 => 'El Paujil',
      7 => 'Florencia',
      8 => 'La Montanita',
      9 => 'Milan',
      10 => 'Morelia',
      11 => 'Puerto Rico',
      12 => 'San Jose del Fragua',
      13 => 'San Vicente del Caguan',
      14 => 'Solano',
      15 => 'Solita',
      16 => 'Valparaiso',
    ),
    'Casanare' => 
    array (
      0 => '',
      1 => 'Aguazul',
      2 => 'Chameza',
      3 => 'Hato Corozal',
      4 => 'La Salina',
      5 => 'Mani',
      6 => 'Monterrey',
      7 => 'Nunchia',
      8 => 'Orocue',
      9 => 'Paz de Ariporo',
      10 => 'Pore',
      11 => 'Recetor',
      12 => 'Sabanalarga',
      13 => 'Sacama',
      14 => 'San Luis de Gaceno',
      15 => 'Tamara',
      16 => 'Tauramena',
      17 => 'Trinidad',
      18 => 'Villanueva',
      19 => 'Yopal',
    ),
    'Cauca' => 
    array (
      0 => '',
      1 => 'Almaguer',
      2 => 'Argelia',
      3 => 'Balboa',
      4 => 'Bolivar',
      5 => 'Buenos Aires',
      6 => 'Cajibio',
      7 => 'Caldono',
      8 => 'Caloto',
      9 => 'Corinto',
      10 => 'El Tambo',
      11 => 'Florencia',
      12 => 'Guachene',
      13 => 'Guapi',
      14 => 'Inza',
      15 => 'Jambalo',
      16 => 'La Sierra',
      17 => 'La Vega',
      18 => 'Lopez',
      19 => 'Mercaderes',
      20 => 'Miranda',
      21 => 'Morales',
      22 => 'Padilla',
      23 => 'Paez',
      24 => 'Patia',
      25 => 'Piamonte',
      26 => 'Piendamo',
      27 => 'Popayan',
      28 => 'Puerto Tejada',
      29 => 'Purace',
      30 => 'Rosas',
      31 => 'San Sebastian',
      32 => 'Santa Rosa',
      33 => 'Santander de Quilichao',
      34 => 'Silvia',
      35 => 'Sotara',
      36 => 'Suarez',
      37 => 'Sucre',
      38 => 'Timbio',
      39 => 'Timbiqui',
      40 => 'Toribio',
      41 => 'Totoro',
      42 => 'Villa Rica',
    ),
    'Cesar' => 
    array (
      0 => '',
      1 => 'Aguachica',
      2 => 'Agustin Codazzi',
      3 => 'Astrea',
      4 => 'Becerril',
      5 => 'Bosconia',
      6 => 'Chimichagua',
      7 => 'Chiriguana',
      8 => 'Curumani',
      9 => 'El Copey',
      10 => 'El Paso',
      11 => 'Gamarra',
      12 => 'Gonzalez',
      13 => 'La Gloria',
      14 => 'La Jagua de Ibirico',
      15 => 'La Paz',
      16 => 'Manaure',
      17 => 'Pailitas',
      18 => 'Pelaya',
      19 => 'Pueblo Bello',
      20 => 'Rio de Oro',
      21 => 'San Alberto',
      22 => 'San Diego',
      23 => 'San Martin',
      24 => 'Tamalameque',
      25 => 'Valledupar',
    ),
    'Choco' => 
    array (
      0 => '',
      1 => 'Acandi',
      2 => 'Alto Baudo',
      3 => 'Atrato',
      4 => 'Bagado',
      5 => 'Bahia Solano',
      6 => 'Bajo Baudo',
      7 => 'Belen de Bajira',
      8 => 'Bojaya',
      9 => 'Carmen del Darien',
      10 => 'Certegui',
      11 => 'Condoto',
      12 => 'El Canton del San Pablo',
      13 => 'El Carmen de Atrato',
      14 => 'El Litoral del San Juan',
      15 => 'Istmina',
      16 => 'Jurado',
      17 => 'Lloro',
      18 => 'Medio Atrato',
      19 => 'Medio Baudo',
      20 => 'Medio San Juan',
      21 => 'Novita',
      22 => 'Nuqui',
      23 => 'Quibdo',
      24 => 'Rio Iro',
      25 => 'Rio Quito',
      26 => 'Riosucio',
      27 => 'San Jose del Palmar',
      28 => 'Sipi',
      29 => 'Tado',
      30 => 'Unguia',
      31 => 'Union Panamericana',
    ),
    'Cordoba' => 
    array (
      0 => '',
      1 => 'Ayapel',
      2 => 'Buenavista',
      3 => 'Canalete',
      4 => 'Cerete',
      5 => 'Chima',
      6 => 'Chinu',
      7 => 'Cienaga de Oro',
      8 => 'Cotorra',
      9 => 'La Apartada',
      10 => 'Lorica',
      11 => 'Los Cordobas',
      12 => 'Momil',
      13 => 'Monitos',
      14 => 'Montelibano',
      15 => 'Monteria',
      16 => 'Planeta Rica',
      17 => 'Pueblo Nuevo',
      18 => 'Puerto Escondido',
      19 => 'Puerto Libertador',
      20 => 'Purisima',
      21 => 'Sahagun',
      22 => 'San Andres Sotavento',
      23 => 'San Antero',
      24 => 'San Bernardo del Viento',
      25 => 'San Carlos',
      26 => 'San Jose de Ure',
      27 => 'San Pelayo',
      28 => 'Tierralta',
      29 => 'Tuchin',
      30 => 'Valencia',
    ),
    'Cundinamarca' => 
    array (
      0 => '',
      1 => 'Agua de Dios',
      2 => 'Alban',
      3 => 'Anapoima',
      4 => 'Anolaima',
      5 => 'Apulo',
      6 => 'Arbelaez',
      7 => 'Beltran',
      8 => 'Bituima',
      9 => 'Bojaca',
      10 => 'Cabrera',
      11 => 'Cachipay',
      12 => 'Cajica',
      13 => 'Caparrapi',
      14 => 'Caqueza',
      15 => 'Carmen de Carupa',
      16 => 'Chaguani',
      17 => 'Chia',
      18 => 'Chipaque',
      19 => 'Choachi',
      20 => 'Choconta',
      21 => 'Cogua',
      22 => 'Cota',
      23 => 'Cucunuba',
      24 => 'El Colegio',
      25 => 'El Penon',
      26 => 'El Rosal',
      27 => 'Facatativa',
      28 => 'Fomeque',
      29 => 'Fosca',
      30 => 'Funza',
      31 => 'Fuquene',
      32 => 'Fusagasuga',
      33 => 'Gachala',
      34 => 'Gachancipa',
      35 => 'Gacheta',
      36 => 'Gama',
      37 => 'Girardot',
      38 => 'Granada',
      39 => 'Guacheta',
      40 => 'Guaduas',
      41 => 'Guasca',
      42 => 'Guataqui',
      43 => 'Guatavita',
      44 => 'Guayabal de Siquima',
      45 => 'Guayabetal',
      46 => 'Gutierrez',
      47 => 'Jerusalen',
      48 => 'Junin',
      49 => 'La Calera',
      50 => 'La Mesa',
      51 => 'La Palma',
      52 => 'La Pena',
      53 => 'La Vega',
      54 => 'Lenguazaque',
      55 => 'Macheta',
      56 => 'Madrid',
      57 => 'Manta',
      58 => 'Medina',
      59 => 'Mosquera',
      60 => 'Narino',
      61 => 'Nemocon',
      62 => 'Nilo',
      63 => 'Nimaima',
      64 => 'Nocaima',
      65 => 'Pacho',
      66 => 'Paime',
      67 => 'Pandi',
      68 => 'Paratebueno',
      69 => 'Pasca',
      70 => 'Puerto Salgar',
      71 => 'Puli',
      72 => 'Quebradanegra',
      73 => 'Quetame',
      74 => 'Quipile',
      75 => 'Ricaurte',
      76 => 'San Antonio del Tequendama',
      77 => 'San Bernardo',
      78 => 'San Cayetano',
      79 => 'San Francisco',
      80 => 'San Juan de Rio Seco',
      81 => 'Sasaima',
      82 => 'Sesquile',
      83 => 'Sibate',
      84 => 'Silvania',
      85 => 'Simijaca',
      86 => 'Soacha',
      87 => 'Sopo',
      88 => 'Subachoque',
      89 => 'Suesca',
      90 => 'Supata',
      91 => 'Susa',
      92 => 'Sutatausa',
      93 => 'Tabio',
      94 => 'Tausa',
      95 => 'Tena',
      96 => 'Tenjo',
      97 => 'Tibacuy',
      98 => 'Tibirita',
      99 => 'Tocaima',
      100 => 'Tocancipa',
      101 => 'Topaipi',
      102 => 'Ubala',
      103 => 'Ubaque',
      104 => 'Une',
      105 => 'utica',
      106 => 'Venecia',
      107 => 'Vergara',
      108 => 'Viani',
      109 => 'Villa de San Diego de Ubate',
      110 => 'Villagomez',
      111 => 'Villapinzon',
      112 => 'Villeta',
      113 => 'Viota',
      114 => 'Yacopi',
      115 => 'Zipacon',
      116 => 'Zipaquira',
    ),
    'Guainia' => 
    array (
      0 => '',
      1 => 'Barranco Minas',
      2 => 'Cacahual',
      3 => 'Inirida',
      4 => 'La Guadalupe',
      5 => 'Mapiripana',
      6 => 'Morichal',
      7 => 'Pana Pana',
      8 => 'Puerto Colombia',
      9 => 'San Felipe',
    ),
    'Guaviare' => 
    array (
      0 => '',
      1 => 'Calamar',
      2 => 'El Retorno',
      3 => 'Miraflores',
      4 => 'San Jose del Guaviare',
    ),
    'Huila' => 
    array (
      0 => '',
      1 => 'Acevedo',
      2 => 'Agrado',
      3 => 'Aipe',
      4 => 'Algeciras',
      5 => 'Altamira',
      6 => 'Baraya',
      7 => 'Campoalegre',
      8 => 'Colombia',
      9 => 'Elias',
      10 => 'Garzon',
      11 => 'Gigante',
      12 => 'Guadalupe',
      13 => 'Hobo',
      14 => 'Iquira',
      15 => 'Isnos',
      16 => 'La Argentina',
      17 => 'La Plata',
      18 => 'Nataga',
      19 => 'Neiva',
      20 => 'Oporapa',
      21 => 'Paicol',
      22 => 'Palermo',
      23 => 'Palestina',
      24 => 'Pital',
      25 => 'Pitalito',
      26 => 'Rivera',
      27 => 'Saladoblanco',
      28 => 'San Agustin',
      29 => 'Santa Maria',
      30 => 'Suaza',
      31 => 'Tarqui',
      32 => 'Tello',
      33 => 'Teruel',
      34 => 'Tesalia',
      35 => 'Timana',
      36 => 'Villavieja',
      37 => 'Yaguara',
    ),
    'La Guajira' => 
    array (
      0 => '',
      1 => 'Albania',
      2 => 'Barrancas',
      3 => 'Dibula',
      4 => 'Distraccion',
      5 => 'El Molino',
      6 => 'Fonseca',
      7 => 'Hatonuevo',
      8 => 'La Jagua del Pilar',
      9 => 'Maicao',
      10 => 'Manaure',
      11 => 'Riohacha',
      12 => 'San Juan del Cesar',
      13 => 'Uribia',
      14 => 'Urumita',
      15 => 'Villanueva',
    ),
    'Magdalena' => 
    array (
      0 => '',
      1 => 'Algarrobo',
      2 => 'Aracataca',
      3 => 'Ariguani',
      4 => 'Cerro San Antonio',
      5 => 'Chivolo',
      6 => 'Cienaga',
      7 => 'Concordia',
      8 => 'El Banco',
      9 => 'El Pinon',
      10 => 'El Reten',
      11 => 'Fundacion',
      12 => 'Guamal',
      13 => 'Nueva Granada',
      14 => 'Pedraza',
      15 => 'Pijino del Carmen',
      16 => 'Pivijay',
      17 => 'Plato',
      18 => 'Pueblo Viejo',
      19 => 'Remolino',
      20 => 'Sabanas de San Angel',
      21 => 'Salamina',
      22 => 'San Sebastian de Buenavista',
      23 => 'San Zenon',
      24 => 'Santa Ana',
      25 => 'Santa Barbara de Pinto',
      26 => 'Santa Marta',
      27 => 'Sitionuevo',
      28 => 'Tenerife',
      29 => 'Zapayan',
      30 => 'Zona Bananera',
    ),
    'Meta' => 
    array (
      0 => '',
      1 => 'Acacias',
      2 => 'Barranca de Upia',
      3 => 'Cabuyaro',
      4 => 'Castilla la Nueva',
      5 => 'Cubarral',
      6 => 'Cumaral',
      7 => 'El Calvario',
      8 => 'El Castillo',
      9 => 'El Dorado',
      10 => 'Fuente de Oro',
      11 => 'Granada',
      12 => 'Guamal',
      13 => 'La Macarena',
      14 => 'Lejanias',
      15 => 'Mapiripan',
      16 => 'Mesetas',
      17 => 'Puerto Concordia',
      18 => 'Puerto Gaitan',
      19 => 'Puerto Lleras',
      20 => 'Puerto Lopez',
      21 => 'Puerto Rico',
      22 => 'Restrepo',
      23 => 'San Carlos de Guaroa',
      24 => 'San Juan de Arama',
      25 => 'San Juanito',
      26 => 'San Martin',
      27 => 'Uribe',
      28 => 'Villavicencio',
      29 => 'Vista Hermosa',
    ),
    'Narino' => 
    array (
      0 => '',
      1 => 'Alban',
      2 => 'Aldana',
      3 => 'Ancuya',
      4 => 'Arboleda',
      5 => 'Barbacoas',
      6 => 'Belen',
      7 => 'Buesaco',
      8 => 'Chachagüi',
      9 => 'Colon',
      10 => 'Consaca',
      11 => 'Contadero',
      12 => 'Cordoba',
      13 => 'Cuaspud',
      14 => 'Cumbal',
      15 => 'Cumbitara',
      16 => 'El Charco',
      17 => 'El Penol',
      18 => 'El Rosario',
      19 => 'El Tablon de Gomez',
      20 => 'El Tambo',
      21 => 'Francisco Pizarro',
      22 => 'Funes',
      23 => 'Guachucal',
      24 => 'Guaitarilla',
      25 => 'Gualmatan',
      26 => 'Iles',
      27 => 'Imues',
      28 => 'Ipiales',
      29 => 'La Cruz',
      30 => 'La Florida',
      31 => 'La Llanada',
      32 => 'La Tola',
      33 => 'La Union',
      34 => 'Leiva',
      35 => 'Linares',
      36 => 'Los Andes',
      37 => 'Magüi',
      38 => 'Mallama',
      39 => 'Mosquera',
      40 => 'Narino',
      41 => 'Olaya Herrera',
      42 => 'Ospina',
      43 => 'Pasto',
      44 => 'Policarpa',
      45 => 'Potosi',
      46 => 'Providencia',
      47 => 'Puerres',
      48 => 'Pupiales',
      49 => 'Ricaurte',
      50 => 'Roberto Payan',
      51 => 'Samaniego',
      52 => 'San Andres de Tumaco',
      53 => 'San Bernardo',
      54 => 'San Lorenzo',
      55 => 'San Pablo',
      56 => 'San Pedro de Cartago',
      57 => 'Sandona',
      58 => 'Santa Barbara',
      59 => 'Santacruz',
      60 => 'Sapuyes',
      61 => 'Taminango',
      62 => 'Tangua',
      63 => 'Tuquerres',
      64 => 'Yacuanquer',
    ),
    'Norte de Santander' => 
    array (
      0 => '',
      1 => 'Abrego',
      2 => 'Arboledas',
      3 => 'Bochalema',
      4 => 'Bucarasica',
      5 => 'Cachira',
      6 => 'Cacota',
      7 => 'Chinacota',
      8 => 'Chitaga',
      9 => 'Convencion',
      10 => 'Cucuta',
      11 => 'Cucutilla',
      12 => 'Durania',
      13 => 'El Carmen',
      14 => 'El Tarra',
      15 => 'El Zulia',
      16 => 'Gramalote',
      17 => 'Hacari',
      18 => 'Herran',
      19 => 'La Esperanza',
      20 => 'La Playa',
      21 => 'Labateca',
      22 => 'Los Patios',
      23 => 'Lourdes',
      24 => 'Mutiscua',
      25 => 'Ocana',
      26 => 'Pamplona',
      27 => 'Pamplonita',
      28 => 'Puerto Santander',
      29 => 'Ragonvalia',
      30 => 'Salazar',
      31 => 'San Calixto',
      32 => 'San Cayetano',
      33 => 'Santiago',
      34 => 'Sardinata',
      35 => 'Silos',
      36 => 'Teorama',
      37 => 'Tibu',
      38 => 'Toledo',
      39 => 'Villa Caro',
      40 => 'Villa del Rosario',
    ),
    'Putumayo' => 
    array (
      0 => '',
      1 => 'Colon',
      2 => 'Leguizamo',
      3 => 'Mocoa',
      4 => 'Orito',
      5 => 'Puerto Asis',
      6 => 'Puerto Caicedo',
      7 => 'Puerto Guzman',
      8 => 'San Francisco',
      9 => 'San Miguel',
      10 => 'Santiago',
      11 => 'Sibundoy',
      12 => 'Valle de Guamez',
      13 => 'Villagarzon',
    ),
    'Quindio' => 
    array (
      0 => '',
      1 => 'Armenia',
      2 => 'Buenavista',
      3 => 'Calarca',
      4 => 'Circasia',
      5 => 'Cordoba',
      6 => 'Filandia',
      7 => 'Genova',
      8 => 'La Tebaida',
      9 => 'Montenegro',
      10 => 'Pijao',
      11 => 'Quimbaya',
      12 => 'Salento',
    ),
    'Risaralda' => 
    array (
      0 => '',
      1 => 'Apia',
      2 => 'Balboa',
      3 => 'Belen de Umbria',
      4 => 'Dosquebradas',
      5 => 'Guatica',
      6 => 'La Celia',
      7 => 'La Virginia',
      8 => 'Marsella',
      9 => 'Mistrato',
      10 => 'Pereira',
      11 => 'Pueblo Rico',
      12 => 'Quinchia',
      13 => 'Santa Rosa de Cabal',
      14 => 'Santuario',
    ),
    'Santander' => 
    array (
      0 => '',
      1 => 'Aguada',
      2 => 'Albania',
      3 => 'Aratoca',
      4 => 'Barbosa',
      5 => 'Barichara',
      6 => 'Barrancabermeja',
      7 => 'Betulia',
      8 => 'Bolivar',
      9 => 'Bucaramanga',
      10 => 'Cabrera',
      11 => 'California',
      12 => 'Capitanejo',
      13 => 'Carcasi',
      14 => 'Cepita',
      15 => 'Cerrito',
      16 => 'Charala',
      17 => 'Charta',
      18 => 'Chima',
      19 => 'Chipata',
      20 => 'Cimitarra',
      21 => 'Concepcion',
      22 => 'Confines',
      23 => 'Contratacion',
      24 => 'Coromoro',
      25 => 'Curiti',
      26 => 'El Carmen de Chucuri',
      27 => 'El Guacamayo',
      28 => 'El Penon',
      29 => 'El Playon',
      30 => 'Encino',
      31 => 'Enciso',
      32 => 'Florian',
      33 => 'Floridablanca',
      34 => 'Galan',
      35 => 'Gambita',
      36 => 'Giron',
      37 => 'Guaca',
      38 => 'Guadalupe',
      39 => 'Guapota',
      40 => 'Guavata',
      41 => 'Güepsa',
      42 => 'Hato',
      43 => 'Jesus Maria',
      44 => 'Jordan',
      45 => 'La Belleza',
      46 => 'La Paz',
      47 => 'Landazuri',
      48 => 'Lebrija',
      49 => 'Los Santos',
      50 => 'Macaravita',
      51 => 'Malaga',
      52 => 'Matanza',
      53 => 'Mogotes',
      54 => 'Molagavita',
      55 => 'Ocamonte',
      56 => 'Oiba',
      57 => 'Onzaga',
      58 => 'Palmar',
      59 => 'Palmas del Socorro',
      60 => 'Paramo',
      61 => 'Piedecuesta',
      62 => 'Pinchote',
      63 => 'Puente Nacional',
      64 => 'Puerto Parra',
      65 => 'Puerto Wilches',
      66 => 'Rionegro',
      67 => 'Sabana de Torres',
      68 => 'San Andres',
      69 => 'San Benito',
      70 => 'San Gil',
      71 => 'San Joaquin',
      72 => 'San Jose de Miranda',
      73 => 'San Miguel',
      74 => 'San Vicente de Chucuri',
      75 => 'Santa Barbara',
      76 => 'Santa Helena del Opon',
      77 => 'Simacota',
      78 => 'Socorro',
      79 => 'Suaita',
      80 => 'Sucre',
      81 => 'Surata',
      82 => 'Tona',
      83 => 'Valle de San Jose',
      84 => 'Velez',
      85 => 'Vetas',
      86 => 'Villanueva',
      87 => 'Zapatoca',
    ),
    'Sucre' => 
    array (
      0 => '',
      1 => 'Buenavista',
      2 => 'Caimito',
      3 => 'Chalan',
      4 => 'Coloso',
      5 => 'Corozal',
      6 => 'Covenas',
      7 => 'El Roble',
      8 => 'Galeras',
      9 => 'Guaranda',
      10 => 'La Union',
      11 => 'Los Palmitos',
      12 => 'Majagual',
      13 => 'Morroa',
      14 => 'Ovejas',
      15 => 'Palmito',
      16 => 'Sampues',
      17 => 'San Benito Abad',
      18 => 'San Juan de Betulia',
      19 => 'San Luis de Since',
      20 => 'San Marcos',
      21 => 'San Onofre',
      22 => 'San Pedro',
      23 => 'Santiago de Tolu',
      24 => 'Sincelejo',
      25 => 'Sucre',
      26 => 'Tolu Viejo',
    ),
    'Tolima' => 
    array (
      0 => '',
      1 => 'Alpujarra',
      2 => 'Alvarado',
      3 => 'Ambalema',
      4 => 'Anzoategui',
      5 => 'Armero',
      6 => 'Ataco',
      7 => 'Cajamarca',
      8 => 'Carmen de Apicala',
      9 => 'Casabianca',
      10 => 'Chaparral',
      11 => 'Coello',
      12 => 'Coyaima',
      13 => 'Cunday',
      14 => 'Dolores',
      15 => 'Espinal',
      16 => 'Falan',
      17 => 'Flandes',
      18 => 'Fresno',
      19 => 'Guamo',
      20 => 'Herveo',
      21 => 'Honda',
      22 => 'Ibague',
      23 => 'Icononzo',
      24 => 'Lerida',
      25 => 'Libano',
      26 => 'Mariquita',
      27 => 'Melgar',
      28 => 'Murillo',
      29 => 'Natagaima',
      30 => 'Ortega',
      31 => 'Palocabildo',
      32 => 'Piedras',
      33 => 'Planadas',
      34 => 'Prado',
      35 => 'Purificacion',
      36 => 'Rio Blanco',
      37 => 'Roncesvalles',
      38 => 'Rovira',
      39 => 'Saldana',
      40 => 'San Antonio',
      41 => 'San Luis',
      42 => 'Santa Isabel',
      43 => 'Suarez',
      44 => 'Valle de San Juan',
      45 => 'Venadillo',
      46 => 'Villahermosa',
      47 => 'Villarrica',
    ),
    'Valle del Cauca' => 
    array (
      0 => '',
      1 => 'Alcala',
      2 => 'Andalucia',
      3 => 'Ansermanuevo',
      4 => 'Argelia',
      5 => 'Bolivar',
      6 => 'Buenaventura',
      7 => 'Bugalagrande',
      8 => 'Caicedonia',
      9 => 'Cali',
      10 => 'Calima',
      11 => 'Candelaria',
      12 => 'Cartago',
      13 => 'Dagua',
      14 => 'El aguila',
      15 => 'El Cairo',
      16 => 'El Cerrito',
      17 => 'El Dovio',
      18 => 'Florida',
      19 => 'Ginebra',
      20 => 'Guacari',
      21 => 'Guadalajara de Buga',
      22 => 'Jamundi',
      23 => 'La Cumbre',
      24 => 'La Union',
      25 => 'La Victoria',
      26 => 'Obando',
      27 => 'Palmira',
      28 => 'Pradera',
      29 => 'Restrepo',
      30 => 'Riofrio',
      31 => 'Roldanillo',
      32 => 'San Pedro',
      33 => 'Sevilla',
      34 => 'Toro',
      35 => 'Trujillo',
      36 => 'Tulua',
      37 => 'Ulloa',
      38 => 'Versalles',
      39 => 'Vijes',
      40 => 'Yotoco',
      41 => 'Yumbo',
      42 => 'Zarzal',
    ),
    'Vaupes' => 
    array (
      0 => '',
      1 => 'Caruru',
      2 => 'Mitu',
      3 => 'Pacoa',
      4 => 'Papunaua',
      5 => 'Taraira',
      6 => 'Yavarate',
    ),
    'Vichada' => 
    array (
      0 => '',
      1 => 'Cumaribo',
      2 => 'La Primavera',
      3 => 'Puerto Carreno',
      4 => 'Santa Rosalia',
    ),
    'No Registra' => 
    array (
      0 => 'No Registra',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipiointer_c.php

 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_municipiointer_c']['labelValue']='Ciudad';
$dictionary['Contact']['fields']['sasa_municipiointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_municipiointer_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_municipiointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_lgbtiq_c.php

 // created: 2022-11-16 02:16:41
$dictionary['Contact']['fields']['sasa_lgbtiq_c']['labelValue']='LGBTIQ+';
$dictionary['Contact']['fields']['sasa_lgbtiq_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_lgbtiq_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_lgbtiq_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_lgbtiq_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_quienrefirio_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['labelValue']='Quien Refirió?';
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['enforced']='false';
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['dependency']='equal($lead_source,"Word of mouth")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_periodicidad_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_periodicidad_c']['labelValue']='Periodicidad';
$dictionary['Contact']['fields']['sasa_periodicidad_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_periodicidad_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_relacioncuenta_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['labelValue']='Relación con la Cuenta';
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['dependency']='and(equal(related($accounts,"sasa_tipopersona_c"),"Juridica"),
equal($sasa_parentescorelacion_c,"Otro")
)';
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nrohijos_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_nrohijos_c']['labelValue']='Número de Hijos';
$dictionary['Contact']['fields']['sasa_nrohijos_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_nrohijos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipocontacto_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_tipocontacto_c']['labelValue']='Tipo de Contacto (eliminar)';
$dictionary['Contact']['fields']['sasa_tipocontacto_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipocontacto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_telefono_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_telefono_c']['labelValue']='Teléfono';
$dictionary['Contact']['fields']['sasa_telefono_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_telefono_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_telefono_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_telefono2_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_telefono2_c']['labelValue']='Teléfono 2';
$dictionary['Contact']['fields']['sasa_telefono2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_telefono2_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_telefono2_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_parentescorelacion_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_parentescorelacion_c']['labelValue']='Parentesco/Relación';
$dictionary['Contact']['fields']['sasa_parentescorelacion_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_parentescorelacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nroidentificacion_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['labelValue']='Número de ID';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['required_formula']='equal($sasa_pqrs_c,"Si")';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_pais_c.php

 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_pais_c']['labelValue']='País';
$dictionary['Contact']['fields']['sasa_pais_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_pais_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_pais_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_pais_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipoinfoperiodicidad_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_tipoinfoperiodicidad_c']['labelValue']='Tipo de Información y Periodicidad';
$dictionary['Contact']['fields']['sasa_tipoinfoperiodicidad_c']['visibility_grid']=array (
  'trigger' => 'sasa_infoquiererecibir_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Renta Fija' => 
    array (
      0 => 'English man en Bogota trimestral',
      1 => 'Tenedores de TES mensual',
      2 => 'Emisiones corporativas deuda privada trimestral',
      3 => 'Informe del PEI mensual',
    ),
    'Renta Variable' => 
    array (
      0 => 'Mensual y flujos de acciones mensual',
      1 => 'Informes corporativos trimestral',
      2 => 'Rebalanceo del indice COLCAP trimestral',
      3 => 'Pasajeros de Avianca mensual',
    ),
    'Saturday Live' => 
    array (
      0 => 'Saturday Live Semanal los sabados',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipoidentificacion_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['labelValue']='Tipo de ID';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['required_formula']='equal($sasa_pqrs_c,"Si")';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_apellidom4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_apellidom4_c']['labelValue']='Cuenta (Apellido) - M4';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_apellidom4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_apellidom4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_fechanacm4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_fechanacm4_c']['labelValue']='Fecha de Nacimiento - M4';
$dictionary['Contact']['fields']['sasa_fechanacm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fechanacm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_fechanacm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_fechanacm4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_correom4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_correom4_c']['labelValue']='Correo Electrónico - M4';
$dictionary['Contact']['fields']['sasa_correom4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_correom4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_celularm4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_celularm4_c']['labelValue']='Celular - M4';
$dictionary['Contact']['fields']['sasa_celularm4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_celularm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_celularm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_celularm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_celularm4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_direccionprincm4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['labelValue']='Dirección Principal - M4';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0,35',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_departamentom4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_departamentom4_c']['labelValue']='Departamento - M4';
$dictionary['Contact']['fields']['sasa_departamentom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_departamentom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_departamentom4_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_departamentom4_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_municipiom4_c.php

 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_municipiom4_c']['labelValue']='Municipio - M4';
$dictionary['Contact']['fields']['sasa_municipiom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_municipiom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_municipiom4_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_municipiom4_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nroidentificacionm4_c.php

 // created: 2022-11-16 02:16:44
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['labelValue']='Numero de ID - M4';
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_nroidentificacionm4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_tipoidentificacionm4_c.php

 // created: 2022-11-16 02:16:44
$dictionary['Contact']['fields']['sasa_tipoidentificacionm4_c']['labelValue']='Tipo de ID - M4';
$dictionary['Contact']['fields']['sasa_tipoidentificacionm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_tipoidentificacionm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_tipoidentificacionm4_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_tipoidentificacionm4_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_nombrem4_c.php

 // created: 2022-11-16 02:16:44
$dictionary['Contact']['fields']['sasa_nombrem4_c']['labelValue']='Cuenta (Nombre) - M4';
$dictionary['Contact']['fields']['sasa_nombrem4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_nombrem4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_nombrem4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_nombrem4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_nombrem4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_razon_socialm4_c.php

 // created: 2022-11-16 02:16:44
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['labelValue']='Razón Social - M4';
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_razon_socialm4_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_sasa_pqrs_c.php

 // created: 2022-12-29 22:41:37
$dictionary['Contact']['fields']['sasa_pqrs_c']['labelValue']='PQRS';
$dictionary['Contact']['fields']['sasa_pqrs_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_pqrs_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_pqrs_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_pqrs_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-03 09:42:31
VardefManager::createVardef('Contacts', 'Contact', [
                                'customer_journey_parent',
                        ]);
?>
