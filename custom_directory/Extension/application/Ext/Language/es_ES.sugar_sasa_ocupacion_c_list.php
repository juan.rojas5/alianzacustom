<?php
 // created: 2018-08-11 20:43:13

$app_list_strings['sasa_ocupacion_c_list']=array (
  '' => '',
  'Asalariado' => 'Asalariado',
  'Comercial' => 'Comercial',
  'Construccion' => 'Construcción',
  'Empleado' => 'Empleado',
  'Estudiante' => 'Estudiante',
  'Financiero' => 'Financiero',
  'Hogar' => 'Hogar',
  'Independiente' => 'Independiente',
  'Industrial' => 'Industrial',
  'Militar' => 'Militar',
  'Pensionado' => 'Pensionado',
  'Religioso' => 'Religioso',
  'Rentista' => 'Rentista',
  'Servicios' => 'Servicios',
  'Socio' => 'Socio',
  'Transporte' => 'Transporte',
  'Otros' => 'Otros',
  'No Registra' => 'No Registra',
);