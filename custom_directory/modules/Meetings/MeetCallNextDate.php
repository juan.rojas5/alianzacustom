<?php

    if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

    class before_save_class_MeetCallNextDate
    {
        static $already_ran = false;

        function before_save_method_MeetCallNextDate($bean, $event, $arguments)
        {
                if(self::$already_ran == true) return;
                self::$already_ran = true;

                if (($bean->fetched_row['sasa_fechaproximcompromiso_c'] == "" && $bean->sasa_fechaproximcompromiso_c != NULL && $bean->parent_type == "Accounts" && $bean->module_name=='Meetings') || ($bean->fetched_row['sasa_fechaproximallamada_c'] == "" && $bean->sasa_fechaproximallamada_c != NULL && $bean->parent_type == "Accounts" && $bean->module_name=='Calls')) {

                    $GLOBALS['log']->security("\n");
                    $GLOBALS['log']->security("******************************************************************");
                    $GLOBALS['log']->security("INICIO LOGIC HOOK - FECHA PROXIMO EVENTO (dentro del if)" . date("F j, Y, g:i a"));
                    $GLOBALS['log']->security("******************************************************************");
                    $GLOBALS['log']->security("MEET ID {$bean->id}");

                    //$MeetCall = BeanFactory::getBean("Meetings","",array('disable_row_level_security' => true));

                    // $fecha_proximo_meet_call = $bean->module_name == "Meetings" ? $fecha_proximo = $bean->sasa_fechaproximcompromiso_c : $fecha_proximo = $bean->sasa_fechaproximallamada_c;

                    if( $bean->sasa_programarllamada_c )
                    {
                        $GLOBALS['log']->security("sasa_programarllamada_c -> ".$bean->sasa_programarllamada_c);
                    }
                    if( $bean->sasa_programarvisita_c )
                    {
                        $GLOBALS['log']->security("sasa_programarvisita_c -> ".$bean->sasa_programarvisita_c);
                    }
                    
                    if( $bean->sasa_programarllamada_c == 'Llamada' || $bean->sasa_programarvisita_c == 'Llamada' || $bean->sasa_programarllamada_c == 'Visita' || $bean->sasa_programarvisita_c == 'Visita' )
                    {
                        if( $bean->sasa_programarllamada_c == 'Llamada' || $bean->sasa_programarvisita_c == 'Llamada' )
                        {
                            $module = "Calls";
                            $MeetCall = BeanFactory::newBean($module);
                            // $MeetCall->sasa_programarllamada_c = '';
                            // $fecha_proximo_meet_call = $bean->sasa_fechaproximallamada_c;
                        }
                        
                        if( $bean->sasa_programarllamada_c == 'Visita' || $bean->sasa_programarvisita_c == 'Visita' )
                        {
                            $module = "Meetings";
                            $MeetCall = BeanFactory::newBean($module);
                            // $MeetCall->sasa_programarvisita_c = '';
                            // $fecha_proximo_meet_call = $bean->sasa_fechaproximcompromiso_c;
                        }

                        $fecha_proximo_meet_call = !empty($bean->sasa_fechaproximcompromiso_c) ? $bean->sasa_fechaproximcompromiso_c : $bean->sasa_fechaproximallamada_c;
                        
                        
                        #Mapeo campos dinamico!!!
                        foreach($bean->field_defs as $campo => $definicion)
                        {
                            if( $campo != 'id' and $campo != 'date_entered' and $campo != 'date_modified' and $campo != 'date_start' and $campo != 'date_end' and $campo != 'status' and $campo != 'sasa_fechaproximcompromiso_c' and $campo != 'sasa_fechaproximallamada_c' ) 
                            {
                                $MeetCall->$campo = $bean->$campo;   
                            }
                        }

                        $MeetCall->date_start = $fecha_proximo_meet_call;
                        
                        $date_end = new DateTime($fecha_proximo_meet_call);
                        $date_end->modify('+30 minutes');
                        $MeetCall->date_end = $date_end->format('Y-m-d H:i:s');
                        $MeetCall->status = "Planned";

                        $GLOBALS['log']->security("date start -> ".$fecha_proximo_meet_call);
                        $GLOBALS['log']->security("date end -> ".$date_end->format('Y-m-d H:i:s'));
                        $GLOBALS['log']->security("Campo a revisar sasa_programarllamada_c ".$MeetCall->sasa_programarllamada_c);
                        $GLOBALS['log']->security("Campo a revisar sasa_programarvisita_c ".$MeetCall->sasa_programarvisita_c);

                        $MeetCall->save();   
                        $GLOBALS['log']->security("Inicia la actualización");
                        $GLOBALS['log']->security("Id registro a actualizar: ".$MeetCall->id);
                        $GLOBALS['log']->security("Módulo a realizar actualización: ".$module);
                        $this->update_bean($MeetCall->id, $module);
                    }
                }
            
                $GLOBALS['log']->security("\n");
                $GLOBALS['log']->security("******************************************************************");
                $GLOBALS['log']->security("FIN LOGIC HOOK" . date("F j, Y, g:i a"));
                $GLOBALS['log']->security("******************************************************************");
        }

        function update_bean($id_bean, $module_name)
        {
            if( $module_name == 'Calls' )
            {
                $query = "UPDATE calls_cstm 
                SET sasa_programarllamada_c = '' 
                WHERE id_c = '{$id_bean}'";
                
            }

            if( $module_name == 'Meetings' )
            {
                $query = "UPDATE meetings_cstm 
                SET sasa_programarvisita_c = '' 
                WHERE id_c = '{$id_bean}'";
                
            }

            $resultQuery = $GLOBALS['db']->query($query);

            $GLOBALS['log']->security("Actualización realizada...");
        }
    }

?>