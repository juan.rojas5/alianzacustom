<?php
// created: 2019-01-04 21:54:12
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_productosafav_sasa_saldosav_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1SASA_PRODUCTOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'sasa_nombreproducto_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NOMBREPRODUCTO_C',
    'width' => 10,
  ),
  'sasa_nemotecnico_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NEMOTECNICO_C',
    'width' => 10,
  ),
  'sasa_emisor_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_EMISOR_C',
    'width' => 10,
  ),
  'sasa_isinespecie_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_ISINESPECIE_C',
    'width' => 10,
  ),
  'sasa_numeroacciones_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_NUMEROACCIONES_C',
    'width' => 10,
  ),
  'sasa_tipotitulo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_TIPOTITULO_C',
    'width' => 10,
  ),
  'sasa_fechaemision_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHAEMISION_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_fechavencimiento_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHAVENCIMIENTO_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_tasafacial_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TASAFACIAL_C',
    'width' => 10,
  ),
  'sasa_tipooperacion_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_TIPOOPERACION_C',
    'width' => 10,
  ),
  'sasa_nominal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_NOMINAL_C',
    'width' => 10,
  ),
  'sasa_valorcompra_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORCOMPRA_C',
    'width' => 10,
  ),
  'sasa_tirprecio_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TIRPRECIO_C',
    'width' => 10,
  ),
  'sasa_valormercado_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORMERCADO_C',
    'width' => 10,
  ),
  'sasa_tirvpn_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TIRVPN_C',
    'width' => 10,
  ),
  'sasa_preciovpn_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_PRECIOVPN_C',
    'width' => 10,
  ),
  'sasa_disponiblenominal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_DISPONIBLENOMINAL_C',
    'width' => 10,
  ),
  'sasa_disponiblevlrmercado_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_DISPONIBLEVLRMERCADO_C',
    'width' => 10,
  ),
  'sasa_garantianominal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_GARANTIANOMINAL_C',
    'width' => 10,
  ),
  'sasa_garantiavlrmercado_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_GARANTIAVLRMERCADO_C',
    'width' => 10,
  ),
  'sasa_valorinicial_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORINICIAL_C',
    'width' => 10,
  ),
  'sasa_valorfinal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORFINAL_C',
    'width' => 10,
  ),
  'sasa_saldototal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_SALDOTOTAL_C',
    'width' => 10,
  ),
  'sasa_fecharegistro_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHAREGISTRO_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_fechacumplimiento_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
    'width' => 10,
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);