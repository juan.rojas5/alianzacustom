<?php
 // created: 2018-08-17 22:10:50
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['options']='salutation_list';
$dictionary['Lead']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Lead']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Lead']['fields']['salutation']['calculated']=false;
$dictionary['Lead']['fields']['salutation']['dependency']=false;
$dictionary['Lead']['fields']['salutation']['required']=false;
$dictionary['Lead']['fields']['salutation']['audited']=false;
$dictionary['Lead']['fields']['salutation']['pii']=false;

 ?>