<?php
 // created: 2022-10-21 14:59:14
$dictionary['Case']['fields']['sasa_horaresolucion_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_horaresolucion_c']['labelValue']='Hora resolución';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_horaresolucion_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['formula']='subStr(toString($sasa_fechacierre_c),11,2)';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['readonly_formula']='';

 ?>