<?php
 // created: 2018-07-11 04:18:45
$dictionary['Lead']['fields']['primary_address_city']['required']=false;
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_city']['importable']='true';
$dictionary['Lead']['fields']['primary_address_city']['audited']=true;
$dictionary['Lead']['fields']['primary_address_city']['reportable']=true;
$dictionary['Lead']['fields']['primary_address_city']['pii']=true;

 ?>