<?php
 // created: 2018-01-29 21:13:46

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Ülesanne',
  'Opportunities' => 'Võimalus',
  'Products' => 'Pakkumuse artikkel',
  'Quotes' => 'Pakkumus',
  'Bugs' => 'Vead',
  'Cases' => 'Juhtum',
  'Leads' => 'Müügivihje',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projekti ülesanne',
  'Prospects' => 'Eesmärk',
  'KBContents' => 'Teadmusbaas',
  'RevenueLineItems' => 'Tuluartiklid',
);