<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Создано по названию',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'PQRs Histórico Список',
  'LBL_MODULE_NAME' => 'PQRs Histórico',
  'LBL_MODULE_TITLE' => 'PQRs Histórico',
  'LBL_MODULE_NAME_SINGULAR' => 'PQRs Histórico',
  'LBL_HOMEPAGE_TITLE' => 'Моя PQRs Histórico',
  'LNK_NEW_RECORD' => 'Создать PQRs Histórico',
  'LNK_LIST' => 'Просмотр PQRs Histórico',
  'LNK_IMPORT_SASA_PQRS_HISTORICO' => 'Importar PQRs Histórico',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск PQRs Histórico',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_SASA_PQRS_HISTORICO_SUBPANEL_TITLE' => 'PQRs Histórico',
  'LBL_NEW_FORM_TITLE' => 'Новый PQRs Histórico',
  'LNK_IMPORT_VCARD' => 'Importar PQRs Histórico vCard',
  'LBL_IMPORT' => 'Importar PQRs Histórico',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new PQRs Histórico record by importing a vCard from your file system.',
  'LBL_SASA_PQRS_HISTORICO_FOCUS_DRAWER_DASHBOARD' => 'PQRs Histórico Panel de enfoque',
  'LBL_SASA_PQRS_HISTORICO_RECORD_DASHBOARD' => 'PQRs Histórico Cuadro de mando del registro',
);