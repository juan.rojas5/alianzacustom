<?php
$mod_strings['LBL_EXPORT_STAGED_CC_CONTACTS'] = 'Handle Mass Export to CC';
$mod_strings['LBL_UPDATE_CC_CAMPAIGN_RESULTS'] = 'Update CC Campaign Results';
$mod_strings['LBL_SYNC_STAGED_LISTS'] = 'Sync Staged Lists';
$mod_strings['LBL_AUTOMATIC_CONTACT_SYNC'] = 'Automatic Contact Sync';
