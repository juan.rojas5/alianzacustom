<?php
 // created: 2023-01-02 19:07:38
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['labelValue']='Número único ';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['readonly_formula']='';

 ?>