<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(99,'Sync to Constant Contact ProspectLists Module','custom/modules/ProspectLists/include/LogicHooks/fbsg_cc_hook.php','fbsg_cc_ProspectListsLogic','after_save',);
$hook_array['after_delete'] = Array(); 
$hook_array['after_delete'][] = Array(99,'Update all of the related records to remove the deleted list from their cc_lists field','custom/modules/ProspectLists/include/LogicHooks/fbsg_cc_hook.php','fbsg_cc_ProspectListsLogic','after_delete',);
$hook_array['after_relationship_add'] = Array(); 
$hook_array['after_relationship_add'][] = Array(99,'Update the related cc_lists value to include the new list','custom/modules/ProspectLists/include/LogicHooks/fbsg_cc_hook.php','fbsg_cc_ProspectListsLogic','after_relationship_add',);
$hook_array['after_relationship_delete'] = Array(); 
$hook_array['after_relationship_delete'][] = Array(99,'Update the related cc_lists value to remove the list value','custom/modules/ProspectLists/include/LogicHooks/fbsg_cc_hook.php','fbsg_cc_ProspectListsLogic','after_relationship_delete',);



?>