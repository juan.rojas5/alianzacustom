<?php
 // created: 2022-11-18 20:12:03
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['labelValue']='Fecha_creacion';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['readonly_formula']='';

 ?>