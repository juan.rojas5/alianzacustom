<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/fbsg_cc_newvars.php

// $cc_module = 'Prospect';
// include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/fbsg_vardefs_prospects.php

$dictionary['Prospect']['fields']['fbsg_ccintegrationlog_prospects'] = array(
    'name' => 'fbsg_ccintegrationlog_prospects',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_prospects',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Prospect"]["fields"]["prospect_list_prospects"] = array(
    'name' => 'prospect_list_prospects',
    'type' => 'link',
    'relationship' => 'prospect_list_prospects',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Prospect';
include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Prospect']['full_text_search']=false;

?>
