<?php
 // created: 2018-05-22 21:53:04
$dictionary['Lead']['fields']['status_description']['audited']=false;
$dictionary['Lead']['fields']['status_description']['massupdate']=false;
$dictionary['Lead']['fields']['status_description']['comments']='Description of the status of the lead';
$dictionary['Lead']['fields']['status_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status_description']['reportable']=false;
$dictionary['Lead']['fields']['status_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['status_description']['calculated']=false;
$dictionary['Lead']['fields']['status_description']['rows']='4';
$dictionary['Lead']['fields']['status_description']['cols']='20';

 ?>