<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_convertedlead';
function tarea_convertedlead(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_convertedlead. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;

    $query_convert_lead = $db->query("SELECT leads.id idlead, accounts_cstm.id_c idcuenta, leads.status, leads.converted, leads_cstm.sasa_nroidentificacion_c numeroidlead, accounts_cstm.sasa_nroidentificacion_c numeroidcuent,accounts.assigned_user_id asignadocuenta, accounts.name nomcuenta FROM leads INNER JOIN leads_cstm ON leads.id=leads_cstm.id_c INNER JOIN accounts_cstm ON leads_cstm.sasa_nroidentificacion_c=accounts_cstm.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c=accounts.id WHERE leads.converted=0 and accounts.deleted=0 AND leads.deleted=0");
    


    while ($row = $db->fetchByAssoc($query_convert_lead)) {
	      $Account = BeanFactory::getBean('Accounts', $row['idcuenta'],array('disable_row_level_security' => true));
	      $Lead = BeanFactory::getBean('Leads', $row['idlead'],array('disable_row_level_security' => true));

	      //Bucar y/o establecer el contacto
	      $query_get_contact = $db->query("SELECT contacts_cstm.id_c FROM contacts_cstm INNER JOIN contacts ON contacts_cstm.id_c=contacts.id WHERE contacts_cstm.sasa_nroidentificacion_c='{$row['numeroidlead']}' AND contacts.deleted=0");

	      if (mysqli_num_rows($query_get_contact)>=1) {
	      	$id_contact = mysqli_fetch_array($query_get_contact);
	      	$Contact = BeanFactory::getBean('Contacts', $id_contact['id_c'],array('disable_row_level_security' => true));
	      }else{
	      	$Contact = new Contact();
		      foreach ($Lead->field_defs as $campo => $definicion) {
		          if ($campo != 'id' and $campo != 'date_entered' and $campo != 'date_modified') { //no incluir descripcion, caso 

		            $Contact->$campo = $Lead->$campo;
		          }
		      }
	      }
	      $Contact->save();
		  $Lead->contact_id=$Contact->id;
	      $Account->load_relationship("contacts"); //Ya se debe tener id de cuenta y contacto
			$Account->contacts->add($Contact->id);

	      


	      $Lead->account_id = $row['idcuenta'];
	      $Lead->account_name=$row["nomcuenta"];
	      $Lead->status = "In Process";

		  $Lead->converted=true;


		  //Mover llamadas y visitas
		  $querymov_call = $db->query("UPDATE calls SET parent_id='{$row['idcuenta']}', parent_type='Accounts', assigned_user_id='{$row['asignadocuenta']}',status='Held' WHERE parent_id='{$row['idlead']}'");
		  $querymov_meetings = $db->query("UPDATE meetings SET parent_id='{$row['idcuenta']}', parent_type='Accounts', assigned_user_id='{$row['asignadocuenta']}',status='Held' WHERE parent_id='{$row['idlead']}'");
		  $query_delete_relation_lead_call = $db->query("UPDATE calls_leads SET deleted = '1' WHERE calls_leads.lead_id = '{$row['idlead']}';");
		  $query_delete_relation_lead_meetings = $db->query("UPDATE meetings_leads SET deleted = '1' WHERE meetings_leads.lead_id = '{$row['idlead']}';");

		  $Lead->save();


        $GLOBALS['log']->security(" Cuenta: {$Account->id}******************************************\n");
        $GLOBALS['log']->security(" Lead: {$Lead->id}******************************************\n");


    }
      
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_convertedlead. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
