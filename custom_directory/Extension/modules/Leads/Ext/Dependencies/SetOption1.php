<?php
$dependencies['Leads']['SetOption1'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Juridica")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("Nit","Otro")',
           'labels' => 'createList("Nit","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_ocupacion_c',
           'keys' => 'createList("","Comercial","Construccion","Financiero","Industrial","Servicios","Transporte","Otros")',
           'labels' => 'createList("","Comercial","Construcción","Financiero","Industrial","Servicios","Transporte","Otros")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_ocupacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
