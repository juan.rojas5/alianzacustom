<?php

$dependencies['Accounts']['readonly_billing_address_city'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('billing_address_city'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'billing_address_city',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);

