<?php
 // created: 2018-06-07 00:45:19
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['labelValue']='Calculo Perfil Riesgo';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['formula']='add(number($sasa_p1rangoedadcalidad_c),number($sasa_p2propositoahorroinve_c),number($sasa_p3horizontetiempoinve_c),number($sasa_p4dondeprovienedinero_c),number($sasa_p5porcentajeactivosah_c),number($sasa_p6experienciainversio_c),number($sasa_p7oportunidadrentabil_c),number($sasa_p8supongainversionesp_c))';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['dependency']='';

 ?>