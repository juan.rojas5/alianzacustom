<?php

$dependencies['Leads']['fields_setrequired'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'triggerFields' => array('sasa_tipopersona_c','sasa_estadoinfocp_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'email',
                'value' => 'and(equal($sasa_tipopersona_c,"Juridica"),equal($sasa_estadoinfocp_c,"Informacion Completa"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ingresosmensuales_c',
                'value' => 'and(equal($sasa_tipopersona_c,"Juridica"),equal($sasa_estadoinfocp_c,"Informacion Completa"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipoidentificacion_c',
                'value' => 'equal($sasa_tipopersona_c,"Juridica")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => 'equal($sasa_tipopersona_c,"Juridica")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'salutation',
                'value' => 'equal($sasa_tipopersona_c,"Natural")',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'first_name',
                'value' => 'not(equal($sasa_tipopersona_c,"Juridica"))',
            ),
        ),
        array(
            'name' => 'SetRequired',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'last_name',
                'value' => 'not(equal($sasa_tipopersona_c,"Juridica"))',
            ),
        ),
    ),
);

