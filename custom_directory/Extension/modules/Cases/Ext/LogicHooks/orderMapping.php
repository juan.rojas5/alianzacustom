<?php
// created: 2023-03-31 21:34:21
$extensionOrderMap = array (
  'modules/Cases/Ext/LogicHooks/RelationshipHook.php' => 
  array (
    'md5' => 'c8d84c5562d6934c11dbedb0335f1dad',
    'mtime' => 1625078063,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/denorm_field_hook.php' => 
  array (
    'md5' => 'fec67ccbfe263c6c56732c1ff550748a',
    'mtime' => 1643412720,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/SfcComplaints.php' => 
  array (
    'md5' => 'd411afb3007770ba150a5152826a1ad2',
    'mtime' => 1651243456,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/before_save_relate_typing_to_cases.php' => 
  array (
    'md5' => '6fa9346ab245d98cabc47ff4793aea62',
    'mtime' => 1669651159,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/before_save_CalcularFechaVencimientoCasosM1.php' => 
  array (
    'md5' => 'fbbac7e82ac89a809f441c1cd4378f7a',
    'mtime' => 1670015922,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/set_case_name_after_save.php' => 
  array (
    'md5' => '18fc319bab8350b36c99276887de62e9',
    'mtime' => 1673040195,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/AlianzaCasesAfterSave.php' => 
  array (
    'md5' => 'd1e45f969c6e92606eb4f798036ffca3',
    'mtime' => 1673876969,
    'is_override' => false,
  ),
  'custom/Extension/modules/Cases/Ext/LogicHooks/set_resolved_date_before_save.php' => 
  array (
    'md5' => 'b09849e5202a6ac19cb6a19c7a49d2e8',
    'mtime' => 1680298459,
    'is_override' => false,
  ),
);