<?php
 // created: 2022-11-18 20:12:46
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['labelValue']='Hora';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['readonly_formula']='';

 ?>