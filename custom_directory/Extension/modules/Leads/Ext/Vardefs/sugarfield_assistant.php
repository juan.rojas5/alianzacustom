<?php
 // created: 2018-05-22 22:00:11
$dictionary['Lead']['fields']['assistant']['audited']=false;
$dictionary['Lead']['fields']['assistant']['massupdate']=false;
$dictionary['Lead']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Lead']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant']['reportable']=false;
$dictionary['Lead']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant']['calculated']=false;
$dictionary['Lead']['fields']['assistant']['pii']=false;

 ?>