<?php
 // created: 2022-11-18 20:30:14
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['labelValue']='Tipo_de_Solicitud';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['readonly_formula']='';

 ?>