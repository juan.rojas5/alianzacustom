<?php
// created: 2023-02-03 09:39:47
$viewdefs['Meetings']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 'name',
        1 => 
        array (
          'name' => 'sasa_objetivovisita_c',
          'label' => 'LBL_SASA_OBJETIVOVISITA_C',
        ),
        2 => 
        array (
          'name' => 'date',
          'type' => 'fieldset',
          'related_fields' => 
          array (
            0 => 'date_start',
            1 => 'date_end',
          ),
          'label' => 'LBL_START_AND_END_DATE_DETAIL_VIEW',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_start',
            ),
            1 => 
            array (
              'name' => 'date_end',
              'required' => true,
              'readonly' => false,
            ),
          ),
        ),
        3 => 
        array (
          'name' => 'reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'reminder_checked',
            1 => 'reminder_time',
          ),
          'label' => 'LBL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'reminder_checked',
            ),
            1 => 
            array (
              'name' => 'reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        4 => 
        array (
          'name' => 'email_reminder',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'email_reminder_checked',
            1 => 'email_reminder_time',
          ),
          'label' => 'LBL_EMAIL_REMINDER',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'email_reminder_checked',
            ),
            1 => 
            array (
              'name' => 'email_reminder_time',
              'type' => 'enum',
              'options' => 'reminder_time_options',
            ),
          ),
        ),
        5 => 
        array (
          'name' => 'location',
          'comment' => 'Meeting location',
          'label' => 'LBL_LOCATION',
        ),
        6 => 'parent_name',
        7 => 'status',
        8 => 'description',
        9 => 
        array (
          'name' => 'sasa_resultadovisita_c',
          'label' => 'LBL_SASA_RESULTADOVISITA_C',
        ),
        10 => 
        array (
          'name' => 'sasa_productosofrecidos_c',
          'label' => 'LBL_SASA_PRODUCTOSOFRECIDOS_C',
        ),
        11 => 
        array (
          'name' => 'sasa_compromiso_c',
          'label' => 'LBL_SASA_COMPROMISO_C',
        ),
        12 => 
        array (
          'name' => 'sasa_responsableejecucion_c',
          'label' => 'LBL_SASA_RESPONSABLEEJECUCION_C',
        ),
        13 => 
        array (
          'name' => 'sasa_fechaproximcompromiso_c',
          'label' => 'LBL_SASA_FECHAPROXIMCOMPROMISO_C',
        ),
        14 => 
        array (
          'name' => 'sasa_referidos_c',
          'label' => 'LBL_SASA_REFERIDOS_C',
        ),
        15 => 
        array (
          'name' => 'sasa_visitacompaniade_c',
          'label' => 'LBL_SASA_VISITACOMPANIADE_C',
        ),
        16 => 'assigned_user_name',
        17 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        18 => 'tag',
      ),
    ),
  ),
);