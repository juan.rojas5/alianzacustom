<?php
$dependencies['Leads']['SetOptionDepartamento18'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Guaviare")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Miraflores","Calamar","El Retorno","San Jose del Guaviare")',
           'labels' => 'createList("","Miraflores","Calamar","El Retorno","San José del Guaviare")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
