<?php
// created: 2023-02-03 09:39:47
$viewdefs['Meetings']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 
        array (
          'name' => 'sasa_objetivovisita_c',
          'label' => 'LBL_SASA_OBJETIVOVISITA_C',
        ),
        2 => 'date_start',
        3 => 
        array (
          'name' => 'date_end',
          'comment' => 'Date meeting ends',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_CALENDAR_END_DATE',
        ),
        4 => 
        array (
          'name' => 'reminder_time',
          'comment' => 'Specifies when a reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'label' => 'LBL_POPUP_REMINDER_TIME',
        ),
        5 => 
        array (
          'name' => 'email_reminder_time',
          'comment' => 'Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start',
          'label' => 'LBL_EMAIL_REMINDER_TIME',
        ),
        6 => 
        array (
          'name' => 'duration',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'duration_hours',
            1 => 'duration_minutes',
          ),
          'label' => 'LBL_DURATION',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'duration_hours',
            ),
            1 => 
            array (
              'type' => 'label',
              'default' => 'LBL_HOURS_ABBREV',
              'css_class' => 'label_duration_hours hide',
            ),
            2 => 
            array (
              'name' => 'duration_minutes',
            ),
            3 => 
            array (
              'type' => 'label',
              'default' => 'LBL_MINSS_ABBREV',
              'css_class' => 'label_duration_minutes hide',
            ),
          ),
        ),
        7 => 
        array (
          'name' => 'location',
          'comment' => 'Meeting location',
          'label' => 'LBL_LOCATION',
        ),
        8 => 'parent_name',
        9 => 'status',
        10 => 'description',
        11 => 
        array (
          'name' => 'sasa_resultadovisita_c',
          'label' => 'LBL_SASA_RESULTADOVISITA_C',
        ),
        12 => 
        array (
          'name' => 'sasa_productosofrecidos_c',
          'label' => 'LBL_SASA_PRODUCTOSOFRECIDOS_C',
        ),
        13 => 
        array (
          'name' => 'sasa_compromiso_c',
          'label' => 'LBL_SASA_COMPROMISO_C',
        ),
        14 => 
        array (
          'name' => 'sasa_responsableejecucion_c',
          'label' => 'LBL_SASA_RESPONSABLEEJECUCION_C',
        ),
        15 => 
        array (
          'name' => 'sasa_fechaproximcompromiso_c',
          'label' => 'LBL_SASA_FECHAPROXIMCOMPROMISO_C',
        ),
        16 => 
        array (
          'name' => 'sasa_visitacompaniade_c',
          'label' => 'LBL_SASA_VISITACOMPANIADE_C',
        ),
        17 => 
        array (
          'name' => 'sasa_referidos_c',
          'label' => 'LBL_SASA_REFERIDOS_C',
        ),
        18 => 'assigned_user_name',
        19 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        20 => 'tag',
      ),
    ),
  ),
);