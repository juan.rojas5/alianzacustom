<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_eliminado_8dias';

function tarea_eliminado_8dias(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_eliminado_8dias. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{

    global $db;

    $queries = array("DELETE ignore accounts_sasa_movimientosaf_1_c FROM accounts_sasa_movimientosaf_1_c left outer join sasa_movimientosaf on accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1sasa_movimientosaf_idb = sasa_movimientosaf.id where sasa_movimientosaf.id is null",
          "DELETE ignore accounts_sasa_movimientosav_1_c FROM accounts_sasa_movimientosav_1_c left outer join sasa_movimientosav on accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1sasa_movimientosav_idb = sasa_movimientosav.id where sasa_movimientosav.id is null",
          "delete ignore accounts_sasa_saldosaf_1_c FROM accounts_sasa_saldosaf_1_c left outer join sasa_saldosaf on accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id where sasa_saldosaf.id is null",
          "delete ignore accounts_sasa_saldosav_1_c FROM accounts_sasa_saldosav_1_c left outer join sasa_saldosav on accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id where sasa_saldosav.id is null",
          "delete ignore sasa_productosafav_sasa_movimientosaf_1_c FROM sasa_productosafav_sasa_movimientosaf_1_c left outer join sasa_movimientosaf on sasa_productosafav_sasa_movimientosaf_1_c.sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb = sasa_movimientosaf.id where sasa_movimientosaf.id is null",
          "delete ignore sasa_productosafav_sasa_saldosaf_1_c FROM sasa_productosafav_sasa_saldosaf_1_c left outer join sasa_saldosaf on sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id where sasa_saldosaf.id is null",
          "delete ignore sasa_productosafav_sasa_saldosav_1_c FROM sasa_productosafav_sasa_saldosav_1_c left outer join sasa_saldosav on sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id where sasa_saldosav.id is null",
        );

    //forecah para empezar a ejecutar los queries del array $queries
        foreach ($queries as $query) {
          $querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
          $result = $db->query($querytotal);
          $lastDbError = $db->lastDbError();        
          if(!empty($lastDbError)){
            $GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
          }
        }
    
    
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_eliminado_8dias. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
