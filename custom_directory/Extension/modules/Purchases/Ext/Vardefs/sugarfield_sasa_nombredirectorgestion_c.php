<?php
 // created: 2023-04-11 19:08:52
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['labelValue']='Nombre director de gestión';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['readonly_formula']='';

 ?>