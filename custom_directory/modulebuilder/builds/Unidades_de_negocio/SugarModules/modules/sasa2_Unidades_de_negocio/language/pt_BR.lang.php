<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id da Equipe',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Marcações',
  'LBL_TAGS' => 'Marcações',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DOC_OWNER' => 'Proprietário do documento',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nome',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Criado por nome',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio Lista',
  'LBL_MODULE_NAME' => 'Unidades de negocio',
  'LBL_MODULE_TITLE' => 'Unidades de negocio',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidades de negocio',
  'LBL_HOMEPAGE_TITLE' => 'Minha Unidades de negocio',
  'LNK_NEW_RECORD' => 'Criar Unidades de negocio',
  'LNK_LIST' => 'Visualização Unidades de negocio',
  'LNK_IMPORT_SASA2_UNIDADES_DE_NEGOCIO' => 'Importar Unidades de negocio',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Unidades de negocio',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Cadeia de atividades',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_SUBPANEL_TITLE' => 'Unidades de negocio',
  'LBL_NEW_FORM_TITLE' => 'Novo Unidades de negocio',
  'LNK_IMPORT_VCARD' => 'Importar Unidades de negocio vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidades de negocio record by importing a vCard from your file system.',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio Panel de enfoque',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_RECORD_DASHBOARD' => 'Unidades de negocio Cuadro de mando del registro',
);