<?php
 // created: 2022-03-08 09:54:59

$app_list_strings['sasa_rectificacion_c_list']=array (
  '' => '',
  1 => 'Queja o reclamo rectificada por la entidad vigilada antes de la decisión del DCF',
  2 => 'Queja o reclamo no rectificada por la entidad vigilada antes de la decisión del DCF',
  3 => 'Queja o reclamo rectificada por la entidad vigilada después de la decisión del DCF',
  4 => 'Queja o reclamo no rectificada por la entidad vigilada después de la decisión del DCF',
);