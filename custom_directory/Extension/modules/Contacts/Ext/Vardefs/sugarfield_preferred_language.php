<?php
 // created: 2018-07-04 18:30:31
$dictionary['Contact']['fields']['preferred_language']['default']='en_us';
$dictionary['Contact']['fields']['preferred_language']['audited']=false;
$dictionary['Contact']['fields']['preferred_language']['massupdate']=true;
$dictionary['Contact']['fields']['preferred_language']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['preferred_language']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['preferred_language']['merge_filter']='disabled';
$dictionary['Contact']['fields']['preferred_language']['calculated']=false;
$dictionary['Contact']['fields']['preferred_language']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>