<?php
 // created: 2022-11-18 20:52:49
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['labelValue']='Fecha_solución';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['readonly_formula']='';

 ?>