<?php
// created: 2023-02-03 09:39:47
$viewdefs['Contacts']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'salutation',
          'comment' => 'Contact salutation (e.g., Mr, Ms)',
          'label' => 'LBL_SALUTATION',
        ),
        1 => 'full_name',
        2 => 
        array (
          'name' => 'sasa_parentescorelacion_c',
          'label' => 'LBL_SASA_PARENTESCORELACION_C',
        ),
        3 => 'account_name',
        4 => 
        array (
          'name' => 'sasa_relacioncuenta_c',
          'label' => 'LBL_SASA_RELACIONCUENTA_C',
        ),
        5 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        6 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        7 => 'title',
        8 => 
        array (
          'name' => 'department',
          'comment' => 'The department of the contact',
          'label' => 'LBL_DEPARTMENT',
        ),
        9 => 'phone_mobile',
        10 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        11 => 'email',
        12 => 'primary_address_country',
        13 => 'primary_address_state',
        14 => 'primary_address_city',
        15 => 'primary_address_street',
        16 => 'phone_home',
        17 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        18 => 
        array (
          'name' => 'assistant_phone',
          'comment' => 'Phone number of the assistant of the contact',
          'label' => 'LBL_ASSISTANT_PHONE',
        ),
        19 => 
        array (
          'name' => 'do_not_call',
          'comment' => 'An indicator of whether contact can be called',
          'label' => 'LBL_DO_NOT_CALL',
        ),
        20 => 
        array (
          'name' => 'sasa_noenviarinfocliente_c',
          'label' => 'LBL_SASA_NOENVIARINFOCLIENTE_C',
        ),
        21 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        22 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
        ),
        23 => 
        array (
          'name' => 'sasa_estadocivil_c',
          'label' => 'LBL_SASA_ESTADOCIVIL_C',
        ),
        24 => 
        array (
          'name' => 'sasa_nrohijos_c',
          'label' => 'LBL_SASA_NROHIJOS_C',
        ),
        25 => 
        array (
          'name' => 'sasa_hobbiesaficionesdepor_c',
          'label' => 'LBL_SASA_HOBBIESAFICIONESDEPOR_C',
        ),
        26 => 
        array (
          'name' => 'sasa_fondopensiones_c',
          'label' => 'LBL_SASA_FONDOPENSIONES_C',
        ),
        27 => 
        array (
          'name' => 'sasa_fondocesantias_c',
          'label' => 'LBL_SASA_FONDOCESANTIAS_C',
        ),
        28 => 
        array (
          'name' => 'sasa_infoquiererecibir_c',
          'label' => 'LBL_SASA_INFOQUIERERECIBIR_C',
        ),
        29 => 
        array (
          'name' => 'sasa_tipoinfoperiodicidad_c',
          'label' => 'LBL_SASA_TIPOINFOPERIODICIDAD_C',
        ),
        30 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the user',
          'label' => 'LBL_FACEBOOK',
        ),
        31 => 
        array (
          'name' => 'twitter',
          'comment' => 'The twitter name of the user',
          'label' => 'LBL_TWITTER',
        ),
        32 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        33 => 
        array (
          'name' => 'lead_source',
          'comment' => 'How did the contact come about',
          'label' => 'LBL_LEAD_SOURCE',
        ),
        34 => 
        array (
          'name' => 'sasa_quienrefirio_c',
          'label' => 'LBL_SASA_QUIENREFIRIO_C',
        ),
        35 => 'alt_address_street',
        36 => 'alt_address_country',
        37 => 'alt_address_state',
        38 => 'alt_address_city',
        39 => 'assigned_user_name',
        40 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        41 => 'tag',
      ),
    ),
  ),
);