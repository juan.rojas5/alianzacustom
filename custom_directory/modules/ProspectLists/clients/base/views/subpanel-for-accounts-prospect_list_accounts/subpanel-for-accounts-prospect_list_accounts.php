<?php
// created: 2015-10-07 15:58:15
$viewdefs['ProspectLists']['base']['view']['subpanel-for-accounts-prospect_list_accounts'] = array(
  'type' => 'subpanel-list',
  'panels' =>
  array(
    0 =>
    array(
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array(
        0 =>
        array(
          'name' => 'name',
          'label' => 'LBL_PROSPECTLISTS_TARGET_LIST_TITLE',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        1 =>
        array(
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
);
