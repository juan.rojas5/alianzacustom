<?php
$module_name = 'sasa_MovimientosAVDivisas';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_numerocuenta_c',
                'label' => 'LBL_SASA_NUMEROCUENTA_C',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'accounts_sasa_movimientosavdivisas_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
                'enabled' => true,
                'id' => 'SASA_PRODU845ETOSAFAV_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_moneda_c',
                'label' => 'LBL_SASA_MONEDA_C',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_tipo_c',
                'label' => 'LBL_SASA_TIPO_C',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_fecha_c',
                'label' => 'LBL_SASA_FECHA_C',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_tasadivisadolar_c',
                'label' => 'LBL_SASA_TASADIVISADOLAR_C',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_tasapesodolar_c',
                'label' => 'LBL_SASA_TASAPESODOLAR_C',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_cantidad_c',
                'label' => 'LBL_SASA_CANTIDAD_C',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_valorbruto_c',
                'label' => 'LBL_SASA_VALORBRUTO_C',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_valorneto_c',
                'label' => 'LBL_SASA_VALORNETO_C',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'sasa_ocobros_c',
                'label' => 'LBL_SASA_OCOBROS_C',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'sasa_retefte_c',
                'label' => 'LBL_SASA_RETEFTE_C',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'sasa_reteiva_c',
                'label' => 'LBL_SASA_RETEIVA_C',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'sasa_iva_c',
                'label' => 'LBL_SASA_IVA_C',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              18 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              19 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              20 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => true,
              ),
              21 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              22 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
              23 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              24 => 
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
