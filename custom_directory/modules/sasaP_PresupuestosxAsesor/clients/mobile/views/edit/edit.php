<?php
$module_name = 'sasaP_PresupuestosxAsesor';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'sasa_presupuestototal_c',
                'label' => 'LBL_SASA_PRESUPUESTOTOTAL_C',
              ),
              2 => 
              array (
                'name' => 'sasa_year_c',
                'label' => 'LBL_SASA_YEAR_C',
              ),
              3 => 
              array (
                'name' => 'sasa_mes_c',
                'label' => 'LBL_SASA_MES_C',
              ),
              4 => 
              array (
                'name' => 'sasa_deficitconsolidado_c',
                'label' => 'LBL_SASA_DEFICITCONSOLIDADO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_saldoconsolidadototal_c',
                'label' => 'LBL_SASA_SALDOCONSOLIDADOTOTAL_C',
              ),
              6 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              7 => 'assigned_user_name',
              8 => 'team_name',
              9 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              10 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
