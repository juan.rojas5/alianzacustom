<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

use function PHPSTORM_META\type;

class CalcularFechaVencimientoCasosM1_class
{
    function CalcularFechaVencimientoCasosM1_method($bean, $event, $arguments)
    {
        // ignore para evitar loop despues de usar el método save()
        $ignore = $bean->ignore_CalcularFechaVencimientoCasosM1;

        if (!isset($ignore) || $ignore === false){

            $ignore = true;
            $bean->ignore_CalcularFechaVencimientoCasosM1 = $ignore;

            $GLOBALS['log']->security("\n\n");
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Inicio acción before_save en CalcularFechaVencimientoCasosM1 Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
            
            if ( !isset($bean->fetched_row['id']) && $bean->source != 'SFC' ) 
            {
                $GLOBALS['log']->security("Registro nuevo");

                $tipo = $bean->sasa_tipodesolicitud_c;
                $proceso = $bean->sasa_proceso_c;
                $subproceso = $bean->sasa_subproceso_c;
                $detalle = $bean->sasa_detalle_c;

                if ( empty($detalle) ) 
                {
                    $detalle = '161';
                }

                $result = $this->consultaConDetalleM1($tipo, $proceso, $subproceso, $detalle);

                while ( $row = $GLOBALS['db']->fetchByAssoc($result) ) 
                {
                    $cantidad = $row['sasa_ans_c']; // cantidad de días a sumar
                    $diferencia = $this->calcularDifM1(); // diferencia horaria
                    
                    if ( !empty($cantidad) ) 
                    {
                        $dia = date("Y-m-d");
                        $fecha = date("Y-m-d H:i:s", strtotime("$dia 18:00:00"));
                        $fechaCreacion = date("Y-m-d H:i:s", strtotime("+$diferencia hour", strtotime($fecha)));
                        $fechaVencimiento = date("Y-m-d H:i:s", strtotime("+$cantidad day", strtotime($fechaCreacion)));
                        $bean->resolved_datetime = $fechaVencimiento;
                        // $GLOBALS['log']->security("Cantidad: ".$cantidad);
                        // $GLOBALS['log']->security("Diferencia: ".$diferencia);
                        // $GLOBALS['log']->security("Fecha creación: ".$fechaCreacion);
                        // $GLOBALS['log']->security("Fecha vencimiento real: ".$fechaVencimiento);
                    }
                }


            } else {
                $GLOBALS['log']->security("Registro existente");

                if ( $bean->source != 'SFC' ) 
                {
                    $tipo = $bean->sasa_tipodesolicitud_c;
                    $proceso = $bean->sasa_proceso_c;
                    $subproceso = $bean->sasa_subproceso_c;
                    $detalle = $bean->sasa_detalle_c;

                    $tipoAntes = $bean->fetched_row['sasa_tipodesolicitud_c'];
                    $procesoAntes = $bean->fetched_row['sasa_proceso_c'];
                    $subprocesoAntes = $bean->fetched_row['sasa_subproceso_c'];
                    $detalleAntes = $bean->fetched_row['sasa_detalle_c'];

                    if( $tipo != $tipoAntes || $proceso != $procesoAntes || $subproceso != $subprocesoAntes || $detalle != $detalleAntes )
                    {
                        $GLOBALS['log']->security("Hubo algún cambio");

                        if ( empty($detalle) ) 
                        {
                            $detalle = '161';
                        }

                        $result = $this->consultaConDetalleM1($tipo, $proceso, $subproceso, $detalle);

                        while ( $row = $GLOBALS['db']->fetchByAssoc($result) ) 
                        {
                            $cantidad = $row['sasa_ans_c']; // cantidad de días a sumar
                            $diferencia = $this->calcularDifM1(); // diferencia horaria
                            
                            if ( !empty($cantidad) ) 
                            {
                                $fechaCreacionCaso = $bean->date_entered;
                                $dia = date("Y-m-d", strtotime($fechaCreacionCaso));
                                $fecha = date("Y-m-d H:i:s", strtotime("$dia 18:00:00"));
                                $fechaCreacion = date("Y-m-d H:i:s", strtotime("+$diferencia hour", strtotime($fecha)));
                                $fechaVencimiento = date("Y-m-d H:i:s", strtotime("+$cantidad day", strtotime($fechaCreacion)));
                                $bean->resolved_datetime = $fechaVencimiento;
                            }
                        }
                    }
                }
            }
            
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Termino acción before_save en CalcularFechaVencimientoCasosM1 Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
        }
    }

    function consultaConDetalleM1($tipo, $proceso, $subproceso, $detalle)
    {
        $query = "SELECT tc.id_c, tc.sasa_ans_c
            FROM sasa_sasa_tipificaciones AS t
            INNER JOIN sasa_sasa_tipificaciones_cstm AS tc ON tc.id_c = t.id
            WHERE tc.sasa_motivo_c = '{$tipo}'
            AND tc.sasa_categoria_c = '{$proceso}'
            AND tc.sasa_proceso_c = '{$subproceso}'
            AND tc.sasa_detalle_c = '{$detalle}'
            AND t.deleted = 0
        LIMIT 1;";

        $resultQuery = $GLOBALS['db']->query($query);
        return $resultQuery;
    }

    function calcularDifM1()
    {
        date_default_timezone_set('UTC'); // configurando por defecto zona horaria Central
        $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

        $timeZoneBogota = 'America/Bogota';
        $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

        $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
        $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

        if ( $dayCentral > $dayColombia ) 
        {
            $GLOBALS['log']->security("Día de fecha central es mayor");
            $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
            $hourColombia = intval($dateColombia->format('h'));
            $dif = $hourCentral - $hourColombia;
            $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        }
        else if( $dayCentral == $dayColombia ) 
        {
            $GLOBALS['log']->security("Días iguales");
            $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
            $hourColombia = intval($dateColombia->format('H:i:s'));
            $dif = $hourCentral - $hourColombia;
            // $GLOBALS['log']->security("Hora Colombia: ".$hourColombia);
            // $GLOBALS['log']->security("Hora central: ".$hourCentral);
            // $GLOBALS['log']->security("Fecha Colombia: ".$dateColombia->format('Y-m-d H:i:s'));
            // $GLOBALS['log']->security("Fecha central: ".$dateCentral);
            // $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        } 
        else 
        {
            $GLOBALS['log']->security("Algo paso");
            $dif = 0;
            $GLOBALS['log']->security("Diferencia horaria: 0");
            return $dif;
        }
    }
}