<?php
 // created: 2022-11-18 20:37:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['labelValue']='Categoría';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['readonly_formula']='';

 ?>