<?php
$viewdefs['Cases'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'primary' => true,
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'escalate-action',
                'event' => 'button:escalate_button:click',
                'name' => 'escalate_button',
                'label' => 'LBL_ESCALATE_BUTTON_LABEL',
                'acl_action' => 'create',
              ),
              2 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              5 => 
              array (
                'type' => 'divider',
              ),
              6 => 
              array (
                'name' => 'create_button',
                'type' => 'rowaction',
                'event' => 'button:create_article_button:click',
                'label' => 'LBL_CREATE_KB_DOCUMENT',
                'acl_module' => 'KBContents',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'divider',
              ),
              8 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'Cases',
                'acl_action' => 'create',
              ),
              10 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:historical_summary_button:click',
                'name' => 'historical_summary_button',
                'label' => 'LBL_HISTORICAL_SUMMARY',
                'acl_action' => 'view',
              ),
              11 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              12 => 
              array (
                'type' => 'divider',
              ),
              13 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              4 => 
              array (
                'name' => 'is_escalated',
                'type' => 'badge',
                'badge_label' => 'LBL_ESCALATED',
                'warning_level' => 'important',
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipodesolicitud_c',
                'label' => 'LBL_SASA_TIPODESOLICITUD_C',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'case_number',
                'readonly' => true,
              ),
              3 => 'priority',
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipoentidad_c',
                'label' => 'LBL_SASA_TIPOENTIDAD_C',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_codigoentidad_c',
                'label' => 'LBL_SASA_CODIGOENTIDAD_C',
              ),
              6 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_codigoqueja_c',
                'label' => 'LBL_SASA_CODIGOQUEJA_C',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'sasa_escalamiento_dcf_c',
                'label' => 'LBL_SASA_ESCALAMIENTO_DCF_C',
              ),
              8 => 'account_name',
              9 => 'primary_contact_name',
              10 => 'source',
              11 => 
              array (
                'readonly' => false,
                'name' => 'sasa_canal_c',
                'label' => 'LBL_SASA_CANAL_C',
              ),
              12 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radsfc_c',
                'label' => 'LBL_SASA_RADSFC_C',
              ),
              13 => 
              array (
                'readonly' => false,
                'name' => 'sasa_raddefensorc_c',
                'label' => 'LBL_SASA_RADDEFENSORC_C',
              ),
              14 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radalianza_c',
                'label' => 'LBL_SASA_RADALIANZA_C',
              ),
              15 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radalianzam2_c',
                'label' => 'LBL_SASA_RADALIANZAM2_C',
              ),
              16 => 
              array (
                'name' => 'sasa_sasa_tipificaciones_cases_1_name',
                'label' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
                'span' => 12,
              ),
              17 => 
              array (
                'readonly' => false,
                'name' => 'sasa_quejaexpres_c',
                'label' => 'LBL_SASA_QUEJAEXPRES_C',
              ),
              18 => 'status',
              19 => 
              array (
                'name' => 'description',
                'nl2br' => true,
                'span' => 12,
              ),
              20 => 
              array (
                'readonly' => false,
                'name' => 'sasa_ente_control_c',
                'label' => 'LBL_SASA_ENTE_CONTROL_C',
              ),
              21 => 
              array (
                'readonly' => false,
                'name' => 'sasa_productodigital_c',
                'label' => 'LBL_SASA_PRODUCTODIGITAL_C',
              ),
              22 => 
              array (
                'readonly' => false,
                'name' => 'sasa_producto_cod_c',
                'label' => 'LBL_SASA_PRODUCTO_COD_C',
              ),
              23 => 
              array (
                'name' => 'work_log',
                'comment' => 'Free-form text used to denote activities of interest',
                'label' => 'LBL_WORK_LOG',
              ),
              24 => 
              array (
                'readonly' => false,
                'name' => 'sasa_negocio_c',
                'label' => 'LBL_SASA_NEGOCIO_C',
              ),
              25 => 
              array (
                'readonly' => false,
                'name' => 'sasa_direccionsfc_c',
                'label' => 'LBL_SASA_DIRECCIONSFC_C',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_sfcfechacreacion_c',
                'label' => 'LBL_SASA_SFCFECHACREACION_C',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_fechaactualizacionsfc_c',
                'label' => 'LBL_SASA_FECHAACTUALIZACIONSFC_C',
              ),
              2 => 
              array (
                'name' => 'resolved_datetime',
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_recepcion_c',
                'label' => 'LBL_SASA_RECEPCION_C',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_puntorecepcion_c',
                'label' => 'LBL_SASA_PUNTORECEPCION_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_admision_c',
                'label' => 'LBL_SASA_ADMISION_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_aceptacion_c',
                'label' => 'LBL_SASA_ACEPTACION_C',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_favoravilidad_c',
                'label' => 'LBL_SASA_FAVORAVILIDAD_C',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_rectificacion_c',
                'label' => 'LBL_SASA_RECTIFICACION_C',
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'sasa_marcacion_c',
                'label' => 'LBL_SASA_MARCACION_C',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tutela_c',
                'label' => 'LBL_SASA_TUTELA_C',
              ),
              8 => 
              array (
                'readonly' => false,
                'name' => 'sasa_desistimiento_c',
                'label' => 'LBL_SASA_DESISTIMIENTO_C',
              ),
              9 => 
              array (
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_estadorespuesta_c',
                'label' => 'LBL_SASA_ESTADORESPUESTA_C',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_estado2_c',
                'label' => 'LBL_SASA_ESTADO2_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_entidadresponsable_c',
                'label' => 'LBL_SASA_ENTIDADRESPONSABLE_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radicadosalida_c',
                'label' => 'LBL_SASA_RADICADOSALIDA_C',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_checkprorroga_c',
                'label' => 'LBL_SASA_CHECKPRORROGA_C',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_diasprorroga_c',
                'label' => 'LBL_SASA_DIASPRORROGA_C',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_prorroga_c',
                'label' => 'LBL_SASA_PRORROGA_C',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_replica_c',
                'label' => 'LBL_SASA_REPLICA_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_is_escalated_c',
                'label' => 'LBL_SASA_IS_ESCALATED_C',
              ),
              3 => 
              array (
                'name' => 'resolution',
                'nl2br' => true,
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_anexoresfinal_c',
                'label' => 'LBL_SASA_ANEXORESFINAL_C',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_docrespfinal_c',
                'label' => 'LBL_SASA_DOCRESPFINAL_C',
              ),
              6 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_sfc_response_api_c',
                'studio' => 'visible',
                'label' => 'LBL_SASA_SFC_RESPONSE_API_C',
              ),
              7 => 
              array (
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 'team_name',
              3 => 'assigned_user_name',
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
