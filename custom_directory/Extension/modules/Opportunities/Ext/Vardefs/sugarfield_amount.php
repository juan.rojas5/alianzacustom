<?php
 // created: 2022-07-28 03:53:01
$dictionary['Opportunity']['fields']['amount']['required']=false;
$dictionary['Opportunity']['fields']['amount']['audited']=false;
$dictionary['Opportunity']['fields']['amount']['massupdate']=false;
$dictionary['Opportunity']['fields']['amount']['comments']='Unconverted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['amount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount']['calculated']=true;
$dictionary['Opportunity']['fields']['amount']['importable']='required';
$dictionary['Opportunity']['fields']['amount']['enable_range_search']='1';
$dictionary['Opportunity']['fields']['amount']['formula']='rollupConditionalSum($revenuelineitems, "likely_case", "sales_stage", forecastSalesStages(true, false))';
$dictionary['Opportunity']['fields']['amount']['enforced']=true;
$dictionary['Opportunity']['fields']['amount']['hidemassupdate']=false;
$dictionary['Opportunity']['fields']['amount']['readonly']=true;
$dictionary['Opportunity']['fields']['amount']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>