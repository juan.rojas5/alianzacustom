<?php
// created: 2019-01-04 21:30:05
$dictionary["sasa_productosafav_sasa_movimientosavdivisas_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_productosafav_sasa_movimientosavdivisas_1' => 
    array (
      'lhs_module' => 'sasa_ProductosAFAV',
      'lhs_table' => 'sasa_productosafav',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_MovimientosAVDivisas',
      'rhs_table' => 'sasa_movimientosavdivisas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_productosafav_sasa_movimientosavdivisas_1_c',
      'join_key_lhs' => 'sasa_produ845etosafav_ida',
      'join_key_rhs' => 'sasa_produd707divisas_idb',
    ),
  ),
  'table' => 'sasa_productosafav_sasa_movimientosavdivisas_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_produ845etosafav_ida' => 
    array (
      'name' => 'sasa_produ845etosafav_ida',
      'type' => 'id',
    ),
    'sasa_produd707divisas_idb' => 
    array (
      'name' => 'sasa_produd707divisas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_produ845etosafav_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_produd707divisas_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_produd707divisas_idb',
      ),
    ),
  ),
);