<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Visitas';
$mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
$mod_strings['LBL_SASA_FECHACONSTITUCION_C'] = 'Fecha de Constitución';
$mod_strings['LBL_SASA_ESTADOCUENTA_C'] = 'Estado de la Cuenta';
$mod_strings['LBL_SASA_ESTADOVINCULACION_C'] = 'Estado de la Vinculación (Eliminar)';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
$mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
$mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos/Ventas Anuales';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSMENSUALES_C'] = 'Ingresos Mensuales (eliminar)';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_PATRIMONIO_C'] = 'Patrimonio Reportado';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_SAS_ACTIVOS_C'] = 'Activos';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_PASIVOS_C'] = 'Pasivos';
$mod_strings['LBL_SASA_OPERAMONEDAEXTRANJERA_C'] = 'Operaciones Moneda Extranjera?';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSOPERACIONES_C'] = 'Ingresos por Operaciones';
$mod_strings['LBL_SASA_FECHAACTUALIZACION_C'] = 'Fecha de Actualización SC';
$mod_strings['LBL_NAME'] = 'Nombre/Razón Social:';
$mod_strings['LBL_PHONE_ALT'] = 'phone_alternate';
$mod_strings['LBL_PHONE_OFFICE'] = 'phone_office';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Dirección alternativa:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Departamento/Estado dirección principal:';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Departamento/Estado dirección alternativa:';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electrónico';
$mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico (eliminar)';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_OWNERSHIP'] = 'Representante Legal:';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Clasificación e Información Financiera';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información del Registro';
$mod_strings['LBL_SASA_DESCRIPCIONCUENTA_C'] = 'Descripción (eliminar)';
$mod_strings['LBL_PHONE_OTHER'] = 'Telefono 2';
$mod_strings['LBL_PHONE_WORK'] = 'Teléfono';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección principal                   **';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Ciudad dirección principal:';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Dirección Alternativa';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Ciudad dirección alternativa:';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País dirección alternativa:';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal dirección alternativa:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Código Postal dirección principal:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País dirección principal:';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Otro';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Otros';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_ACTIVOS_C'] = 'Activos';
$mod_strings['LBL_SASA_IMPORTADOREXPORTADOR_C'] = 'Importador/Exportador';
$mod_strings['LBL_ASSIGNED_USER_ID_OLD'] = 'assigned user id old';
$mod_strings['LBL_SASA_CTRLWF_C'] = 'Ctrl WF';
$mod_strings['LBL_SASA_CTRLANOACTUALIZACION_C'] = 'Ctrl Año actualizacion';
$mod_strings['LBL_RLI_SUBPANEL_TITLE'] = 'Líneas de Ingreso';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento internacional';
$mod_strings['LBL_SASA_INGRESOSVENTASMENSUAL_C'] = 'Ingresos/Ventas Mensuales';
$mod_strings['LBL_SASA_CTRLFILTRO_C'] = 'Ctrl Filtro';
$mod_strings['LBL_LIST_ASSIGNED_USER'] = 'Asignado a';
$mod_strings['LBL_SASA_FECHADEMODIFICACIONSC_C'] = 'Fecha de Modificación SC';
$mod_strings['LBL_SASA_FECHADEVINCULACIONSC_C'] = 'Fecha de Vinculación SC';
$mod_strings['LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE'] = 'Movimientos AV Divisas';
$mod_strings['LBL_CASES_SUBPANEL_TITLE'] = 'PQRs';
$mod_strings['LBL_SASA_GERESENTESEGMENTO_C'] = 'Gerente del segmento';
$mod_strings['LBL_SASA_ASISTENTE_C'] = 'Nombre del asistente';
$mod_strings['LBL_SASA_ESTADOCLIENTE_C'] = 'Estado del cliente';
$mod_strings['LBL_SASA_REPRESENTANTELEGA_C'] = 'Representante Legal';
$mod_strings['LBL_SASA_NOMBRECOMUNICA_C'] = 'Nombre de quien se comunica ';
$mod_strings['LBL_SASA_FECHANAC_C'] = 'Fecha de nacimiento';
$mod_strings['LBL_SASA_RAZON_SOCIAL_C'] = 'Razón social';
$mod_strings['LBL_SASA_NOMBRESEGMENTO_C'] = 'Nombre segmento';
$mod_strings['LBL_SASA_NOMBREPERFILRIESGO_C'] = 'Nombre perfil de riesgo';
