<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAVDivisas/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2018-11-21 22:21:30
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['calculated']='1';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosavdivisas_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAVDivisas/Ext/Vardefs/sugarfield_name.php

 // created: 2018-11-21 22:19:43
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['formula']='$sasa_tipo_c';
$dictionary['sasa_MovimientosAVDivisas']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAVDivisas/Ext/Vardefs/sasa_productosafav_sasa_movimientosavdivisas_1_sasa_MovimientosAVDivisas.php

// created: 2019-01-04 21:30:05
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_produ845etosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_produ845etosafav_ida"] = array (
  'name' => 'sasa_produ845etosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE_ID',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAVDivisas/Ext/Vardefs/accounts_sasa_movimientosavdivisas_1_sasa_MovimientosAVDivisas.php

// created: 2019-01-04 21:41:00
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["accounts_sasa_movimientosavdivisas_1"] = array (
  'name' => 'accounts_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'id_name' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["accounts_sasa_movimientosavdivisas_1_name"] = array (
  'name' => 'accounts_sasa_movimientosavdivisas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  'link' => 'accounts_sasa_movimientosavdivisas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["accounts_sasa_movimientosavdivisas_1accounts_ida"] = array (
  'name' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE_ID',
  'id_name' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  'link' => 'accounts_sasa_movimientosavdivisas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAVDivisas/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_MovimientosAVDivisas']['full_text_search']=true;

?>
