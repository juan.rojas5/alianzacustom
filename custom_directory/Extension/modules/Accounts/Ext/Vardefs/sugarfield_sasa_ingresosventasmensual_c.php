<?php
 // created: 2022-10-25 23:16:58
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['labelValue']='Ingresos/Ventas Mensuales';
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['calculated']='false';
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['dependency']='';
$dictionary['Account']['fields']['sasa_ingresosventasmensual_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>