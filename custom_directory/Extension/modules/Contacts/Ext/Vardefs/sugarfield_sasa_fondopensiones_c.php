<?php
 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['labelValue']='Fondo de Pensiones';
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fondopensiones_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>