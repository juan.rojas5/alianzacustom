<?php
 // created: 2022-06-25 19:24:49

$app_list_strings['sasa_segmento_c_list']=array (
  '' => '',
  'Banca Privada' => 'Banca Privada',
  'Canales' => 'Canales',
  'Corporativo' => 'Corporativo',
  'Empresarial' => 'Empresarial',
  'Pyme' => 'Pyme',
  'Farmers' => 'Farmers',
  'Preferencial' => 'Preferencial',
  'Renta Alta' => 'Renta Alta',
  'Retail Bogota' => 'Retail Bogotá',
  'Retail Nacional' => 'Retail Nacional',
  'Institucional' => 'Institucional',
  'Empresarial y Pyme' => 'Empresarial & Pyme',
  'Parte Activa CXC' => 'Parte Activa CXC',
  'Banca Privada 2' => 'Banca Privada 2',
  'Renta Alta 1' => 'Renta Alta 1',
  'Renta Alta 2' => 'Renta Alta 2',
);