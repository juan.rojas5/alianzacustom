<?php
// created: 2019-01-04 21:39:25
$dictionary["Account"]["fields"]["accounts_sasa_movimientosaf_1"] = array (
  'name' => 'accounts_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAF',
  'bean_name' => 'sasa_MovimientosAF',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_movimientosaf_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
