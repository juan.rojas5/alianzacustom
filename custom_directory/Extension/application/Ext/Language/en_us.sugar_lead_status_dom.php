<?php
 // created: 2018-05-23 16:05:07

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'New',
  'Assigned' => 'Assigned',
  'In Process' => 'In Process',
  'Converted' => 'Converted',
  'Recycled' => 'Recycled',
  'Dead' => 'Dead',
);