<?php
$dependencies['Leads']['SetOptionDepartamento19'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Huila")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Palestina","Santa Maria","Acevedo","Agrado","Aipe","Algeciras","Altamira","Baraya","Campoalegre","Colombia","Elias","Garzon","Gigante","Guadalupe","Hobo","Iquira","Isnos","La Argentina","La Plata","Nataga","Neiva","Oporapa","Paicol","Palermo","Pital","Pitalito","Rivera","Saladoblanco","San Agustin","Suaza","Tarqui","Tello","Teruel","Tesalia","Timana","Villavieja","Yaguara")',
           'labels' => 'createList("","Palestina","Santa María","Acevedo","Agrado","Aipe","Algeciras","Altamira","Baraya","Campoalegre","Colombia","Elías","Garzón","Gigante","Guadalupe","Hobo","Iquira","Isnos","La Argentina","La Plata","Nátaga","Neiva","Oporapa","Paicol","Palermo","Pital","Pitalito","Rivera","Saladoblanco","San Agustín","Suaza","Tarqui","Tello","Teruel","Tesalia","Timaná","Villavieja","Yaguará")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
