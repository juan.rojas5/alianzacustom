<?php
// created: 2023-02-06 20:16:31
$dictionary["User"]["fields"]["users_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'users_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'bean_name' => 'sasa1_Unidades_de_negocio_por_',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_USERS_TITLE',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link-type' => 'many',
  'side' => 'left',
);
