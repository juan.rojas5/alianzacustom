<?php
// created: 2023-02-03 09:39:47
$viewdefs['Users']['mobile']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'link' => true,
          'orderBy' => 'last_name',
          'default' => true,
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'first_name',
            1 => 'last_name',
            2 => 'salutation',
          ),
        ),
        1 => 
        array (
          'name' => 'title',
          'label' => 'LBL_TITLE',
          'default' => true,
          'enabled' => true,
        ),
        2 => 
        array (
          'name' => 'department',
          'label' => 'LBL_DEPARTMENT',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'email',
          'label' => 'LBL_EMAIL',
          'sortable' => false,
          'link' => true,
          'customCode' => '{$EMAIL_LINK}{$EMAIL}</a>',
          'default' => true,
          'enabled' => true,
        ),
        4 => 
        array (
          'name' => 'reports_to_name',
          'label' => 'LBL_REPORTS_TO_NAME',
          'enabled' => true,
          'id' => 'REPORTS_TO_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'phone_home',
          'label' => 'LBL_HOME_PHONE',
          'default' => false,
          'enabled' => true,
        ),
        6 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'default' => false,
          'enabled' => true,
        ),
        7 => 
        array (
          'name' => 'phone_other',
          'label' => 'LBL_WORK_PHONE',
          'default' => false,
          'enabled' => true,
        ),
        8 => 
        array (
          'name' => 'phone_work',
          'label' => 'LBL_OFFICE_PHONE',
          'default' => false,
          'enabled' => true,
        ),
        9 => 
        array (
          'name' => 'phone_fax',
          'label' => 'LBL_FAX_PHONE',
          'default' => false,
          'enabled' => true,
        ),
        10 => 
        array (
          'name' => 'address_street',
          'label' => 'LBL_PRIMARY_ADDRESS_STREET',
          'default' => false,
          'enabled' => true,
        ),
        11 => 
        array (
          'name' => 'address_city',
          'label' => 'LBL_PRIMARY_ADDRESS_CITY',
          'default' => false,
          'enabled' => true,
        ),
        12 => 
        array (
          'name' => 'address_state',
          'label' => 'LBL_PRIMARY_ADDRESS_STATE',
          'default' => false,
          'enabled' => true,
        ),
        13 => 
        array (
          'name' => 'address_postalcode',
          'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
          'default' => false,
          'enabled' => true,
        ),
        14 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'default' => false,
          'readonly' => true,
          'enabled' => true,
        ),
      ),
    ),
  ),
);