<?php

$job_strings[] = 'MarkCFVFieldCases';

function MarkCFVFieldCases()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Marcado de campo CFV en Casos.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    $dif = calcularDifInCases();

    date_default_timezone_set('America/Bogota'); // configurando por defecto zona horaria de Colombia

    $dateYesterday = date('Y-m-d', strtotime('-1 day')); // obtener fecha día anterior
    $dateToday = date('Y-m-d');

    // Fecha inicio y fin para tareas vencidas
    $startDateMoment1 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateYesterday 07:30:00"))); // establecer fecha inicio
    $endDateMoment1 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment1))); // establecer fecha fin

    // fecha inicio y fin para tareas proximas a vencer
    $startDateMoment2 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateToday 07:30:00")));
    $endDateMoment2 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment2)));

    $queryVencidas = "SELECT c.id, c.name, c.resolved_datetime, cc.sasa_cfv_c, cc.sasa_tipodesolicitud_c
        FROM cases AS c
        INNER JOIN cases_cstm AS cc ON cc.id_c = c.id
        WHERE c.resolved_datetime > '{$startDateMoment1}'
        AND c.resolved_datetime <= '{$endDateMoment1}'
        AND cc.sasa_cfv_c IS null AND c.deleted = 0
    LIMIT 1000;";

    $queryAVencer = "SELECT c.id, c.name, c.resolved_datetime, cc.sasa_cfv_c, cc.sasa_tipodesolicitud_c
        FROM cases AS c
        INNER JOIN cases_cstm AS cc ON cc.id_c = c.id
        WHERE c.resolved_datetime > '{$startDateMoment2}'
        AND c.resolved_datetime <= '{$endDateMoment2}'
        AND cc.sasa_cfv_c IS null AND c.deleted = 0
    LIMIT 1000;";

    $resultVencidas = $GLOBALS['db']->query($queryVencidas);
    $resultAVencer = $GLOBALS['db']->query($queryAVencer);

    $contQuery = mysqli_num_rows($resultVencidas);
    $contQuery2 = mysqli_num_rows($resultAVencer);

    $GLOBALS['log']->security("Cantidad de casos vencidos ---> ".$contQuery."\n");
    $GLOBALS['log']->security("Cantidad de casos a vencer ---> ".$contQuery2."\n");

    while ($row = $GLOBALS['db']->fetchByAssoc($resultVencidas)) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' && $row['sasa_tipodesolicitud_c'] != 'quejas' && $row['sasa_tipodesolicitud_c'] != '' && $row['sasa_tipodesolicitud_c'] != null ) 
        {
            $bean = BeanFactory::getBean("Cases", $row['id']);
            $bean->sasa_cfv_c = 1;
            $bean->save();
        }
    }

    while ( $row = $GLOBALS['db']->fetchByAssoc($resultAVencer) ) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' && $row['sasa_tipodesolicitud_c'] != 'quejas' && $row['sasa_tipodesolicitud_c'] != '' && $row['sasa_tipodesolicitud_c'] != null ) 
        {
            $bean = BeanFactory::getBean("Cases", $row['id']);
            $bean->sasa_cfv_c = 2;
            $bean->save();
        }
    }

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Marcado de campo CFV en Casos.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

function calcularDifInCases()
{
    $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

    $timeZoneBogota = 'America/Bogota';
    $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

    $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
    $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

    if ( $dayCentral > $dayColombia ) 
    {
        $GLOBALS['log']->security("Día de fecha central es mayor");
        $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
        $hourColombia = intval($dateColombia->format('h'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    }
    else if( $dayCentral == $dayColombia ) 
    {
        $GLOBALS['log']->security("Días iguales");
        $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
        $hourColombia = intval($dateColombia->format('H:i:s'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    } 
    else 
    {
        $GLOBALS['log']->security("Algo paso");
        $dif = 0;
        $GLOBALS['log']->security("Diferencia horaria: 0");
        return $dif;
    }
}