<?php
$dependencies['Leads']['SetOptionDepartamento26'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Quindio")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Cordoba","Buenavista","Armenia","Calarca","Circasia","Filandia","Genova","La Tebaida","Montenegro","Pijao","Quimbaya","Salento")',
           'labels' => 'createList("","Córdoba","Buenavista","Armenia","Calarcá","Circasia","Filandia","Génova","La Tebaida","Montenegro","Pijao","Quimbaya","Salento")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
