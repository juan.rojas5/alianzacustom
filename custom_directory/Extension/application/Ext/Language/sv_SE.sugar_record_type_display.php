<?php
 // created: 2018-01-29 21:13:45

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Affärsmöjlighet',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Bugg',
  'Project' => 'Projekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Projektuppgift',
  'Tasks' => 'Uppgift',
  'KBContents' => 'Kunskapsbas',
  'RevenueLineItems' => 'Intäktsposter',
);