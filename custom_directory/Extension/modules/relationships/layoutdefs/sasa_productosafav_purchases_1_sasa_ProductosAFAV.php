<?php
 // created: 2023-02-06 20:21:32
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_purchases_1'] = array (
  'order' => 100,
  'module' => 'Purchases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_purchases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
