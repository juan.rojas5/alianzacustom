<?php
// created: 2019-01-04 22:02:19
$viewdefs['sasa_MovimientosAVDivisas']['base']['view']['subpanel-for-sasa_productosafav-sasa_productosafav_sasa_movimientosavdivisas_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_tipo_c',
          'label' => 'LBL_SASA_TIPO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'accounts_sasa_movimientosavdivisas_1_name',
          'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
          'enabled' => true,
          'id' => 'ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1ACCOUNTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);