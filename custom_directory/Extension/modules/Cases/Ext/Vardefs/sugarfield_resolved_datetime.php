<?php
 // created: 2022-05-24 20:19:34
$dictionary['Case']['fields']['resolved_datetime']['massupdate']=true;
$dictionary['Case']['fields']['resolved_datetime']['hidemassupdate']=false;
$dictionary['Case']['fields']['resolved_datetime']['comments']='Date when an issue is resolved';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['resolved_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolved_datetime']['calculated']=false;
$dictionary['Case']['fields']['resolved_datetime']['enable_range_search']=false;

 ?>