<?php
// created: 2019-01-04 21:25:02
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_saldosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAF',
  'bean_name' => 'sasa_SaldosAF',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
