<?php
 // created: 2018-05-22 21:58:40
$dictionary['Lead']['fields']['do_not_call']['default']=false;
$dictionary['Lead']['fields']['do_not_call']['audited']=false;
$dictionary['Lead']['fields']['do_not_call']['massupdate']=false;
$dictionary['Lead']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Lead']['fields']['do_not_call']['unified_search']=false;
$dictionary['Lead']['fields']['do_not_call']['calculated']=false;

 ?>