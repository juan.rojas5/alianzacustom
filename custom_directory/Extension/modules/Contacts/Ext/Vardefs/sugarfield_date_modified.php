<?php
 // created: 2018-05-30 20:33:59
$dictionary['Contact']['fields']['date_modified']['audited']=true;
$dictionary['Contact']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_modified']['calculated']=false;
$dictionary['Contact']['fields']['date_modified']['enable_range_search']='1';

 ?>