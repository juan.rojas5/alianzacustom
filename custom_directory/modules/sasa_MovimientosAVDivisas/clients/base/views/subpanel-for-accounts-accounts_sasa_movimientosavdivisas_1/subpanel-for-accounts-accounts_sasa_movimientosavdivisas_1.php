<?php
// created: 2019-01-04 21:55:18
$viewdefs['sasa_MovimientosAVDivisas']['base']['view']['subpanel-for-accounts-accounts_sasa_movimientosavdivisas_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_name',
          'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
          'enabled' => true,
          'id' => 'SASA_PRODU845ETOSAFAV_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_moneda_c',
          'label' => 'LBL_SASA_MONEDA_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_tipo_c',
          'label' => 'LBL_SASA_TIPO_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_fecha_c',
          'label' => 'LBL_SASA_FECHA_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_tasadivisadolar_c',
          'label' => 'LBL_SASA_TASADIVISADOLAR_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_tasapesodolar_c',
          'label' => 'LBL_SASA_TASAPESODOLAR_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_cantidad_c',
          'label' => 'LBL_SASA_CANTIDAD_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_valorbruto_c',
          'label' => 'LBL_SASA_VALORBRUTO_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_ocobros_c',
          'label' => 'LBL_SASA_OCOBROS_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'sasa_retefte_c',
          'label' => 'LBL_SASA_RETEFTE_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'sasa_iva_c',
          'label' => 'LBL_SASA_IVA_C',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'sasa_reteiva_c',
          'label' => 'LBL_SASA_RETEIVA_C',
          'enabled' => true,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'sasa_valorneto_c',
          'label' => 'LBL_SASA_VALORNETO_C',
          'enabled' => true,
          'default' => true,
        ),
        15 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);