<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/fbsg_ConstantContactIntegration/include/ConstantContact.php');

class fbsg_cc_ProspectListsLogic
{

    protected function GetSyncInfo($bean)
    {
        global $log;
        if ($bean->sync_to_cc == false || $bean->name === null || $bean->name == '') {
            return false;
        }

        $cc_acc = SugarCC::GetOnlyCC();
        if (!$cc_acc) {
            return false;
        }

        $cc = new CCBasic($cc_acc->name, $cc_acc->password);
        $ccList = new SugarCCList($cc);
        return $ccList;
    }

    protected function IsSecondSave()
    {
        return isset($_REQUEST['fbsg_save']) && $_REQUEST['fbsg_save'] === false;
    }

    public function after_save(&$bean, $event, $arguments)
    {
        $requestAction = '';
        $viewed = 0;

        if (isset($_REQUEST) && isset($_REQUEST['action'])) {
            $requestAction = $_REQUEST['action'];
        }

        // TH Update: adding in $_REQUEST['viewed'] to make it compatible with Sugar 7
        if (isset($_REQUEST) && isset($_REQUEST['viewed'])) {
            $viewed = $_REQUEST['viewed'];
        }

        // If the action is Save or it was saved from the UI proceed to check if it is a CC list
        if (($requestAction === 'Save' || $viewed == 1) && !$this->IsSecondSave()) {
            $list = $this->GetSyncInfo($bean);
            if ($list !== false) $list->Sync($bean);
        }
        // This should only fire for target lists that were related to CC
        if(isset($bean->cc_id) && strlen($bean->cc_id) > 0){
            $this->checkDropdownStatus($bean->id);
        }
    }

    public function after_delete($bean, $event, $arguments)
    {
        global $db;
        // This should only fire for target lists that were related to CC
        if(isset($bean->cc_id) && strlen($bean->cc_id) > 0){
            // We just want to retrieve the related records that were deleted today
            $date = date('Y-m-d') . ' 00:00:00';
            $q = "SELECT * FROM prospect_lists_prospects WHERE prospect_list_id = '{$bean->id}' AND date_modified >= '{$date}'";
            $results = $db->query($q);

            while ($row = $db->fetchByAssoc($results)) {
                $row['related_module'] = $row['related_type'];
                $this->after_relationship_delete($bean, 'after_relationship_delete', $row);
            }
            $this->checkDropdownStatus($bean->id, true);
        }
    }

    /**
     * @param string $bean->id  This will be the id of the target list
     * @param bool   $delete    This will let us know if the item needs to be removed from the list
     *
     */
    protected function checkDropdownStatus($bean_id = null, $delete = false){
        global $db, $app_list_strings;

        $cc_list_dom = isset($app_list_strings['cc_list_dom']) ? $app_list_strings['cc_list_dom'] : false;

        if($bean_id !== null && $cc_list_dom){
            $q = "SELECT * FROM prospect_lists WHERE id = '{$bean_id}' ";
            $q .= ($delete) ? "AND deleted = 1 " : "AND deleted = 0 "; // If the target list has been deleted we need to retrieve a deleted record
            $results = $db->query($q);

            if($row = $db->fetchByAssoc($results)){
                if( $delete && isset($cc_list_dom[(string) $row['cc_id']]) ){
                    unset($cc_list_dom[(string) $row['cc_id']]);
                    // This will force a full rebuild of the cc_list_dom list
                    SugarCC::SetDropdown('cc_list_dom', $cc_list_dom, false);

                } else {
                    // Only update the cc_list_dom list IF the item doesn't exist OR the display has been changed
                    if( !isset($cc_list_dom[(string) $row['cc_id']]) || $cc_list_dom[(string) $row['cc_id']] !== $row['name'] ){
                        $cc_list_dom[(string) $row['cc_id']] = $row['name'];
                        SugarCC::SetDropdown('cc_list_dom', $cc_list_dom);
                    }
                }
            }
        }
    }


    // Start after_relationship_add

    public function after_relationship_add($bean, $event, $arguments){
        if(in_array($arguments['related_module'], array('Accounts', 'Contacts', 'Leads', 'Prospects'))){
            $related_bean = \BeanFactory::getBean($arguments['related_module'], $arguments['related_id'],  array('strict_retrieve' => true, 'disable_row_level_security' => true));
            if($related_bean !== null && $related_bean->getFieldDefinition('cc_lists') !== false){
                $exploded_multiselect = (strlen($related_bean->cc_lists) > 1) ? SugarCC::ExplodeCaretDelimited($related_bean->cc_lists) : array();

                if(!in_array($bean->cc_id, $exploded_multiselect)){
                    $exploded_multiselect[] = $bean->cc_id;
                    $related_bean->cc_lists = "^" . implode("^,^", $exploded_multiselect) . "^";
                    // If the related record is currently in the middle of a save operation skip the bean save as it will cause conflicts (this usually occurs if someone is making a new (example) contact record from the contact subpanel of a target list record
                    if(!$related_bean->in_save){
                        $related_bean->save();
                    }
                }
            }
        }
    }

    // End after_relationship_add

    // Start after_relationship_delete

    public function after_relationship_delete($bean, $event, $arguments){
        if(in_array($arguments['related_module'], array('Accounts', 'Contacts', 'Leads', 'Prospects'))){
            $related_bean = \BeanFactory::retrieveBean($arguments['related_module'], $arguments['related_id'], array(), true);

            if($related_bean !== null && $related_bean->getFieldDefinition('cc_lists') !== false){
                $exploded_multiselect = (strlen($related_bean->cc_lists) > 1) ? SugarCC::ExplodeCaretDelimited($related_bean->cc_lists) : array();

                $list_index = array_search($bean->cc_id, $exploded_multiselect);
                if($list_index !== false){
                    unset($exploded_multiselect[$list_index]);
                    $lists_string = (count($exploded_multiselect) > 0) ? ("^" . implode("^,^", $exploded_multiselect) . "^") : ''; // When there are NO items left selected then we must set it to '' NOT null
                    $related_bean->cc_lists = $lists_string;
                    $related_bean->save();
                }
            }
        }
    }

    // End after_relationship_delete

}
