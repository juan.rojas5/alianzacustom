<?php
 // created: 2018-08-23 11:22:55
$dictionary['User']['fields']['external_auth_only']['default']=false;
$dictionary['User']['fields']['external_auth_only']['audited']=false;
$dictionary['User']['fields']['external_auth_only']['duplicate_merge']='enabled';
$dictionary['User']['fields']['external_auth_only']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['external_auth_only']['merge_filter']='disabled';
$dictionary['User']['fields']['external_auth_only']['reportable']=true;
$dictionary['User']['fields']['external_auth_only']['unified_search']=false;
$dictionary['User']['fields']['external_auth_only']['calculated']=false;

 ?>