<?php
 // created: 2022-10-25 23:16:54
$dictionary['Account']['fields']['sasa_asistente_c']['labelValue']='Nombre del asistente';
$dictionary['Account']['fields']['sasa_asistente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_asistente_c']['enforced']='';
$dictionary['Account']['fields']['sasa_asistente_c']['dependency']='';
$dictionary['Account']['fields']['sasa_asistente_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_asistente_c']['readonly_formula']='';

 ?>