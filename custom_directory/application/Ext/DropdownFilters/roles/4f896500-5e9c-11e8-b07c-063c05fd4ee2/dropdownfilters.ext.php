<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/lead_source_dom.php

// created: 2018-05-23 21:15:37
$role_dropdown_filters['lead_source_dom'] = array (
  '' => true,
  'Cold Call' => true,
  'Existing Customer' => true,
  'Self Generated' => true,
  'Employee' => true,
  'Partner' => false,
  'Public Relations' => true,
  'Direct Mail' => true,
  'Conference' => true,
  'Trade Show' => true,
  'Web Site' => true,
  'Word of mouth' => true,
  'Email' => true,
  'Campaign' => true,
  'Support Portal User Registration' => false,
  'Other' => true,
  'Socio' => true,
  'Correo Electronico' => true,
  'Regristro de Usuario del Portal de Soporte' => true,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/lead_status_dom.php

// created: 2018-07-21 20:33:46
$role_dropdown_filters['lead_status_dom'] = array (
  '' => false,
  'New' => false,
  'Assigned' => true,
  'In Process' => false,
  'Converted' => false,
  'Recycled' => false,
  'Dead' => true,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_perfilriesgo_c_list.php

// created: 2018-08-11 23:09:56
$role_dropdown_filters['sasa_perfilriesgo_c_list'] = array (
  '' => true,
  'Conservador' => true,
  'Mayor Riesgo' => true,
  'Moderado' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/account_type_dom.php

// created: 2018-08-11 23:13:53
$role_dropdown_filters['account_type_dom'] = array (
  '' => true,
  'Customer' => true,
  'Prospect' => true,
  'No Interesado' => true,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/industry_dom.php

// created: 2018-08-11 23:17:37
$role_dropdown_filters['industry_dom'] = array (
  '' => true,
  'Agropecuario' => true,
  'Atencion Sanitaria' => true,
  'Banking' => true,
  'Biotechnology' => true,
  'Ciencia y Tecnica' => true,
  'Communications' => true,
  'Construction' => true,
  'Consulting' => true,
  'Education' => true,
  'Electronics' => true,
  'Energy' => true,
  'Entertainment' => true,
  'Shipping' => true,
  'Manufacturing' => true,
  'Finance' => true,
  'Floricultura' => true,
  'Government' => true,
  'Hospitality' => true,
  'Engineering' => true,
  'Machinery' => true,
  'Environmental' => true,
  'Media' => true,
  'Minera y Petroleos' => true,
  'Recreation' => true,
  'Other' => true,
  'Chemicals' => true,
  'Saneamiento Ambiental' => true,
  'Healthcare' => true,
  'Insurance' => true,
  'Utilities' => true,
  'Not For Profit' => true,
  'Technology' => true,
  'Telecommunications' => true,
  'Apparel' => true,
  'Transportation' => true,
  'Retail' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_sector_c_list.php

// created: 2018-08-11 23:22:19
$role_dropdown_filters['sasa_sector_c_list'] = array (
  '' => true,
  'Privada' => true,
  'Publica' => true,
  'Mixta' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_segmentocuentas_c_list.php

// created: 2018-08-11 23:23:19
$role_dropdown_filters['sasa_segmentocuentas_c_list'] = array (
  '' => true,
  'Preferenciales' => true,
  'Renta Alta' => true,
  'Banca Privada' => true,
  'PYME' => true,
  'Empresarial' => true,
  'Banca Corporativa' => true,
  'Institucional' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_tipoidentificacion_c_list.php

// created: 2018-08-11 23:24:14
$role_dropdown_filters['sasa_tipoidentificacion_c_list'] = array (
  '' => true,
  'Cedula de Ciudadania' => true,
  'Cedula de Extranjeria' => true,
  'Pasaporte' => true,
  'Nit' => true,
  'NUIP' => true,
  'Registro Civil' => true,
  'Tarjeta de identidad' => true,
  'Carne Diplomatico' => true,
  'Fideicomiso' => true,
  'Sociedad Extranjera' => true,
  'Otro' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_ocupacion_c_list.php

// created: 2018-08-11 23:25:06
$role_dropdown_filters['sasa_ocupacion_c_list'] = array (
  '' => true,
  'Asalariado' => true,
  'Comercial' => true,
  'Construccion' => true,
  'Empleado' => true,
  'Estudiante' => true,
  'Financiero' => true,
  'Hogar' => true,
  'Independiente' => true,
  'Industrial' => true,
  'Militar' => true,
  'Pensionado' => true,
  'Religioso' => true,
  'Rentista' => true,
  'Servicios' => true,
  'Socio' => true,
  'Transporte' => true,
  'Otros' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_pais_c_list.php

// created: 2018-08-11 23:26:13
$role_dropdown_filters['sasa_pais_c_list'] = array (
  '' => true,
  'Colombia' => true,
  'Afganistan' => true,
  'Albania' => true,
  'Alemania' => true,
  'Andorra' => true,
  'Angola' => true,
  'Antigua y Barbuda' => true,
  'Arabia Saudita' => true,
  'Argelia' => true,
  'Argentina' => true,
  'Armenia' => true,
  'Australia' => true,
  'Austria' => true,
  'Azerbaiyan' => true,
  'Bahamas' => true,
  'Banglades' => true,
  'Barbados' => true,
  'Barein' => true,
  'Belgica' => true,
  'Belice' => true,
  'Benin' => true,
  'Bielorrusia' => true,
  'Birmania' => true,
  'Bolivia' => true,
  'Bosnia y Herzegovina' => true,
  'Botsuana' => true,
  'Brasil' => true,
  'Brunei' => true,
  'Bulgaria' => true,
  'Burkina Faso' => true,
  'Burundi' => true,
  'Butan' => true,
  'Cabo Verde' => true,
  'Camboya' => true,
  'Camerun' => true,
  'Canada' => true,
  'Catar' => true,
  'Chad' => true,
  'Chile' => true,
  'China' => true,
  'Chipre' => true,
  'Ciudad del Vaticano' => true,
  'Comoras' => true,
  'Corea del Norte' => true,
  'Corea del Sur' => true,
  'Costa de Marfil' => true,
  'Costa Rica' => true,
  'Croacia' => true,
  'Cuba' => true,
  'Dinamarca' => true,
  'Dominica' => true,
  'Ecuador' => true,
  'Egipto' => true,
  'El Salvador' => true,
  'Emiratos Arabes Unidos' => true,
  'Eritrea' => true,
  'Eslovaquia' => true,
  'Eslovenia' => true,
  'España' => true,
  'Estados Unidos' => true,
  'Estonia' => true,
  'Etiopia' => true,
  'Filipinas' => true,
  'Finlandia' => true,
  'Fiyi' => true,
  'Francia' => true,
  'Gabon' => true,
  'Gambia' => true,
  'Georgia' => true,
  'Ghana' => true,
  'Granada' => true,
  'Grecia' => true,
  'Guatemala' => true,
  'Guyana' => true,
  'Guinea' => true,
  'Guinea ecuatorial' => true,
  'Guinea-Bisau' => true,
  'Haiti' => true,
  'Honduras' => true,
  'Hungria' => true,
  'India' => true,
  'Indonesia' => true,
  'Irak' => true,
  'Iran' => true,
  'Irlanda' => true,
  'Islandia' => true,
  'Islas Marshall' => true,
  'Islas Salomon' => true,
  'Israel' => true,
  'Italia' => true,
  'Jamaica' => true,
  'Japon' => true,
  'Jordania' => true,
  'Kazajistan' => true,
  'Kenia' => true,
  'Kirguistan' => true,
  'Kiribati' => true,
  'Kuwait' => true,
  'Laos' => true,
  'Lesoto' => true,
  'Letonia' => true,
  'Libano' => true,
  'Liberia' => true,
  'Libia' => true,
  'Liechtenstein' => true,
  'Lituania' => true,
  'Luxemburgo' => true,
  'Madagascar' => true,
  'Malasia' => true,
  'Malaui' => true,
  'Maldivas' => true,
  'Mali' => true,
  'Malta' => true,
  'Marruecos' => true,
  'Mauricio' => true,
  'Mauritania' => true,
  'Mexico' => true,
  'Micronesia' => true,
  'Moldavia' => true,
  'Monaco' => true,
  'Mongolia' => true,
  'Montenegro' => true,
  'Mozambique' => true,
  'Namibia' => true,
  'Nauru' => true,
  'Nepal' => true,
  'Nicaragua' => true,
  'Niger' => true,
  'Nigeria' => true,
  'Noruega' => true,
  'Nueva Zelanda' => true,
  'Oman' => true,
  'Paises Bajos' => true,
  'Pakistan' => true,
  'Palaos' => true,
  'Panama' => true,
  'Papua Nueva Guinea' => true,
  'Paraguay' => true,
  'Peru' => true,
  'Polonia' => true,
  'Portugal' => true,
  'Reino Unido' => true,
  'Republica Centroafricana' => true,
  'Republica Checa' => true,
  'Republica de Macedonia' => true,
  'Republica del Congo' => true,
  'Republica Democratica del Congo' => true,
  'Republica Dominicana' => true,
  'Republica Sudafricana' => true,
  'Ruanda' => true,
  'Rumania' => true,
  'Rusia' => true,
  'Samoa' => true,
  'San Cristobal y Nieves' => true,
  'San Marino' => true,
  'San Vicente y las Granadinas' => true,
  'Santa Lucia' => true,
  'Santo Tome y Principe' => true,
  'Senegal' => true,
  'Serbia' => true,
  'Seychelles' => true,
  'Sierra Leona' => true,
  'Singapur' => true,
  'Siria' => true,
  'Somalia' => true,
  'Sri Lanka' => true,
  'Suazilandia' => true,
  'Sudan' => true,
  'Sudan del Sur' => true,
  'Suecia' => true,
  'Suiza' => true,
  'Surinam' => true,
  'Tailandia' => true,
  'Tanzania' => true,
  'Tayikistan' => true,
  'Timor Oriental' => true,
  'Togo' => true,
  'Tonga' => true,
  'Trinidad y Tobago' => true,
  'Tunez' => true,
  'Turkmenistan' => true,
  'Turquia' => true,
  'Tuvalu' => true,
  'Ucrania' => true,
  'Uganda' => true,
  'Uruguay' => true,
  'Uzbekistan' => true,
  'Vanuatu' => true,
  'Venezuela' => true,
  'Vietnam' => true,
  'Yemen' => true,
  'Yibuti' => true,
  'Zambia' => true,
  'Zimbabue' => true,
  'Otro' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_tipopersona_c_list.php

// created: 2018-08-22 23:53:40
$role_dropdown_filters['sasa_tipopersona_c_list'] = array (
  '' => true,
  'Natural' => true,
  'Juridica' => true,
  'Fideicomiso' => true,
  'No Registra' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f896500-5e9c-11e8-b07c-063c05fd4ee2/sasa_actividadeconomica_c_list.php

// created: 2018-08-24 15:08:57
$role_dropdown_filters['sasa_actividadeconomica_c_list'] = array (
  '' => true,
  'Agroindustria' => true,
  'Comercial' => true,
  'Comunicacion' => true,
  'Construccion' => true,
  'Educacion' => true,
  'Financiero' => true,
  'Industrial' => true,
  'Salud' => true,
  'Servicios' => true,
  'Transporte' => true,
  'Otros' => true,
  'No Registra' => false,
);
?>
