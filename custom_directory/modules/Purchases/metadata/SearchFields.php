<?php
// created: 2023-02-21 14:07:23
$searchFields['Purchases'] = array (
  'name' => 
  array (
    'query_type' => 'default',
    'force_unifiedsearch' => true,
  ),
  'range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_total_revenue' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_total_revenue' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_total_revenue' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_total_quantity' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_total_quantity' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_total_quantity' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_pli_count' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_pli_count' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_pli_count' => 
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
);