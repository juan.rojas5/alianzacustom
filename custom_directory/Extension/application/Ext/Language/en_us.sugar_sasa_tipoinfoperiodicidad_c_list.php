<?php
 // created: 2018-06-26 16:26:40

$app_list_strings['sasa_tipoinfoperiodicidad_c_list']=array (
  '' => '',
  'English man en Bogota trimestral' => 'English-man en Bogotá (trimestral)',
  'Tenedores de TES mensual' => 'Tenedores de TES (mensual)',
  'Emisiones corporativas deuda privada trimestral' => 'Emisiones corporativas deuda privada (trimestral)',
  'Informe del PEI mensual' => 'Informe del PEI (mensual)',
  'Mensual y flujos de acciones mensual' => 'Mensual y flujos de acciones (mensual)',
  'Informes corporativos trimestral' => 'Informes corporativos (trimestral)',
  'Rebalanceo del indice COLCAP trimestral' => 'Rebalanceo del índice COLCAP (trimestral)',
  'Pasajeros de Avianca mensual' => 'Pasajeros de Avianca (mensual)',
  'Saturday Live Semanal los sabados' => 'Saturday Live (Semanal los sábados)',
);