<?php
// created: 2019-03-08 18:57:52
$dictionary["sasa_productosafav_sasap_presupuestoxproducto_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_productosafav_sasap_presupuestoxproducto_1' => 
    array (
      'lhs_module' => 'sasa_ProductosAFAV',
      'lhs_table' => 'sasa_productosafav',
      'lhs_key' => 'id',
      'rhs_module' => 'sasaP_PresupuestoxProducto',
      'rhs_table' => 'sasap_presupuestoxproducto',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_productosafav_sasap_presupuestoxproducto_1_c',
      'join_key_lhs' => 'sasa_produff64tosafav_ida',
      'join_key_rhs' => 'sasa_produ6f56roducto_idb',
    ),
  ),
  'table' => 'sasa_productosafav_sasap_presupuestoxproducto_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_produff64tosafav_ida' => 
    array (
      'name' => 'sasa_produff64tosafav_ida',
      'type' => 'id',
    ),
    'sasa_produ6f56roducto_idb' => 
    array (
      'name' => 'sasa_produ6f56roducto_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_produff64tosafav_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_produ6f56roducto_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_produ6f56roducto_idb',
      ),
    ),
  ),
);