<?php
 // created: 2022-11-18 20:38:51
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['labelValue']='Estado_de_caso';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['readonly_formula']='';

 ?>