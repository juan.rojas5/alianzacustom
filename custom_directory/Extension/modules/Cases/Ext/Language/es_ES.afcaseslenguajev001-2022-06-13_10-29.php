<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono Fijo';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Empresa';
$mod_strings['LBL_DESCRIPTION'] = 'Notas';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección Alternativa';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_LEAD_SOURCE_DESCRIPTION'] = 'Descripción de Toma de Contacto:';
$mod_strings['LBL_REFERED_BY'] = 'Referido Por:';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Queja';
$mod_strings['LNK_CREATE'] = 'Nuevo Queja';
$mod_strings['LBL_MODULE_NAME'] = 'Quejas';
$mod_strings['LBL_CASE'] = 'Queja:';
