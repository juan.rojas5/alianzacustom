<?php
 // created: 2018-06-13 16:19:52
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['required']=true;
$dictionary['Contact']['fields']['salutation']['audited']=false;
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Contact']['fields']['salutation']['calculated']=false;
$dictionary['Contact']['fields']['salutation']['dependency']=false;
$dictionary['Contact']['fields']['salutation']['pii']=false;
$dictionary['Contact']['fields']['salutation']['options']='salutation_list';

 ?>