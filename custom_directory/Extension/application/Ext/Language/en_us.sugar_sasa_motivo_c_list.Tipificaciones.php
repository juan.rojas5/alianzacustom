<?php 
$app_list_strings['sasa_motivo_c_list'] = array (
  '' => '',
  401 => 'Cobro de penalidad por desistimiento',
  402 => 'Inconsistencia o falta de pago de la cláusula penal por incumplimiento o demora del proyecto inmobiliario',
  403 => 'Incumplimiento de inversiones con políticas definidas',
  404 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  405 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  406 => 'Inconsistencia en el valor de rendimientos',
  407 => 'Redención de derechos o participaciones - cancelacion del producto o servicio',
  408 => 'Incumplimiento del deber legal de rendición de cuentas (información periódica)',
  499 => 'Otros motivos',
  906 => 'Mal trato por parte de un funcionario',
  907 => 'Mal trato por parte del asesor comercial o proveedor',
  965 => 'Indebido deber de asesoría',
  938 => 'Inconsistencias en los pagos a terceros',
  961 => 'Inconsistencias en el movimiento y saldo total del producto',
  909 => 'Incumplimiento de los términos del contrato',
  902 => 'Dificultad en el acceso a la información',
  903 => 'Información o asesoría incompleta y/o errada',
  904 => 'Información inoportuna',
  945 => 'Dificultad o imposibilidad para realizar transacciones o consulta de información por el canal',
  949 => 'Errores en el contenido de la información en informes, extractos o reportes.',
  964 => 'Información sujeta a reserva',
  905 => 'Dificultad en la comunicación con la entidad',
  946 => 'Demora en la atención o en el servicio requerido',
  928 => 'Demora en la respuesta a quejas, reclamos o peticiones',
  942 => 'Demora o no aplicación del pago',
  921 => 'Demora o no devolución de saldos, aportes o primas',
  926 => 'No disponibilidad o fallas de los canales de atención',
  963 => 'Fallas o inoportunidad en el proceso de vinculación',
  943 => 'Error en la aplicación del pago',
  955 => 'Error en la facturación o cobro no pactado',
  929 => 'Errores en la resolución de quejas, reclamos o peticiones.',
  934 => 'Actualización equivocada de datos personales',
  933 => 'Demora o no modificación de datos personales',
  935 => 'Inadecuado tratamiento de datos personales',
  910 => 'Presunta suplantación de personas',
  908 => 'Presunta actuación fraudulenta o no ética del personal',
  931 => 'Reporte injustificado a centrales de riesgo',
  932 => 'No levantamiento de reporte negativo a centrales de riesgo',
  916 => 'Vinculación no autorizada',
  901 => 'Publicidad engañosa',
  917 => 'Condicionamiento a la adquisición de productos o servicios',
  962 => 'Inconformidad con procesos internos de conocimiento del cliente y SARLAFT',
  954 => 'Incrementos de tarifas no pactadas o informadas',
  950 => 'Limitación en la expedición de certificaciones',
  956 => 'Modificación de condiciones en contratos',
  918 => 'No cancelación o terminación de los productos',
  920 => 'No entrega de paz y salvo',
  930 => 'No resolución a quejas, peticiones y reclamos',
  927 => 'Obstáculo para la interposición de quejas, reclamos o peticiones',
  948 => 'Omisión o envío tardío o inoportuno de informes, extractos o reportes a los que esté obligada la entidad.',
  952 => 'Producto terminado o cancelado sin justificación',
  947 => 'Seguridad en canales',
  940 => 'Transacción no reconocida',
  513 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  514 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  515 => 'Inconsistencia en el valor de rendimientos',
  501 => 'Inconsistencia en la compra, intercambio, transferencia, traspaso y/o redención',
  505 => 'Incumplimiento en instrucción del cliente (ejecución operación)',
  512 => 'Incumplimiento de inversiones con políticas definidas',
  502 => 'Desvalorización por riesgos del mercado',
  511 => 'Perdida o desvalorización unidad',
  966 => 'Fallas en operaciones en moneda extranjera',
  967 => 'Diferencias en monetización',
  504 => 'Errores en la colocación y adjudicación de valores',
  510 => 'Negativa o demora en el pago',
  923 => 'Negación injustificada a la apertura del producto',
);