<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/CampaignTrackers/Ext/Vardefs/fbsg_ctcid_index.php

$dictionary['CampaignTracker']['indices'][] = array(
    'name' => 'campaign_tracker_camid_idx',
    'type' => 'index',
    'fields' => array(
        'campaign_id',
    ),
);

$dictionary['CampaignTracker']['fields']['clicks'] = array(
    'name' => 'clicks',
    'type' => 'int',
    'vname' => 'LBL_CLICKS',
    'len' => '11',
    'studio' => 'visible',
);
?>
<?php
// Merged from custom/Extension/modules/CampaignTrackers/Ext/Vardefs/fbsg_cc_url_id.php

$dictionary['CampaignTracker']['fields']['cc_url_id'] = array(
    'required' => false,
    'name' => 'cc_url_id',
    'vname' => 'LBL_CC_URL_ID',
    'type' => 'varchar',
    'len' => 255,
);


?>
