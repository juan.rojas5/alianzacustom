<?php
 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_departamentointer_c']['labelValue']='Departamento';
$dictionary['Contact']['fields']['sasa_departamentointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_departamentointer_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_departamentointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 ?>