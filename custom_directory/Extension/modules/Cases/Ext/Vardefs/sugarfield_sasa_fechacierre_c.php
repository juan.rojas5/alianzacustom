<?php
 // created: 2022-09-28 14:17:21
$dictionary['Case']['fields']['sasa_fechacierre_c']['labelValue']='Fecha de Cierre';
$dictionary['Case']['fields']['sasa_fechacierre_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechacierre_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_fechacierre_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechacierre_c']['readonly_formula']='';

 ?>