<?php
 // created: 2022-10-25 23:16:58
$dictionary['Account']['fields']['sasa_nroidentificacion_c']['labelValue']='Número de ID';
$dictionary['Account']['fields']['sasa_nroidentificacion_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Account']['fields']['sasa_nroidentificacion_c']['enforced']='';
$dictionary['Account']['fields']['sasa_nroidentificacion_c']['dependency']='';

 ?>