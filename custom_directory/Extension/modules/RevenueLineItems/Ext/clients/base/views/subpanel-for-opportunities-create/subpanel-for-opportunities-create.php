<?php
$viewdefs['RevenueLineItems']['base']['view']['subpanel-for-opportunities-create']['panels'][0]['fields'] = 
 array(
	array(
		'name' => 'product_template_name',
		'enabled' => true,
		'default' => true
	),
	'discount_price',
	array(
		'name' => 'likely_case',
		'type' => 'currency',
		'related_fields' => array(
		    'currency_id',
		    'base_rate',
		    'total_amount',
		    'quantity',
		    'discount_amount',
		    'discount_price'
		),
		'showTransactionalAmount' => true,
		'convertToBase' => true,
		'currency_field' => 'currency_id',
		'base_rate_field' => 'base_rate',
		'enabled' => true,
		'default' => true
	),
	'date_closed',
	'sales_stage',
	array(
		'name' => 'probability',
		'readonly' => true
	),
	array(
		'name' => 'assigned_user_name',
		'enabled' => true,
		'default' => true
	),
);
