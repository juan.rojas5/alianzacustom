<?php

    $admin_option_defs = array();
    $admin_option_defs['Administration']['Parameters_Advanced_Instance'] = array(
        //Icon name. Available icons are located in ./themes/default/images
        'Administration',

        //Link name label 
        'Parameters Advanced Instance',

        //Link description label
        'Parameters Advanced Instance',

        //Link URL - For Sidecar modules
        //'javascript:parent.SUGAR.App.router.navigate("parameters_advanced_instance", {trigger: true});',

        //Alternatively, if you are linking to BWC modules
        './index.php?entryPoint=PAI',
    );

    $admin_group_header[] = array(
        //Section header label
        'SASA Consultoria.com',

        //$other_text parameter for get_form_header()
        'SASA Consultoria.com',

        //$show_help parameter for get_form_header()
        false,

        //Section links
        $admin_option_defs, 

        //Section description label
        'Especialistas en Sugar<b>CRM</b>. <a href="http://sasaconsultoria.com/">www.sasaconsultoria.com</a>'
    );
