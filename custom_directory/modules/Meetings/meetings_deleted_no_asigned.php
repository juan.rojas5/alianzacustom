<?php

//if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class meetings_deleted_no_asigned_class
{
        function after_save($bean, $event, $arguments)
        {
                //Codigo suministrado por SUGAR
                //If not linked to an Account, exit
                $parent_type = $bean->parent_type;
                $meeting_id = $bean->id;
                if ($parent_type !== 'Accounts')
                {
                        return;
                }
                //Get Meeting's assigned user ID
                $meeting_user_id = $bean->assigned_user_id;
                //Get parent Account SugarBean object
                $account_id = $bean->parent_id;
                $account = BeanFactory::getBean('Accounts', $account_id, array('disable_row_level_security' => true));
                //Get Account assigned user ID
                /*$account_user_id = $account->assigned_user_id;
                if ($meeting_user_id != $account_user_id)
                {
                        $bean->mark_deleted($meeting_id);
                        return;
                }*/
                //Instantiate TeamSet object and get list of existing Team IDs on Account
                $account_ts_id = $account->team_set_id;
                $ts = new TeamSet();
                $account_team_ids = $ts->getTeamIds($account_ts_id);
                $meeting_team_ids = array();
                foreach($account_team_ids as $tid)
                {
                        if ($tid !== '1')
                        {
                                $meeting_team_ids[] = $tid;
                        }
                }
                $primary_team_id = $meeting_team_ids[0];
                //Need to set Primary to something that will persist or replace not allowed 
                $bean->team_id = $primary_team_id;
                $bean->load_relationship('teams');
                $bean->teams->replace($meeting_team_ids);

                if($bean->team_set_id)
                {
                        $bean->acl_team_set_id=$bean->team_set_id;
                        $bean->save();
                }
        }
}
?>