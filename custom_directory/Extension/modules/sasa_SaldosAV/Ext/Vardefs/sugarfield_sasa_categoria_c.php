<?php
 // created: 2018-11-21 21:46:09
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_saldosav_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['enforced']=true;

 ?>