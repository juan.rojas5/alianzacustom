<?php
// created: 2023-02-17 20:29:36
$dictionary["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1' => 
    array (
      'lhs_module' => 'sasa2_Unidades_de_negocio',
      'lhs_table' => 'sasa2_unidades_de_negocio',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa1_Unidades_de_negocio_por_',
      'rhs_table' => 'sasa1_unidades_de_negocio_por_',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_c',
      'join_key_lhs' => 'sasa2_unid7bfcnegocio_ida',
      'join_key_rhs' => 'sasa2_unid6506io_por__idb',
    ),
  ),
  'table' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa2_unid7bfcnegocio_ida' => 
    array (
      'name' => 'sasa2_unid7bfcnegocio_ida',
      'type' => 'id',
    ),
    'sasa2_unid6506io_por__idb' => 
    array (
      'name' => 'sasa2_unid6506io_por__idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa2_unid7bfcnegocio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa2_unid6506io_por__idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa2_unid6506io_por__idb',
      ),
    ),
  ),
);