<?php
// created: 2023-04-25 03:53:11
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Створено за назвою',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio por cliente Список',
  'LBL_MODULE_NAME' => 'Unidades de negocio por cliente',
  'LBL_MODULE_TITLE' => 'Unidades de negocio por cliente',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidad de negocio por cliente',
  'LBL_HOMEPAGE_TITLE' => 'Мій Unidades de negocio por cliente',
  'LNK_NEW_RECORD' => 'Створити Unidad de negocio por cliente',
  'LNK_LIST' => 'Переглянути Unidades de negocio por cliente',
  'LNK_IMPORT_SASA1_UNIDADES_DE_NEGOCIO_POR_' => 'Importar Unidades de negocio por cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Unidad de negocio por cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__SUBPANEL_TITLE' => 'Unidades de negocio por cliente',
  'LBL_NEW_FORM_TITLE' => 'Новий Unidad de negocio por cliente',
  'LNK_IMPORT_VCARD' => 'Importar Unidad de negocio por cliente vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio por cliente',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidad de negocio por cliente record by importing a vCard from your file system.',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio por cliente Panel de enfoque',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__RECORD_DASHBOARD' => 'Unidades de negocio por cliente Cuadro de mando del registro',
);