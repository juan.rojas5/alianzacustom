<?php
 // created: 2018-06-17 22:20:25
$dictionary['Account']['fields']['phone_alternate']['len']='100';
$dictionary['Account']['fields']['phone_alternate']['audited']=true;
$dictionary['Account']['fields']['phone_alternate']['massupdate']=false;
$dictionary['Account']['fields']['phone_alternate']['comments']='An alternate phone number';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.03',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_alternate']['calculated']=false;

 ?>