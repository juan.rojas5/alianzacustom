<?php
 // created: 2022-11-18 20:19:02
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['labelValue']='Número_Documento_Cliente';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['readonly_formula']='';

 ?>