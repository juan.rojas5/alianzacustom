<?php
 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_type']['len']=255;
$dictionary['Call']['fields']['parent_type']['audited']=false;
$dictionary['Call']['fields']['parent_type']['massupdate']=false;
$dictionary['Call']['fields']['parent_type']['comments']='';
$dictionary['Call']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_type']['calculated']=false;
$dictionary['Call']['fields']['parent_type']['options']='';

 ?>