<?php
 // created: 2022-09-28 14:17:19
$dictionary['Case']['fields']['sasa_correoelectronico_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_correoelectronico_c']['labelValue']='Correo electrónico';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_correoelectronico_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['formula']='related($accounts,"email")';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['readonly_formula']='';

 ?>