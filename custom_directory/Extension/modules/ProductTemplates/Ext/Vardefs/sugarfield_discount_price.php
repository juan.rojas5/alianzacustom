<?php
 // created: 2018-07-07 16:22:38
$dictionary['ProductTemplate']['fields']['discount_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['discount_price']['len']=26;
$dictionary['ProductTemplate']['fields']['discount_price']['audited']=true;
$dictionary['ProductTemplate']['fields']['discount_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['ProductTemplate']['fields']['discount_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['discount_price']['duplicate_merge']='disabled';
$dictionary['ProductTemplate']['fields']['discount_price']['duplicate_merge_dom_value']='0';
$dictionary['ProductTemplate']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['discount_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['enable_range_search']=false;

 ?>