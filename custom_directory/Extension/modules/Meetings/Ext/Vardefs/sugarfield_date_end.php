<?php
 // created: 2018-09-10 22:11:11
$dictionary['Meeting']['fields']['date_end']['audited']=true;
$dictionary['Meeting']['fields']['date_end']['comments']='Date meeting ends';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_end']['calculated']=false;
$dictionary['Meeting']['fields']['date_end']['enable_range_search']='1';
$dictionary['Meeting']['fields']['date_end']['required']=true;
$dictionary['Meeting']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_end']['group_label']='';

 ?>