<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Categorias/Ext/Layoutdefs/sasa_categorias_sasa_productosafav_1_sasa_Categorias.php

 // created: 2019-01-04 21:22:57
$layout_defs["sasa_Categorias"]["subpanel_setup"]['sasa_categorias_sasa_productosafav_1'] = array (
  'order' => 100,
  'module' => 'sasa_ProductosAFAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'get_subpanel_data' => 'sasa_categorias_sasa_productosafav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Categorias/Ext/Layoutdefs/sasa_categorias_sasas_saldosconsolidados_1_sasa_Categorias.php

 // created: 2019-01-04 22:24:53
$layout_defs["sasa_Categorias"]["subpanel_setup"]['sasa_categorias_sasas_saldosconsolidados_1'] = array (
  'order' => 100,
  'module' => 'sasaS_SaldosConsolidados',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'get_subpanel_data' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
