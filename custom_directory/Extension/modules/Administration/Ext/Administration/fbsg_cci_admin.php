<?php
$url = preg_match('/^7/', $sugar_version)
    ? 'javascript:parent.SUGAR.App.router.navigate("bwc/index.php?module=fbsg_ConstantContactIntegration&action=config", {trigger: true})'
    : './index.php?module=fbsg_ConstantContactIntegration&action=config';

$admin_option_defs = array();
$admin_option_defs['Administration']['cci_config'] = array(
    'fbsg_ConstantContactIntegration',
    'Constant Contact Control Panel',
    '',
    $url
);
$admin_option_defs['Administration']['cci_errors'] = array(
    'fbsg_ConstantContactIntegration',
    'Constant Contact Errors',
    '',
    'javascript:parent.SUGAR.App.router.navigate("fbsg_CCIErrors", {trigger: true})'
);
$admin_group_header[] = array(
    'Constant Contact Integration',
    'Manage Constant Contact Configuration',
    false,
    $admin_option_defs,
    'Synchronize contacts, campaigns, and campaign results from Constant Contact'
);
