<?php
 // created: 2018-07-06 21:59:35
$dictionary['Call']['fields']['reminder_time']['default']='-1';
$dictionary['Call']['fields']['reminder_time']['audited']=true;
$dictionary['Call']['fields']['reminder_time']['comments']='Specifies when a reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Call']['fields']['reminder_time']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['reminder_time']['merge_filter']='disabled';
$dictionary['Call']['fields']['reminder_time']['calculated']=false;
$dictionary['Call']['fields']['reminder_time']['dependency']=false;

 ?>