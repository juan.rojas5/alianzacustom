<?php
 // created: 2018-09-19 22:32:13

$app_list_strings['sasa_estadocivil_c_list']=array (
  '' => '',
  'Soltero' => 'Soltero',
  'Casado' => 'Casado',
  'Divorsiado' => 'Divorsiado',
  'Union libre' => 'Unión libre',
  'Viudo' => 'Viudo',
  'Separado' => 'Separado',
  'Otro' => 'Otro',
);