<?php
 // created: 2018-05-22 21:55:31
$dictionary['Lead']['fields']['lead_source_description']['audited']=false;
$dictionary['Lead']['fields']['lead_source_description']['massupdate']=false;
$dictionary['Lead']['fields']['lead_source_description']['comments']='Description of the lead source';
$dictionary['Lead']['fields']['lead_source_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['lead_source_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['lead_source_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source_description']['reportable']=false;
$dictionary['Lead']['fields']['lead_source_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['lead_source_description']['calculated']=false;
$dictionary['Lead']['fields']['lead_source_description']['rows']='4';
$dictionary['Lead']['fields']['lead_source_description']['cols']='20';

 ?>