<?php
 // created: 2022-09-15 22:31:01
$dictionary['Meeting']['fields']['sasa_compromiso_c']['labelValue']='Compromiso';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_compromiso_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadovisita_c,"Descartado")))';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['readonly_formula']='';

 ?>