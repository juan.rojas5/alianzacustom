<?php
// created: 2022-12-09 21:37:28
$app_list_strings['record_type_display'][''] = '';
$app_list_strings['record_type_display']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display']['Opportunities'] = 'Oportunidad';
$app_list_strings['record_type_display']['Cases'] = 'PQR';
$app_list_strings['record_type_display']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display']['Contacts'] = 'Contactos';
$app_list_strings['record_type_display']['Products'] = 'Lnea de la Oferta';
$app_list_strings['record_type_display']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display']['Project'] = 'Proyecto';
$app_list_strings['record_type_display']['Prospects'] = 'Pblico Objetivo';
$app_list_strings['record_type_display']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display']['Notes'] = 'Nota';
$app_list_strings['record_type_display']['RevenueLineItems'] = 'Líneas de Ingreso';
