<?php

$job_strings[] = 'UpdateLeads';

function UpdateLeads()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Leads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    //Realación de leads a account con where l.delete = 0 y usuario no asignado/libre diferente
    $query="SELECT l.id as idLead 
            FROM leads l
            INNER JOIN accounts a ON l.account_id = a.id
            INNER JOIN users u ON l.assigned_user_id = u.id
            WHERE l.deleted = 0 and u.user_name != 'No Asignado/Libre'";

    $result = $GLOBALS['db']->query($query);
    $cantConsulta = mysqli_num_rows($result);


    //$GLOBALS['log']->security("-------EJECUTAR SIGUIENTE QUERY-----------------");
    //$GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            
            $beanLeads = BeanFactory::retrieveBean("Leads", $row['idLead']);

            //Fields to update
            //$beanStatus->update_date_modified = true;
          
            //Save
            $beanLeads->save();


    }//while

    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Leads ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}
