<?php
 // created: 2022-10-25 23:17:02
$dictionary['Account']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Account']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Account']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['Account']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Account']['fields']['sasa_superior2_c']['dependency']='';

 ?>