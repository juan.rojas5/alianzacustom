<?php
 // created: 2018-07-06 21:49:11
$dictionary['Meeting']['fields']['name']['audited']=true;
$dictionary['Meeting']['fields']['name']['massupdate']=false;
$dictionary['Meeting']['fields']['name']['comments']='Meeting name';
$dictionary['Meeting']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.43',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['name']['calculated']=false;

 ?>