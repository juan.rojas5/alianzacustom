<?php
// created: 2019-03-08 18:56:45
$dictionary["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1' => 
    array (
      'lhs_module' => 'sasaP_PresupuestosxAsesor',
      'lhs_table' => 'sasap_presupuestosxasesor',
      'lhs_key' => 'id',
      'rhs_module' => 'sasaP_PresupuestoxProducto',
      'rhs_table' => 'sasap_presupuestoxproducto',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c',
      'join_key_lhs' => 'sasap_pres107bxasesor_ida',
      'join_key_rhs' => 'sasap_pres4d00roducto_idb',
    ),
  ),
  'table' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasap_pres107bxasesor_ida' => 
    array (
      'name' => 'sasap_pres107bxasesor_ida',
      'type' => 'id',
    ),
    'sasap_pres4d00roducto_idb' => 
    array (
      'name' => 'sasap_pres4d00roducto_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasap_pres107bxasesor_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasap_pres4d00roducto_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasap_pres4d00roducto_idb',
      ),
    ),
  ),
);