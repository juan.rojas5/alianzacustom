<?php
 // created: 2023-04-11 19:11:20
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['labelValue']='Nombre asistente de gestión';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['readonly_formula']='';

 ?>