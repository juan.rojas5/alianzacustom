<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_case_number_c.php

 // created: 2022-11-18 19:36:52
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['labelValue']='Case_Number';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_fecha_de_vencimiento_c.php

 // created: 2022-11-18 19:47:58
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['labelValue']='Fecha_de_Vencimiento';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_2_codigo_c.php

 // created: 2022-11-18 19:58:09
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['labelValue']='Tipificación_2_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_codigo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_1_codigo_c.php

 // created: 2022-11-18 20:07:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['labelValue']='Tipificación_1_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_codigo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_2_c.php

 // created: 2022-11-18 20:07:50
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['labelValue']='Tipificación_2';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_3_c.php

 // created: 2022-11-18 20:08:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['labelValue']='Tipificación_3';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_1_c.php

 // created: 2022-11-18 20:08:57
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['labelValue']='Tipificación_1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_1_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipificacion_3_codigo_c.php

 // created: 2022-11-18 20:10:05
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['labelValue']='Tipificación_3_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipificacion_3_codigo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_fecha_creacion_c.php

 // created: 2022-11-18 20:12:03
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['labelValue']='Fecha_creacion';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_creacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_hora_c.php

 // created: 2022-11-18 20:12:46
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['labelValue']='Hora';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_hora_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_creado_por_nombre_compl_c.php

 // created: 2022-11-18 20:14:46
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['labelValue']='Creado_por_Nombre_Completo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_nombres_cliente_c.php

 // created: 2022-11-18 20:16:47
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['labelValue']='Nombres_Cliente';
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_nombres_cliente_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_apellidos_cliente_c.php

 // created: 2022-11-18 20:18:11
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['labelValue']='Apellidos_Cliente';
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_apellidos_cliente_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_numero_documento_client_c.php

 // created: 2022-11-18 20:19:02
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['labelValue']='Número_Documento_Cliente';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numero_documento_client_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_medio_de_recepcion_c.php

 // created: 2022-11-18 20:28:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['labelValue']='Medio_de_Recepción';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_segmento_datawarehouse__c.php

 // created: 2022-11-18 20:29:13
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['labelValue']='Segmento_DataWareHouse';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipo_de_solicitud_c.php

 // created: 2022-11-18 20:30:14
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['labelValue']='Tipo_de_Solicitud';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_tipo_de_solicitud_codig_c.php

 // created: 2022-11-18 20:36:24
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['labelValue']='Tipo_de_Solicitud_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_tipo_de_solicitud_codig_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2022-11-18 20:37:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['labelValue']='Categoría';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_categoria_codigo_c.php

 // created: 2022-11-18 20:38:06
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['labelValue']='Categoría_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_categoria_codigo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_estado_de_caso_c.php

 // created: 2022-11-18 20:38:51
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['labelValue']='Estado_de_caso';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_estado_de_caso_codigo_c.php

 // created: 2022-11-18 20:41:50
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['labelValue']='Estado_de_caso_codigo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_estado_de_caso_codigo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_encargo_c.php

 // created: 2022-11-18 20:45:23
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['labelValue']='Encargo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_solucionador_inmediato__c.php

 // created: 2022-11-18 20:46:39
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['labelValue']='Solucionador_Inmediato';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_documentado_c.php

 // created: 2022-11-18 20:47:52
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['labelValue']='¿Documentado?';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_escalado_c.php

 // created: 2022-11-18 20:48:38
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['labelValue']='Escalado';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_escalado_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_vencido_c.php

 // created: 2022-11-18 20:49:11
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['labelValue']='Vencido';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_descripcion_de_la_solic_c.php

 // created: 2022-11-18 20:51:42
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['labelValue']='Descripción de la Solicitud';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_solucionador_c.php

 // created: 2022-11-18 20:52:19
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['labelValue']='Solucionador';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_fecha_solucion_c.php

 // created: 2022-11-18 20:52:49
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['labelValue']='Fecha_solución';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_solucion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_usuario_asistente_encar_c.php

 // created: 2022-11-18 20:53:37
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['labelValue']='Usuario_Asistente_Encargo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_usuario_asistente_encar_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/accounts_sasa_pqrs_historico_1_sasa_pqrs_historico.php

// created: 2022-11-21 20:51:44
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_pqrs_historico_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1_name"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link' => 'accounts_sasa_pqrs_historico_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1accounts_ida"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE_ID',
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link' => 'accounts_sasa_pqrs_historico_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_numerounico_c.php

 // created: 2023-01-02 19:07:38
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['labelValue']='Número único ';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_numerounico_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_pqrs_historico/Ext/Vardefs/sugarfield_sasa_respuesta_c.php

 // created: 2023-03-21 21:56:22
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['labelValue']='Respuesta';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['readonly_formula']='';

 
?>
