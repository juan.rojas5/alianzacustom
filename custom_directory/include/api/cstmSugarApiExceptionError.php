<?php

require_once 'include/api/SugarApiException.php';

/**
 * Custom error.
 */
class cstmSugarApiExceptionError extends SugarApiException
{
    public $httpCode = 422;
    public $errorLabel = 'Numero de ID ya existe';
    public $messageLabel = 'EXCEPTION_NUMID_PARAMETER';
}