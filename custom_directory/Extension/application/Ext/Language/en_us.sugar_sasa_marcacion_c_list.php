<?php
 // created: 2022-03-08 11:32:58

$app_list_strings['sasa_marcacion_c_list']=array (
  '' => '',
  1 => 'Si la queja o reclamo corresponde a una réplica, es decir, si el consumidor financiero solicita reabrir la queja o reclamo por inconformidad con la respuesta dada por la entidad vigilada y/o el defensor del consumidor financiero, dentro de los dos (2) meses siguientes a la fecha final de cierre de la última respuesta',
  2 => 'Si la queja o reclamo fue reclasificada por la entidad vigilada respecto de lo que fue remitido a la superintendencia Financiera de Colombia a través del Formato “Smartsupervision - Interposición de queja o reclamo”',
  3 => 'Si el caso fue cerrado por la entidad vigilada por no ser una queja o reclamo sino otro tipo de petición ante la entidad vigilada',
  4 => 'Si la queja o reclamo fue cerrada por falta de competencia',
);