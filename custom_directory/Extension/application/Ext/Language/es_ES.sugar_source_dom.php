<?php
 // created: 2023-02-09 16:43:46

$app_list_strings['source_dom']=array (
  '' => '',
  'Internal' => 'Interno',
  'Forum' => 'Foro',
  'Web' => 'Web',
  'InboundEmail' => 'Correo electrónico',
  'Twitter' => 'Twitter',
  'Portal' => 'Portal',
  'SFC' => 'SFC',
  'Defensor del Consumidor' => 'Defensor del Consumidor',
  'Carta' => 'Carta',
  'Telefono' => 'Teléfono',
  'Portal publico' => 'Portal público',
  'Portal transaccional' => 'Portal transaccional',
  'Bizagi' => 'Bizagi',
);