<?php

global $sugar_version;
$is7 = !!(preg_match('/^7/', $sugar_version));

$dictionary['ProspectList']['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'len' => 255,
);

$dictionary['ProspectList']['fields']['sync_to_cc'] = array(
    'required' => false,
    'name' => 'sync_to_cc',
    'vname' => 'LBL_SYNC_TO_CC',
    'type' => 'bool',
    'studio' => false,
    'default' => 0,
);

$dictionary['ProspectList']['fields']['last_successful_cc_update'] = array(
    'required' => false,
    'name' => 'last_successful_cc_update',
    'vname' => 'LBL_LAST_SUCCESSFUL_CC_UPDATE',
    'type' => 'datetime',
    'studio' => 'visible',
    'reportable' => true,
);

$dictionary['ProspectList']['fields']['last_successful_cc_remove'] = array(
    'required' => false,
    'name' => 'last_successful_cc_remove',
    'vname' => 'LBL_LAST_SUCCESSFUL_CC_REMOVE',
    'type' => 'datetime',
    'studio' => 'visible',
    'reportable' => true,
);

/* REMOVE */
// if($is7) {
//     $dictionary['ProspectList']['fields']['cc_ui_save'] = array(
//         'name' => 'cc_ui_save',
//         'vname' => 'CC Save from UI',
//         'type' => 'bool',
//         'massupdate' => false,
//         'default' => '0',
//         'importable' => false,
//         'reportable' => true,
//         'studio' => false,
//         'duplicate_merge' => 'disabled',
//         'source' => 'non-db'
//     );
// }

//$dictionary['ProspectList']['fields']['cc_sync_button'] = array(
//    'name' => 'cc_sync_button',
//    'type' => 'html',
//    'vname' => 'LBL_CC_SYNC_BUTTON',
//    'function' => array(
//        'name' => 'cc_sync',
//        'returns' => 'html',
//        'include' => 'modules/fbsg_ConstantContactIntegration/include/cc_list_sync.php',
//    ),
//    'source' => 'non-db',
//    'studio' => 'visible',
//);
