<?php
// created: 2019-01-04 22:24:53
$dictionary["sasa_categorias_sasas_saldosconsolidados_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_categorias_sasas_saldosconsolidados_1' => 
    array (
      'lhs_module' => 'sasa_Categorias',
      'lhs_table' => 'sasa_categorias',
      'lhs_key' => 'id',
      'rhs_module' => 'sasaS_SaldosConsolidados',
      'rhs_table' => 'sasas_saldosconsolidados',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_categorias_sasas_saldosconsolidados_1_c',
      'join_key_lhs' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
      'join_key_rhs' => 'sasa_categbe53lidados_idb',
    ),
  ),
  'table' => 'sasa_categorias_sasas_saldosconsolidados_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida' => 
    array (
      'name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
      'type' => 'id',
    ),
    'sasa_categbe53lidados_idb' => 
    array (
      'name' => 'sasa_categbe53lidados_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_categorias_sasas_saldosconsolidados_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_categorias_sasas_saldosconsolidados_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_categorias_sasas_saldosconsolidados_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_categbe53lidados_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_categorias_sasas_saldosconsolidados_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_categbe53lidados_idb',
      ),
    ),
  ),
);