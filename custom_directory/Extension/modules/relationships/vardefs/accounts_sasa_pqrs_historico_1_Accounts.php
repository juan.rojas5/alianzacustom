<?php
// created: 2022-11-21 20:51:44
$dictionary["Account"]["fields"]["accounts_sasa_pqrs_historico_1"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_pqrs_historico_1',
  'source' => 'non-db',
  'module' => 'sasa_pqrs_historico',
  'bean_name' => 'sasa_pqrs_historico',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
