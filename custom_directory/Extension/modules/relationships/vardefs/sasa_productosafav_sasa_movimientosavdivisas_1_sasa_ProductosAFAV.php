<?php
// created: 2019-01-04 21:30:05
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAVDivisas',
  'bean_name' => 'sasa_MovimientosAVDivisas',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
