<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_case_number.php

 // created: 2018-07-13 20:59:39
$dictionary['Case']['fields']['case_number']['len']='11';
$dictionary['Case']['fields']['case_number']['audited']=false;
$dictionary['Case']['fields']['case_number']['massupdate']=false;
$dictionary['Case']['fields']['case_number']['comments']='Visual unique identifier';
$dictionary['Case']['fields']['case_number']['merge_filter']='disabled';
$dictionary['Case']['fields']['case_number']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.29',
  'searchable' => true,
);
$dictionary['Case']['fields']['case_number']['calculated']=false;
$dictionary['Case']['fields']['case_number']['enable_range_search']=false;
$dictionary['Case']['fields']['case_number']['autoinc_next']='2';
$dictionary['Case']['fields']['case_number']['min']=false;
$dictionary['Case']['fields']['case_number']['max']=false;
$dictionary['Case']['fields']['case_number']['disable_num_format']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-02-03 20:54:36
$dictionary['Case']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/denorm_account_name.php


// 'account_name'
$dictionary['Case']['fields']['account_name']['is_denormalized'] = true;
$dictionary['Case']['fields']['account_name']['denormalized_field_name'] = 'denorm_account_name';

// 'denorm_account_name'
$dictionary['Case']['fields']['denorm_account_name']['name'] = 'denorm_account_name';
$dictionary['Case']['fields']['denorm_account_name']['type'] = 'varchar';
$dictionary['Case']['fields']['denorm_account_name']['dbType'] = 'varchar';
$dictionary['Case']['fields']['denorm_account_name']['vname'] = 'LBL_ACCOUNT_NAME';
$dictionary['Case']['fields']['denorm_account_name']['len'] = '150';
$dictionary['Case']['fields']['denorm_account_name']['comment'] = 'Name of the Company';
$dictionary['Case']['fields']['denorm_account_name']['unified_search'] = true;
$dictionary['Case']['fields']['denorm_account_name']['full_text_search'] = array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Case']['fields']['denorm_account_name']['audited'] = true;
$dictionary['Case']['fields']['denorm_account_name']['required'] = false;
$dictionary['Case']['fields']['denorm_account_name']['importable'] = 'required';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_on_record_copy'] = 'always';
$dictionary['Case']['fields']['denorm_account_name']['merge_filter'] = 'disabled';
$dictionary['Case']['fields']['denorm_account_name']['massupdate'] = false;
$dictionary['Case']['fields']['denorm_account_name']['comments'] = 'Name of the Company';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_merge'] = 'enabled';
$dictionary['Case']['fields']['denorm_account_name']['duplicate_merge_dom_value'] = '1';
$dictionary['Case']['fields']['denorm_account_name']['calculated'] = false;
$dictionary['Case']['fields']['denorm_account_name']['denorm_from_module'] = 'Accounts';
$dictionary['Case']['fields']['denorm_account_name']['studio'] = false;

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_producto_cod_c.php

 // created: 2022-03-08 08:56:42
$dictionary['Case']['fields']['sasa_producto_cod_c']['labelValue']='Producto';
$dictionary['Case']['fields']['sasa_producto_cod_c']['dependency']='';
$dictionary['Case']['fields']['sasa_producto_cod_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_producto_cod_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_producto_cod_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_ente_control_c.php

 // created: 2022-03-08 09:08:08
$dictionary['Case']['fields']['sasa_ente_control_c']['labelValue']='Ente de Control';
$dictionary['Case']['fields']['sasa_ente_control_c']['dependency']='';
$dictionary['Case']['fields']['sasa_ente_control_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_ente_control_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_ente_control_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_desistimiento_c.php

 // created: 2022-03-08 09:19:36
$dictionary['Case']['fields']['sasa_desistimiento_c']['labelValue']='Desistimiento';
$dictionary['Case']['fields']['sasa_desistimiento_c']['dependency']='';
$dictionary['Case']['fields']['sasa_desistimiento_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_desistimiento_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_desistimiento_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_recepcion_c.php

 // created: 2022-03-08 09:30:52
$dictionary['Case']['fields']['sasa_recepcion_c']['labelValue']='Recepción';
$dictionary['Case']['fields']['sasa_recepcion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_recepcion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_recepcion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_recepcion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_puntorecepcion_c.php

 // created: 2022-03-08 09:35:23
$dictionary['Case']['fields']['sasa_puntorecepcion_c']['labelValue']='Punto de recepción';
$dictionary['Case']['fields']['sasa_puntorecepcion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_puntorecepcion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_puntorecepcion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_puntorecepcion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_admision_c.php

 // created: 2022-03-08 09:40:46
$dictionary['Case']['fields']['sasa_admision_c']['labelValue']='Admisión';
$dictionary['Case']['fields']['sasa_admision_c']['dependency']='';
$dictionary['Case']['fields']['sasa_admision_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_admision_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_admision_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_aceptacion_c.php

 // created: 2022-03-08 09:47:56
$dictionary['Case']['fields']['sasa_aceptacion_c']['labelValue']='Aceptación';
$dictionary['Case']['fields']['sasa_aceptacion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_aceptacion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_aceptacion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_aceptacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_favoravilidad_c.php

 // created: 2022-03-08 09:48:25
$dictionary['Case']['fields']['sasa_favoravilidad_c']['labelValue']='Favoravilidad';
$dictionary['Case']['fields']['sasa_favoravilidad_c']['dependency']='';
$dictionary['Case']['fields']['sasa_favoravilidad_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_favoravilidad_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_favoravilidad_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_rectificacion_c.php

 // created: 2022-03-08 09:55:10
$dictionary['Case']['fields']['sasa_rectificacion_c']['labelValue']='Rectificacion';
$dictionary['Case']['fields']['sasa_rectificacion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_rectificacion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_rectificacion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_rectificacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_marcacion_c.php

 // created: 2022-03-08 11:33:19
$dictionary['Case']['fields']['sasa_marcacion_c']['labelValue']='Marcación';
$dictionary['Case']['fields']['sasa_marcacion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_marcacion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_marcacion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_marcacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tutela_c.php

 // created: 2022-03-08 11:43:44
$dictionary['Case']['fields']['sasa_tutela_c']['labelValue']='Tutela';
$dictionary['Case']['fields']['sasa_tutela_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_tutela_c']['enforced']='';
$dictionary['Case']['fields']['sasa_tutela_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tutela_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tutela_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_prorroga_c.php

 // created: 2022-03-08 12:00:41
$dictionary['Case']['fields']['sasa_prorroga_c']['labelValue']='Prórroga';
$dictionary['Case']['fields']['sasa_prorroga_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_prorroga_c']['enforced']='';
$dictionary['Case']['fields']['sasa_prorroga_c']['dependency']='';
$dictionary['Case']['fields']['sasa_prorroga_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_prorroga_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_docrespfinal_c.php

 // created: 2022-03-08 15:02:19
$dictionary['Case']['fields']['sasa_docrespfinal_c']['labelValue']='Documentación de respuesta final';
$dictionary['Case']['fields']['sasa_docrespfinal_c']['dependency']='';
$dictionary['Case']['fields']['sasa_docrespfinal_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_docrespfinal_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_docrespfinal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_anexoqueja_c.php

 // created: 2022-03-08 15:03:03
$dictionary['Case']['fields']['sasa_anexoqueja_c']['labelValue']='Anexos de la queja o reclamo';
$dictionary['Case']['fields']['sasa_anexoqueja_c']['dependency']='';
$dictionary['Case']['fields']['sasa_anexoqueja_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_anexoqueja_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_anexoqueja_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_anexoresfinal_c.php

 // created: 2022-03-08 15:06:31
$dictionary['Case']['fields']['sasa_anexoresfinal_c']['labelValue']='Anexos a la respuesta final';
$dictionary['Case']['fields']['sasa_anexoresfinal_c']['dependency']='';
$dictionary['Case']['fields']['sasa_anexoresfinal_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_anexoresfinal_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_anexoresfinal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_description.php

 // created: 2022-03-08 15:39:06
$dictionary['Case']['fields']['description']['required']=true;
$dictionary['Case']['fields']['description']['audited']=true;
$dictionary['Case']['fields']['description']['massupdate']=false;
$dictionary['Case']['fields']['description']['hidemassupdate']=false;
$dictionary['Case']['fields']['description']['comments']='Full text of the note';
$dictionary['Case']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['description']['merge_filter']='disabled';
$dictionary['Case']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.66',
  'searchable' => true,
);
$dictionary['Case']['fields']['description']['calculated']=false;
$dictionary['Case']['fields']['description']['rows']='6';
$dictionary['Case']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_is_escalated_c.php

 // created: 2022-03-08 16:10:47
$dictionary['Case']['fields']['sasa_is_escalated_c']['labelValue']='Anexos de la queja o Reclamo';
$dictionary['Case']['fields']['sasa_is_escalated_c']['dependency']='';
$dictionary['Case']['fields']['sasa_is_escalated_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_is_escalated_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_is_escalated_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_quejaexpres_c.php

 // created: 2022-03-09 11:27:41
$dictionary['Case']['fields']['sasa_quejaexpres_c']['labelValue']='Queja Exprés';
$dictionary['Case']['fields']['sasa_quejaexpres_c']['dependency']='';
$dictionary['Case']['fields']['sasa_quejaexpres_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_quejaexpres_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_quejaexpres_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_escalamiento_dcf_c.php

 // created: 2022-03-09 11:28:07
$dictionary['Case']['fields']['sasa_escalamiento_dcf_c']['labelValue']='Escalamiento del Defensor';
$dictionary['Case']['fields']['sasa_escalamiento_dcf_c']['dependency']='';
$dictionary['Case']['fields']['sasa_escalamiento_dcf_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_escalamiento_dcf_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_escalamiento_dcf_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_replica_c.php

 // created: 2022-03-09 11:28:40
$dictionary['Case']['fields']['sasa_replica_c']['labelValue']='Replica';
$dictionary['Case']['fields']['sasa_replica_c']['dependency']='';
$dictionary['Case']['fields']['sasa_replica_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_replica_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_replica_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_productodigital_c.php

 // created: 2022-03-09 11:30:00
$dictionary['Case']['fields']['sasa_productodigital_c']['labelValue']='Producto Digital';
$dictionary['Case']['fields']['sasa_productodigital_c']['dependency']='';
$dictionary['Case']['fields']['sasa_productodigital_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_productodigital_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_productodigital_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_canal_c.php

 // created: 2022-03-14 09:09:23
$dictionary['Case']['fields']['sasa_canal_c']['labelValue']='Canal';
$dictionary['Case']['fields']['sasa_canal_c']['dependency']='';
$dictionary['Case']['fields']['sasa_canal_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_canal_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_canal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_radsfc_c.php

 // created: 2022-03-14 09:44:21
$dictionary['Case']['fields']['sasa_radsfc_c']['labelValue']='Rad SFC';
$dictionary['Case']['fields']['sasa_radsfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radsfc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_raddefensorc_c.php

 // created: 2022-03-14 09:45:49
$dictionary['Case']['fields']['sasa_raddefensorc_c']['labelValue']='Rad Defensor del Consumidor ';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_raddefensorc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_raddefensorc_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_radalianza_c.php

 // created: 2022-03-14 09:46:40
$dictionary['Case']['fields']['sasa_radalianza_c']['labelValue']='Rad Alianza ';
$dictionary['Case']['fields']['sasa_radalianza_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radalianza_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radalianza_c']['dependency']='';
$dictionary['Case']['fields']['sasa_radalianza_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radalianza_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tiponegocio_c.php

 // created: 2022-03-14 09:50:50
$dictionary['Case']['fields']['sasa_tiponegocio_c']['labelValue']='Tipo de Negocio';
$dictionary['Case']['fields']['sasa_tiponegocio_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tiponegocio_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tiponegocio_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tipoentidad_c.php

 // created: 2022-03-30 11:35:16
$dictionary['Case']['fields']['sasa_tipoentidad_c']['labelValue']='Tipo de Entidad';
$dictionary['Case']['fields']['sasa_tipoentidad_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tipoentidad_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tipoentidad_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_tipoentidad_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_motivo_c.php

 // created: 2022-04-07 21:26:35

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_sfcfechacreacion_c.php

 // created: 2022-04-28 21:56:48
$dictionary['Case']['fields']['sasa_sfcfechacreacion_c']['labelValue']='Fecha Creación SFC';
$dictionary['Case']['fields']['sasa_sfcfechacreacion_c']['enforced']='';
$dictionary['Case']['fields']['sasa_sfcfechacreacion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_sfcfechacreacion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_sfcfechacreacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechaactualizacionsfc_c.php

 // created: 2022-04-28 22:01:14
$dictionary['Case']['fields']['sasa_fechaactualizacionsfc_c']['labelValue']='Fecha de actualización SFC';
$dictionary['Case']['fields']['sasa_fechaactualizacionsfc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechaactualizacionsfc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_fechaactualizacionsfc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechaactualizacionsfc_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sasa_sasa_tipificaciones_cases_1_Cases.php

// created: 2022-04-29 13:46:19
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_sasa_tipificaciones_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_sasa_tipificaciones',
  'bean_name' => 'sasa_sasa_tipificaciones',
  'side' => 'right',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1_name"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
  'save' => true,
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link' => 'sasa_sasa_tipificaciones_cases_1',
  'table' => 'sasa_sasa_tipificaciones',
  'module' => 'sasa_sasa_tipificaciones',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link' => 'sasa_sasa_tipificaciones_cases_1',
  'table' => 'sasa_sasa_tipificaciones',
  'module' => 'sasa_sasa_tipificaciones',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_codigoentidad_c.php

 // created: 2022-05-17 13:35:47
$dictionary['Case']['fields']['sasa_codigoentidad_c']['labelValue']='Código de Entidad';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipoentidad_c',
  'values' => 
  array (
    5 => 
    array (
      0 => '16',
    ),
    85 => 
    array (
      0 => '51',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolution.php

 // created: 2022-05-24 19:44:18
$dictionary['Case']['fields']['resolution']['required']=false;
$dictionary['Case']['fields']['resolution']['audited']=false;
$dictionary['Case']['fields']['resolution']['massupdate']=false;
$dictionary['Case']['fields']['resolution']['hidemassupdate']=false;
$dictionary['Case']['fields']['resolution']['comments']='The resolution of the case';
$dictionary['Case']['fields']['resolution']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['resolution']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['resolution']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolution']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.65',
  'searchable' => true,
);
$dictionary['Case']['fields']['resolution']['calculated']=false;
$dictionary['Case']['fields']['resolution']['rows']='4';
$dictionary['Case']['fields']['resolution']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_work_log.php

 // created: 2022-05-24 19:45:30
$dictionary['Case']['fields']['work_log']['required']=false;
$dictionary['Case']['fields']['work_log']['audited']=false;
$dictionary['Case']['fields']['work_log']['massupdate']=false;
$dictionary['Case']['fields']['work_log']['hidemassupdate']=false;
$dictionary['Case']['fields']['work_log']['comments']='Free-form text used to denote activities of interest';
$dictionary['Case']['fields']['work_log']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['work_log']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['work_log']['merge_filter']='disabled';
$dictionary['Case']['fields']['work_log']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.64',
  'searchable' => true,
);
$dictionary['Case']['fields']['work_log']['calculated']=false;
$dictionary['Case']['fields']['work_log']['rows']='4';
$dictionary['Case']['fields']['work_log']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_codigoqueja_c.php

 // created: 2022-05-24 19:46:34
$dictionary['Case']['fields']['sasa_codigoqueja_c']['labelValue']='Código de la queja o reclamo';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_codigoqueja_c']['enforced']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['dependency']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_resolved_datetime.php

 // created: 2022-05-24 20:19:34
$dictionary['Case']['fields']['resolved_datetime']['massupdate']=true;
$dictionary['Case']['fields']['resolved_datetime']['hidemassupdate']=false;
$dictionary['Case']['fields']['resolved_datetime']['comments']='Date when an issue is resolved';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['resolved_datetime']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['resolved_datetime']['merge_filter']='disabled';
$dictionary['Case']['fields']['resolved_datetime']['calculated']=false;
$dictionary['Case']['fields']['resolved_datetime']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_sfc_response_api_c.php

 // created: 2022-06-01 15:52:36
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['labelValue']='Respuesta SFC API';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['enforced']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['dependency']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_listaprueba_c.php

 // created: 2022-06-03 15:01:59
$dictionary['Case']['fields']['sasa_listaprueba_c']['labelValue']='Listas de pruebas';
$dictionary['Case']['fields']['sasa_listaprueba_c']['dependency']='';
$dictionary['Case']['fields']['sasa_listaprueba_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_listaprueba_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_listaprueba_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_negocio_c.php

 // created: 2022-06-15 16:17:17
$dictionary['Case']['fields']['sasa_negocio_c']['labelValue']='Negocio';
$dictionary['Case']['fields']['sasa_negocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_negocio_c']['enforced']='';
$dictionary['Case']['fields']['sasa_negocio_c']['dependency']='';
$dictionary['Case']['fields']['sasa_negocio_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_negocio_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_account_name.php

 // created: 2022-08-26 16:58:07
$dictionary['Case']['fields']['account_name']['len']=255;
$dictionary['Case']['fields']['account_name']['required']=false;
$dictionary['Case']['fields']['account_name']['audited']=true;
$dictionary['Case']['fields']['account_name']['massupdate']=true;
$dictionary['Case']['fields']['account_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['account_name']['comments']='The name of the account represented by the account_id field';
$dictionary['Case']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['account_name']['reportable']=false;
$dictionary['Case']['fields']['account_name']['calculated']=false;
$dictionary['Case']['fields']['account_name']['dependency']='or(equal($sasa_tipodesolicitud_c,"quejas"),equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['account_name']['related_fields']=array (
  0 => 'account_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_priority.php

 // created: 2022-08-26 16:54:58
$dictionary['Case']['fields']['priority']['massupdate']=true;
$dictionary['Case']['fields']['priority']['hidemassupdate']=false;
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['calculated']=false;
$dictionary['Case']['fields']['priority']['dependency']='equal($sasa_tipodesolicitud_c,"quejas")';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_account_id.php

 // created: 2022-08-26 16:58:07
$dictionary['Case']['fields']['account_id']['name']='account_id';
$dictionary['Case']['fields']['account_id']['type']='relate';
$dictionary['Case']['fields']['account_id']['dbType']='id';
$dictionary['Case']['fields']['account_id']['rname']='id';
$dictionary['Case']['fields']['account_id']['module']='Accounts';
$dictionary['Case']['fields']['account_id']['id_name']='account_id';
$dictionary['Case']['fields']['account_id']['reportable']=false;
$dictionary['Case']['fields']['account_id']['vname']='LBL_ACCOUNT_ID';
$dictionary['Case']['fields']['account_id']['audited']=false;
$dictionary['Case']['fields']['account_id']['massupdate']=false;
$dictionary['Case']['fields']['account_id']['comment']='The account to which the case is associated';
$dictionary['Case']['fields']['account_id']['importable']='required';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_correoelectronico_c.php

 // created: 2022-09-28 14:17:19
$dictionary['Case']['fields']['sasa_correoelectronico_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_correoelectronico_c']['labelValue']='Correo electrónico';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_correoelectronico_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['formula']='related($accounts,"email")';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_correoelectronico_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_comercial_c.php

 // created: 2022-09-28 14:17:19
$dictionary['Case']['fields']['sasa_comercial_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_comercial_c']['labelValue']='Comercial';
$dictionary['Case']['fields']['sasa_comercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_comercial_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_comercial_c']['formula']='related($accounts,"assigned_user_name")';
$dictionary['Case']['fields']['sasa_comercial_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_comercial_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_comercial_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_comercial_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_direccionsfc_c.php

 // created: 2022-09-28 14:17:20
$dictionary['Case']['fields']['sasa_direccionsfc_c']['labelValue']='Dirección';
$dictionary['Case']['fields']['sasa_direccionsfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_direccionsfc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_direccionsfc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_direccionsfc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_direccionsfc_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_funcionario_c.php

 // created: 2022-09-28 14:17:21
$dictionary['Case']['fields']['sasa_funcionario_c']['labelValue']='Funcionario/area';
$dictionary['Case']['fields']['sasa_funcionario_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_funcionario_c']['enforced']='';
$dictionary['Case']['fields']['sasa_funcionario_c']['dependency']='';
$dictionary['Case']['fields']['sasa_funcionario_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_funcionario_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_fechacierre_c.php

 // created: 2022-09-28 14:17:21
$dictionary['Case']['fields']['sasa_fechacierre_c']['labelValue']='Fecha de Cierre';
$dictionary['Case']['fields']['sasa_fechacierre_c']['enforced']='';
$dictionary['Case']['fields']['sasa_fechacierre_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_fechacierre_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_fechacierre_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_mostrardetalle_c.php

 // created: 2022-09-28 14:17:21
$dictionary['Case']['fields']['sasa_mostrardetalle_c']['labelValue']='Mostrar Detalle';
$dictionary['Case']['fields']['sasa_mostrardetalle_c']['dependency']='';
$dictionary['Case']['fields']['sasa_mostrardetalle_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_mostrardetalle_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_mostrardetalle_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_radalianzam2_c.php

 // created: 2022-09-28 14:17:23
$dictionary['Case']['fields']['sasa_radalianzam2_c']['labelValue']='Rad Alianza M2';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radalianzam2_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['dependency']='equal($sasa_tipodesolicitud_c,"quejas")';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_segmento_c.php

 // created: 2022-09-28 14:17:24
$dictionary['Case']['fields']['sasa_segmento_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_segmento_c']['labelValue']='Segmento';
$dictionary['Case']['fields']['sasa_segmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_segmento_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_segmento_c']['formula']='related($accounts,"sasa_segmento_c")';
$dictionary['Case']['fields']['sasa_segmento_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_segmento_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_segmento_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_segmento_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_respuesta_c.php

 // created: 2022-09-28 14:17:24
$dictionary['Case']['fields']['sasa_respuesta_c']['labelValue']='Respuesta';
$dictionary['Case']['fields']['sasa_respuesta_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_respuesta_c']['enforced']='';
$dictionary['Case']['fields']['sasa_respuesta_c']['dependency']='';
$dictionary['Case']['fields']['sasa_respuesta_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_respuesta_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tipoproducto_c.php

 // created: 2022-09-28 14:17:26
$dictionary['Case']['fields']['sasa_tipoproducto_c']['labelValue']='Tipo de producto';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_tipoproducto_c']['visibility_grid']=array (
  'trigger' => 'sasa_negocios_c',
  'values' => 
  array (
    'Inversiones' => 
    array (
      0 => '',
      1 => '1',
      2 => '2',
      3 => '5',
      4 => '6',
      5 => '7',
      6 => '9',
      7 => '8',
      8 => '3',
      9 => '10',
      10 => '4',
      11 => '13',
      12 => '15',
      13 => '16',
      14 => '14',
      15 => '12',
      16 => '11',
    ),
    'Fiduciaria' => 
    array (
      0 => '',
      1 => '17',
      2 => '18',
    ),
    'Valores' => 
    array (
      0 => '',
      1 => '19',
    ),
    '' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_tipodesolicitud_c.php

 // created: 2022-09-28 14:17:26
$dictionary['Case']['fields']['sasa_tipodesolicitud_c']['labelValue']='Tipo de Solicitud';
$dictionary['Case']['fields']['sasa_tipodesolicitud_c']['dependency']='';
$dictionary['Case']['fields']['sasa_tipodesolicitud_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_tipodesolicitud_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_tipodesolicitud_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/cases_contacts_1_Cases.php

// created: 2022-10-18 19:53:20
$dictionary["Case"]["fields"]["cases_contacts_1"] = array (
  'name' => 'cases_contacts_1',
  'type' => 'link',
  'relationship' => 'cases_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CASES_CONTACTS_1_FROM_CASES_TITLE',
  'id_name' => 'cases_contacts_1cases_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida.php

 // created: 2022-10-19 15:24:17
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['name']='sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['type']='id';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['vname']='LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['id_name']='sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['link']='sasa_sasa_tipificaciones_cases_1';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['table']='sasa_sasa_tipificaciones';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['module']='sasa_sasa_tipificaciones';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['side']='right';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_sasa_tipificaciones_cases_1_name.php

 // created: 2022-10-19 15:24:17
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['audited']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['massupdate']=true;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['vname']='LBL_SASA_SASA_TIPIFICACIONES_CASES_1_NAME_FIELD_TITLE';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_horaresolucion_c.php

 // created: 2022-10-21 14:59:14
$dictionary['Case']['fields']['sasa_horaresolucion_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_horaresolucion_c']['labelValue']='Hora resolución';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_horaresolucion_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['formula']='subStr(toString($sasa_fechacierre_c),11,2)';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_horaresolucion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_source.php

 // created: 2022-11-17 15:25:40
$dictionary['Case']['fields']['source']['len']=100;
$dictionary['Case']['fields']['source']['audited']=true;
$dictionary['Case']['fields']['source']['massupdate']=false;
$dictionary['Case']['fields']['source']['hidemassupdate']=false;
$dictionary['Case']['fields']['source']['comments']='An indicator of how the case was entered (ex: via web, email, etc.)';
$dictionary['Case']['fields']['source']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['source']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['source']['merge_filter']='disabled';
$dictionary['Case']['fields']['source']['calculated']=false;
$dictionary['Case']['fields']['source']['dependency']=false;
$dictionary['Case']['fields']['source']['options']='source_dom';
$dictionary['Case']['fields']['source']['required']=true;
$dictionary['Case']['fields']['source']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_primary_contact_id.php

 // created: 2022-11-18 20:05:52
$dictionary['Case']['fields']['primary_contact_id']['name']='primary_contact_id';
$dictionary['Case']['fields']['primary_contact_id']['type']='id';
$dictionary['Case']['fields']['primary_contact_id']['group']='primary_contact_name';
$dictionary['Case']['fields']['primary_contact_id']['reportable']=true;
$dictionary['Case']['fields']['primary_contact_id']['vname']='LBL_PRIMARY_CONTACT_ID';
$dictionary['Case']['fields']['primary_contact_id']['audited']=false;
$dictionary['Case']['fields']['primary_contact_id']['importable']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_primary_contact_name.php

 // created: 2022-11-18 20:05:52
$dictionary['Case']['fields']['primary_contact_name']['len']=255;
$dictionary['Case']['fields']['primary_contact_name']['massupdate']=true;
$dictionary['Case']['fields']['primary_contact_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['primary_contact_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['primary_contact_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['primary_contact_name']['reportable']=false;
$dictionary['Case']['fields']['primary_contact_name']['calculated']=false;
$dictionary['Case']['fields']['primary_contact_name']['related_fields']=array (
  0 => 'primary_contact_id',
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_segundocorreo_c.php

 // created: 2022-11-29 13:32:39
$dictionary['Case']['fields']['sasa_segundocorreo_c']['labelValue']='Email actualizado';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_segundocorreo_c']['enforced']='';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['dependency']='or(equal($sasa_detalle_c,"128"),equal($sasa_detalle_c,"129"))';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['required_formula']='or(equal($sasa_detalle_c,"128"),equal($sasa_detalle_c,"129"))';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_reenviarcierrecaso_c.php

 // created: 2022-12-12 19:37:40
$dictionary['Case']['fields']['sasa_reenviarcierrecaso_c']['labelValue']='Reenviar cierre de caso';
$dictionary['Case']['fields']['sasa_reenviarcierrecaso_c']['enforced']='false';
$dictionary['Case']['fields']['sasa_reenviarcierrecaso_c']['dependency']='equal($status,"4")';
$dictionary['Case']['fields']['sasa_reenviarcierrecaso_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_cfv_c.php

 // created: 2022-12-15 22:32:38
$dictionary['Case']['fields']['sasa_cfv_c']['labelValue']='Control fecha vencimiento';
$dictionary['Case']['fields']['sasa_cfv_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_cfv_c']['enforced']='';
$dictionary['Case']['fields']['sasa_cfv_c']['dependency']='';
$dictionary['Case']['fields']['sasa_cfv_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_cfv_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_instructivo_c.php

 // created: 2022-12-16 16:00:06
$dictionary['Case']['fields']['sasa_instructivo_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_instructivo_c']['labelValue']='Instructivo';
$dictionary['Case']['fields']['sasa_instructivo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_instructivo_c']['calculated']='true';
$dictionary['Case']['fields']['sasa_instructivo_c']['formula']='related($sasa_sasa_tipificaciones_cases_1,"sasa_instructivo_c")';
$dictionary['Case']['fields']['sasa_instructivo_c']['enforced']='true';
$dictionary['Case']['fields']['sasa_instructivo_c']['dependency']='';
$dictionary['Case']['fields']['sasa_instructivo_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_instructivo_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_instructivo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_proceso_c.php

 // created: 2022-12-27 21:21:04
$dictionary['Case']['fields']['sasa_proceso_c']['labelValue']='Proceso';
$dictionary['Case']['fields']['sasa_proceso_c']['dependency']='';
$dictionary['Case']['fields']['sasa_proceso_c']['required_formula']='or(equal($source,"Portal transaccional"),equal($source,"Portal publico"),equal($source,"Telefono"))';
$dictionary['Case']['fields']['sasa_proceso_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_proceso_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_negocios_c.php

 // created: 2022-12-27 21:21:39
$dictionary['Case']['fields']['sasa_negocios_c']['labelValue']='Negocios';
$dictionary['Case']['fields']['sasa_negocios_c']['dependency']='';
$dictionary['Case']['fields']['sasa_negocios_c']['required_formula']='or(equal($source,"Portal transaccional"),equal($source,"Portal publico"),equal($source,"Telefono"))';
$dictionary['Case']['fields']['sasa_negocios_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_negocios_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_name.php

 // created: 2023-01-10 19:20:48
$dictionary['Case']['fields']['name']['len']='255';
$dictionary['Case']['fields']['name']['massupdate']=false;
$dictionary['Case']['fields']['name']['hidemassupdate']=false;
$dictionary['Case']['fields']['name']['comments']='The short description of the bug';
$dictionary['Case']['fields']['name']['importable']='false';
$dictionary['Case']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['name']['merge_filter']='disabled';
$dictionary['Case']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.53',
  'searchable' => true,
);
$dictionary['Case']['fields']['name']['calculated']='1';
$dictionary['Case']['fields']['name']['formula']='ifElse(
equal($sasa_tipodesolicitud_c,"quejas"),concat(toString($case_number)," - ",toString($sasa_radalianza_c)," - ",toString($sasa_radalianzam2_c)," - ",related($sasa_sasa_tipificaciones_cases_1,"name")),

ifElse(
equal($sasa_tipodesolicitud_c,"peticion"),concat(toString($case_number)," - ",related($sasa_sasa_tipificaciones_cases_1,"name")),

concat(toString($case_number)," - ",toString($sasa_tipodesolicitud_c))
)
)';
$dictionary['Case']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_envionotificacion_c.php

 // created: 2023-01-27 15:25:45
$dictionary['Case']['fields']['sasa_envionotificacion_c']['labelValue']='Enviar Notificación al cliente';
$dictionary['Case']['fields']['sasa_envionotificacion_c']['dependency']='';
$dictionary['Case']['fields']['sasa_envionotificacion_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_envionotificacion_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_envionotificacion_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_detalle_c.php

 // created: 2023-02-02 19:30:49
$dictionary['Case']['fields']['sasa_detalle_c']['labelValue']='Detalle';
$dictionary['Case']['fields']['sasa_detalle_c']['dependency']='';
$dictionary['Case']['fields']['sasa_detalle_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_detalle_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_detalle_c']['visibility_grid']=array (
  'trigger' => 'sasa_subproceso_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '1',
    ),
    2 => 
    array (
      0 => '',
      1 => '2',
      2 => '3',
    ),
    3 => 
    array (
      0 => '',
      1 => '4',
      2 => '5',
      3 => '6',
      4 => '10',
      5 => '12',
      6 => '13',
      7 => '14',
      8 => '15',
      9 => '2',
    ),
    4 => 
    array (
      0 => '',
      1 => '7',
      2 => '8',
      3 => '9',
      4 => '35',
    ),
    5 => 
    array (
      0 => '',
      1 => '11',
    ),
    6 => 
    array (
      0 => '',
      1 => '16',
    ),
    7 => 
    array (
      0 => '',
      1 => '17',
      2 => '18',
    ),
    8 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
      3 => '22',
      4 => '23',
      5 => '24',
      6 => '19',
      7 => '25',
      8 => '66',
      9 => '67',
      10 => '28',
      11 => '162',
      12 => '163',
    ),
    9 => 
    array (
      0 => '',
      1 => '26',
      2 => '27',
      3 => '28',
      4 => '29',
    ),
    10 => 
    array (
      0 => '',
      1 => '37',
      2 => '38',
      3 => '39',
    ),
    11 => 
    array (
      0 => '',
      1 => '40',
    ),
    12 => 
    array (
      0 => '',
      1 => '41',
      2 => '42',
      3 => '43',
      4 => '47',
      5 => '56',
      6 => '58',
      7 => '49',
      8 => '63',
      9 => '66',
      10 => '62',
      11 => '70',
    ),
    13 => 
    array (
      0 => '',
      1 => '47',
      2 => '56',
      3 => '58',
      4 => '59',
      5 => '60',
      6 => '61',
    ),
    14 => 
    array (
      0 => '',
      1 => '62',
      2 => '63',
      3 => '64',
    ),
    15 => 
    array (
      0 => '',
      1 => '102',
      2 => '19',
      3 => '70',
      4 => '71',
      5 => '21',
    ),
    16 => 
    array (
      0 => '',
      1 => '19',
      2 => '75',
      3 => '77',
      4 => '21',
      5 => '78',
      6 => '79',
    ),
    17 => 
    array (
      0 => '',
      1 => '80',
      2 => '81',
      3 => '82',
      4 => '83',
      5 => '84',
      6 => '85',
      7 => '86',
    ),
    18 => 
    array (
      0 => '',
      1 => '87',
      2 => '88',
      3 => '89',
    ),
    19 => 
    array (
      0 => '',
      1 => '90',
      2 => '91',
      3 => '92',
      4 => '93',
      5 => '94',
    ),
    20 => 
    array (
      0 => '',
      1 => '95',
      2 => '96',
      3 => '97',
      4 => '98',
    ),
    21 => 
    array (
      0 => '',
      1 => '99',
      2 => '100',
      3 => '101',
    ),
    22 => 
    array (
      0 => '',
      1 => '95',
      2 => '102',
      3 => '88',
      4 => '103',
      5 => '96',
    ),
    23 => 
    array (
      0 => '',
      1 => '104',
      2 => '105',
      3 => '106',
    ),
    24 => 
    array (
      0 => '',
      1 => '107',
      2 => '108',
    ),
    25 => 
    array (
      0 => '',
      1 => '109',
    ),
    26 => 
    array (
      0 => '',
      1 => '110',
      2 => '111',
      3 => '112',
      4 => '113',
      5 => '114',
      6 => '95',
      7 => '169',
      8 => '170',
    ),
    27 => 
    array (
      0 => '',
      1 => '110',
      2 => '115',
      3 => '116',
      4 => '95',
      5 => '112',
      6 => '117',
      7 => '113',
      8 => '118',
      9 => '119',
      10 => '120',
      11 => '122',
      12 => '170',
      13 => '169',
    ),
    28 => 
    array (
      0 => '',
      1 => '110',
      2 => '116',
      3 => '95',
      4 => '123',
      5 => '124',
    ),
    29 => 
    array (
      0 => '',
      1 => '70',
    ),
    30 => 
    array (
      0 => '',
      1 => '125',
      2 => '126',
      3 => '127',
    ),
    31 => 
    array (
      0 => '',
      1 => '66',
    ),
    32 => 
    array (
      0 => '',
      1 => '128',
      2 => '129',
    ),
    33 => 
    array (
      0 => '',
      1 => '130',
    ),
    34 => 
    array (
      0 => '',
      1 => '95',
      2 => '131',
      3 => '132',
      4 => '133',
      5 => '134',
      6 => '135',
    ),
    35 => 
    array (
      0 => '',
      1 => '136',
      2 => '137',
    ),
    36 => 
    array (
      0 => '',
      1 => '138',
    ),
    37 => 
    array (
      0 => '',
      1 => '139',
    ),
    38 => 
    array (
      0 => '',
      1 => '141',
      2 => '66',
    ),
    39 => 
    array (
      0 => '',
      1 => '142',
      2 => '143',
    ),
    40 => 
    array (
      0 => '',
      1 => '144',
      2 => '145',
      3 => '146',
      4 => '148',
      5 => '147',
    ),
    41 => 
    array (
      0 => '',
      1 => '59',
      2 => '149',
      3 => '150',
      4 => '151',
    ),
    42 => 
    array (
      0 => '',
      1 => '154',
      2 => '152',
      3 => '153',
      4 => '168',
    ),
    43 => 
    array (
      0 => '',
      1 => '155',
      2 => '156',
    ),
    44 => 
    array (
      0 => '',
      1 => '157',
      2 => '158',
      3 => '159',
    ),
    45 => 
    array (
      0 => '',
      1 => '19',
      2 => '21',
    ),
    46 => 
    array (
      0 => '',
      1 => '164',
      2 => '165',
      3 => '166',
      4 => '167',
    ),
    '' => 
    array (
      0 => '',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_subproceso_c.php

 // created: 2023-02-02 19:45:03
$dictionary['Case']['fields']['sasa_subproceso_c']['labelValue']='Subproceso';
$dictionary['Case']['fields']['sasa_subproceso_c']['dependency']='';
$dictionary['Case']['fields']['sasa_subproceso_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_subproceso_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_subproceso_c']['visibility_grid']=array (
  'trigger' => 'sasa_proceso_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '33',
      2 => '34',
      3 => '35',
      4 => '36',
      5 => '37',
    ),
    2 => 
    array (
      0 => '',
    ),
    3 => 
    array (
      0 => '',
      1 => '23',
      2 => '24',
      3 => '25',
      4 => '26',
      5 => '27',
      6 => '28',
    ),
    4 => 
    array (
      0 => '',
      1 => '7',
      2 => '39',
    ),
    5 => 
    array (
      0 => '',
      1 => '7',
      2 => '8',
      3 => '9',
      4 => '3',
      5 => '4',
      6 => '10',
      7 => '29',
      8 => '30',
      9 => '31',
      10 => '32',
    ),
    6 => 
    array (
      0 => '',
      1 => '7',
      2 => '16',
      3 => '46',
    ),
    7 => 
    array (
      0 => '',
      1 => '12',
      2 => '29',
      3 => '8',
      4 => '7',
      5 => '31',
    ),
    8 => 
    array (
      0 => '',
      1 => '7',
      2 => '13',
      3 => '14',
      4 => '8',
      5 => '29',
      6 => '31',
    ),
    9 => 
    array (
      0 => '',
      1 => '7',
      2 => '8',
      3 => '29',
      4 => '41',
      5 => '31',
    ),
    10 => 
    array (
      0 => '',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
      6 => '6',
      7 => '29',
      8 => '30',
    ),
    14 => 
    array (
      0 => '',
      1 => '15',
      2 => '29',
    ),
    15 => 
    array (
      0 => '',
      1 => '40',
    ),
    16 => 
    array (
      0 => '',
      1 => '42',
      2 => '43',
      3 => '44',
      4 => '32',
    ),
    17 => 
    array (
      0 => '',
      1 => '14',
      2 => '38',
    ),
    18 => 
    array (
      0 => '',
      1 => '11',
      2 => '21',
      3 => '22',
    ),
    20 => 
    array (
      0 => '',
    ),
    21 => 
    array (
      0 => '',
    ),
    22 => 
    array (
      0 => '',
    ),
    23 => 
    array (
      0 => '',
    ),
    24 => 
    array (
      0 => '',
      1 => '17',
      2 => '18',
      3 => '19',
      4 => '20',
    ),
    25 => 
    array (
      0 => '',
      1 => '45',
    ),
    26 => 
    array (
      0 => '',
    ),
    '' => 
    array (
      0 => '',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-03 09:42:31
VardefManager::createVardef('Cases', 'Case', [
                                'customer_journey_parent',
                        ]);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estadorespuesta_c.php

 // created: 2023-03-10 13:24:19
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['labelValue']='Estado de respuesta';
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['enforced']='';
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['dependency']='';
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_estadorespuesta_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_entidadresponsable_c.php

 // created: 2023-03-10 13:25:20
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['labelValue']='Entidad responsable';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['enforced']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['dependency']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_radicadosalida_c.php

 // created: 2023-03-10 13:26:09
$dictionary['Case']['fields']['sasa_radicadosalida_c']['labelValue']='Radicado de salida';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radicadosalida_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['dependency']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radicadosalida_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_estado2_c.php

 // created: 2023-03-10 13:43:37
$dictionary['Case']['fields']['sasa_estado2_c']['labelValue']='Estado 2';
$dictionary['Case']['fields']['sasa_estado2_c']['enforced']='';
$dictionary['Case']['fields']['sasa_estado2_c']['dependency']='';
$dictionary['Case']['fields']['sasa_estado2_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_estado2_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_estado2_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_checkprorroga_c.php

 // created: 2023-03-24 14:01:49
$dictionary['Case']['fields']['sasa_checkprorroga_c']['labelValue']='Check prorroga';
$dictionary['Case']['fields']['sasa_checkprorroga_c']['enforced']='';
$dictionary['Case']['fields']['sasa_checkprorroga_c']['dependency']='';
$dictionary['Case']['fields']['sasa_checkprorroga_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_sasa_diasprorroga_c.php

 // created: 2023-03-31 19:46:06
$dictionary['Case']['fields']['sasa_diasprorroga_c']['labelValue']='Días prorroga';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_diasprorroga_c']['enforced']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['dependency']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Vardefs/sugarfield_status.php

 // created: 2023-04-10 20:40:33
$dictionary['Case']['fields']['status']['default']='';
$dictionary['Case']['fields']['status']['massupdate']=true;
$dictionary['Case']['fields']['status']['hidemassupdate']=false;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';
$dictionary['Case']['fields']['status']['calculated']=false;
$dictionary['Case']['fields']['status']['dependency']=false;
$dictionary['Case']['fields']['status']['options']='status_list';
$dictionary['Case']['fields']['status']['required']=true;
$dictionary['Case']['fields']['status']['visibility_grid']=array (
);

 
?>
