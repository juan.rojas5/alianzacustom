<?php
// created: 2019-01-04 21:54:39
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_productosafav_sasa_movimientosaf_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1SASA_PRODUCTOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'sasa_nombrefondo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NOMBREFONDO_C',
    'width' => 10,
  ),
  'sasa_nombresubcuenta_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NOMBRESUBCUENTA_C',
    'width' => 10,
  ),
  'sasa_encargo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_ENCARGO_C',
    'width' => 10,
  ),
  'sasa_fecha_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHA_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_motivo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_MOTIVO_C',
    'width' => 10,
  ),
  'sasa_valor_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALOR_C',
    'width' => 10,
  ),
  'sasa_formapago_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_FORMAPAGO_C',
    'width' => 10,
  ),
  'sasa_concepto_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CONCEPTO_C',
    'width' => 10,
  ),
  'sasa_cuentabancaria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CUENTABANCARIA_C',
    'width' => 10,
  ),
  'sasa_compartimiento_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_COMPARTIMIENTO_C',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);