<?php
 // created: 2023-02-01 19:09:49
$dictionary['Purchase']['fields']['sasa_codregional_c']['labelValue']='codigo regional';
$dictionary['Purchase']['fields']['sasa_codregional_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codregional_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codregional_c']['readonly_formula']='';

 ?>