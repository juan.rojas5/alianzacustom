<?php
 // created: 2022-11-18 20:46:39
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['labelValue']='Solucionador_Inmediato';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_inmediato__c']['readonly_formula']='';

 ?>