<?php
 // created: 2018-06-26 19:49:50

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Llamada en Frío',
  'Existing Customer' => 'Cliente Existente',
  'Self Generated' => 'Generado Automáticamente',
  'Employee' => 'Empleado',
  'Partner' => 'Socio',
  'Public Relations' => 'Relaciones Públicas',
  'Direct Mail' => 'Correo directo',
  'Conference' => 'Conferencia',
  'Trade Show' => 'Exposición',
  'Web Site' => 'Sitio Web',
  'Word of mouth' => 'Referido',
  'Email' => 'Correo Electrónico',
  'Campaign' => 'Campaña',
  'Support Portal User Registration' => 'Registro de Usuario del Portal de Soporte',
  'Evento Social' => 'Evento Social',
  'Recomendacion' => 'Recomendación',
  'Other' => 'Otros',
);