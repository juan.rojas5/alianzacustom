<?php
 // created: 2018-11-21 22:21:30
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['calculated']='1';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosavdivisas_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAVDivisas']['fields']['sasa_categoria_c']['enforced']=true;

 ?>