<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

/**
 * Variables.less
 * Variables to customize the look and feel of Bootstrap
 */

$lessdefs = array(

    'colors' => array(
        /**
        Linea de carga debajo del menu
         */
        'BorderColor' => '#0072ce',
        /**
        Barra  menu
         */
        'NavigationBar' => '#00A3E0',
        /**
        Botones azules en la vista por defecto
         */
        'PrimaryButton' => '#0072CE',
        
        
    ),
);
