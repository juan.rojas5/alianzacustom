<?php
$dependencies['Leads']['SetOptionDepartamento16'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cundinamarca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","La Vega","El Penon","Agua de Dios","Alban","Anapoima","Anolaima","Apulo","Arbelaez","Beltran","Bituima","Bojaca","Cabrera","Cachipay","Cajica","Caparrapi","Caqueza","Carmen de Carupa","Chaguani","Chia","Chipaque","Choachi","Choconta","Cogua","Cota","Cucunuba","El Colegio","El Rosal","Facatativa","Fomeque","Fosca","Funza","Fuquene","Fusagasuga","Gachala","Gachancipa","Gacheta","Gama","Girardot","Granada","Guacheta","Guaduas","Guasca","Guataqui","Guatavita","Guayabal de Siquima","Guayabetal","Gutierrez","Jerusalen","Junin","La Calera","La Mesa","La Palma","La Pena","Lenguazaque","Macheta","Madrid","Manta","Medina","Mosquera","Narino","Nemocon","Nilo","Nimaima","Nocaima","Pacho","Paime","Pandi","Paratebueno","Pasca","Puerto Salgar","Puli","Quebradanegra","Quetame","Quipile","Ricaurte","San Antonio del Tequendama","San Bernardo","San Cayetano","San Francisco","San Juan de Rio Seco","Sasaima","Sesquile","Sibate","Silvania","Simijaca","Soacha","Sopo","Subachoque","Suesca","Supata","Susa","Sutatausa","Tabio","Tausa","Tena","Tenjo","Tibacuy","Tibirita","Tocaima","Tocancipa","Topaipi","Ubala","Ubaque","Une","utica","Venecia","Vergara","Viani","Villa de San Diego de Ubate","Villagomez","Villapinzon","Villeta","Viota","Yacopi","Zipacon","Zipaquira")',
           'labels' => 'createList("","La Vega","El Peñón","Agua de Dios","Albán","Anapoima","Anolaima","Apulo","Arbeláez","Beltrán","Bituima","Bojacá","Cabrera","Cachipay","Cajicá","Caparrapí","Caqueza","Carmen de Carupa","Chaguaní","Chía","Chipaque","Choachí","Chocontá","Cogua","Cota","Cucunubá","El Colegio","El Rosal","Facatativá","Fomeque","Fosca","Funza","Fúquene","Fusagasugá","Gachala","Gachancipá","Gachetá","Gama","Girardot","Granada","Guachetá","Guaduas","Guasca","Guataquí","Guatavita","Guayabal de Siquima","Guayabetal","Gutiérrez","Jerusalén","Junín","La Calera","La Mesa","La Palma","La Peña","Lenguazaque","Macheta","Madrid","Manta","Medina","Mosquera","Nariño","Nemocón","Nilo","Nimaima","Nocaima","Pacho","Paime","Pandi","Paratebueno","Pasca","Puerto Salgar","Pulí","Quebradanegra","Quetame","Quipile","Ricaurte","San Antonio del Tequendama","San Bernardo","San Cayetano","San Francisco","San Juan de Río Seco","Sasaima","Sesquilé","Sibaté","Silvania","Simijaca","Soacha","Sopó","Subachoque","Suesca","Supatá","Susa","Sutatausa","Tabio","Tausa","Tena","Tenjo","Tibacuy","Tibirita","Tocaima","Tocancipá","Topaipí","Ubalá","Ubaque","Une","Útica","Venecia","Vergara","Vianí","Villa de San Diego de Ubate","Villagómez","Villapinzón","Villeta","Viotá","Yacopí","Zipacón","Zipaquirá")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
