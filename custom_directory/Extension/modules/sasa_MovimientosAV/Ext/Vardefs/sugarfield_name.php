<?php
 // created: 2018-11-21 22:04:59
$dictionary['sasa_MovimientosAV']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAV']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAV']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAV']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAV']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAV']['fields']['name']['formula']='$sasa_nemotecnico_c';
$dictionary['sasa_MovimientosAV']['fields']['name']['enforced']=true;

 ?>