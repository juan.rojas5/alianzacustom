<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_Categorias/Ext/Vardefs/sasa_categorias_sasa_productosafav_1_sasa_Categorias.php

// created: 2019-01-04 21:22:57
$dictionary["sasa_Categorias"]["fields"]["sasa_categorias_sasa_productosafav_1"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_CATEGORIAS_TITLE',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Categorias/Ext/Vardefs/sasa_categorias_sasas_saldosconsolidados_1_sasa_Categorias.php

// created: 2019-01-04 22:24:53
$dictionary["sasa_Categorias"]["fields"]["sasa_categorias_sasas_saldosconsolidados_1"] = array (
  'name' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'sasaS_SaldosConsolidados',
  'bean_name' => 'sasaS_SaldosConsolidados',
  'vname' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASA_CATEGORIAS_TITLE',
  'id_name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_Categorias/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_Categorias']['full_text_search']=true;

?>
