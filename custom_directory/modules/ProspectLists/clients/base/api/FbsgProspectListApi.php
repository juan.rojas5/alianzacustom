<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


require_once('clients/base/api/ModuleApi.php');
require_once('modules/fbsg_ConstantContactIntegration/include/ConstantContact.php');

class FbsgProspectListApi extends ModuleApi
{
    public function registerApiRest()
    {
        return array(
            'getCcLists' => array(
                'reqType' => 'GET',
                'path' => array('<module>', 'getCcLists'),
                'pathVars' => array('', ''),
                'method' => 'getCcLists',
                'shortHelp' => 'Get all CC prospect lists',
                'longHelp' => 'include/api/help/module_addtolist_post_help.html',
            ),
            'getBeanCcLists' => array(
                'reqType' => 'GET',
                'path' => array('<module>', 'getBeanCcLists'),
                'pathVars' => array('', ''),
                'method' => 'getBeanCcLists',
                'shortHelp' => 'Get all CC prospect lists for the given bean',
                'longHelp' => 'include/api/help/module_addtolist_post_help.html',
            ),
            'sendToCc' => array(
                'reqType' => 'POST',
                'path' => array('<module>', 'sendToCc', '?'),
                'pathVars' => array('module', 'call', 'record'),
                'method' => 'SendToCc',
                'shortHelp' => 'Send this list to CC, or queue it up for scheduled sending',
                'longHelp' => ''
            ),
        );
    }

    public function getCcLists($api, $args)
    {
        $pl = BeanFactory::newBean('ProspectLists');
        $query = new SugarQuery();
        $query->select(array('cc_id', 'name'));
        $query->from($pl);
        $query->where()->notNull('cc_id');
        $query->where()->notEquals('cc_id', '');
        $query->where()->notEquals('deleted', '1');

        $results = $query->execute();

        $lists = array();

        foreach ($results as $row) {
            $name = $row['name'];
            $lists[$row['cc_id']] = $name;
        }

        return $lists;
    }

    public function getBeanCcLists($api, $args) {
        $cc = SugarCC::GetOnlyCCUtility();
        $this->requireArgs($args, array('bean_module', 'bean_id'));
        if(!$cc) return array('error' => 'License Expired, or no CC account configured');

        $id = $args['bean_id'];
        $module = $args['bean_module'];

        $bean = BeanFactory::getBean($module, $id);
        if(empty($bean->id)) return array('status' => 'No bean found');
        $ccPerson = new SugarCCPerson($cc);
        $lists = $ccPerson->getCcListIdsAndNames($bean);

        return $lists;
    }

    public function SendToCc($api, $args) {
        $this->requireArgs($args, array('record'));
        $cc = SugarCC::GetOnlyCC();

        if(!$cc) return array('error' => true, 'message' => 'License expired, or no CC account configured.');

        $id = $args['record'];

        $pl = BeanFactory::getBean('ProspectLists', $id);

        if(!$pl || !$pl->id) return array('error' => true, 'message' => 'Could not retrieve prospect list ' . $id);

        $count = SugarCCList::GetListCount($pl->id);

        if($count > $cc->maxPLToSend) {
            $pl->sync_to_cc = true;
            $pl->save();

            return array(
                'error' => false,
                'message' => 'Due to its size, this list will be synchronized in the background.',
            );
        }

        $ut = new CCBasic($cc->name, $cc->password);
        $ccMass = new SugarCCMass($ut);

        $response = $ccMass->SyncListToCC($pl);

        if($response['error']) {
            $message = "There was an error sending the list to CC: " . $response['error_message'];

            return array(
                'error' => true,
                'message' => $message
            );
        }

        if($response['created']) {
            $message = "New List created on CC. ";
        } else $message = "Updated list on CC. ";

        $message .= $response['num_sent'] . " contacts sent to CC.";

        return array(
            'error' => false,
            'message' => $message
        );
    }
}
