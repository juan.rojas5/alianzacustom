<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/LogicHooks/leads_before_save.php


$hook_array['before_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'leads_before_save',

  //The PHP file where your class is located.
  'custom/modules/Leads/leads_before_save.php',

  //The class the method is in.
  'leads_before_save',

  //The method to call.
  'before_save'
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/LogicHooks/leads_mirror_primary_street.php

// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
	// position, file, function 

	/*Georeferencing Leads*/
	$hook_array['before_save'][] = Array(99,'Mirror field georeferencing','custom/modules/Leads/georeferencer_hook.php','Georeferencer_Leads','before_save');


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/LogicHooks/convercion_lead_manual.php

$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	10,

	//Label. A string value to identify the hook.
	'convercion_lead_manual',

	//The PHP file where your class is located.
	'custom/modules/Leads/convercion_lead_manual.php',

	//The class the method is in.
	'convercion_lead_manual',

	//The method to call.
	'after_save'
);


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/LogicHooks/BeforeSaveNumIdUnico.php


$hook_array['before_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'BeforeSaveNumIdUnico',

  //The PHP file where your class is located.
  'custom/modules/Leads/BeforeSaveNumIdUnico.php',

  //The class the method is in.
  'BeforeSaveNumIdUnico',

  //The method to call.
  'before_save'
);


?>
