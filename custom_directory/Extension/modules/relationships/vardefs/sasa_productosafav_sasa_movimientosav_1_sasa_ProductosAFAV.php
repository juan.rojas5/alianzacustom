<?php
// created: 2019-01-04 21:28:46
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosav_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAV',
  'bean_name' => 'sasa_MovimientosAV',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
