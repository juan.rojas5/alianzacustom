<?php
 // created: 2022-10-25 23:16:55
$dictionary['Account']['fields']['sasa_estadocuenta_c']['labelValue']='Estado de la Cuenta';
$dictionary['Account']['fields']['sasa_estadocuenta_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_estadocuenta_c']['enforced']='';
$dictionary['Account']['fields']['sasa_estadocuenta_c']['dependency']='equal($account_type,"Customer")';

 ?>