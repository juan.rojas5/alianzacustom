<?php
 // created: 2022-09-16 13:30:42
$dictionary['Task']['fields']['sasa_control_status_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_control_status_c']['labelValue']='Control Status';
$dictionary['Task']['fields']['sasa_control_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_control_status_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_control_status_c']['formula']='concat($status,$sasa_ctrl_sec_c)';
$dictionary['Task']['fields']['sasa_control_status_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_control_status_c']['dependency']='';
$dictionary['Task']['fields']['sasa_control_status_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_control_status_c']['readonly_formula']='';

 ?>