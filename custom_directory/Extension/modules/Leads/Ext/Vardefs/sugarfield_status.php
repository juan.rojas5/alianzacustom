<?php
 // created: 2018-08-21 01:13:50
$dictionary['Lead']['fields']['status']['len']=100;
$dictionary['Lead']['fields']['status']['required']=true;
$dictionary['Lead']['fields']['status']['massupdate']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['calculated']=false;
$dictionary['Lead']['fields']['status']['dependency']=false;
$dictionary['Lead']['fields']['status']['default']='Assigned';
$dictionary['Lead']['fields']['status']['options']='lead_status_dom';

 ?>