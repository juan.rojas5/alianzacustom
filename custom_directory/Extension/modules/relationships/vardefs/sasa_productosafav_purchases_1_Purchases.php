<?php
// created: 2023-02-06 20:21:32
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1"] = array (
  'name' => 'sasa_productosafav_purchases_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_purchases_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1_name"] = array (
  'name' => 'sasa_productosafav_purchases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_purchases_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE_ID',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_purchases_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
