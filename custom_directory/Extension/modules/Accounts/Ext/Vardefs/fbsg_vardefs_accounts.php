<?php
$dictionary['Account']['fields']['fbsg_ccintegrationlog_accounts'] = array(
    'name' => 'fbsg_ccintegrationlog_accounts',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_accounts',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Account"]["fields"]["prospect_list_accounts"] = array(
    'name' => 'prospect_list_accounts',
    'type' => 'link',
    'relationship' => 'prospect_list_accounts',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Account';
include('custom/include/fbsg_cc_newvars.php');
