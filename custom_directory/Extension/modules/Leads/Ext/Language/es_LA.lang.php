<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_FIRST_NAME'] = 'Nombres:';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono:';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre de la empresa:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_LEAD_SOURCE_DESCRIPTION'] = 'Descripción de Toma de Contacto:';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Tel. Asistente';
$mod_strings['LBL_HOME_PHONE'] = 'Tel. Casa:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo:';
