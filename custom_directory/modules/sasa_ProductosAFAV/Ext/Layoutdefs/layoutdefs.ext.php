<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasa_saldosaf_1_sasa_ProductosAFAV.php

 // created: 2019-01-04 21:25:02
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_saldosaf_1'] = array (
  'order' => 100,
  'module' => 'sasa_SaldosAF',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_saldosaf_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasa_saldosav_1_sasa_ProductosAFAV.php

 // created: 2019-01-04 21:26:58
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_saldosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_SaldosAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_saldosav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasa_movimientosaf_1_sasa_ProductosAFAV.php

 // created: 2019-01-04 21:27:42
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_movimientosaf_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAF',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_movimientosaf_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasa_movimientosav_1_sasa_ProductosAFAV.php

 // created: 2019-01-04 21:28:46
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_movimientosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_movimientosav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasa_movimientosavdivisas_1_sasa_ProductosAFAV.php

 // created: 2019-01-04 21:30:05
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_movimientosavdivisas_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAVDivisas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_sasap_presupuestoxproducto_1_sasa_ProductosAFAV.php

 // created: 2019-03-08 18:57:52
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasap_presupuestoxproducto_1'] = array (
  'order' => 100,
  'module' => 'sasaP_PresupuestoxProducto',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/sasa_productosafav_purchases_1_sasa_ProductosAFAV.php

 // created: 2023-02-06 20:21:32
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_purchases_1'] = array (
  'order' => 100,
  'module' => 'Purchases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_purchases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/_overridesasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_saldosaf_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_ProductosAFAV']['subpanel_setup']['sasa_productosafav_sasa_saldosaf_1']['override_subpanel_name'] = 'sasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_saldosaf_1';

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/_overridesasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_saldosav_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_ProductosAFAV']['subpanel_setup']['sasa_productosafav_sasa_saldosav_1']['override_subpanel_name'] = 'sasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_saldosav_1';

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/_overridesasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosaf_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_ProductosAFAV']['subpanel_setup']['sasa_productosafav_sasa_movimientosaf_1']['override_subpanel_name'] = 'sasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosaf_1';

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/_overridesasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosav_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_ProductosAFAV']['subpanel_setup']['sasa_productosafav_sasa_movimientosav_1']['override_subpanel_name'] = 'sasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosav_1';

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Layoutdefs/_overridesasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosavdivisas_1.php

//auto-generated file DO NOT EDIT
$layout_defs['sasa_ProductosAFAV']['subpanel_setup']['sasa_productosafav_sasa_movimientosavdivisas_1']['override_subpanel_name'] = 'sasa_ProductosAFAV_subpanel_sasa_productosafav_sasa_movimientosavdivisas_1';

?>
