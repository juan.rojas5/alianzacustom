<?php
$module_name = 'sasa_MovimientosAVDivisas';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'accounts_sasa_movimientosavdivisas_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_cantidad_c',
                'label' => 'LBL_SASA_CANTIDAD_C',
              ),
              5 => 
              array (
                'name' => 'sasa_fecha_c',
                'label' => 'LBL_SASA_FECHA_C',
              ),
              6 => 
              array (
                'name' => 'sasa_tipo_c',
                'label' => 'LBL_SASA_TIPO_C',
              ),
              7 => 
              array (
                'name' => 'sasa_iva_c',
                'label' => 'LBL_SASA_IVA_C',
              ),
              8 => 
              array (
                'name' => 'sasa_moneda_c',
                'label' => 'LBL_SASA_MONEDA_C',
              ),
              9 => 
              array (
                'name' => 'sasa_retefte_c',
                'label' => 'LBL_SASA_RETEFTE_C',
              ),
              10 => 
              array (
                'name' => 'sasa_reteiva_c',
                'label' => 'LBL_SASA_RETEIVA_C',
              ),
              11 => 
              array (
                'name' => 'sasa_tasadivisadolar_c',
                'label' => 'LBL_SASA_TASADIVISADOLAR_C',
              ),
              12 => 
              array (
                'name' => 'sasa_tasapesodolar_c',
                'label' => 'LBL_SASA_TASAPESODOLAR_C',
              ),
              13 => 
              array (
                'name' => 'sasa_valorbruto_c',
                'label' => 'LBL_SASA_VALORBRUTO_C',
              ),
              14 => 
              array (
                'name' => 'sasa_valorneto_c',
                'label' => 'LBL_SASA_VALORNETO_C',
              ),
              15 => 
              array (
                'name' => 'sasa_ocobros_c',
                'label' => 'LBL_SASA_OCOBROS_C',
              ),
              16 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              17 => 
              array (
                'name' => 'tag',
                'studio' => 
                array (
                  'portal' => false,
                  'base' => 
                  array (
                    'popuplist' => false,
                  ),
                  'mobile' => 
                  array (
                    'wirelesseditview' => true,
                    'wirelessdetailview' => true,
                  ),
                ),
                'label' => 'LBL_TAGS',
              ),
              18 => 'assigned_user_name',
              19 => 'team_name',
              20 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              21 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              22 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              23 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
