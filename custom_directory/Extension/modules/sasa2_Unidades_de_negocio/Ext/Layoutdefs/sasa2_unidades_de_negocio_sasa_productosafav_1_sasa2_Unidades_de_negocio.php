<?php
 // created: 2023-02-06 20:17:53
$layout_defs["sasa2_Unidades_de_negocio"]["subpanel_setup"]['sasa2_unidades_de_negocio_sasa_productosafav_1'] = array (
  'order' => 100,
  'module' => 'sasa_ProductosAFAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'get_subpanel_data' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
