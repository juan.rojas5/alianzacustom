<?php
 // created: 2022-11-16 02:16:39
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['labelValue']='Fondo de Cesantías';
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_fondocesantias_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>