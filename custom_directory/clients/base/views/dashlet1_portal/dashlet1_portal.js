({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	Username:"**PAI**",
	Password:"**PAI**",
	url:'**PAI**',
	ws:'**PAI**',
	initialize: function(options) {
		var self = this;
		dashlet1 = self;		
		if (typeof app === 'undefined') app = App;		
			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet1_crear').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet1_No_Identificacion").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet1_No_Identificacion").css("border", "none"); 
						$("#dashlet1_No_Identificacion").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet1_No_Identificacion").attr('readonly', true);
					}
					
					$("#dashlet1_Compania").change( function(){
						self.dashlet1_Compania_change();	
					});
					
					$("#dashlet1_crear").click( function(){
						self.dashlet1_bizagi();	
					});
					
					$("#dashlet1_crear").show();
					self.dashlet1_bizagi_tipo_de_solicitud();
					self.dashlet1_Compania_change();
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet1_bizagi: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet1_bizagi_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		if (!_.isEmpty( $("#dashlet1_No_Identificacion").val() )){	
			self.dashlet1_nombre_cuenta();//poblando nombre cuenta
			self.dashlet1_crear();//creando caso
		}
		else{
			app.alert.dismiss('dashlet1_bizagi_procesando');
			app.alert.show('error_procesando_id_cliente', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},
	dashlet1_Compania_change: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		$("#dashlet1_Asunto").empty();
		self.dashlet_bizagi_tipoParametrica('2','',$("#dashlet1_Compania").val());
		if (!_.isEmpty( self.respuesta )){ 		
			if(!$.isArray( self.respuesta )){			
				var temp = self.respuesta;
				self.respuesta = [];
				self.respuesta.push(temp);
			}
			console.log('categoria ('+$("#dashlet1_Compania").val()+'):',self.respuesta);	
			
			if (Array.isArray(self.respuesta)){
				if (typeof self.respuesta[0] !== 'undefined'){
					if (typeof self.respuesta[0].lista !== 'undefined'){
						if (Array.isArray(self.respuesta[0].lista)){
							$.each(self.respuesta[0].lista, function( i, tipo ) {		
								if (!_.isEmpty( tipo )){					
									$("#dashlet1_Asunto").append("<option value='"+tipo.codigo+"'>"+tipo.tipo+"</option>");
								}
							});	
						}
					}
				}	
				if ( _.isEmpty( $("#dashlet1_Asunto").html() ) ){ 
					$("#dashlet1_Asunto").append('<option value="">--</option>');
				}
			}
			else{			
				if ( _.isEmpty( $("#dashlet1_Asunto").html() ) ){ 
					$("#dashlet1_Asunto").append('<option value="">--</option>');
				}		
				app.alert.dismiss('dashlet2_bizagi_procesando');
				app.alert.dismiss('error_dashlet1_bizagi_tipo_de_solicitud');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Error tipoParametrica no valido!',
					autoClose: false,
				});
			}
		}
		else{		
			if ( _.isEmpty( $("#dashlet1_Asunto").html() ) ){ 
				$("#dashlet1_Asunto").append('<option value="">--</option>');
			}
			app.alert.dismiss('dashlet1_bizagi_procesando');
			app.alert.show('error_dashlet1_bizagi_tipo_de_solicitud', {
				level: 'error',
				title: 'tipoParametrica vacio!',
				autoClose: false,
			});
		}
	},
	dashlet1_bizagi_tipo_de_solicitud: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		$("#dashlet1_Tipo_Solicitud").empty();
		self.dashlet_bizagi_tipoParametrica('1','','');
		if (!_.isEmpty( self.respuesta )){ 		
			if(!$.isArray( self.respuesta )){			
				var temp = self.respuesta;
				self.respuesta = [];
				self.respuesta.push(temp);
			}
			console.log('tipo_de_solicitud ',self.respuesta);	
			
			if (Array.isArray(self.respuesta)){
				if (typeof self.respuesta[0] !== 'undefined'){
					if (typeof self.respuesta[0].lista !== 'undefined'){
						if (Array.isArray(self.respuesta[0].lista)){
							$.each(self.respuesta[0].lista, function( i, tipo ) {		
								if (!_.isEmpty( tipo )){					
									$("#dashlet1_Tipo_Solicitud").append("<option value='"+tipo.codigo+"'>"+tipo.tipo+"</option>");
								}
							});	
						}
					}
				}	
				if ( _.isEmpty( $("#dashlet1_Tipo_Solicitud").html() ) ){ 
					$("#dashlet1_Tipo_Solicitud").append('<option value="">--</option>');
				}
			}
			else{			
				if ( _.isEmpty( $("#dashlet1_Tipo_Solicitud").html() ) ){ 
					$("#dashlet1_Tipo_Solicitud").append('<option value="">--</option>');
				}		
				app.alert.dismiss('dashlet2_bizagi_procesando');
				app.alert.dismiss('error_dashlet1_bizagi_tipo_de_solicitud');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Error tipoParametrica no valido!',
					autoClose: false,
				});
			}
		}
		else{		
			if ( _.isEmpty( $("#dashlet1_Tipo_Solicitud").html() ) ){ 
				$("#dashlet1_Tipo_Solicitud").append('<option value="">--</option>');
			}
			app.alert.dismiss('dashlet1_bizagi_procesando');
			app.alert.show('error_dashlet1_bizagi_tipo_de_solicitud', {
				level: 'error',
				title: 'tipoParametrica vacio!',
				autoClose: false,
			});
		}
	},
	dashlet1_nombre_cuenta: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
	
		/*
		Obteniendo cuenta
		*/
		$("#dashlet1_nombre").html('');
		app.api.call(
			'GET', 
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet1_No_Identificacion").val()), 
				{
					"sasa_nroidentificacion_c": $("#dashlet1_No_Identificacion").val(),
				}, 
				{
				success: function (data) {
					if(!_.isEmpty(data.records)){
						$("#dashlet1_nombre").html(data.records[0].name);
					}
					else{
						$("#dashlet1_nombre").html('--<b>No Existe en sugar</b>--');
					}
				},
				error: function (e) {	
					app.alert.dismiss('dashlet1_bizagi_procesando');					
					app.alert.show('error_procesando_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
						autoClose: false,
					});
				}
			}, {async: false}
		);
	},
	dashlet1_crear: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		if (!_.isEmpty( $("#dashlet1_No_Identificacion").val() )){			
			if (!_.isEmpty( $("#dashlet1_nombre").html() )){	
				self.dashlet_bizagi_call();
				if (!_.isEmpty( self.respuesta )){ 		
					console.log('dashlet1_crear ',self.respuesta);	
					if (typeof self.respuesta.bExitosa !== 'undefined'){
						if (self.respuesta.bExitosa){
							if (typeof self.respuesta.processRadNumber !== 'undefined'){
								app.alert.dismiss('dashlet1_bizagi_procesando');
								app.alert.show('info_crear', {
									level: 'info',
									title: 'Creado',
									messages: self.respuesta.processRadNumber+": "+self.respuesta.description,
									autoClose: false,
								});
							}
							else{				
								app.alert.dismiss('dashlet1_bizagi_procesando');
								app.alert.show('error_crear', {
									level: 'error',
									title: 'Error al crear por WS3:<p>',
									messages: self.respuesta.description,
									autoClose: false,
								});
							}
						}
						else{				
							app.alert.dismiss('dashlet1_bizagi_procesando');
							app.alert.show('error_crear', {
								level: 'error',
								title: 'Error al crear por WS2:<p>'+JSON.stringify(self.respuesta,null,'\t'),
								autoClose: false,
							});
						}
					}
					else{				
						app.alert.dismiss('dashlet1_bizagi_procesando');
						app.alert.show('error_crear', {
							level: 'error',
							title: 'Error al crear por WS1:<p>'+JSON.stringify(self.respuesta,null,'\t'),
							autoClose: false,
						});
					}
				}
				else{				
					app.alert.dismiss('dashlet1_bizagi_procesando');
					app.alert.show('error_crear', {
						level: 'error',
						title: 'Error al crear por WS0:<p>'+JSON.stringify(self.respuesta,null,'\t'),
						autoClose: false,
					});
				}
			}
			else{
				app.alert.dismiss('dashlet1_bizagi_procesando');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Nombre vacio!',
					autoClose: false,
				});
			}
		}
		else{
			app.alert.dismiss('dashlet1_bizagi_procesando');
			app.alert.show('error_crear', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},
	dashlet_bizagi_tipoParametrica: function(tipoParametrica,codigoFiltro,Compania){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		var externo ='0';
		var fiduciaria ='0';
		var valores ='0';	
		
		if(Compania == 'EXTERNO'){
			externo = '1';
		}
		else if(Compania == 'FIDUCIARIA'){
			fiduciaria = '1';
		}
		else if(Compania == 'VALORES'){
			valores = '1';
		}	
			
		var method  = "dashlet_bizagi";		
		var data = [
			{
				'url':self.url,
				'ws':self.ws,
				'tipo':'POST',
				'Username':self.Username,
				'Password':self.Password,
			},
			{	
				
				'codigoFiltro':codigoFiltro,
				'externo':externo,
				'fiduciaria':fiduciaria,
				'valores':valores,
				'tipoParametrica':tipoParametrica,
            
				'xmlns:soapenv':'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"',
				'xmlns:web':'xmlns:web="http://web.ws.sqdm.com/"',
				'web':'listarParametrica',
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_bizagi_procesando');
				
				if (typeof response !== 'undefined')
				if (typeof response.descripcion !== 'undefined')
				if (response.descripcion == 'Error de autenticación. Por favor valide sus credenciales')
				app.alert.show('error_dashlet1_autenticacion', {
					level: 'error',
					title: 'Bizagi1: '+response.descripcion,
					autoClose: false,
				});
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_bizagi_procesando');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
	dashlet_bizagi_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		var numeroDocumento = $("#dashlet1_No_Identificacion").val();	
		var method  = "dashlet_bizagi";		
		var data = [
			{
				'url':self.url,
				'ws':self.ws,
				'tipo':'POST',
				'Username':self.Username,
				'Password':self.Password,
			},
			{		
				'categoria':$("#dashlet1_Asunto").val(),	
				'ciudadRadicacion':'1',//BOGOTA
				'description':$("#dashlet1_Descripcion").val(),
				'nombreCliente':$("#dashlet1_nombre").html(),	
				'numeroDocumento':$("#dashlet1_No_Identificacion").val(),
				'receptionMean':'108',//CRM
				'SEmpresaPortal':$("#dashlet1_Compania").val(),
				'solucionInmediata':'',
				'tipificacion1':"",
				'tipificacion2':"",
				'tipificacion3':"",
				'tipodeSolicitud':$("#dashlet1_Tipo_Solicitud").val(),
				
				'xmlns:soapenv':'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"',
				'xmlns:web':'xmlns:web="http://web.ws.sqdm.com/"',
				'web':'crearSolicitud',
			},
		];
		console.log("data: ",data);
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_bizagi_procesando');
				
				if (typeof response !== 'undefined')
				if (typeof response.descripcion !== 'undefined')
				if (response.descripcion == 'Error de autenticación. Por favor valide sus credenciales')
				app.alert.show('error_dashlet1_autenticacion', {
					level: 'error',
					title: 'Bizagi1: '+response.descripcion,
					autoClose: false,
				});
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_bizagi_procesando');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
})
