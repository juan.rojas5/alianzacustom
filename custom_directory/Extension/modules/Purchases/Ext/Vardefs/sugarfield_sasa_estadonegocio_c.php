<?php
 // created: 2023-02-21 13:32:00
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['labelValue']='Estado de negocio';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['readonly_formula']='';

 ?>