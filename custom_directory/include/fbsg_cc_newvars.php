<?php

global $sugar_version;
$is7 = !!(preg_match('/^7/', $sugar_version));

$dictionary[$cc_module]['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'reportable' => true,
    'importable' => false,
    'len' => 255,
    'duplicate_merge' => 'enabled',
);

$dictionary[$cc_module]['fields']['cc_optout'] = array(
	'required' => false,
	'name' => 'cc_optout',
	'vname' => 'Opted Out on Constant Contact',
	'type' => 'bool',
    'massupdate' => false,
	'default' => '0',
    'importable' => false,
    'reportable' => true,
    'duplicate_merge' => 'enabled',
    'readonly' => true,
);

$dictionary[$cc_module]['fields']['cc_synced'] = array(
    'required' => false,
    'name' => 'cc_synced',
    'vname' => 'LBL_CC_SYNCED',
    'type' => 'bool',
    'default' => false,
    'importable' => false,
    'reportable' => true,
    'duplicate_merge' => 'enabled',
    'readonly' => true,
);

$dictionary[$cc_module]['fields']['cc_lists'] = array(
    'required' => false,
    'name' => 'cc_lists',
    'vname' => 'LBL_CCLISTS',
    'type' => 'multienum',
    'isMultiSelect' => true,
    'options' => 'cc_list_dom',
    'reportable' => false,
    'importable' => false,
    'massupdate' => true,
    'default' => NULL,
    'studio' => true,
    'duplicate_merge' => 'enabled',
);

//
/* REMOVE */
// $dictionary[$cc_module]['fields']['cc_sync'] = array(
//     'required' => false,
//     'name' => 'cc_sync',
//     'vname' => 'LBL_CCSYNC',
//     'type' => 'bool',
//     'default' => false,
//     /* REMOVE */
//     // 'function' => array(
//     //     'name' => 'displayCcSync',
//     //     'returns' => 'html',
//     //     'include' => 'modules/fbsg_ConstantContactIntegration/cc_list_view.php',
//     // ),
//     'importable' => false,
//     'reportable' => false,
//     'duplicate_merge' => 'disabled',
// );
//
/* REMOVE */
// if($is7) {
//     $dictionary[$cc_module]['fields']['cc_ui_save'] = array(
//         'name' => 'cc_ui_save',
//         'vname' => 'CC Save from UI',
//         'type' => 'bool',
//         'massupdate' => false,
//         'default' => '0',
//         'importable' => false,
//         'reportable' => true,
//         'studio' => false,
//         'duplicate_merge' => 'disabled',
//         'source' => 'non-db'
//     );
// }
//
// if(!$is7) {
//     $dictionary[$cc_module]['fields']['cc_lists_view'] = array(
//         'name' => 'cc_lists_view',
//         'vname' => 'LBL_CCLISTS',
//         'type' => 'varchar',
//         'function' => array(
//             'name' => 'displayLists',
//             'returns' => 'html',
//             'include' => 'modules/fbsg_ConstantContactIntegration/cc_list_view.php',
//         ),
//         'source' => 'non-db',
//         'studio' => 'visible',
//         'importable' => false,
//         'reportable' => false,
//         'duplicate_merge' => 'disabled',
//     );
// }
