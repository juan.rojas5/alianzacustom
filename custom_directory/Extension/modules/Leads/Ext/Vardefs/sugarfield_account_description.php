<?php
 // created: 2018-06-23 16:01:37
$dictionary['Lead']['fields']['account_description']['audited']=false;
$dictionary['Lead']['fields']['account_description']['massupdate']=false;
$dictionary['Lead']['fields']['account_description']['comments']='Description of lead account';
$dictionary['Lead']['fields']['account_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['account_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['account_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_description']['reportable']=false;
$dictionary['Lead']['fields']['account_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_description']['calculated']=false;
$dictionary['Lead']['fields']['account_description']['rows']='4';
$dictionary['Lead']['fields']['account_description']['cols']='20';
$dictionary['Lead']['fields']['account_description']['dependency']='equal($sasa_tipopersona_c,"Juridica")';

 ?>