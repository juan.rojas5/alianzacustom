<?php
 // created: 2018-06-17 22:22:59
$dictionary['Lead']['fields']['phone_other']['len']='100';
$dictionary['Lead']['fields']['phone_other']['audited']=false;
$dictionary['Lead']['fields']['phone_other']['massupdate']=false;
$dictionary['Lead']['fields']['phone_other']['comments']='Formato: +xxx-x-xxxxxxx';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_other']['reportable']=false;
$dictionary['Lead']['fields']['phone_other']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.99',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_other']['calculated']=false;
$dictionary['Lead']['fields']['phone_other']['pii']=false;
$dictionary['Lead']['fields']['phone_other']['help']='Formato: +xxx-x-xxxxxxx';

 ?>