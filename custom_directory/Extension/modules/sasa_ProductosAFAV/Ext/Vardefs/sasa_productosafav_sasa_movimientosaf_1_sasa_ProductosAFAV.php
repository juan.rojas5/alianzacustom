<?php
// created: 2019-01-04 21:27:42
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAF',
  'bean_name' => 'sasa_MovimientosAF',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
