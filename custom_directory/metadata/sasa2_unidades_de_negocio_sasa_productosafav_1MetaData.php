<?php
// created: 2023-02-06 20:17:53
$dictionary["sasa2_unidades_de_negocio_sasa_productosafav_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa2_unidades_de_negocio_sasa_productosafav_1' => 
    array (
      'lhs_module' => 'sasa2_Unidades_de_negocio',
      'lhs_table' => 'sasa2_unidades_de_negocio',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_ProductosAFAV',
      'rhs_table' => 'sasa_productosafav',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_c',
      'join_key_lhs' => 'sasa2_unid99b0negocio_ida',
      'join_key_rhs' => 'sasa2_unid7f8btosafav_idb',
    ),
  ),
  'table' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa2_unid99b0negocio_ida' => 
    array (
      'name' => 'sasa2_unid99b0negocio_ida',
      'type' => 'id',
    ),
    'sasa2_unid7f8btosafav_idb' => 
    array (
      'name' => 'sasa2_unid7f8btosafav_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa2_unid99b0negocio_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa2_unid7f8btosafav_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa2_unid7f8btosafav_idb',
      ),
    ),
  ),
);