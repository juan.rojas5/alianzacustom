<?php
 // created: 2022-11-18 19:47:58
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['labelValue']='Fecha_de_Vencimiento';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_fecha_de_vencimiento_c']['readonly_formula']='';

 ?>