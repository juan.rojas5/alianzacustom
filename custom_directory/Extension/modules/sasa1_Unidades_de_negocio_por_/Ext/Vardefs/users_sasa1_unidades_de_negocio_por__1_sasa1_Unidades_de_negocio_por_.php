<?php
// created: 2023-02-06 20:16:31
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'users_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'side' => 'right',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link' => 'users_sasa1_unidades_de_negocio_por__1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1users_ida"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link' => 'users_sasa1_unidades_de_negocio_por__1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
