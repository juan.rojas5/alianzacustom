<?php

//if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AlianzaCasesAfterSave
{
        function after_save($bean, $event, $arguments)
        {
                //loop prevention check
                if (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false) {
                        //update
                        $bean->ignore_update_c = true;

                        $bean->save(false);
                }
        }
}
?>