<?php
/*
* Funcion para planificador de tareas
* Caso sasa 5407 Cuentas - Funcionalidad campo "Fecha de Actualización"
* 
* Se requiere, para el módulo de cuentas, monitorear el campo "Fecha de Actualización" y 60 días antes del valor de este campo se debe marcar el cliente potencial.
*
* La idea es validar el campo "Fecha de Actualización" y poblar el campo "Ctrl WF" en el módulo de Cuentas con el valor "Proceso Fecha de Actualización", la funcionalidad se debe ejecutar, solo si el campo "Ctrl WF" está "Vacío"
*
* Un WF monitoreará la actualización de registros del CP con este valor en el campo "Ctrl WF" , para hacer un proceso de generación de tarea y notificación en caso de no gestionarse en 60 días.
*/
$job_strings[] = 'tarea_fecha_actualizacion_cuenta';
function tarea_fecha_actualizacion_cuenta(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_fecha_actualizacion_cuenta. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		require_once("custom/dias_actualizacion_account.php");
		$query= "
		SELECT
			id,
			accounts_cstm.sasa_ctrlwf_c as proceso,
			accounts_cstm.sasa_fechaactualizacion_c as fecha,
			accounts_cstm.sasa_ctrlanoactualizacion_c as periodo
		FROM
			accounts
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
			accounts.deleted = 0 
			AND( accounts_cstm.sasa_ctrlwf_c = '' OR accounts_cstm.sasa_ctrlwf_c IS NULL ) 
			AND accounts_cstm.sasa_fechaactualizacion_c IS NOT NULL 
			AND accounts_cstm.sasa_ctrlanoactualizacion_c != '{$sasa_ctrlanoactualizacion_c}'
			and DATE_SUB(accounts_cstm.sasa_fechaactualizacion_c , INTERVAL {$dias_actualizacion_account} DAY) = CURRENT_DATE
		";
		/*
		SELECT
			id,
			accounts_cstm.sasa_ctrlwf_c as proceso,
			accounts_cstm.sasa_fechaactualizacion_c as fecha,
			accounts_cstm.sasa_ctrlanoactualizacion_c as periodo
		FROM
			accounts
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
			accounts.deleted = 0 
			AND( accounts_cstm.sasa_ctrlwf_c = '' OR accounts_cstm.sasa_ctrlwf_c IS NULL ) 
			AND accounts_cstm.sasa_fechaactualizacion_c IS NOT NULL 
			AND accounts_cstm.sasa_ctrlanoactualizacion_c != '2018'
			and DATE_SUB(accounts_cstm.sasa_fechaactualizacion_c , INTERVAL 60 DAY) = CURRENT_DATE
		*/
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$id = $row['id'];
				$fecha = $row['fecha'];
				$GLOBALS['log']->security("registro id: {$id} fecha: {$fecha}"); 
				
				$bean = BeanFactory::getBean("Accounts", $id, array('disable_row_level_security' => true));
				$bean->sasa_ctrlanoactualizacion_c=$sasa_ctrlanoactualizacion_c;//periodo en el que se disparo el WF
				$bean->sasa_ctrlwf_c="Proceso Fecha Actualizacion";
				$bean->save();
				$bean = null;
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_fecha_actualizacion_cuenta. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}
?>
