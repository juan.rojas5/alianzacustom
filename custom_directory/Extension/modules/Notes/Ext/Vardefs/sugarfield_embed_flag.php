<?php
 // created: 2018-05-30 15:01:51
$dictionary['Note']['fields']['embed_flag']['default']=false;
$dictionary['Note']['fields']['embed_flag']['audited']=false;
$dictionary['Note']['fields']['embed_flag']['massupdate']=false;
$dictionary['Note']['fields']['embed_flag']['comments']='Embed flag indicator determines if note embedded in email';
$dictionary['Note']['fields']['embed_flag']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['embed_flag']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['embed_flag']['merge_filter']='disabled';
$dictionary['Note']['fields']['embed_flag']['reportable']=false;
$dictionary['Note']['fields']['embed_flag']['unified_search']=false;
$dictionary['Note']['fields']['embed_flag']['calculated']=false;

 ?>