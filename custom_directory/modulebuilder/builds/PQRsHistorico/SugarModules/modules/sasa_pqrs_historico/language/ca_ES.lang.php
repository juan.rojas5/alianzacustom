<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'ID de l&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'ID d&#39;usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat per',
  'LBL_MODIFIED_ID' => 'Modificat per ID',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat per ID',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Suprimit',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Edita',
  'LBL_REMOVE' => 'Suprimir',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Creat pel nom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'PQRs Histórico Llista',
  'LBL_MODULE_NAME' => 'PQRs Histórico',
  'LBL_MODULE_TITLE' => 'PQRs Histórico',
  'LBL_MODULE_NAME_SINGULAR' => 'PQRs Histórico',
  'LBL_HOMEPAGE_TITLE' => 'Meu PQRs Histórico',
  'LNK_NEW_RECORD' => 'Crea PQRs Histórico',
  'LNK_LIST' => 'Vista PQRs Histórico',
  'LNK_IMPORT_SASA_PQRS_HISTORICO' => 'Importar PQRs Histórico',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca PQRs Histórico',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_SASA_PQRS_HISTORICO_SUBPANEL_TITLE' => 'PQRs Histórico',
  'LBL_NEW_FORM_TITLE' => 'Nou PQRs Histórico',
  'LNK_IMPORT_VCARD' => 'Importar PQRs Histórico vCard',
  'LBL_IMPORT' => 'Importar PQRs Histórico',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new PQRs Histórico record by importing a vCard from your file system.',
  'LBL_SASA_PQRS_HISTORICO_FOCUS_DRAWER_DASHBOARD' => 'PQRs Histórico Panel de enfoque',
  'LBL_SASA_PQRS_HISTORICO_RECORD_DASHBOARD' => 'PQRs Histórico Cuadro de mando del registro',
);