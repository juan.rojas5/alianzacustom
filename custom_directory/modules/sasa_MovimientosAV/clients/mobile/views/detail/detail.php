<?php
$module_name = 'sasa_MovimientosAV';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'detail' => 
      array (
        'templateMeta' => 
        array (
          'form' => 
          array (
            'buttons' => 
            array (
              0 => 'EDIT',
              1 => 'DUPLICATE',
              2 => 'DELETE',
            ),
          ),
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'accounts_sasa_movimientosav_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_ACCOUNTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosav_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_nemotecnico_c',
                'label' => 'LBL_SASA_NEMOTECNICO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_emisor_c',
                'label' => 'LBL_SASA_EMISOR_C',
              ),
              6 => 
              array (
                'name' => 'sasa_fechaliquid_c',
                'label' => 'LBL_SASA_FECHALIQUID_C',
              ),
              7 => 
              array (
                'name' => 'sasa_fechacumplimiento_c',
                'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
              ),
              8 => 
              array (
                'name' => 'sasa_afavor_c',
                'label' => 'LBL_SASA_AFAVOR_C',
              ),
              9 => 
              array (
                'name' => 'sasa_encontra_c',
                'label' => 'LBL_SASA_ENCONTRA_C',
              ),
              10 => 
              array (
                'name' => 'sasa_saldo_c',
                'label' => 'LBL_SASA_SALDO_C',
              ),
              11 => 
              array (
                'name' => 'sasa_comision_c',
                'label' => 'LBL_SASA_COMISION_C',
              ),
              12 => 
              array (
                'name' => 'sasa_retencion_c',
                'label' => 'LBL_SASA_RETENCION_C',
              ),
              13 => 
              array (
                'name' => 'sasa_iva_c',
                'label' => 'LBL_SASA_IVA_C',
              ),
              14 => 
              array (
                'name' => 'sasa_tirprecio_c',
                'label' => 'LBL_SASA_TIRPRECIO_C',
              ),
              15 => 
              array (
                'name' => 'sasa_valor_c',
                'label' => 'LBL_SASA_VALOR_C',
              ),
              16 => 
              array (
                'name' => 'sasa_tipooperacion_c',
                'label' => 'LBL_SASA_TIPOOPERACION_C',
              ),
              17 => 
              array (
                'name' => 'sasa_nominal_c',
                'label' => 'LBL_SASA_NOMINAL_C',
              ),
              18 => 
              array (
                'name' => 'sasa_detalle_c',
                'label' => 'LBL_SASA_DETALLE_C',
              ),
              19 => 
              array (
                'name' => 'sasa_documento_c',
                'label' => 'LBL_SASA_DOCUMENTO_C',
              ),
              20 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              21 => 
              array (
                'name' => 'tag',
                'studio' => 
                array (
                  'portal' => false,
                  'base' => 
                  array (
                    'popuplist' => false,
                  ),
                  'mobile' => 
                  array (
                    'wirelesseditview' => true,
                    'wirelessdetailview' => true,
                  ),
                ),
                'label' => 'LBL_TAGS',
              ),
              22 => 'assigned_user_name',
              23 => 'team_name',
              24 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              25 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              26 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              27 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
