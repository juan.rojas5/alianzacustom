<?php
$module_name = 'sasaP_PresupuestoxProducto';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
                'enabled' => true,
                'id' => 'SASA_PRODUFF64TOSAFAV_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_presupuestoproducto_c',
                'label' => 'LBL_SASA_PRESUPUESTOPRODUCTO_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name',
                'label' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
                'enabled' => true,
                'id' => 'SASAP_PRES107BXASESOR_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_saldoconsolidado_c',
                'label' => 'LBL_SASA_SALDOCONSOLIDADO_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_porcentajecumplimiento_c',
                'label' => 'LBL_SASA_PORCENTAJECUMPLIMIENTO_C',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_deficit_c',
                'label' => 'LBL_SASA_DEFICIT_C',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
