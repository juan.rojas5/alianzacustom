<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

use function PHPSTORM_META\type;

class CalcularFechaVencimientoCasosM2_class
{
    function CalcularFechaVencimientoCasosM2_method($bean, $event, $arguments)
    {
        // ignore para evitar loop despues de usar el método save()
        $ignore = $bean->ignore_CalcularFechaVencimientoCasosM2;

        if (!isset($ignore) || $ignore === false){

            $ignore = true;
            $bean->ignore_CalcularFechaVencimientoCasosM2 = $ignore;

            $GLOBALS['log']->security("\n\n");
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Inicio acción before_save en CalcularFechaVencimientoCasosM2 Tasks -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
            
            // $GLOBALS['log']->security("Task id: ".$bean->id);
            // $GLOBALS['log']->security("Task parent_id: ".$bean->parent_id);
            // $GLOBALS['log']->security("Task tipo proceso: ".$bean->sasa_tipoproceso_c);
            // $GLOBALS['log']->security("Task estado: ".$bean->status);

            $caso = $this->consultarCasoM2($bean->parent_id);

            while ( $row = $GLOBALS['db']->fetchByAssoc($caso) ) 
            {
                // $GLOBALS['log']->security("Id caso: ".$row['id']);
                // $GLOBALS['log']->security("Fuente caso: ".$row['source']);
                // $GLOBALS['log']->security("Fecha Ven caso: ".$row['resolved_datetime']);

                if ( $row['source'] != 'SFC' && $bean->status == 'Completed' && $bean->sasa_tipoproceso_c == 'Cliente' && $bean->fetched_row['status'] != $bean->status) 
                {
                    // $GLOBALS['log']->security("Vas bien para poner fecha vencimiento momento 2");
                    // $GLOBALS['log']->security("Fecha actualización o creación de tarea: ".$bean->date_modified);

                    $dateEndCaseM1 = intval(date('YmdHis', strtotime($row['resolved_datetime'])));
                    $dateEnteredTask = intval(date('YmdHis', strtotime($bean->date_entered)));
                    
                    if ( $dateEndCaseM1 > $dateEnteredTask ) 
                    {
                        $GLOBALS['log']->security("Fecha ven de caso es mayor a fecha de creación de tarea");

                        $dif = $this->calcularDifM2();
                        $baseHour = date("H:i:s", strtotime("+$dif hour", strtotime("12:00:00")));
                        $hourDateEntered = date('H:i:s', strtotime($dateEnteredTask));

                        if ( $baseHour > $hourDateEntered ) 
                        {
                            // Cuando la tarea se crea antes de la hora base, ese día no se toma como día de gestión
                            $dateCase = new DateTime(date('YmdHis', strtotime("+1 day", strtotime($row['resolved_datetime'])))); // fecha vencimiento del caso
                            $dateCaseOption2 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($row['resolved_datetime']))); // fecha vencimiento del caso
                            $dateTask = new DateTime($bean->date_entered); // fecha creación de la tarea
                            $diffDates = $dateCase->diff($dateTask); // diferencia entre fecha creación tarea y fecha vencimiento del caso

                            // calcula la nueva fecha de vencimiento del caso
                            $newDateEndCase = date('Y-m-d H:i:s', strtotime("+{$diffDates->days} day", strtotime($dateCaseOption2)));
                            $this->actualizarFechacasoM2($row['id'], $newDateEndCase);

                            $GLOBALS['log']->security("Diferencia entre fechas 1: ".$diffDates->days);
                            $GLOBALS['log']->security("Nueva fecha de vencimiento de caso: ".$newDateEndCase);
                        }

                        if ( $baseHour <= $hourDateEntered ) 
                        {
                            // Cuando la tarea se crea despues de la hora base, ese día se toma como día de gestión
                            
                            $dateCase = new DateTime($row['resolved_datetime']); // fecha de vencimiento del caso FVC
                            $dateTask = new DateTime($bean->date_entered); // fecha de creación de la tarea FCT
                            $diffDates = $dateCase->diff($dateTask); // diferencia entre FVC y FCT

                            // calcula la nueva fecha de vencimiento del caso
                            $newDateEndCase = date('Y-m-d H:i:s', strtotime("+{$diffDates->days} day", strtotime($row['resolved_datetime'])));
                            $this->actualizarFechacasoM2($row['id'], $newDateEndCase);
                            
                            $GLOBALS['log']->security("Diferencia entre fechas 2: ".$diffDates->days);
                            $GLOBALS['log']->security("Nueva fecha de vencimiento de caso: ".$newDateEndCase);
                        }
                    } else {
                        $GLOBALS['log']->security("Fecha creación de tarea es mayor a la fecha de vencimiento del caso");
                    }
                }else {
                    $GLOBALS['log']->security("");
                }
            }

            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Termino acción before_save en CalcularFechaVencimientoCasosM2 Tasks -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
        }
    }

    function consultarCasoM2($idCaso)
    {
        if ( !empty($idCaso) ) 
        {
            $query = "SELECT c.id, c.source, c.resolved_datetime
                FROM cases AS c
                WHERE c.id = '{$idCaso}'
            AND c.deleted = 0";
    
            $result = $GLOBALS['db']->query($query);
            
            return $result;
        }else {
            $GLOBALS['log']->security("No tiene caso relacionado");
            return '';
        }
    }

    function calcularDifM2()
    {
        date_default_timezone_set('UTC'); // configurando por defecto zona horaria Central
        $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

        $timeZoneBogota = 'America/Bogota';
        $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

        $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
        $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

        if ( $dayCentral > $dayColombia ) 
        {
            // $GLOBALS['log']->security("Día de fecha central es mayor");
            $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
            $hourColombia = intval($dateColombia->format('h'));
            $dif = $hourCentral - $hourColombia;
            // $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        }
        else if( $dayCentral == $dayColombia ) 
        {
            // $GLOBALS['log']->security("Días iguales");
            $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
            $hourColombia = intval($dateColombia->format('H:i:s'));
            $dif = $hourCentral - $hourColombia;
            // $GLOBALS['log']->security("Hora Colombia: ".$hourColombia);
            // $GLOBALS['log']->security("Hora central: ".$hourCentral);
            // $GLOBALS['log']->security("Fecha Colombia: ".$dateColombia->format('Y-m-d H:i:s'));
            // $GLOBALS['log']->security("Fecha central: ".$dateCentral);
            // $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        } 
        else 
        {
            $GLOBALS['log']->security("Algo paso");
            $dif = 0;
            $GLOBALS['log']->security("Diferencia horaria: 0");
            return $dif;
        }
    }

    function actualizarFechacasoM2($idCaso, $fecha)
    {
        $query = "UPDATE cases 
        SET resolved_datetime = '{$fecha}'
        WHERE id = '{$idCaso}'";

        $result = $GLOBALS['db']->query($query);

        return $result;
    }
}