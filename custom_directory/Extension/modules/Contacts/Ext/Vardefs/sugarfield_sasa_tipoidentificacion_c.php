<?php
 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['labelValue']='Tipo de ID';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['required_formula']='equal($sasa_pqrs_c,"Si")';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['readonly_formula']='';
$dictionary['Contact']['fields']['sasa_tipoidentificacion_c']['visibility_grid']='';

 ?>