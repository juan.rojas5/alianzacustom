<?php
$dependencies['Leads']['SetOptionPais'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_pais_c,"Colombia")',
   'triggerFields' => array('sasa_pais_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_departamento_c',
           'keys' => 'createList("","Amazonas","Antioquia","Arauca","Archipielago de San Andres Providencia y Santa Catalina","Atlantico","Bogota D.C.","Bolivar","Boyaca","Caldas","Caqueta","Casanare","Cauca","Cesar","Choco","Cundinamarca","Cordoba","Guainia","Guaviare","Huila","La Guajira","Magdalena","Meta","Narino","Norte de Santander","Putumayo","Quindio","Risaralda","Santander","Sucre","Tolima","Valle del Cauca","Vaupes","Vichada")',
           'labels' => 'createList("","Amazonas","Antioquia","Arauca","Archipiélago de San Andrés, Providencia y Santa Catalina","Atlántico","Bogotá D.C.","Bolívar","Boyacá","Caldas","Caquetá","Casanare","Cauca","Cesar","Chocó","Cundinamarca","Córdoba","Guainía","Guaviare","Huila","La Guajira","Magdalena","Meta","Nariño","Norte de Santander","Putumayo","Quindío","Risaralda","Santander","Sucre","Tolima","Valle del Cauca","Vaupés","Vichada")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_departamento_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
