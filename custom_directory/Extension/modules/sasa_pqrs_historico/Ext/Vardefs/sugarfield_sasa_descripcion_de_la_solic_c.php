<?php
 // created: 2022-11-18 20:51:42
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['labelValue']='Descripción de la Solicitud';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_descripcion_de_la_solic_c']['readonly_formula']='';

 ?>