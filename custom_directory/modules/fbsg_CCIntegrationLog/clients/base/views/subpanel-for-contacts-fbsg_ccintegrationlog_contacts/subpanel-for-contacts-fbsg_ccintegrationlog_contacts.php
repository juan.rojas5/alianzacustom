<?php
// created: 2015-10-07 11:05:26
$viewdefs['fbsg_CCIntegrationLog']['base']['view']['subpanel-for-contacts-fbsg_ccintegrationlog_contacts'] = array(
  'type' => 'subpanel-list',
  'panels' =>
  array(
    0 =>
    array(
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array(
        0 =>
        array(
          'name' => 'message',
          'label' => 'LBL_MESSAGE',
          'enabled' => true,
          'default' => true,
        ),
        1 =>
        array(
          'name' => 'action',
          'label' => 'LBL_ACTION',
          'enabled' => true,
          'default' => true,
        ),
        2 =>
        array(
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);
