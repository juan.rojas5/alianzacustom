<?php
// created: 2019-01-04 21:27:42
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE_ID',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
