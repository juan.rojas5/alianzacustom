<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class convercion_lead_manual {
	function after_save($bean, $event, $arguments) {
		try {

			//Hacer que se elimine el equipo privado en el registro y solo quede en el equipo cargos superiores caso 6179
			global $db;

			//loop prevention check
			if ($bean->status == 'Converted') {
				if (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false) {
					$bean->ignore_update_c = true;

					$query = "UPDATE leads SET leads.team_id='16b39cc2-5f92-11e8-b0f8-0699afa2cd79', leads.team_set_id='16b39cc2-5f92-11e8-b0f8-0699afa2cd79' WHERE leads.id = '{$bean->id}';";
					//$bean->description .= $query;
					$result = $db->query($query);
				}
			}

			if ($bean->status == "In Process" and $bean->converted == false and empty($bean->account_id) and empty($bean->contact_id)) { //Para que solo convierta una vez y solo si no tiene cuenta/contacto
				/*
				 * Creando objetos para convertir
				*/
				$Account = new Account();
				$Contact = new Contact();

				/*
				 * Mapeo campos dinamico!!!
				*/
				foreach ($bean->field_defs as $campo => $definicion) {
					if ($campo != 'id' and $campo != 'date_entered' and $campo != 'date_modified') { //no incluir descripcion, caso sasa 	5404,........ incluir descripcion caso sasa 5506
						/*
						 * Del contacto
						*/
						$Contact->$campo = $bean->$campo;

						/*
						 * De la cuenta
						*/
						$Account->$campo = $bean->$campo;
					}
				}

				/*
				 * Mapeo propio de cuentas
				*/
				$Account->billing_address_street = $bean->primary_address_street;
				$Account->billing_address_city = $bean->primary_address_city;
				$Account->billing_address_state = $bean->primary_address_state;
				$Account->billing_address_postalcode = $bean->primary_address_postalcode;
				$Account->billing_address_country = $bean->primary_address_country;
				$Account->name = (empty(trim($bean->account_name))) ? $bean->first_name . " " . $bean->last_name : $bean->account_name;
				$Account->account_type = "Prospect"; //requerimiento de negocio				
				
				$Account->save(); //Se debe pasar un Objeto con id
				$Contact->save(); //Se debe pasar un Objeto con id
				/*
				 * Parametros convercion
				*/
				$transferActivitiesAction = "copy"; //Pasar actividades de Lead a Contacto, no incluye casos...
				$transferActivitiesModules = array(
					"Accounts",
					"Contacts"
				); //Dado que ya no soporta copiar actividades, la opcion de mover solo funciona para contactos, esto sobra
				$transferActivitiesAction = "";
				$transferActivitiesModules = array(
					"Accounts"
				); //desactivar pasar registros automaivamente del lead
				$modules = array(
					"Contacts" => $Contact,
					"Accounts" => $Account,
				);

				/*
				 * Convirtiendo
				*/
				$leadConvert = new LeadConvert($bean->id);
				$modules = $leadConvert->convertLead($modules, $transferActivitiesAction, $transferActivitiesModules);

				/*
				 * Moviendo actividades a Cuentas, tomado de leadConvert->performLeadActivitiesTransfer($transferActivitiesAction, $transferActivitiesModules)
				*/
				$lead = $leadConvert->getLead();
				Activity::disable();
				$leadConvert->moveActivities($lead, $Account);
				Activity::restoreToPreviousState();

				/*
				 * Relacion cuanta-contacto, forzar debido un problema introducido con unas dependencias de campos , despues de convertir desde WF
				*/
				$Account->load_relationship("contacts"); //Ya se debe tener id de cuenta y contacto
				$Account
					->contacts
					->add($Contact);

				$GLOBALS['log']->security("Convertido lead: {$bean->id} Account:{$modules["Accounts"]->id} Contact:{$modules["Contacts"]->id}");
			}

		}
		catch(Exception $e) {
			$GLOBALS['log']->security("ERROR: error al convertir lead - " . $e->getMessage());
		}
	}
}
?>