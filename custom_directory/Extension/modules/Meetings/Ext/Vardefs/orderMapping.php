<?php
// created: 2023-01-26 21:28:48
$extensionOrderMap = array (
  'custom/Extension/modules/Meetings/Ext/Vardefs/audit.php' => 
  array (
    'md5' => 'a7fe146886f15c32013a49e6197c9b54',
    'mtime' => 1530913358,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => '1dd1f477a71f7900f15fc9af7d389580',
    'mtime' => 1530913751,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_status.php' => 
  array (
    'md5' => '11b98822f4be8053872b45cfefd78caf',
    'mtime' => 1530913821,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_start.php' => 
  array (
    'md5' => '884008929bb7f057ccd40da8b4d3a763',
    'mtime' => 1530913843,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_location.php' => 
  array (
    'md5' => '5066d67fd81773bba1b4cd1637eed527',
    'mtime' => 1530913972,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_email_reminder_time.php' => 
  array (
    'md5' => '606c69ca248a6df0b6ffa61147e13824',
    'mtime' => 1530914008,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_entered.php' => 
  array (
    'md5' => 'c86ff577d4474d0182aa563d1451d693',
    'mtime' => 1530914038,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_modified.php' => 
  array (
    'md5' => '62155a8db436cc8ab0f8400be016d149',
    'mtime' => 1530914060,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_referidos_c.php' => 
  array (
    'md5' => '936bb8528be4cf27abc4fdf39129460f',
    'mtime' => 1530914140,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_responsableejecucion_c.php' => 
  array (
    'md5' => 'ad6983ad10be92cfbc9dc719083ea199',
    'mtime' => 1531261298,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_vencida_c.php' => 
  array (
    'md5' => 'dd0c4cca296c16f18ebce61ec42507d0',
    'mtime' => 1532206967,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_superior1_c.php' => 
  array (
    'md5' => '1878de69ac9bab3f3e4ff986dd966fef',
    'mtime' => 1532573222,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_superior2_c.php' => 
  array (
    'md5' => '4323af06112112a9b034e0949b7229b0',
    'mtime' => 1532573268,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/rli_link_workflow.php' => 
  array (
    'md5' => '4e34008d380f9451d708b6e0ed37e025',
    'mtime' => 1533751914,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_end.php' => 
  array (
    'md5' => '2da5cebcb4bfba1e8482a289569eb534',
    'mtime' => 1536617471,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_name.php' => 
  array (
    'md5' => 'b248b1f15253a7e4bf4116db22f27fc8',
    'mtime' => 1536702672,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_type.php' => 
  array (
    'md5' => 'e8e1ddd0677208cc11f2d8a20a11c5fd',
    'mtime' => 1536702672,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_id.php' => 
  array (
    'md5' => '791b54589d3d1f9ce4a75ac8b3636c24',
    'mtime' => 1536702672,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_envio_compro_c.php' => 
  array (
    'md5' => '3c0a5ea310553b0bab74db9f1139a0ba',
    'mtime' => 1572048778,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_visitacompaniade_c.php' => 
  array (
    'md5' => '1c5c9a110ba2a7d941443c7646603c33',
    'mtime' => 1588806642,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_fechaproximcompromiso_c.php' => 
  array (
    'md5' => '8a509e4dd0e7c1d4e3cfbba47f6d7ac2',
    'mtime' => 1588806642,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/full_text_search_admin.php' => 
  array (
    'md5' => 'dc282e99977442fcfa6e8b6944394c55',
    'mtime' => 1636002233,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_resultadovisita_c.php' => 
  array (
    'md5' => '927ee8dafdd67d37af584bfefba808b8',
    'mtime' => 1663280037,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_productosofrecidos_c.php' => 
  array (
    'md5' => '9bdf311f6d0d81b8f8cab33c17e0a4c4',
    'mtime' => 1663280128,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_objetivovisita_c.php' => 
  array (
    'md5' => 'dc86047178057822fcbf271357edd9bf',
    'mtime' => 1663280232,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_presencial_c.php' => 
  array (
    'md5' => '6af4908ac15d3cf0593eeed7dffe47a5',
    'mtime' => 1663280360,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_compromiso_c.php' => 
  array (
    'md5' => '92b5d87622d65cf2f3d33e08e3b44e01',
    'mtime' => 1663281061,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_description.php' => 
  array (
    'md5' => '68616f91b61ca98652313f7851ab5c16',
    'mtime' => 1663281087,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_enviarcapacitacion_c.php' => 
  array (
    'md5' => '009d705181dc8b8f50d5beaa80d9a81f',
    'mtime' => 1664220276,
    'is_override' => false,
  ),
  'custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_programarvisita_c.php' => 
  array (
    'md5' => '30de3487528f3d520fc5a0ebd55072c9',
    'mtime' => 1674768525,
    'is_override' => false,
  ),
);