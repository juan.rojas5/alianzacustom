<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_TAGS_LINK' => 'Tag',
  'LBL_TAGS' => 'Tag',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Proprietario del documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Eliminato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato dal Nome',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Creato da Nome',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'PQRs Histórico Lista',
  'LBL_MODULE_NAME' => 'PQRs Histórico',
  'LBL_MODULE_TITLE' => 'PQRs Histórico',
  'LBL_MODULE_NAME_SINGULAR' => 'PQRs Histórico',
  'LBL_HOMEPAGE_TITLE' => 'Mio PQRs Histórico',
  'LNK_NEW_RECORD' => 'Crea PQRs Histórico',
  'LNK_LIST' => 'Visualizza PQRs Histórico',
  'LNK_IMPORT_SASA_PQRS_HISTORICO' => 'Importar PQRs Histórico',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca PQRs Histórico',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_SASA_PQRS_HISTORICO_SUBPANEL_TITLE' => 'PQRs Histórico',
  'LBL_NEW_FORM_TITLE' => 'Nuovo PQRs Histórico',
  'LNK_IMPORT_VCARD' => 'Importar PQRs Histórico vCard',
  'LBL_IMPORT' => 'Importar PQRs Histórico',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new PQRs Histórico record by importing a vCard from your file system.',
  'LBL_SASA_PQRS_HISTORICO_FOCUS_DRAWER_DASHBOARD' => 'PQRs Histórico Panel de enfoque',
  'LBL_SASA_PQRS_HISTORICO_RECORD_DASHBOARD' => 'PQRs Histórico Cuadro de mando del registro',
);