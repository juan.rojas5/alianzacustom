<?php
 // created: 2021-06-01 16:43:02

$app_list_strings['sasa_segmentocuentas_c_list']=array (
  '' => '',
  'Preferenciales' => 'Preferenciales',
  'Renta Alta' => 'Renta Alta',
  'Banca Privada' => 'Banca Privada',
  'PYME' => 'PYME',
  'Empresarial' => 'Empresarial',
  'Banca Corporativa' => 'Banca Corporativa',
  'Institucional' => 'Institucional',
  'No Registra' => 'No Registra',
  'Retail' => 'Retail',
);