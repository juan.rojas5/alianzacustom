<?php
// created: 2019-01-04 21:55:18
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_productosafav_sasa_movimientosavdivisas_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODU845ETOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_produ845etosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'sasa_moneda_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_MONEDA_C',
    'width' => 10,
  ),
  'sasa_tipo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_TIPO_C',
    'width' => 10,
  ),
  'sasa_fecha_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHA_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_tasadivisadolar_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TASADIVISADOLAR_C',
    'width' => 10,
  ),
  'sasa_tasapesodolar_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TASAPESODOLAR_C',
    'width' => 10,
  ),
  'sasa_cantidad_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_CANTIDAD_C',
    'width' => 10,
  ),
  'sasa_valorbruto_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORBRUTO_C',
    'width' => 10,
  ),
  'sasa_ocobros_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_OCOBROS_C',
    'width' => 10,
  ),
  'sasa_retefte_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_RETEFTE_C',
    'width' => 10,
  ),
  'sasa_iva_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_IVA_C',
    'width' => 10,
  ),
  'sasa_reteiva_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_RETEIVA_C',
    'width' => 10,
  ),
  'sasa_valorneto_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORNETO_C',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);