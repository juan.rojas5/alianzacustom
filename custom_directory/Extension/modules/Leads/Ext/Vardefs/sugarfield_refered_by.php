<?php
 // created: 2018-06-23 16:28:28
$dictionary['Lead']['fields']['refered_by']['audited']=false;
$dictionary['Lead']['fields']['refered_by']['massupdate']=false;
$dictionary['Lead']['fields']['refered_by']['comments']='Identifies who refered the lead';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['refered_by']['merge_filter']='disabled';
$dictionary['Lead']['fields']['refered_by']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['refered_by']['calculated']=false;
$dictionary['Lead']['fields']['refered_by']['dependency']='equal($lead_source,"Word of mouth")';

 ?>