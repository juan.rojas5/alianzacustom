<?php
 // created: 2023-04-10 20:40:22

$app_list_strings['status_list']=array (
  '' => '',
  1 => 'Recibida',
  2 => 'Abierta',
  3 => 'Abierta en conciliación',
  4 => 'Cerrada',
  10 => 'Crear Queja SFC',
  5 => 'Reabierto',
  6 => 'Escalada',
  7 => 'Vencida',
  8 => 'Nuevo',
);