<?php
 // created: 2018-05-30 15:01:31
$dictionary['Note']['fields']['portal_flag']['default']=false;
$dictionary['Note']['fields']['portal_flag']['audited']=false;
$dictionary['Note']['fields']['portal_flag']['massupdate']=false;
$dictionary['Note']['fields']['portal_flag']['comments']='Portal flag indicator determines if note created via portal';
$dictionary['Note']['fields']['portal_flag']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['portal_flag']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['portal_flag']['merge_filter']='disabled';
$dictionary['Note']['fields']['portal_flag']['reportable']=false;
$dictionary['Note']['fields']['portal_flag']['unified_search']=false;
$dictionary['Note']['fields']['portal_flag']['calculated']=false;

 ?>