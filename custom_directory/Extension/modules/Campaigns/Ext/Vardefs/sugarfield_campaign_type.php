<?php
 // created: 2018-06-25 14:02:52
$dictionary['Campaign']['fields']['campaign_type']['massupdate']=true;
$dictionary['Campaign']['fields']['campaign_type']['comments']='The type of campaign';
$dictionary['Campaign']['fields']['campaign_type']['duplicate_merge']='enabled';
$dictionary['Campaign']['fields']['campaign_type']['duplicate_merge_dom_value']='1';
$dictionary['Campaign']['fields']['campaign_type']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['campaign_type']['unified_search']=false;
$dictionary['Campaign']['fields']['campaign_type']['full_text_search']=array (
);
$dictionary['Campaign']['fields']['campaign_type']['calculated']=false;
$dictionary['Campaign']['fields']['campaign_type']['dependency']=false;

 ?>