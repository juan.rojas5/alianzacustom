<?php 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';
    $mod_strings['LBL_SASA_P1RANGOEDADCALIDAD_C'] = '1. ¿En qué rango  de edad o calidad se encuentra?';
    $mod_strings['LBL_SASA_P2PROPOSITOAHORROINVE_C'] = '2. ¿Cuál es el propósito de su ahorro o inversión?';
    $mod_strings['LBL_SASA_P3HORIZONTETIEMPOINVE_C'] = '3. ¿Cuál es el horizonte de tiempo en el que mantendrá su inversión?';
    $mod_strings['LBL_SASA_P4DONDEPROVIENEDINERO_C'] = '4. ¿De dónde proviene el dinero que desea invertir?';
    $mod_strings['LBL_SASA_P5PORCENTAJEACTIVOSAH_C'] = '5. ¿Qué porcentaje del total de sus activos representa este ahorro o inversión?';
    $mod_strings['LBL_SASA_P6EXPERIENCIAINVERSIO_C'] = '6. ¿Ha tenido experiencia en el tema de inversiones?';
    $mod_strings['LBL_SASA_P7OPORTUNIDADRENTABIL_C'] = '7. Si tuviera la oportunidad de aumentar su rentabilidad asumiendo mayor riesgo, e incluyendo pérdidas potenciales a su inversión inicial ¿qué haría usted?';
    $mod_strings['LBL_SASA_P8SUPONGAINVERSIONESP_C'] = '8. Suponga que sus inversiones pierden un 10% este mes, ¿qué haría usted?';
    $mod_strings['LBL_SASA_CALCULOPERFILRIESGO_C'] = 'Calculo Perfil Riesgo';
