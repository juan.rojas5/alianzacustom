<?php
// created: 2018-07-19 23:16:12
$dictionary['fbsg_cc_map'] = array (
  'relationships' => 
  array (
  ),
  'table' => 'fbsg_cc_map',
  'fields' => 
  array (
    'map_direction' => 
    array (
      'name' => 'map_direction',
      'type' => 'varchar',
      'len' => 10,
    ),
    'sugar_module' => 
    array (
      'name' => 'sugar_module',
      'type' => 'varchar',
      'len' => 100,
    ),
    'from_field' => 
    array (
      'name' => 'from_field',
      'type' => 'varchar',
      'len' => 100,
      'isnull' => false,
    ),
    'to_field' => 
    array (
      'name' => 'to_field',
      'type' => 'varchar',
      'len' => 100,
    ),
    'cc_type' => 
    array (
      'name' => 'cc_type',
      'type' => 'varchar',
      'len' => 10,
    ),
    'type' => 
    array (
      'name' => 'type',
      'type' => 'varchar',
      'len' => 30,
    ),
    'from_master' => 
    array (
      'name' => 'from_master',
      'type' => 'int',
    ),
    'field_index' => 
    array (
      'name' => 'field_index',
      'type' => 'int',
    ),
  ),
  'indices' => 
  array (
  ),
);