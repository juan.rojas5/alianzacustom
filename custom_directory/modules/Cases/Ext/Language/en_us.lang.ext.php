<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0012022-04-07.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.customsasa_sasa_tipificaciones_cases_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE'] = 'Tipificaciones';
$mod_strings['LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE'] = 'Tipificaciones';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcaseslenguajev001-2022-06-13_10-29.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono Fijo';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Empresa';
$mod_strings['LBL_DESCRIPTION'] = 'Notas';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección Alternativa';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento';
$mod_strings['LBL_LEAD_SOURCE_DESCRIPTION'] = 'Descripción de Toma de Contacto:';
$mod_strings['LBL_REFERED_BY'] = 'Referido Por:';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo Queja';
$mod_strings['LNK_CREATE'] = 'Nuevo Queja';
$mod_strings['LBL_MODULE_NAME'] = 'Quejas';
$mod_strings['LBL_CASE'] = 'Queja:';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0022022-06-17.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0032022-06-28.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0042022-07-01.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcaseslenguajev001-2022-07-01_01-43.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['LNK_CREATE'] = 'Nuevo PQRs';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_CASE'] = 'PQRs:';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0062022-07-15.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcaseslenguajev002-2022-07-15_09-16.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['LNK_CREATE'] = 'Nuevo PQRs';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_CASE'] = 'PQRs:';
$mod_strings['LBL_SASA_DIRECCIONSFC_C'] = 'Dirección';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0072022-07-22.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0082022-09-20.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'PQRs';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo PQRs';
$mod_strings['LNK_CASE_LIST'] = 'Ver PQRs';
$mod_strings['LNK_IMPORT_CASES'] = 'Importar PQRs';
$mod_strings['LBL_CONTACT_CASE_TITLE'] = 'Contacto-PQRs:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'PQRs';
$mod_strings['LBL_MODULE_TITLE'] = 'PQRs: Inicio';
$mod_strings['LBL_SHOW_MORE'] = 'Mostrar más PQRs';
$mod_strings['LNK_CREATE_WHEN_EMPTY'] = 'Crear un PQRs ahora';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_INTRO'] = 'El módulo de PQRs es para la gestión de problemas de compatibilidad que afecta a las Cuentas. Utilice las flechas más abajo para una visita rápida.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_PAGE'] = 'Esta página muestra el listado de PQRs existentes asociados a su Cuenta.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_FILTER'] = 'Puede filtrar el listado de PQRs indicando un término de búsqueda.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_CREATE'] = 'Si tiene un nuevo PQRs de asistencia que le gustaría enviar, puede hacer clic aquí para enviarlo.';
$mod_strings['LBL_SASA_CAMPOPRUEBA_C'] = 'campo de prueba';
$mod_strings['LBL_SASA_TIPODESOLICITUD_C'] = 'Tipo de Solicitud';
$mod_strings['LBL_SASA_NUMERODEID_C'] = 'Número de ID';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Nuevo Panel 5';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_TIPODEIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_TIPOID_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_CORREOELECTRONICO_C'] = 'Correo electrónico';
$mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_SASA_FECHACIERRE_C'] = 'Fecha de Cierre';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_SASA_COMERCIAL_C'] = 'Comercial';
$mod_strings['LBL_SOURCE'] = 'Medio de recepción';
$mod_strings['LBL_DESCRIPTION'] = 'Descripción PQR';
$mod_strings['LBL_PRIMARY_CONTACT_NAME'] = 'Contacto principal';
$mod_strings['LBL_SASA_SASA_TIPIFICACIONES_CASES_1_NAME_FIELD_TITLE'] = 'Tipificaciones';
$mod_strings['LBL_SASA_NEGOCIOS_C'] = 'Negocios';
$mod_strings['LBL_SASA_TIPOPRODUCTO_C'] = 'Tipo de producto';
$mod_strings['LBL_SASA_SUBPROCESO_C'] = 'Subproceso';
$mod_strings['LBL_SASA_PROCESO_C'] = 'Proceso';
$mod_strings['LBL_SASA_FUNCIONARIO_C'] = 'Funcionario/area';
$mod_strings['LBL_STATUS'] = 'Estado PQR';
$mod_strings['LBL_SASA_PQRS_C'] = 'PQRs';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0022022-06-18.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcasescustomfieldv0032022-06-29.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcaseslenguajev001-2022-07-01_02-16.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['LNK_CREATE'] = 'Nuevo PQRs';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_CASE'] = 'PQRs:';

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.afcaseslenguajev002-2022-07-15_11-43.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['LNK_CREATE'] = 'Nuevo PQRs';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_CASE'] = 'PQRs:';
$mod_strings['LBL_SASA_DIRECCIONSFC_C'] = 'Dirección';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.AFCasesCustomFieldsv0082022-09-28.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';
$mod_strings['LBL_MODULE_NAME'] = 'PQRs';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'PQRs';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo PQRs';
$mod_strings['LNK_CASE_LIST'] = 'Ver PQRs';
$mod_strings['LNK_IMPORT_CASES'] = 'Importar PQRs';
$mod_strings['LBL_CONTACT_CASE_TITLE'] = 'Contacto-PQRs:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'PQRs';
$mod_strings['LBL_MODULE_TITLE'] = 'PQRs: Inicio';
$mod_strings['LBL_SHOW_MORE'] = 'Mostrar más PQRs';
$mod_strings['LNK_CREATE_WHEN_EMPTY'] = 'Crear un PQRs ahora';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_INTRO'] = 'El módulo de PQRs es para la gestión de problemas de compatibilidad que afecta a las Cuentas. Utilice las flechas más abajo para una visita rápida.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_PAGE'] = 'Esta página muestra el listado de PQRs existentes asociados a su Cuenta.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_FILTER'] = 'Puede filtrar el listado de PQRs indicando un término de búsqueda.';
$mod_strings['LBL_PORTAL_TOUR_RECORDS_CREATE'] = 'Si tiene un nuevo PQRs de asistencia que le gustaría enviar, puede hacer clic aquí para enviarlo.';
$mod_strings['LBL_SASA_CAMPOPRUEBA_C'] = 'campo de prueba';
$mod_strings['LBL_SASA_TIPODESOLICITUD_C'] = 'Tipo de Solicitud';
$mod_strings['LBL_SASA_NUMERODEID_C'] = 'Número de ID';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Nuevo Panel 5';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_TIPODEIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_TIPOID_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_CORREOELECTRONICO_C'] = 'Correo electrónico';
$mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_SASA_FECHACIERRE_C'] = 'Fecha de Cierre';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_SASA_COMERCIAL_C'] = 'Comercial';
$mod_strings['LBL_SOURCE'] = 'Medio de recepción';
$mod_strings['LBL_DESCRIPTION'] = 'Descripción PQR';
$mod_strings['LBL_PRIMARY_CONTACT_NAME'] = 'Contacto principal';
$mod_strings['LBL_SASA_SASA_TIPIFICACIONES_CASES_1_NAME_FIELD_TITLE'] = 'Tipificaciones';
$mod_strings['LBL_SASA_NEGOCIOS_C'] = 'Negocios';
$mod_strings['LBL_SASA_TIPOPRODUCTO_C'] = 'Tipo de producto';
$mod_strings['LBL_SASA_SUBPROCESO_C'] = 'Subproceso';
$mod_strings['LBL_SASA_PROCESO_C'] = 'Proceso';
$mod_strings['LBL_SASA_FUNCIONARIO_C'] = 'Funcionario/area';
$mod_strings['LBL_STATUS'] = 'Estado PQR';
$mod_strings['LBL_SASA_PQRS_C'] = 'PQRs';
$mod_strings['LBL_SASA_DETALLE_C'] = 'Detalle';
$mod_strings['LBL_SASA_MOSTRARDETALLE_C'] = 'Mostrar Detalle';
$mod_strings['LBL_SASA_RESPUESTA_C'] = 'Respuesta';
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/Language/en_us.customcases_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CASES_CONTACTS_1_FROM_CASES_TITLE'] = 'Contactos';

?>
