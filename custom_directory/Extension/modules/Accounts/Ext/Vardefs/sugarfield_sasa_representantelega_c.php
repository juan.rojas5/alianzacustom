<?php
 // created: 2022-10-25 23:17:01
$dictionary['Account']['fields']['sasa_representantelega_c']['labelValue']='Representante Legal';
$dictionary['Account']['fields']['sasa_representantelega_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_representantelega_c']['enforced']='';
$dictionary['Account']['fields']['sasa_representantelega_c']['dependency']='';
$dictionary['Account']['fields']['sasa_representantelega_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_representantelega_c']['readonly_formula']='';

 ?>