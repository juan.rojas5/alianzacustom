<?php
// created: 2019-03-08 18:56:45
$dictionary["sasaP_PresupuestosxAsesor"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestoxProducto',
  'bean_name' => 'sasaP_PresupuestoxProducto',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link-type' => 'many',
  'side' => 'left',
);
