<?php
$dependencies['Leads']['SetOptionDepartamento4'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Archipielago de San Andres Providencia y Santa Catalina")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Providencia","San Andres")',
           'labels' => 'createList("","Providencia","San Andrés")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
