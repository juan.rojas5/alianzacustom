<?php
 // created: 2022-06-25 19:22:49

$app_list_strings['sasa_superior1_c_list']=array (
  '' => '',
  'Director Comercial de Canales Externos' => 'Director Comercial de Canales Externos',
  'Director Comercial Grupo Inversion 1' => 'Director Comercial Grupo Inversión 1',
  'Director Comercial Grupo Inversion 2' => 'Director Comercial Grupo Inversión 2',
  'Director Comercial Nacional Banca Corporativa' => 'Director Comercial Nacional Banca Corporativa',
  'Gerente Nacional Negocios de Inversion' => 'Gerente Nacional Negocios de Inversión',
  'Gerente Regional Barranquilla' => 'Gerente Regional Barranquilla',
  'Gerente Regional Cali' => 'Gerente Regional Cali',
  'Gerente Regional Medellin' => 'Gerente Regional Medellín',
  'Vicepresidente Distribucion y Ventas' => 'Vicepresidente Distribución y Ventas',
  'Gerente Regional Eje Cafetero' => 'Gerente Regional Eje Cafetero',
  'Gerente Fondos Alternativos' => 'Gerente Fondos Alternativos',
  'Director Comercial Grupo Inversion Renta Alta 1' => 'Director Comercial Grupo Inversión Renta Alta 1',
  'Director Comercial Grupo Inversion Renta Alta 2' => 'Director Comercial Grupo Inversión Renta Alta 2',
  'Gerencia SAC' => 'Gerencia SAC',
  'Gerente Banca Privada' => 'Gerente Banca Privada',
);