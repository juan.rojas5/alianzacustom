<?php
$dependencies['Leads']['SetOptionDepartamento29'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Sucre")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Sucre","Buenavista","La Union","Caimito","Chalan","Coloso","Corozal","Covenas","El Roble","Galeras","Guaranda","Los Palmitos","Majagual","Morroa","Ovejas","Palmito","Sampues","San Benito Abad","San Juan de Betulia","San Luis de Since","San Marcos","San Onofre","San Pedro","Santiago de Tolu","Sincelejo","Tolu Viejo")',
           'labels' => 'createList("","Sucre","Buenavista","La Unión","Caimito","Chalán","Coloso","Corozal","Coveñas","El Roble","Galeras","Guaranda","Los Palmitos","Majagual","Morroa","Ovejas","Palmito","Sampués","San Benito Abad","San Juan de Betulia","San Luis de Sincé","San Marcos","San Onofre","San Pedro","Santiago de Tolú","Sincelejo","Tolú Viejo")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
