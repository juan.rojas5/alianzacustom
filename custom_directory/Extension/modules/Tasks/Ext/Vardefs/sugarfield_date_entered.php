<?php
 // created: 2018-05-29 19:07:04
$dictionary['Task']['fields']['date_entered']['audited']=true;
$dictionary['Task']['fields']['date_entered']['comments']='Date record created';
$dictionary['Task']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_entered']['unified_search']=false;
$dictionary['Task']['fields']['date_entered']['calculated']=false;
$dictionary['Task']['fields']['date_entered']['enable_range_search']='1';

 ?>