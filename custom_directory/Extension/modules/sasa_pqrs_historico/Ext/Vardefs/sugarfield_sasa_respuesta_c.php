<?php
 // created: 2023-03-21 21:56:22
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['labelValue']='Respuesta';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_respuesta_c']['readonly_formula']='';

 ?>