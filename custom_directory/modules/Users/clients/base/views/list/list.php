<?php
// created: 2023-02-03 09:39:47
$viewdefs['Users']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'sortable' => true,
        ),
        1 => 
        array (
          'name' => 'title',
          'label' => 'LBL_TITLE',
          'enabled' => true,
          'default' => true,
          'sortable' => true,
        ),
        2 => 
        array (
          'name' => 'department',
          'label' => 'LBL_DEPARTMENT',
          'enabled' => true,
          'default' => true,
          'sortable' => true,
        ),
        3 => 
        array (
          'name' => 'email',
          'label' => 'LBL_EMAIL',
          'enabled' => true,
          'default' => true,
          'sortable' => true,
        ),
        4 => 
        array (
          'name' => 'reports_to_name',
          'label' => 'LBL_REPORTS_TO_NAME',
          'enabled' => true,
          'id' => 'REPORTS_TO_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'phone_home',
          'label' => 'LBL_HOME_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        6 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        7 => 
        array (
          'name' => 'phone_other',
          'label' => 'LBL_OTHER_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        8 => 
        array (
          'name' => 'phone_work',
          'label' => 'LBL_OFFICE_PHONE',
          'default' => false,
          'enabled' => true,
          'sortable' => true,
        ),
        9 => 
        array (
          'name' => 'phone_fax',
          'label' => 'LBL_FAX_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'address_street',
          'label' => 'LBL_ADDRESS_STREET',
          'enabled' => true,
          'sortable' => false,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'address_city',
          'label' => 'LBL_ADDRESS_CITY',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'address_state',
          'label' => 'LBL_ADDRESS_STATE',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'address_postalcode',
          'label' => 'LBL_ADDRESS_POSTALCODE',
          'enabled' => true,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);