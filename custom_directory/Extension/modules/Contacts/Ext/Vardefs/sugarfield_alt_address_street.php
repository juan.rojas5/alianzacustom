<?php
 // created: 2018-07-14 15:07:53
$dictionary['Contact']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.32',
  'searchable' => true,
);
$dictionary['Contact']['fields']['alt_address_street']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_street']['rows']='4';
$dictionary['Contact']['fields']['alt_address_street']['cols']='20';
$dictionary['Contact']['fields']['alt_address_street']['required']=false;
$dictionary['Contact']['fields']['alt_address_street']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>