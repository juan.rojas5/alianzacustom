<?php
 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_tipoinfoperiodicidad_c']['labelValue']='Tipo de Información y Periodicidad';
$dictionary['Contact']['fields']['sasa_tipoinfoperiodicidad_c']['visibility_grid']=array (
  'trigger' => 'sasa_infoquiererecibir_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'Renta Fija' => 
    array (
      0 => 'English man en Bogota trimestral',
      1 => 'Tenedores de TES mensual',
      2 => 'Emisiones corporativas deuda privada trimestral',
      3 => 'Informe del PEI mensual',
    ),
    'Renta Variable' => 
    array (
      0 => 'Mensual y flujos de acciones mensual',
      1 => 'Informes corporativos trimestral',
      2 => 'Rebalanceo del indice COLCAP trimestral',
      3 => 'Pasajeros de Avianca mensual',
    ),
    'Saturday Live' => 
    array (
      0 => 'Saturday Live Semanal los sabados',
    ),
  ),
);

 ?>