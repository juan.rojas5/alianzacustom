<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

use function PHPSTORM_META\type;

class relate_typing_to_cases_class
{
    function relate_typing_to_cases_method($bean, $event, $arguments)
    {
        // ignore para evitar loop despues de usar el método save()
        $ignore = $bean->ignore_relate_typing_to_cases;

        if (!isset($ignore) || $ignore === false){

            $ignore = true;
            $bean->ignore_relate_typing_to_cases = $ignore;

            $GLOBALS['log']->security("\n\n");
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Inicio acción before_save en relate_typing módulo Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
            
            $GLOBALS['log']->security("Bean tipo de solicitud -> ".$bean->sasa_tipodesolicitud_c);
            $GLOBALS['log']->security("Bean medio de recepción -> ".$bean->source);
            // $GLOBALS['log']->security("Bean proceso -> ".$bean->sasa_proceso_c);
            // $GLOBALS['log']->security("Bean subproceso -> ".$bean->sasa_subproceso_c);
            // $GLOBALS['log']->security("Bean detalle -> ".$bean->sasa_detalle_c);
            // $GLOBALS['log']->security("Bean motivo ->".$bean->sasa_motivo_c);

            if (  $bean->sasa_tipodesolicitud_c == 'peticion' && $bean->source != 'SFC' ) 
            {
                if ( !empty($bean->sasa_proceso_c) && !empty($bean->sasa_subproceso_c) ) 
                {
                    $tipo = $bean->sasa_tipodesolicitud_c;
                    $proceso = $bean->sasa_proceso_c;
                    $subproceso = $bean->sasa_subproceso_c;
                    $detalle = $bean->sasa_detalle_c;

                    if ( !isset($bean->fetched_row['id']) ) 
                    {
                        $GLOBALS['log']->security("Registro nuevo");

                        if ( !empty($detalle) ) 
                        {
                            // $GLOBALS['log']->security("Detalle no esta vacio");
                            $resultQuery = $this->consultaConDetalle($tipo, $proceso, $subproceso, $detalle);

                            while ( $row = $GLOBALS['db']->fetchByAssoc($resultQuery) ) 
                            {
                                // $GLOBALS['log']->security("Antes de crear relación");
                                if ( !empty($row['id_c']) ) 
                                {   
                                    // $GLOBALS['log']->security("Ya todo listo para crear relación");
                                    $this->crearRelacion($bean->id, $row['id_c']);
                                }
                            }
                        } else {
                            // $GLOBALS['log']->security("Detalle esta vacio");
                            
                            $bean->sasa_detalle_c = '161';
                            $resultQuery = $this->consultaConDetalle($tipo, $proceso, $subproceso, $bean->sasa_detalle_c);

                            while ( $row = $GLOBALS['db']->fetchByAssoc($resultQuery) ) 
                            {
                                // $GLOBALS['log']->security("Antes de crear relación");
                                if ( !empty($row['id_c']) ) 
                                {
                                    // $GLOBALS['log']->security("Ya todo listo para crear relación");
                                    $this->crearRelacion($bean->id, $row['id_c']);
                                }
                            }
                        }
                    } else {
                        $GLOBALS['log']->security("Registro existente");

                        $tipoAntes = $bean->fetched_row['sasa_tipodesolicitud_c'];
                        $procesoAntes = $bean->fetched_row['sasa_proceso_c'];
                        $subprocesoAntes = $bean->fetched_row['sasa_subproceso_c'];
                        $detalleAntes = $bean->fetched_row['sasa_detalle_c'];

                        if ( empty($detalle) ) // validar para momento 1 
                        {
                            // $GLOBALS['log']->security("Detalle esta vacio");
                            if ( $detalleAntes != $detalle || $detalleAntes == $detalle )
                            {
                                // $GLOBALS['log']->security("Si detalle ha estado o paso a vacio");
                                if ( $tipoAntes != $tipo || $procesoAntes != $proceso || $subprocesoAntes != $subproceso || $detalleAntes != $detalle ) 
                                {
                                    // $GLOBALS['log']->security("Si alguno de los 4 campos cambio de valor");
                                    $resultQuery = $this->consultaTipificacionRelacionada($bean->id);
                                    $bean->sasa_detalle_c = '161';

                                    while ( $row = $GLOBALS['db']->fetchByAssoc($resultQuery) ) 
                                    {
                                        // $GLOBALS['log']->security("Cargo relación: ".$row['id']);
                                        if ( !empty($row['id']) ) 
                                        {
                                            $resultConDetalles = $this->consultaConDetalle($tipo, $proceso, $subproceso, $bean->sasa_detalle_c);

                                            while ( $rowNew = $GLOBALS['db']->fetchByAssoc($resultConDetalles) ) 
                                            {
                                                // $GLOBALS['log']->security("Ya inicia actualización sin detalle");
                                                $this->actualizarRelacion($bean->id, $row['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida'], $rowNew['id_c']);
                                            }
                                        }    
                                    }

                                    if ( mysqli_num_rows($resultQuery) == 0 )
                                    {
                                        $resultConDestalles = $this->consultaConDetalle($tipo, $proceso, $subproceso, $bean->sasa_detalle_c);

                                        while ( $rowNew = $GLOBALS['db']->fetchByAssoc($resultConDestalles) ) 
                                        {
                                            $this->crearRelacion($bean->id, $rowNew['id_c']);
                                        }
                                    }
                                }
                            }
                        }

                        if ( !empty($detalle) ) // validar para momento 2
                        {
                            // $GLOBALS['log']->security("Detalle no esta vacio");
                            if ( $tipoAntes != $tipo || $procesoAntes != $proceso || $subprocesoAntes != $subproceso || $detalleAntes != $detalle ) 
                            {
                                // $GLOBALS['log']->security("Hay algún cambio");
                                $resultQuery = $this->consultaTipificacionRelacionada($bean->id);

                                while ( $row = $GLOBALS['db']->fetchByAssoc($resultQuery) ) 
                                {
                                    // $GLOBALS['log']->security("Cargo relación de tipificación");
                                    if ( !empty($row['id']) ) 
                                    {
                                        $resultConDestalles = $this->consultaConDetalle($tipo, $proceso, $subproceso, $detalle);

                                        while ( $rowNew = $GLOBALS['db']->fetchByAssoc($resultConDestalles) ) 
                                        {
                                            $this->actualizarRelacion($bean->id, $row['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida'], $rowNew['id_c']);
                                        }
                                    }
                                }

                                if ( mysqli_num_rows($resultQuery) == 0 ) 
                                {
                                    $resultConDestalles = $this->consultaConDetalle($tipo, $proceso, $subproceso, $detalle);

                                    while ( $rowNew = $GLOBALS['db']->fetchByAssoc($resultConDestalles) ) 
                                    {
                                        $this->crearRelacion($bean->id, $rowNew['id_c']);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Termino acción before_save en relate_typing módulo Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");

        }
    }
    
    function consultaSinDetalle($tipo, $proceso, $subproceso)
    {
        $query = "SELECT tc.id_c, tc.sasa_motivo_c, tc.sasa_categoria_c, 
            tc.sasa_proceso_c, tc.sasa_detalle_c
            FROM sasa_sasa_tipificaciones AS t
            INNER JOIN sasa_sasa_tipificaciones_cstm AS tc ON tc.id_c = t.id
            WHERE tc.sasa_motivo_c = '{$tipo}'
            AND tc.sasa_categoria_c = '{$proceso}'
            AND tc.sasa_proceso_c = '{$subproceso}'
            AND tc.sasa_detalle_c IS null
            AND t.deleted = 0
        LIMIT 1;";

        $resultQuery = $GLOBALS['db']->query($query);
        return $resultQuery;
    }

    function consultaConDetalle($tipo, $proceso, $subproceso, $detalle)
    {
        $query = "SELECT tc.id_c, tc.sasa_motivo_c, tc.sasa_categoria_c, 
            tc.sasa_proceso_c, tc.sasa_detalle_c
            FROM sasa_sasa_tipificaciones AS t
            INNER JOIN sasa_sasa_tipificaciones_cstm AS tc ON tc.id_c = t.id
            WHERE tc.sasa_motivo_c = '{$tipo}'
            AND tc.sasa_categoria_c = '{$proceso}'
            AND tc.sasa_proceso_c = '{$subproceso}'
            AND tc.sasa_detalle_c = '{$detalle}'
            AND t.deleted = 0
        LIMIT 1;";

        $resultQuery = $GLOBALS['db']->query($query);
        return $resultQuery;
    }

    function consultaTipificacionRelacionada($idcaso)
    {
        $query = "SELECT id, sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida, sasa_sasa_tipificaciones_cases_1cases_idb
            FROM sasa_sasa_tipificaciones_cases_1_c
            WHERE sasa_sasa_tipificaciones_cases_1cases_idb = '{$idcaso}'
            AND deleted = 0
        LIMIT 1;";

        $resultQuery = $GLOBALS['db']->query($query);
        return $resultQuery;
    }

    function crearRelacion($idCaso, $idTipificacion)
    {
        $date = date('Y-m-d H:i:s');
        $query = "INSERT INTO sasa_sasa_tipificaciones_cases_1_c (id, date_modified, 
        deleted, sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida, 
        sasa_sasa_tipificaciones_cases_1cases_idb) 
        VALUES (uuid(), '{$date}', 0, '{$idTipificacion}', '{$idCaso}')";      
        
        $resultQuery = $GLOBALS['db']->query($query);
    }

    function actualizarRelacion($idCaso, $idTipificacion, $newIdTipificacion)
    {
        $query = "UPDATE sasa_sasa_tipificaciones_cases_1_c 
            SET sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida = '{$newIdTipificacion}'
            WHERE sasa_sasa_tipificaciones_cases_1cases_idb = '{$idCaso}'
            AND sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida = '{$idTipificacion}'
        AND deleted = 0";

        $resultQuery = $GLOBALS['db']->query($query);
    }

}