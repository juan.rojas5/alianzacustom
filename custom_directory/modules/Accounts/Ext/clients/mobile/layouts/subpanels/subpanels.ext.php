<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-02-20 21:38:37
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  ),
);

// created: 2019-01-04 21:39:25
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosaf_1',
  ),
);

// created: 2019-01-04 21:40:08
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosav_1',
  ),
);

// created: 2019-01-04 21:41:00
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosavdivisas_1',
  ),
);

// created: 2022-11-21 20:51:44
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_pqrs_historico_1',
  ),
);

// created: 2019-01-04 21:36:33
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_saldosaf_1',
  ),
);

// created: 2019-01-04 21:37:35
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_saldosav_1',
  ),
);

// created: 2019-01-04 22:23:30
$viewdefs['Accounts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasas_saldosconsolidados_1',
  ),
);