<?php

$dependencies['Leads']['ReadOnlyConverted'] = array(
    'hooks' => array("all"),
    'trigger' => 'and(equal($converted,true),not(isUserAdmin("")))',
    'triggerFields' => array('sasa_nroidentificacion_c','sasa_tipopersona_c','account_name','sasa_sector_c','sasa_tipoidentificacion_c','sasa_ocupacion_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipopersona_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'birthdate',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'account_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_sector_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_tipoidentificacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'lead_source',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'refered_by',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'title',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_telefono_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'do_not_call',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_mobile',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'email',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_pais_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_departamentointer_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_municipiointer_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_departamento_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_municipio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'primary_address',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'description',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'business_center_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_actividadeconomica_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ocupacion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'department',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_ingresosventasanuales_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_segmento_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nombreproducto_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_valorinversion_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'campaign_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'lead_source_description',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_home',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_telefono2_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assistant_phone',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'phone_fax',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'facebook',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'twitter',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'googleplus',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'website',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assistant',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'assigned_user_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_regional_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'team_name',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p1rangoedadcalidad_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p2propositoahorroinve_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p3horizontetiempoinve_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p4dondeprovienedinero_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p5porcentajeactivosah_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p6experienciainversio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p7oportunidadrentabil_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_p8supongainversionesp_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_perfilriesgo_c',
                'value' => 'true',
            ),
        ),
    ),
);

