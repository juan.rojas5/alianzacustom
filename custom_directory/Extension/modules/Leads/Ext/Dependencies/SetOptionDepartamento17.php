<?php
$dependencies['Leads']['SetOptionDepartamento17'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Guainia")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Puerto Colombia","Barranco Minas","Cacahual","Inirida","La Guadalupe","Mapiripana","Morichal","Pana Pana","San Felipe")',
           'labels' => 'createList("","Puerto Colombia","Barranco Minas","Cacahual","Inírida","La Guadalupe","Mapiripana","Morichal","Pana Pana","San Felipe")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
