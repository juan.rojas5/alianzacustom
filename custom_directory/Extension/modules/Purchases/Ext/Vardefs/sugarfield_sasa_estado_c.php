<?php
 // created: 2023-02-01 19:05:31
$dictionary['Purchase']['fields']['sasa_estado_c']['labelValue']='Estado producto';
$dictionary['Purchase']['fields']['sasa_estado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_estado_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_estado_c']['readonly_formula']='';

 ?>