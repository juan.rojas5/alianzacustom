<?php
 // created: 2022-10-25 23:17:00
$dictionary['Account']['fields']['sasa_regional_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sasa_regional_c']['labelValue']='Regional';
$dictionary['Account']['fields']['sasa_regional_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_regional_c']['calculated']='1';
$dictionary['Account']['fields']['sasa_regional_c']['formula']='related($assigned_user_link,"sasa_regional_c")';
$dictionary['Account']['fields']['sasa_regional_c']['enforced']='1';
$dictionary['Account']['fields']['sasa_regional_c']['dependency']='';

 ?>