<?php
// created: 2019-01-04 21:30:05
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_produ845etosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAVDivisas"]["fields"]["sasa_produ845etosafav_ida"] = array (
  'name' => 'sasa_produ845etosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE_ID',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
