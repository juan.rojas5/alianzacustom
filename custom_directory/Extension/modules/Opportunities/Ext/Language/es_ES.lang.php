<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RLI'] = 'Líneas de Ingreso';
$mod_strings['LBL_SASA_MOTIVOPERDIDO_C'] = 'Motivo Perdida o Ganacia (eliminar)';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_LIKELY'] = 'Valor Proyectado';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_SASA_NUMEROCONSECUTIVO_C'] = 'Número Consecutivo';
$mod_strings['LBL_SASA_NOMBRECOMPETIDOR_C'] = 'Nombre Competidor';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_TEST'] = 'test';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_MOTIVOPERDIDA_C'] = 'Motivo de Perdida';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['TPL_RLI_CREATE'] = 'Una oportunidad debe tener una Lnea de Ingreso asociada.';
