<?php
 // created: 2022-09-16 13:32:35
$dictionary['Task']['fields']['sasa_controlresplist_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_controlresplist_c']['labelValue']='Control Lista resp';
$dictionary['Task']['fields']['sasa_controlresplist_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_controlresplist_c']['calculated']='1';
$dictionary['Task']['fields']['sasa_controlresplist_c']['formula']='concat($status,$sasa_ctrl_sec_c,$sasa_lista_respuesta_c)';
$dictionary['Task']['fields']['sasa_controlresplist_c']['enforced']='1';
$dictionary['Task']['fields']['sasa_controlresplist_c']['dependency']='equal($sasa_tipo_tarea_list_c,"1")';
$dictionary['Task']['fields']['sasa_controlresplist_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_controlresplist_c']['readonly_formula']='';

 ?>