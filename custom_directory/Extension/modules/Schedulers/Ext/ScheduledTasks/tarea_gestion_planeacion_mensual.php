<?php
/*
* Funcion para planificador de tareas
* Caso sasa 5481 Tarea Programada - Generación de tarea para gestionar "Planeación Mensual"Se requiere generar una tarea programada, que se ejecute el primer día de cada mes y asigne una tarea a cada usuario con rol de "Asesor Comerciales" con un ANS de 5 días.
* El asunto de la Tarea debe ser: "Realizar Planeación Mensual"
*/
$job_strings[] = 'tarea_gestion_planeacion_mensual';
function tarea_gestion_planeacion_mensual(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_gestion_planeacion_mensual. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query= "
		SELECT
			users.id as id,users.user_name as name
		FROM
			`acl_roles_users`
		LEFT JOIN acl_roles ON acl_roles.id = acl_roles_users.role_id
		LEFT JOIN users ON users.id = acl_roles_users.user_id
		WHERE
			acl_roles_users.role_id = 'dae9f6ca-8a41-11e8-a40c-06cd310e1a43' AND acl_roles_users.deleted = 0 AND acl_roles.deleted = 0 AND users.deleted = 0
		";
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$bean = BeanFactory::newBean("Tasks", "", array('disable_row_level_security' => true));
				
				$assigned_user_id = $row['id'];
				$name = $row['name'];	
				
				$bogota_tz = new DateTimeZone("America/Bogota");
				$utc_tz = new DateTimeZone("UTC");
				
				$date_start = new SugarDateTime($bean->date_start);//http://support.sugarcrm.com/Documentation/Sugar_Developer/Sugar_Developer_Guide_7.9/Architecture/DateTime/
				$date_start->setTimezone($bogota_tz);
				$date_start->setTime(0, 1);
				$date_start->setTimezone($utc_tz);
				
				$date_due = new SugarDateTime($bean->date_due);
				$date_due->setTimezone($bogota_tz);
				$date_due->modify("+5 days");
				$date_due->setTime(23, 59);
				$date_due->setTimezone($utc_tz);
				
				$GLOBALS['log']->security("name: {$name} id: {$assigned_user_id}"); 
				$GLOBALS['log']->security(    "$date_start: ",print_r( $date_start  ,true)  );
				$GLOBALS['log']->security(    "$date_due: ",print_r( $date_due  ,true)  );
				
				
				$bean->name="Realizar Planeación Mensual";
				$bean->date_start=$date_start->asDb();
				$bean->date_due=$date_due->asDb();
				$bean->assigned_user_id=$assigned_user_id;
				$bean->save();
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_gestion_planeacion_mensual. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}
?>
