<?php
 // created: 2022-11-18 20:14:46
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['labelValue']='Creado_por_Nombre_Completo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_creado_por_nombre_compl_c']['readonly_formula']='';

 ?>