<?php
$dependencies['Leads']['SetOption2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Fideicomiso")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("Fideicomiso","Otro")',
           'labels' => 'createList("Fideicomiso","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
