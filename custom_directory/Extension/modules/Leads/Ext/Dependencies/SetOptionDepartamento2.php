<?php
$dependencies['Leads']['SetOptionDepartamento2'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Antioquia")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Abejorral","Abriaqui","Alejandria","Amaga","Amalfi","Andes","Angelopolis","Angostura","Anori","Anza","Apartado","Arboletes","Argelia","Armenia","Barbosa","Bello","Belmira","Betania","Betulia","Briceno","Buritica","Caceres","Caicedo","Caldas","Campamento","Canasgordas","Caracoli","Caramanta","Carepa","Carolina","Caucasia","Chigorodo","Cisneros","Ciudad Bolivar","Cocorna","Concepcion","Concordia","Copacabana","Dabeiba","Don Matias","Ebejico","El Bagre","El Carmen de Viboral","El Santuario","Entrerrios","Envigado","Fredonia","Frontino","Giraldo","Girardota","Gomez Plata","Granada","Guadalupe","Guarne","Guatape","Heliconia","Hispania","Itagui","Ituango","Jardin","Jerico","La Ceja","La Estrella","La Pintada","La Union","Liborina","Maceo","Marinilla","Medellin","Montebello","Murindo","Mutata","Narino","Nechi","Necocli","Olaya","Penol","Peque","Pueblorrico","Puerto Berrio","Puerto Nare","Puerto Triunfo","Remedios","Retiro","Rionegro","Sabanalarga","Sabaneta","Salgar","San Andres de Cuerquia","San Carlos","San Francisco","San Jeronimo","San Jose de La Montana","San Juan de Uraba","San Luis","San Pedro","San Pedro de Uraba","San Rafael","San Roque","San Vicente","Santa Barbara","Santa Rosa de Osos","Santafe de Antioquia","Santo Domingo","Segovia","Sonson","Sopetran","Tamesis","Taraza","Tarso","Titiribi","Toledo","Turbo","Uramita","Urrao","Valdivia","Valparaiso","Vegachi","Venecia","Vigia del Fuerte","Yali","Yarumal","Yolombo","Yondo","Zaragoza")',
           'labels' => 'createList("","Abejorral","Abriaquí","Alejandría","Amagá","Amalfi","Andes","Angelópolis","Angostura","Anorí","Anza","Apartadó","Arboletes","Argelia","Armenia","Barbosa","Bello","Belmira","Betania","Betulia","Briceño","Buriticá","Cáceres","Caicedo","Caldas","Campamento","Cañasgordas","Caracolí","Caramanta","Carepa","Carolina","Caucasia","Chigorodó","Cisneros","Ciudad Bolívar","Cocorná","Concepción","Concordia","Copacabana","Dabeiba","Don Matías","Ebéjico","El Bagre","El Carmen de Viboral","El Santuario","Entrerrios","Envigado","Fredonia","Frontino","Giraldo","Girardota","Gómez Plata","Granada","Guadalupe","Guarne","Guatapé","Heliconia","Hispania","Itagui","Ituango","Jardín","Jericó","La Ceja","La Estrella","La Pintada","La Unión","Liborina","Maceo","Marinilla","Medellín","Montebello","Murindó","Mutatá","Nariño","Nechí","Necoclí","Olaya","Peñol","Peque","Pueblorrico","Puerto Berrío","Puerto Nare","Puerto Triunfo","Remedios","Retiro","Rionegro","Sabanalarga","Sabaneta","Salgar","San Andrés de Cuerquía","San Carlos","San Francisco","San Jerónimo","San José de La Montaña","San Juan de Urabá","San Luis","San Pedro","San Pedro de Uraba","San Rafael","San Roque","San Vicente","Santa Bárbara","Santa Rosa de Osos","Santafé de Antioquia","Santo Domingo","Segovia","Sonsón","Sopetrán","Támesis","Tarazá","Tarso","Titiribí","Toledo","Turbo","Uramita","Urrao","Valdivia","Valparaíso","Vegachí","Venecia","Vigía del Fuerte","Yalí","Yarumal","Yolombó","Yondó","Zaragoza")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
