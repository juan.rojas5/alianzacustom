<?php
 // created: 2022-11-16 02:16:38
$dictionary['Contact']['fields']['sasa_departamento_c']['labelValue']='Departamento';
$dictionary['Contact']['fields']['sasa_departamento_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_departamento_c']['visibility_grid']=array (
  'trigger' => 'sasa_pais_c',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'No Registra' => 
    array (
      0 => 'No Registra',
    ),
    'Colombia' => 
    array (
      0 => '',
      1 => 'Amazonas',
      2 => 'Antioquia',
      3 => 'Arauca',
      4 => 'Archipielago de San Andres Providencia y Santa Catalina',
      5 => 'Atlantico',
      6 => 'Bogota D.C.',
      7 => 'Bolivar',
      8 => 'Boyaca',
      9 => 'Caldas',
      10 => 'Caqueta',
      11 => 'Casanare',
      12 => 'Cauca',
      13 => 'Cesar',
      14 => 'Choco',
      15 => 'Cundinamarca',
      16 => 'Cordoba',
      17 => 'Guainia',
      18 => 'Guaviare',
      19 => 'Huila',
      20 => 'La Guajira',
      21 => 'Magdalena',
      22 => 'Meta',
      23 => 'Narino',
      24 => 'Norte de Santander',
      25 => 'Putumayo',
      26 => 'Quindio',
      27 => 'Risaralda',
      28 => 'Santander',
      29 => 'Sucre',
      30 => 'Tolima',
      31 => 'Valle del Cauca',
      32 => 'Vaupes',
      33 => 'Vichada',
    ),
  ),
);

 ?>