<?php
$dependencies['Leads']['SetOptionDepartamento14'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Choco")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Riosucio","Acandi","Alto Baudo","Atrato","Bagado","Bahia Solano","Bajo Baudo","Belen de Bajira","Bojaya","Carmen del Darien","Certegui","Condoto","El Canton del San Pablo","El Carmen de Atrato","El Litoral del San Juan","Istmina","Jurado","Lloro","Medio Atrato","Medio Baudo","Medio San Juan","Novita","Nuqui","Quibdo","Rio Iro","Rio Quito","San Jose del Palmar","Sipi","Tado","Unguia","Union Panamericana")',
           'labels' => 'createList("","Riosucio","Acandí","Alto Baudo","Atrato","Bagadó","Bahía Solano","Bajo Baudó","Belén de Bajira","Bojaya","Carmen del Darien","Cértegui","Condoto","El Cantón del San Pablo","El Carmen de Atrato","El Litoral del San Juan","Istmina","Juradó","Lloró","Medio Atrato","Medio Baudó","Medio San Juan","Nóvita","Nuquí","Quibdó","Río Iro","Río Quito","San José del Palmar","Sipí","Tadó","Unguía","Unión Panamericana")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
