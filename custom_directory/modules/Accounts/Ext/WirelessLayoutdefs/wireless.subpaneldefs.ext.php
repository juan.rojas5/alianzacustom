<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_saldosaf_1_Accounts.php

 // created: 2019-01-04 21:36:33
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_saldosaf_1'] = array (
  'order' => 100,
  'module' => 'sasa_SaldosAF',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'get_subpanel_data' => 'accounts_sasa_saldosaf_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_saldosav_1_Accounts.php

 // created: 2019-01-04 21:37:35
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_saldosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_SaldosAV',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'get_subpanel_data' => 'accounts_sasa_saldosav_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_movimientosaf_1_Accounts.php

 // created: 2019-01-04 21:39:25
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_movimientosaf_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAF',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'get_subpanel_data' => 'accounts_sasa_movimientosaf_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_movimientosav_1_Accounts.php

 // created: 2019-01-04 21:40:08
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_movimientosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAV',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'get_subpanel_data' => 'accounts_sasa_movimientosav_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_movimientosavdivisas_1_Accounts.php

 // created: 2019-01-04 21:41:00
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_movimientosavdivisas_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAVDivisas',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'get_subpanel_data' => 'accounts_sasa_movimientosavdivisas_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasas_saldosconsolidados_1_Accounts.php

 // created: 2019-01-04 22:23:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasas_saldosconsolidados_1'] = array (
  'order' => 100,
  'module' => 'sasaS_SaldosConsolidados',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'get_subpanel_data' => 'accounts_sasas_saldosconsolidados_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa_pqrs_historico_1_Accounts.php

 // created: 2022-11-21 20:51:44
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_pqrs_historico_1'] = array (
  'order' => 100,
  'module' => 'sasa_pqrs_historico',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'get_subpanel_data' => 'accounts_sasa_pqrs_historico_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_sasa1_unidades_de_negocio_por__1_Accounts.php

 // created: 2023-02-20 21:38:36
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa1_unidades_de_negocio_por__1'] = array (
  'order' => 100,
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'get_subpanel_data' => 'accounts_sasa1_unidades_de_negocio_por__1',
);

?>
