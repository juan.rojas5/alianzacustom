<?php

require_once('clients/base/api/FilterApi.php');

/**
 * Class CustomFilterApi
 */
class CustomFilterApi extends FilterApi
{
    use \Sugarcrm\Sugarcrm\custom\inc\utils\ModuleHelper;

    /**
     * Disable list view without search if configured.
     *
     * @param ServiceBase $api
     * @param array $args
     * @param string $acl
     * @return array
     */
    public function filterList(ServiceBase $api, array $args, $acl = 'list')
    {
    	if($args['module'] == 'Dashboards'){
	    	$module =$args['module'][0]['dashboard_module'];
	    }
	    else{
	    	$module =$args['module'];
	    }
	    
        if ($this->isListDisabledForModule($args['module']) && !isset($args['filter'])) {
            return [
                'next_offset' => 0,
                'records' => [],
            ];
        }
        
        //$GLOBALS["log"]->security("oe: ".$module. "   -> ".print_r($args['filter'],true));

        return $this->returnParent($api, $args, $acl);
    }

    /**
     * @codeCoverageIgnore
     *
     * @param $api
     * @param $args
     * @param $acl
     * @return array
     */
    public function returnParent($api, $args, $acl)
    {
        return parent::filterList($api, $args, $acl);
    }
}
