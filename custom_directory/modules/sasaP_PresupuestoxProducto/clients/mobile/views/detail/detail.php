<?php
$module_name = 'sasaP_PresupuestoxProducto';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'detail' => 
      array (
        'templateMeta' => 
        array (
          'form' => 
          array (
            'buttons' => 
            array (
              0 => 'EDIT',
              1 => 'DUPLICATE',
              2 => 'DELETE',
            ),
          ),
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_presupuestoproducto_c',
                'label' => 'LBL_SASA_PRESUPUESTOPRODUCTO_C',
              ),
              3 => 
              array (
                'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name',
                'label' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
              ),
              4 => 
              array (
                'name' => 'sasa_saldoconsolidado_c',
                'label' => 'LBL_SASA_SALDOCONSOLIDADO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_deficit_c',
                'label' => 'LBL_SASA_DEFICIT_C',
              ),
              6 => 
              array (
                'name' => 'sasa_porcentajecumplimiento_c',
                'label' => 'LBL_SASA_PORCENTAJECUMPLIMIENTO_C',
              ),
              7 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              8 => 'assigned_user_name',
              9 => 'team_name',
              10 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              11 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
