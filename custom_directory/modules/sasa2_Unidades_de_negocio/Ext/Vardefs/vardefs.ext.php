<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa2_Unidades_de_negocio/Ext/Vardefs/sasa2_unidades_de_negocio_sasa_productosafav_1_sasa2_Unidades_de_negocio.php

// created: 2023-02-06 20:17:53
$dictionary["sasa2_Unidades_de_negocio"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa2_Unidades_de_negocio/Ext/Vardefs/sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_sasa2_Unidades_de_negocio.php

// created: 2023-02-17 20:29:36
$dictionary["sasa2_Unidades_de_negocio"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'bean_name' => 'sasa1_Unidades_de_negocio_por_',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
