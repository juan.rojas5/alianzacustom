<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'tarea_accounts_retraso_ctrl_wf';
function tarea_accounts_retraso_ctrl_wf(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_accounts_retraso_ctrl_wf. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		$query = "
		SELECT
		accounts_cstm.id_c AS idAccounts
		FROM
		accounts		
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
		accounts.deleted = 0 AND
		accounts.account_type = 'Customer' AND
		(
		accounts_cstm.sasa_retrasoctrlworkflow_c = '' OR
		accounts_cstm.sasa_retrasoctrlworkflow_c IS NULL
		)
		;
		";

		global $db;
		$result = $db->query($query);
		while ($row = $db->fetchByAssoc($result)){
			$idAccount = $row['idAccounts'];
			if(!empty($idAccount))
			{
				$Account = BeanFactory::getBean('Accounts', $idAccount,array('disable_row_level_security' => true));
				if(!empty($Account->id)){
					$Account->sasa_retrasoctrlworkflow_c = '1';
					$Account->save();
					Registry\Registry::getInstance()->drop('triggered_starts');	
					$GLOBALS['log']->security("\n{$Account->id} -> {$Account->name} = Update RetrasoCtrlWorkFlow");
				}
			}
		}

		//Tarea 5860 Por favor actualizar los equipos de la cuentas cada vez que la integración la cambia a Global. Debe quedar con los equipos del usuario asignado.
		//El wf de CU es el que asigna los equipos 
		$queryAccountsGlobal = "SELECT accounts.id AS idAccountsGlobal FROM accounts INNER JOIN users ON accounts.assigned_user_id=users.id INNER JOIN users_cstm ON users.id=users_cstm.id_c WHERE accounts.team_id='1' AND accounts.team_set_id='1' AND accounts.deleted='0' AND accounts.account_type='Customer' AND (users_cstm.sasa_superior1_c != 'Gerente Regional Eje Cafetero' or users_cstm.sasa_superior1_c != '' or users_cstm.sasa_superior1_c IS NOT NULL) AND (accounts.assigned_user_id != '' OR accounts.assigned_user_id IS NOT NULL) AND users_cstm.sasa_superior1_c!='Gerente Regional Eje Cafetero'";

		$resultAccountsGlobal = $db->query($queryAccountsGlobal);
		while ($row = $db->fetchByAssoc($resultAccountsGlobal)) {
			$idAccountGlobal = $row["idAccountsGlobal"];

			$AccountGlobal = BeanFactory::getBean('Accounts', $idAccountGlobal,array('disable_row_level_security' => true));

			if (!empty($AccountGlobal->id)) {
				$AccountGlobal->sasa_retrasoctrlworkflow_c = '3';
				$AccountGlobal->save();
				Registry\Registry::getInstance()->drop('triggered_starts');	
				$GLOBALS['log']->security("\n{$AccountGlobal->id} Cuentas Team Global");
			}
		}
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_accounts_retraso_ctrl_wf. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}
