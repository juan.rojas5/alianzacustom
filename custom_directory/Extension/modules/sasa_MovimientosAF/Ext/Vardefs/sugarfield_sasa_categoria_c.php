<?php
 // created: 2018-11-21 21:58:41
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosaf_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['enforced']=true;

 ?>