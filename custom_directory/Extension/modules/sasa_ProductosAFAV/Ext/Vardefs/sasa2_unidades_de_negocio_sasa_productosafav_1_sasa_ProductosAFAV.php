<?php
// created: 2023-02-06 20:17:53
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa2_Unidades_de_negocio',
  'bean_name' => 'sasa2_Unidades_de_negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link-type' => 'one',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1_name"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'name',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unid99b0negocio_ida"] = array (
  'name' => 'sasa2_unid99b0negocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE_ID',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
