<?php
 // created: 2018-09-12 21:54:41
$dictionary['Contact']['fields']['phone_other']['len']='100';
$dictionary['Contact']['fields']['phone_other']['massupdate']=false;
$dictionary['Contact']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_other']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_other']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.07',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_other']['calculated']=false;
$dictionary['Contact']['fields']['phone_other']['audited']=false;
$dictionary['Contact']['fields']['phone_other']['importable']='false';
$dictionary['Contact']['fields']['phone_other']['reportable']=false;
$dictionary['Contact']['fields']['phone_other']['pii']=false;

 ?>