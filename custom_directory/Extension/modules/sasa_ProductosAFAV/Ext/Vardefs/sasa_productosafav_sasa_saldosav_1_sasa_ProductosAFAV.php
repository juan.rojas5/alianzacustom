<?php
// created: 2019-01-04 21:26:58
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_saldosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAV',
  'bean_name' => 'sasa_SaldosAV',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
