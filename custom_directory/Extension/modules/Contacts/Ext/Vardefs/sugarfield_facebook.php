<?php
 // created: 2018-07-04 18:31:21
$dictionary['Contact']['fields']['facebook']['audited']=false;
$dictionary['Contact']['fields']['facebook']['massupdate']=false;
$dictionary['Contact']['fields']['facebook']['comments']='The facebook name of the user';
$dictionary['Contact']['fields']['facebook']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['facebook']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['facebook']['merge_filter']='disabled';
$dictionary['Contact']['fields']['facebook']['reportable']=false;
$dictionary['Contact']['fields']['facebook']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['facebook']['calculated']=false;
$dictionary['Contact']['fields']['facebook']['pii']=false;
$dictionary['Contact']['fields']['facebook']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>