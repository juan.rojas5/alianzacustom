<?php
 // created: 2023-02-06 20:21:32
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_purchases_1'] = array (
  'order' => 100,
  'module' => 'Purchases',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_purchases_1',
);
