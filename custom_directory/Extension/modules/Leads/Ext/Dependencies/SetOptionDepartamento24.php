<?php
$dependencies['Leads']['SetOptionDepartamento24'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Norte de Santander")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Cayetano","Puerto Santander","Abrego","Arboledas","Bochalema","Bucarasica","Cachira","Cacota","Chinacota","Chitaga","Convencion","Cucuta","Cucutilla","Durania","El Carmen","El Tarra","El Zulia","Gramalote","Hacari","Herran","La Esperanza","La Playa","Labateca","Los Patios","Lourdes","Mutiscua","Ocana","Pamplona","Pamplonita","Ragonvalia","Salazar","San Calixto","Santiago","Sardinata","Silos","Teorama","Tibu","Toledo","Villa Caro","Villa del Rosario")',
           'labels' => 'createList("","San Cayetano","Puerto Santander","Abrego","Arboledas","Bochalema","Bucarasica","Cachirá","Cácota","Chinácota","Chitagá","Convención","Cúcuta","Cucutilla","Durania","El Carmen","El Tarra","El Zulia","Gramalote","Hacarí","Herrán","La Esperanza","La Playa","Labateca","Los Patios","Lourdes","Mutiscua","Ocaña","Pamplona","Pamplonita","Ragonvalia","Salazar","San Calixto","Santiago","Sardinata","Silos","Teorama","Tibú","Toledo","Villa Caro","Villa del Rosario")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
