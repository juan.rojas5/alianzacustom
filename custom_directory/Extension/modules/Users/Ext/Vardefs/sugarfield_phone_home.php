<?php
 // created: 2018-07-13 21:31:52
$dictionary['User']['fields']['phone_home']['audited']=false;
$dictionary['User']['fields']['phone_home']['massupdate']=false;
$dictionary['User']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';
$dictionary['User']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['User']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['User']['fields']['phone_home']['unified_search']=false;
$dictionary['User']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['phone_home']['calculated']=false;

 ?>