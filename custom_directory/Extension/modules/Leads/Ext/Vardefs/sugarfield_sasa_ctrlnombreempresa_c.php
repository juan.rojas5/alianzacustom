<?php
 // created: 2018-08-31 13:32:02
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['labelValue']='Nombre Empresa';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1,35',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['formula']='ifElse(equal($sasa_tipopersona_c,"Juridica"),$account_name,"")';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['dependency']='';

 ?>