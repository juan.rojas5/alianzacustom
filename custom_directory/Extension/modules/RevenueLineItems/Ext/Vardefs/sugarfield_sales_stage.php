<?php
 // created: 2018-08-08 18:18:29
$dictionary['RevenueLineItem']['fields']['sales_stage']['default']='Perception Analysis';
$dictionary['RevenueLineItem']['fields']['sales_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['sales_stage']['required']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['massupdate']=true;
$dictionary['RevenueLineItem']['fields']['sales_stage']['options']='sasa_etapaventas_c_list';
$dictionary['RevenueLineItem']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['dependency']=false;

 ?>