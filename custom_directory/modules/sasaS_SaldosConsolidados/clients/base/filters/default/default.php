<?php
// created: 2018-12-18 16:39:47
$viewdefs['sasaS_SaldosConsolidados']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'accounts_sasas_saldosconsolidados_1_name' => 
    array (
    ),
    'sasa_categorias_sasas_saldosconsolidados_1_name' => 
    array (
    ),
    'sasa_saldototal_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);