<?php
 // created: 2023-04-10 20:40:33
$dictionary['Case']['fields']['status']['default']='';
$dictionary['Case']['fields']['status']['massupdate']=true;
$dictionary['Case']['fields']['status']['hidemassupdate']=false;
$dictionary['Case']['fields']['status']['comments']='The status of the case';
$dictionary['Case']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['status']['merge_filter']='disabled';
$dictionary['Case']['fields']['status']['calculated']=false;
$dictionary['Case']['fields']['status']['dependency']=false;
$dictionary['Case']['fields']['status']['options']='status_list';
$dictionary['Case']['fields']['status']['required']=true;
$dictionary['Case']['fields']['status']['visibility_grid']=array (
);

 ?>