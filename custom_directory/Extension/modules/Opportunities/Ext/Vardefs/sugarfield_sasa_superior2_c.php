<?php
 // created: 2018-07-09 13:22:33
$dictionary['Opportunity']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Opportunity']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_superior2_c']['calculated']='1';
$dictionary['Opportunity']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['Opportunity']['fields']['sasa_superior2_c']['enforced']='1';
$dictionary['Opportunity']['fields']['sasa_superior2_c']['dependency']='';

 ?>