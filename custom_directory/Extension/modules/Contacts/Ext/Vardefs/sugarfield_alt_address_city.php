<?php
 // created: 2018-07-14 15:06:42
$dictionary['Contact']['fields']['alt_address_city']['required']=false;
$dictionary['Contact']['fields']['alt_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_city']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_city']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>