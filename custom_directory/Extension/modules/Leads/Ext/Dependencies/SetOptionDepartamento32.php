<?php
$dependencies['Leads']['SetOptionDepartamento32'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Vaupes")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Caruru","Mitu","Pacoa","Papunaua","Taraira","Yavarate")',
           'labels' => 'createList("","Caruru","Mitú","Pacoa","Papunaua","Taraira","Yavaraté")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
