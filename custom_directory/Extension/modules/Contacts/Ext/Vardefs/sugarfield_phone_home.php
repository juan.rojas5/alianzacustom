<?php
 // created: 2018-07-13 21:10:57
$dictionary['Contact']['fields']['phone_home']['len']='20';
$dictionary['Contact']['fields']['phone_home']['required']=false;
$dictionary['Contact']['fields']['phone_home']['massupdate']=false;
$dictionary['Contact']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['phone_home']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_home']['calculated']=false;
$dictionary['Contact']['fields']['phone_home']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 ?>