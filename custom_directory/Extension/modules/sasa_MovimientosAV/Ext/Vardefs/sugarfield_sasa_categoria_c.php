<?php
 // created: 2018-11-21 22:08:07
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosav_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['enforced']=true;

 ?>