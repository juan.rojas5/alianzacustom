<?php
 // created: 2018-06-06 22:05:31

$app_list_strings['sasa_p8supongainversionesp_c_list']=array (
  '' => '',
  5 => 'a. Vendería todas mis inversiones',
  9 => 'b. Mantendría mis inversiones un poco más, esperando que se recuperen',
  13 => 'c. Aumentaría mis inversiones esperando que los precios regresen a niveles anteriores',
  1 => 'd. Me molestaría al punto de quejarme ante el regulador',
);