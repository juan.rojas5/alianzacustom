<?php

$job_strings[] = 'AddTeamToAccounts7';

function AddTeamToAccounts7()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Inicio tarea programada para agregar equipo Global a Cuentas #7. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    $ids = [];

    $query_accounts = "SELECT a.id, a.name, a.team_id, a.team_set_id
        FROM accounts AS a
        WHERE a.team_set_id NOT IN (
            SELECT tst.team_set_id
            FROM team_sets_teams AS tst
            WHERE tst.team_id = '1'
            AND tst.deleted = 0
        )
        AND a.deleted = 0
        AND a.team_id != '1'
        AND a.team_set_id != '1'
    LIMIT 200";

    $result_query_accounts = $GLOBALS['db']->query($query_accounts);

    while( $row_account = $GLOBALS['db']->fetchByAssoc($result_query_accounts) )
    {
        if( !empty($row_account['id']) )
        {
            $ids[] = $row_account['id'];
        }
    }

    $teamId = '';

    $queryTeam = "SELECT t.id, t.name
        FROM teams AS t
        WHERE t.name = 'Global'
        AND t.deleted = 0
    LIMIT 1"; 

    $resultQueryTeam = $GLOBALS['db']->query($queryTeam);

    while( $rowTeam = $GLOBALS['db']->fetchByAssoc($resultQueryTeam) )
    {
        if( !empty($rowTeam['id']) )
        {
            $teamId = $rowTeam['id'];
        }
    }

    if( !empty($teamId) )
    {
        foreach ($ids as $id) 
        {
            $account = BeanFactory::getBean('Accounts', $id);
            
            if( !empty($account->id) )
            {
                $teamSetBean = new TeamSet();
                $teams = $teamSetBean->getTeams($account->team_set_id);
    
                $hasTeamRelationship = false;

                foreach ($teams as $team) 
                {
                    // $GLOBALS['log']->security("Relación id: ".$team->id);
                    // $GLOBALS['log']->security("Relación name: ".$team->name);
    
                    if ($team->id === $teamId) 
                    {
                        $hasTeamRelationship = true;
                        break;
                    } 
                }
    
                if( $hasTeamRelationship )
                {
                    $GLOBALS['log']->security("Cuanta ya tiene relación con equipo Global...".$id);
    
                } else {
    
                    // $GLOBALS['log']->security("Se inicia creación de relación entre Cuenta y equipo Global...");
    
                    if( $account->load_relationship('teams') )
                    {
                        // $GLOBALS['log']->security("Se cargo relación con equipo Global...");

                        $account->teams->add(
                            array(
                                $teamId
                            )
                        );

                        $GLOBALS['log']->security("Se agrego equipo Global a cuenta: ".$id);
                    } else {

                        $GLOBALS['log']->security("No se logro crear relación para la cuenta: ".$id);
                    }
                }
            }
        }
    } else {

        $GLOBALS['log']->security("No se encontró equipo Global paara relacionar con cuentas");
    }

    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Fin tarea programada para agregar equipo Global a Cuentas #7. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    return true;
}


