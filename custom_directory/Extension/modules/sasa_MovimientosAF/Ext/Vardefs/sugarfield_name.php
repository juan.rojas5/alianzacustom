<?php
 // created: 2018-11-21 21:52:02
$dictionary['sasa_MovimientosAF']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAF']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAF']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAF']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAF']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAF']['fields']['name']['formula']='$sasa_encargo_c';
$dictionary['sasa_MovimientosAF']['fields']['name']['enforced']=true;

 ?>