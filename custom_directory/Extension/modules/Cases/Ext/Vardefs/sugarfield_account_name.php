<?php
 // created: 2022-08-26 16:58:07
$dictionary['Case']['fields']['account_name']['len']=255;
$dictionary['Case']['fields']['account_name']['required']=false;
$dictionary['Case']['fields']['account_name']['audited']=true;
$dictionary['Case']['fields']['account_name']['massupdate']=true;
$dictionary['Case']['fields']['account_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['account_name']['comments']='The name of the account represented by the account_id field';
$dictionary['Case']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['account_name']['reportable']=false;
$dictionary['Case']['fields']['account_name']['calculated']=false;
$dictionary['Case']['fields']['account_name']['dependency']='or(equal($sasa_tipodesolicitud_c,"quejas"),equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['account_name']['related_fields']=array (
  0 => 'account_id',
);

 ?>