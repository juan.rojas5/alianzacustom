<?php
 // created: 2018-08-08 18:11:45
$dictionary['Product']['fields']['revenuelineitem_name']['audited']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['massupdate']=true;
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge']='enabled';
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge_dom_value']=1;
$dictionary['Product']['fields']['revenuelineitem_name']['merge_filter']='disabled';
$dictionary['Product']['fields']['revenuelineitem_name']['calculated']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['studio']='visible';

 ?>