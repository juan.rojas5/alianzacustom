<?php
$job_strings[] = 'update_cc_campaign_results';
//$job_strings[] = 'export_staged_cc_contacts';
$job_strings[] = 'sync_staged_lists';
$job_strings[] = 'automatic_contact_sync';

require_once ('modules/fbsg_ConstantContactIntegration/include/ConstantContact.php');
function update_cc_campaign_results() {
	require_once ('modules/Contacts/Contact.php');
	require_once ('modules/Leads/Lead.php');
	global $db;

	// Check if this is the first time running a full detail campaign sync
	// OR if the Client has reset the sync to do a full detail campaign sync
	$csbean = BeanFactory::getBean('fbsg_ConstantContactIntegration');
	$csquery = new SugarQuery;
	$csquery->select();
	$csquery->from($csbean);
	$csquery->where()->equals('deleted', '0');
	$csresults = $csquery->execute();

	if (isset($csresults[0])){
		if ($csresults[0]['first_auto_campaign_sync'] == true){
			// If this is true then first_campaign_sync MUST also be true in
			// order to sync ALL the campaigns
			$csreset_bean = BeanFactory::getBean('fbsg_ConstantContactIntegration',$csresults[0]['id']);
			$csreset_bean->first_campaign_sync = true;
			$csreset_bean->save();
		}
	}

	$sCC = SugarCC::GetOnlyCC();
	if (! $sCC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found');
		return true;
	}

	$ut = new CCBasic($sCC->name, $sCC->password);

	$ccMass = new SugarCCMass($ut);

	$ccMass->GetAllCampaignResults();

	if (isset($csresults[0])){
		if ($csresults[0]['first_auto_campaign_sync'] == true){
			// If this is true then this means that the full deatil campaign
			// sync completed successfully and now this flag can be set to false
			// so that the sync will only sync the last 90 days worth of campaign
			// details thereby reducing the resources needed for the automatic sync
			$csreset_bean = BeanFactory::getBean('fbsg_ConstantContactIntegration',$csresults[0]['id']);
			$csreset_bean->first_auto_campaign_sync = false;
			$csreset_bean->save();
		}
	}

	return true;
}
function sync_staged_lists() {
	$sCC = SugarCC::GetOnlyCC();
	if (! $sCC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found.');
		return true;
	}

	$ut = new CCBasic($sCC->name, $sCC->password);
	$ccMass = new SugarCCMass($ut);
	$results = $ccMass->SyncStagedLists();

    echo print_r($results, true);

	return true;
}

/**
 * Auto-Syncs new & updated contacts from Constant Contact daily.
 *
 * @return boolean
 */
function automatic_contact_sync() {
	global $db, $timedate;
	$CC = SugarCC::GetOnlyCC();
	if (! $CC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found.');
		return true;
	}

	$ut = new CCBasic($CC->name, $CC->password);
	$incSync = new SugarIncrementalSync($ut, $CC->auto_import_module);

	$incSync->SyncAllLists();

	return true;
}

/**
 *
 *
 *
 */
function cc_people_update(){
	$results = fbsg_ConstantContactIntegration::updateCCListsAndPeople();
	if($results !== false){
		$admin = new \Administration();
		$admin->saveSetting('fbsgcci', 'cc_people_updated', 1);
	}
	return true;
}
