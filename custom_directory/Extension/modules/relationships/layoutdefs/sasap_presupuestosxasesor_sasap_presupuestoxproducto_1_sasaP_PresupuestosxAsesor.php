<?php
 // created: 2019-03-08 18:56:45
$layout_defs["sasaP_PresupuestosxAsesor"]["subpanel_setup"]['sasap_presupuestosxasesor_sasap_presupuestoxproducto_1'] = array (
  'order' => 100,
  'module' => 'sasaP_PresupuestoxProducto',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'get_subpanel_data' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
