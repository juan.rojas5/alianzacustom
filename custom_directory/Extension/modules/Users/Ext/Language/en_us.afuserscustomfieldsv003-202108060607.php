<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_ADDRESS_POSTALCODE'] = 'Código Postal de dirección';
$mod_strings['LBL_HOME_PHONE'] = 'Tel. Casa:';
$mod_strings['LBL_SASA_NOMBRECOMPLETO_C'] = 'Nombre completo';
$mod_strings['EN VACACIONES'] = 'En vacaciones';
$mod_strings['LBL_SASA_FECHAINICIO_C'] = 'Fecha inicio';
$mod_strings['LBL_SASA_FECHAFIN_C'] = 'Fecha fin';
$mod_strings['LBL_SASA_ENVACACIONES_C'] = 'En vacaciones';
$mod_strings['LBL_SASA_USUARIOREEMPLAZANTE_C_USER_ID'] = 'Usuario reemplazante (relacionado Usuario ID)';
$mod_strings['LBL_SASA_USUARIOREEMPLAZANTE_C'] = 'Usuario reemplazante';
$mod_strings['LBL_SASA_CONTROLVACACIONES_C'] = 'Control vacaciones';
