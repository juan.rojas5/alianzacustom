<?php
 // created: 2018-05-30 15:01:02
$dictionary['Note']['fields']['filename']['audited']=false;
$dictionary['Note']['fields']['filename']['massupdate']=false;
$dictionary['Note']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['Note']['fields']['filename']['importable']='true';
$dictionary['Note']['fields']['filename']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['filename']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['filename']['merge_filter']='disabled';
$dictionary['Note']['fields']['filename']['reportable']=false;
$dictionary['Note']['fields']['filename']['calculated']=false;

 ?>