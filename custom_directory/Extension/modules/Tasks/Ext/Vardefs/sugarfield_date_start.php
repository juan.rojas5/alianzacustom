<?php
 // created: 2018-05-30 14:18:06
$dictionary['Task']['fields']['date_start']['audited']=true;
$dictionary['Task']['fields']['date_start']['massupdate']=true;
$dictionary['Task']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_start']['unified_search']=false;
$dictionary['Task']['fields']['date_start']['calculated']=false;
$dictionary['Task']['fields']['date_start']['enable_range_search']='1';

 ?>