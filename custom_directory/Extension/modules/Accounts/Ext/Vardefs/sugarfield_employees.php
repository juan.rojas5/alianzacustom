<?php
 // created: 2018-06-12 20:19:12
$dictionary['Account']['fields']['employees']['len']='10';
$dictionary['Account']['fields']['employees']['audited']=true;
$dictionary['Account']['fields']['employees']['massupdate']=false;
$dictionary['Account']['fields']['employees']['comments']='Number of employees, varchar to accomodate for both number (100) or range (50-100)';
$dictionary['Account']['fields']['employees']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['employees']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['employees']['merge_filter']='disabled';
$dictionary['Account']['fields']['employees']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['employees']['calculated']=false;
$dictionary['Account']['fields']['employees']['dependency']='equal($sasa_tipopersona_c,"Juridica")';

 ?>