<?php
 // created: 2023-02-21 13:29:48
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['labelValue']='Código comercial';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['readonly_formula']='';

 ?>