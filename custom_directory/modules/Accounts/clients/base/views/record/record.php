<?php
// created: 2022-10-31 07:45:21
$viewdefs['Accounts']['base']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 => 
        array (
          'type' => 'escalate-action',
          'event' => 'button:escalate_button:click',
          'name' => 'escalate_button',
          'label' => 'LBL_ESCALATE_BUTTON_LABEL',
          'acl_action' => 'create',
        ),
        2 => 
        array (
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        3 => 
        array (
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        4 => 
        array (
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        5 => 
        array (
          'type' => 'divider',
        ),
        6 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates_button',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        7 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'Accounts',
          'acl_action' => 'create',
        ),
        8 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        9 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        10 => 
        array (
          'type' => 'divider',
        ),
        11 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_HEADER',
      'header' => true,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'picture',
          'type' => 'avatar',
          'size' => 'large',
          'dismiss_label' => true,
          'readonly' => true,
          'white_list' => true,
          'licenseDependency' => 
          array (
            'HINT' => 
            array (
              'name' => 'hint_account_pic',
              'type' => 'hint-accounts-logo',
            ),
          ),
        ),
        1 => 
        array (
          'name' => 'name',
          'type' => 'name',
          'licenseDependency' => 
          array (
            'HINT' => 
            array (
              'type' => 'hint-accounts-search-dropdown',
            ),
          ),
        ),
        2 => 
        array (
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        3 => 
        array (
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
        4 => 
        array (
          'name' => 'is_escalated',
          'type' => 'badge',
          'badge_label' => 'LBL_ESCALATED',
          'warning_level' => 'important',
          'dismiss_label' => true,
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 'parent_name',
        1 => 'account_type',
        2 => 
        array (
          'name' => 'sasa_tipopersona_c',
          'label' => 'LBL_SASA_TIPOPERSONA_C',
        ),
        3 => 
        array (
        ),
        4 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        5 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        6 => 'email',
        7 => 'website',
        8 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        9 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        10 => 
        array (
          'name' => 'billing_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_BILLING_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'billing_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_BILLING_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'billing_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_BILLING_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'billing_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_BILLING_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'billing_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_BILLING_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'billing_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_BILLING_ADDRESS_COUNTRY',
            ),
          ),
        ),
        11 => 
        array (
          'name' => 'shipping_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_SHIPPING_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'shipping_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'shipping_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'shipping_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'shipping_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'shipping_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
            ),
            5 => 
            array (
              'name' => 'copy',
              'label' => 'NTC_COPY_BILLING_ADDRESS',
              'type' => 'copy',
              'mapping' => 
              array (
                'billing_address_street' => 'shipping_address_street',
                'billing_address_city' => 'shipping_address_city',
                'billing_address_state' => 'shipping_address_state',
                'billing_address_postalcode' => 'shipping_address_postalcode',
                'billing_address_country' => 'shipping_address_country',
              ),
            ),
          ),
        ),
        12 => 
        array (
          'readonly' => false,
          'name' => 'sasa_retrasoctrlworkflow_c',
          'label' => 'LBL_SASA_RETRASOCTRLWORKFLOW_C',
        ),
        13 => 
        array (
          'readonly' => false,
          'name' => 'sasa_municipiointer_c',
          'label' => 'LBL_SASA_MUNICIPIOINTER_C',
        ),
        14 => 
        array (
          'readonly' => false,
          'name' => 'sasa_fechanac_c',
          'label' => 'LBL_SASA_FECHANAC_C',
        ),
        15 => 
        array (
          'readonly' => false,
          'name' => 'sasa_razon_social_c',
          'label' => 'LBL_SASA_RAZON_SOCIAL_C',
        ),
      ),
    ),
    2 => 
    array (
      'name' => 'panel_hidden',
      'label' => 'LBL_RECORD_SHOWMORE',
      'hide' => true,
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'readonly' => false,
          'name' => 'sasa_pais_c',
          'label' => 'LBL_SASA_PAIS_C',
        ),
        1 => 
        array (
        ),
        2 => 
        array (
          'readonly' => false,
          'name' => 'sasa_departamento_c',
          'label' => 'LBL_SASA_DEPARTAMENTO_C',
        ),
        3 => 
        array (
          'readonly' => false,
          'name' => 'sasa_municipio_c',
          'label' => 'LBL_SASA_MUNICIPIO_C',
        ),
        4 => 
        array (
          'name' => 'sasa_actividadeconomica_c',
          'label' => 'LBL_SASA_ACTIVIDADECONOMICA_C',
        ),
        5 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
        ),
        6 => 
        array (
          'name' => 'sasa_ocupacion_c',
          'label' => 'LBL_SASA_OCUPACION_C',
        ),
        7 => 
        array (
          'name' => 'sasa_importadorexportador_c',
          'label' => 'LBL_SASA_IMPORTADOREXPORTADOR_C',
        ),
        8 => 
        array (
          'name' => 'ownership',
        ),
        9 => 
        array (
          'name' => 'sasa_fechaconstitucion_c',
          'label' => 'LBL_SASA_FECHACONSTITUCION_C',
        ),
        10 => 
        array (
          'name' => 'sasa_segmento_c',
          'label' => 'LBL_SASA_SEGMENTO_C',
        ),
        11 => 
        array (
        ),
        12 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosventasmensual_c',
          'label' => 'LBL_SASA_INGRESOSVENTASMENSUAL_C',
        ),
        13 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosventasanuales_c',
          'label' => 'LBL_SASA_INGRESOSVENTASANUALES_C',
        ),
        14 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_activos_c',
          'label' => 'LBL_SASA_ACTIVOS_C',
        ),
        15 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_pasivos_c',
          'label' => 'LBL_SASA_PASIVOS_C',
        ),
        16 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosoperaciones_c',
          'label' => 'LBL_SASA_INGRESOSOPERACIONES_C',
        ),
        17 => 
        array (
          'name' => 'sasa_operamonedaextranjera_c',
          'label' => 'LBL_SASA_OPERAMONEDAEXTRANJERA_C',
        ),
        18 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_patrimonio_c',
          'label' => 'LBL_SASA_PATRIMONIO_C',
        ),
        19 => 'employees',
        20 => 
        array (
          'name' => 'description',
          'span' => 6,
        ),
        21 => 
        array (
          'name' => 'sasa_estadocuenta_c',
          'label' => 'LBL_SASA_ESTADOCUENTA_C',
          'span' => 6,
        ),
        22 => 
        array (
          'name' => 'geocode_status',
          'licenseFilter' => 
          array (
            0 => 'MAPS',
          ),
        ),
      ),
    ),
    3 => 
    array (
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_RECORDVIEW_PANEL3',
      'label' => 'LBL_RECORDVIEW_PANEL3',
      'columns' => 2,
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the company',
          'label' => 'LBL_FACEBOOK',
        ),
        1 => 
        array (
          'name' => 'googleplus',
          'comment' => 'The Google Plus name of the company',
          'label' => 'LBL_GOOGLEPLUS',
        ),
        2 => 
        array (
          'name' => 'twitter',
        ),
        3 => 
        array (
          'name' => 'campaign_name',
        ),
        4 => 
        array (
          'name' => 'phone_fax',
          'span' => 12,
        ),
        5 => 'hint_account_size',
        6 => 'hint_account_industry',
        7 => 'hint_account_location',
        8 => 'hint_account_founded_year',
        9 => 'hint_account_industry_tags',
        10 => 'hint_account_naics_code_lbl',
        11 => 'hint_account_fiscal_year_end',
        12 => 
        array (
          'name' => 'hint_account_facebook_handle',
          'type' => 'stage2_url',
        ),
        13 => 
        array (
          'name' => 'hint_account_logo',
          'type' => 'stage2_image',
          'readonly' => true,
          'dismiss_label' => true,
          'white_list' => true,
          'fields' => 
          array (
            0 => 'hint_account_pic',
          ),
        ),
        14 => 
        array (
        ),
      ),
    ),
    4 => 
    array (
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_RECORDVIEW_PANEL1',
      'label' => 'LBL_RECORDVIEW_PANEL1',
      'columns' => 2,
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'assigned_user_name',
        ),
        1 => 
        array (
          'name' => 'sasa_regional_c',
          'label' => 'LBL_SASA_REGIONAL_C',
        ),
        2 => 
        array (
          'name' => 'date_entered_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_ENTERED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_entered',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'created_by_name',
            ),
          ),
        ),
        3 => 
        array (
          'name' => 'date_modified_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_MODIFIED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_modified',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'modified_by_name',
            ),
          ),
        ),
        4 => 
        array (
          'name' => 'sasa_fechadevinculacionsc_c',
          'label' => 'LBL_SASA_FECHADEVINCULACIONSC_C',
        ),
        5 => 
        array (
          'name' => 'sasa_fechaactualizacion_c',
          'label' => 'LBL_SASA_FECHAACTUALIZACION_C',
        ),
        6 => 
        array (
          'name' => 'sasa_fechademodificacionsc_c',
          'label' => 'LBL_SASA_FECHADEMODIFICACIONSC_C',
        ),
        7 => 
        array (
          'name' => 'team_name',
        ),
        8 => 
        array (
          'name' => 'tag',
          'span' => 12,
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => false,
  ),
);