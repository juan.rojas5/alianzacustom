<?php
// created: 2022-12-11 05:52:01
$unified_search_modules_display = array (
  'Leads' => 
  array (
    'visible' => true,
  ),
  'Contacts' => 
  array (
    'visible' => true,
  ),
  'Accounts' => 
  array (
    'visible' => true,
  ),
  'Bugs' => 
  array (
    'visible' => false,
  ),
  'Calls' => 
  array (
    'visible' => false,
  ),
  'Cases' => 
  array (
    'visible' => false,
  ),
  'Documents' => 
  array (
    'visible' => false,
  ),
  'KBContents' => 
  array (
    'visible' => false,
  ),
  'Manufacturers' => 
  array (
    'visible' => false,
  ),
  'Meetings' => 
  array (
    'visible' => false,
  ),
  'Notes' => 
  array (
    'visible' => false,
  ),
  'Opportunities' => 
  array (
    'visible' => false,
  ),
  'ProductCategories' => 
  array (
    'visible' => false,
  ),
  'Products' => 
  array (
    'visible' => false,
  ),
  'RevenueLineItems' => 
  array (
    'visible' => false,
  ),
  'Contracts' => 
  array (
    'visible' => false,
  ),
  'Project' => 
  array (
    'visible' => false,
  ),
  'ProjectTask' => 
  array (
    'visible' => false,
  ),
  'ProspectLists' => 
  array (
    'visible' => false,
  ),
  'Prospects' => 
  array (
    'visible' => false,
  ),
  'Quotes' => 
  array (
    'visible' => false,
  ),
  'Tasks' => 
  array (
    'visible' => false,
  ),
  'fbsg_CCIntegrationLog' => 
  array (
    'visible' => false,
  ),
  'fbsg_ConstantContactIntegration' => 
  array (
    'visible' => false,
  ),
);