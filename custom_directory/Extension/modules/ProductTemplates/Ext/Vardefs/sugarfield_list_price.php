<?php
 // created: 2018-07-07 16:02:33
$dictionary['ProductTemplate']['fields']['list_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['list_price']['len']=26;
$dictionary['ProductTemplate']['fields']['list_price']['audited']=false;
$dictionary['ProductTemplate']['fields']['list_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['list_price']['comments']='List price of product ("List" in Quote)';
$dictionary['ProductTemplate']['fields']['list_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['list_price']['duplicate_merge']='enabled';
$dictionary['ProductTemplate']['fields']['list_price']['duplicate_merge_dom_value']='1';
$dictionary['ProductTemplate']['fields']['list_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['list_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['list_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['list_price']['enable_range_search']=false;

 ?>