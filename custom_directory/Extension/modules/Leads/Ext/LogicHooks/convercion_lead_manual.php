<?php
$hook_array['after_save'][] = Array(
	//Processing index. For sorting the array.
	10,

	//Label. A string value to identify the hook.
	'convercion_lead_manual',

	//The PHP file where your class is located.
	'custom/modules/Leads/convercion_lead_manual.php',

	//The class the method is in.
	'convercion_lead_manual',

	//The method to call.
	'after_save'
);

?>
