<?php 

    $mod_strings['LBL_SASA_PASIVOS_C'] = 'Pasivos';
    $mod_strings['LBL_SASA_INGRESOSOPERACIONES_C'] = 'Ingresos por Operaciones';
    $mod_strings['LBL_SASA_PATRIMONIO_C'] = 'Patrimonio Reportado';
    $mod_strings['LBL_SASA_ACTIVOS_C'] = 'Activos';
    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos/Ventas Anuales';
    $mod_strings['LBL_SASA_INGRESOSVENTASMENSUAL_C'] = 'Ingresos/Ventas Mensuales';
    $mod_strings['LBL_SASA_IMPORTADOREXPORTADOR_C'] = 'Importador/Exportador';
    $mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';