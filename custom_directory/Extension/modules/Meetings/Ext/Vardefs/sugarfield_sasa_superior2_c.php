<?php
 // created: 2018-07-26 02:47:48
$dictionary['Meeting']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Meeting']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Meeting']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Meeting']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Meeting']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Meeting']['fields']['sasa_superior2_c']['dependency']='';

 ?>