<?php
$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	10,

	//Label. A string value to identify the hook.
	'leads_valor_minimo_invercion',

	//The PHP file where your class is located.
	'custom/modules/RevenueLineItems/leads_valor_minimo_invercion.php',

	//The class the method is in.
	'leads_valor_minimo_invercion',

	//The method to call.
	'before_save'
);
?>
