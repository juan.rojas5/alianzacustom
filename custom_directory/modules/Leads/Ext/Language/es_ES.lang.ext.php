<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201805250845.php
 

    $mod_strings['LBL_TEXT_FIELD_EXAMPLE'] = 'Text Field Example';
    $mod_strings['LBL_DROPDOWN_FIELD_EXAMPLE'] = 'DropDown Field Example';
    $mod_strings['LBL_CHECKBOX_FIELD_EXAMPLE'] = 'Checkbox Field Example';
    $mod_strings['LBL_MULTISELECT_FIELD_EXAMPLE'] = 'Multi-Select Field Example';
    $mod_strings['LBL_DATE_FIELD_EXAMPLE'] = 'Date Field Example';
    $mod_strings['LBL_DATETIME_FIELD_EXAMPLE'] = 'DateTime Field Example';
    $mod_strings['LBL_ENCRYPT_FIELD_EXAMPLE'] = 'Encrypt Field Example';
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201805250952.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de Identificación';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Nro. de Identificación';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201805281102.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de Identificación';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Nro. de Identificación';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201805310950.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de Identificación';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Nro. de Identificación';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201805310956.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de Identificación';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Nro. de Identificación';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv001-201806071231.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';
    $mod_strings['LBL_SASA_P1RANGOEDADCALIDAD_C'] = '1. ¿En qué rango  de edad o calidad se encuentra?';
    $mod_strings['LBL_SASA_P2PROPOSITOAHORROINVE_C'] = '2. ¿Cuál es el propósito de su ahorro o inversión?';
    $mod_strings['LBL_SASA_P3HORIZONTETIEMPOINVE_C'] = '3. ¿Cuál es el horizonte de tiempo en el que mantendrá su inversión?';
    $mod_strings['LBL_SASA_P4DONDEPROVIENEDINERO_C'] = '4. ¿De dónde proviene el dinero que desea invertir?';
    $mod_strings['LBL_SASA_P5PORCENTAJEACTIVOSAH_C'] = '5. ¿Qué porcentaje del total de sus activos representa este ahorro o inversión?';
    $mod_strings['LBL_SASA_P6EXPERIENCIAINVERSIO_C'] = '6. ¿Ha tenido experiencia en el tema de inversiones?';
    $mod_strings['LBL_SASA_P7OPORTUNIDADRENTABIL_C'] = '7. Si tuviera la oportunidad de aumentar su rentabilidad asumiendo mayor riesgo, e incluyendo pérdidas potenciales a su inversión inicial ¿qué haría usted?';
    $mod_strings['LBL_SASA_P8SUPONGAINVERSIONESP_C'] = '8. Suponga que sus inversiones pierden un 10% este mes, ¿qué haría usted?';
    $mod_strings['LBL_SASA_CALCULOPERFILRIESGO_C'] = 'Calculo Perfil Riesgo';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.etiqueta_filtro_todos_los_registros.php

$mod_strings['LBL_LISTVIEW_FILTER_ALL'] = 'Todos los Clientes Potenciales';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.afleadscustomfieldsv002-202111180200.php
 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
    $mod_strings['LBL_SASA_VALORINVERSION_C'] = 'Valor Inversión';
    $mod_strings['LBL_SASA_P1RANGOEDADCALIDAD_C'] = '1. ¿En qué rango  de edad o calidad se encuentra?';
    $mod_strings['LBL_SASA_P2PROPOSITOAHORROINVE_C'] = '2. ¿Cuál es el propósito de su ahorro o inversión?';
    $mod_strings['LBL_SASA_P3HORIZONTETIEMPOINVE_C'] = '3. ¿Cuál es el horizonte de tiempo en el que mantendrá su inversión?';
    $mod_strings['LBL_SASA_P4DONDEPROVIENEDINERO_C'] = '4. ¿De dónde proviene el dinero que desea invertir?';
    $mod_strings['LBL_SASA_P5PORCENTAJEACTIVOSAH_C'] = '5. ¿Qué porcentaje del total de sus activos representa este ahorro o inversión?';
    $mod_strings['LBL_SASA_P6EXPERIENCIAINVERSIO_C'] = '6. ¿Ha tenido experiencia en el tema de inversiones?';
    $mod_strings['LBL_SASA_P7OPORTUNIDADRENTABIL_C'] = '7. Si tuviera la oportunidad de aumentar su rentabilidad asumiendo mayor riesgo, e incluyendo pérdidas potenciales a su inversión inicial ¿qué haría usted?';
    $mod_strings['LBL_SASA_P8SUPONGAINVERSIONESP_C'] = '8. Suponga que sus inversiones pierden un 10% este mes, ¿qué haría usted?';
    $mod_strings['LBL_SASA_CALCULOPERFILRIESGO_C'] = 'Calculo Perfil Riesgo';
	$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
	$mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos/Ventas Anuales';
	$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
	$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
	$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre Empresa:';
	$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
	$mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
	$mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
	$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
	$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad de la Oportunidad';
	$mod_strings['LBL_RECORD_SHOWMORE'] = 'Clasificación e Información Financiera';
	$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Otros';
	$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento:';
	$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work';
	$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
	$mod_strings['LBL_FIRST_NAME'] = 'Nombres:';
	$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad de dirección principal:';
	$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
	$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País de dirección principal';
	$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
	$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
	$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
	$mod_strings['LBL_INGRESOS_MENSUALES'] = 'Ingresos Mensuales';
	$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
	$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
	$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Perfil de Riesgo';
	$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
	$mod_strings['LBL_SASA_PAIS_C'] = 'País';
	$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
	$mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
	$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
	$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Visitas';
	$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
	$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
	$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other';
	$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
	$mod_strings['LBL_SASA_INGRESOSMENSUALES_C'] = 'Ingresos Mensuales (eliminar)';
	$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
	$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Empresa';
	$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del Registro';
	$mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico (Eliminar)';
	$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
	$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
	$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3';
	$mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Productos de Interés';
	$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Nombre Empresa';
	$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
	$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
	$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa';
	$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
	$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
	$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
	$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
	$mod_strings['LBL_SASA_ESTADOINFOCP_C'] = 'Información Requerida (eliminar)';
	$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
	$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
	$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
	$mod_strings['LBL_SASA_CTRLTEXTOAYUDA_C'] = 'Tips Importantes';
	$mod_strings['LBL_SASA_CTRLCONVERSION_C'] = 'Ctrl Conversión';
	$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
	$mod_strings['LBL_SASA_CTRLPOSTERGADO_C'] = 'Ctrl Postergado';
	$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
	$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
	$mod_strings['LBL_SASA_NOMBRECOMPLETO_C'] = 'Nombre completo';
	$mod_strings['LBL_SASA_INFOREQUERIDA_C'] = 'Información Requerida*';
	$mod_strings['LBL_SASA_CTRLNOMBREEMPRESA_C'] = 'Nombre Empresa';
	$mod_strings['LBL_SASA_PATRIMONIO'] = 'Patrimonio Reportado';

   


?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos/Ventas Anuales';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre Empresa:';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
$mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad de la Oportunidad';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Clasificación e Información Financiera';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Otros';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de Nacimiento:';
$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
$mod_strings['LBL_FIRST_NAME'] = 'Nombres:';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Ciudad de dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País de dirección principal';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_INGRESOS_MENSUALES'] = 'Ingresos Mensuales';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Perfil de Riesgo';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Visitas';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_INGRESOSMENSUALES_C'] = 'Ingresos Mensuales (eliminar)';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Empresa';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del Registro';
$mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico (Eliminar)';
$mod_strings['LBL_SASA_OCUPACION_C'] = 'Ocupación';
$mod_strings['LBL_SASA_SECTOR_C'] = 'Sector';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3';
$mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Productos de Interés';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Nombre Empresa';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_SASA_ESTADOINFOCP_C'] = 'Información Requerida (eliminar)';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['LBL_SASA_CTRLTEXTOAYUDA_C'] = 'Tips Importantes';
$mod_strings['LBL_SASA_CTRLCONVERSION_C'] = 'Ctrl Conversión';
$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
$mod_strings['LBL_SASA_CTRLPOSTERGADO_C'] = 'Ctrl Postergado';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
$mod_strings['LBL_SASA_NOMBRECOMPLETO_C'] = 'Nombre completo';
$mod_strings['LBL_SASA_INFOREQUERIDA_C'] = 'Información Requerida*';
$mod_strings['LBL_SASA_CTRLNOMBREEMPRESA_C'] = 'Nombre Empresa';
$mod_strings['LBL_SASA_PATRIMONIO_C'] = 'Patrimonio Reportado';

?>
