<?php
// created: 2023-02-06 20:16:31
$dictionary["users_sasa1_unidades_de_negocio_por__1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'users_sasa1_unidades_de_negocio_por__1' => 
    array (
      'lhs_module' => 'Users',
      'lhs_table' => 'users',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa1_Unidades_de_negocio_por_',
      'rhs_table' => 'sasa1_unidades_de_negocio_por_',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'users_sasa1_unidades_de_negocio_por__1_c',
      'join_key_lhs' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
      'join_key_rhs' => 'users_sasa2f7aio_por__idb',
    ),
  ),
  'table' => 'users_sasa1_unidades_de_negocio_por__1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'users_sasa1_unidades_de_negocio_por__1users_ida' => 
    array (
      'name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
      'type' => 'id',
    ),
    'users_sasa2f7aio_por__idb' => 
    array (
      'name' => 'users_sasa2f7aio_por__idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_users_sasa1_unidades_de_negocio_por__1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_users_sasa1_unidades_de_negocio_por__1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'users_sasa1_unidades_de_negocio_por__1users_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_users_sasa1_unidades_de_negocio_por__1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'users_sasa2f7aio_por__idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'users_sasa1_unidades_de_negocio_por__1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'users_sasa2f7aio_por__idb',
      ),
    ),
  ),
);