<?php
 // created: 2018-06-20 17:33:55
$dictionary['RevenueLineItem']['fields']['description']['audited']=false;
$dictionary['RevenueLineItem']['fields']['description']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['description']['comments']='Full text of the note';
$dictionary['RevenueLineItem']['fields']['description']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['description']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['description']['reportable']=false;
$dictionary['RevenueLineItem']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.47',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['description']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['description']['rows']='6';
$dictionary['RevenueLineItem']['fields']['description']['cols']='80';

 ?>