<?php
 // created: 2018-07-26 03:44:13
$dictionary['Task']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Task']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Task']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_superior2_c']['dependency']='';

 ?>