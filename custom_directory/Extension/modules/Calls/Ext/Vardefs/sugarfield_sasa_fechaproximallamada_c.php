<?php
 // created: 2022-06-25 14:58:09
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['labelValue']='Fecha de la Próxima Llamada';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['enforced']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadollamada_c,"Descartado")))';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['required_formula']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['readonly_formula']='';
$dictionary['Call']['fields']['sasa_fechaproximallamada_c']['required']='1';

 ?>