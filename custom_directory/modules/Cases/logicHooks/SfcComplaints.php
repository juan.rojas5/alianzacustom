<?php
    
    class SfcComplaints {

        function sfcComplaintsBeforeSave($bean, $event, $arguments) {

            $GLOBALS['log']->security("\n\n\n");
            $GLOBALS['log']->security("******************************************************");
            $GLOBALS['log']->security("START-LOGICHOOK-SfcCreateComplaints. ".date("Y-m-d h:i:s"));
            $GLOBALS['log']->security("******************************************************");
            
            try {

                $this->mainSfcBeforeSave($bean);

            } catch (Exception $e) {
                if ($e->getMessage() != NULL AND !empty($e->getMessage())) {
                    
                    $bean->sasa_sfc_response_api_c = 
                    "********************" . date("Y-m-d H:i:s", strtotime('-5 hours')) . "********************"
                    ."\nERROR: " .$e->getMessage() 
                    ."\n******************************"
                    ."\n".$bean->sasa_sfc_response_api_c;
                }
            }
            
            $GLOBALS['log']->security("******************************************************");
            $GLOBALS['log']->security("END-LOGICHOOK-SfcCreateComplaints. ".date("Y-m-d h:i:s"));
            $GLOBALS['log']->security("******************************************************");
        }

        private function mainSfcBeforeSave($bean) {
            $typifications = $bean->sasa_sasa_tipificaciones_cases_1->get();
            foreach ($typifications as $id ) {
                $assignedTypificationId = $id;
            }
            //Si la tipificación es Para Momento 1
            if (!empty($assignedTypificationId) AND !empty($bean->status)) {
                //Obteniendo bean de tipificación
                $typificationBean = BeanFactory::getBean('sasa_sasa_tipificaciones', $assignedTypificationId);
                if ($typificationBean->sasa_wf_c) {
                    $accountBean = BeanFactory::getBean('Accounts', $bean->account_id);
                    if (!empty($accountBean->id)) {

                        require_once 'custom/sasa_projects/sfc/SfcRestApi.php';
                        require_once 'custom/sasa_projects/sfc/config/config.php';

                        $this->validateFieldsToCreateComplaintSfc($bean, $typificationBean, $accountBean);

                        if ($bean->status == 10) {
                            // lógica de creación de Queja en SFC momento 2
                            $GLOBALS['log']->security("Caso con perfil para Momento 2");

                            $sfcRestApi = new SfcRestApi($this->createSfcRestApiObjectInstance($bean->sasa_tipoentidad_c), $config);
                            $bean->sasa_codigoqueja_c = $this->getComplaintCode($bean);
                            $complaintArray = $this->createArrayComplaintToSfc($bean, $typificationBean, $accountBean);
                                                        
                            #Enviar complaint 
                            $responseCreateComplaint = $sfcRestApi->postCreateNewComplaintInSfc($complaintArray);
                            $responseCreatecomplaintObj = json_decode($responseCreateComplaint['response']);
                            $GLOBALS['log']->security("responseCreateComplaint: " .$responseCreateComplaint['response'] );
                            
                            if ($responseCreateComplaint['info']['http_code'] != 201 ) {

                                $bean->sasa_sfc_response_api_c =  
                                "********************" . date("Y-m-d H:i:s", strtotime('-5 hours')) . "********************"
                                ."\n".json_encode($responseCreatecomplaintObj->message, JSON_PRETTY_PRINT)
                                ."\n********************"
                                ."\n".$bean->sasa_sfc_response_api_c;
                            } else {
                                $bean->sasa_sfc_response_api_c = 
                                "********************" . date("Y-m-d H:i:s", strtotime('-5 hours')) . "********************"
                                ."\nOK: SFC-MOMENTO 2 -> Queja creada, con código: " . $bean->sasa_codigoqueja_c
                                ."\n********************"
                                ."\n".$bean->sasa_sfc_response_api_c;

                                if ($bean->sasa_anexoqueja_c === "1") {
                                    
                                    # Crear lògica para recolecciòn de adjuntos
                                    $GLOBALS['log']->security("Se debe disparar la lógica para recolección de Notas.");

                                    $bodyArrayFiles = $this->filesFromSugarCrmToSend($bean->id);

                                    $ArrayFiles = $this->validateAttachedNotes($bean->id);

                                    $GLOBALS['log']->security("Número de notas encontradas vacías m2: " .sizeof($ArrayFiles));
                                    $GLOBALS['log']->security("Número de notas encontradas: " .sizeof($bodyArrayFiles));
                                    
                                    if (sizeof($bodyArrayFiles) > 0 && sizeof($ArrayFiles) == 0) {
                    $GLOBALS['log']->security("ENTRO..... ");
                                        $this->sendAttachments($bodyArrayFiles, $bean->sasa_codigoqueja_c, $sfcRestApi);

                                    } elseif(sizeof($ArrayFiles) > 0 || sizeof($bodyArrayFiles) == 0) {
                                        throw new Exception('case:sasa_anexoqueja_c seleccionado pero no se encontraron adjuntos para enviar o tiene notas sin adjuntos.');
                                    }
                                }                               
                            }

                            $GLOBALS['log']->security("SAVE");
                                
                        }

                        //status 4:Cerrada
                        if ($bean->status >= 1 AND $bean->status <= 4) {
                            # lógica de actualización de queja en SFC en momento 3
                            $contactBean = BeanFactory::getBean('Contacts', $bean->primary_contact_id);
                            if ($contactBean->id) {
                                $GLOBALS['log']->security("Caso con perfil para Momento 3");
                                
                                $this->validateFieldsToUpdateComplaintSfc($bean, $contactBean);

                                if ($bean->status == 4) {
                                    $this->validateFilestoUpdate($bean->id);
                                    
                                }  


                                $sfcRestApi = new SfcRestApi($this->createSfcRestApiObjectInstance($bean->sasa_tipoentidad_c), $config);

                                if ($bean->sasa_anexoqueja_c === "1") {
                            
                                    # Crear lògica para recolecciòn de adjuntos
                                    $GLOBALS['log']->security("Se debe disparar la lógica para recolección de Notas.");

                                    $bodyArrayFiles = $this->filesFromSugarCrmToSend($bean->id);

                                    $ArrayFiles = $this->validateAttachedNotes($bean->id);

                                    $GLOBALS['log']->security("Número de notas encontradas vacías: " .sizeof($ArrayFiles));

                                    $GLOBALS['log']->security("Número de notas encontradas: " .sizeof($bodyArrayFiles));
                                    if (sizeof($bodyArrayFiles) > 0 && sizeof($ArrayFiles) == 0) {

                                        $this->sendAttachments($bodyArrayFiles, $bean->sasa_codigoqueja_c, $sfcRestApi);

                                    } elseif(sizeof($ArrayFiles) > 0) {

                                        throw new Exception('case:sasa_anexoqueja_c seleccionado pero no se encontraron adjuntos para enviar 3.');
                                    }
                                }


                                $complaintUpdate = $this->generateComplaintUpdateDto($bean, $typificationBean, $contactBean);
                                $responseUpdateComplaint = $sfcRestApi->updateComplaintInSfc($complaintUpdate, $bean->sasa_codigoqueja_c);
                                $responseUpdateComplaintObj = json_decode($responseUpdateComplaint['response']);
                                $GLOBALS['log']->security("responseUpdateComplaint: " .$responseUpdateComplaint['response'] );

                                if ($responseUpdateComplaint['info']['http_code'] != 200 ) {

                                    $bean->sasa_sfc_response_api_c = 
                                    "\n********************" . date("Y-m-d H:i:s", strtotime('-5 hours')) . "**********"
                                    ."\n".json_encode($responseUpdateComplaintObj->message, JSON_PRETTY_PRINT)
                                    ."\n********************"
                                    ."\n".$bean->sasa_sfc_response_api_c;
                                } else {
                                    $bean->sasa_sfc_response_api_c = 
                                    "********************" . date("Y-m-d H:i:s", strtotime('-5 hours')) . "**********"
                                    ."\nOK: SFC-MOMENTO 3 -> Queja ACTUALIZADA"
                                    ."\n********************"
                                    ."\n".$bean->sasa_sfc_response_api_c;
                                }  
                            } else {
                                throw new Exception('Es requerido tener un contacto principal relacionado.');
                            }
                        }
                    } else {
                            throw new Exception('Es requerido tener una cuenta relacionada.');
                    }
                } else {
                    $GLOBALS['log']->security("Case {$bean->id} doesn't have SFC mark to create Complaint.");
                }
            }
        }
        private function validateFilesToUpdate($caseId) {
            global $db;
            $queryNotesByCaseId = "SELECT child.id, child.filename FROM notes parent 
                                    INNER JOIN notes child ON child.note_parent_id = parent.id 
                                    INNER JOIN notes_cstm child_cstm ON child_cstm.id_c = child.id 
                                    WHERE parent.parent_id = '{$caseId}' 
                                    AND parent.parent_type = 'Cases'
                                    AND child.filename IS NOT NULL  
                                    AND child_cstm.sasa_id_sfc_c IS NULL  
                                    AND parent.deleted <> 1 
                                    AND child.deleted <> 1";
            $resultNotesByCaseId = $db->query($queryNotesByCaseId);
            $lastDbError = $db->lastDbError();
            if(!empty($lastDbError)){
                    $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryNotesByCaseId}");
            } else {
                while($row = $db->fetchByAssoc($resultNotesByCaseId)){
                    if(!empty($row['id'])){
                        $GLOBALS['log']->security("Case: '{$caseId}' has a Note: '{$row['id']}'");
                        if ($this->validateNameExt($row['filename'])) {
                            throw new Exception("ERROR: nombre de archivo incorrecto en noteId '{$row['id']}'.\nEjemplo: filename_RESP_FINAL_SFC.pdf");
                        }
                    } 
                }  
            }
        }

        private function validateNameExt($word) {

            
            $n = strrpos($word, '.');
            $ext =  ($n === false) ? '' : substr($word, $n+1);
            $validate1 = strlen($word) - strpos($word, "RESP_FINAL_SFC.{$ext}"); 
            $validate2 =  strlen("RESP_FINAL_SFC.{$ext}");

            return ($validate1 != $validate2);
        }

        private function sendAttachments($bodyArrayFiles, $complaintId, $sfcRestApi) {
            foreach ($bodyArrayFiles as  $recordFile) {
                $fileToCreate = array(
                    'path' => $recordFile['path'],
                    'codigo_queja' => $complaintId,
                    'type' => $recordFile['type'],
                    'mime' => $recordFile['mime'],
                    'name' => $recordFile['name']
                );
                $beanNote = BeanFactory::getBean('Notes', $recordFile['id']);
                if (!empty($beanNote->id)) {
                    $responseFilesForSfc = $sfcRestApi->uploadFiles($fileToCreate);
                    $responseObj = json_decode($responseFilesForSfc['response']);

                    if ($responseFilesForSfc['info']['http_code'] == 201) {
                        
                        $beanNote->sasa_id_sfc_c = $responseObj->id;
                        $beanNote->description = 'SFC Ok: Nota guardada';
                        $beanNote->save();
                    } else {
                        $beanNote->description = 'SFC error: ' . json_encode($responseObj->message, JSON_PRETTY_PRINT);
                        $beanNote->save();
                    }
                } else {
                    $GLOBALS['log']->security("No se encontró el noteBean asociado a: {$recordFile['id']}" );
                }

                
                $GLOBALS['log']->security("responseFilesForSfc: " . $responseFilesForSfc['response'] );
            }

        }
        private function validateAttachedNotes($caseId){

            $GLOBALS['log']->security("############### FUNCION:######");
            global $db;
            $arrayToSendFile = array();
            $queryNotesByCaseId = "SELECT parent.id as id_note, parent.name, tabla.id_father FROM notes parent
            LEFT JOIN (SELECT child.id, child.filename, child.note_parent_id AS id_father, parent.name FROM notes parent 
                INNER JOIN notes child ON child.note_parent_id = parent.id 
                INNER JOIN notes_cstm child_cstm ON child_cstm.id_c = child.id 
                WHERE parent.parent_id = '{$caseId}' 
                AND parent.parent_type = 'Cases'
                AND parent.deleted <> 1 
                AND child.deleted <> 1) AS tabla ON tabla.id_father = parent.id
                WHERE parent.parent_id = '{$caseId}' 
                    AND parent.parent_type = 'Cases'
                    AND parent.deleted <> 1 
                    AND tabla.id_father IS NULL";
            
            $resultNotesByCaseId = $db->query($queryNotesByCaseId);
            
            $lastDbError = $db->lastDbError();

            if(!empty($lastDbError)){
                    $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryNotesByCaseId}");
            } else {
                while($row = $db->fetchByAssoc($resultNotesByCaseId))
                {
                    $arrayToSendFile[] = array( 
                        'id' => $row['id_note'],  
                    );
                }      
            }
            
            $GLOBALS['log']->security("valideAttachedNotes: Array: ".print_r($arrayToSendFile, TRUE));
            
            return $arrayToSendFile;

        }//metodo valideAttachedNotes
        private function filesFromSugarCrmToSend($caseId) {
            global $db;
            $arrayToSendFile = array();
            $queryNotesByCaseId = "SELECT child.id, child.filename, child.file_ext, child.file_mime_type FROM notes parent 
                                    INNER JOIN notes child ON child.note_parent_id = parent.id 
                                    INNER JOIN notes_cstm child_cstm ON child_cstm.id_c = child.id 
                                    WHERE parent.parent_id = '{$caseId}'  
                                    AND parent.parent_type = 'Cases'  
                                    AND child.filename IS NOT NULL  
                                    AND child_cstm.sasa_id_sfc_c IS NULL 
                                    AND parent.deleted <> 1 
                                    AND child.deleted <> 1";
            $resultNotesByCaseId = $db->query($queryNotesByCaseId);

            $GLOBALS['log']->security($queryNotesByCaseId);

            $lastDbError = $db->lastDbError();
            if(!empty($lastDbError)){
                    $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$queryNotesByCaseId}");
            } else {
                while($row = $db->fetchByAssoc($resultNotesByCaseId)){
                    if(!empty($row['id'])){
                        $GLOBALS['log']->security("Case: '{$caseId}' has a Note: '{$row['id']}'");

                        $file = new UploadFile();
                        //get the file location
                        $file->temp_file_location = UploadFile::realpath(
                            UploadFile::get_upload_path($row['id'])
                        );
                        if (!empty($file->get_file_contents())) {

                            $arrayToSendFile[] = array(
                                'path' => $file->temp_file_location, 
                                'name' => $row['filename'], 
                                'type' =>  $row['file_ext'],
                                'mime' => $row['file_mime_type'],
                                'id' => $row['id'] 
                            );
                        } else {
                            $GLOBALS['log']->security("Nota no encontrada... {$row['id']}");
                        }
                    } 
                }  
            }
            $GLOBALS['log']->security("filesFromSugarCrmToSend: ArrayToSend: ".print_r($arrayToSendFile, TRUE));
            return $arrayToSendFile;
        }

        private function generateComplaintUpdateDto($caseBean, $typificationBean, $contactBean) {

            $updateComplaintArray = array();
            if ($contactBean->sasa_genero_c != NULL AND !empty($contactBean->sasa_genero_c)) {
                $updateComplaintArray['sexo'] = $this->syncDataConf( $contactBean->sasa_genero_c, 'gender');

            }
            if ($contactBean->sasa_lgbtiq_c != NULL AND empty($contactBean->sasa_lgbtiq_c)) {
                $updateComplaintArray['lgbtiq'] = intval($contactBean->sasa_lgbtiq_c);
            }

            if ($contactBean->sasa_condicionespecial_c != NULL AND empty($contactBean->sasa_condicionespecial_c)) {
                $updateComplaintArray['condicion_especial'] = intval($contactBean->sasa_condicionespecial_c);
            }
            
            $updateComplaintArray['canal_cod'] = intval($caseBean->sasa_canal_c);
            $updateComplaintArray['producto_cod'] = intval($caseBean->sasa_producto_cod_c);
            $updateComplaintArray['macro_motivo_cod'] = $this->syncDataConf($typificationBean->id, 'typifications');
            $updateComplaintArray['estado_cod'] = intval($caseBean->status);
            $updateComplaintArray['fecha_actualizacion'] = date('Y-m-d\TH:i:s', strtotime($caseBean->sasa_fechaactualizacionsfc_c . ' - 5 hours'));
            $updateComplaintArray['a_favor_de'] = (empty($caseBean->sasa_favoravilidad_c))? NULL : intval($caseBean->sasa_favoravilidad_c);
            $updateComplaintArray['aceptacion_queja'] = ($caseBean->sasa_recepcion_c != 3)? NULL : intval($caseBean->sasa_aceptacion_c);
            $updateComplaintArray['rectificacion_queja'] = ($caseBean->sasa_recepcion_c != 3)? NULL : intval($caseBean->sasa_rectificacion_c);
            $updateComplaintArray['desistimiento_queja'] = intval($caseBean->sasa_desistimiento_c);
            $updateComplaintArray['prorroga_queja'] = (empty($caseBean->sasa_prorroga_c) OR 
                $caseBean->sasa_prorroga_c == 0 )? NULL : $caseBean->sasa_prorroga_c ;
            $updateComplaintArray['admision'] = (empty($caseBean->sasa_admision_c))? NULL : intval($caseBean->sasa_admision_c);
            $updateComplaintArray['documentacion_rta_final'] = ($caseBean->sasa_docrespfinal_c === "1")? TRUE : FALSE;
            $updateComplaintArray['anexo_queja'] = ($caseBean->sasa_anexoqueja_c === "1")? TRUE : FALSE; 
            $updateComplaintArray['fecha_cierre'] = ($caseBean->status != 4)? NULL : date('Y-m-d\TH:i:s', strtotime($caseBean->resolved_datetime. ' - 5 hours'));
            $updateComplaintArray['tutela'] = $caseBean->sasa_tutela_c;
            $updateComplaintArray['ente_control'] = (empty($caseBean->sasa_ente_control_c))? NULL : intval($caseBean->sasa_ente_control_c);
            $updateComplaintArray['marcacion'] = (empty($caseBean->sasa_marcacion_c))? NULL : intval($caseBean->sasa_marcacion_c);
            $updateComplaintArray['queja_expres'] = intval($caseBean->sasa_quejaexpres_c);
            $updateComplaintArray['producto_digital'] = intval($caseBean->sasa_productodigital_c);

            return $updateComplaintArray;

        }

        private function validateFieldsToUpdateComplaintSfc($caseBean, $contactBean) {
            
            if (empty($caseBean->sasa_fechaactualizacionsfc_c)) {
                throw new Exception('case:sasa_fechaactualizacionsfc_c es requerido');
            }
            if (empty($caseBean->sasa_productodigital_c)) {
                throw new Exception('case:sasa_productodigital_c es requerido');
            }
            if (empty($caseBean->sasa_aceptacion_c)) {
                throw new Exception('case:sasa_aceptacion_c es requerido');
            }
            if (empty($caseBean->sasa_rectificacion_c)) {
                throw new Exception('case:sasa_rectificacion_c es requerido');
            }
            if (empty($caseBean->sasa_desistimiento_c)) {
                throw new Exception('case:sasa_desistimiento_c es requerido');
            }
            if (empty($caseBean->sasa_desistimiento_c)) {
                throw new Exception('case:sasa_desistimiento_c es requerido');
            }
            if (empty($caseBean->sasa_anexoresfinal_c)) {
                throw new Exception('case:sasa_anexoresfinal_c es requerido');
            }
            if (empty($caseBean->sasa_anexoqueja_c)) {
                throw new Exception('case:sasa_anexoqueja_c es requerido');
            }
            if (empty($caseBean->sasa_quejaexpres_c)) {
                throw new Exception('case:sasa_quejaexpres_c es requerido');
            }            
        }

        private function createSfcRestApiObjectInstance($bussinessId) {
            
            $bussiness = '';
            switch ($bussinessId) {
                case 5:
                    $bussiness = 'fiduciaria';
                    break;
                case 85:
                    $bussiness = 'valores';
                    break;
                default:
                    throw new Exception("La entidad seleccionada en case:sasa_tipoentidad_c no es aplicable para Queja SFC: {$bussinessId} to SFC.");
            }

            return $bussiness;
        }

        private function validateFieldsToCreateComplaintSfc($caseBean, $typificationBean, $accountBean ) {
            
            if (empty($caseBean->sasa_tipoentidad_c)) {
                throw new Exception('case:sasa_tipoentidad_c es requerido');
            }
            if (empty($caseBean->sasa_codigoentidad_c)) {
                throw new Exception('case:sasa_codigoentidad_c es requerido');
            }
            if (empty($accountBean->sasa_pais_c)) {
                throw new Exception('account:sasa_pais_c es requerido');
            }
            if (!empty($accountBean->sasa_pais_c) AND $accountBean->sasa_pais_c == 'Colombia') {
                if (empty($accountBean->sasa_departamento_c)) {
                    throw new Exception('account:sasa_departamento_c es requerido');
                }
            }
            
            if (!empty($accountBean->sasa_pais_c) AND $accountBean->sasa_pais_c == 'Colombia') {
                if (empty($accountBean->sasa_municipio_c)) {
                    throw new Exception('account:sasa_municipio_c es requerido');
                }                
            }    

            if (empty($caseBean->sasa_canal_c)) {
                throw new Exception('case:sasa_canal_c es requerido');
            }
            if (empty($caseBean->sasa_producto_cod_c)) {
                throw new Exception('case:sasa_producto_cod_c es requerido');
            }
            if (empty($accountBean->name)) {
                throw new Exception('account:name es requerido');
            }
            if (empty($accountBean->sasa_tipoidentificacion_c)) {
                throw new Exception('account:sasa_tipoidentificacion_c es requerido');
            }
            if (empty($accountBean->sasa_nroidentificacion_c)) {
                throw new Exception('account:sasa_nroidentificacion_c es requerido');
            }
            if (empty($accountBean->sasa_tipopersona_c)) {
                throw new Exception('account:sasa_tipopersona_c es requerido');
            }
            if (empty($caseBean->sasa_recepcion_c)) {
                throw new Exception('case:sasa_recepcion_c es requerido');
            }
            if (empty($caseBean->sasa_puntorecepcion_c)) {
                throw new Exception('case:sasa_puntorecepcion_c es requerido');
            }
            if (empty($caseBean->sasa_admision_c)) {
                throw new Exception('case:sasa_admision_c es requerido');
            }
            if (empty($caseBean->description)) {
                throw new Exception('case:description es requerido');
            }
            if (empty($caseBean->sasa_anexoqueja_c)) {
                throw new Exception('case:sasa_anexoqueja_c es requerido');
            }
            if (empty($caseBean->sasa_tipoentidad_c)) {
                throw new Exception('case:tipo_entidad es requerido');
            }
        }

        private function createArrayComplaintToSfc(&$caseBean, &$typificationBean, &$accountBean ) {

               $arrayComplaint['tipo_entidad'] = intval($caseBean->sasa_tipoentidad_c);
               $arrayComplaint['entidad_cod'] = $caseBean->sasa_codigoentidad_c;
               $arrayComplaint['codigo_queja'] = $caseBean->sasa_codigoqueja_c;
               $arrayComplaint['codigo_pais'] = strval($this->syncDataConf($accountBean->sasa_pais_c, 'countries')); //cuentas-transformación
               $arrayComplaint['departamento_cod'] = (empty($accountBean->sasa_municipio_c)) ? NULL : $this->syncDataConf($accountBean->sasa_departamento_c, 'states');
               $arrayComplaint['municipio_cod'] = (empty($accountBean->sasa_municipio_c)) ? NULL : $this->syncDataConf($accountBean->sasa_municipio_c, 'cities');
               $arrayComplaint['canal_cod'] = intval($caseBean->sasa_canal_c);
               $arrayComplaint['producto_cod'] = intval($caseBean->sasa_producto_cod_c);
               $arrayComplaint['macro_motivo_cod'] = $this->syncDataConf($typificationBean->id, 'typifications'); 
               $arrayComplaint['fecha_creacion'] = date('Y-m-d\TH:i:s', strtotime($caseBean->sasa_sfcfechacreacion_c . ' - 5 hours')); //aplicar formato de fecha SFC
               $arrayComplaint['nombres'] = strtoupper($this->removeAccents($accountBean->name)); //cuentas
               $arrayComplaint['tipo_id_CF'] = $this->syncDataConf($accountBean->sasa_tipoidentificacion_c, 'documentType'); //cuenta-aplicar tr
               $arrayComplaint['numero_id_CF'] = $accountBean->sasa_nroidentificacion_c;
               $arrayComplaint['tipo_persona'] = $this->syncDataConf($accountBean->sasa_tipopersona_c, 'personType');
               $arrayComplaint['insta_recepcion'] = intval($caseBean->sasa_recepcion_c);
               $arrayComplaint['punto_recepcion'] = intval($caseBean->sasa_puntorecepcion_c);
               $arrayComplaint['admision'] = intval($caseBean->sasa_admision_c);
               $arrayComplaint['texto_queja'] = $this->utf8_ansi($this->removeAccents($caseBean->description));
               $arrayComplaint['anexo_queja'] = ($caseBean->sasa_anexoqueja_c === "1")? TRUE : FALSE; 
               $arrayComplaint['ente_control'] = (empty($caseBean->sasa_ente_control_c))? NULL : intval($caseBean->sasa_ente_control_c);

               return  $arrayComplaint;
        }

        private function syncDataConf( $sugarValue, $dataType) { 
            require('custom/sasa_projects/sfc/config/config.php');
            $sfcValue = array_search($sugarValue, $config['sync'][$dataType]);
            if (empty($sfcValue)) {
                throw new Exception("Se deben sincronizar las listas SFC con las de SugarCRM para el tipo: {$dataType} con valorSugar: {$sugarValue}");
            } else {
                return $sfcValue;
            }
        }

        private function getComplaintCode(&$caseBean) {

            $complaintSfcCode = '';
            if (empty($caseBean->sasa_tipoentidad_c) || empty($caseBean->sasa_codigoentidad_c)) {
                throw new Exception('Los campos Tipo o Código de entidad son obligatorios');
            }

            $complaintSfcCode = $caseBean->sasa_tipoentidad_c . $caseBean->sasa_codigoentidad_c;

            if (!empty($caseBean->sasa_radsfc_c)) {
                $complaintSfcCode .= $caseBean->sasa_radsfc_c;
            } else if(!empty($caseBean->sasa_raddefensorc_c)) {
                $complaintSfcCode .= $caseBean->sasa_raddefensorc_c;
            } else if (!empty($caseBean->sasa_radalianza_c)) {
                $complaintSfcCode .= $caseBean->sasa_radalianza_c;
            } else {
                throw new Exception('Debe completar los campos necesarios para generar el código de la queja SFC');
            }

            return $complaintSfcCode;
        }

        private function utf8_ansi($value='') {

            $utf8_ansi2 = array(
            "\u00c0" =>"A",
            "\u00c1" =>"A",
            "\u00c2" =>"A",
            "\u00c3" =>"A",
            "\u00c4" =>"A",
            "\u00c5" =>"A",
            "\u00c6" =>"AE",
            "\u00c7" =>"C",
            "\u00c8" =>"E",
            "\u00c9" =>"E",
            "\u00ca" =>"E",
            "\u00cb" =>"I",
            "\u00cc" =>"I",
            "\u00cd" =>"I",
            "\u00ce" =>"I",
            "\u00cf" =>"I",
            "\u00d1" =>"N",
            "\u00d2" =>"O",
            "\u00d3" =>"O",
            "\u00d4" =>"O",
            "\u00d5" =>"O",
            "\u00d6" =>"O",
            "\u00d8" =>"O",
            "\u00d9" =>"U",
            "\u00da" =>"U",
            "\u00db" =>"U",
            "\u00dc" =>"U",
            "\u00dd" =>"Y",
            "\u00df" =>"B",
            "\u00e0" =>"a",
            "\u00e1" =>"a",
            "\u00e2" =>"a",
            "\u00e3" =>"a",
            "\u00e4" =>"a",
            "\u00e5" =>"a",
            "\u00e6" =>"ae",
            "\u00e7" =>"c",
            "\u00e8" =>"e",
            "\u00e9" =>"e",
            "\u00ea" =>"e",
            "\u00eb" =>"e",
            "\u00ec" =>"i",
            "\u00ed" =>"i",
            "\u00ee" =>"i",
            "\u00ef" =>"i",
            "\u00f0" =>"o",
            "\u00f1" =>"n",
            "\u00f2" =>"o",
            "\u00f3" =>"o",
            "\u00f4" =>"o",
            "\u00f5" =>"o",
            "\u00f6" =>"o",
            "\u00f8" =>"o",
            "\u00f9" =>"u",
            "\u00fa" =>"u",
            "\u00fb" =>"u",
            "\u00fc" =>"u",
            "\u00fd" =>"y",
            "\u00ff" =>"y");

            return strtr($value, $utf8_ansi2);      
        }

        function removeAccents($string){

            //Ahora reemplazamos las letras
            $string = str_replace(
                array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
                array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
                $string
            );

            $string = str_replace(
                array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
                array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
                $string );

            $string = str_replace(
                array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
                array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
                $string );

            $string = str_replace(
                array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
                array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
                $string );

            $string = str_replace(
                array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
                array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
                $string );

            $string = str_replace(
                array('ñ', 'Ñ', 'ç', 'Ç'),
                array('n', 'N', 'c', 'C'),
                $string
            );

            return $string;
        }
    }

?>