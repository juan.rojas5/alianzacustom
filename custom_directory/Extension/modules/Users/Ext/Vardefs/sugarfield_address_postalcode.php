<?php
 // created: 2018-07-11 20:56:55
$dictionary['User']['fields']['address_postalcode']['audited']=false;
$dictionary['User']['fields']['address_postalcode']['massupdate']=false;
$dictionary['User']['fields']['address_postalcode']['duplicate_merge']='enabled';
$dictionary['User']['fields']['address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['address_postalcode']['merge_filter']='disabled';
$dictionary['User']['fields']['address_postalcode']['unified_search']=false;
$dictionary['User']['fields']['address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['address_postalcode']['calculated']=false;

 ?>