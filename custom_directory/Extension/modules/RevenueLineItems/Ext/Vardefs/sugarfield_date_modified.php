<?php
 // created: 2018-06-20 17:35:32
$dictionary['RevenueLineItem']['fields']['date_modified']['audited']=true;
$dictionary['RevenueLineItem']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['RevenueLineItem']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_modified']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_modified']['enable_range_search']='1';

 ?>