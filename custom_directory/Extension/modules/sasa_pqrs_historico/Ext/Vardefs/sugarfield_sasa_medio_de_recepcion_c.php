<?php
 // created: 2022-11-18 20:28:18
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['labelValue']='Medio_de_Recepción';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_medio_de_recepcion_c']['readonly_formula']='';

 ?>