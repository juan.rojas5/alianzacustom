<?php 

    $mod_strings['LBL_SASA_INGRESOSVENTASANUALES_C'] = 'Ingresos y/o Ventas Anuales';
    $mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de Identificación';
    $mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Nro. de Identificación';
    $mod_strings['LBL_SASA_ACTIVIDADECONOMICA_C'] = 'Actividad Económica';
    $mod_strings['LBL_SASA_SECTORECONOMICO_C'] = 'Sector Económico';
    $mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona';
    $mod_strings['LBL_SASA_PERFILRIESGO_C'] = 'Perfil de Riesgo';
    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_NOMBREPRODUCTO_C'] = 'Nombre del Producto';

