({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	vinculaciones: [],
	Username:"**PAI**",
	Password:"**PAI**",
	url:'**PAI**',
	ws:'**PAI**',
	initialize: function(options) {
		var self = this;
		dashlet4 = self;		
		if (typeof app === 'undefined') app = App;		
			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet4_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet4_No_Identificacion").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet4_No_Identificacion").css("border", "none"); 
						$("#dashlet4_No_Identificacion").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet4_No_Identificacion").attr('readonly', true);
					}
					
					$("#dashlet4_consultar").click( function(){
						self.dashlet4_portal();	
					});
					
					$("#dashlet4_consultar").show();
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet4_portal: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet4_portal_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		if (!_.isEmpty( $("#dashlet4_No_Identificacion").val() )){	
			self.dashlet4_consultar();//consultando caso
		}
		else{
			app.alert.dismiss('dashlet4_portal_procesando');
			app.alert.show('error_procesando_id_cliente', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},
	dashlet4_consultar: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.vinculaciones = [];
		$("#dashlet4_tabla").html("");//limpiando	
		if (!_.isEmpty( $("#dashlet4_No_Identificacion").val() )){	
			self.dashlet_bizagi_call();
			if (!_.isEmpty( self.respuesta )){ 						
				console.log('dashlet4_consultar ',self.respuesta);	
				if (typeof self.respuesta.descripcion !== 'undefined'){					
					app.alert.dismiss('dashlet4_portal_procesando');
					app.alert.dismiss('info_crear');
					app.alert.show('info_crear', {
						level: 'info',
						title: 'Consultado',
						messages: self.respuesta.descripcion,
						autoClose: true,
					});		
					if (typeof self.respuesta.vinculacionResponse !== 'undefined'){	
						self.vinculaciones = self.respuesta.vinculacionResponse;
						if(!$.isArray( self.vinculaciones )){			
							var temp = self.vinculaciones;
							self.vinculaciones = [];
							self.vinculaciones.push(temp);
						}						
						$.each(self.vinculaciones, function( i, vinculacion ) {
							var html = "<tr><td>--</td><td>--</td><td>--</td><td>--</td><td style='display:none'>--</td></tr>";
							if (!_.isEmpty( vinculacion )){	
								var date_time = vinculacion.fechaApertura.split("T");
								html = "<tr>";
								html += "<td>"+vinculacion.numeroCaso+"</td>";
								html += "<td>"+date_time[0]+"</td>";
								html += "<td>"+vinculacion.estadoVinculacion+"</td>";
								html += "<td>"+vinculacion.rechazoVinculacion+"</td>";
								html += "<td style='display:none'>--???????--</td>";
								html += "</tr>";		
							}		
							$("#dashlet4_tabla").append(html);
						});
						if (_.isEmpty( $("#dashlet4_tabla").html() )){		
							$("#dashlet4_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td style='display:none'>--</td></tr>");//limpiando				
							app.alert.dismiss('dashlet4_portal_procesando');
							app.alert.dismiss('error_crear');
							app.alert.show('error_crear', {
								level: 'error',
								title: 'Error al crear por WS2:<p>'+JSON.stringify(self.respuesta,null,'\t'),
								autoClose: false,
							});
						}
					}
				}
				else{		
					$("#dashlet4_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><tdstyle='display:none'>--</td></tr>");//limpiando				
					app.alert.dismiss('dashlet4_portal_procesando');
					app.alert.dismiss('error_crear');
					app.alert.show('error_crear', {
						level: 'error',
						title: 'Error al crear por WS1:<p>'+JSON.stringify(self.respuesta,null,'\t'),
						autoClose: false,
					});
				}
			}
			else{		
				$("#dashlet4_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando					
				app.alert.dismiss('dashlet4_portal_procesando');
				app.alert.dismiss('error_crear');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Error al crear por WS0:<p>'+JSON.stringify(self.respuesta,null,'\t'),
					autoClose: false,
				});
			}
		}
		else{
			$("#dashlet4_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando			
			app.alert.dismiss('dashlet4_portal_procesando');
			app.alert.dismiss('error_crear');
			app.alert.show('error_crear', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},
	dashlet_bizagi_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		var numeroDocumento = $("#dashlet4_No_Identificacion").val();	
		var method  = "dashlet_bizagi";		
		var data = [
			{
				'url':self.url,
				'ws':self.ws,
				'tipo':'POST',
				'Username':self.Username,
				'Password':self.Password,
			},
			{		
				'identificacion':$("#dashlet4_No_Identificacion").val(),
				'numeroCaso':'',
				
				'xmlns:soapenv':'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"',
				'xmlns:web':'xmlns:web="http://web.ws.sqdm.com/"',
				'web':'listarVinculacion',
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_bizagi_procesando');
				
				if (typeof response !== 'undefined')
				if (typeof response.descripcion !== 'undefined')
				if (response.descripcion == 'Error de autenticación. Por favor valide sus credenciales')
				app.alert.show('error_dashlet4_autenticacion', {
					level: 'error',
					title: 'Bizagi4: '+response.descripcion,
					autoClose: false,
				});
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_bizagi_procesando');
				app.alert.dismiss('error_procesando_pedido');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
})
