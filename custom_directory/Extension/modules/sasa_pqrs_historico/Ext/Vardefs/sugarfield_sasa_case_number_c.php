<?php
 // created: 2022-11-18 19:36:52
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['labelValue']='Case_Number';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_case_number_c']['readonly_formula']='';

 ?>