<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-02-06 20:21:32
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_purchases_1',
  ),
);

// created: 2019-01-04 21:27:42
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  ),
);

// created: 2019-01-04 21:28:46
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasa_movimientosav_1',
  ),
);

// created: 2019-01-04 21:30:05
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  ),
);

// created: 2019-01-04 21:25:02
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasa_saldosaf_1',
  ),
);

// created: 2019-01-04 21:26:58
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasa_saldosav_1',
  ),
);

// created: 2019-03-08 18:57:52
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'context' => 
  array (
    'link' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  ),
);

//auto-generated file DO NOT EDIT
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  'view' => 'subpanel-for-sasa_productosafav-sasa_productosafav_sasa_movimientosaf_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'sasa_productosafav_sasa_movimientosav_1',
  'view' => 'subpanel-for-sasa_productosafav-sasa_productosafav_sasa_movimientosav_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'view' => 'subpanel-for-sasa_productosafav-sasa_productosafav_sasa_movimientosavdivisas_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'sasa_productosafav_sasa_saldosaf_1',
  'view' => 'subpanel-for-sasa_productosafav-sasa_productosafav_sasa_saldosaf_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['sasa_ProductosAFAV']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'sasa_productosafav_sasa_saldosav_1',
  'view' => 'subpanel-for-sasa_productosafav-sasa_productosafav_sasa_saldosav_1',
);
