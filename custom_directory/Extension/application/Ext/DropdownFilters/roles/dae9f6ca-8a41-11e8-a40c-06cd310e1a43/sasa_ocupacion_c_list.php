<?php
// created: 2018-08-11 23:24:54
$role_dropdown_filters['sasa_ocupacion_c_list'] = array (
  '' => true,
  'Asalariado' => true,
  'Comercial' => true,
  'Construccion' => true,
  'Empleado' => true,
  'Estudiante' => true,
  'Financiero' => true,
  'Hogar' => true,
  'Independiente' => true,
  'Industrial' => true,
  'Militar' => true,
  'Pensionado' => true,
  'Religioso' => true,
  'Rentista' => true,
  'Servicios' => true,
  'Socio' => true,
  'Transporte' => true,
  'Otros' => true,
  'No Registra' => false,
);