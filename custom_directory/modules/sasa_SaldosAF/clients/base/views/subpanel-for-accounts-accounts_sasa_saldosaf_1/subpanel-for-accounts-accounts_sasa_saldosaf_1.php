<?php
// created: 2019-01-04 21:52:59
$viewdefs['sasa_SaldosAF']['base']['view']['subpanel-for-accounts-accounts_sasa_saldosaf_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_productosafav_sasa_saldosaf_1_name',
          'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
          'enabled' => true,
          'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1SASA_PRODUCTOSAFAV_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_nombrefondo_c',
          'label' => 'LBL_SASA_NOMBREFONDO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_nombresubcuenta_c',
          'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_encargo_c',
          'label' => 'LBL_SASA_ENCARGO_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_inversiones_c',
          'label' => 'LBL_SASA_INVERSIONES_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_digito_c',
          'label' => 'LBL_SASA_DIGITO_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_plan_c',
          'label' => 'LBL_SASA_PLAN_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_numerounid_c',
          'label' => 'LBL_SASA_NUMEROUNID_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_valorunid_c',
          'label' => 'LBL_SASA_VALORUNID_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'sasa_saldoactual_c',
          'label' => 'LBL_SASA_SALDOACTUAL_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'sasa_fechasaldo_c',
          'label' => 'LBL_SASA_FECHASALDO_C',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'sasa_ingresosdia_c',
          'label' => 'LBL_SASA_INGRESOSDIA_C',
          'enabled' => true,
          'default' => true,
        ),
        14 => 
        array (
          'name' => 'sasa_egresosdia_c',
          'label' => 'LBL_SASA_EGRESOSDIA_C',
          'enabled' => true,
          'default' => true,
        ),
        15 => 
        array (
          'name' => 'sasa_retcont_c',
          'label' => 'LBL_SASA_RETCONT_C',
          'enabled' => true,
          'default' => true,
        ),
        16 => 
        array (
          'name' => 'sasa_retrend_c',
          'label' => 'LBL_SASA_RETREND_C',
          'enabled' => true,
          'default' => true,
        ),
        17 => 
        array (
          'name' => 'sasa_canje_c',
          'label' => 'LBL_SASA_CANJE_C',
          'enabled' => true,
          'default' => true,
        ),
        18 => 
        array (
          'name' => 'sasa_ctrlcancel_c',
          'label' => 'LBL_SASA_CTRLCANCEL_C',
          'enabled' => true,
          'default' => true,
        ),
        19 => 
        array (
          'name' => 'sasa_reservado_c',
          'label' => 'LBL_SASA_RESERVADO_C',
          'enabled' => true,
          'default' => true,
        ),
        20 => 
        array (
          'name' => 'sasa_4x1000_c',
          'label' => 'LBL_SASA_4X1000_C',
          'enabled' => true,
          'default' => true,
        ),
        21 => 
        array (
          'name' => 'sasa_sinconsol_c',
          'label' => 'LBL_SASA_SINCONSOL_C',
          'enabled' => true,
          'default' => true,
        ),
        22 => 
        array (
          'name' => 'sasa_maximoretiro_c',
          'label' => 'LBL_SASA_MAXIMORETIRO_C',
          'enabled' => true,
          'default' => true,
        ),
        23 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'MODIFIED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        24 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);