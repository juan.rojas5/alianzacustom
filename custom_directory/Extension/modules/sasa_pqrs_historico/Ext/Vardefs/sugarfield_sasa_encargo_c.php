<?php
 // created: 2022-11-18 20:45:23
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['labelValue']='Encargo';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_encargo_c']['readonly_formula']='';

 ?>