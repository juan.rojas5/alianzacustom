<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/sugarfield_sasa_nombrefondo_c.php

 // created: 2018-11-21 21:59:16
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['importable']='false';
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['calculated']='true';
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['formula']='related($sasa_productosafav_sasa_movimientosaf_1,"sasa_producto_c")';
$dictionary['sasa_MovimientosAF']['fields']['sasa_nombrefondo_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2018-11-21 21:58:41
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosaf_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAF']['fields']['sasa_categoria_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/sugarfield_name.php

 // created: 2018-11-21 21:52:02
$dictionary['sasa_MovimientosAF']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAF']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAF']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAF']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAF']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAF']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAF']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAF']['fields']['name']['formula']='$sasa_encargo_c';
$dictionary['sasa_MovimientosAF']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/sasa_productosafav_sasa_movimientosaf_1_sasa_MovimientosAF.php

// created: 2019-01-04 21:27:42
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAF"]["fields"]["sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE_ID',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/accounts_sasa_movimientosaf_1_sasa_MovimientosAF.php

// created: 2019-01-04 21:39:25
$dictionary["sasa_MovimientosAF"]["fields"]["accounts_sasa_movimientosaf_1"] = array (
  'name' => 'accounts_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'id_name' => 'accounts_sasa_movimientosaf_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAF"]["fields"]["accounts_sasa_movimientosaf_1_name"] = array (
  'name' => 'accounts_sasa_movimientosaf_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_movimientosaf_1accounts_ida',
  'link' => 'accounts_sasa_movimientosaf_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAF"]["fields"]["accounts_sasa_movimientosaf_1accounts_ida"] = array (
  'name' => 'accounts_sasa_movimientosaf_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE_ID',
  'id_name' => 'accounts_sasa_movimientosaf_1accounts_ida',
  'link' => 'accounts_sasa_movimientosaf_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAF/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_MovimientosAF']['full_text_search']=true;

?>
