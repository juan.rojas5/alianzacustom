<?php

use function PHPSTORM_META\type;

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AlianzaCoreApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'CreateSale' => array(
                'reqType' => array('POST'), //request type

                'noLoginRequired' => true, //set authentication

                'path' => array('core-system', '<module>'), //endpoint path

                'pathVars' => array('', 'module'), //endpoint variables
                
                'method' => 'createRecords',//method to call

                'shortHelp' => 'An api to create sales', //short help string to be displayed in the help documentation

                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_CreateSale_help.html', //long help to be displayed in the help documentation
            ),
            'UpdateSale' => array(
                'reqType' => array('PUT'), //request type

                'noLoginRequired' => true, //set authentication

                'path' => array('core-system', '<module>'), //endpoint path

                'pathVars' => array('', 'module'), //endpoint variables
                
                'method' => 'updateRecords',//method to call

                'shortHelp' => 'An api to create sales', //short help string to be displayed in the help documentation

                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_CreateSale_help.html', //long help to be displayed in the help documentation
            ),
        );
    }

    // ========== Inicio proceso de creación en Alianza Core ========== 
        public function createRecords($api, $args)
        {
            $GLOBALS['log']->security("==============================================================================");
            $GLOBALS['log']->security("Inicio proceso de creación en Alianza Core ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("------------------------------------------------------------------------------");

            if( ( count($args['unidades_negocios']) > 0 && $args['module'] == 'sasa2_Unidades_de_negocio' ) || ( count($args['unidades_negocios_por_']) > 0 && $args['module'] == 'sasa1_Unidades_de_negocio_por_' ) || ( count($args['productos_de_cliente']) > 0 && $args['module'] == 'Purchases' ) )
            {
                require_once("custom/modules/sasa2_Unidades_de_negocio/sasa_clases/Unidades_de_Negocio_Master_API.php");
                $obj = new Unidades_de_Negocio_Master_API;
                
                $return['module'] = $args['module'];

                switch ($args['module']) 
                {
                    case 'sasa2_Unidades_de_negocio':

                        $return = $obj->crearRegistros($args['unidades_negocios'], $args['module'], $return);
                        break;
                    
                    case 'sasa1_Unidades_de_negocio_por_':
                        
                        $return = $obj->crearRegistros($args['unidades_negocios_por_'], $args['module'], $return);
                        break;

                    case 'Purchases':

                        $return = $obj->crearRegistros($args['productos_de_cliente'], $args['module'], $return);
                        break;

                    default:

                        $return['response']['message'] = "No hay una API para el modulo {$args['module']}"; 
                        break;
                }
            } else {
                $return = $this->generateMessage($args, $return);
            }
            
            $GLOBALS['log']->security("------------------------------------------------------------------------------");
            $GLOBALS['log']->security("Finalizo proceso de creación en Alianza Core ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("==============================================================================");
            
            return $return;
        }
    // ========== Fin proceso de creación en Alianza Core ============= 

    // ========== Inicio proceso de actualización en Alianza Core ========== 
        public function updateRecords($api, $args)
        {
            $GLOBALS['log']->security("==============================================================================");
            $GLOBALS['log']->security("Inicio proceso de actualización en Alianza Core ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("------------------------------------------------------------------------------");

            if( ( count($args['unidades_negocios']) > 0 && $args['module'] == 'sasa2_Unidades_de_negocio' ) || ( count($args['unidades_negocios_por_']) > 0 && $args['module'] == 'sasa1_Unidades_de_negocio_por_' ) || ( count($args['productos_de_cliente']) > 0 && $args['module'] == 'Purchases' ) )
            {
                // return $args;
                require_once("custom/modules/sasa2_Unidades_de_negocio/sasa_clases/Unidades_de_Negocio_Master_API.php");
                $obj = new Unidades_de_Negocio_Master_API;

                $return['module'] = $args['module'];

                switch ($args['module']) 
                {
                    case 'sasa2_Unidades_de_negocio':
                        
                        $return =$obj->actualizarRegistros($args['unidades_negocios'], $args['module'], $return);
                        break;
                    
                    case 'sasa1_Unidades_de_negocio_por_':
                        
                        $return = $obj->actualizarRegistros($args['unidades_negocios_por_'], $args['module'], $return);
                        break;

                    case 'Purchases':

                        $return = $obj->actualizarRegistros($args['productos_de_cliente'], $args['module'], $return);
                        break;

                    default:

                        $return['response']['message'] = "No hay una API para el modulo {$args['module']}"; 
                        break;
                }   
            } else {
                $return = $this->generateMessage($args, $return);
            }
            
            $GLOBALS['log']->security("------------------------------------------------------------------------------");
            $GLOBALS['log']->security("Finalizo proceso de actualización en Alianza Core ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("==============================================================================");
            
            return $return;
        }
    // ========== Fin proceso de actualización en Alianza Core =============

    // ========== Inicia de Generador de Mensajes ========== 
        private function generateMessage($args, $return)
        {
            $module = $args['module'];
            $unidadNegocio = 'sasa2_Unidades_de_negocio';
            $unidadNegocioPor = 'sasa1_Unidades_de_negocio_por_';
            $productoDeCliente = 'Purchases';

            if( array_key_exists('unidades_negocios', $args) && count($args['unidades_negocios']) < 1 && $module == $unidadNegocio )
            {
                $return['response']['message'] = 'El objeto unidades_negocios enviado no tiene elementos asociados.';
                $return['response']['object_name'] = 'unidades_negocios';
            } 
            else if( array_key_exists('unidades_negocios_por_', $args) && count($args['unidades_negocios_por_']) < 1 && $module == $unidadNegocioPor )
            {
                $return['response']['message'] = 'El objeto unidades_negocios_por_ enviado no tiene elementos asociados.';
                $return['response']['object_name'] = 'unidades_negocios_por_';

            }
            else if( array_key_exists('productos_de_cliente', $args) && count($args['productos_de_cliente']) < 1 && $module == $productoDeCliente ) 
            {
                $return['response']['message'] = 'El objeto productos_de_cliente enviado no tiene elementos asociados.';
                $return['response']['object_name'] = 'productos_de_cliente';
            }
            else 
            {
                $return['response']['message'] = "No concuerda el objeto enviado a " . $module;
            }
            
            $return['response']['module'] = $args['module'];
            return $return;
        }
    // ========== Finaliza Generador de Mensajes ==========
}