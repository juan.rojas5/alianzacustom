<?php

$hook_array['before_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'leads_before_save',

  //The PHP file where your class is located.
  'custom/modules/Leads/leads_before_save.php',

  //The class the method is in.
  'leads_before_save',

  //The method to call.
  'before_save'
);

?>
