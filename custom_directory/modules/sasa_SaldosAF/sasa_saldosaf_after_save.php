<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sasa_saldosaf_after_save_class
{
  function after_save($bean, $event, $arguments)
  {
    global $current_user, $db;
    
    //loop prevention check
    if (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false)
    {
      $bean->ignore_update_c = true;
      
      $ProductoAFAV = BeanFactory::retrieveBean('sasa_ProductosAFAV', $bean->sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida);
      $Account = BeanFactory::retrieveBean('Accounts', $bean->accounts_sasa_saldosaf_1accounts_ida);
      $idProducto = $ProductoAFAV->id;
      $idAccount = $Account->id;
      $idUserAccount = $Account->assigned_user_id;
      
      $yearSaldo = date("Y", strtotime($bean->sasa_fechasaldo_c));
      $monthSaldo = date("m", strtotime($bean->sasa_fechasaldo_c));
      
/****Old*****
      $query = "
SELECT
    AVG(saldo_actual) AS saldoActual,
    nameAccounts,
    idpxp
FROM
    (
    SELECT
        users.id AS idUsuarioCuenta,
        sasa_productosafav.id AS idProducto,
        users.user_name AS usuarioCuenta,
        sasa_productosafav.name AS producto,
        sasa_saldosaf.name AS namesaldoaf,
        accounts.name AS nameAccounts,
        sasap_presupuestoxproducto.id AS idpxp,
        SUBSTRING_INDEX(
            GROUP_CONCAT(
                sasa_saldosaf.sasa_saldoactual_c
            ORDER BY
                sasa_saldosaf.sasa_fechasaldo_c
            DESC
            ),
            ',',
            1
        ) AS saldo_actual,
        SUBSTRING_INDEX(
            GROUP_CONCAT(
                sasa_saldosaf.sasa_fechasaldo_c
            ORDER BY
                sasa_saldosaf.sasa_fechasaldo_c
            DESC
            ),
            ',',
            1
        ) AS fecha_saldo
    FROM
        sasa_saldosaf
    LEFT JOIN accounts_sasa_saldosaf_1_c ON sasa_saldosaf.id = accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb
    LEFT JOIN accounts ON accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida = accounts.id
    LEFT JOIN users ON users.id = accounts.assigned_user_id
    LEFT JOIN sasa_productosafav_sasa_saldosaf_1_c ON sasa_saldosaf.id = sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb
    LEFT JOIN sasa_productosafav ON sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida = sasa_productosafav.id
    LEFT JOIN sasa_productosafav_sasap_presupuestoxproducto_1_c ON sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produff64tosafav_ida = sasa_productosafav.id
    LEFT JOIN sasap_presupuestoxproducto ON sasap_presupuestoxproducto.id = sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produ6f56roducto_idb
    LEFT JOIN sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c ON sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres4d00roducto_idb = sasap_presupuestoxproducto.id
    LEFT JOIN sasap_presupuestosxasesor ON sasap_presupuestosxasesor.id = sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres107bxasesor_ida
    
    WHERE
        sasa_saldosaf.deleted = 0 AND
        accounts_sasa_saldosaf_1_c.deleted = 0 AND
        sasa_productosafav_sasa_saldosaf_1_c.deleted = 0 AND
        accounts.deleted = 0 AND
        sasap_presupuestoxproducto.deleted = 0 AND
        sasa_productosafav.deleted = 0 AND
        
        sasa_saldosaf.sasa_fechasaldo_c LIKE '{$yearSaldo}-{$monthSaldo}%' AND
        sasa_productosafav.id = '{$idProducto}' AND
        users.id = '{$idUserAccount}' AND
        sasap_presupuestoxproducto.assigned_user_id = '{$idUserAccount}' AND
        sasap_presupuestosxasesor.sasa_year_c = '{$yearSaldo}' AND
        sasap_presupuestosxasesor.sasa_mes_c = '{$monthSaldo}'
        
    GROUP BY
        namesaldoaf
    ORDER BY
        idUsuarioCuenta,
        idProducto,
        namesaldoaf
    DESC
) tbl_saldos GROUP BY idUsuarioCuenta, idProducto;
      ";

***Old***/
      
      $query = "
        SELECT
            idpxp,
            SUM(saldoActual) AS saldoActual
        FROM
            (
            SELECT
                sasa_saldosaf.id,
                sasa_saldosaf.name AS nameSaldo,
                sasa_saldosaf.sasa_saldoactual_c,
                sasa_saldosaf.sasa_fechasaldo_c,
                accounts.name AS Cuenta,
                users.user_name AS Usuario,
                sasa_productosafav.name AS Producto,
                sasap_presupuestoxproducto.id AS idpxp,
                AVG(sasa_saldoactual_c) AS saldoActual
            FROM
                sasa_saldosaf
            LEFT JOIN accounts_sasa_saldosaf_1_c ON accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
            LEFT JOIN accounts ON accounts.id = accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida
            LEFT JOIN users ON users.id = accounts.assigned_user_id
            LEFT JOIN sasa_productosafav_sasa_saldosaf_1_c ON sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
            LEFT JOIN sasa_productosafav ON sasa_productosafav.id = sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida
            LEFT JOIN sasa_productosafav_sasap_presupuestoxproducto_1_c ON sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produff64tosafav_ida = sasa_productosafav.id
            LEFT JOIN sasap_presupuestoxproducto ON sasap_presupuestoxproducto.id = sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produ6f56roducto_idb
            LEFT JOIN sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c ON sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres4d00roducto_idb = sasap_presupuestoxproducto.id
            LEFT JOIN sasap_presupuestosxasesor ON sasap_presupuestosxasesor.id = sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres107bxasesor_ida
            WHERE
                sasa_saldosaf.deleted = 0 AND
                accounts_sasa_saldosaf_1_c.deleted = 0 AND
                accounts.deleted = 0 AND
                users.deleted = 0 AND
                sasa_productosafav_sasa_saldosaf_1_c.deleted = 0 AND
                sasa_productosafav.deleted = 0 AND
                sasa_productosafav_sasap_presupuestoxproducto_1_c.deleted = 0 AND
                sasap_presupuestoxproducto.deleted = 0 AND
                sasa_saldosaf.sasa_fechasaldo_c LIKE '{$yearSaldo}-{$monthSaldo}%' AND
                sasa_productosafav.id = '{$idProducto}' AND
                users.id = '{$idUserAccount}' AND
                sasap_presupuestoxproducto.assigned_user_id = '{$idUserAccount}' AND
                sasap_presupuestosxasesor.sasa_year_c = '{$yearSaldo}' AND
                sasap_presupuestosxasesor.sasa_mes_c = '{$monthSaldo}'
            GROUP BY
                sasa_saldosaf.name
        ) tbl_saldos;
      ";
      
      $result = $db->query($query);
      
      while($row = $db->fetchByAssoc($result) )
      {
        $idPxP = $row['idpxp'];
        $saldoActual = $row['saldoActual'];
        //$nameAccounts = $row['nameAccounts'];
      }
      
      if (!empty($idPxP))
      {
        $PresupuestoxProducto = BeanFactory::retrieveBean('sasaP_PresupuestoxProducto', $idPxP);
        //$PresupuestoxProducto->description = $nameAccounts." | ".$query;
        $PresupuestoxProducto->sasa_saldoconsolidado_c = $saldoActual;
        $PresupuestoxProducto->save();
      }
      
    }
    
  }
}
