<?php
 // created: 2018-09-10 22:12:07
$dictionary['Call']['fields']['date_end']['audited']=true;
$dictionary['Call']['fields']['date_end']['comments']='Date is which call is scheduled to (or did) end';
$dictionary['Call']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_end']['calculated']=false;
$dictionary['Call']['fields']['date_end']['enable_range_search']='1';
$dictionary['Call']['fields']['date_end']['required']=true;
$dictionary['Call']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Call']['fields']['date_end']['group_label']='';

 ?>