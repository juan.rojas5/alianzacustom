<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_temp_accounts_canastas';
function tarea_temp_accounts_canastas(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_temp_accounts_canastas. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;
    
    /******Saldos AF******/    
    $queryaccounts_canastas = "		
  	SELECT IFNULL(accounts.id,'') primaryid
      ,IFNULL(accounts.name,'') accounts_name
      ,IFNULL(l1.id,'') l1_id
      ,IFNULL(l1.first_name,'') l1_first_name
      ,IFNULL(l1.last_name,'') l1_last_name
      ,IFNULL(l1.title,'') l1_title
      FROM accounts
      LEFT JOIN  users l1 ON accounts.assigned_user_id=l1.id AND l1.deleted=0
      LEFT JOIN  teams l2 ON accounts.team_id=l2.id AND l2.deleted=0
       WHERE (((((l1.description = 'Canastas.'))) AND (((l2.id<>'449f5dc4-43a1-11eb-8457-0286beac7abe'
      ) AND (l2.id<>'4dda40a2-43a1-11eb-b1d6-069273903c1e'
      ) AND (l2.id<>'57124e62-43a1-11eb-a3b5-069273903c1e'
      ) AND (l2.id<>'611f0666-43a1-11eb-973b-0286beac7abe'
      ))))) 
      AND  accounts.deleted=0 ";
    
    $result = $db->query($queryaccounts_canastas);
    while($row = $db->fetchRow($result)){
      $bean = BeanFactory::getBean("Accounts", $row['primaryid'], array('disable_row_level_security' => true));
      $bean->sasa_retrasoctrlworkflow_c="3";
      $bean->account_type="Customer";
      $bean->save();
    }
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_temp_accounts_canastas. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
