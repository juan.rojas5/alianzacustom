<?php
// created: 2021-01-14 23:21:08
$viewdefs['Accounts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'account_type' => 
    array (
    ),
    'sasa_tipopersona_c' => 
    array (
    ),
    'sasa_tipoidentificacion_c' => 
    array (
    ),
    'sasa_nroidentificacion_c' => 
    array (
    ),
    'industry' => 
    array (
    ),
    'ownership' => 
    array (
    ),
    'sasa_actividadeconomica_c' => 
    array (
    ),
    'billing_address_city' => 
    array (
    ),
    'billing_address_state' => 
    array (
    ),
    'billing_address_country' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'sasa_superior1_c' => 
    array (
    ),
    'sasa_superior2_c' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'sasa_ctrlfiltro_c' => 
    array (
    ),
  ),
);