<?php

$hook_array['after_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'AlianzaCasesAfterSave',

  //The PHP file where your class is located.
  'custom/modules/Cases/AlianzaCasesAfterSave.php',

  //The class the method is in.
  'after_save',

  //The method to call.
  'after_save'
);

?>