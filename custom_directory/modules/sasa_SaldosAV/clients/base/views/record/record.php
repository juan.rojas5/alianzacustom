<?php
$module_name = 'sasa_SaldosAV';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'sasa_SaldosAV',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'accounts_sasa_saldosav_1_name',
              ),
              1 => 
              array (
                'name' => 'sasa_productosafav_sasa_saldosav_1_name',
              ),
              2 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              3 => 
              array (
                'name' => 'sasa_nombreproducto_c',
                'label' => 'LBL_SASA_NOMBREPRODUCTO_C',
              ),
              4 => 
              array (
                'name' => 'sasa_nemotecnico_c',
                'label' => 'LBL_SASA_NEMOTECNICO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_emisor_c',
                'label' => 'LBL_SASA_EMISOR_C',
              ),
              6 => 
              array (
                'name' => 'sasa_isinespecie_c',
                'label' => 'LBL_SASA_ISINESPECIE_C',
              ),
              7 => 
              array (
                'name' => 'sasa_numeroacciones_c',
                'label' => 'LBL_SASA_NUMEROACCIONES_C',
              ),
              8 => 
              array (
                'name' => 'sasa_tipotitulo_c',
                'label' => 'LBL_SASA_TIPOTITULO_C',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'sasa_fechaemision_c',
                'label' => 'LBL_SASA_FECHAEMISION_C',
              ),
              11 => 
              array (
                'name' => 'sasa_fechavencimiento_c',
                'label' => 'LBL_SASA_FECHAVENCIMIENTO_C',
              ),
              12 => 
              array (
                'name' => 'sasa_tasafacial_c',
                'label' => 'LBL_SASA_TASAFACIAL_C',
              ),
              13 => 
              array (
                'name' => 'sasa_tipooperacion_c',
                'label' => 'LBL_SASA_TIPOOPERACION_C',
              ),
              14 => 
              array (
                'name' => 'sasa_nominal_c',
                'label' => 'LBL_SASA_NOMINAL_C',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'sasa_valorcompra_c',
                'label' => 'LBL_SASA_VALORCOMPRA_C',
              ),
              17 => 
              array (
                'name' => 'sasa_tirprecio_c',
                'label' => 'LBL_SASA_TIRPRECIO_C',
              ),
              18 => 
              array (
                'name' => 'sasa_valormercado_c',
                'label' => 'LBL_SASA_VALORMERCADO_C',
              ),
              19 => 
              array (
                'name' => 'sasa_tirvpn_c',
                'label' => 'LBL_SASA_TIRVPN_C',
              ),
              20 => 
              array (
                'name' => 'sasa_preciovpn_c',
                'label' => 'LBL_SASA_PRECIOVPN_C',
              ),
              21 => 
              array (
              ),
              22 => 
              array (
                'name' => 'sasa_disponiblenominal_c',
                'label' => 'LBL_SASA_DISPONIBLENOMINAL_C',
              ),
              23 => 
              array (
                'name' => 'sasa_disponiblevlrmercado_c',
                'label' => 'LBL_SASA_DISPONIBLEVLRMERCADO_C',
              ),
              24 => 
              array (
                'name' => 'sasa_garantianominal_c',
                'label' => 'LBL_SASA_GARANTIANOMINAL_C',
              ),
              25 => 
              array (
                'name' => 'sasa_garantiavlrmercado_c',
                'label' => 'LBL_SASA_GARANTIAVLRMERCADO_C',
              ),
              26 => 
              array (
                'name' => 'sasa_valorinicial_c',
                'label' => 'LBL_SASA_VALORINICIAL_C',
              ),
              27 => 
              array (
                'name' => 'sasa_valorfinal_c',
                'label' => 'LBL_SASA_VALORFINAL_C',
              ),
              28 => 
              array (
                'name' => 'sasa_fecharegistro_c',
                'label' => 'LBL_SASA_FECHAREGISTRO_C',
              ),
              29 => 
              array (
                'name' => 'sasa_fechacumplimiento_c',
                'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
              ),
              30 => 
              array (
                'name' => 'sasa_saldototal_c',
                'label' => 'LBL_SASA_SALDOTOTAL_C',
                'span' => 6,
              ),
              31 => 
              array (
                'span' => 6,
              ),
              32 => 
              array (
                'name' => 'description',
                'span' => 6,
              ),
              33 => 
              array (
                'span' => 6,
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'tag',
                'span' => 12,
              ),
              1 => 'assigned_user_name',
              2 => 'team_name',
              3 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              4 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
