<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class mover_actividades_asignado_ceutnas
{
	function before_save($bean, $event, $arguments)
	{
		try{
			/*
			* Caso sasa 5361
			* Se requiere en la instancia de Alianza, en el módulo de Cuentas, cuando cambie el usuario asignado, se deben reasignar todos los registros asociados a dicha cuenta para el usuario reasignado a la misma.
			* Los registros que deben reasignarse son los que se encuentren pendiente de gestión, es decir, en un estado diferente a Completado/Realizado/Cerrado.
			*/
			if(!empty($bean->id)){//por si acaso... caso sasa 6634
				if(!empty($bean->assigned_user_id)){//por si acaso... caso sasa 6634
					if($bean->assigned_user_id_old_c != $bean->assigned_user_id){
						$GLOBALS['log']->security("\n Reasignando registros relacionados de la cuenta {$bean->id} de usaario {$bean->assigned_user_id_old_c} al {$bean->assigned_user_id}  \n"); 
						$bean->assigned_user_id_old_c = $bean->assigned_user_id;
						
						global $db;
						$queries = array(
							array(
								"modulo"=>"Calls",
								"query"=>"SELECT id,assigned_user_id FROM calls WHERE parent_id = '{$bean->id}' AND parent_type = 'Accounts' and calls.status != 'Held'"
							),
							array(
								"modulo"=>"Meetings",
								"query"=>"SELECT id,assigned_user_id FROM meetings WHERE parent_id = '{$bean->id}' AND parent_type = 'Accounts' and meetings.status != 'Held'"
							),
							array(
								"modulo"=>"Tasks",
								"query"=>"SELECT id,assigned_user_id FROM tasks WHERE parent_id = '{$bean->id}' AND parent_type = 'Accounts' and tasks.status != 'Completed'"
							),
							array(
								"modulo"=>"Contacts",
								"query"=>"SELECT contacts.id as id,assigned_user_id FROM contacts left join accounts_contacts on accounts_contacts.contact_id = contacts.id where accounts_contacts.account_id = '{$bean->id}' and accounts_contacts.deleted = 0"
							),
							array(
								"modulo"=>"Opportunities",
								"query"=>"SELECT opportunities.id as id,assigned_user_id FROM opportunities left join accounts_opportunities on accounts_opportunities.opportunity_id = opportunities.id where accounts_opportunities.account_id = '{$bean->id}' and accounts_opportunities.deleted = 0 and opportunities.sales_status not in ('Closed Won','Closed Lost')"
							),
							array(
								"modulo"=>"Accounts",
								"query"=>"SELECT id,assigned_user_id FROM accounts WHERE parent_id = '{$bean->id}'"
							),
							array(
								"modulo"=>"sasa_SaldosAF",
								"query"=>"SELECT sasa_saldosaf.id as id, assigned_user_id FROM sasa_saldosaf LEFT JOIN accounts_sasa_saldosaf_1_c ON accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id WHERE accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida = '{$bean->id}' and accounts_sasa_saldosaf_1_c.deleted = 0"
							),
							array(
								"modulo"=>"sasa_SaldosAV",
								"query"=>"SELECT sasa_saldosav.id AS id, assigned_user_id FROM sasa_saldosav LEFT JOIN accounts_sasa_saldosav_1_c ON accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id WHERE accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida = '{$bean->id}' AND accounts_sasa_saldosav_1_c.deleted = 0"
							),
						);
						foreach ($queries as $set) {
							$result = $db->query($set["query"], true);
							while ($row = $db->fetchByAssoc($result)) {
								if(!empty($row["id"])){//por si acaso... caso sasa 6634
									$bean_update = BeanFactory::getBean($set["modulo"], $row["id"], array('disable_row_level_security' => true));//se cambia por getBean ... caso sasa 6634
									if(!empty($bean_update->id)){//por si acaso... caso sasa 6634
										$bean_update->assigned_user_id = $bean->assigned_user_id;
										$bean_update->save();
									}
								}
							}
						}
					}
				}
			}
		}
		catch (Exception $e) {
			$GLOBALS['log']->security("ERROR: error al convertir ".$e->getMessage()); 
		}
	}
}
?>
