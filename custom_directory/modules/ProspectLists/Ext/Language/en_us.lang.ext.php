<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ProspectLists/Ext/Language/en_us.fbsgpl.php

$mod_strings['LBL_CC_SYNC_BUTTON'] = 'Sync Contacts and Leads on this list to CC';
$mod_strings['LBL_SYNC_TO_CC'] = 'Sync List Details To CC';
$mod_strings['LBL_LAST_SUCCESSFUL_CC_UPDATE'] = 'Last Successful CC Update';
$mod_strings['LBL_PROSPECTLISTS_TARGET_LIST_TITLE'] = 'Target Lists';
$mod_strings['LBL_CCID'] = 'Constant Contact ID';
$mod_strings['LBL_LAST_SUCCESSFUL_CC_REMOVE'] = 'Last Successful CC Contact Removal';

?>
