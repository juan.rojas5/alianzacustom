<?php
 // created: 2022-11-29 13:32:39
$dictionary['Case']['fields']['sasa_segundocorreo_c']['labelValue']='Email actualizado';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_segundocorreo_c']['enforced']='';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['dependency']='or(equal($sasa_detalle_c,"128"),equal($sasa_detalle_c,"129"))';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['required_formula']='or(equal($sasa_detalle_c,"128"),equal($sasa_detalle_c,"129"))';
$dictionary['Case']['fields']['sasa_segundocorreo_c']['readonly_formula']='';

 ?>