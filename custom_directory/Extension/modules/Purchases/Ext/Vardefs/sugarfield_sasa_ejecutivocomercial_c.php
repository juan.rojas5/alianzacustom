<?php
 // created: 2023-02-01 19:06:19
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['labelValue']='ejecutivo comercial';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['readonly_formula']='';

 ?>