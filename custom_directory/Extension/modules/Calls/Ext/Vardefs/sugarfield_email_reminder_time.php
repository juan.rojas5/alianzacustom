<?php
 // created: 2018-07-06 21:59:47
$dictionary['Call']['fields']['email_reminder_time']['default']='-1';
$dictionary['Call']['fields']['email_reminder_time']['audited']=true;
$dictionary['Call']['fields']['email_reminder_time']['comments']='Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Call']['fields']['email_reminder_time']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['email_reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['email_reminder_time']['merge_filter']='disabled';
$dictionary['Call']['fields']['email_reminder_time']['calculated']=false;
$dictionary['Call']['fields']['email_reminder_time']['dependency']=false;

 ?>