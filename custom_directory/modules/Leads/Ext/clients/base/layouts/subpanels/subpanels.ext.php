<?php
// WARNING: The contents of this file are auto-generated.


 // created: 2012-06-13 21:08:52
$layout_defs["Leads"]["subpanel_setup"]['fbsg_ccintegrationlog_leads'] = array (
  'order' => 100,
  'module' => 'fbsg_CCIntegrationLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'CC Integration Log',
  'get_subpanel_data' => 'fbsg_ccintegrationlog_leads',
  'top_buttons' => 
  array (
  ),
);


$viewdefs["Leads"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_CCILOG_SUBPANEL',
  'context' => array(
    'link' => 'fbsg_ccintegrationlog_leads',
  ),
);


$viewdefs["Leads"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
  'context' => array(
    'link' => 'prospect_list_leads',
  ),
);


//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'fbsg_ccintegrationlog_leads',
  'view' => 'subpanel-for-leads-fbsg_ccintegrationlog_leads',
);


//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'meetings',
  'view' => 'subpanel-for-leads-meetings',
);


//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunity',
  'view' => 'subpanel-for-leads-opportunity',
);


//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prospect_list_leads',
  'view' => 'subpanel-for-leads-prospect_list_leads',
);
