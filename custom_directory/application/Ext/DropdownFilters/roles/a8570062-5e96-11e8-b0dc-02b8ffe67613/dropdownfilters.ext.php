<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/a8570062-5e96-11e8-b0dc-02b8ffe67613/lead_source_dom.php

// created: 2018-05-23 21:15:17
$role_dropdown_filters['lead_source_dom'] = array (
  '' => true,
  'Cold Call' => true,
  'Existing Customer' => true,
  'Self Generated' => true,
  'Employee' => true,
  'Partner' => false,
  'Public Relations' => true,
  'Direct Mail' => true,
  'Conference' => true,
  'Trade Show' => true,
  'Web Site' => true,
  'Word of mouth' => true,
  'Email' => true,
  'Campaign' => true,
  'Support Portal User Registration' => false,
  'Other' => true,
  'Socio' => true,
  'Correo Electronico' => true,
  'Regristro de Usuario del Portal de Soporte' => true,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/a8570062-5e96-11e8-b0dc-02b8ffe67613/lead_status_dom.php

// created: 2018-05-23 21:18:50
$role_dropdown_filters['lead_status_dom'] = array (
  '' => true,
  'New' => true,
  'Assigned' => true,
  'In Process' => false,
  'Converted' => true,
  'Recycled' => true,
  'Dead' => true,
);
?>
