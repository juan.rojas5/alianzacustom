<?php
 // created: 2018-05-30 20:13:32
$dictionary['Contact']['fields']['date_entered']['audited']=true;
$dictionary['Contact']['fields']['date_entered']['comments']='Date record created';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Contact']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Contact']['fields']['date_entered']['calculated']=false;
$dictionary['Contact']['fields']['date_entered']['enable_range_search']='1';

 ?>