<?php
// created: 2022-04-29 13:46:19
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_sasa_tipificaciones_cases_1',
  'source' => 'non-db',
  'module' => 'sasa_sasa_tipificaciones',
  'bean_name' => 'sasa_sasa_tipificaciones',
  'side' => 'right',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link-type' => 'one',
);
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1_name"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
  'save' => true,
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link' => 'sasa_sasa_tipificaciones_cases_1',
  'table' => 'sasa_sasa_tipificaciones',
  'module' => 'sasa_sasa_tipificaciones',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE_ID',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link' => 'sasa_sasa_tipificaciones_cases_1',
  'table' => 'sasa_sasa_tipificaciones',
  'module' => 'sasa_sasa_tipificaciones',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
