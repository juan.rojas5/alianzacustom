<?php
 // created: 2018-08-04 17:42:19
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['labelValue']='Motivo de Perdida';
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['calculated']='true';
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['formula']='related($revenuelineitems,"sasa_motivoperdida_c")';
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['enforced']='true';
$dictionary['Opportunity']['fields']['sasa_motivoperdida_c']['dependency']='equal($sales_status,"Closed Lost")';

 ?>