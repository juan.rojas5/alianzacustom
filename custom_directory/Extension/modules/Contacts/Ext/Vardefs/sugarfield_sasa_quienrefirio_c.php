<?php
 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['labelValue']='Quien Refirió?';
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['enforced']='false';
$dictionary['Contact']['fields']['sasa_quienrefirio_c']['dependency']='equal($lead_source,"Word of mouth")';

 ?>