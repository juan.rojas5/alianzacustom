<?php
 // created: 2018-10-04 19:36:15

$app_list_strings['sasa_regional_c_list']=array (
  '' => '',
  'Barranquilla' => 'Barranquilla',
  'Bogota' => 'Bogotá',
  'Bucaramanga' => 'Bucaramanga',
  'Cali' => 'Cali',
  'Cartagena' => 'Cartagena',
  'Manizales' => 'Manizales',
  'Medellin' => 'Medellín',
  'Pereira' => 'Pereira',
  'Eje Cafetero' => 'Eje Cafetero',
);