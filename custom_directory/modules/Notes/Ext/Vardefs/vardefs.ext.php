<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_name.php

 // created: 2018-05-30 14:55:41
$dictionary['Note']['fields']['name']['audited']=false;
$dictionary['Note']['fields']['name']['massupdate']=false;
$dictionary['Note']['fields']['name']['comments']='Name of the note';
$dictionary['Note']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['name']['merge_filter']='disabled';
$dictionary['Note']['fields']['name']['reportable']=false;
$dictionary['Note']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.83',
  'searchable' => true,
);
$dictionary['Note']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_filename.php

 // created: 2018-05-30 15:01:02
$dictionary['Note']['fields']['filename']['audited']=false;
$dictionary['Note']['fields']['filename']['massupdate']=false;
$dictionary['Note']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['Note']['fields']['filename']['importable']='true';
$dictionary['Note']['fields']['filename']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['filename']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['filename']['merge_filter']='disabled';
$dictionary['Note']['fields']['filename']['reportable']=false;
$dictionary['Note']['fields']['filename']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_portal_flag.php

 // created: 2018-05-30 15:01:31
$dictionary['Note']['fields']['portal_flag']['default']=false;
$dictionary['Note']['fields']['portal_flag']['audited']=false;
$dictionary['Note']['fields']['portal_flag']['massupdate']=false;
$dictionary['Note']['fields']['portal_flag']['comments']='Portal flag indicator determines if note created via portal';
$dictionary['Note']['fields']['portal_flag']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['portal_flag']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['portal_flag']['merge_filter']='disabled';
$dictionary['Note']['fields']['portal_flag']['reportable']=false;
$dictionary['Note']['fields']['portal_flag']['unified_search']=false;
$dictionary['Note']['fields']['portal_flag']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_embed_flag.php

 // created: 2018-05-30 15:01:51
$dictionary['Note']['fields']['embed_flag']['default']=false;
$dictionary['Note']['fields']['embed_flag']['audited']=false;
$dictionary['Note']['fields']['embed_flag']['massupdate']=false;
$dictionary['Note']['fields']['embed_flag']['comments']='Embed flag indicator determines if note embedded in email';
$dictionary['Note']['fields']['embed_flag']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['embed_flag']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['embed_flag']['merge_filter']='disabled';
$dictionary['Note']['fields']['embed_flag']['reportable']=false;
$dictionary['Note']['fields']['embed_flag']['unified_search']=false;
$dictionary['Note']['fields']['embed_flag']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/rli_link_workflow.php

$dictionary['Note']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/full_text_search_admin.php

 // created: 2020-02-03 20:54:36
$dictionary['Note']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_id_sfc_c.php

 // created: 2022-06-28 15:16:17
$dictionary['Note']['fields']['sasa_id_sfc_c']['labelValue']='ID SFC';
$dictionary['Note']['fields']['sasa_id_sfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Note']['fields']['sasa_id_sfc_c']['enforced']='';
$dictionary['Note']['fields']['sasa_id_sfc_c']['dependency']='';
$dictionary['Note']['fields']['sasa_id_sfc_c']['required_formula']='';
$dictionary['Note']['fields']['sasa_id_sfc_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_referencia_c.php

 // created: 2022-06-28 15:16:17
$dictionary['Note']['fields']['sasa_referencia_c']['labelValue']='Referencia';
$dictionary['Note']['fields']['sasa_referencia_c']['dependency']='';
$dictionary['Note']['fields']['sasa_referencia_c']['required_formula']='';
$dictionary['Note']['fields']['sasa_referencia_c']['readonly_formula']='';
$dictionary['Note']['fields']['sasa_referencia_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_cfv_c.php

 // created: 2022-11-18 20:15:15
$dictionary['Note']['fields']['sasa_cfv_c']['labelValue']='Control fecha vencimiento';
$dictionary['Note']['fields']['sasa_cfv_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Note']['fields']['sasa_cfv_c']['enforced']='';
$dictionary['Note']['fields']['sasa_cfv_c']['dependency']='';
$dictionary['Note']['fields']['sasa_cfv_c']['required_formula']='';
$dictionary['Note']['fields']['sasa_cfv_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/sugarfield_sasa_fechavencimiento_c.php

 // created: 2022-11-29 16:33:36
$dictionary['Note']['fields']['sasa_fechavencimiento_c']['labelValue']='Fecha vencimiento';
$dictionary['Note']['fields']['sasa_fechavencimiento_c']['enforced']='';
$dictionary['Note']['fields']['sasa_fechavencimiento_c']['dependency']='';
$dictionary['Note']['fields']['sasa_fechavencimiento_c']['required_formula']='';
$dictionary['Note']['fields']['sasa_fechavencimiento_c']['readonly_formula']='';

 
?>
