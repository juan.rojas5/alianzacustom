<?php

namespace Sugarcrm\Sugarcrm\custom\inc\utils;

/**
 * Class ModuleHelper
 * @package Sugarcrm\Sugarcrm\custom\inc\utils
 */
trait ModuleHelper
{

    protected function isListDisabledForModule($module)
    {
        $result = false;

        $modules = $this->getListRestrictModules();

        foreach ($modules as $key => $value) {
            if (strcasecmp($value, $module) === 0) {
                $result = (bool) $value;
                break;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getListRestrictModules()
    {
        //return \SugarConfig::get('don_t_load_records_unless_search', []);//Dejo de funcionar en sugar8.0 o php 7.x
        return \SugarConfig::getInstance()->get('don_t_load_records_unless_search', []);
    }
}
