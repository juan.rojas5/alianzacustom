<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_SaldosAF'] = 'Saldos AF';
$app_list_strings['moduleList']['sasa_SaldosAV'] = 'Saldos AV';
$app_list_strings['moduleList']['sasa_MovimientosAF'] = 'Movimientos AF';
$app_list_strings['moduleList']['sasa_MovimientosAV'] = 'Movimientos AV';
$app_list_strings['moduleList']['sasa_MovimientosAVDivisas'] = 'Movimientos AV (Divisas)';
$app_list_strings['moduleList']['sasa_ProductosAFAV'] = 'Productos AF/AV';
$app_list_strings['moduleList']['sasa_Categorias'] = 'Categorias';
$app_list_strings['moduleListSingular']['sasa_SaldosAF'] = 'Saldo AF';
$app_list_strings['moduleListSingular']['sasa_SaldosAV'] = 'Saldo AV';
$app_list_strings['moduleListSingular']['sasa_MovimientosAF'] = 'Movimiento AF';
$app_list_strings['moduleListSingular']['sasa_MovimientosAV'] = 'Movimiento AV';
$app_list_strings['moduleListSingular']['sasa_MovimientosAVDivisas'] = 'Movimiento AV (Divisas)';
$app_list_strings['moduleListSingular']['sasa_ProductosAFAV'] = 'Producto AF/AV';
$app_list_strings['moduleListSingular']['sasa_Categorias'] = 'Categoria';
$app_list_strings['sasa_categoria_c_list'][''] = '';
