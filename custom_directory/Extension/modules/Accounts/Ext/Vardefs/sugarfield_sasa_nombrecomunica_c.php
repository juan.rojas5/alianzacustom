<?php
 // created: 2022-10-25 23:16:58
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['labelValue']='Nombre de quien se comunica ';
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['enforced']='';
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['dependency']='equal($sasa_tipopersona_c,"Juridica")';
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_nombrecomunica_c']['readonly_formula']='';

 ?>