<?php
// created: 2023-02-03 09:39:47
$viewdefs['Cases']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'case_number',
          'label' => 'LBL_LIST_NUMBER',
          'link' => true,
          'default' => true,
          'enabled' => true,
          'readonly' => true,
        ),
        1 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'link' => true,
          'default' => true,
          'enabled' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_negocio_c',
          'label' => 'LBL_SASA_NEGOCIO_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'default' => true,
          'enabled' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_sfcfechacreacion_c',
          'label' => 'LBL_SASA_SFCFECHACREACION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'resolved_datetime',
          'label' => 'LBL_RESOLVED_DATETIME',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_tipoentidad_c',
          'label' => 'LBL_SASA_TIPOENTIDAD_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_radalianza_c',
          'label' => 'LBL_SASA_RADALIANZA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_radalianzam2_c',
          'label' => 'LBL_SASA_RADALIANZAM2_C',
          'enabled' => true,
          'readonly' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_sasa_tipificaciones_cases_1_name',
          'label' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
          'enabled' => true,
          'id' => 'SASA_SASA_TIPIFICACIONES_CASES_1SASA_SASA_TIPIFICACIONES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO_NAME',
          'id' => 'ASSIGNED_USER_ID',
          'default' => true,
          'enabled' => true,
        ),
        11 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'default' => true,
          'enabled' => true,
          'readonly' => true,
        ),
        12 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'name' => 'sasa_recepcion_c',
          'label' => 'LBL_SASA_RECEPCION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'sasa_admision_c',
          'label' => 'LBL_SASA_ADMISION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'sasa_favoravilidad_c',
          'label' => 'LBL_SASA_FAVORAVILIDAD_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        16 => 
        array (
          'name' => 'sasa_marcacion_c',
          'label' => 'LBL_SASA_MARCACION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        17 => 
        array (
          'name' => 'sasa_desistimiento_c',
          'label' => 'LBL_SASA_DESISTIMIENTO_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        18 => 
        array (
          'name' => 'sasa_puntorecepcion_c',
          'label' => 'LBL_SASA_PUNTORECEPCION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        19 => 
        array (
          'name' => 'sasa_rectificacion_c',
          'label' => 'LBL_SASA_RECTIFICACION_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        20 => 
        array (
          'name' => 'sasa_tutela_c',
          'label' => 'LBL_SASA_TUTELA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        21 => 
        array (
          'name' => 'sasa_prorroga_c',
          'label' => 'LBL_SASA_PRORROGA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        22 => 
        array (
          'name' => 'sasa_anexoqueja_c',
          'label' => 'LBL_SASA_ANEXOQUEJA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        23 => 
        array (
          'name' => 'sasa_docrespfinal_c',
          'label' => 'LBL_SASA_DOCRESPFINAL_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        24 => 
        array (
          'name' => 'sasa_replica_c',
          'label' => 'LBL_SASA_REPLICA_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        25 => 
        array (
          'name' => 'sasa_anexoresfinal_c',
          'label' => 'LBL_SASA_ANEXORESFINAL_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        26 => 
        array (
          'name' => 'resolution',
          'label' => 'LBL_RESOLUTION',
          'enabled' => true,
          'sortable' => false,
          'default' => false,
        ),
        27 => 
        array (
          'name' => 'sasa_producto_cod_c',
          'label' => 'LBL_SASA_PRODUCTO_COD_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        28 => 
        array (
          'name' => 'sasa_fechaactualizacionsfc_c',
          'label' => 'LBL_SASA_FECHAACTUALIZACIONSFC_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        29 => 
        array (
          'name' => 'sasa_raddefensorc_c',
          'label' => 'LBL_SASA_RADDEFENSORC_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        30 => 
        array (
          'name' => 'sasa_quejaexpres_c',
          'label' => 'LBL_SASA_QUEJAEXPRES_C',
          'enabled' => true,
          'readonly' => false,
          'default' => false,
        ),
        31 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => false,
        ),
      ),
    ),
  ),
);