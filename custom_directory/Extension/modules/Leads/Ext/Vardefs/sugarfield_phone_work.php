<?php
 // created: 2018-07-11 04:12:56
$dictionary['Lead']['fields']['phone_work']['len']='100';
$dictionary['Lead']['fields']['phone_work']['required']=false;
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['help']='';
$dictionary['Lead']['fields']['phone_work']['importable']='false';
$dictionary['Lead']['fields']['phone_work']['audited']=false;
$dictionary['Lead']['fields']['phone_work']['reportable']=false;
$dictionary['Lead']['fields']['phone_work']['pii']=false;

 ?>