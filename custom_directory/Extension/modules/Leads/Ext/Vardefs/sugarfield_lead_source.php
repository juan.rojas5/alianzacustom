<?php
 // created: 2018-08-21 01:01:37
$dictionary['Lead']['fields']['lead_source']['len']=100;
$dictionary['Lead']['fields']['lead_source']['required']=false;
$dictionary['Lead']['fields']['lead_source']['massupdate']=true;
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['calculated']=false;
$dictionary['Lead']['fields']['lead_source']['dependency']=false;

 ?>