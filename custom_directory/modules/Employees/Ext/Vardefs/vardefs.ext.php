<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Employees/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2018-07-13 21:06:09
$dictionary['Employee']['fields']['phone_home']['audited']=false;
$dictionary['Employee']['fields']['phone_home']['massupdate']=false;
$dictionary['Employee']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';
$dictionary['Employee']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Employee']['fields']['phone_home']['unified_search']=false;
$dictionary['Employee']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Employee']['fields']['phone_home']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Employees/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Employee']['full_text_search']=true;

?>
