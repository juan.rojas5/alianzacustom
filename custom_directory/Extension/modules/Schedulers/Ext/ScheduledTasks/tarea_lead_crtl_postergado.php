<?php
/*
 * caso sasa 5696 Clientes Potenciales - Tarea programada Gestión "Postergados"
 * Se requiere para la instancia de AlianzaQAS, generar una tarea programada que identifique los registros del módulo de Clientes Potenciales, que en el campo "Crtl Postergado" equivalga a "Postergar" y ésta debe colocar en este campo el valor "vacío".
 * La periodicidad de ejecución de esta tarea debe ser una vez al día a las 23hrs (para efecto de pruebas podría configurarse cada 15 min)
*/
$job_strings[] = 'tarea_lead_crtl_postergado';
function tarea_lead_crtl_postergado(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_lead_crtl_postergado. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query= "
		SELECT
			id
		FROM
			leads
		LEFT JOIN leads_cstm ON id = id_c
		WHERE
			deleted = 0 AND id_c IS NOT NULL AND sasa_ctrlpostergado_c != ''
		";
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$bean = BeanFactory::getBean("Leads", $row['id'], array('disable_row_level_security' => true));
				if( !empty($bean->id) ){
					$GLOBALS['log']->security("tarea_lead_crtl_postergado lead {$bean->first_name}  {$bean->last_name} id:{$bean->id}"); 
					$bean->sasa_ctrlpostergado_c='';
					$bean->save();
				}
				else{
					$GLOBALS['log']->security("tarea_lead_crtl_postergado error no existe o esta eliminado lead {$row['id']}"); 
				}
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_lead_crtl_postergado. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}
?>
