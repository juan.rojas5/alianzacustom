<?php
 // created: 2018-07-04 18:31:44
$dictionary['Contact']['fields']['twitter']['audited']=false;
$dictionary['Contact']['fields']['twitter']['massupdate']=false;
$dictionary['Contact']['fields']['twitter']['comments']='The twitter name of the user';
$dictionary['Contact']['fields']['twitter']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['twitter']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Contact']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['twitter']['calculated']=false;
$dictionary['Contact']['fields']['twitter']['pii']=false;
$dictionary['Contact']['fields']['twitter']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>