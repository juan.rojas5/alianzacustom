<?php
$dictionary['CampaignTracker']['indices'][] = array(
    'name' => 'campaign_tracker_camid_idx',
    'type' => 'index',
    'fields' => array(
        'campaign_id',
    ),
);

$dictionary['CampaignTracker']['fields']['clicks'] = array(
    'name' => 'clicks',
    'type' => 'int',
    'vname' => 'LBL_CLICKS',
    'len' => '11',
    'studio' => 'visible',
);