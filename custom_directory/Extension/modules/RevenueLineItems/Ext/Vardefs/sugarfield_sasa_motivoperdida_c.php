<?php
 // created: 2018-08-04 17:03:59
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['labelValue']='Motivo de Pérdida';
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['enforced']='';
$dictionary['RevenueLineItem']['fields']['sasa_motivoperdida_c']['dependency']='equal($sales_stage,"Closed Lost")';

 ?>