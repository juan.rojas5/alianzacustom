<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestosxAsesor/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2019-03-08 18:51:48
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestosxAsesor/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2019-03-08 18:51:49
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['sasaP_PresupuestosxAsesor']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestosxAsesor/Ext/Vardefs/sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_sasaP_PresupuestosxAsesor.php

// created: 2019-03-08 18:56:45
$dictionary["sasaP_PresupuestosxAsesor"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestoxProducto',
  'bean_name' => 'sasaP_PresupuestoxProducto',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestosxAsesor/Ext/Vardefs/sugarfield_sasa_deficitconsolidado_c.php

 // created: 2019-03-08 19:11:10

 
?>
<?php
// Merged from custom/Extension/modules/sasaP_PresupuestosxAsesor/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasaP_PresupuestosxAsesor']['full_text_search']=true;

?>
