<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/fbsg_ConstantContactIntegration/include/ConstantContact.php');

class fbsg_cc_PersonLogic
{
    public function after_save(&$bean, $event, $arguments)
    {
        $requestViewed = isset($_REQUEST['viewed']) ? $_REQUEST['viewed'] : false;

        if ($requestViewed && !$this->IsSecondSave()) {
            $this->HandleSync($bean, $event, $arguments);
        }
    }

    protected function IsSecondSave()
    {
        return isset($_REQUEST['fbsg_save']) && $_REQUEST['fbsg_save'] === false;
    }

    protected function GetSyncInfo($bean)
    {
        global $log;
        $cc_acc = SugarCC::GetOnlyCC();
        if (!$cc_acc) return false;

        if ($bean->cc_synced == false) {
            return false;
        }

        $cc = new CCBasic($cc_acc->name, $cc_acc->password);
        $ccContact = new SugarCCPerson($cc);

        return $ccContact;
    }

    protected function HandleSync(&$bean)
    {
        $admin = new \Administration();
        $admin->retrieveSettings(false, true);
        // This will prevent the integration from unsetting people from their target lists when the cc_lists field has not been updated for everyone
        $updateRels = (isset($admin->settings['fbsgcci_cc_people_updated']) &&  $admin->settings['fbsgcci_cc_people_updated'] == true) ? true : false;

        $contact = $this->GetSyncInfo($bean);
        if ($contact !== false){
            $contact->Sync($bean, $updateRels);
        } else {
            $contact = new SugarCCPerson();
            // This will update just the relationships
            $contact->Sync($bean, $updateRels, true);
        }
    }
}
