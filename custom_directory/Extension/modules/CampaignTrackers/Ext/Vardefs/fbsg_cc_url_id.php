<?php
$dictionary['CampaignTracker']['fields']['cc_url_id'] = array(
    'required' => false,
    'name' => 'cc_url_id',
    'vname' => 'LBL_CC_URL_ID',
    'type' => 'varchar',
    'len' => 255,
);

