<?php
//custom/Extension/application/Ext/Language/es_ES.dashlet3_portal.php
$viewdefs['base']['view']['dashlet3_portal'] = array(
    'dashlets' => array(
        array(
            'label' => 'LBL_dashlet3_portal',
            'description' => 'LBL_dashlet3_portal_description',
            'config' => array(
                'limit' => 20,
                'auto_refresh' => 0,
            ),
            'preview' => array(
                'limit' => 5,
                'auto_refresh' => 0,
                'feed_url' => 'http://blog.sugarcrm.com/feed/',
            ),
            'filter' => array(
                'module' => array(
                   'Home',
                   'Accounts'
                ),
                'view' => 'record',
            )
        ),
    ),
);
