<?php
 // created: 2018-01-29 21:13:44

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kliens',
  'Opportunities' => 'Lehetőség',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Contacts' => 'Kapcsolatok',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'Prospects' => 'Cél',
  'ProjectTask' => 'Projektfeladat',
  'Tasks' => 'Feladat',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevételi sorok',
);