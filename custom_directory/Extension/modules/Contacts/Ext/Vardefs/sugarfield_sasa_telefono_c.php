<?php
 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_telefono_c']['labelValue']='Teléfono';
$dictionary['Contact']['fields']['sasa_telefono_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_telefono_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_telefono_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>