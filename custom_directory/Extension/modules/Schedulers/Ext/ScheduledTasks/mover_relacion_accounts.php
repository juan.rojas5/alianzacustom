<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'mover_relacion_accounts';
function mover_relacion_accounts(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-mover_relacion_accounts. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		//Conuslta para sacar el total agrupado por numeroid de las cuentas duplicadas
		$query = "SELECT accounts_cstm.id_c idcuentamayor, accounts_cstm.sasa_nroidentificacion_c numiden FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' group by accounts_cstm.sasa_nroidentificacion_c ORDER BY accounts.date_modified DESC";

		global $db;
		$cuentasmenores = array();
		$idcuentamayor = $db->query($query);
		//Ciclo para recorrer el total agrupado
		while ($row = $db->fetchByAssoc($idcuentamayor)) {
			$GLOBALS['log']->security("NumeroID del lote: {$row['numiden']}");
			$numid = $row['numiden'];
			//Consulta para sacar las cuentas con menor fecha de modificacion
			$querysubcuentas = "SELECT accounts_cstm.id_c, accounts_cstm.sasa_nroidentificacion_c, tabla.nomcu FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina, accounts.name nomcu FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' AND accounts_cstm.sasa_nroidentificacion_c='{$row['numiden']}' ORDER BY accounts.date_modified DESC LIMIT 1,5";
			//consulta para sacar el id de la cuenta con mayor fecha de modificacion
			$querycuentamayor = $db->query("SELECT accounts_cstm.id_c, accounts_cstm.sasa_nroidentificacion_c, tabla.nomcu FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina, accounts.name nomcu FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' AND accounts_cstm.sasa_nroidentificacion_c='{$row['numiden']}' ORDER BY accounts.date_modified DESC LIMIT 0,1");
			$idcuentamas = mysqli_fetch_array($querycuentamayor);
			$GLOBALS['log']->security("IDCUENTAMAYOR: {$idcuentamas['id_c']}");
			$idscuentasantiguas = $db->query($querysubcuentas);
			
			//ciclo para empezar a ejecutar los querys de reasignacion de registros relacionados
			while ($row2 = $db->fetchByAssoc($idscuentasantiguas)) {
				//array de queries 15 en total para reasignacion de registros relacionados a la cuenta con mayor fecha de modificacion
				$queries = array("UPDATE meetings SET meetings.parent_id='{$idcuentamas['id_c']}' WHERE meetings.parent_id='{$row2['id_c']}' AND meetings.deleted=0",
					"UPDATE calls SET calls.parent_id='{$idcuentamas['id_c']}' WHERE calls.parent_id='{$row2['id_c']}' AND calls.deleted=0",
					"UPDATE tasks SET tasks.parent_id='{$idcuentamas['id_c']}' WHERE tasks.parent_id='{$row2['id_c']}' AND tasks.deleted=0",
					"UPDATE notes SET notes.parent_id='{$idcuentamas['id_c']}' WHERE notes.parent_id='{$row2['id_c']}' AND notes.deleted=0",
					"UPDATE accounts_contacts SET accounts_contacts.account_id='{$idcuentamas['id_c']}' WHERE accounts_contacts.account_id='{$row2['id_c']}' AND accounts_contacts.deleted=0",
					"UPDATE accounts_opportunities SET accounts_opportunities.account_id='{$idcuentamas['id_c']}' WHERE accounts_opportunities.account_id='{$row2['id_c']}' AND accounts_opportunities.deleted=0",
					"UPDATE revenue_line_items SET revenue_line_items.account_id='{$idcuentamas['id_c']}' WHERE revenue_line_items.account_id='{$row2['id_c']}' AND revenue_line_items.deleted=0",
					"UPDATE leads SET leads.account_id='{$idcuentamas['id_c']}' WHERE leads.account_id='{$row2['id_c']}' AND leads.deleted=0",
					"UPDATE accounts_sasa_saldosaf_1_c SET accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida ='{$idcuentamas['id_c']}' WHERE accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_saldosaf_1_c.deleted=0",
					"UPDATE accounts_sasa_saldosav_1_c SET accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_saldosav_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosav_1_c SET accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosav_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosaf_1_c SET accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosaf_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosavdivisas_1_c SET accounts_sasa_movimientosavdivisas_1_c.accounts_sasa_movimientosavdivisas_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosavdivisas_1_c.accounts_sasa_movimientosavdivisas_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosavdivisas_1_c.deleted=0",
					"UPDATE accounts_sasas_saldosconsolidados_1_c SET accounts_sasas_saldosconsolidados_1_c.accounts_sasas_saldosconsolidados_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasas_saldosconsolidados_1_c.accounts_sasas_saldosconsolidados_1accounts_ida='{$row2['id_c']}' AND accounts_sasas_saldosconsolidados_1_c.deleted=0",
					"UPDATE emails SET emails.parent_id='{$idcuentamas['id_c']}' WHERE emails.parent_id='{$row2['id_c']}' AND emails.deleted=0",
					"UPDATE accounts SET accounts.deleted=1 WHERE accounts.id='{$row2['id_c']}'",
				);
				$GLOBALS['log']->security("IDSUBCUENTAS: {$row2['id_c']}");
				//forecah para empezar a ejecutar los queries del array $queries
				foreach ($queries as $query) {
					$querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
					$result = $db->query($querytotal);
					$lastDbError = $db->lastDbError();				
					if(!empty($lastDbError)){
						$GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
					}
					//$GLOBALS['log']->security("querytotal: {$querytotal}");
				}
			}
		}
		
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-mover_relacion_accounts. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}
