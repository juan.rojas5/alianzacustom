<?php

  if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
  class leads_before_save
  {
  
    function before_save($bean, $event, $arguments)
    {
      //echo "before_save";
      /*
      * Si los campos "Nombre" y "Apellido" están "vacíos" deben poblarse con el valor "No Disponible" solo para Tipo persona Juridica
      */
      if ($bean->sasa_tipopersona_c == "Juridica") {
        //Valida campo vacio
        if(empty($bean->first_name)){
          $bean->first_name = "No Disponible";
        }
        //Valida campo vacio
        if(empty($bean->last_name)){
          $bean->last_name = "No Disponible";
        }
      }
    }
  }

?>
