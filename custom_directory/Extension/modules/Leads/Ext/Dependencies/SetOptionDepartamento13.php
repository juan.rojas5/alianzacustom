<?php
$dependencies['Leads']['SetOptionDepartamento13'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cesar")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Aguachica","Agustin Codazzi","Astrea","Becerril","Bosconia","Chimichagua","Chiriguana","Curumani","El Copey","El Paso","Gamarra","Gonzalez","La Gloria","La Jagua de Ibirico","La Paz","Manaure","Pailitas","Pelaya","Pueblo Bello","Rio de Oro","San Alberto","San Diego","San Martin","Tamalameque","Valledupar")',
           'labels' => 'createList("","Aguachica","Agustín Codazzi","Astrea","Becerril","Bosconia","Chimichagua","Chiriguaná","Curumaní","El Copey","El Paso","Gamarra","González","La Gloria","La Jagua de Ibirico","La Paz","Manaure","Pailitas","Pelaya","Pueblo Bello","Río de Oro","San Alberto","San Diego","San Martín","Tamalameque","Valledupar")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
