<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_description.php

 // created: 2018-05-22 21:46:05
$dictionary['Lead']['fields']['description']['audited']=false;
$dictionary['Lead']['fields']['description']['massupdate']=false;
$dictionary['Lead']['fields']['description']['comments']='Full text of the note';
$dictionary['Lead']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['description']['reportable']=false;
$dictionary['Lead']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.7',
  'searchable' => true,
);
$dictionary['Lead']['fields']['description']['calculated']=false;
$dictionary['Lead']['fields']['description']['rows']='6';
$dictionary['Lead']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_status_description.php

 // created: 2018-05-22 21:53:04
$dictionary['Lead']['fields']['status_description']['audited']=false;
$dictionary['Lead']['fields']['status_description']['massupdate']=false;
$dictionary['Lead']['fields']['status_description']['comments']='Description of the status of the lead';
$dictionary['Lead']['fields']['status_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status_description']['reportable']=false;
$dictionary['Lead']['fields']['status_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['status_description']['calculated']=false;
$dictionary['Lead']['fields']['status_description']['rows']='4';
$dictionary['Lead']['fields']['status_description']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_lead_source_description.php

 // created: 2018-05-22 21:55:31
$dictionary['Lead']['fields']['lead_source_description']['audited']=false;
$dictionary['Lead']['fields']['lead_source_description']['massupdate']=false;
$dictionary['Lead']['fields']['lead_source_description']['comments']='Description of the lead source';
$dictionary['Lead']['fields']['lead_source_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['lead_source_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['lead_source_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source_description']['reportable']=false;
$dictionary['Lead']['fields']['lead_source_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['lead_source_description']['calculated']=false;
$dictionary['Lead']['fields']['lead_source_description']['rows']='4';
$dictionary['Lead']['fields']['lead_source_description']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_do_not_call.php

 // created: 2018-05-22 21:58:40
$dictionary['Lead']['fields']['do_not_call']['default']=false;
$dictionary['Lead']['fields']['do_not_call']['audited']=false;
$dictionary['Lead']['fields']['do_not_call']['massupdate']=false;
$dictionary['Lead']['fields']['do_not_call']['comments']='An indicator of whether contact can be called';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['do_not_call']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['do_not_call']['merge_filter']='disabled';
$dictionary['Lead']['fields']['do_not_call']['unified_search']=false;
$dictionary['Lead']['fields']['do_not_call']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_assistant.php

 // created: 2018-05-22 22:00:11
$dictionary['Lead']['fields']['assistant']['audited']=false;
$dictionary['Lead']['fields']['assistant']['massupdate']=false;
$dictionary['Lead']['fields']['assistant']['comments']='Name of the assistant of the contact';
$dictionary['Lead']['fields']['assistant']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant']['reportable']=false;
$dictionary['Lead']['fields']['assistant']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant']['calculated']=false;
$dictionary['Lead']['fields']['assistant']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2018-05-23 13:48:33

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2018-05-23 13:48:34

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_opportunity_amount.php

 // created: 2018-05-28 23:35:20
$dictionary['Lead']['fields']['opportunity_amount']['audited']=true;
$dictionary['Lead']['fields']['opportunity_amount']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_amount']['comments']='Amount of the opportunity';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_amount']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_amount']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_amount']['reportable']=true;
$dictionary['Lead']['fields']['opportunity_amount']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_amount']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-06-03 16:58:03
$dictionary['Lead']['fields']['date_entered']['audited']=false;
$dictionary['Lead']['fields']['date_entered']['comments']='Date record created';
$dictionary['Lead']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Lead']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Lead']['fields']['date_entered']['reportable']=true;
$dictionary['Lead']['fields']['date_entered']['calculated']=false;
$dictionary['Lead']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_regional_c.php

 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_regional_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_regional_c']['labelValue']='Regional';
$dictionary['Lead']['fields']['sasa_regional_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_regional_c']['calculated']=true;
$dictionary['Lead']['fields']['sasa_regional_c']['formula']='related($assigned_user_link,"sasa_regional_c")';
$dictionary['Lead']['fields']['sasa_regional_c']['enforced']='true';
$dictionary['Lead']['fields']['sasa_regional_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_segmento_c.php

 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_segmento_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_segmento_c']['labelValue']='Segmento';
$dictionary['Lead']['fields']['sasa_segmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_segmento_c']['calculated']=true;
$dictionary['Lead']['fields']['sasa_segmento_c']['formula']='related($assigned_user_link,"sasa_segmento_c")';
$dictionary['Lead']['fields']['sasa_segmento_c']['enforced']='true';
$dictionary['Lead']['fields']['sasa_segmento_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_valorinversion_c.php

 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_valorinversion_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_valorinversion_c']['labelValue']='Valor Inversión';
$dictionary['Lead']['fields']['sasa_valorinversion_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_valorinversion_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_valorinversion_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p1rangoedadcalidad_c.php

 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_p1rangoedadcalidad_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p1rangoedadcalidad_c']['labelValue']='1. ¿En qué rango  de edad o calidad se encuentra?';
$dictionary['Lead']['fields']['sasa_p1rangoedadcalidad_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p1rangoedadcalidad_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p5porcentajeactivosah_c.php

 // created: 2018-06-07 00:31:56
$dictionary['Lead']['fields']['sasa_p5porcentajeactivosah_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p5porcentajeactivosah_c']['labelValue']='5. ¿Qué porcentaje del total de sus activos representa este ahorro o inversión?';
$dictionary['Lead']['fields']['sasa_p5porcentajeactivosah_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p5porcentajeactivosah_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p4dondeprovienedinero_c.php

 // created: 2018-06-07 00:31:56
$dictionary['Lead']['fields']['sasa_p4dondeprovienedinero_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p4dondeprovienedinero_c']['labelValue']='4. ¿De dónde proviene el dinero que desea invertir?';
$dictionary['Lead']['fields']['sasa_p4dondeprovienedinero_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p4dondeprovienedinero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p2propositoahorroinve_c.php

 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_p2propositoahorroinve_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p2propositoahorroinve_c']['labelValue']='2. ¿Cuál es el propósito de su ahorro o inversión?';
$dictionary['Lead']['fields']['sasa_p2propositoahorroinve_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p2propositoahorroinve_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p3horizontetiempoinve_c.php

 // created: 2018-06-07 00:31:56
$dictionary['Lead']['fields']['sasa_p3horizontetiempoinve_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p3horizontetiempoinve_c']['labelValue']='3. ¿Cuál es el horizonte de tiempo en el que mantendrá su inversión?';
$dictionary['Lead']['fields']['sasa_p3horizontetiempoinve_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p3horizontetiempoinve_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p6experienciainversio_c.php

 // created: 2018-06-07 00:31:57
$dictionary['Lead']['fields']['sasa_p6experienciainversio_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p6experienciainversio_c']['labelValue']='6. ¿Ha tenido experiencia en el tema de inversiones?';
$dictionary['Lead']['fields']['sasa_p6experienciainversio_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p6experienciainversio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p8supongainversionesp_c.php

 // created: 2018-06-07 00:31:57
$dictionary['Lead']['fields']['sasa_p8supongainversionesp_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_p8supongainversionesp_c']['labelValue']='8. Suponga que sus inversiones pierden un 10% este mes, ¿qué haría usted?';
$dictionary['Lead']['fields']['sasa_p8supongainversionesp_c']['calculated']=false;
$dictionary['Lead']['fields']['sasa_p8supongainversionesp_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_calculoperfilriesgo_c.php

 // created: 2018-06-07 00:45:19
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['labelValue']='Calculo Perfil Riesgo';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['formula']='add(number($sasa_p1rangoedadcalidad_c),number($sasa_p2propositoahorroinve_c),number($sasa_p3horizontetiempoinve_c),number($sasa_p4dondeprovienedinero_c),number($sasa_p5porcentajeactivosah_c),number($sasa_p6experienciainversio_c),number($sasa_p7oportunidadrentabil_c),number($sasa_p8supongainversionesp_c))';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_calculoperfilriesgo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_p7oportunidadrentabil_c.php

 // created: 2018-06-07 00:48:27
$dictionary['Lead']['fields']['sasa_p7oportunidadrentabil_c']['labelValue']='7. Si tuviera la oportunidad de aumentar su rentabilidad asumiendo mayor riesgo, e incluyendo pérdidas potenciales a su inversión inicial ¿qué haría usted?';
$dictionary['Lead']['fields']['sasa_p7oportunidadrentabil_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_p7oportunidadrentabil_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_other.php

 // created: 2018-06-17 22:22:59
$dictionary['Lead']['fields']['phone_other']['len']='100';
$dictionary['Lead']['fields']['phone_other']['audited']=false;
$dictionary['Lead']['fields']['phone_other']['massupdate']=false;
$dictionary['Lead']['fields']['phone_other']['comments']='Formato: +xxx-x-xxxxxxx';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_other']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_other']['reportable']=false;
$dictionary['Lead']['fields']['phone_other']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.99',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_other']['calculated']=false;
$dictionary['Lead']['fields']['phone_other']['pii']=false;
$dictionary['Lead']['fields']['phone_other']['help']='Formato: +xxx-x-xxxxxxx';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/fbsg_cc_newvars.php

// $cc_module = 'Lead';
// include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/fbsg_vardefs_leads.php

$dictionary['Lead']['fields']['fbsg_ccintegrationlog_leads'] = array(
    'name' => 'fbsg_ccintegrationlog_leads',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_leads',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Lead"]["fields"]["prospect_list_leads"] = array(
    'name' => 'prospect_list_leads',
    'type' => 'link',
    'relationship' => 'prospect_list_leads',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Lead';
include('custom/include/fbsg_cc_newvars.php');

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_opportunity_name.php

 // created: 2018-06-23 15:05:46
$dictionary['Lead']['fields']['opportunity_name']['audited']=false;
$dictionary['Lead']['fields']['opportunity_name']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_name']['comments']='Opportunity name associated with lead';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_name']['reportable']=false;
$dictionary['Lead']['fields']['opportunity_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_account_description.php

 // created: 2018-06-23 16:01:37
$dictionary['Lead']['fields']['account_description']['audited']=false;
$dictionary['Lead']['fields']['account_description']['massupdate']=false;
$dictionary['Lead']['fields']['account_description']['comments']='Description of lead account';
$dictionary['Lead']['fields']['account_description']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['account_description']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['account_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_description']['reportable']=false;
$dictionary['Lead']['fields']['account_description']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_description']['calculated']=false;
$dictionary['Lead']['fields']['account_description']['rows']='4';
$dictionary['Lead']['fields']['account_description']['cols']='20';
$dictionary['Lead']['fields']['account_description']['dependency']='equal($sasa_tipopersona_c,"Juridica")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_refered_by.php

 // created: 2018-06-23 16:28:28
$dictionary['Lead']['fields']['refered_by']['audited']=false;
$dictionary['Lead']['fields']['refered_by']['massupdate']=false;
$dictionary['Lead']['fields']['refered_by']['comments']='Identifies who refered the lead';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['refered_by']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['refered_by']['merge_filter']='disabled';
$dictionary['Lead']['fields']['refered_by']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['refered_by']['calculated']=false;
$dictionary['Lead']['fields']['refered_by']['dependency']='equal($lead_source,"Word of mouth")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2018-07-09 13:05:29
$dictionary['Lead']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Lead']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_superior1_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Lead']['fields']['sasa_superior1_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2018-07-09 13:07:27
$dictionary['Lead']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Lead']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_superior2_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior2_c")';
$dictionary['Lead']['fields']['sasa_superior2_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2018-07-11 04:12:56
$dictionary['Lead']['fields']['phone_work']['len']='100';
$dictionary['Lead']['fields']['phone_work']['required']=false;
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['help']='';
$dictionary['Lead']['fields']['phone_work']['importable']='false';
$dictionary['Lead']['fields']['phone_work']['audited']=false;
$dictionary['Lead']['fields']['phone_work']['reportable']=false;
$dictionary['Lead']['fields']['phone_work']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_country.php

 // created: 2018-07-11 04:18:26
$dictionary['Lead']['fields']['primary_address_country']['len']='255';
$dictionary['Lead']['fields']['primary_address_country']['required']=false;
$dictionary['Lead']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_country']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2018-07-11 04:18:45
$dictionary['Lead']['fields']['primary_address_city']['required']=false;
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_city']['importable']='true';
$dictionary['Lead']['fields']['primary_address_city']['audited']=true;
$dictionary['Lead']['fields']['primary_address_city']['reportable']=true;
$dictionary['Lead']['fields']['primary_address_city']['pii']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2018-07-11 20:34:52
$dictionary['Lead']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_street.php

 // created: 2018-07-11 20:38:08
$dictionary['Lead']['fields']['alt_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.3',
  'searchable' => true,
);
$dictionary['Lead']['fields']['alt_address_street']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_street']['rows']='4';
$dictionary['Lead']['fields']['alt_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2018-07-11 20:52:56
$dictionary['Lead']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_postalcode.php

 // created: 2018-07-11 20:53:26
$dictionary['Lead']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_postalcode']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_telefono2_c.php

 // created: 2018-07-14 14:44:41
$dictionary['Lead']['fields']['sasa_telefono2_c']['labelValue']='Teléfono 2';
$dictionary['Lead']['fields']['sasa_telefono2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_telefono2_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_telefono2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2018-07-18 04:32:41
$dictionary['Lead']['fields']['phone_home']['len']='20';
$dictionary['Lead']['fields']['phone_home']['audited']=false;
$dictionary['Lead']['fields']['phone_home']['massupdate']=false;
$dictionary['Lead']['fields']['phone_home']['comments']='';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_home']['reportable']=false;
$dictionary['Lead']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.02',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_home']['calculated']=false;
$dictionary['Lead']['fields']['phone_home']['pii']=false;
$dictionary['Lead']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_assistant_phone.php

 // created: 2018-07-18 04:33:11
$dictionary['Lead']['fields']['assistant_phone']['len']='20';
$dictionary['Lead']['fields']['assistant_phone']['audited']=false;
$dictionary['Lead']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Lead']['fields']['assistant_phone']['comments']='';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant_phone']['reportable']=false;
$dictionary['Lead']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant_phone']['calculated']=false;
$dictionary['Lead']['fields']['assistant_phone']['pii']=false;
$dictionary['Lead']['fields']['assistant_phone']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_fax.php

 // created: 2018-07-18 04:35:14
$dictionary['Lead']['fields']['phone_fax']['len']='100';
$dictionary['Lead']['fields']['phone_fax']['audited']=false;
$dictionary['Lead']['fields']['phone_fax']['massupdate']=false;
$dictionary['Lead']['fields']['phone_fax']['comments']='';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_fax']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_fax']['reportable']=false;
$dictionary['Lead']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.98',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_fax']['calculated']=false;
$dictionary['Lead']['fields']['phone_fax']['pii']=false;
$dictionary['Lead']['fields']['phone_fax']['help']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2018-07-18 20:39:24
$dictionary['Lead']['fields']['primary_address_street']['audited']=true;
$dictionary['Lead']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.31',
  'searchable' => true,
);
$dictionary['Lead']['fields']['primary_address_street']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_street']['pii']=true;
$dictionary['Lead']['fields']['primary_address_street']['rows']='4';
$dictionary['Lead']['fields']['primary_address_street']['cols']='20';
$dictionary['Lead']['fields']['primary_address_street']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ctrlconversion_c.php

 // created: 2018-07-20 16:07:02
$dictionary['Lead']['fields']['sasa_ctrlconversion_c']['labelValue']='Ctrl Conversión';
$dictionary['Lead']['fields']['sasa_ctrlconversion_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_ctrlconversion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2018-08-04 15:46:55
$dictionary['Lead']['fields']['phone_mobile']['len']='10';
$dictionary['Lead']['fields']['phone_mobile']['required']=false;
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']=false;
$dictionary['Lead']['fields']['phone_mobile']['help']='Formato: (Número de celular de diez dígitos)';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_nombreproducto_c.php

 // created: 2018-08-08 03:27:52
$dictionary['Lead']['fields']['sasa_nombreproducto_c']['labelValue']='Productos de Interés';
$dictionary['Lead']['fields']['sasa_nombreproducto_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_nombreproducto_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_perfilriesgo_c.php

 // created: 2018-08-11 23:10:16
$dictionary['Lead']['fields']['sasa_perfilriesgo_c']['labelValue']='Perfil de Riesgo';
$dictionary['Lead']['fields']['sasa_perfilriesgo_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_perfilriesgo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2018-08-15 19:46:50
$dictionary['Lead']['fields']['primary_address_state']['required']=false;
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_salutation.php

 // created: 2018-08-17 22:10:50
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['options']='salutation_list';
$dictionary['Lead']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Lead']['fields']['salutation']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['salutation']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Lead']['fields']['salutation']['calculated']=false;
$dictionary['Lead']['fields']['salutation']['dependency']=false;
$dictionary['Lead']['fields']['salutation']['required']=false;
$dictionary['Lead']['fields']['salutation']['audited']=false;
$dictionary['Lead']['fields']['salutation']['pii']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_first_name.php

 // created: 2018-08-17 22:11:18
$dictionary['Lead']['fields']['first_name']['required']=false;
$dictionary['Lead']['fields']['first_name']['massupdate']=false;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['first_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.87',
  'searchable' => true,
);
$dictionary['Lead']['fields']['first_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_last_name.php

 // created: 2018-08-17 22:11:39
$dictionary['Lead']['fields']['last_name']['massupdate']=false;
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.85',
  'searchable' => true,
);
$dictionary['Lead']['fields']['last_name']['calculated']=false;
$dictionary['Lead']['fields']['last_name']['importable']='required';
$dictionary['Lead']['fields']['last_name']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_nombrecompleto_c.php

 // created: 2018-08-18 14:11:02
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['labelValue']='Nombre completo';
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['calculated']='true';
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['formula']='concat($first_name," ",$last_name)';
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['enforced']='true';
$dictionary['Lead']['fields']['sasa_nombrecompleto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_inforequerida_c.php

 // created: 2018-08-20 23:46:18
$dictionary['Lead']['fields']['sasa_inforequerida_c']['labelValue']='Información Requerida*';
$dictionary['Lead']['fields']['sasa_inforequerida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_inforequerida_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_inforequerida_c']['dependency']='equal($sasa_ctrlconversion_c,"Conversion II")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_telefono_c.php

 // created: 2018-08-21 01:00:58
$dictionary['Lead']['fields']['sasa_telefono_c']['labelValue']='Teléfono';
$dictionary['Lead']['fields']['sasa_telefono_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_telefono_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_lead_source.php

 // created: 2018-08-21 01:01:37
$dictionary['Lead']['fields']['lead_source']['len']=100;
$dictionary['Lead']['fields']['lead_source']['required']=false;
$dictionary['Lead']['fields']['lead_source']['massupdate']=true;
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['calculated']=false;
$dictionary['Lead']['fields']['lead_source']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_status.php

 // created: 2018-08-21 01:13:50
$dictionary['Lead']['fields']['status']['len']=100;
$dictionary['Lead']['fields']['status']['required']=true;
$dictionary['Lead']['fields']['status']['massupdate']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['calculated']=false;
$dictionary['Lead']['fields']['status']['dependency']=false;
$dictionary['Lead']['fields']['status']['default']='Assigned';
$dictionary['Lead']['fields']['status']['options']='lead_status_dom';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ingresosventasanuales_c.php

 // created: 2018-08-22 13:36:44
$dictionary['Lead']['fields']['sasa_ingresosventasanuales_c']['labelValue']='Ingresos/Ventas Anuales';
$dictionary['Lead']['fields']['sasa_ingresosventasanuales_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_ingresosventasanuales_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_ingresosventasanuales_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_pais_c.php

 // created: 2018-06-25 22:07:22
$dictionary['Lead']['fields']['sasa_pais_c']['labelValue']='País';
$dictionary['Lead']['fields']['sasa_pais_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_email.php

 // created: 2018-08-27 17:55:31
$dictionary['Lead']['fields']['email']['len']='100';
$dictionary['Lead']['fields']['email']['required']=false;
$dictionary['Lead']['fields']['email']['massupdate']=true;
$dictionary['Lead']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['email']['merge_filter']='disabled';
$dictionary['Lead']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.83',
  'searchable' => true,
);
$dictionary['Lead']['fields']['email']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ctrlnombreempresa_c.php

 // created: 2018-08-31 13:32:02
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['labelValue']='Nombre Empresa';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1,35',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['calculated']='1';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['formula']='ifElse(equal($sasa_tipopersona_c,"Juridica"),$account_name,"")';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['enforced']='1';
$dictionary['Lead']['fields']['sasa_ctrlnombreempresa_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_estadoinfocp_c.php

 // created: 2018-09-10 11:59:53
$dictionary['Lead']['fields']['sasa_estadoinfocp_c']['labelValue']='Información Requerida (eliminar)';
$dictionary['Lead']['fields']['sasa_estadoinfocp_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_estadoinfocp_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ingresosmensuales_c.php

 // created: 2018-09-12 21:58:09
$dictionary['Lead']['fields']['sasa_ingresosmensuales_c']['labelValue']='Ingresos Mensuales (eliminar)';
$dictionary['Lead']['fields']['sasa_ingresosmensuales_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_ingresosmensuales_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_ingresosmensuales_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/custom_import_index.php

$dictionary['Lead']['indices'][] = array(
     'name' => 'idx_sasa_nroidentificacion_c',
     'type' => 'index',
     'fields' => array(
         'sasa_nroidentificacion_c',
     ),
     'source' => 'non-db',
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_nroidentificacion_c.php

 // created: 2018-11-26 22:09:42
$dictionary['Lead']['fields']['sasa_nroidentificacion_c']['labelValue']='Número de ID';
$dictionary['Lead']['fields']['sasa_nroidentificacion_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.5',
  'searchable' => true,
);
$dictionary['Lead']['fields']['sasa_nroidentificacion_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_nroidentificacion_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ctrlpostergado_c.php

 // created: 2019-02-25 14:10:06
$dictionary['Lead']['fields']['sasa_ctrlpostergado_c']['labelValue']='Ctrl Postergado';
$dictionary['Lead']['fields']['sasa_ctrlpostergado_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_ctrlpostergado_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_departamentointer_c.php

 // created: 2021-03-10 19:40:50
$dictionary['Lead']['fields']['sasa_departamentointer_c']['labelValue']='Departamento';
$dictionary['Lead']['fields']['sasa_departamentointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_departamentointer_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_title.php

 // created: 2021-03-09 20:31:09
$dictionary['Lead']['fields']['title']['massupdate']=false;
$dictionary['Lead']['fields']['title']['comments']='The title of the contact';
$dictionary['Lead']['fields']['title']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['title']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['title']['merge_filter']='disabled';
$dictionary['Lead']['fields']['title']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['title']['calculated']=false;
$dictionary['Lead']['fields']['title']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_municipio_c.php

 // created: 2021-03-10 19:14:30
$dictionary['Lead']['fields']['sasa_municipio_c']['labelValue']='Ciudad';
$dictionary['Lead']['fields']['sasa_municipio_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_actividadeconomica_c.php

 // created: 2021-03-09 20:28:19
$dictionary['Lead']['fields']['sasa_actividadeconomica_c']['labelValue']='Actividad Económica';
$dictionary['Lead']['fields']['sasa_actividadeconomica_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_website.php

 // created: 2021-03-10 19:33:45
$dictionary['Lead']['fields']['website']['len']='255';
$dictionary['Lead']['fields']['website']['audited']=false;
$dictionary['Lead']['fields']['website']['massupdate']=false;
$dictionary['Lead']['fields']['website']['comments']='URL of website for the company';
$dictionary['Lead']['fields']['website']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['website']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['website']['merge_filter']='disabled';
$dictionary['Lead']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['website']['calculated']=false;
$dictionary['Lead']['fields']['website']['gen']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_municipiointer_c.php

 // created: 2021-03-10 19:39:51
$dictionary['Lead']['fields']['sasa_municipiointer_c']['labelValue']='Ciudad';
$dictionary['Lead']['fields']['sasa_municipiointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_municipiointer_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_ocupacion_c.php

 // created: 2021-03-09 21:30:02
$dictionary['Lead']['fields']['sasa_ocupacion_c']['labelValue']='Ocupación';
$dictionary['Lead']['fields']['sasa_ocupacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_department.php

 // created: 2021-03-09 20:29:01
$dictionary['Lead']['fields']['department']['audited']=true;
$dictionary['Lead']['fields']['department']['massupdate']=false;
$dictionary['Lead']['fields']['department']['comments']='Department the lead belongs to';
$dictionary['Lead']['fields']['department']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['department']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['department']['merge_filter']='disabled';
$dictionary['Lead']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['department']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_account_name.php

 // created: 2021-03-08 16:05:55
$dictionary['Lead']['fields']['account_name']['audited']=true;
$dictionary['Lead']['fields']['account_name']['massupdate']=false;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_name']['calculated']=false;
$dictionary['Lead']['fields']['account_name']['required']=true;
$dictionary['Lead']['fields']['account_name']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_departamento_c.php

 // created: 2021-03-10 11:09:38
$dictionary['Lead']['fields']['sasa_departamento_c']['labelValue']='Departamento';
$dictionary['Lead']['fields']['sasa_departamento_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_birthdate.php

 // created: 2021-03-09 20:30:32
$dictionary['Lead']['fields']['birthdate']['audited']=false;
$dictionary['Lead']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Lead']['fields']['birthdate']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['birthdate']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Lead']['fields']['birthdate']['calculated']=false;
$dictionary['Lead']['fields']['birthdate']['pii']=false;
$dictionary['Lead']['fields']['birthdate']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_sector_c.php

 // created: 2021-03-08 15:24:32
$dictionary['Lead']['fields']['sasa_sector_c']['labelValue']='Sector';
$dictionary['Lead']['fields']['sasa_sector_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipopersona_c.php

 // created: 2021-03-08 13:56:08
$dictionary['Lead']['fields']['sasa_tipopersona_c']['labelValue']='Tipo de Persona';
$dictionary['Lead']['fields']['sasa_tipopersona_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_tipoidentificacion_c.php

 // created: 2021-03-09 20:05:16
$dictionary['Lead']['fields']['sasa_tipoidentificacion_c']['labelValue']='Tipo de ID';
$dictionary['Lead']['fields']['sasa_tipoidentificacion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Lead']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_sasa_patrimonio_c.php

 // created: 2021-11-19 14:10:55
$dictionary['Lead']['fields']['sasa_patrimonio_c']['labelValue']='Patrimonio Reportado';
$dictionary['Lead']['fields']['sasa_patrimonio_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_patrimonio_c']['dependency']='';
$dictionary['Lead']['fields']['sasa_patrimonio_c']['required_formula']='';
$dictionary['Lead']['fields']['sasa_patrimonio_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/customer_journey_parent.php

// created: 2023-02-03 09:42:31
VardefManager::createVardef('Leads', 'Lead', [
                                'customer_journey_parent',
                        ]);
?>
