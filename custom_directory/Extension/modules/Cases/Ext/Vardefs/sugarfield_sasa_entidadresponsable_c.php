<?php
 // created: 2023-03-10 13:25:20
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['labelValue']='Entidad responsable';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['enforced']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['dependency']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_entidadresponsable_c']['readonly_formula']='';

 ?>