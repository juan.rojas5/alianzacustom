<?php
 // created: 2022-09-28 14:17:24
$dictionary['Case']['fields']['sasa_segmento_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_segmento_c']['labelValue']='Segmento';
$dictionary['Case']['fields']['sasa_segmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_segmento_c']['calculated']='1';
$dictionary['Case']['fields']['sasa_segmento_c']['formula']='related($accounts,"sasa_segmento_c")';
$dictionary['Case']['fields']['sasa_segmento_c']['enforced']='1';
$dictionary['Case']['fields']['sasa_segmento_c']['dependency']='or(equal($sasa_tipodesolicitud_c,"peticion"),equal($sasa_tipodesolicitud_c,"felicitaciones"),equal($sasa_tipodesolicitud_c,"sugerencia"))';
$dictionary['Case']['fields']['sasa_segmento_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_segmento_c']['readonly_formula']='';

 ?>