<?php
 // created: 2018-06-06 21:51:35

$app_list_strings['sasa_p6experienciainversio_c_list']=array (
  '' => '',
  2 => 'a. Poca',
  5 => 'b. Alguna, preferiría recibir orientación y asesoria en los temas de inversiones',
  7 => 'c. Mucha, me siento seguro y tranquilo para entender los riesgos asociados a cada inversión y tomar decisiones',
  10 => 'd. Soy inversionista profesional',
);