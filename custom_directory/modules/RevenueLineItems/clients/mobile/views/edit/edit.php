<?php
// created: 2023-02-03 09:39:47
$viewdefs['RevenueLineItems']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'required' => true,
        ),
        1 => 
        array (
          'name' => 'opportunity_name',
          'required' => true,
        ),
        2 => 
        array (
          'name' => 'account_name',
          'readonly' => true,
        ),
        3 => 
        array (
          'name' => 'date_closed',
          'required' => true,
        ),
        4 => 
        array (
          'name' => 'likely_case',
          'required' => true,
        ),
        5 => 'best_case',
        6 => 'worst_case',
        7 => 'sales_stage',
        8 => 'probability',
        9 => 'product_template_name',
        10 => 'quantity',
        11 => 'discount_amount',
        12 => 
        array (
          'name' => 'quote_name',
          'readonly' => true,
        ),
        13 => 'tag',
        14 => 'assigned_user_name',
        15 => 'team_name',
      ),
    ),
  ),
);