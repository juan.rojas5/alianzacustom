<?php
 // created: 2018-11-21 21:42:12
$dictionary['sasa_SaldosAV']['fields']['name']['len']='255';
$dictionary['sasa_SaldosAV']['fields']['name']['audited']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['massupdate']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['unified_search']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_SaldosAV']['fields']['name']['calculated']='true';
$dictionary['sasa_SaldosAV']['fields']['name']['importable']='false';
$dictionary['sasa_SaldosAV']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAV']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAV']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_SaldosAV']['fields']['name']['formula']='$sasa_nemotecnico_c';
$dictionary['sasa_SaldosAV']['fields']['name']['enforced']=true;

 ?>