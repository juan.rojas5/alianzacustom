<?php
 // created: 2018-07-13 21:06:09
$dictionary['Employee']['fields']['phone_home']['audited']=false;
$dictionary['Employee']['fields']['phone_home']['massupdate']=false;
$dictionary['Employee']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';
$dictionary['Employee']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['Employee']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['Employee']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Employee']['fields']['phone_home']['unified_search']=false;
$dictionary['Employee']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Employee']['fields']['phone_home']['calculated']=false;

 ?>