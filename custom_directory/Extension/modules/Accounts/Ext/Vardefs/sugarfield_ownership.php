<?php
 // created: 2019-07-10 19:31:53
$dictionary['Account']['fields']['ownership']['len']='100';
$dictionary['Account']['fields']['ownership']['required']=false;
$dictionary['Account']['fields']['ownership']['audited']=true;
$dictionary['Account']['fields']['ownership']['massupdate']=false;
$dictionary['Account']['fields']['ownership']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['ownership']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['ownership']['merge_filter']='disabled';
$dictionary['Account']['fields']['ownership']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['ownership']['calculated']=false;
$dictionary['Account']['fields']['ownership']['dependency']='equal($sasa_tipopersona_c,"Juridica")';

 ?>