<?php
 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_id']['len']=255;
$dictionary['Call']['fields']['parent_id']['audited']=false;
$dictionary['Call']['fields']['parent_id']['massupdate']=false;
$dictionary['Call']['fields']['parent_id']['comments']='';
$dictionary['Call']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_id']['calculated']=false;
$dictionary['Call']['fields']['parent_id']['reportable']=true;
$dictionary['Call']['fields']['parent_id']['unified_search']=false;
$dictionary['Call']['fields']['parent_id']['group']='';

 ?>