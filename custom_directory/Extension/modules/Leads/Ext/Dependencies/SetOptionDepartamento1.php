<?php
$dependencies['Leads']['SetOptionDepartamento1'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Amazonas")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","El Encanto","La Chorrera","La Pedrera","La Victoria","Leticia","Miriti Parana","Puerto Alegria","Puerto Arica","Puerto Narino","Puerto Santander","Tarapaca")',
           'labels' => 'createList("","El Encanto","La Chorrera","La Pedrera","La Victoria","Leticia","Miriti Paraná","Puerto Alegría","Puerto Arica","Puerto Nariño","Puerto Santander","Tarapacá")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
