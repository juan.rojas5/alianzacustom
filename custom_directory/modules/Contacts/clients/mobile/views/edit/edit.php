<?php
// created: 2023-02-03 09:39:47
$viewdefs['Contacts']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'salutation',
          'comment' => 'Contact salutation (e.g., Mr, Ms)',
          'label' => 'LBL_SALUTATION',
        ),
        1 => 
        array (
          'name' => 'first_name',
          'customCode' => '{html_options name="salutation" options=$fields.salutation.options selected=$fields.salutation.value}&nbsp;<input name="first_name" size="15" maxlength="25" type="text" value="{$fields.first_name.value}">',
          'displayParams' => 
          array (
            'wireless_edit_only' => true,
          ),
        ),
        2 => 
        array (
          'name' => 'last_name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        3 => 
        array (
          'name' => 'sasa_parentescorelacion_c',
          'label' => 'LBL_SASA_PARENTESCORELACION_C',
        ),
        4 => 'account_name',
        5 => 
        array (
          'name' => 'sasa_relacioncuenta_c',
          'label' => 'LBL_SASA_RELACIONCUENTA_C',
        ),
        6 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        7 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        8 => 'title',
        9 => 
        array (
          'name' => 'department',
          'comment' => 'The department of the contact',
          'label' => 'LBL_DEPARTMENT',
        ),
        10 => 'phone_mobile',
        11 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        12 => 'email',
        13 => 
        array (
          'name' => 'sasa_pais_c',
          'label' => 'LBL_SASA_PAIS_C',
        ),
        14 => 
        array (
          'name' => 'sasa_departamento_c',
          'label' => 'LBL_SASA_DEPARTAMENTO_C',
        ),
        15 => 
        array (
          'name' => 'sasa_municipio_c',
          'label' => 'LBL_SASA_MUNICIPIO_C',
        ),
        16 => 'primary_address_street',
        17 => 'phone_home',
        18 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        19 => 
        array (
          'name' => 'assistant_phone',
          'comment' => 'Phone number of the assistant of the contact',
          'label' => 'LBL_ASSISTANT_PHONE',
        ),
        20 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        21 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
        ),
        22 => 
        array (
          'name' => 'sasa_estadocivil_c',
          'label' => 'LBL_SASA_ESTADOCIVIL_C',
        ),
        23 => 
        array (
          'name' => 'sasa_nrohijos_c',
          'label' => 'LBL_SASA_NROHIJOS_C',
        ),
        24 => 
        array (
          'name' => 'sasa_hobbiesaficionesdepor_c',
          'label' => 'LBL_SASA_HOBBIESAFICIONESDEPOR_C',
        ),
        25 => 
        array (
          'name' => 'sasa_fondopensiones_c',
          'label' => 'LBL_SASA_FONDOPENSIONES_C',
        ),
        26 => 
        array (
          'name' => 'sasa_fondocesantias_c',
          'label' => 'LBL_SASA_FONDOCESANTIAS_C',
        ),
        27 => 
        array (
          'name' => 'sasa_infoquiererecibir_c',
          'label' => 'LBL_SASA_INFOQUIERERECIBIR_C',
        ),
        28 => 
        array (
          'name' => 'sasa_tipoinfoperiodicidad_c',
          'label' => 'LBL_SASA_TIPOINFOPERIODICIDAD_C',
        ),
        29 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        30 => 
        array (
          'name' => 'lead_source',
          'comment' => 'How did the contact come about',
          'label' => 'LBL_LEAD_SOURCE',
        ),
        31 => 
        array (
          'name' => 'sasa_quienrefirio_c',
          'label' => 'LBL_SASA_QUIENREFIRIO_C',
        ),
        32 => 
        array (
          'name' => 'sasa_noenviarinfocliente_c',
          'label' => 'LBL_SASA_NOENVIARINFOCLIENTE_C',
        ),
        33 => 'assigned_user_name',
        34 => 'tag',
      ),
    ),
  ),
);