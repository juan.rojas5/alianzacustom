<?php

$job_strings[] = 'UpdateAccountAssignedTeamPrivate';


function UpdateAccountAssignedTeamPrivate()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Account Assigned Team Private.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    /*
    *
    CONSULTA PARA TRAER CUENTAS usuario_asiagando != equipo privado y que no esten los siquientes equipos Canastas: Medellin 1, Medellin 2, Bogota 1, Bogota 2 
    *
    */
    $cantRel = 0;
    $query = "SELECT
                a.id AS ID_CUENTA, 
                a.name AS CUENTA,
                u.id AS ID_USERS,
                u.first_name AS USUARIO,
                t.name AS EQUIPO
            FROM
                accounts a
            LEFT JOIN(
                SELECT
                    a1.id AS ID_A
                FROM
                    accounts a1
                INNER JOIN team_sets_teams tst1 ON tst1.team_set_id = a1.team_set_id
                INNER JOIN teams t1 ON  t1.id = tst1.team_id
                WHERE
                    t1.id = '449f5dc4-43a1-11eb-8457-0286beac7abe' OR t1.id = '4dda40a2-43a1-11eb-b1d6-069273903c1e' OR t1.id = '57124e62-43a1-11eb-a3b5-069273903c1e' OR t1.id = '611f0666-43a1-11eb-973b-0286beac7abe' AND a1.deleted = 0 AND t1.deleted = 0
            ) tabla1 ON a.id = tabla1.ID_A
            LEFT JOIN(
                SELECT
                    a2.id AS ID_A2
                FROM
                    accounts a2
                INNER JOIN users u2 ON  u2.id = a2.assigned_user_id
                INNER JOIN team_sets_teams tst2 ON  tst2.team_set_id = a2.team_set_id
                INNER JOIN teams t2 ON  t2.id = tst2.team_id
                WHERE t2.private = 1 AND u2.deleted = 0 AND a2.deleted = 0 AND t2.deleted = 0
            ) tabla2 ON a.id = tabla2.ID_A2
            INNER JOIN users u ON u.id = a.assigned_user_id
            INNER JOIN team_sets_teams tst ON tst.team_set_id = a.team_set_id
            INNER JOIN teams t ON  t.id = tst.team_id
            WHERE
                tabla1.ID_A IS NULL AND tabla2.ID_A2 IS NULL AND t.private != 1 AND a.assigned_user_id != '' AND a.assigned_user_id IS NOT NULL AND u.deleted = 0 AND a.deleted = 0 AND t.deleted = 0 
                    GROUP BY a.id LIMIT 2000";

    $result = $GLOBALS['db']->query($query);

    $cantidadQuery = mysqli_num_rows($result);

    $GLOBALS['log']->security("***Cantidad Consulta=>***" . $cantidadQuery);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
    {
       $GLOBALS['log']->security("Id Cuenta=>" . $row['ID_CUENTA'] . "name=>" . $row['CUENTA'] . "Id User=>" . $row['ID_USERS']);

       // Consulta de Equipos de las cuentas para asignar el equipo privado
       $queryTeamPrivate = "SELECT t.id AS ID_TEAM FROM teams t WHERE t.associated_user_id = '{$row['ID_USERS']}' AND t.private = 1 AND t.deleted = 0";

           $resultTeam = $GLOBALS['db']->query($queryTeamPrivate);

           $cantidadTeam = mysqli_num_rows($resultTeam);

           $GLOBALS['log']->security("***Cantidad Consulta Teams=>***" . $cantidadTeam);

           while ($row2 = $GLOBALS['db']->fetchByAssoc($resultTeam))
           {
                $cantRel++;
            $GLOBALS['log']->security("id Teams=>" . $row2['ID_TEAM'] . "id cuenta=>" . $row['ID_CUENTA']);
                //Retrieve the bean
                $bean = BeanFactory::getBean("Accounts", $row['ID_CUENTA']);

                //Load the team relationship 
                $bean->load_relationship('teams');

                //Add the teams
                $bean->teams->add(
                    array(
                        $row2['ID_TEAM']
                    )
                );

           }
    }

    $GLOBALS['log']->security("Cantidad Relacionadas=>" .$cantRel);
    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Account Assigned Team Private. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}
