<?php
// created: 2023-02-20 21:38:36
$dictionary["Account"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'bean_name' => 'sasa1_Unidades_de_negocio_por_',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
