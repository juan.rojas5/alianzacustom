<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CASES_SUBPANEL_TITLE'] = 'PQRs';
$mod_strings['LBL_SASA_ENCARGO_C'] = 'Número Encargo';
$mod_strings['LBL_SASA_TIPOTITULAR_C'] = 'tipo titular';
$mod_strings['LBL_SASA_CODIGONEGOCIO_C'] = 'Código negocio';
$mod_strings['LBL_SASA_FECHACANCELACION_C'] = 'Fecha de cancelación';
$mod_strings['LBL_SASA_ESTADO_C'] = 'Estado producto';
$mod_strings['LBL_SASA_EJECUTIVOCOMERCIAL_C'] = 'ejecutivo comercial';
$mod_strings['LBL_SASA_CODREGIONAL_C'] = 'codigo regional';
$mod_strings['LBL_SASA_CODSUCURSAL_C'] = 'codigo sucursal';
$mod_strings['LBL_SASA_DIRECTORDEGESTION_C'] = 'director de gestión';
$mod_strings['LBL_SASA_ASISTENTEDEGESTION_C'] = 'asistente de gestión';
$mod_strings['LBL_SASA_NOMBREDENEGOCIO_C'] = 'Nombre de negocio';
$mod_strings['LBL_SASA_DVENCARGO_C'] = 'DV encargo';
$mod_strings['LBL_SASA_FECHACREACION_C'] = 'Fecha de creación Producto';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Fechas productos';
$mod_strings['LBL_SASA_CODIGOCOMERCIAL_C'] = 'Código comercial';
$mod_strings['LBL_SASA_ESTADONEGOCIO_C'] = 'Estado de negocio';
$mod_strings['LBL_SASA_NOMBREDIRECTORGESTION_C'] = 'Nombre director de gestión';
$mod_strings['LBL_SASA_NOMBREASISTENTEGESTION_'] = 'Nombre asistente de gestión';
