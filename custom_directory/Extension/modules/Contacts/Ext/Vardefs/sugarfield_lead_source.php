<?php
 // created: 2018-08-31 23:46:30
$dictionary['Contact']['fields']['lead_source']['len']=100;
$dictionary['Contact']['fields']['lead_source']['required']=false;
$dictionary['Contact']['fields']['lead_source']['audited']=true;
$dictionary['Contact']['fields']['lead_source']['massupdate']=true;
$dictionary['Contact']['fields']['lead_source']['comments']='How did the contact come about';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['lead_source']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Contact']['fields']['lead_source']['reportable']=true;
$dictionary['Contact']['fields']['lead_source']['calculated']=false;
$dictionary['Contact']['fields']['lead_source']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>