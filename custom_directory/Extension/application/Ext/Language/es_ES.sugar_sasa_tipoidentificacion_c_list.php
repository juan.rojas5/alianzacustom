<?php
 // created: 2022-03-10 09:23:04

$app_list_strings['sasa_tipoidentificacion_c_list']=array (
  '' => '',
  'Cedula de Ciudadania' => 'Cédula de Ciudadanía',
  'Cedula de Extranjeria' => 'Cédula de Extranjería',
  'Pasaporte' => 'Pasaporte',
  'Nit' => 'Nit',
  'NUIP' => 'NUIP',
  'Registro Civil' => 'Registro Civil',
  'Tarjeta de identidad' => 'Tarjeta de Identidad',
  'Carne Diplomatico' => 'Carné Diplomatico',
  'Fideicomiso' => 'Fideicomiso',
  'Sociedad Extranjera' => 'Sociedad Extranjera',
  'Otro' => 'Otro',
  'No Registra' => 'No Registra',
  8 => 'Permiso especial de permanencia (PEP)',
  10 => 'Permiso por protección temporal (PPT)',
);