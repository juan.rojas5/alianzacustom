<?php
// created: 2023-02-03 09:39:47
$viewdefs['Contacts']['base']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 => 
        array (
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        2 => 
        array (
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        3 => 
        array (
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        4 => 
        array (
          'type' => 'divider',
        ),
        5 => 
        array (
          'type' => 'manage-subscription',
          'name' => 'manage_subscription_button',
          'label' => 'LBL_MANAGE_SUBSCRIPTIONS',
          'showOn' => 'view',
          'value' => 'edit',
        ),
        6 => 
        array (
          'type' => 'vcard',
          'name' => 'vcard_button',
          'label' => 'LBL_VCARD_DOWNLOAD',
          'acl_action' => 'edit',
        ),
        7 => 
        array (
          'type' => 'divider',
        ),
        8 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        9 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'Contacts',
          'acl_action' => 'create',
        ),
        10 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        11 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        12 => 
        array (
          'type' => 'divider',
        ),
        13 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'header' => true,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'picture',
          'type' => 'hint-contacts-photo',
          'size' => 'large',
          'dismiss_label' => true,
          'white_list' => true,
          'related_fields' => 
          array (
            0 => 'hint_contact_pic',
          ),
        ),
        1 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'dismiss_label' => true,
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
        ),
        2 => 
        array (
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        3 => 
        array (
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_relacioncuenta_c',
          'label' => 'LBL_SASA_RELACIONCUENTA_C',
          'span' => 12,
        ),
        1 => 
        array (
          'name' => 'account_name',
        ),
        2 => 
        array (
        ),
        3 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        4 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        5 => 
        array (
          'name' => 'title',
          'span' => 12,
        ),
        6 => 
        array (
          'name' => 'email',
        ),
        7 => 
        array (
        ),
        8 => 
        array (
        ),
        9 => 
        array (
        ),
        10 => 
        array (
        ),
        11 => 
        array (
          'name' => 'cases_contacts_1_name',
        ),
      ),
    ),
    2 => 
    array (
      'columns' => 2,
      'name' => 'panel_hidden',
      'label' => 'LBL_RECORD_SHOWMORE',
      'hide' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'collapsed',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'sasa_pais_c',
          'label' => 'LBL_SASA_PAIS_C',
          'span' => 12,
        ),
        1 => 
        array (
        ),
        2 => 
        array (
          'name' => 'sasa_departamentointer_c',
          'label' => 'LBL_SASA_DEPARTAMENTOINTER_C',
        ),
        3 => 
        array (
          'name' => 'sasa_municipiointer_c',
          'label' => 'LBL_SASA_MUNICIPIOINTER_C',
        ),
        4 => 
        array (
          'name' => 'sasa_departamento_c',
          'label' => 'LBL_SASA_DEPARTAMENTO_C',
        ),
        5 => 
        array (
          'name' => 'sasa_municipio_c',
          'label' => 'LBL_SASA_MUNICIPIO_C',
        ),
        6 => 
        array (
          'name' => 'primary_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_PRIMARY_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'primary_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'primary_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'primary_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'primary_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'primary_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
            ),
          ),
        ),
        7 => 
        array (
          'name' => 'alt_address',
          'type' => 'fieldset',
          'css_class' => 'address',
          'label' => 'LBL_ALT_ADDRESS',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'alt_address_street',
              'css_class' => 'address_street',
              'placeholder' => 'LBL_ALT_ADDRESS_STREET',
            ),
            1 => 
            array (
              'name' => 'alt_address_city',
              'css_class' => 'address_city',
              'placeholder' => 'LBL_ALT_ADDRESS_CITY',
            ),
            2 => 
            array (
              'name' => 'alt_address_state',
              'css_class' => 'address_state',
              'placeholder' => 'LBL_ALT_ADDRESS_STATE',
            ),
            3 => 
            array (
              'name' => 'alt_address_postalcode',
              'css_class' => 'address_zip',
              'placeholder' => 'LBL_ALT_ADDRESS_POSTALCODE',
            ),
            4 => 
            array (
              'name' => 'alt_address_country',
              'css_class' => 'address_country',
              'placeholder' => 'LBL_ALT_ADDRESS_COUNTRY',
            ),
            5 => 
            array (
              'name' => 'copy',
              'label' => 'NTC_COPY_PRIMARY_ADDRESS',
              'type' => 'copy',
              'mapping' => 
              array (
                'primary_address_street' => 'alt_address_street',
                'primary_address_city' => 'alt_address_city',
                'primary_address_state' => 'alt_address_state',
                'primary_address_postalcode' => 'alt_address_postalcode',
                'primary_address_country' => 'alt_address_country',
              ),
            ),
          ),
        ),
        8 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        9 => 
        array (
          'name' => 'phone_home',
          'comment' => 'Home phone number of the contact',
          'label' => 'LBL_HOME_PHONE',
        ),
        10 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        11 => 
        array (
          'name' => 'assistant_phone',
          'comment' => 'Phone number of the assistant of the contact',
          'label' => 'LBL_ASSISTANT_PHONE',
        ),
        12 => 
        array (
          'name' => 'phone_fax',
        ),
        13 => 
        array (
        ),
        14 => 
        array (
          'name' => 'sasa_fondocesantias_c',
          'label' => 'LBL_SASA_FONDOCESANTIAS_C',
        ),
        15 => 
        array (
          'name' => 'sasa_fondopensiones_c',
          'label' => 'LBL_SASA_FONDOPENSIONES_C',
        ),
        16 => 
        array (
          'name' => 'birthdate',
          'comment' => 'The birthdate of the contact',
          'label' => 'LBL_BIRTHDATE',
        ),
        17 => 
        array (
          'name' => 'sasa_hobbiesaficionesdepor_c',
          'label' => 'LBL_SASA_HOBBIESAFICIONESDEPOR_C',
        ),
        18 => 
        array (
          'name' => 'sasa_nrohijos_c',
          'label' => 'LBL_SASA_NROHIJOS_C',
        ),
        19 => 
        array (
          'name' => 'preferred_language',
          'type' => 'language',
        ),
        20 => 
        array (
          'name' => 'facebook',
          'comment' => 'The facebook name of the user',
          'label' => 'LBL_FACEBOOK',
        ),
        21 => 'twitter',
        22 => 
        array (
          'name' => 'googleplus',
          'comment' => 'The google plus id of the user',
          'label' => 'LBL_GOOGLEPLUS',
        ),
        23 => 
        array (
        ),
        24 => 
        array (
        ),
        25 => 
        array (
          'name' => 'campaign_name',
          'span' => 12,
        ),
        26 => 
        array (
          'name' => 'report_to_name',
        ),
        27 => 
        array (
          'name' => 'portal_user_company_name',
        ),
        28 => 
        array (
          'span' => 12,
        ),
      ),
    ),
    3 => 
    array (
      'newTab' => false,
      'panelDefault' => 'collapsed',
      'name' => 'LBL_RECORDVIEW_PANEL1',
      'label' => 'LBL_RECORDVIEW_PANEL1',
      'columns' => 2,
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 'assigned_user_name',
        1 => 'team_name',
        2 => 
        array (
          'name' => 'date_entered_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_ENTERED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_entered',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'created_by_name',
            ),
          ),
        ),
        3 => 
        array (
          'name' => 'date_modified_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_MODIFIED',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'date_modified',
            ),
            1 => 
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 => 
            array (
              'name' => 'modified_by_name',
            ),
          ),
        ),
        4 => 
        array (
          'name' => 'sasa_fechadevinculacionsc_c',
          'label' => 'LBL_SASA_FECHADEVINCULACIONSC_C',
        ),
        5 => 
        array (
          'name' => 'sasa_fechadeactualizacionsc_c',
          'label' => 'LBL_SASA_FECHADEACTUALIZACIONSC',
        ),
        6 => 
        array (
          'name' => 'sasa_fechademodificacionsc_c',
          'label' => 'LBL_SASA_FECHADEMODIFICACIONSC_C',
        ),
        7 => 
        array (
          'name' => 'tag',
        ),
        8 => 
        array (
          'name' => 'hint_account_website',
          'label' => 'LBL_HINT_COMPANY_WEBSITE',
          'type' => 'stage2_url',
          'white_list' => true,
          'fields' => 
          array (
            0 => 'hint_photo',
            1 => 'hint_account_logo',
          ),
        ),
        9 => 'hint_education',
        10 => 
        array (
          'name' => 'hint_education_2',
          'parent_key' => 'hint_education',
        ),
        11 => 'hint_job_2',
        12 => 'hint_account_size',
        13 => 'hint_account_industry',
        14 => 'hint_account_location',
        15 => 
        array (
          'name' => 'hint_account_description',
          'account_key' => 'description',
        ),
        16 => 'hint_account_founded_year',
        17 => 
        array (
          'name' => 'hint_industry_tags',
          'account_key' => 'hint_account_industry_tags',
        ),
        18 => 'hint_account_naics_code_lbl',
        19 => 
        array (
          'name' => 'hint_account_sic_code_label',
          'account_key' => 'sic_code',
        ),
        20 => 'hint_account_fiscal_year_end',
        21 => 
        array (
          'name' => 'hint_account_annual_revenue',
          'account_key' => 'annual_revenue',
        ),
        22 => 
        array (
          'name' => 'hint_facebook',
          'type' => 'stage2_url',
        ),
        23 => 
        array (
          'name' => 'hint_twitter',
          'type' => 'stage2_url',
        ),
        24 => 
        array (
          'name' => 'hint_account_facebook_handle',
          'type' => 'stage2_url',
        ),
        25 => 
        array (
          'name' => 'hint_account_twitter_handle',
          'type' => 'stage2_url',
          'account_key' => 'twitter',
        ),
        26 => 
        array (
          'name' => 'phone_other',
          'type' => 'phone',
        ),
        27 => 
        array (
        ),
      ),
    ),
    4 => 
    array (
      'newTab' => false,
      'panelDefault' => 'collapsed',
      'name' => 'LBL_RECORDVIEW_PANEL2',
      'label' => 'LBL_RECORDVIEW_PANEL2',
      'columns' => 2,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'readonly' => false,
          'name' => 'sasa_nombrem4_c',
          'label' => 'LBL_SASA_NOMBREM4',
        ),
        1 => 
        array (
          'readonly' => false,
          'name' => 'sasa_apellidom4_c',
          'label' => 'LBL_SASA_APELLIDOM4',
        ),
        2 => 
        array (
          'readonly' => false,
          'name' => 'sasa_nroidentificacionm4_c',
          'label' => 'LBL_SASA_NROIDENTIFICACIONM4_C',
        ),
        3 => 
        array (
          'readonly' => false,
          'name' => 'sasa_tipoidentificacionm4_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACIONM4_C',
        ),
        4 => 
        array (
          'readonly' => false,
          'name' => 'sasa_correom4_c',
          'label' => 'LBL_SASA_CORREOM4',
        ),
        5 => 
        array (
          'readonly' => false,
          'name' => 'sasa_celularm4_c',
          'label' => 'LBL_SASA_CELULARM4',
        ),
        6 => 
        array (
          'readonly' => false,
          'name' => 'sasa_direccionprincm4_c',
          'studio' => 'visible',
          'label' => 'LBL_SASA_DIRECCIONPRINCM4',
        ),
        7 => 
        array (
          'readonly' => false,
          'name' => 'sasa_municipiom4_c',
          'label' => 'LBL_SASA_MUNICIPIOM4_C',
        ),
        8 => 
        array (
          'readonly' => false,
          'name' => 'sasa_departamentom4_c',
          'label' => 'LBL_SASA_DEPARTAMENTOM4_C',
        ),
        9 => 
        array (
          'readonly' => false,
          'name' => 'sasa_fechanacm4_c',
          'label' => 'LBL_SASA_FECHANACM4_C',
        ),
        10 => 
        array (
          'readonly' => false,
          'name' => 'sasa_razon_socialm4_c',
          'label' => 'LBL_SASA_RAZON_SOCIALM4_C',
        ),
        11 => 
        array (
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => false,
  ),
);