<?php
// created: 2023-04-25 03:50:38
$listViewDefs['Users'] = array (
  'name' => 
  array (
    'width' => '30',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'related_fields' => 
    array (
      0 => 'last_name',
      1 => 'first_name',
    ),
    'orderBy' => 'last_name',
    'default' => true,
  ),
  'user_name' => 
  array (
    'width' => '5',
    'label' => 'LBL_USER_NAME',
    'link' => true,
    'default' => true,
  ),
  'title' => 
  array (
    'width' => '15',
    'label' => 'LBL_TITLE',
    'link' => true,
    'default' => true,
  ),
  'department' => 
  array (
    'width' => '15',
    'label' => 'LBL_DEPARTMENT',
    'link' => true,
    'default' => true,
  ),
  'email' => 
  array (
    'width' => '30',
    'sortable' => false,
    'label' => 'LBL_LIST_EMAIL',
    'link' => true,
    'default' => true,
  ),
  'phone_work' => 
  array (
    'width' => '25',
    'label' => 'LBL_LIST_PHONE',
    'link' => true,
    'default' => true,
  ),
  'status' => 
  array (
    'width' => '10',
    'label' => 'LBL_STATUS',
    'link' => false,
    'default' => true,
  ),
  'sasa_superior1_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_SUPERIOR1_C',
    'width' => 10,
  ),
  'sasa_superior2_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SASA_SUPERIOR2_C',
    'width' => 10,
  ),
  'is_admin' => 
  array (
    'width' => '10',
    'label' => 'LBL_ADMIN',
    'link' => false,
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'editview' => false,
      'quickcreate' => false,
      'wirelesseditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => false,
  ),
  'reports_to_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_REPORTS_TO_NAME',
    'id' => 'REPORTS_TO_ID',
    'width' => 10,
    'default' => false,
  ),
  'LICENSE_TYPE' => 
  array (
    'width' => '20',
    'label' => 'LBL_LICENSE_TYPE',
    'link' => false,
    'default' => false,
  ),
);