<?php
// created: 2022-10-31 07:45:21
$viewdefs['Calls']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 
        array (
          'name' => 'sasa_objetivollamada_c',
          'label' => 'LBL_SASA_OBJETIVOLLAMADA_C',
        ),
        2 => 'status',
        3 => 'date_start',
        4 => 
        array (
          'name' => 'date_end',
          'comment' => 'Date is which call is scheduled to (or did) end',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_CALENDAR_END_DATE',
        ),
        5 => 
        array (
          'name' => 'reminder_time',
          'comment' => 'Specifies when a reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'label' => 'LBL_POPUP_REMINDER_TIME',
        ),
        6 => 
        array (
          'name' => 'email_reminder_time',
          'comment' => 'Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start',
          'studio' => 
          array (
            'recordview' => false,
            'wirelesseditview' => false,
          ),
          'label' => 'LBL_EMAIL_REMINDER_TIME',
        ),
        7 => 
        array (
          'name' => 'duration',
          'type' => 'fieldset',
          'orientation' => 'horizontal',
          'related_fields' => 
          array (
            0 => 'duration_hours',
            1 => 'duration_minutes',
          ),
          'label' => 'LBL_DURATION',
          'fields' => 
          array (
            0 => 
            array (
              'name' => 'duration_hours',
            ),
            1 => 
            array (
              'type' => 'label',
              'default' => 'LBL_HOURS_ABBREV',
              'css_class' => 'label_duration_hours hide',
            ),
            2 => 
            array (
              'name' => 'duration_minutes',
            ),
            3 => 
            array (
              'type' => 'label',
              'default' => 'LBL_MINSS_ABBREV',
              'css_class' => 'label_duration_minutes hide',
            ),
          ),
        ),
        8 => 'direction',
        9 => 'parent_name',
        10 => 'description',
        11 => 
        array (
          'name' => 'sasa_resultadollamada_c',
          'label' => 'LBL_SASA_RESULTADOLLAMADA_C',
        ),
        12 => 
        array (
          'name' => 'sasa_compromiso_c',
          'label' => 'LBL_SASA_COMPROMISO_C',
        ),
        13 => 
        array (
          'name' => 'sasa_responsableejecucion_c',
          'label' => 'LBL_SASA_RESPONSABLEEJECUCION_C',
        ),
        14 => 
        array (
          'name' => 'sasa_fechaproximallamada_c',
          'label' => 'LBL_SASA_FECHAPROXIMALLAMADA_C',
        ),
        15 => 'assigned_user_name',
        16 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        17 => 'tag',
      ),
    ),
  ),
);