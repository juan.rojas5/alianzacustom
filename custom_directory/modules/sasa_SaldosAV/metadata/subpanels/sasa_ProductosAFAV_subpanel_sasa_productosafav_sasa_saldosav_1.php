<?php
// created: 2019-01-04 21:59:07
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'accounts_sasa_saldosav_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SASA_SALDOSAV_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_sasa_saldosav_1accounts_ida',
  ),
  'sasa_nemotecnico_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NEMOTECNICO_C',
    'width' => 10,
  ),
  'sasa_fecharegistro_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHAREGISTRO_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_saldototal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_SALDOTOTAL_C',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);