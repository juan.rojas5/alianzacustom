<?php
// created: 2023-02-03 09:39:47
$viewdefs['Opportunities']['mobile']['view']['detail'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 'account_name',
        2 => 
        array (
          'name' => 'opportunity_type',
          'comment' => 'Type of opportunity (ex: Existing, New)',
          'label' => 'LBL_TYPE',
        ),
        3 => 'amount',
        4 => 'date_closed',
        5 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        6 => 
        array (
          'name' => 'lead_source',
          'comment' => 'Source of the opportunity',
          'label' => 'LBL_LEAD_SOURCE',
        ),
        7 => 
        array (
          'name' => 'sasa_nombrecompetidor_c',
          'label' => 'LBL_SASA_NOMBRECOMPETIDOR_C',
        ),
        8 => 'assigned_user_name',
        9 => 
        array (
          'name' => 'date_entered',
          'comment' => 'Date record created',
          'studio' => 
          array (
            'portaleditview' => false,
          ),
          'readonly' => true,
          'label' => 'LBL_DATE_ENTERED',
        ),
        10 => 
        array (
          'name' => 'sales_status',
          'readonly' => true,
          'studio' => true,
          'label' => 'LBL_SALES_STATUS',
        ),
        11 => 'tag',
        12 => 'sales_stage',
        13 => 'forecasted_likely',
        14 => 'commit_stage',
        15 => 'lost',
      ),
    ),
  ),
);