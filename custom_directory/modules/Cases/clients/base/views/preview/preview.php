<?php
$viewdefs['Cases'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              4 => 
              array (
                'name' => 'is_escalated',
                'type' => 'badge',
                'badge_label' => 'LBL_ESCALATED',
                'warning_level' => 'important',
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'case_number',
                'readonly' => true,
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_codigoqueja_c',
                'label' => 'LBL_SASA_CODIGOQUEJA_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipoentidad_c',
                'label' => 'LBL_SASA_TIPOENTIDAD_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_codigoentidad_c',
                'label' => 'LBL_SASA_CODIGOENTIDAD_C',
              ),
              4 => 'status',
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_escalamiento_dcf_c',
                'label' => 'LBL_SASA_ESCALAMIENTO_DCF_C',
              ),
              6 => 
              array (
                'name' => 'account_name',
              ),
              7 => 
              array (
                'name' => 'primary_contact_name',
              ),
              8 => 
              array (
                'readonly' => false,
                'name' => 'sasa_negocio_c',
                'label' => 'LBL_SASA_NEGOCIO_C',
              ),
              9 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radsfc_c',
                'label' => 'LBL_SASA_RADSFC_C',
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'sasa_raddefensorc_c',
                'label' => 'LBL_SASA_RADDEFENSORC_C',
              ),
              11 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radalianza_c',
                'label' => 'LBL_SASA_RADALIANZA_C',
              ),
              12 => 
              array (
                'readonly' => false,
                'name' => 'sasa_radalianzam2_c',
                'label' => 'LBL_SASA_RADALIANZAM2_C',
              ),
              13 => 
              array (
                'name' => 'sasa_sasa_tipificaciones_cases_1_name',
              ),
              14 => 
              array (
                'readonly' => false,
                'name' => 'sasa_quejaexpres_c',
                'label' => 'LBL_SASA_QUEJAEXPRES_C',
              ),
              15 => 
              array (
                'readonly' => false,
                'name' => 'sasa_ente_control_c',
                'label' => 'LBL_SASA_ENTE_CONTROL_C',
              ),
              16 => 
              array (
                'readonly' => false,
                'name' => 'sasa_producto_cod_c',
                'label' => 'LBL_SASA_PRODUCTO_COD_C',
              ),
              17 => 
              array (
                'name' => 'work_log',
                'comment' => 'Free-form text used to denote activities of interest',
                'label' => 'LBL_WORK_LOG',
              ),
              18 => 
              array (
                'readonly' => false,
                'name' => 'sasa_productodigital_c',
                'label' => 'LBL_SASA_PRODUCTODIGITAL_C',
              ),
              19 => 
              array (
                'readonly' => false,
                'name' => 'sasa_sfcfechacreacion_c',
                'label' => 'LBL_SASA_SFCFECHACREACION_C',
              ),
              20 => 'resolved_datetime',
              21 => 
              array (
                'readonly' => false,
                'name' => 'sasa_fechaactualizacionsfc_c',
                'label' => 'LBL_SASA_FECHAACTUALIZACIONSFC_C',
              ),
              22 => 
              array (
                'readonly' => false,
                'name' => 'sasa_recepcion_c',
                'label' => 'LBL_SASA_RECEPCION_C',
              ),
              23 => 
              array (
                'readonly' => false,
                'name' => 'sasa_puntorecepcion_c',
                'label' => 'LBL_SASA_PUNTORECEPCION_C',
              ),
              24 => 
              array (
                'readonly' => false,
                'name' => 'sasa_admision_c',
                'label' => 'LBL_SASA_ADMISION_C',
              ),
              25 => 
              array (
                'readonly' => false,
                'name' => 'sasa_rectificacion_c',
                'label' => 'LBL_SASA_RECTIFICACION_C',
              ),
              26 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tutela_c',
                'label' => 'LBL_SASA_TUTELA_C',
              ),
              27 => 
              array (
                'readonly' => false,
                'name' => 'sasa_desistimiento_c',
                'label' => 'LBL_SASA_DESISTIMIENTO_C',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_prorroga_c',
                'label' => 'LBL_SASA_PRORROGA_C',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_anexoqueja_c',
                'label' => 'LBL_SASA_ANEXOQUEJA_C',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_anexoresfinal_c',
                'label' => 'LBL_SASA_ANEXORESFINAL_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_docrespfinal_c',
                'label' => 'LBL_SASA_DOCRESPFINAL_C',
              ),
              4 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              5 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              6 => 'assigned_user_name',
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
