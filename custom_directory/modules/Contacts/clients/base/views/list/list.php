<?php
// created: 2023-02-03 09:39:47
$viewdefs['Contacts']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'title',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'account_name',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_parentescorelacion_c',
          'label' => 'LBL_SASA_PARENTESCORELACION_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        10 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'sasa_pais_c',
          'label' => 'LBL_SASA_PAIS_C',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'sasa_departamento_c',
          'label' => 'LBL_SASA_DEPARTAMENTO_C',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'sasa_municipio_c',
          'label' => 'LBL_SASA_MUNICIPIO_C',
          'enabled' => true,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'sasa_genero_c',
          'label' => 'LBL_SASA_GENERO_C',
          'enabled' => true,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'phone_other',
          'enabled' => true,
          'default' => true,
          'selected' => false,
        ),
        16 => 
        array (
          'name' => 'assistant_phone',
          'enabled' => true,
          'default' => true,
          'selected' => false,
        ),
      ),
    ),
  ),
);