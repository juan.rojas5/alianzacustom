<?php
$viewdefs["Leads"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
  'context' => array(
    'link' => 'prospect_list_leads',
  ),
);
