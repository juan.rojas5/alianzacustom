<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/fbsg_cc_jobs.php

$job_strings[] = 'update_cc_campaign_results';
//$job_strings[] = 'export_staged_cc_contacts';
$job_strings[] = 'sync_staged_lists';
$job_strings[] = 'automatic_contact_sync';

require_once ('modules/fbsg_ConstantContactIntegration/include/ConstantContact.php');
function update_cc_campaign_results() {
	require_once ('modules/Contacts/Contact.php');
	require_once ('modules/Leads/Lead.php');
	global $db;

	// Check if this is the first time running a full detail campaign sync
	// OR if the Client has reset the sync to do a full detail campaign sync
	$csbean = BeanFactory::getBean('fbsg_ConstantContactIntegration');
	$csquery = new SugarQuery;
	$csquery->select();
	$csquery->from($csbean);
	$csquery->where()->equals('deleted', '0');
	$csresults = $csquery->execute();

	if (isset($csresults[0])){
		if ($csresults[0]['first_auto_campaign_sync'] == true){
			// If this is true then first_campaign_sync MUST also be true in
			// order to sync ALL the campaigns
			$csreset_bean = BeanFactory::getBean('fbsg_ConstantContactIntegration',$csresults[0]['id']);
			$csreset_bean->first_campaign_sync = true;
			$csreset_bean->save();
		}
	}

	$sCC = SugarCC::GetOnlyCC();
	if (! $sCC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found');
		return true;
	}

	$ut = new CCBasic($sCC->name, $sCC->password);

	$ccMass = new SugarCCMass($ut);

	$ccMass->GetAllCampaignResults();

	if (isset($csresults[0])){
		if ($csresults[0]['first_auto_campaign_sync'] == true){
			// If this is true then this means that the full deatil campaign
			// sync completed successfully and now this flag can be set to false
			// so that the sync will only sync the last 90 days worth of campaign
			// details thereby reducing the resources needed for the automatic sync
			$csreset_bean = BeanFactory::getBean('fbsg_ConstantContactIntegration',$csresults[0]['id']);
			$csreset_bean->first_auto_campaign_sync = false;
			$csreset_bean->save();
		}
	}

	return true;
}
function sync_staged_lists() {
	$sCC = SugarCC::GetOnlyCC();
	if (! $sCC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found.');
		return true;
	}

	$ut = new CCBasic($sCC->name, $sCC->password);
	$ccMass = new SugarCCMass($ut);
	$results = $ccMass->SyncStagedLists();

    echo print_r($results, true);

	return true;
}

/**
 * Auto-Syncs new & updated contacts from Constant Contact daily.
 *
 * @return boolean
 */
function automatic_contact_sync() {
	global $db, $timedate;
	$CC = SugarCC::GetOnlyCC();
	if (! $CC) {
		CCLog::fatal('Sugar-CC integration expired, or no validated CC account found.');
		return true;
	}

	$ut = new CCBasic($CC->name, $CC->password);
	$incSync = new SugarIncrementalSync($ut, $CC->auto_import_module);

	$incSync->SyncAllLists();

	return true;
}

/**
 *
 *
 *
 */
function cc_people_update(){
	$results = fbsg_ConstantContactIntegration::updateCCListsAndPeople();
	if($results !== false){
		$admin = new \Administration();
		$admin->saveSetting('fbsgcci', 'cc_people_updated', 1);
	}
	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_fecha_actualizacion_cuenta.php

/*
* Funcion para planificador de tareas
* Caso sasa 5407 Cuentas - Funcionalidad campo "Fecha de Actualización"
* 
* Se requiere, para el módulo de cuentas, monitorear el campo "Fecha de Actualización" y 60 días antes del valor de este campo se debe marcar el cliente potencial.
*
* La idea es validar el campo "Fecha de Actualización" y poblar el campo "Ctrl WF" en el módulo de Cuentas con el valor "Proceso Fecha de Actualización", la funcionalidad se debe ejecutar, solo si el campo "Ctrl WF" está "Vacío"
*
* Un WF monitoreará la actualización de registros del CP con este valor en el campo "Ctrl WF" , para hacer un proceso de generación de tarea y notificación en caso de no gestionarse en 60 días.
*/
$job_strings[] = 'tarea_fecha_actualizacion_cuenta';
function tarea_fecha_actualizacion_cuenta(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_fecha_actualizacion_cuenta. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		require_once("custom/dias_actualizacion_account.php");
		$query= "
		SELECT
			id,
			accounts_cstm.sasa_ctrlwf_c as proceso,
			accounts_cstm.sasa_fechaactualizacion_c as fecha,
			accounts_cstm.sasa_ctrlanoactualizacion_c as periodo
		FROM
			accounts
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
			accounts.deleted = 0 
			AND( accounts_cstm.sasa_ctrlwf_c = '' OR accounts_cstm.sasa_ctrlwf_c IS NULL ) 
			AND accounts_cstm.sasa_fechaactualizacion_c IS NOT NULL 
			AND accounts_cstm.sasa_ctrlanoactualizacion_c != '{$sasa_ctrlanoactualizacion_c}'
			and DATE_SUB(accounts_cstm.sasa_fechaactualizacion_c , INTERVAL {$dias_actualizacion_account} DAY) = CURRENT_DATE
		";
		/*
		SELECT
			id,
			accounts_cstm.sasa_ctrlwf_c as proceso,
			accounts_cstm.sasa_fechaactualizacion_c as fecha,
			accounts_cstm.sasa_ctrlanoactualizacion_c as periodo
		FROM
			accounts
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
			accounts.deleted = 0 
			AND( accounts_cstm.sasa_ctrlwf_c = '' OR accounts_cstm.sasa_ctrlwf_c IS NULL ) 
			AND accounts_cstm.sasa_fechaactualizacion_c IS NOT NULL 
			AND accounts_cstm.sasa_ctrlanoactualizacion_c != '2018'
			and DATE_SUB(accounts_cstm.sasa_fechaactualizacion_c , INTERVAL 60 DAY) = CURRENT_DATE
		*/
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$id = $row['id'];
				$fecha = $row['fecha'];
				$GLOBALS['log']->security("registro id: {$id} fecha: {$fecha}"); 
				
				$bean = BeanFactory::getBean("Accounts", $id, array('disable_row_level_security' => true));
				$bean->sasa_ctrlanoactualizacion_c=$sasa_ctrlanoactualizacion_c;//periodo en el que se disparo el WF
				$bean->sasa_ctrlwf_c="Proceso Fecha Actualizacion";
				$bean->save();
				$bean = null;
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_fecha_actualizacion_cuenta. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_gestion_planeacion_mensual.php

/*
* Funcion para planificador de tareas
* Caso sasa 5481 Tarea Programada - Generación de tarea para gestionar "Planeación Mensual"Se requiere generar una tarea programada, que se ejecute el primer día de cada mes y asigne una tarea a cada usuario con rol de "Asesor Comerciales" con un ANS de 5 días.
* El asunto de la Tarea debe ser: "Realizar Planeación Mensual"
*/
$job_strings[] = 'tarea_gestion_planeacion_mensual';
function tarea_gestion_planeacion_mensual(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_gestion_planeacion_mensual. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query= "
		SELECT
			users.id as id,users.user_name as name
		FROM
			`acl_roles_users`
		LEFT JOIN acl_roles ON acl_roles.id = acl_roles_users.role_id
		LEFT JOIN users ON users.id = acl_roles_users.user_id
		WHERE
			acl_roles_users.role_id = 'dae9f6ca-8a41-11e8-a40c-06cd310e1a43' AND acl_roles_users.deleted = 0 AND acl_roles.deleted = 0 AND users.deleted = 0
		";
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$bean = BeanFactory::newBean("Tasks", "", array('disable_row_level_security' => true));
				
				$assigned_user_id = $row['id'];
				$name = $row['name'];	
				
				$bogota_tz = new DateTimeZone("America/Bogota");
				$utc_tz = new DateTimeZone("UTC");
				
				$date_start = new SugarDateTime($bean->date_start);//http://support.sugarcrm.com/Documentation/Sugar_Developer/Sugar_Developer_Guide_7.9/Architecture/DateTime/
				$date_start->setTimezone($bogota_tz);
				$date_start->setTime(0, 1);
				$date_start->setTimezone($utc_tz);
				
				$date_due = new SugarDateTime($bean->date_due);
				$date_due->setTimezone($bogota_tz);
				$date_due->modify("+5 days");
				$date_due->setTime(23, 59);
				$date_due->setTimezone($utc_tz);
				
				$GLOBALS['log']->security("name: {$name} id: {$assigned_user_id}"); 
				$GLOBALS['log']->security(    "$date_start: ",print_r( $date_start  ,true)  );
				$GLOBALS['log']->security(    "$date_due: ",print_r( $date_due  ,true)  );
				
				
				$bean->name="Realizar Planeación Mensual";
				$bean->date_start=$date_start->asDb();
				$bean->date_due=$date_due->asDb();
				$bean->assigned_user_id=$assigned_user_id;
				$bean->save();
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_gestion_planeacion_mensual. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_lead_crtl_postergado.php

/*
 * caso sasa 5696 Clientes Potenciales - Tarea programada Gestión "Postergados"
 * Se requiere para la instancia de AlianzaQAS, generar una tarea programada que identifique los registros del módulo de Clientes Potenciales, que en el campo "Crtl Postergado" equivalga a "Postergar" y ésta debe colocar en este campo el valor "vacío".
 * La periodicidad de ejecución de esta tarea debe ser una vez al día a las 23hrs (para efecto de pruebas podría configurarse cada 15 min)
*/
$job_strings[] = 'tarea_lead_crtl_postergado';
function tarea_lead_crtl_postergado(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_lead_crtl_postergado. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query= "
		SELECT
			id
		FROM
			leads
		LEFT JOIN leads_cstm ON id = id_c
		WHERE
			deleted = 0 AND id_c IS NOT NULL AND sasa_ctrlpostergado_c != ''
		";
		
		global $db;	
		$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
		$result = $db->query($query);
		$lastDbError = $db->lastDbError();				
		if(!empty($lastDbError)){
			$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
		}
		else{
			while($row = $db->fetchRow($result)){
				$bean = BeanFactory::getBean("Leads", $row['id'], array('disable_row_level_security' => true));
				if( !empty($bean->id) ){
					$GLOBALS['log']->security("tarea_lead_crtl_postergado lead {$bean->first_name}  {$bean->last_name} id:{$bean->id}"); 
					$bean->sasa_ctrlpostergado_c='';
					$bean->save();
				}
				else{
					$GLOBALS['log']->security("tarea_lead_crtl_postergado error no existe o esta eliminado lead {$row['id']}"); 
				}
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_lead_crtl_postergado. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_vencidas_task_calls_meetings.php

/*
* Funcion para planificador de tareas
* Caso sasa 5407 Cuentas - Funcionalidad campo "Fecha de Actualización"
* 
* Se requiere, para el módulo de cuentas, monitorear el campo "Fecha de Actualización" y 60 días antes del valor de este campo se debe marcar el cliente potencial.
*
* La idea es validar el campo "Fecha de Actualización" y poblar el campo "Ctrl WF" en el módulo de Cuentas con el valor "Proceso Fecha de Actualización", la funcionalidad se debe ejecutar, solo si el campo "Ctrl WF" está "Vacío"
*
* Un WF monitoreará la actualización de registros del CP con este valor en el campo "Ctrl WF" , para hacer un proceso de generación de tarea y notificación en caso de no gestionarse en 60 días.
*/
$job_strings[] = 'tarea_vencidas_task_calls_meetings';
function tarea_vencidas_task_calls_meetings(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_vencidas_task_calls_meetings. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$queries= array(
		//"select sasa_vencida_c,id, date_end,CURRENT_TIMESTAMP from meetings left join meetings_cstm on id = id_c where date_end < CURRENT_TIMESTAMP and status not in ('Held','Not Held')",
		//"select sasa_vencida_c,id, date_end,CURRENT_TIMESTAMP from calls left join calls_cstm on id = id_c where date_end < CURRENT_TIMESTAMP and status not in ('Held','Not Held')",
		//"select sasa_vencida_c,status, id, date_due,CURRENT_TIMESTAMP from tasks left join tasks_cstm on id = id_c where date_due < CURRENT_TIMESTAMP and status not in ('Completed')",
		"UPDATE meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c = meetings.id SET sasa_vencida_c='Si' WHERE date_end < utc_timestamp and status not in ('Held','Not Held')",
		"UPDATE calls_cstm INNER JOIN calls ON calls_cstm.id_c = calls.id SET sasa_vencida_c='Si' WHERE date_end < utc_timestamp and status not in ('Held','Not Held')",
		"UPDATE tasks_cstm INNER JOIN tasks ON tasks_cstm.id_c = tasks.id SET sasa_vencida_c='Si' WHERE date_due < utc_timestamp and status not in ('Completed')",
		);
		
		foreach( $queries as $query ){
			global $db;	
			$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
			$result = $db->query($query);
			$lastDbError = $db->lastDbError();				
			if(!empty($lastDbError)){
				$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
			}
			else{
				while($row = $db->fetchRow($result)){					
					$GLOBALS['log']->security("row: ".print_r( $row  ,true)); 
				}
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_vencidas_task_calls_meetings. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_consolidado_saldos_af.php


$job_strings[] = 'tarea_consolidado_saldos_af';
function tarea_consolidado_saldos_af(){
	$GLOBALS['log']->security("\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_consolidado_saldos_AF. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query="
			SELECT
				tipo,
				cuenta,
				categoria,
				SUM(saldo_total) AS saldo_total,
				descripcion,
				cantidad,
				id_cuenta,
				id_categoria,
				id_producto
			FROM (
					SELECT
						'Saldos AF' AS tipo,
						accounts.name AS cuenta,
						sasa_categorias.name AS categoria,
						SUBSTRING_INDEX(
							GROUP_CONCAT(
								sasa_saldosaf.sasa_saldoactual_c
								ORDER BY
									sasa_saldosaf.sasa_fechasaldo_c
								DESC
							),
							',',
							1
						) AS saldo_total,
						'--' AS descripcion,
						count(0) AS cantidad,
						accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida AS id_cuenta,
						sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida AS id_categoria,
						sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida AS id_producto
					FROM
						sasa_saldosaf
					LEFT JOIN accounts_sasa_saldosaf_1_c ON accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
					LEFT JOIN sasa_productosafav_sasa_saldosaf_1_c ON sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id
					LEFT JOIN sasa_categorias_sasa_productosafav_1_c ON sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_productosafav_idb = sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida
					LEFT JOIN sasa_categorias ON sasa_categorias.id = sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida
					LEFT JOIN accounts ON accounts.id = accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida
					WHERE
						sasa_saldosaf.deleted = 0 AND accounts_sasa_saldosaf_1_c.deleted = 0 AND sasa_productosafav_sasa_saldosaf_1_c.deleted = 0 AND sasa_categorias_sasa_productosafav_1_c.deleted = 0 AND sasa_categorias.deleted = 0 AND accounts.deleted = 0
					GROUP BY
						id_cuenta,
						id_categoria,
						id_producto
					ORDER BY
						id_cuenta,
						id_categoria

			) af
			GROUP BY
				id_cuenta,
				id_categoria,
				tipo
			ORDER BY
				tipo,
				id_cuenta,
				id_categoria,
				id_producto
		";
		$hoy = date("Y-m-d");
		global $db;
		$db->query("DELETE FROM sasas_saldosconsolidados WHERE name LIKE 'Saldos AF%'");//caso sasa 6430 Saldos - Visualización registro mas reciente |||| Se limina todo, ya que son muchos registros para mantener el historico de eliminados....
		$result = $db->query($query);
		while($row = $db->fetchByAssoc($result)){
			$sasaS_SaldosConsolidados = BeanFactory::newBean('sasaS_SaldosConsolidados', "", array('disable_row_level_security' => true));
			$sasaS_SaldosConsolidados->name = "{$row["tipo"]} de {$hoy} - {$row["categoria"]} ({$row["cantidad"]})";
			$sasaS_SaldosConsolidados->accounts_sasas_saldosconsolidados_1accounts_ida = $row["id_cuenta"];
			$sasaS_SaldosConsolidados->sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida = $row["id_categoria"];
			$sasaS_SaldosConsolidados->sasa_saldototal_c= $row["saldo_total"];
			$sasaS_SaldosConsolidados->description= $row["descripcion"];
			$sasaS_SaldosConsolidados->save();
			$GLOBALS['log']->security("\n{$sasaS_SaldosConsolidados->id} -> {$sasaS_SaldosConsolidados->name} = \${$sasaS_SaldosConsolidados->sasa_saldototal_c}");
		}
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_consolidado_saldos_AF. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n\n");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_consolidado_saldos_av.php


$job_strings[] = 'tarea_consolidado_saldos_av';
function tarea_consolidado_saldos_av(){
	$GLOBALS['log']->security("\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_consolidado_saldos_AV. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$query="
			SELECT
				tipo,
				cuenta,
				categoria,
				SUM(saldo_total) AS saldo_total,
				descripcion,
				cantidad,
				id_cuenta,
				id_categoria,
				id_producto
			FROM (
					SELECT
						'Saldos AV' AS tipo,
						accounts.name AS cuenta,
						sasa_categorias.name AS categoria,
						SUBSTRING_INDEX(
							GROUP_CONCAT(
								sasa_saldosav.sasa_saldototal_c
								ORDER BY
									sasa_saldosav.sasa_fecharegistro_c
								DESC
							),
							',',
							1
						) AS saldo_total,
						'--' AS descripcion,
						COUNT(0) AS cantidad,
						accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida AS id_cuenta,
						sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida AS id_categoria,
						sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida AS id_producto
					FROM
						sasa_saldosav
					LEFT JOIN accounts_sasa_saldosav_1_c ON accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id
					LEFT JOIN sasa_productosafav_sasa_saldosav_1_c ON sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id
					LEFT JOIN sasa_categorias_sasa_productosafav_1_c ON sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_productosafav_idb = sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida
					LEFT JOIN sasa_categorias ON sasa_categorias.id = sasa_categorias_sasa_productosafav_1_c.sasa_categorias_sasa_productosafav_1sasa_categorias_ida
					LEFT JOIN accounts ON accounts.id = accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida
					WHERE
						sasa_saldosav.deleted = 0 AND accounts_sasa_saldosav_1_c.deleted = 0 AND sasa_productosafav_sasa_saldosav_1_c.deleted = 0 AND sasa_categorias_sasa_productosafav_1_c.deleted = 0 AND sasa_categorias.deleted = 0 AND accounts.deleted = 0
					GROUP BY
						id_cuenta,
						id_categoria,
						id_producto
					ORDER BY
						id_cuenta,
						id_categoria
			) av
			GROUP BY
				id_cuenta,
				id_categoria,
				tipo
			ORDER BY
				tipo,
				id_cuenta,
				id_categoria,
				id_producto
		";
		$hoy = date("Y-m-d");
		global $db;
		$db->query("DELETE FROM sasas_saldosconsolidados WHERE name LIKE 'Saldos AV%'");//caso sasa 6430 Saldos - Visualización registro mas reciente |||| Se limina todo, ya que son muchos registros para mantener el historico de eliminados....
		$result = $db->query($query);
		while($row = $db->fetchByAssoc($result)){
			$sasaS_SaldosConsolidados = BeanFactory::newBean('sasaS_SaldosConsolidados', "", array('disable_row_level_security' => true));
			$sasaS_SaldosConsolidados->name = "{$row["tipo"]} de {$hoy} - {$row["categoria"]} ({$row["cantidad"]})";
			$sasaS_SaldosConsolidados->accounts_sasas_saldosconsolidados_1accounts_ida = $row["id_cuenta"];
			$sasaS_SaldosConsolidados->sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida = $row["id_categoria"];
			$sasaS_SaldosConsolidados->sasa_saldototal_c= $row["saldo_total"];
			$sasaS_SaldosConsolidados->description= $row["descripcion"];
			$sasaS_SaldosConsolidados->save();
			$GLOBALS['log']->security("\n{$sasaS_SaldosConsolidados->id} -> {$sasaS_SaldosConsolidados->name} = \${$sasaS_SaldosConsolidados->sasa_saldototal_c}");
		}
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_consolidado_saldos_AV. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n\n");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_consolidado_saldos.php


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_accounts_retraso_ctrl_wf.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'tarea_accounts_retraso_ctrl_wf';
function tarea_accounts_retraso_ctrl_wf(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_accounts_retraso_ctrl_wf. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		$query = "
		SELECT
		accounts_cstm.id_c AS idAccounts
		FROM
		accounts		
		LEFT JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c
		WHERE
		accounts.deleted = 0 AND
		accounts.account_type = 'Customer' AND
		(
		accounts_cstm.sasa_retrasoctrlworkflow_c = '' OR
		accounts_cstm.sasa_retrasoctrlworkflow_c IS NULL
		)
		;
		";

		global $db;
		$result = $db->query($query);
		while ($row = $db->fetchByAssoc($result)){
			$idAccount = $row['idAccounts'];
			if(!empty($idAccount))
			{
				$Account = BeanFactory::getBean('Accounts', $idAccount,array('disable_row_level_security' => true));
				if(!empty($Account->id)){
					$Account->sasa_retrasoctrlworkflow_c = '1';
					$Account->save();
					Registry\Registry::getInstance()->drop('triggered_starts');	
					$GLOBALS['log']->security("\n{$Account->id} -> {$Account->name} = Update RetrasoCtrlWorkFlow");
				}
			}
		}

		//Tarea 5860 Por favor actualizar los equipos de la cuentas cada vez que la integración la cambia a Global. Debe quedar con los equipos del usuario asignado.
		//El wf de CU es el que asigna los equipos 
		$queryAccountsGlobal = "SELECT accounts.id AS idAccountsGlobal FROM accounts INNER JOIN users ON accounts.assigned_user_id=users.id INNER JOIN users_cstm ON users.id=users_cstm.id_c WHERE accounts.team_id='1' AND accounts.team_set_id='1' AND accounts.deleted='0' AND accounts.account_type='Customer' AND (users_cstm.sasa_superior1_c != 'Gerente Regional Eje Cafetero' or users_cstm.sasa_superior1_c != '' or users_cstm.sasa_superior1_c IS NOT NULL) AND (accounts.assigned_user_id != '' OR accounts.assigned_user_id IS NOT NULL) AND users_cstm.sasa_superior1_c!='Gerente Regional Eje Cafetero'";

		$resultAccountsGlobal = $db->query($queryAccountsGlobal);
		while ($row = $db->fetchByAssoc($resultAccountsGlobal)) {
			$idAccountGlobal = $row["idAccountsGlobal"];

			$AccountGlobal = BeanFactory::getBean('Accounts', $idAccountGlobal,array('disable_row_level_security' => true));

			if (!empty($AccountGlobal->id)) {
				$AccountGlobal->sasa_retrasoctrlworkflow_c = '3';
				$AccountGlobal->save();
				Registry\Registry::getInstance()->drop('triggered_starts');	
				$GLOBALS['log']->security("\n{$AccountGlobal->id} Cuentas Team Global");
			}
		}
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_accounts_retraso_ctrl_wf. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/mover_relacion_accounts.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
//use Sugarcrm\Sugarcrm\ProcessManager\Registry;
$job_strings[] = 'mover_relacion_accounts';
function mover_relacion_accounts(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-mover_relacion_accounts. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return = true;
	try{
		//Conuslta para sacar el total agrupado por numeroid de las cuentas duplicadas
		$query = "SELECT accounts_cstm.id_c idcuentamayor, accounts_cstm.sasa_nroidentificacion_c numiden FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' group by accounts_cstm.sasa_nroidentificacion_c ORDER BY accounts.date_modified DESC";

		global $db;
		$cuentasmenores = array();
		$idcuentamayor = $db->query($query);
		//Ciclo para recorrer el total agrupado
		while ($row = $db->fetchByAssoc($idcuentamayor)) {
			$GLOBALS['log']->security("NumeroID del lote: {$row['numiden']}");
			$numid = $row['numiden'];
			//Consulta para sacar las cuentas con menor fecha de modificacion
			$querysubcuentas = "SELECT accounts_cstm.id_c, accounts_cstm.sasa_nroidentificacion_c, tabla.nomcu FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina, accounts.name nomcu FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' AND accounts_cstm.sasa_nroidentificacion_c='{$row['numiden']}' ORDER BY accounts.date_modified DESC LIMIT 1,5";
			//consulta para sacar el id de la cuenta con mayor fecha de modificacion
			$querycuentamayor = $db->query("SELECT accounts_cstm.id_c, accounts_cstm.sasa_nroidentificacion_c, tabla.nomcu FROM accounts_cstm INNER JOIN( SELECT COUNT( accounts_cstm.sasa_nroidentificacion_c ), accounts.id account_id, accounts_cstm.id_c cuentactm, accounts_cstm.sasa_nroidentificacion_c, accounts.deleted elimina, accounts.name nomcu FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' GROUP BY accounts_cstm.sasa_nroidentificacion_c HAVING COUNT( accounts_cstm.sasa_nroidentificacion_c ) > 1 ) AS tabla ON accounts_cstm.sasa_nroidentificacion_c = tabla.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c = accounts.id WHERE accounts.deleted = 0 AND accounts.account_type = 'Customer' AND accounts_cstm.sasa_nroidentificacion_c='{$row['numiden']}' ORDER BY accounts.date_modified DESC LIMIT 0,1");
			$idcuentamas = mysqli_fetch_array($querycuentamayor);
			$GLOBALS['log']->security("IDCUENTAMAYOR: {$idcuentamas['id_c']}");
			$idscuentasantiguas = $db->query($querysubcuentas);
			
			//ciclo para empezar a ejecutar los querys de reasignacion de registros relacionados
			while ($row2 = $db->fetchByAssoc($idscuentasantiguas)) {
				//array de queries 15 en total para reasignacion de registros relacionados a la cuenta con mayor fecha de modificacion
				$queries = array("UPDATE meetings SET meetings.parent_id='{$idcuentamas['id_c']}' WHERE meetings.parent_id='{$row2['id_c']}' AND meetings.deleted=0",
					"UPDATE calls SET calls.parent_id='{$idcuentamas['id_c']}' WHERE calls.parent_id='{$row2['id_c']}' AND calls.deleted=0",
					"UPDATE tasks SET tasks.parent_id='{$idcuentamas['id_c']}' WHERE tasks.parent_id='{$row2['id_c']}' AND tasks.deleted=0",
					"UPDATE notes SET notes.parent_id='{$idcuentamas['id_c']}' WHERE notes.parent_id='{$row2['id_c']}' AND notes.deleted=0",
					"UPDATE accounts_contacts SET accounts_contacts.account_id='{$idcuentamas['id_c']}' WHERE accounts_contacts.account_id='{$row2['id_c']}' AND accounts_contacts.deleted=0",
					"UPDATE accounts_opportunities SET accounts_opportunities.account_id='{$idcuentamas['id_c']}' WHERE accounts_opportunities.account_id='{$row2['id_c']}' AND accounts_opportunities.deleted=0",
					"UPDATE revenue_line_items SET revenue_line_items.account_id='{$idcuentamas['id_c']}' WHERE revenue_line_items.account_id='{$row2['id_c']}' AND revenue_line_items.deleted=0",
					"UPDATE leads SET leads.account_id='{$idcuentamas['id_c']}' WHERE leads.account_id='{$row2['id_c']}' AND leads.deleted=0",
					"UPDATE accounts_sasa_saldosaf_1_c SET accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida ='{$idcuentamas['id_c']}' WHERE accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_saldosaf_1_c.deleted=0",
					"UPDATE accounts_sasa_saldosav_1_c SET accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_saldosav_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosav_1_c SET accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosav_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosaf_1_c SET accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosaf_1_c.deleted=0",
					"UPDATE accounts_sasa_movimientosavdivisas_1_c SET accounts_sasa_movimientosavdivisas_1_c.accounts_sasa_movimientosavdivisas_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasa_movimientosavdivisas_1_c.accounts_sasa_movimientosavdivisas_1accounts_ida='{$row2['id_c']}' AND accounts_sasa_movimientosavdivisas_1_c.deleted=0",
					"UPDATE accounts_sasas_saldosconsolidados_1_c SET accounts_sasas_saldosconsolidados_1_c.accounts_sasas_saldosconsolidados_1accounts_ida='{$idcuentamas['id_c']}' WHERE accounts_sasas_saldosconsolidados_1_c.accounts_sasas_saldosconsolidados_1accounts_ida='{$row2['id_c']}' AND accounts_sasas_saldosconsolidados_1_c.deleted=0",
					"UPDATE emails SET emails.parent_id='{$idcuentamas['id_c']}' WHERE emails.parent_id='{$row2['id_c']}' AND emails.deleted=0",
					"UPDATE accounts SET accounts.deleted=1 WHERE accounts.id='{$row2['id_c']}'",
				);
				$GLOBALS['log']->security("IDSUBCUENTAS: {$row2['id_c']}");
				//forecah para empezar a ejecutar los queries del array $queries
				foreach ($queries as $query) {
					$querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
					$result = $db->query($querytotal);
					$lastDbError = $db->lastDbError();				
					if(!empty($lastDbError)){
						$GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
					}
					//$GLOBALS['log']->security("querytotal: {$querytotal}");
				}
			}
		}
		
	}
	catch (Exception $e) {
		$GLOBALS['log']->security("ERROR: ".$e->getMessage());
		$return=false;
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-mover_relacion_accounts. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************\n\n\n\n");
	return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_saldos_af_ultimo_registro.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_saldos_af_ultimo_registro';
function tarea_saldos_af_ultimo_registro(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_saldos_af_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;
    
    /******Saldos AF******/    
    $querySAF = "		
	UPDATE sasa_saldosaf LEFT JOIN (select sasa_saldosaf.name, sasa_saldosaf.id Idsaldo, accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida from sasa_saldosaf inner join accounts_sasa_saldosaf_1_c ON sasa_saldosaf.id=accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb GROUP BY sasa_saldosaf.name, sasa_saldosaf.assigned_user_id, accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida ORDER BY `sasa_saldosaf`.`sasa_fechasaldo_c` DESC)As tabla ON sasa_saldosaf.id=tabla.Idsaldo WHERE sasa_saldosaf.deleted=0 AND sasa_saldosaf.team_id != '' AND sasa_saldosaf.team_set_id !='' AND tabla.Idsaldo IS NULL
    ";
    
    $resultSAF = $db->query($querySAF);
    $getAffectedRowCount = $GLOBALS['db']->getAffectedRowCount($resultSAF)." ".$GLOBALS['db']->affected_rows;
    $GLOBALS['log']->security("- {$getAffectedRowCount}= SaldosAF Actualizados");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_saldos_af_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_saldos_av_ultimo_registro.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_saldos_av_ultimo_registro';
function tarea_saldos_av_ultimo_registro(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_saldos_av_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;  
    /******Saldos AV******/
    //285
    $querySAV = "    
	UPDATE sasa_saldosav LEFT JOIN (select sasa_saldosav.name, sasa_saldosav.id Idsaldo, accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida from sasa_saldosav inner join accounts_sasa_saldosav_1_c ON sasa_saldosav.id=accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb GROUP BY sasa_saldosav.name, sasa_saldosav.assigned_user_id, accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida ORDER BY `sasa_saldosav`.`sasa_fecharegistro_c` DESC)As tabla ON sasa_saldosav.id=tabla.Idsaldo WHERE sasa_saldosav.deleted=0 AND sasa_saldosav.team_id != '' AND sasa_saldosav.team_set_id !='' AND tabla.Idsaldo IS NULL
    ";
    $resultSAV = $db->query($querySAV);
    $getAffectedRowCount = $GLOBALS['db']->getAffectedRowCount($resultSAV)." ".$GLOBALS['db']->affected_rows;
    $GLOBALS['log']->security("- {$getAffectedRowCount}= SaldosAV Actualizados");
    
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_saldos_av_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_saldosafav_ultimo_registro.php

//obsoleto

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_meetings_compromiso.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_meetings_compromiso';
function tarea_meetings_compromiso(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_meetings_compromiso. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{
    global $mod_strings;
         global $current_user;
         global $sugar_config;
         global $locale;
    global $db;
    global $timedate;
    global $datetime;

    // $fecha = new DateTime(date("Y-m-d H:i:s"));
    // $fecha->modify('+5 hours');
    // $actual = $fecha->format('Y-m-d H:i:s');
    // $fecha->modify('+40 minute');
    // $menos30 = $fecha->format('Y-m-d H:i:s');


    // $timefecha = $timedate->getInstance()->nowDb();

    $fechasugar = new SugarDateTime();
    $actual = $fechasugar->formatDateTime("datetime", "db");
    
    $fechasugar->modify("+40 minute");
    $menos30 = $fechasugar->format("Y-m-d H:i:s");

    $GLOBALS['log']->security("*************FECHA OBJETO GLOBAL: {$actual} Y fecha DateTime {$menos30} ***********************\n");

    //$query = $db->query("SELECT meetings.assigned_user_id FROM meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c=meetings.id WHERE (meetings_cstm.sasa_fechaproximcompromiso_c BETWEEN '{$menos30}' AND '{$actual}') AND meetings.deleted=0 GROUP BY meetings.assigned_user_id;");
    //SELECT email_addresses.email_address FROM email_addr_bean_rel inner JOIN email_addresses ON email_addresses.id=email_addr_bean_rel.email_address_id WHERE email_addr_bean_rel.bean_id="a67e6c88-bedc-11e9-bc7b-000000000000";

    $query = $db->query("SELECT GROUP_CONCAT(users.first_name, ' ', users.last_name) AS nomuser, meetings_cstm.sasa_fechaproximcompromiso_c AS compromiso, meetings.parent_type AS parenttipo, meetings.parent_id AS parent, email_addresses.email_address, meetings.id AS emailid, meetings.assigned_user_id AS idassigned FROM meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c=meetings.id INNER JOIN users ON users.id=meetings.assigned_user_id INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=meetings.assigned_user_id INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id=email_addresses.id WHERE (meetings_cstm.sasa_fechaproximcompromiso_c BETWEEN '{$actual}' AND '{$menos30}') AND email_addr_bean_rel.deleted=0 AND meetings.deleted=0 AND meetings_cstm.sasa_envio_compro_c IS NULL GROUP BY meetings.assigned_user_id");
    $querycalls = $db->query("SELECT GROUP_CONCAT(users.first_name, ' ', users.last_name) AS nomuser, calls_cstm.sasa_fechaproximallamada_c AS compromiso, calls.parent_type AS parenttipo, calls.parent_id AS parent, email_addresses.email_address, calls.id AS emailid, calls.assigned_user_id AS idassigned FROM calls_cstm INNER JOIN calls ON calls_cstm.id_c=calls.id INNER JOIN users ON users.id=calls.assigned_user_id INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=calls.assigned_user_id INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id=email_addresses.id WHERE (calls_cstm.sasa_fechaproximallamada_c BETWEEN '{$actual}' AND '{$menos30}') AND email_addr_bean_rel.deleted=0 AND calls.deleted=0 AND calls_cstm.sasa_envio_compro_c IS NULL GROUP BY calls.assigned_user_id");

    //$queryemail = $db->query("SELECT email_addresses.email_address FROM email_addr_bean_rel inner JOIN email_addresses ON email_addresses.id=email_addr_bean_rel.email_address_id WHERE email_addr_bean_rel.bean_id='{$idsasiigned[]}'");

    //$email = mysqli_fetch_array($queryemail);
    //GROUP_CONCAT("'",meetings.assigned_user_id,"'")


    try {
        $usersemil=array();
      
        require_once 'modules/Emails/Email.php';
        require_once 'include/SugarPHPMailer.php';
        if (mysqli_num_rows($query)>=1) {
          $email_template = new EmailTemplate();
          $email_template->retrieve("f336fa98-f5a2-11e9-84b8-023ef1e03e82");

          while ($row = $db->fetchByAssoc($query)) {
            if ($row['parenttipo']=='Accounts') {
              $parent = BeanFactory::retrieveBean('Accounts',$row['parent'],array('disable_row_level_security' => true));
            }else{
              $parent = BeanFactory::retrieveBean('Leads',$row['parent'],array('disable_row_level_security' => true));
            }

            $fechasignado = new SugarDateTime($row['compromiso']);
            $fechasignado->modify('-5 hours');
            $asignadofecha = $fechasignado->format("H:i:s");

            // $fechacompromiso = new DateTime($row['compromiso']);
            // $fechacompromiso->modify('-5 hours');
            // $fecharealcom = $fechacompromiso->format('H:i:s');
            
            $url="https://alianza.sugarondemand.com/#Meetings/".$row['emailid'];
            $bodyhtml = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);
            $body = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);
            $email_obj = new Email();
            $defaults = $email_obj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults["email"];
            $mail->FromName = $defaults["name"];
            $mail->Subject = $email_template->subject;
            $mail->Body = from_html($bodyhtml);
            $mail->AltBody =from_html($body);
            $mail->prepForOutbound();
            $param = array();
            $param['asingenemail']=$row['email_address'];
            $mail->AddAddress($row['email_address']);
            $GLOBALS['log']->security("****REUNION {$row['email_address']} **********************\n");
            $actualizacompromisome=$db->query("UPDATE meetings_cstm SET meetings_cstm.sasa_envio_compro_c='si' WHERE meetings_cstm.id_c='{$row['emailid']}'");
             $GLOBALS['log']->security("****REUNION {$actualizacompromisome} **********************\n");
             if (@$mail->Send()) {
              }
          }
        }
        if (mysqli_num_rows($querycalls)>=1) {
          while ($row = $db->fetchByAssoc($querycalls)) {
            if ($row['parenttipo']=='Accounts') {
              $parent2 = BeanFactory::retrieveBean('Accounts',$row['parent'],array('disable_row_level_security' => true));
            }else{
              $parent2 = BeanFactory::retrieveBean('Leads',$row['parent'],array('disable_row_level_security' => true));
            }

            $fechasignado = new SugarDateTime($row['compromiso']);
            $fechasignado->modify('-5 hours');
            $asignadofecha = $fechasignado->format("H:i:s");
            
            $email_template = new EmailTemplate();
            $email_template->retrieve("babf2962-f5a5-11e9-9cf4-06f2b4fb7f46");

            $url="https://alianza.sugarondemand.com/#Calls/".$row['emailid'];
            $bodyhtml = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent2->name,$url),$email_template->body_html);
            $body = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);

            $email_obj = new Email();
            $defaults = $email_obj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults["email"];
            $mail->FromName = $defaults["name"];
            $mail->Subject = $email_template->subject;
            $mail->Body = from_html($bodyhtml);
            $mail->AltBody =from_html($body);
            $mail->prepForOutbound();
          
            $param = array();
            $param['asingenemail']=$row['email_address'];
            $mail->AddAddress($row['email_address']);
            $GLOBALS['log']->security("****LLAMADAS {$row['email_address']} **********************\n");
            $actualizacompromisoca=$db->query("UPDATE calls_cstm SET calls_cstm.sasa_envio_compro_c='si' WHERE calls_cstm.id_c='{$row['emailid']}'");
            if (@$mail->Send()) {
            }
          }
          
        }

    } catch (Exception $e) {
      
    }


    $GLOBALS['log']->security("****FECHA ACTUAL8 {$actual} Menos 30 {$menos30}**********************\n");
    $GLOBALS['log']->security("****QUERYRESULASSIGNED : {$idsasiigned[0]}**********************\n");
    $GLOBALS['log']->security("****QUERYEMAIL : {$email[0]}**********************\n");
    $GLOBALS['log']->security("****UserenvioEmail : {$usersemil['usersemail']}**********************\n");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_meetings_compromiso. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_mantenimiento8diasbd.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_eliminado_8dias';

function tarea_eliminado_8dias(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_eliminado_8dias. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{

    global $db;

    $queries = array("DELETE ignore accounts_sasa_movimientosaf_1_c FROM accounts_sasa_movimientosaf_1_c left outer join sasa_movimientosaf on accounts_sasa_movimientosaf_1_c.accounts_sasa_movimientosaf_1sasa_movimientosaf_idb = sasa_movimientosaf.id where sasa_movimientosaf.id is null",
          "DELETE ignore accounts_sasa_movimientosav_1_c FROM accounts_sasa_movimientosav_1_c left outer join sasa_movimientosav on accounts_sasa_movimientosav_1_c.accounts_sasa_movimientosav_1sasa_movimientosav_idb = sasa_movimientosav.id where sasa_movimientosav.id is null",
          "delete ignore accounts_sasa_saldosaf_1_c FROM accounts_sasa_saldosaf_1_c left outer join sasa_saldosaf on accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id where sasa_saldosaf.id is null",
          "delete ignore accounts_sasa_saldosav_1_c FROM accounts_sasa_saldosav_1_c left outer join sasa_saldosav on accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id where sasa_saldosav.id is null",
          "delete ignore sasa_productosafav_sasa_movimientosaf_1_c FROM sasa_productosafav_sasa_movimientosaf_1_c left outer join sasa_movimientosaf on sasa_productosafav_sasa_movimientosaf_1_c.sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb = sasa_movimientosaf.id where sasa_movimientosaf.id is null",
          "delete ignore sasa_productosafav_sasa_saldosaf_1_c FROM sasa_productosafav_sasa_saldosaf_1_c left outer join sasa_saldosaf on sasa_productosafav_sasa_saldosaf_1_c.sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb = sasa_saldosaf.id where sasa_saldosaf.id is null",
          "delete ignore sasa_productosafav_sasa_saldosav_1_c FROM sasa_productosafav_sasa_saldosav_1_c left outer join sasa_saldosav on sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id where sasa_saldosav.id is null",
        );

    //forecah para empezar a ejecutar los queries del array $queries
        foreach ($queries as $query) {
          $querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
          $result = $db->query($querytotal);
          $lastDbError = $db->lastDbError();        
          if(!empty($lastDbError)){
            $GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
          }
        }
    
    
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_eliminado_8dias. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_mantenimientodiariobd.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_eliminado_diario';
function tarea_eliminado_diario(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_Mantenimiento_diario. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{

    global $db;
    
        $queries = array("TRUNCATE TABLE activities",
          "TRUNCATE TABLE activities_users",
          "TRUNCATE TABLE sasa_saldosaf_audit",
          "TRUNCATE TABLE sasa_saldosav_audit",
          "TRUNCATE TABLE sasa_movimientosav_audit",
          "TRUNCATE TABLE sasa_movimientosaf_audit",
          "DELETE FROM tracker where tracker.date_modified <= date_sub(curdate(),INTERVAL 7 DAY)",
          "DELETE IGNORE  FROM sasa_movimientosaf WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE  FROM sasa_movimientosav WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE  FROM sasa_saldosaf WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE FROM accounts_audit WHERE date_created < date_sub(curdate(), interval 6 MONTH)",
        );

        //forecah para empezar a ejecutar los queries del array $queries
        foreach ($queries as $query) {
          $querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
          $result = $db->query($querytotal);
          $lastDbError = $db->lastDbError();        
          if(!empty($lastDbError)){
            $GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
          }
        }
    
    $GLOBALS['log']->security("****Hola Diario**********************\n");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_Mantenimiento_diario. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_convertedlead.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_convertedlead';
function tarea_convertedlead(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_convertedlead. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;

    $query_convert_lead = $db->query("SELECT leads.id idlead, accounts_cstm.id_c idcuenta, leads.status, leads.converted, leads_cstm.sasa_nroidentificacion_c numeroidlead, accounts_cstm.sasa_nroidentificacion_c numeroidcuent,accounts.assigned_user_id asignadocuenta, accounts.name nomcuenta FROM leads INNER JOIN leads_cstm ON leads.id=leads_cstm.id_c INNER JOIN accounts_cstm ON leads_cstm.sasa_nroidentificacion_c=accounts_cstm.sasa_nroidentificacion_c INNER JOIN accounts ON accounts_cstm.id_c=accounts.id WHERE leads.converted=0 and accounts.deleted=0 AND leads.deleted=0");
    


    while ($row = $db->fetchByAssoc($query_convert_lead)) {
	      $Account = BeanFactory::getBean('Accounts', $row['idcuenta'],array('disable_row_level_security' => true));
	      $Lead = BeanFactory::getBean('Leads', $row['idlead'],array('disable_row_level_security' => true));

	      //Bucar y/o establecer el contacto
	      $query_get_contact = $db->query("SELECT contacts_cstm.id_c FROM contacts_cstm INNER JOIN contacts ON contacts_cstm.id_c=contacts.id WHERE contacts_cstm.sasa_nroidentificacion_c='{$row['numeroidlead']}' AND contacts.deleted=0");

	      if (mysqli_num_rows($query_get_contact)>=1) {
	      	$id_contact = mysqli_fetch_array($query_get_contact);
	      	$Contact = BeanFactory::getBean('Contacts', $id_contact['id_c'],array('disable_row_level_security' => true));
	      }else{
	      	$Contact = new Contact();
		      foreach ($Lead->field_defs as $campo => $definicion) {
		          if ($campo != 'id' and $campo != 'date_entered' and $campo != 'date_modified') { //no incluir descripcion, caso 

		            $Contact->$campo = $Lead->$campo;
		          }
		      }
	      }
	      $Contact->save();
		  $Lead->contact_id=$Contact->id;
	      $Account->load_relationship("contacts"); //Ya se debe tener id de cuenta y contacto
			$Account->contacts->add($Contact->id);

	      


	      $Lead->account_id = $row['idcuenta'];
	      $Lead->account_name=$row["nomcuenta"];
	      $Lead->status = "In Process";

		  $Lead->converted=true;


		  //Mover llamadas y visitas
		  $querymov_call = $db->query("UPDATE calls SET parent_id='{$row['idcuenta']}', parent_type='Accounts', assigned_user_id='{$row['asignadocuenta']}',status='Held' WHERE parent_id='{$row['idlead']}'");
		  $querymov_meetings = $db->query("UPDATE meetings SET parent_id='{$row['idcuenta']}', parent_type='Accounts', assigned_user_id='{$row['asignadocuenta']}',status='Held' WHERE parent_id='{$row['idlead']}'");
		  $query_delete_relation_lead_call = $db->query("UPDATE calls_leads SET deleted = '1' WHERE calls_leads.lead_id = '{$row['idlead']}';");
		  $query_delete_relation_lead_meetings = $db->query("UPDATE meetings_leads SET deleted = '1' WHERE meetings_leads.lead_id = '{$row['idlead']}';");

		  $Lead->save();


        $GLOBALS['log']->security(" Cuenta: {$Account->id}******************************************\n");
        $GLOBALS['log']->security(" Lead: {$Lead->id}******************************************\n");


    }
      
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_convertedlead. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/tarea_temp_accounts_canastas.php

/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_temp_accounts_canastas';
function tarea_temp_accounts_canastas(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_temp_accounts_canastas. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;
    
    /******Saldos AF******/    
    $queryaccounts_canastas = "		
  	SELECT IFNULL(accounts.id,'') primaryid
      ,IFNULL(accounts.name,'') accounts_name
      ,IFNULL(l1.id,'') l1_id
      ,IFNULL(l1.first_name,'') l1_first_name
      ,IFNULL(l1.last_name,'') l1_last_name
      ,IFNULL(l1.title,'') l1_title
      FROM accounts
      LEFT JOIN  users l1 ON accounts.assigned_user_id=l1.id AND l1.deleted=0
      LEFT JOIN  teams l2 ON accounts.team_id=l2.id AND l2.deleted=0
       WHERE (((((l1.description = 'Canastas.'))) AND (((l2.id<>'449f5dc4-43a1-11eb-8457-0286beac7abe'
      ) AND (l2.id<>'4dda40a2-43a1-11eb-b1d6-069273903c1e'
      ) AND (l2.id<>'57124e62-43a1-11eb-a3b5-069273903c1e'
      ) AND (l2.id<>'611f0666-43a1-11eb-973b-0286beac7abe'
      ))))) 
      AND  accounts.deleted=0 ";
    
    $result = $db->query($queryaccounts_canastas);
    while($row = $db->fetchRow($result)){
      $bean = BeanFactory::getBean("Accounts", $row['primaryid'], array('disable_row_level_security' => true));
      $bean->sasa_retrasoctrlworkflow_c="3";
      $bean->account_type="Customer";
      $bean->save();
    }
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_temp_accounts_canastas. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/ScheduleTaskVacations.php


$job_strings[] = 'ScheduleTaskVacations';
function ScheduleTaskVacations()
{
    global $db;
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio ScheduleTaskVacations. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    //Tarea 16425

    //Consulta para obtener quien está en vacaciones.
    $query="SELECT * FROM users_cstm WHERE users_cstm.sasa_envacaciones_c=1 and users_cstm.sasa_controlvacaciones_c=0";
    $result = $db->query($query);
    while($row = $db->fetchByAssoc($result)){
        if(!empty($row['id_c'])){
            //Validar que fecha sea igual a hoy
            $today = new DateTime(); // This object represents current date/time
            $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $row['sasa_fechainicio_c']);
            $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $diff = $today->diff( $match_date );
            $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

            if ($diffDays==0) {
                //Optener quien sale de vacaciones custom
                $Sale_Vacaciones = BeanFactory::getBean($users_cstm, $row['id_c']);
                //optener equipo privado de quien sale a vacaciones
                $queryTeam="SELECT * FROM `teams` WHERE associated_user_id='{$row['id_c']}'";
                $resultTeam = $db->query($queryTeam);
                while($rowTeam = $db->fetchByAssoc($resultTeam)){
                    if(!empty($rowTeam['id'])){
                        $PrivateTeam=$rowTeam['id'];
                    }
                }
                //Asignar equipo privado al reemplazante
                $Remplazante = BeanFactory::getBean($users, $row['user_id_c']);
                //Load the team relationship 
                $Remplazante->load_relationship('teams');
                //Add the teams
                $Remplazante->teams->add(
                    array(
                        $PrivateTeam
                    )
                );
                //guardar
                $Remplazante->save();
                //Marcacion para evitar leer dos veces
                $Sale_Vacaciones->sasa_controlvacaciones_c=1;
                $Sale_Vacaciones->save();
            }
        }
    }


    //Consulta para obtener quien vuelve de vacaciones con base al campo fecha fin
    $query="SELECT * FROM users_cstm WHERE users_cstm.sasa_envacaciones_c=1 and users_cstm.sasa_controlvacaciones_c=1";
    $result = $db->query($query);
    while($row = $db->fetchByAssoc($result)){
        if(!empty($row['id_c'])){
            //Validar que fecha sea igual a hoy
            $today = new DateTime(); // This object represents current date/time
            $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $row['sasa_fechafin_c']);
            $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

            $diff = $today->diff( $match_date );
            $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

            if ($diffDays==0) {
                //Optener quien sale a vacaciones
                $Sale_Vacaciones = BeanFactory::getBean($users_cstm, $row['id_c']);
                //optener equipo privado de quien sale a vacaciones
                $queryTeam="SELECT * FROM `teams` WHERE associated_user_id='{$row['id_c']}'";
                $resultTeam = $db->query($queryTeam);
                while($rowTeam = $db->fetchByAssoc($resultTeam)){
                    if(!empty($rowTeam['id'])){
                        $PrivateTeam=$rowTeam['id'];
                    }
                }
                //Optener el remplazante
                $Remplazante = BeanFactory::getBean($users, $row['user_id_c']);
                //Retirar equipo privado al remplazante del entrante
                //Load the team relationship 
                $Remplazante->load_relationship('teams');

                //Remove the teams
                $Remplazante->teams->remove(
                    array(
                        $PrivateTeam 
                    )
                );
                //guardar
                $Remplazante->save();
                //retirar marcacion para evitar leer dos veces
                $Sale_Vacaciones->sasa_controlvacaciones_c=0;
                $Sale_Vacaciones->save();   
            }
            
        }
    }



    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin ScheduleTaskVacations. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/UpdateLeadsStatus.php


$job_strings[] = 'UpdateLeadsStatus';

function UpdateLeadsStatus()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Leads Status and Converted 1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    //Realación de leads a account y status != Converted  or  converted = 0
    $query="SELECT l.id as idLeads
                FROM leads l
                INNER JOIN accounts a ON a.id = l.account_id
                WHERE (l.status != 'Converted' OR l.converted = 0 ) AND l.deleted = 0";

    $result = $GLOBALS['db']->query($query);
    $cantConsulta = mysqli_num_rows($result);

    //$arrayConsulta = array();
    //$cont = 0;
    $GLOBALS['log']->security("-------EJECUTAR SIGUIENTE QUERY-----------------");
    $GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        
                    /*$arrayConsulta = $row['idLeads'];
                    $cont++;*/
            //$GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $row['idLeads']);
            
            $beanStatus = BeanFactory::retrieveBean("Leads", $row['idLeads']);

            //Fields to update
            $beanStatus->status = 'Converted';
            $beanStatus->converted = 1;

            //Save
            $beanStatus->save();


    }//while

    $GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    /*foreach ($arrayConsulta as $idLeads) {
        // code...
            


    }*/
    



    /*$result2 = $GLOBALS['db']->query($query);
    $cantConsulta2 = mysqli_num_rows($result2);

     $GLOBALS['log']->security("-----CANTDAD DE REGISTROS NUEVOS RELACIONADOS----" . $cantConsulta2);*/



    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Leads Status and Converted 1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/UpdateLeads.php


$job_strings[] = 'UpdateLeads';

function UpdateLeads()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Leads. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    //Realación de leads a account con where l.delete = 0 y usuario no asignado/libre diferente
    $query="SELECT l.id as idLead 
            FROM leads l
            INNER JOIN accounts a ON l.account_id = a.id
            INNER JOIN users u ON l.assigned_user_id = u.id
            WHERE l.deleted = 0 and u.user_name != 'No Asignado/Libre'";

    $result = $GLOBALS['db']->query($query);
    $cantConsulta = mysqli_num_rows($result);


    //$GLOBALS['log']->security("-------EJECUTAR SIGUIENTE QUERY-----------------");
    //$GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            
            $beanLeads = BeanFactory::retrieveBean("Leads", $row['idLead']);

            //Fields to update
            //$beanStatus->update_date_modified = true;
          
            //Save
            $beanLeads->save();


    }//while

    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Leads ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/update_retraso_ctrl_wf_canastas.php

$job_strings[] = 'update_retraso_ctrl_wf_canastas';
require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

function update_retraso_ctrl_wf_canastas() {
    global $db;
    $GLOBALS['log']->security("Inicio-Actualiza retrasoctrlworkflow en Canastas-> ".date("Y-m-d h:i:s"));

    $sql_accounts="SELECT accounts.id AS id";
    $sql_accounts.=" FROM accounts INNER JOIN users ON accounts.assigned_user_id=users.id";
    $sql_accounts.=" WHERE users.description='Canastas.' AND accounts.date_entered > '2019-05-03' AND accounts.deleted=0;";
    $query_result = $db->query($sql_accounts);
    
    while($rows = $db->fetchByAssoc($query_result)){
        $id=$rows["id"];
        //Obtener fecha de primera compra(Sera la fecha de creacion del cliente)
        $account_bean = BeanFactory::getBean('Accounts',$id);
        $account_bean->sasa_retrasoctrlworkflow_c ='3';
        $account_bean->save();
	}
    $GLOBALS['log']->security("Fin-Actualiza retrasoctrlworkflow en Canastas--> ".date("Y-m-d h:i:s"));

	return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/UpdateAccountAssignedTeamPrivate.php


$job_strings[] = 'UpdateAccountAssignedTeamPrivate';


function UpdateAccountAssignedTeamPrivate()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Account Assigned Team Private.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    /*
    *
    CONSULTA PARA TRAER CUENTAS usuario_asiagando != equipo privado y que no esten los siquientes equipos Canastas: Medellin 1, Medellin 2, Bogota 1, Bogota 2 
    *
    */
    $cantRel = 0;
    $query = "SELECT
                a.id AS ID_CUENTA, 
                a.name AS CUENTA,
                u.id AS ID_USERS,
                u.first_name AS USUARIO,
                t.name AS EQUIPO
            FROM
                accounts a
            LEFT JOIN(
                SELECT
                    a1.id AS ID_A
                FROM
                    accounts a1
                INNER JOIN team_sets_teams tst1 ON tst1.team_set_id = a1.team_set_id
                INNER JOIN teams t1 ON  t1.id = tst1.team_id
                WHERE
                    t1.id = '449f5dc4-43a1-11eb-8457-0286beac7abe' OR t1.id = '4dda40a2-43a1-11eb-b1d6-069273903c1e' OR t1.id = '57124e62-43a1-11eb-a3b5-069273903c1e' OR t1.id = '611f0666-43a1-11eb-973b-0286beac7abe' AND a1.deleted = 0 AND t1.deleted = 0
            ) tabla1 ON a.id = tabla1.ID_A
            LEFT JOIN(
                SELECT
                    a2.id AS ID_A2
                FROM
                    accounts a2
                INNER JOIN users u2 ON  u2.id = a2.assigned_user_id
                INNER JOIN team_sets_teams tst2 ON  tst2.team_set_id = a2.team_set_id
                INNER JOIN teams t2 ON  t2.id = tst2.team_id
                WHERE t2.private = 1 AND u2.deleted = 0 AND a2.deleted = 0 AND t2.deleted = 0
            ) tabla2 ON a.id = tabla2.ID_A2
            INNER JOIN users u ON u.id = a.assigned_user_id
            INNER JOIN team_sets_teams tst ON tst.team_set_id = a.team_set_id
            INNER JOIN teams t ON  t.id = tst.team_id
            WHERE
                tabla1.ID_A IS NULL AND tabla2.ID_A2 IS NULL AND t.private != 1 AND a.assigned_user_id != '' AND a.assigned_user_id IS NOT NULL AND u.deleted = 0 AND a.deleted = 0 AND t.deleted = 0 
                    GROUP BY a.id LIMIT 2000";

    $result = $GLOBALS['db']->query($query);

    $cantidadQuery = mysqli_num_rows($result);

    $GLOBALS['log']->security("***Cantidad Consulta=>***" . $cantidadQuery);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
    {
       $GLOBALS['log']->security("Id Cuenta=>" . $row['ID_CUENTA'] . "name=>" . $row['CUENTA'] . "Id User=>" . $row['ID_USERS']);

       // Consulta de Equipos de las cuentas para asignar el equipo privado
       $queryTeamPrivate = "SELECT t.id AS ID_TEAM FROM teams t WHERE t.associated_user_id = '{$row['ID_USERS']}' AND t.private = 1 AND t.deleted = 0";

           $resultTeam = $GLOBALS['db']->query($queryTeamPrivate);

           $cantidadTeam = mysqli_num_rows($resultTeam);

           $GLOBALS['log']->security("***Cantidad Consulta Teams=>***" . $cantidadTeam);

           while ($row2 = $GLOBALS['db']->fetchByAssoc($resultTeam))
           {
                $cantRel++;
            $GLOBALS['log']->security("id Teams=>" . $row2['ID_TEAM'] . "id cuenta=>" . $row['ID_CUENTA']);
                //Retrieve the bean
                $bean = BeanFactory::getBean("Accounts", $row['ID_CUENTA']);

                //Load the team relationship 
                $bean->load_relationship('teams');

                //Add the teams
                $bean->teams->add(
                    array(
                        $row2['ID_TEAM']
                    )
                );

           }
    }

    $GLOBALS['log']->security("Cantidad Relacionadas=>" .$cantRel);
    $GLOBALS['log']->security("---------------TAREA EJECUTADA-----------------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Account Assigned Team Private. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/SfcCreateComplaints.php


use Sugarcrm\Sugarcrm\Util\Uuid;

$job_strings[] = 'SfcCreateComplaints';
function SfcCreateComplaints() {
	$GLOBALS['log']->security("\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("START-SfcCreateComplaints-Momento1. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");

	$return = true;
	try {
		require_once("custom/sasa_projects/sfc/SfcRestApi.php");
		require_once('custom/sasa_projects/sfc/config/config.php');

//++++++++++++++++++++++++MOMENTO 1+++++++++++++++++++++++++++++++++

		$GLOBALS['log']->security("++++ALIANZA VALORES MOMENTO 1:++++");
		getComplaintsFromSfcApi(new SfcRestApi('valores', $config));
		
		$GLOBALS['log']->security("++++ALIANZA FIDUCIARIA MOMENTO 1:++++");
		getComplaintsFromSfcApi(new SfcRestApi('fiduciaria', $config));
//++++++++++++++++++++++++MOMENTO 4+++++++++++++++++++++++++++++++++

		$GLOBALS['log']->security("#####ALIANZA VALORES MOMENTO 4:######");
		getUserFromSfcApi(new SfcRestApi('valores', $config));
		
		$GLOBALS['log']->security("#####ALIANZA FIDUCIARIA MOMENTO 4:######");
		getUserFromSfcApi(new SfcRestApi('fiduciaria', $config));
		
	} catch (Exception $e) {
		$return = false;
		$GLOBALS['log']->security("SFC-Exception: ".$e->getMessage());
	}

	$GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin SfcCreateComplaints Momento1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return $return;
}
//TRAER USUARIOS DE LA SUPER.
function getUserFromSfcApi($sfcRestApi){

	$cuenta_id;

	$processedUserIds = array();

	$sfcUserResponse = $sfcRestApi->getUserInfo();
	//array de prueba.
	/*$sfcUserResponse = array("response" => array(
		"count"=>1,
		"pages"=>1,
		"results"=>array(
			"numero_id_CF"=>1096187206,
			"tipo_id_CF"=>"Cedula de Ciudadania",
			"nombres"=>"Jhon",
			"apellido"=>"Mantilla",
			"fecha_nacimiento"=>"1987-01-27",
			"correo"=>"jhon.mantilla@sasaconsultoria.com",
			"telefono"=>3043440883,
			"razon_social"=>"jhon devs",
			"direccion"=>"Calle 52 a # 34c 192",
			"departamento_cod"=>"",
			"municipio_cod"=>"",
			)
		)
	);*/


	$GLOBALS['log']->security("Response User:" . print_r($sfcUserResponse, true));
	
	$accountFromSfc2 = json_decode($sfcUserResponse['response']);
	
	/*$accountFromSfc = json_encode($sfcUserResponse['response']);

	$accountFromSfc2 = json_decode($accountFromSfc);*/

	$GLOBALS['log']->security("Response account:" . print_r($accountFromSfc2, true));

	if ($accountFromSfc2->count > 0) {
	
	$GLOBALS['log']->security("Entro count > 1:#: ".$accountFromSfc2->count);
		
		foreach ($accountFromSfc2->results as $index => $accountUser) {
			//$accoutId = getAccountByIdCF($accountUser);
			$GLOBALS['log']->security("###id Cuenta::::".$accountUser->numero_id_CF);
			$cuenta_id = getAccountByUser($accountUser);
			//$GLOBALS['log']->security("###index: " . " datos: " . $accountUser);

			$processedUserIds[] = $accountUser->numero_id_CF;
		}

		
		$GLOBALS['log']->security("Array id User:" . print_r($processedUserIds, true));
		$sfcAckResponse = $sfcRestApi->userAck($processedUserIds);
		$GLOBALS['log']->security("ackUserResponse: " .$sfcAckResponse['response']);
	}	

}
//############Quejas para casos##########
function getComplaintsFromSfcApi($sfcRestApi) {
	$sfcComplaintsResponse = $sfcRestApi->getComplaints();
	$GLOBALS['log']->security("getComplaints: {$sfcComplaintsResponse['response']}");
	$complaintsFromSfc = json_decode($sfcComplaintsResponse['response']);
	$processedComplaintIds = array();
	if ($complaintsFromSfc->count > 0) {

		foreach ($complaintsFromSfc->results as $index => $complaint) {
			$caseId = checkCaseBySfcId($complaint->codigo_queja);
			
			if (empty($caseId)) {
				$caseId = createNewCase($complaint);
			} 

			if ($complaint->anexo_queja) {
				$GLOBALS['log']->security("Complaint has file complaintId: {$complaint->codigo_queja}, creating note");
				$sfcComplaintFileResponse = $sfcRestApi->getFilesComplaint($complaint->codigo_queja);
				$newNote = createNoteWithAttchment($caseId, $complaint->codigo_queja, $sfcComplaintFileResponse['response']);
			}

			$processedComplaintIds[] = $complaint->codigo_queja;
		} 
		$sfcAckResponse = $sfcRestApi->postAck($processedComplaintIds);
		$GLOBALS['log']->security("acKResponse: " .$sfcAckResponse['response']);

		
	} else {
		$GLOBALS['log']->security("SFC: There aren't complaints to report");
	}
}

function createNewCase($complaint) {
	$module = 'Cases';

	$newCase = BeanFactory::newBean($module);
	$newCase->sasa_tipoentidad_c = $complaint->tipo_entidad; 
	$newCase->sasa_codigoentidad_c = $complaint->entidad_cod;
	$newCase->sasa_sfcfechacreacion_c = date('Y-m-d h:i:s', strtotime($complaint->fecha_creacion . ' + 5 hours'));
	$newCase->sasa_codigoqueja_c = $complaint->codigo_queja;
	$newCase->sasa_canal_c = $complaint->canal_cod;
	$newCase->sasa_producto_cod_c = $complaint->producto_cod;
	$newCase->work_log = $complaint->producto_nombre;
	$newCase->description = $complaint->texto_queja;
	$newCase->sasa_anexoqueja_c = $complaint->anexo_queja;
	$newCase->sasa_tutela_c = $complaint->tutela;
	$newCase->sasa_ente_control_c = $complaint->ente_control;
	$newCase->sasa_escalamiento_dcf_c = $complaint->escalamiento_DCF;
	$newCase->sasa_replica_c = $complaint->replica;
	$newCase->resolution = $complaint->argumento_replica;
	$newCase->sasa_desistimiento_c = $complaint->desistimiento_queja;
	$newCase->sasa_quejaexpres_c = $complaint->queja_expres;
	$newCase->sasa_condicionespecial_c = $complaint->condicion_especial;
	$newCase->sasa_direccionsfc_c = $complaint->direccion;
	$newCase->source = 'SFC';

	$newCase->save();
	$accountId = getAccountByIdCF($complaint);
	$contactdId = getContactByIdCFAndAccountId($complaint, $accountId);
	$newCase->primary_contact_id = $contactdId;
	$newCase->load_relationship("accounts"); 
	$newCase->accounts->add($accountId);
	$newCase->load_relationship("sasa_sasa_tipificaciones_cases_1");
	$newCase->sasa_sasa_tipificaciones_cases_1->add(
		syncDataConf('typifications', $complaint->macro_motivo_cod));

	$newCase->save();

	$GLOBALS['log']->security("Case created id: " . $newCase->id);

	return $newCase->id;
}

function urlGetContent ($Url) {
    if (!function_exists('curl_init')){ 
        $errorMessage = "SFC: there isn't installed cURL";
		throw new Exception($errorMessage);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function createFileRelatedToNote($noteId, $urlContent) {
	$content = urlGetContent($urlContent);
	$imageFile = 'upload://' . $noteId;
	$fh = new UploadStream();
	$fh->stream_open($imageFile, 'w');
	$fh->stream_write($content);
	$fh->stream_close();
}

function createNoteWithAttchment($caseId, $complaintId, $sfcComplaintFileResponse) {
	if (empty($caseId) OR empty($sfcComplaintFileResponse) OR empty($complaintId)) {
		$errorMessage = "SFC: caseId: {$caseId} OR sfcComplaintFileResponse: {$sfcComplaintFileResponse} OR complaintId: {$complaintId} can't be empty createNoteWithAttchment";
		throw new Exception($errorMessage);
	}
	$GLOBALS['log']->security("Responsefile is " . $sfcComplaintFileResponse);
	$sfcFileResponseObject = json_decode($sfcComplaintFileResponse);
	if ($sfcFileResponseObject->count > 0) {
		$module = 'Notes';	
		$parentNote = BeanFactory::newBean($module);
		$parentNoteId = Uuid::uuid4();
		$parentNote->name = 'Attachments from SFC - '.$complaintId;
		$parentNote->new_with_id = true;
		$parentNote->parent_type = 'Cases';
		$parentNote->parent_id = $caseId;
		$parentNote->save();
		
		$childArrayIds = array();
		foreach ($sfcFileResponseObject->results as $key => $sfcImgObject) {

			$newNote = BeanFactory::newBean($module);
			$noteId = Uuid::uuid4();
			$newNote->id = $noteId;
			$newNote->new_with_id = true;

			createFileRelatedToNote($noteId, $sfcImgObject->file);

			$newNote->file_mime_type = get_file_mime_type("upload://{$noteId}");

			$pathVariables = parse_url($sfcImgObject->file)['path'];
			$fileName = substr($pathVariables, strrpos($pathVariables, '/') + 1);
			$newNote->name = $complaintId ." - " .$fileName;
			$newNote->sasa_referencia_c = $sfcImgObject->reference;
			$newNote->sasa_id_sfc_c = $sfcImgObject->id;
			$newNote->file_ext = substr($fileName, strpos($fileName, '.') + 1);
			$newNote->filename = $fileName;
			$newNote->attachment_flag = 1;
			$newNote->note_parent_id = $parentNote->id;
			$note->entry_source = 'internal';

			$newNote->save();
			$childArrayIds[] = $newNote->id;
			$GLOBALS['log']->security("Note created id: " . $newNote->id);
		}
		$description = "Se han relacionado {$sfcFileResponseObject->count} adjuntos de la SFC:\n";
		$description .= implode("\n", $childArrayIds);
		$parentNote->description = $description;
		$parentNote->save();
	}
}


function syncDataConf($dataType, $indexCode) {
	require('custom/sasa_projects/sfc/config/config.php');

	if ($dataType == 'typifications') {
		if ($indexCode == 1 ) $indexCode = 907;
		else if ($indexCode == 2 ) $indexCode = 926;
		else if ($indexCode == 5 ) $indexCode = 901;
		else if ($indexCode == 6 ) $indexCode = 903;
		else if ($indexCode == 7 ) $indexCode = 905;
		else if ($indexCode == 8 ) $indexCode = 914;
		else if ($indexCode == 11 ) $indexCode = 916;
		else if ($indexCode == 12 ) $indexCode = 917;
		else if ($indexCode == 13 ) $indexCode = 918;
		else if ($indexCode == 15 ) $indexCode = 948;
		else if ($indexCode == 16 ) $indexCode = 921;
		else if ($indexCode == 17 ) $indexCode = 923;
		else if ($indexCode == 18 ) $indexCode = 928;
		else if ($indexCode == 21 ) $indexCode = 951;
		else if ($indexCode == 22 ) $indexCode = 959;
		else if ($indexCode == 26 ) $indexCode = 942;
		else if ($indexCode == 29 ) $indexCode = 404;
		else if ($indexCode == 35 ) $indexCode = 942;
		else if ($indexCode == 47 ) $indexCode = 515;
		else if ($indexCode == 48 ) $indexCode = 515;
		else if ($indexCode == 49 ) $indexCode = 515;
		else if ($indexCode == 50 ) $indexCode = 402;
		else if ($indexCode == 51 ) $indexCode = 408;
		else if ($indexCode == 99 ) $indexCode = 499;
		else if ($indexCode == 20 ) $indexCode = 939;
		else if ($indexCode == 67 ) $indexCode = 946;
		else if ($indexCode == 52 ) $indexCode = 501;
		else if ($indexCode == 53 ) $indexCode = 511;
		else if ($indexCode == 55 ) $indexCode = 504;
		else if ($indexCode == 56 ) $indexCode = 505;
		else if ($indexCode == 60 ) $indexCode = 511;
		else if($indexCode == 61 ) $indexCode = 512;
	}

	$sugarValue = $config['sync'][$dataType][$indexCode];
	if (empty($sugarValue)) {
		switch ($dataType) {
			case 'typifications':
				$sugarValue = 'f0c450c6-b9db-11ec-9148-901b0efe306a';
				break;														
			default:
				$sugarValue = $indexCode;
				break;
		}
	}

	return $sugarValue;
}

function checkCaseBySfcId($complaintId){
	global $db;

	$query="SELECT cases_cstm.id_c
	FROM cases 
	INNER JOIN cases_cstm ON cases.id = cases_cstm.id_c
	WHERE cases_cstm.sasa_codigoqueja_c = '{$complaintId}' AND cases.deleted <> 1 
	LIMIT 1";
    $result = $db->query($query);
    $caseId = NULL;
    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id_c'])){
    		$GLOBALS['log']->security("Complaint exists {$row['id_c']}");
    		$caseId = $row['id_c'];
    	} 
    }

    return $caseId;
}
//###############MOMENTO 4.####################
function getAccountByUser($account) {

	$doc = $account->numero_id_CF;
	$module = 'Contacts';
	$contactId = '';
    $accountId = '';
	
	$GLOBALS['log']->security("#####get Account document :######".$doc);

	global $db;

	//Consulta para revisar si la cuenta tiene relacionado el contacto
	$query="SELECT c.id
		FROM accounts a
		INNER JOIN accounts_cstm ON accounts_cstm.id_c = a.id 
		INNER JOIN accounts_contacts ac ON ac.account_id = a.id
		INNER JOIN contacts c ON c.id = ac.contact_id
		INNER JOIN contacts_cstm cc ON cc.id_c = c.id
		WHERE accounts_cstm.sasa_nroidentificacion_c= '{$doc}' AND a.deleted <> 1 AND c.deleted = 0 AND cc.sasa_nroidentificacion_c = '{$doc}' AND ac.deleted = 0
		LIMIT 1";

    $result = $db->query($query);


    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id'])){
    		$GLOBALS['log']->security("Contacts exists user: {$row['id']}");
    		$contactId = $row['id'];
    	} 
    }



	if(empty($contactId)){
		//####Consulto si existe un contacto.######
		$contactIdExist = '';

		$queryContact = "SELECT c.id 
		FROM contacts c 
		INNER JOIN contacts_cstm cc ON cc.id_c = c.id
		WHERE cc.sasa_nroidentificacion_c = '{$doc}' AND c.deleted = 0";
		
		$resultContact = $db->query($queryContact);

		while($row = $db->fetchByAssoc($resultContact)){
	    
	    if(!empty($row['id'])){
	    		$GLOBALS['log']->security("Contact exists {$row['id']}");
	    		$contactIdExist = $row['id'];
	    	} 
	    }

		if(empty($contactIdExist)){
			//Empiezo crear contacto
			$GLOBALS['log']->security("#####Create Contacts empty: contacts id:######");
		    $bean = BeanFactory::newBean($module);
			
			$bean->first_name = $account->nombre;
			$bean->last_name = $account->apellido;
			$bean->sasa_tipoidentificacion_c = syncDataConf('documentType', $account->tipo_id_CF);
			$bean->sasa_nroidentificacion_c = $account->numero_id_CF;

		}else{
			$GLOBALS['log']->security("#####UPDATE Contacts empty: sin relación:######");
			$bean = BeanFactory::retrieveBean($module, $contactIdExist);
		}

		//Consulto la cuenta para la relación
		$query="SELECT accounts.id 
		FROM accounts 
		INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id 
		WHERE accounts_cstm.sasa_nroidentificacion_c= '{$doc}' AND accounts.deleted <> 1 
		LIMIT 1";

	    $result = $db->query($query);
	   

	    while($row = $db->fetchByAssoc($result)){
	    	if(!empty($row['id'])){
	    		$GLOBALS['log']->security("Account exists {$row['id']}");
	    		$accountId = $row['id'];
	    	} 
	    }


	}else{

		$GLOBALS['log']->security("#####Update Contacts :######".$contactId);

		$bean = BeanFactory::retrieveBean($module, $contactId);
	}

		//Populate bean fields
		//$bean->name = $account->nombre ." ". $account->apellido;
		$bean->sasa_nombrem4_c = $account->nombre;
		$bean->sasa_apellidom4_c = $account->apellido;
		$bean->sasa_nroidentificacionm4_c = $account->numero_id_CF;
		$bean->sasa_tipoidentificacionm4_c = syncDataConf('documentType', $account->tipo_id_CF);
		$bean->sasa_correom4_c = $account->correo;
		$bean->sasa_celularm4_c = $account->telefono;
		$bean->sasa_direccionprincm4_c  = $account->direccion;
		$bean->sasa_municipiom4_c = syncDataConf('cities', $account->municipio_cod);
		$bean->sasa_departamentom4_c = syncDataConf('states', $account->departamento_cod);
		$bean->sasa_fechanacm4_c = $account->fecha_nacimiento;
		$bean->sasa_razon_socialm4_c = $account->razon_social;
		/*if(!empty($accountId)){
			$bean->load_relationship('accounts');
            $bean->account_id->add($accountId);
		}*/
		//Save
		$bean->save();
		
		$contactId = $bean->id;
	
		if(!empty($accountId)){

			$GLOBALS['log']->security("Contact Relacionado o Creado: ".$contactId);

			$bean = BeanFactory::getBean('Accounts', $accountId);
			$bean->account_id = $accountId;
			$bean->load_relationship('contacts');
            $bean->contacts->add($contactId);
		}
		
		return $contactId;

}
function getAccountByIdCF($complaint) {
	global $db;

	$query="SELECT accounts.id 
	FROM accounts 
	INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id 
	WHERE accounts_cstm.sasa_nroidentificacion_c= '{$complaint->numero_id_CF}' AND accounts.deleted <> 1 
	LIMIT 1";
    $result = $db->query($query);
    $accountId = '';
    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id'])){
    		$GLOBALS['log']->security("Account exists {$row['id']}");
    		$accountId = $row['id'];
    	} 
    }
    if(empty($accountId)){
		$module = 'Accounts';
		$newAccount = BeanFactory::newBean($module);

		$newAccount->sasa_pais_c = syncDataConf('countries', $complaint->codigo_pais); //sync //account
		$newAccount->sasa_departamento_c = syncDataConf('states', $complaint->departamento_cod); //sync //account
		$newAccount->sasa_municipio_c = syncDataConf('cities', $complaint->municipio_cod); 
		$newAccount->sasa_tipoidentificacion_c = syncDataConf('documentType', $complaint->tipo_id_CF); //account
		$newAccount->sasa_nroidentificacion_c = $complaint->numero_id_CF; //account
		$newAccount->sasa_telefono_c = $complaint->telefono;	//account
		$newAccount->email1 = $complaint->correo;	//account
		$newAccount->sasa_tipopersona_c = syncDataConf('personType', $complaint->tipo_persona); //account	

		$newAccount->name = $complaint->nombres;
		$newAccount->description = 'Cliente no registra en el CRM.';
		$newAccount->account_type = 'No Registra';

		$newAccount->save();

		$accountId = $newAccount->id;
		$GLOBALS['log']->security("There isn't account, new account: {$accountId}");
    }

    return $accountId;
}

function getContactByIdCFAndAccountId($complaint, $accountId) {
	global $db;
	$contactId = '';
	$query="SELECT contacts.id 
			FROM contacts 
			INNER JOIN contacts_cstm ON contacts_cstm.id_c = contacts.id 
			WHERE contacts_cstm.sasa_nroidentificacion_c = '{$complaint->numero_id_CF}' 
			LIMIT 1";
    $result = $db->query($query);
    $lastDbError = $db->lastDbError();
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}");
    } else {
    	while($row = $db->fetchByAssoc($result)){
    		$contactId = $row['id'];
	    }

	    $module = 'Contacts';
	    if(!empty($contactId)) {
    		$GLOBALS['log']->security("Contact exists {$contactId}");

    		$contact = BeanFactory::getBean($module, $contactId);
    		$contact->sasa_lgbtiq_c = $complaint->lgbtiq;
			$contact->sasa_genero_c = $complaint->sexo;
			$contact->sasa_condicionespecial_c = $complaint->condicion_especial;
			$contactId = $contact->id;
			$contact->save();
    	} else {
			$newContact = BeanFactory::newBean($module);
			$newContact->primary_address_country = syncDataConf('countries', $complaint->codigo_pais);
			$newContact->primary_address_state = syncDataConf('states', $complaint->departamento_cod);
			$newContact->primary_address_city = syncDataConf('cities', $complaint->municipio_cod);
			$newContact->sasa_tipoidentificacion_c = syncDataConf('documentType', $complaint->tipo_id_CF); 
			$newContact->sasa_nroidentificacion_c = $complaint->numero_id_CF;
			$newContact->sasa_telefono_c = $complaint->telefono;
			$newContact->email1 = $complaint->correo;	

			$newContact->sasa_lgbtiq_c = $complaint->lgbtiq;
			$newContact->sasa_genero_c = syncDataConf('gender', $complaint->lgbtiq);;
			$newContact->sasa_condicionespecial_c = $complaint->condicion_especial;

			$newContact->first_name = $complaint->nombres;

			$newContact->description = 'Contacto no registra en el CRM.';

			$newContact->save();

			$contactId = $newContact->id;
			$GLOBALS['log']->security("Not exists contact {$newContact->id}");
    	}
    }
    
    return $contactId;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/MarkCFVFieldNotes.php


$job_strings[] = 'MarkCFVFieldNotes';

function MarkCFVFieldNotes()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Marcado de campo CFV en Notas.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    $dif = calcularDifInNotes();

    date_default_timezone_set('America/Bogota'); // configurando por defecto zona horaria de Colombia

    $dateYesterday = date('Y-m-d', strtotime('-1 day')); // obtener fecha día anterior
    $dateToday = date('Y-m-d');

    // Fecha inicio y fin para tareas vencidas
    $startDateMoment1 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateYesterday 06:00:00"))); // establecer fecha inicio
    $endDateMoment1 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment1))); // establecer fecha fin

    // fecha inicio y fin para tareas proximas a vencer
    $startDateMoment2 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateToday 06:00:00")));
    $endDateMoment2 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment2)));

    $queryVencidas = "SELECT n.id, n.name, nc.sasa_fechavencimiento_c, nc.sasa_cfv_c
        FROM notes AS n
        INNER JOIN notes_cstm AS nc ON nc.id_c = n.id
        WHERE nc.sasa_fechavencimiento_c > '{$startDateMoment1}'
        AND nc.sasa_fechavencimiento_c <= '{$endDateMoment1}'
        AND nc.sasa_cfv_c IS null AND n.deleted = 0
    LIMIT 1000";

    $queryAVencer = "SELECT n.id, n.name, nc.sasa_fechavencimiento_c, nc.sasa_cfv_c
        FROM notes AS n
        INNER JOIN notes_cstm AS nc ON nc.id_c = n.id
        WHERE nc.sasa_fechavencimiento_c > '{$startDateMoment2}'
        AND nc.sasa_fechavencimiento_c <= '{$endDateMoment2}'
        AND nc.sasa_cfv_c IS null AND n.deleted = 0
    LIMIT 1000";

    $resultVencidas = $GLOBALS['db']->query($queryVencidas);
    $resultAVencer = $GLOBALS['db']->query($queryAVencer);

    $contQuery = mysqli_num_rows($resultVencidas);
    $contQuery2 = mysqli_num_rows($resultAVencer);

    $GLOBALS['log']->security("Cantidad de notas vencidas ---> ".$contQuery."\n");
    $GLOBALS['log']->security("Cantidad de notas a vencer ---> ".$contQuery2."\n");

    while ($row = $GLOBALS['db']->fetchByAssoc($resultVencidas)) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' ) 
        {
            $bean = BeanFactory::getBean("Notes", $row['id']);
            $bean->sasa_cfv_c = 1;
            $bean->save();
        }
    }

    while ( $row = $GLOBALS['db']->fetchByAssoc($resultAVencer) ) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' ) 
        {
            $bean = BeanFactory::getBean("Notes", $row['id']);
            $bean->sasa_cfv_c = 2;
            $bean->save();
        }
    }

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Marcado de campo CFV en Notas.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

function calcularDifInNotes()
{
    $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

    $timeZoneBogota = 'America/Bogota';
    $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

    $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
    $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

    if ( $dayCentral > $dayColombia ) 
    {
        $GLOBALS['log']->security("Día de fecha central es mayor");
        $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
        $hourColombia = intval($dateColombia->format('h'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    }
    else if( $dayCentral == $dayColombia ) 
    {
        $GLOBALS['log']->security("Días iguales");
        $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
        $hourColombia = intval($dateColombia->format('H:i:s'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    } 
    else 
    {
        $GLOBALS['log']->security("Algo paso");
        $dif = 0;
        $GLOBALS['log']->security("Diferencia horaria: 0");
        return $dif;
    }
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/cleanDatabase.php


$job_strings[] = 'cleanDatabase';


function cleanDatabase()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Clean database. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    /*ejecutaQueryModulos("SELECT l.id AS ID_MODULOS FROM leads l
    WHERE l.deleted=0 AND l.id NOT IN (
    SELECT l.id 
    FROM leads l
    INNER JOIN accounts a ON a.id = l.account_id
    WHERE a.id = '101f5b14-507b-11e9-9985-02dfd714a754' OR 
    a.id = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
    a.id = '814d94ea-5090-11ed-9f94-0286beac7abe' OR  
    a.id = '21c2c574-b240-11e8-8982-02b8ffe67613' OR 
    a.id = '055ddb88-1f14-11eb-b585-02dfd714a754' OR
    a.id = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR 
    a.id = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
    a.id = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR 
    a.id = '4bb49a26-2433-11eb-bf96-065a187ca958' OR
    a.id = 'e610841c-57c1-11e9-9f87-065a187ca958' OR
    a.id = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR 
    a.id = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR
    a.id = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR
    a.id = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR 
    a.id = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR 
    a.id = '9638697a-6834-11eb-8ae3-02fb8f607ac4') LIMIT 500","Leads");*/
    
    
    /*ejecutaQueryModulos("SELECT c.id AS ID_MODULOS FROM contacts c
    WHERE c.deleted = 0 AND c.id NOT IN (SELECT c.id FROM contacts c
    INNER JOIN accounts_contacts ac ON ac.contact_id = c.id
    INNER JOIN accounts a ON a.id = ac.account_id
    WHERE c.deleted = 0 AND a.id = '101f5b14-507b-11e9-9985-02dfd714a754' OR 
    a.id = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
    a.id = '814d94ea-5090-11ed-9f94-0286beac7abe' OR 
    a.id = '21c2c574-b240-11e8-8982-02b8ffe67613' OR 
    a.id = '055ddb88-1f14-11eb-b585-02dfd714a754' OR
    a.id = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR 
    a.id = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
    a.id = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR 
    a.id = '4bb49a26-2433-11eb-bf96-065a187ca958' OR
    a.id = 'e610841c-57c1-11e9-9f87-065a187ca958' OR
    a.id = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR 
    a.id = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR
    a.id = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR
    a.id = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR 
    a.id = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR 
    a.id = '9638697a-6834-11eb-8ae3-02fb8f607ac4') LIMIT 500","Contacts");

    ejecutaQueryModulos("SELECT c.id AS ID_MODULOS
        FROM calls c
        WHERE c.deleted = 0 AND c.id NOT IN (SELECT m.id FROM calls m
        INNER JOIN accounts a ON a.id = m.parent_id 
        WHERE m.deleted = 0 AND a.deleted = 0 AND a.id = '101f5b14-507b-11e9-9985-02dfd714a754' OR 
        a.id = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
        a.id = '814d94ea-5090-11ed-9f94-0286beac7abe' OR 
        a.id = '21c2c574-b240-11e8-8982-02b8ffe67613' OR 
        a.id = '055ddb88-1f14-11eb-b585-02dfd714a754')
        AND c.id NOT IN (SELECT m.id FROM calls m 
        INNER JOIN accounts a ON a.id = m.parent_id
        WHERE m.deleted = 0 AND a.deleted = 0 AND a.id = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR 
        a.id = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
        a.id = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR 
        a.id = '4bb49a26-2433-11eb-bf96-065a187ca958')
        AND c.id NOT IN (SELECT m.id FROM calls m 
        INNER JOIN accounts a ON a.id = m.parent_id
        WHERE m.deleted = 0 AND a.deleted = 0 AND a.id = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR 
        a.id = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR
        a.id = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR 
        a.id = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR 
        a.id = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR 
        a.id = '9638697a-6834-11eb-8ae3-02fb8f607ac4')
        AND c.id NOT IN (SELECT m.id FROM calls m INNER JOIN accounts a ON a.id = m.parent_id
        WHERE m.deleted = 0 AND a.deleted = 0 AND a.id = 'e610841c-57c1-11e9-9f87-065a187ca958') LIMIT 500","Calls");*/

    ejecutaQueryModulos("SELECT n.id AS ID_MODULOS 
    FROM notes n 
    LEFT JOIN (
    SELECT n.id AS id_notas FROM notes n 
    WHERE n.filename != ''
    ORDER BY n.date_entered DESC
    LIMIT 16) tabla ON tabla.id_notas = n.id
    WHERE n.deleted = 0 AND tabla.id_notas IS NULL
    ORDER BY n.id DESC LIMIT 500","Notes");

    ejecutaQueryModulos("SELECT m.id AS ID_MODULOS
            FROM meetings m
            WHERE m.deleted = 0 
            AND m.id NOT IN(SELECT m.id FROM meetings m INNER JOIN accounts a ON a.id = m.parent_id WHERE m.deleted = 0 
            AND a.id = '101f5b14-507b-11e9-9985-02dfd714a754' OR a.id = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
            a.id = '814d94ea-5090-11ed-9f94-0286beac7abe' OR 
            a.id = '21c2c574-b240-11e8-8982-02b8ffe67613' OR a.id = '055ddb88-1f14-11eb-b585-02dfd714a754') 
            AND m.id NOT IN(SELECT m.id FROM meetings m INNER JOIN accounts a ON a.id = m.parent_id WHERE m.deleted = 0 AND
            a.id = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR a.id = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
            a.id = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR a.id = '4bb49a26-2433-11eb-bf96-065a187ca958') 
            AND m.id NOT IN(SELECT m.id FROM meetings m INNER JOIN accounts a ON a.id = m.parent_id WHERE m.deleted = 0 AND
            a.id = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR a.id = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR
            a.id = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR a.id = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR 
            a.id = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR a.id = '9638697a-6834-11eb-8ae3-02fb8f607ac4') 
            AND m.id NOT IN(SELECT m.id FROM meetings m INNER JOIN accounts a ON a.id = m.parent_id WHERE m.deleted = 0 AND
            a.id = 'e610841c-57c1-11e9-9f87-065a187ca958' 
            ORDER BY m.date_entered DESC) LIMIT 500","Meetings");

    ejecutaQueryModulos("SELECT ss.id AS ID_MODULOS FROM sasa_saldosav ss WHERE ss.deleted = 0 AND ss.id NOT IN (
        SELECT ass.accounts_sasa_saldosav_1sasa_saldosav_idb FROM accounts_sasa_saldosav_1_c ass
        WHERE ass.deleted=0 AND 
        (ass.accounts_sasa_saldosav_1accounts_ida = '101f5b14-507b-11e9-9985-02dfd714a754' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
         ass.accounts_sasa_saldosav_1accounts_ida = '814d94ea-5090-11ed-9f94-0286beac7abe' OR
         ass.accounts_sasa_saldosav_1accounts_ida = '21c2c574-b240-11e8-8982-02b8ffe67613' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '055ddb88-1f14-11eb-b585-02dfd714a754' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '4bb49a26-2433-11eb-bf96-065a187ca958' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = 'e610841c-57c1-11e9-9f87-065a187ca958' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR
         ass.accounts_sasa_saldosav_1accounts_ida = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR 
         ass.accounts_sasa_saldosav_1accounts_ida = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR
         ass.accounts_sasa_saldosav_1accounts_ida = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR
         ass.accounts_sasa_saldosav_1accounts_ida = '9638697a-6834-11eb-8ae3-02fb8f607ac4' 
        ))LIMIT 500","sasa_SaldosAV");

        ejecutaQueryModulos("SELECT a.id AS ID_MODULOS 
    FROM accounts a 
    WHERE a.deleted = 0 AND a.id NOT IN (
    SELECT a.id FROM accounts a
    WHERE a.id = '101f5b14-507b-11e9-9985-02dfd714a754' OR 
    a.id = '3e8f44de-8684-11e9-bdab-06ab610ac1b0' OR
    a.id = '814d94ea-5090-11ed-9f94-0286beac7abe' OR 
    a.id = '21c2c574-b240-11e8-8982-02b8ffe67613' OR 
    a.id = '055ddb88-1f14-11eb-b585-02dfd714a754' OR 
    a.id = 'b44af0dc-b241-11e8-94fc-02b8ffe67613' OR 
    a.id = '0288da16-b242-11e8-a576-0699afa2cd79' OR 
    a.id = 'be2cb2c4-496e-11e9-9900-02dfd714a754' OR 
    a.id = '4bb49a26-2433-11eb-bf96-065a187ca958' OR
    a.id = 'e610841c-57c1-11e9-9f87-065a187ca958' OR
    a.id = 'aacd8592-b241-11e8-8afc-02b8ffe67613' OR 
    a.id = '2da591a8-3b18-11e9-97b8-023ef1e03e82' OR
    a.id = '70dcf22a-1199-11e9-9b3c-061691c47ae2' OR
    a.id = '1de00e50-b22f-11e8-874c-02b8ffe67613' OR 
    a.id = 'd51e5068-1260-11e9-a0d7-023ef1e03e82' OR 
    a.id = '9638697a-6834-11eb-8ae3-02fb8f607ac4') LIMIT 500","Accounts");

    $GLOBALS['log']->security("---------TAREA EJECUTADA------------");

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Clean database ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}


function ejecutaQueryModulos($query, $modulo){

        $cantidad = 0;

        //$GLOBALS['log']->security("modulo=>" . $modulo);
        
        //$GLOBALS['log']->security("QUERY=>" . $query);
        
        $result = $GLOBALS['db']->query($query);

        $cant = mysqli_num_rows($result);

        $GLOBALS['log']->security("Cantidad Resultado=>" . $cant);

       while ($row = $GLOBALS['db']->fetchByAssoc($result)) 
        {
            // code...
            $cantidad ++;
            $GLOBALS['log']->security("id {$modulo}=>" . $row['ID_MODULOS']);
            //Retrieve bean
            $bean = BeanFactory::retrieveBean($modulo, $row['ID_MODULOS']);

            //Set deleted to true
            $bean->mark_deleted($row['ID_MODULOS']);

            //Save
            $bean->save();
        }

        $GLOBALS['log']->security("TOTAL : {$modulo}=>" . $cantidad);

}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/MarkFieldCFVTasks.php


$job_strings[] = 'MarkFieldCFVTasks';

function MarkFieldCFVTasks()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Marcado de campo CFV en Tareas.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    $dif = calcularDifInTasks();

    date_default_timezone_set('America/Bogota');

    $dateYesterday = date('Y-m-d', strtotime('-1 day')); // obtener fecha día anterior
    $dateToday = date('Y-m-d');

    // Fecha inicio y fin para tareas vencidas
    $startDateMoment1 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateYesterday 07:30:00"))); // establecer fecha inicio
    $endDateMoment1 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment1))); // establecer fecha fin

    // fecha inicio y fin para tareas proximas a vencer
    $startDateMoment2 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateToday 07:30:00")));
    $endDateMoment2 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment2)));

    $queryVencidas = "SELECT t.id, t.name, t.date_due, tc.sasa_cfv_c
        FROM tasks AS t
        INNER JOIN tasks_cstm AS tc ON tc.id_c = t.id
        WHERE t.date_due > '{$startDateMoment1}'
        AND t.date_due <= '{$endDateMoment1}'
        AND tc.sasa_cfv_c IS null AND t.deleted = 0
    LIMIT 1000";

    $queryAVencer = "SELECT t.id, t.name, t.date_due, tc.sasa_cfv_c
        FROM tasks AS t
        INNER JOIN tasks_cstm AS tc ON tc.id_c = t.id
        WHERE t.date_due > '{$startDateMoment2}'
        AND t.date_due <= '{$endDateMoment2}'
        AND tc.sasa_cfv_c IS null AND t.deleted = 0
    LIMIT 1000";

    $resultVencidas = $GLOBALS['db']->query($queryVencidas);
    $resultAVencer = $GLOBALS['db']->query($queryAVencer);

    $contQuery = mysqli_num_rows($resultVencidas);
    $contQuery2 = mysqli_num_rows($resultAVencer);

    $GLOBALS['log']->security("Cantidad de tareas vencidas ---> ".$contQuery."\n");
    $GLOBALS['log']->security("Cantidad de tareas a vencer ---> ".$contQuery2."\n");

    while ($row = $GLOBALS['db']->fetchByAssoc($resultVencidas)) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' ) 
        {
            $bean = BeanFactory::getBean("Tasks", $row['id']);
            $bean->sasa_cfv_c = 1;
            $bean->save();
        }
    }

    while ( $row = $GLOBALS['db']->fetchByAssoc($resultAVencer) ) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' ) 
        {
            $bean = BeanFactory::getBean("Tasks", $row['id']);
            $bean->sasa_cfv_c = 2;
            $bean->save();
        }
    }

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Marcado de campo CFV en Tareas.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

    function calcularDifInTasks()
    {
        $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

        $timeZoneBogota = 'America/Bogota';
        $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

        $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
        $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

        if ( $dayCentral > $dayColombia ) 
        {
            $GLOBALS['log']->security("Día de fecha central es mayor");
            $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
            $hourColombia = intval($dateColombia->format('h'));
            $dif = $hourCentral - $hourColombia;
            $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        }
        else if( $dayCentral == $dayColombia ) 
        {
            $GLOBALS['log']->security("Días iguales");
            $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
            $hourColombia = intval($dateColombia->format('H:i:s'));
            $dif = $hourCentral - $hourColombia;
            $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        } 
        else 
        {
            $GLOBALS['log']->security("Algo paso");
            $dif = 0;
            $GLOBALS['log']->security("Diferencia horaria: 0");
            return $dif;
        }
    }
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/MarkCFVFieldCases.php


$job_strings[] = 'MarkCFVFieldCases';

function MarkCFVFieldCases()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Marcado de campo CFV en Casos.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    $dif = calcularDifInCases();

    date_default_timezone_set('America/Bogota'); // configurando por defecto zona horaria de Colombia

    $dateYesterday = date('Y-m-d', strtotime('-1 day')); // obtener fecha día anterior
    $dateToday = date('Y-m-d');

    // Fecha inicio y fin para tareas vencidas
    $startDateMoment1 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateYesterday 07:30:00"))); // establecer fecha inicio
    $endDateMoment1 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment1))); // establecer fecha fin

    // fecha inicio y fin para tareas proximas a vencer
    $startDateMoment2 = date('Y-m-d H:i:s', strtotime("+$dif hour", strtotime("$dateToday 07:30:00")));
    $endDateMoment2 = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDateMoment2)));

    $queryVencidas = "SELECT c.id, c.name, c.resolved_datetime, cc.sasa_cfv_c, cc.sasa_tipodesolicitud_c
        FROM cases AS c
        INNER JOIN cases_cstm AS cc ON cc.id_c = c.id
        WHERE c.resolved_datetime > '{$startDateMoment1}'
        AND c.resolved_datetime <= '{$endDateMoment1}'
        AND cc.sasa_cfv_c IS null AND c.deleted = 0
    LIMIT 1000;";

    $queryAVencer = "SELECT c.id, c.name, c.resolved_datetime, cc.sasa_cfv_c, cc.sasa_tipodesolicitud_c
        FROM cases AS c
        INNER JOIN cases_cstm AS cc ON cc.id_c = c.id
        WHERE c.resolved_datetime > '{$startDateMoment2}'
        AND c.resolved_datetime <= '{$endDateMoment2}'
        AND cc.sasa_cfv_c IS null AND c.deleted = 0
    LIMIT 1000;";

    $resultVencidas = $GLOBALS['db']->query($queryVencidas);
    $resultAVencer = $GLOBALS['db']->query($queryAVencer);

    $contQuery = mysqli_num_rows($resultVencidas);
    $contQuery2 = mysqli_num_rows($resultAVencer);

    $GLOBALS['log']->security("Cantidad de casos vencidos ---> ".$contQuery."\n");
    $GLOBALS['log']->security("Cantidad de casos a vencer ---> ".$contQuery2."\n");

    while ($row = $GLOBALS['db']->fetchByAssoc($resultVencidas)) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' && $row['sasa_tipodesolicitud_c'] != 'quejas' && $row['sasa_tipodesolicitud_c'] != '' && $row['sasa_tipodesolicitud_c'] != null ) 
        {
            $bean = BeanFactory::getBean("Cases", $row['id']);
            $bean->sasa_cfv_c = 1;
            $bean->save();
        }
    }

    while ( $row = $GLOBALS['db']->fetchByAssoc($resultAVencer) ) 
    {
        if ( $row['sasa_cfv_c'] != '1' && $row['sasa_cfv_c'] != '2' && $row['sasa_tipodesolicitud_c'] != 'quejas' && $row['sasa_tipodesolicitud_c'] != '' && $row['sasa_tipodesolicitud_c'] != null ) 
        {
            $bean = BeanFactory::getBean("Cases", $row['id']);
            $bean->sasa_cfv_c = 2;
            $bean->save();
        }
    }

    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Marcado de campo CFV en Casos.".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return true;
}

function calcularDifInCases()
{
    $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

    $timeZoneBogota = 'America/Bogota';
    $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

    $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
    $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

    if ( $dayCentral > $dayColombia ) 
    {
        $GLOBALS['log']->security("Día de fecha central es mayor");
        $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
        $hourColombia = intval($dateColombia->format('h'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    }
    else if( $dayCentral == $dayColombia ) 
    {
        $GLOBALS['log']->security("Días iguales");
        $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
        $hourColombia = intval($dateColombia->format('H:i:s'));
        $dif = $hourCentral - $hourColombia;
        $GLOBALS['log']->security("Diferencia horaria: ".$dif);
        return $dif;
    } 
    else 
    {
        $GLOBALS['log']->security("Algo paso");
        $dif = 0;
        $GLOBALS['log']->security("Diferencia horaria: 0");
        return $dif;
    }
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/DeletePmseBpmFlowRecords.php


$job_strings[] = 'DeletePmseBpmFlowRecords';

function DeletePmseBpmFlowRecords()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Inicio TP Delete pmse_bpm_flow records. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    $GLOBALS['log']->security("Ejecutando proceso de eliminación para registros de pmse_bpm_flow...");
    delete_rows_pmse_bpm_flow();
    
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Fin TP Delete pmse_bpm_flow records. ".date('Y-m-d H:i:s'));
    $GLOBALS['log']->security("**********************************************************************************");

    return true;
}


function delete_rows_pmse_bpm_flow()
{
    $antes = get_pmse_bpm_flow();
    
    $query1 = "DELETE FROM pmse_bpm_flow 
    WHERE cas_flow_status = 'TERMINATED'
    AND date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $query2 = "DELETE FROM pmse_bpm_flow
    WHERE cas_flow_status = 'CLOSED'
    AND date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $resultQuery1 = $GLOBALS['db']->query($query1);
    $resultQuery2 = $GLOBALS['db']->query($query2);

    $despues = get_pmse_bpm_flow();
}

// Consultas
// pmse_bpm_flow

function get_pmse_bpm_flow()
{
    $query3 = "SELECT COUNT(p.id) AS total 
    FROM pmse_bpm_flow AS p 
    WHERE p.cas_flow_status = 'TERMINATED'
    AND p.date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";
    
    $query4 = "SELECT COUNT(p.id) AS total 
    FROM pmse_bpm_flow AS p 
    WHERE p.cas_flow_status = 'CLOSED'
    AND p.date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $resultQuery3 = $GLOBALS['db']->query($query3);
    $resultQuery4 = $GLOBALS['db']->query($query4);

    while( $row = $GLOBALS['db']->fetchByAssoc($resultQuery3) )
    {
        $re = !empty($row['total']) ? $row['total'] : 0; // result
        $GLOBALS['log']->security("Total pmse_bpm_flow con estado terminated: ".$re);
    }
    
    while( $row = $GLOBALS['db']->fetchByAssoc($resultQuery4) )
    {
        $re = !empty($row['total']) ? $row['total'] : 0; // result
        $GLOBALS['log']->security("Total pmse_bpm_flow con estado closed: ".$re);
    }
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/AddTeamToAccounts7.php


$job_strings[] = 'AddTeamToAccounts7';

function AddTeamToAccounts7()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Inicio tarea programada para agregar equipo Global a Cuentas #7. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    $ids = [];

    $query_accounts = "SELECT a.id, a.name, a.team_id, a.team_set_id
        FROM accounts AS a
        WHERE a.team_set_id NOT IN (
            SELECT tst.team_set_id
            FROM team_sets_teams AS tst
            WHERE tst.team_id = '1'
            AND tst.deleted = 0
        )
        AND a.deleted = 0
        AND a.team_id != '1'
        AND a.team_set_id != '1'
    LIMIT 200";

    $result_query_accounts = $GLOBALS['db']->query($query_accounts);

    while( $row_account = $GLOBALS['db']->fetchByAssoc($result_query_accounts) )
    {
        if( !empty($row_account['id']) )
        {
            $ids[] = $row_account['id'];
        }
    }

    $teamId = '';

    $queryTeam = "SELECT t.id, t.name
        FROM teams AS t
        WHERE t.name = 'Global'
        AND t.deleted = 0
    LIMIT 1"; 

    $resultQueryTeam = $GLOBALS['db']->query($queryTeam);

    while( $rowTeam = $GLOBALS['db']->fetchByAssoc($resultQueryTeam) )
    {
        if( !empty($rowTeam['id']) )
        {
            $teamId = $rowTeam['id'];
        }
    }

    if( !empty($teamId) )
    {
        foreach ($ids as $id) 
        {
            $account = BeanFactory::getBean('Accounts', $id);
            
            if( !empty($account->id) )
            {
                $teamSetBean = new TeamSet();
                $teams = $teamSetBean->getTeams($account->team_set_id);
    
                $hasTeamRelationship = false;

                foreach ($teams as $team) 
                {
                    // $GLOBALS['log']->security("Relación id: ".$team->id);
                    // $GLOBALS['log']->security("Relación name: ".$team->name);
    
                    if ($team->id === $teamId) 
                    {
                        $hasTeamRelationship = true;
                        break;
                    } 
                }
    
                if( $hasTeamRelationship )
                {
                    $GLOBALS['log']->security("Cuanta ya tiene relación con equipo Global...".$id);
    
                } else {
    
                    // $GLOBALS['log']->security("Se inicia creación de relación entre Cuenta y equipo Global...");
    
                    if( $account->load_relationship('teams') )
                    {
                        // $GLOBALS['log']->security("Se cargo relación con equipo Global...");

                        $account->teams->add(
                            array(
                                $teamId
                            )
                        );

                        $GLOBALS['log']->security("Se agrego equipo Global a cuenta: ".$id);
                    } else {

                        $GLOBALS['log']->security("No se logro crear relación para la cuenta: ".$id);
                    }
                }
            }
        }
    } else {

        $GLOBALS['log']->security("No se encontró equipo Global paara relacionar con cuentas");
    }

    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Fin tarea programada para agregar equipo Global a Cuentas #7. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    return true;
}



?>
