<?php
// created: 2018-05-29 14:27:23
$viewdefs['Users']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'search_name' => 
    array (
      'dbFields' => 
      array (
        0 => 'first_name',
        1 => 'last_name',
      ),
      'vname' => 'LBL_NAME',
    ),
    'first_name' => 
    array (
    ),
    'last_name' => 
    array (
    ),
    'user_name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'status' => 
    array (
    ),
    'is_admin' => 
    array (
    ),
    'title' => 
    array (
    ),
    'is_group' => 
    array (
    ),
    'department' => 
    array (
    ),
    'address_street' => 
    array (
    ),
    'address_city' => 
    array (
    ),
    'address_state' => 
    array (
    ),
    'address_postalcode' => 
    array (
    ),
    'address_country' => 
    array (
    ),
    'sasa_segmento_c' => 
    array (
      'type' => 'multienum',
      'default' => true,
      'width' => '10',
      'name' => 'sasa_segmento_c',
      'vname' => 'LBL_SASA_SEGMENTO_C',
    ),
    'sasa_regional_c' => 
    array (
      'type' => 'enum',
      'default' => true,
      'width' => '10',
      'name' => 'sasa_regional_c',
      'vname' => 'LBL_SASA_REGIONAL_C',
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);