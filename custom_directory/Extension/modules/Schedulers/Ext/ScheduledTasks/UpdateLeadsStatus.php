<?php

$job_strings[] = 'UpdateLeadsStatus';

function UpdateLeadsStatus()
{
    $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Inicio Update Leads Status and Converted 1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");
    
    //Realación de leads a account y status != Converted  or  converted = 0
    $query="SELECT l.id as idLeads
                FROM leads l
                INNER JOIN accounts a ON a.id = l.account_id
                WHERE (l.status != 'Converted' OR l.converted = 0 ) AND l.deleted = 0";

    $result = $GLOBALS['db']->query($query);
    $cantConsulta = mysqli_num_rows($result);

    //$arrayConsulta = array();
    //$cont = 0;
    $GLOBALS['log']->security("-------EJECUTAR SIGUIENTE QUERY-----------------");
    $GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        
                    /*$arrayConsulta = $row['idLeads'];
                    $cont++;*/
            //$GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $row['idLeads']);
            
            $beanStatus = BeanFactory::retrieveBean("Leads", $row['idLeads']);

            //Fields to update
            $beanStatus->status = 'Converted';
            $beanStatus->converted = 1;

            //Save
            $beanStatus->save();


    }//while

    $GLOBALS['log']->security("-----CANTDAD DE REGISTROS RELACIONADOS----" . $cantConsulta);

    /*foreach ($arrayConsulta as $idLeads) {
        // code...
            


    }*/
    



    /*$result2 = $GLOBALS['db']->query($query);
    $cantConsulta2 = mysqli_num_rows($result2);

     $GLOBALS['log']->security("-----CANTDAD DE REGISTROS NUEVOS RELACIONADOS----" . $cantConsulta2);*/



    $GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin Update Leads Status and Converted 1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

}
