<?php
 // created: 2018-07-06 21:52:52
$dictionary['Meeting']['fields']['location']['audited']=true;
$dictionary['Meeting']['fields']['location']['massupdate']=false;
$dictionary['Meeting']['fields']['location']['comments']='Meeting location';
$dictionary['Meeting']['fields']['location']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['location']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['location']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['location']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.36',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['location']['calculated']=false;

 ?>