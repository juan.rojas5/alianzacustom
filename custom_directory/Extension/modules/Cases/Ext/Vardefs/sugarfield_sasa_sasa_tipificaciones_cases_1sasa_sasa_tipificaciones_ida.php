<?php
 // created: 2022-10-19 15:24:17
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['name']='sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['type']='id';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['source']='non-db';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['vname']='LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_CASES_TITLE_ID';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['id_name']='sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['link']='sasa_sasa_tipificaciones_cases_1';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['table']='sasa_sasa_tipificaciones';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['module']='sasa_sasa_tipificaciones';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['rname']='id';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['reportable']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['side']='right';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['massupdate']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['hideacl']=true;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['audited']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida']['importable']='true';

 ?>