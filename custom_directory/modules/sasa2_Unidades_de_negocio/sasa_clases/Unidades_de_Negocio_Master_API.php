<?php

use function PHPSTORM_META\type;

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class Unidades_de_Negocio_Master_API extends SugarApi
{
    public function crearRegistros($registros, $modulo, $return2)
    {
        $GLOBALS['log']->security("Preparando para creación de registros... 30%");
        $return = $return2;
        $index = 1;
        
        foreach ($registros as $registro) 
        {
            $bean = BeanFactory::newBean($modulo);

            $GLOBALS['log']->security("Va a ingresar a unidad");
            $this->iterarRegistros($registro, $bean, $modulo);

            if( $modulo == 'sasa2_Unidades_de_negocio' )
            {
                $return = $this->validarUnidadesNegocio($bean, $return, $registro, $index);
            }

            if( $modulo == 'sasa1_Unidades_de_negocio_por_' )
            {
                $return = $this->validarUnidadesNegocioPor($bean, $return, $registro, $index);
            }

            if( $modulo == 'Purchases' )
            {
                $return = $this->validarProductosDeCliente($bean, $return, $registro, $index);
            }
            $index++;
        }

        return $return;
    }

    public function actualizarRegistros($registros, $modulo, $return2)
    {
        $GLOBALS['log']->security("Preparando para actualización de registros... 30%");
        $return = $return2;
        $index = 1;

        foreach ($registros as $registro) 
        {
            $bean = BeanFactory::getBean($modulo, $registro['id'], array('disable_row_level_security' => true));

            if( empty($bean->id) )
            {
                $return['registros_no_existentes_para_actualizar'][] = [ 
                    "registro" => $registro,
                    "posición en el objeto" => $index
                ];      
            } else {

                $this->iterarRegistros($registro, $bean, $modulo);

                if( $modulo == 'sasa2_Unidades_de_negocio' )
                {
                    $return = $this->validarUnidadesNegocio($bean, $return, $registro, $index);
                }

                if( $modulo == 'sasa1_Unidades_de_negocio_por_' )
                {
                    $return = $this->validarUnidadesNegocioPor($bean, $return, $registro, $index);
                }

                if( $modulo == 'Purchases' )
                {
                    $return = $this->validarProductosDeCliente($bean, $return, $registro, $index);
                }
            }
            $index++;
        }

        return $return;
    }

    // ========== Inicio validador de campos requeridos =========================

        // Antes de crear registros, se valida que los campos requeridos tengan asignado algún valor
        // de lo contrarío en la variable $return se guardará los registros que no se crearón

        private function validarUnidadesNegocio($bean, $return, $registro, $index)
        {
            // aca se agregan todas las relaciones que se tenga
            $relaciones = [
                [
                    'sasa1_Unidades_de_negocio_por_', // modulo relacionado
                    $registro['sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1'], // id módulo relacionado
                    'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1' // nombre de la relación
                ],
                [
                    'sasa_ProductosAFAV',
                    $registro['sasa2_unidades_de_negocio_sasa_productosafav_1'],
                    'sasa2_unidades_de_negocio_sasa_productosafav_1'
                ]
            ];

            $validarRelaciones = [];

            if( !empty($bean->name) )
            {
                $GLOBALS['log']->security("Creación/Actualización de registro exitoso... 100%");
                $bean->save();

                foreach ($relaciones as $relacion) 
                {  
                    $GLOBALS['log']->security("relaciones ".print_r($relacion[1], true));
                    $GLOBALS['log']->security("cantidad: ".count($relacion[1]));
                    if( ( $relacion[0] == 'sasa1_Unidades_de_negocio_por_' && count($relacion[1]) > 0 ) ||  ( $relacion[0] == 'sasa_ProductosAFAV' && count($relacion[1]) > 0 )  )
                    { 
                        foreach ($relacion[1] as $value ) {
                            $bean_related = BeanFactory::getBean($relacion[0], $value, array('disable_row_level_security' => true));
                    
                            if( !empty($bean_related->id) )
                            {
                                if( $bean->load_relationship($relacion[2]) )
                                {
                                    $bean->{$relacion[2]}->add($value);
                                }
                            } else {
                                if( $value !== NULL )
                                {
                                    $validarRelaciones['mensajeValidar'] .= " id " . $value . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                                }
                            }
                        }
                    } else {
                        $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                    
                        if( !empty($bean_related->id) )
                        {
                            if( $bean->load_relationship($relacion[2]) )
                            {
                                $bean->{$relacion[2]}->add($relacion[1]);
                            }
                        } else {
                            if( $relacion[1] != NULL )
                            {
                                $validarRelaciones['mensajeValidar'] .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    }  
                }

                if( !empty($validarRelaciones['mensajeValidar']) )
                {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name,
                        "excepción" => $validarRelaciones['mensajeValidar'],
                    ];
                } else {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name
                    ];
                }

            } else {
                $mensaje = "Es requerido definir y enviar con valor el(los) siguiente(s) campo(s): ";
                $mensaje .= ( empty($bean->name) ) ? "name " : "";
                $return['registros_no_creados_o_actualizados'][] = [
                    "mensaje" => ( !empty($mensaje) ) ? $mensaje : "",
                    "registro" => "Registro en la posición $index"
                ];
            }
            return $return;
        }

        private function validarUnidadesNegocioPor($bean, $return, $registro, $index)
        {
            // aca se agregan todas las relaciones que se tenga
            $relaciones = [
                [
                    'Accounts', // modulo relacionado
                    $registro['accounts_sasa1_unidades_de_negocio_por__1'], // id módulo relacionado
                    'accounts_sasa1_unidades_de_negocio_por__1' // nombre de la relación
                ],
                [
                    'sasa2_Unidades_de_negocio',
                    $registro['sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1'],
                    'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1'
                ],
                [
                    'Users',
                    $registro['assigned_user_id'],
                    'assigned_user_id'
                ]
            ];

            $modulo = "sasa1_Unidades_de_negocio_por_";
            $isnot_empty = true; // para validar los ids de las relaciones

            $validarRelaciones = $this->validarValoresDeRelaciones($relaciones, $isnot_empty, $modulo);

            $mensajeAlt = ( !empty($validarRelaciones['mensaje']) ) ? $validarRelaciones['mensaje'] : "";
            $isnot_empty = $validarRelaciones['isnot_empty'];

            // if( !empty($bean->name) && !empty($bean->sasa_regional_c) && !empty($bean->sasa_ejecutivocomercial_c) && $isnot_empty )
            if( !empty($bean->sasa_regional_c) && !empty($bean->sasa_ejecutivocomercial_c) && $isnot_empty )
            {
                $GLOBALS['log']->security("Creación/Actualizazión 90%...");

                // $bean->name = 'Concatenación  Nombre de Unidad de Negocio y nombre de Cuenta';

                $bean->save();

                foreach ($relaciones as $relacion) 
                {
                    if( $relacion[2] == 'assigned_user_id' )
                    {
                        $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));

                        if( !empty($bean_related->id) )
                        {
                            $bean->{$relacion[2]} = $relacion[1];
                            $bean->save();
                        } else {
                            if( $relacion[1] !== NULL )
                            {
                                $validarRelaciones['mensajeValidar'] .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    } else {
                        $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                        if( !empty($bean_related->id) )
                        {
                            if( $bean->load_relationship($relacion[2]) )
                            {
                                $bean->{$relacion[2]}->add($relacion[1]);
                            }
                        } else {
                            if( $relacion[1] !== NULL )
                            {
                                $validarRelaciones['mensajeValidar'] .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    }    
                }

                if( !empty($validarRelaciones['mensajeValidar']) )
                {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name,
                        // "sasa_regional_c" => $bean->sasa_regional_c,
                        "excepción" => $validarRelaciones['mensajeValidar'],
                    ];
                } else {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name
                        // "sasa_regional_c" => $bean->sasa_regional_c
                    ];
                }
            } else {

                if( empty($bean->name) || empty($bean->sasa_regional_c) || empty($bean->sasa_ejecutivocomercial_c) || $isnot_empty )
                {
                    $mensaje = "Es requerido definir y enviar con valor el(los) siguiente(s) campo(s): ";
                    // $mensaje .= ( empty($bean->name) ) ? "name " : "";
                    $mensaje .= ( empty($bean->sasa_regional_c) ) ? "sasa_regional_c " : "";
                    $mensaje .= ( empty($bean->sasa_ejecutivocomercial_c) ) ? "sasa_ejecutivocomercial_c" : "";
                    $mensaje .= ( !empty($mensajeAlt) ) ? $mensajeAlt : "";
                }

                if( !empty($validarRelaciones['mensajeValidar']) )
                {
                    $return['registros_no_creados_o_actualizados'][] = [
                        "mensaje" => ( !empty($mensaje) ) ? $mensaje : "",
                        "validar" => $validarRelaciones['mensajeValidar'],
                        "reistro" => $registro
                    ];
                } else {
                    $return['registros_no_creados_o_actualizados'][] = [
                        "mensaje" => ( !empty($mensaje) ) ? $mensaje : "",
                        "registro" => $registro
                    ];
                }
            }
            return $return;
        }

        private function validarProductosDeCliente($bean, $return, $registro, $index)
        {
            // aca se agregan todas las relaciones que se tenga
            $relaciones = [
                [
                    'sasa_ProductosAFAV', // modulo relacionado
                    $registro['sasa_productosafav_purchases_1'], // id módulo relacionado
                    'sasa_productosafav_purchases_1' // nombre de la relación
                ],
                [
                    'Accounts',
                    $registro['account_id'],
                    'account_id'
                ],
                [
                    'Users',
                    $registro['assigned_user_id'],
                    'assigned_user_id'
                ]
            ];

            $modulo = "Purchases";
            $isnot_empty = true; // para validar los ids de las relaciones

            $validarRelaciones = $this->validarValoresDeRelaciones($relaciones, $isnot_empty, $modulo);

            $mensajeAlt = ( !empty($validarRelaciones['mensaje']) ) ? $validarRelaciones['mensaje'] : "";
            $isnot_empty = $validarRelaciones['isnot_empty'];

            if( !empty($bean->name) && !empty($bean->sasa_encargo_c) && !empty($bean->sasa_tipotitular_c) && !empty($bean->sasa_fechacreacion_c) && strtotime($bean->sasa_fechacreacion_c) && !empty($bean->date_modified) && !empty($bean->sasa_estado_c) && !empty($bean->email_c) && $isnot_empty )
            {
                $GLOBALS['log']->security("Creación/Actualización de registro exitoso... 100%");
                $bean->save();

                foreach ($relaciones as $relacion) 
                {
                    if( $relacion[2] == 'assigned_user_id' || $relacion[2] == 'account_id' )
                    {
                        $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));

                        if( !empty($bean_related->id) )
                        {
                            $bean->{$relacion[2]} = $relacion[1];
                            $bean->save();
                        } else {
                            if( $relacion[1] !== NULL )
                            {
                                $validarRelaciones['mensajeValidar'] .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    } else {
                        $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                        if( !empty($bean_related->id) )
                        {
                            if( $bean->load_relationship($relacion[2]) )
                            {
                                $bean->{$relacion[2]}->add($relacion[1]);
                            }
                        } else {
                            if( $relacion[1] !== NULL )
                            {
                                $validarRelaciones['mensajeValidar'] .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    }    
                }

                if( !empty($validarRelaciones['mensajeValidar']) )
                {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name,
                        "excepción" => $validarRelaciones['mensajeValidar'],
                    ];
                } else {
                    $return['registros_creados_o_actualizados'][] = [
                        "id" => $bean->id,
                        "name" => $bean->name
                    ];
                }
            } else {
                $GLOBALS['log']->security("Entro 4");
                if( empty($bean->name) || empty($bean->sasa_encargo_c) || empty($bean->sasa_tipotitular_c) || empty($bean->sasa_fechacreacion_c) || !strtotime($bean->sasa_fechacreacion_c) || empty($bean->date_modified) || empty($bean->sasa_estado_c) || empty($bean->email_c) || $isnot_empty )
                {
                    $GLOBALS['log']->security("mensaje alt: ".$mensajeAlt);
                    $mensaje = "Es requerido definir y enviar con valor correcto el(los) siguiente(s) campo(s): ";
                    $mensaje .= ( empty($bean->name) ) ? "name " : "";
                    $mensaje .= ( empty($bean->sasa_encargo_c) ) ? "sasa_encargo_c " : "";
                    $mensaje .= ( empty($bean->sasa_tipotitular_c) ) ? "sasa_tipotitular_c " : "";
                    $mensaje .= ( empty($bean->sasa_fechacreacion_c) || !strtotime($bean->sasa_fechacreacion_c) ) ? "sasa_fechacreacion_c " : "";
                    $mensaje .= ( empty($bean->date_modified) ) ? "date_modified " : "";
                    $mensaje .= ( empty($bean->sasa_estado_c) ) ? "sasa_estado_c " : "";
                    $mensaje .= ( empty($bean->email_c) ) ? "email_c " : "";
                    $mensaje .= ( !empty($mensajeAlt) ) ? $mensajeAlt : "";
                }

                if( !empty($validarRelaciones['mensajeValidar']) )
                {
                    $return['registros_no_creados_o_actualizados'][] = [
                        "mensaje" => ( !empty($mensaje) ) ? $mensaje : "",
                        "validar" => $validarRelaciones['mensajeValidar'],
                        "reistro" => $registro
                    ];
                } else {
                    $return['registros_no_creados_o_actualizados'][] = [
                        "mensaje" => ( !empty($mensaje) ) ? $mensaje : "",
                        "registro" => $registro
                    ];
                }
            }
            return $return;
        }
    // ========== Fin validador de campos requeridos ============================

    // ========== Inicio validador de campos relacionados =======================

        private function validarValoresDeRelaciones($relaciones, $isnot_empty, $modulo)
        {
            $mensajeValidar = "";
            $mensajeValidarAlt = "";
            $mensaje = "";

            if( $modulo == 'sasa1_Unidades_de_negocio_por_' )
            {
                if( $_SERVER['REQUEST_METHOD'] != 'PUT' )
                {
                    foreach ($relaciones as $relacion) 
                    {
                        // se valida que no estén los ids para las relaciones que son requeridas
                        if( ( empty($relacion[1]) && $relacion[2] ==  'accounts_sasa1_unidades_de_negocio_por__1' ) || ( empty($relacion[1]) && $relacion[2] ==  'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1' ) )
                        {
                            $isnot_empty = false;
                            $mensaje .= $relacion[2] ." ";
                        }
    
                        // se valida que estén los ids de las relaciones que son requeridas
                        if( ( !empty($relacion[1]) && $relacion[2] == 'accounts_sasa1_unidades_de_negocio_por__1' ) || ( !empty($relacion[1]) && $relacion[2] == 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1' ) )
                        {
                            // se valida que el id de la relación exista en el respectivo módulo relacionado 
                            $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                            if( empty($bean_related->id) )
                            {
                                $isnot_empty = false;
                                $mensajeValidar .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
    
                        // se valida las relaciones que no son requeridas
                        if( !empty($relacion[1]) && $relacion[2] != 'accounts_sasa1_unidades_de_negocio_por__1' && $relacion[2] != 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1' )
                        {
                            // se valida que el id de la relación exista en el respectivo módulo relacionado 
                            $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                            if( empty($bean_related->id) )
                            {
                                $mensajeValidarAlt .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    }

                    if( !$isnot_empty && !empty($mensajeValidarAlt) )
                    {
                        $mensajeValidar .= $mensajeValidarAlt;
                    }
                }   
            }

            if( $modulo == 'Purchases' )
            {
                if( $_SERVER['REQUEST_METHOD'] != 'PUT' )
                {
                    foreach ($relaciones as $relacion) 
                    {
                        if( ( empty($relacion[1]) && $relacion[2] ==  'sasa_productosafav_purchases_1' ) || ( empty($relacion[1]) && $relacion[2] ==  'account_id' ) || ( empty($relacion[1]) && $relacion[2] ==  'assigned_user_id' ) )
                        {
                            $isnot_empty = false;
                            $mensaje .= "".$relacion[2] ." ";
                        }

                        if( ( !empty($relacion[1]) && $relacion[2] == 'sasa_productosafav_purchases_1' ) || ( !empty($relacion[1]) && $relacion[2] == 'account_id' ) || ( !empty($relacion[1]) && $relacion[2] ==  'assigned_user_id' ) )
                        {
                            // se valida que el id de la relación exista en el respectivo módulo relacionado 
                            $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                            if( empty($bean_related->id) )
                            {
                                $isnot_empty = false;
                                $mensajeValidar .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }

                        if( !empty($relacion[1]) && $relacion[2] != 'sasa_productosafav_purchases_1' && $relacion[2] != 'account_id' && $relacion[2] != 'assigned_user_id' )
                        {
                            $GLOBALS['log']->security("entro 3");
                            // se valida que el id de la relación exista en el respectivo módulo relacionado 
                            $bean_related = BeanFactory::getBean($relacion[0], $relacion[1], array('disable_row_level_security' => true));
                            if( empty($bean_related->id) )
                            {
                                $mensajeValidarAlt .= " id " . $relacion[1] . " para relación " . $relacion[2] . " no existe en " . $relacion[0] . ",";
                            }
                        }
                    }

                    if( !$isnot_empty && !empty($mensajeValidarAlt) )
                    {
                        $mensajeValidar .= $mensajeValidarAlt;
                    }
                }
            }

            return [
                'mensaje' => $mensaje,
                'mensajeValidar' => $mensajeValidar,
                'isnot_empty' => $isnot_empty
            ];
        }
    // ========== Fin validador de campos relacionados ==========================

    // ========== Inicio iterador de registros ==================================

        // En la siguiente función iterarRegistros() se busca declarar y asignar
        // cada campo que se relaciona al bean previamente instanciado

        private function iterarRegistros($registro, $bean, $modulo)
        {
            if( ( $modulo == 'sasa1_Unidades_de_negocio_por_' ) || ( $modulo == 'sasa2_Unidades_de_negocio' ) || ( $modulo == 'Purchases' ) )
            {
                if( $_SERVER['REQUEST_METHOD'] == 'PUT' )
                {
                    foreach ($registro as $key => $value) 
                    {
                        if( $key != 'assigned_user_id' && $key != 'account_id' ) // campos de relación que se asignan sin cargar la relación
                        {
                            $bean->{$key} = $value;
                        }
                    }
                } else {
                    foreach ($registro as $key => $value) 
                    {
                        if( $modulo == 'sasa1_Unidades_de_negocio_por_' && $key != 'name' && !empty($value) )
                        {
                            $bean->{$key} = $value;
                        }
                        if( $modulo == 'sasa2_Unidades_de_negocio' && !empty($value) )
                        {
                            $bean->{$key} = $value;
                        }
                        if( $modulo == 'Purchases' )
                        {
                            if( !empty($value) && $key != 'sasa_fechacreacion_c' && $key != 'sasa_fechacancelacion_c' )
                            {
                                $bean->{$key} = $value;
                            }
                            if( ( $key == 'sasa_fechacreacion_c' || $key == 'sasa_fechacancelacion_c' ) && strtotime($value) )
                            {
                                $bean->{$key} = date('Y-m-d H:i:s', strtotime($value));
                            }
                        }
                        // if( ( !empty($value) && $key != 'sasa_fechacreacion_c' && $key != 'sasa_fechacancelacion_c' ) || ( $modulo == 'sasa1_Unidades_de_negocio_por_' && $key != 'name' ) )
                        // {
                        //     $bean->{$key} = $value;
                        // }   
                        
                    }
                }
            }
            return $bean;
        }
    // ========== Inicio iterador de registros ==================================
}