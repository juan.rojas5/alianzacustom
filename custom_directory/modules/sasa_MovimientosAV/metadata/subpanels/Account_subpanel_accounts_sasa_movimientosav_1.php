<?php
// created: 2019-01-04 21:54:58
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_productosafav_sasa_movimientosav_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1SASA_PRODUCTOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'sasa_nemotecnico_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NEMOTECNICO_C',
    'width' => 10,
  ),
  'sasa_emisor_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_EMISOR_C',
    'width' => 10,
  ),
  'sasa_fechacumplimiento_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_fechaliquid_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHALIQUID_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_documento_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_DOCUMENTO_C',
    'width' => 10,
  ),
  'sasa_tipooperacion_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_TIPOOPERACION_C',
    'width' => 10,
  ),
  'sasa_detalle_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_DETALLE_C',
    'width' => 10,
  ),
  'sasa_nominal_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_NOMINAL_C',
    'width' => 10,
  ),
  'sasa_tirprecio_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_TIRPRECIO_C',
    'width' => 10,
  ),
  'sasa_valor_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALOR_C',
    'width' => 10,
  ),
  'sasa_comision_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_COMISION_C',
    'width' => 10,
  ),
  'sasa_iva_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_IVA_C',
    'width' => 10,
  ),
  'sasa_retencion_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_RETENCION_C',
    'width' => 10,
  ),
  'sasa_afavor_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_AFAVOR_C',
    'width' => 10,
  ),
  'sasa_encontra_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_ENCONTRA_C',
    'width' => 10,
  ),
  'sasa_saldo_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_SALDO_C',
    'width' => 10,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);