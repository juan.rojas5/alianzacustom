<?php
// created: 2019-03-08 19:02:12
$wireless_module_registry = array (
  'Accounts' => 
  array (
  ),
  'Contacts' => 
  array (
  ),
  'Leads' => 
  array (
  ),
  'Opportunities' => 
  array (
    'disable_create' => true,
  ),
  'RevenueLineItems' => 
  array (
  ),
  'Calls' => 
  array (
  ),
  'Meetings' => 
  array (
  ),
  'Notes' => 
  array (
  ),
  'Tasks' => 
  array (
  ),
  'sasa_SaldosAF' => 
  array (
  ),
  'sasa_SaldosAV' => 
  array (
  ),
  'sasaS_SaldosConsolidados' => 
  array (
  ),
  'sasa_MovimientosAF' => 
  array (
  ),
  'sasa_MovimientosAV' => 
  array (
  ),
  'sasa_MovimientosAVDivisas' => 
  array (
  ),
  'sasa_ProductosAFAV' => 
  array (
  ),
  'sasa_Categorias' => 
  array (
  ),
  'Employees' => 
  array (
    'disable_create' => true,
  ),
  'Reports' => 
  array (
    'disable_create' => true,
  ),
  'Emails' => 
  array (
  ),
  'sasaP_PresupuestosxAsesor' => 
  array (
  ),
  'sasaP_PresupuestoxProducto' => 
  array (
  ),
);