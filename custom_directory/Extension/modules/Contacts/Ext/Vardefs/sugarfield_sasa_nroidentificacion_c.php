<?php
 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['labelValue']='Número de ID';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['required_formula']='equal($sasa_pqrs_c,"Si")';
$dictionary['Contact']['fields']['sasa_nroidentificacion_c']['readonly_formula']='';

 ?>