({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	casos: [],
	Username:"**PAI**",
	Password:"**PAI**",
	url:'**PAI**',
	ws:'**PAI**',
	initialize: function(options) {
		var self = this;
		dashlet3 = self;		
		if (typeof app === 'undefined') app = App;		
			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet3_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet3_No_Identificacion").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet3_No_Identificacion").css("border", "none"); 
						$("#dashlet3_No_Identificacion").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet3_No_Identificacion").attr('readonly', true);
					}
					
					$("#dashlet3_consultar").click( function(){
						self.dashlet3_portal();	
					});
					
					$("#dashlet3_consultar").show();
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet3_portal: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet3_portal_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		if (!_.isEmpty( $("#dashlet3_No_Identificacion").val() )){	
			self.dashlet3_consultar();//consultando caso
		}
		else{
			app.alert.dismiss('dashlet3_portal_procesando');
			app.alert.show('error_procesando_id_cliente', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},
	dashlet3_salida_no_de_caso_change: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		if ( _.isEmpty( $("#dashlet3_salida_no_de_caso").html() ) ){ 
			$("#dashlet3_salida_no_de_caso").append('<option value="">--</option>');
		}
		
		$("#dashlet3_salida_Descripción").val("")//limpiando
		$("#dashlet3_salida_Fecha_Apertura").val("")//limpiando
		$("#dashlet3_salida_Estado").val("")//limpiando
		$("#dashlet3_salida_Tipo_Solicitud").val("")//limpiando
		$("#dashlet3_salida_Asignado_A").val("");//limpiando
		$("#dashlet3_salida_Respuesta").val("")//limpiando
		console.log("dashlet3_salida_no_de_caso_change",self.casos);
		if (!_.isEmpty( self.casos )){
			if (!_.isEmpty( $("#dashlet3_salida_no_de_caso").val() )){	
				var i = $("#dashlet3_salida_no_de_caso").val();	
				$("#dashlet3_salida_Tipo_Solicitud").val(self.casos[i].tipoSolicitud);
				$("#dashlet3_salida_Descripción").val(self.casos[i].descripcion);
				$("#dashlet3_salida_Estado").val(self.casos[i].estado);
				$("#dashlet3_salida_Fecha_Apertura").val(self.casos[i].fechaApertura);
				
				$("#dashlet3_salida_Asignado_A").val("--??????????--");
				$("#dashlet3_salida_Respuesta").val("--??????????--");
				//canalRecepcion
				//categoria
				//compania
			}
		}
	},
	dashlet3_consultar: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.casos = [];
		$("#dashlet3_tabla").html("");//limpiando	
		if (!_.isEmpty( $("#dashlet3_No_Identificacion").val() )){	
			self.dashlet_bizagi_call();
			if (!_.isEmpty( self.respuesta )){ 		
				console.log('dashlet3_consultar ',self.respuesta);	
				if (typeof self.respuesta.consultaSolicitudResponseWS !== 'undefined'){				
					if(!$.isArray( self.respuesta.consultaSolicitudResponseWS )){			
						var temp = self.respuesta.consultaSolicitudResponseWS;
						self.respuesta.consultaSolicitudResponseWS = [];
						self.respuesta.consultaSolicitudResponseWS.push(temp);
					}
					self.casos = self.respuesta.consultaSolicitudResponseWS;
					if (Array.isArray(self.respuesta.consultaSolicitudResponseWS)){
						app.alert.dismiss('dashlet3_portal_procesando');
						app.alert.dismiss('info_crear');
						app.alert.show('info_crear', {
							level: 'info',
							title: 'Consultado',
							messages: self.respuesta.descripcion,
							autoClose: true,
						});
						$.each(self.respuesta.consultaSolicitudResponseWS, function( i, caso ) {
							$("#dashlet3_tabla").append("<tr><td>"+caso.numeroDeCaso+"</td><td>"+caso.fechaApertura+"</td><td>"+caso.estado+"</td><td>"+caso.tipoSolicitud+"</td><td style='display:none'>--???????--</td></tr>");
						});
						self.dashlet3_salida_no_de_caso_change();
					}
					else{	
						$("#dashlet3_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando				
						app.alert.dismiss('dashlet3_portal_procesando');
						app.alert.dismiss('error_crear');
						app.alert.show('error_crear', {
							level: 'error',
							title: 'Error al crear por WS2:<p>'+JSON.stringify(self.respuesta,null,'\t'),
							autoClose: false,
						});
					}
				}
				else{		
					$("#dashlet3_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando				
					app.alert.dismiss('dashlet3_portal_procesando');
					app.alert.dismiss('error_crear');
					app.alert.show('error_crear', {
						level: 'error',
						title: 'Error al crear por WS1:<p>'+self.respuesta.descripcion,
						autoClose: false,
					});
				}
			}
			else{		
				$("#dashlet3_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando					
				app.alert.dismiss('dashlet3_portal_procesando');
				app.alert.dismiss('error_crear');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Error al crear por WS0:<p>'+JSON.stringify(self.respuesta,null,'\t'),
					autoClose: false,
				});
			}
		}
		else{
			$("#dashlet3_tabla").append("<tr><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td></tr>");//limpiando			
			app.alert.dismiss('dashlet3_portal_procesando');
			app.alert.dismiss('error_crear');
			app.alert.show('error_crear', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}
		self.dashlet3_salida_no_de_caso_change();		
	},
	dashlet_bizagi_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		var numeroDocumento = $("#dashlet3_No_Identificacion").val();	
		var method  = "dashlet_bizagi";		
		var data = [
			{
				'url':self.url,
				'ws':self.ws,
				'tipo':'POST',
				'Username':self.Username,
				'Password':self.Password,
			},
			{		
				'canalRecepcion':'',
				'estado':'',
				'fechaAperturaFin':'',
				'fechaAperturaInicio':'',
				'identificacionCliente':$("#dashlet3_No_Identificacion").val(),
				'numeroCaso':'',
				
				'xmlns:soapenv':'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"',
				'xmlns:web':'xmlns:web="http://web.ws.sqdm.com/"',
				'web':'consultaSolicitud',
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_bizagi_procesando');
				
				if (typeof response !== 'undefined')
				if (typeof response.descripcion !== 'undefined')
				if (response.descripcion == 'Error de autenticación. Por favor valide sus credenciales')
				app.alert.show('error_dashlet3_autenticacion', {
					level: 'error',
					title: 'Bizagi3: '+response.descripcion,
					autoClose: false,
				});
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_bizagi_procesando');
				app.alert.dismiss('error_procesando_pedido');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
})
