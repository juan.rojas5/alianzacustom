<?php
// created: 2022-01-26 20:02:52
$mapping = array (
  'beans' => 
  array (
    'Accounts' => 
    array (
      'name' => 'name',
      'id' => 'id',
    ),
    'Contacts' => 
    array (
      'name' => 'full_name',
      'id' => 'id',
    ),
    'Leads' => 
    array (
      'name' => 'account_name',
      'id' => 'id',
    ),
    'Prospects' => 
    array (
      'name' => 'account_name',
      'id' => 'id',
    ),
  ),
);