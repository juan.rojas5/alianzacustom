<?php
$viewdefs['Notes'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labels' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 'contact_name',
              1 => 'parent_name',
              2 => 
              array (
                'name' => 'description',
                'rows' => 5,
              ),
              3 => 'team_name',
              4 => 
              array (
                'name' => 'assigned_user_name',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'tag',
                'span' => 12,
              ),
              7 => 
              array (
                'name' => 'attachment_list',
                'label' => 'LBL_ATTACHMENTS',
                'type' => 'multi-attachments',
                'link' => 'attachments',
                'module' => 'Notes',
                'modulefield' => 'filename',
                'bLabel' => 'LBL_ADD_ATTACHMENT',
                'max_num' => -1,
                'related_fields' => 
                array (
                  0 => 'filename',
                  1 => 'file_mime_type',
                ),
                'fields' => 
                array (
                  0 => 'name',
                  1 => 'filename',
                  2 => 'file_size',
                  3 => 'file_source',
                  4 => 'file_mime_type',
                  5 => 'file_ext',
                  6 => 'upload_id',
                ),
              ),
              8 => 
              array (
              ),
              9 => 
              array (
                'readonly' => false,
                'name' => 'sasa_id_sfc_c',
                'label' => 'LBL_SASA_ID_SFC_C',
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'sasa_referencia_c',
                'label' => 'LBL_SASA_REFERENCIA_C',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 'portal_flag',
              3 => 
              array (
                'name' => 'embed_flag',
                'comment' => 'Embed flag indicator determines if note embedded in email',
                'label' => 'LBL_EMBED_FLAG',
              ),
              4 => 
              array (
                'name' => 'file_mime_type',
                'comment' => 'Attachment MIME type',
                'label' => 'LBL_FILE_MIME_TYPE',
              ),
              5 => 
              array (
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
