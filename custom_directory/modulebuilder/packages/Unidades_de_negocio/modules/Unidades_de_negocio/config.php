<?php
// created: 2023-04-25 03:53:07
$config = array (
  'team_security' => true,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Unidades de negocio',
  'label_singular' => 'Unidades de negocio',
  'importable' => true,
);