<?php
// created: 2019-03-08 18:57:52
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1_name"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_produff64tosafav_ida',
  'link' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasa_produff64tosafav_ida"] = array (
  'name' => 'sasa_produff64tosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE_ID',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
