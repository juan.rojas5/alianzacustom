<?php
 // created: 2018-07-18 04:33:11
$dictionary['Lead']['fields']['assistant_phone']['len']='20';
$dictionary['Lead']['fields']['assistant_phone']['audited']=false;
$dictionary['Lead']['fields']['assistant_phone']['massupdate']=false;
$dictionary['Lead']['fields']['assistant_phone']['comments']='';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['assistant_phone']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['assistant_phone']['merge_filter']='disabled';
$dictionary['Lead']['fields']['assistant_phone']['reportable']=false;
$dictionary['Lead']['fields']['assistant_phone']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['assistant_phone']['calculated']=false;
$dictionary['Lead']['fields']['assistant_phone']['pii']=false;
$dictionary['Lead']['fields']['assistant_phone']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 ?>