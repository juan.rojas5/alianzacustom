<?php
 // created: 2022-10-25 23:16:57
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['labelValue']='Gerente del segmento';
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['enforced']='';
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['dependency']='';
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_geresentesegmento_c']['readonly_formula']='';

 ?>