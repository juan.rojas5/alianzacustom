<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class VIEndpointsApi extends SugarApi
{
	public function registerApiRest()
	{
		return array(
			//GET
			'VIEndpointsApi' => array(
				//request type
				'reqType' => array('POST','GET'),

				//set authentication
				'noLoginRequired' => false,

				//endpoint path
				'path' => array('VIEndpointsApi'),

				//endpoint variables
				'pathVars' => array(''),

				//method to call
				'method' => 'VIGetApi',

				//short help string to be displayed in the help documentation
				'shortHelp' => 'An example of a POST endpoint',

				//long help to be displayed in the help documentation
				'longHelp' => 'custom/clients/base/api/help/VIGetApi.html',
			),
		);
	}

	/**
	* Method to be used for my MyEndpoint/GetExample endpoint
	*/

	public function VIGetApi($api, $args)
	{	
		global $db;
		//agregar validaccion de datos

		//Construir la estructura del objeto respuesta
		$ResponseArray=array(
			"Datos"=>array(
				"IDAsesor"=>"",
				"NombreAsesor"=>"",
				"CorreoAsesor"=>"",
				"IDCliente"=>"",
				"NombreCliente"=>"",
				"RegistrosRelacionados"=>""
			),
			"Mensaje"=>""
		);
		//cuando se envie un comercial se envia la comannia

		//Tarea 16165 Consultar si existe cuentas con Numero de identidad y Tipo de documento
		$Accounts = BeanFactory::newBean("Accounts");
		$Accounts->retrieve_by_string_fields(
			array(
				'sasa_nroidentificacion_c' => $args["Numero_de_identificacion"],
				'sasa_tipoidentificacion_c'=>$args["Tipo_de_indentificacion"]
			)
		);

		if(!empty($Accounts->id)){
			//Si existe la cuenta con el numero de identificacion y tipo de documento
			//H2
			$Account = BeanFactory::getBean("Accounts", $Accounts->id, array('disable_row_level_security' => true));
			if($this->CheckAssignedUser($Account)){

				$this->CreateTaskToAdviser($Account);
				$this->DateVinculacion($Account);
			//respuesta
				$ResponseArray["Mensaje"]="M1";
				$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;	
				$ResponseArray["Datos"]["NombreAsesor"]=$Account->assigned_user_name;
				$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
				$ResponseArray["Datos"]["IDCliente"]=$Account->id;
				$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
				$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';

				return $ResponseArray;

			}else{
				$UsuariosAMostrar=$this->LookUpAssignedUserInRelatedFilds($Account->id);
				$this->DateVinculacion($Account);
				if($UsuariosAMostrar!=false){
					//respuesta
					//se crea tarea ?
					//$this->CreateTaskToAdviser($lead);
					$ResponseArray["Mensaje"]="M2";
					$ResponseArray["Datos"]["IDAsesor"]=null;
					$ResponseArray["Datos"]["NombreAsesor"]=null;
					$ResponseArray["Datos"]["CorreoAsesor"]=null;
					$ResponseArray["Datos"]["IDCliente"]=$Account->id;
					$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
					$ResponseArray["Datos"]["RegistrosRelacionados"]=$UsuariosAMostrar;

					return $ResponseArray;

				}else{
					$Account=$this->RandonAssigned($args);
					$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
					//respuesta
					$ResponseArray["Mensaje"]="M3";
					$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
					$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
					$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
					$ResponseArray["Datos"]["IDCliente"]=$Account->id;
					$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
					$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
					return $ResponseArray;

				}

			}	
		}else{
			//H1
			//Si NO existe la cuenta con el numero de identificacion y tipo de documento
			//Tarea 16166 Desarrollo para consultar si existe el cliente en Clientes Potenciales con doc. de identidad.
			$Leads = BeanFactory::newBean("Leads");
			//Agregar validacion de Tipo de documento
			$Leads->retrieve_by_string_fields(
				array(
					'sasa_nroidentificacion_c' => $args["Numero_de_identificacion"],
					'sasa_tipoidentificacion_c'=>$args["Tipo_de_indentificacion"]
				)
			);
			if(!empty($Leads->id)){
				//Existe con Numero de identificacion
				$lead = BeanFactory::getBean("Leads", $Leads->id, array('disable_row_level_security' => true));
				$this->ActualizarDato($lead,$args);
				//Validar el asignado a	
				if($this->CheckAssignedUser($lead)){
					if($this->ConvertCPToC($lead)){
						//Crear tarea para notificar 
						//Si cuenta con usuario Notificar al asesor mediante la creacion de una tarea 
						$this->CreateTaskToAdviser($lead);
						//Retonar a quien se le creo la tarea
						//En este caso el ID del cliente hace referencia a el ID del lead
						$ResponseArray["Mensaje"]="M4";
						$ResponseArray["Datos"]["IDAsesor"]=$lead->assigned_user_id;
						$ResponseArray["Datos"]["NombreAsesor"]=$lead->assigned_user_name;
						$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($lead->assigned_user_id);
						$ResponseArray["Datos"]["IDCliente"]=$lead->id;
						$ResponseArray["Datos"]["NombreCliente"]=$lead->name;
						$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
						return $ResponseArray;
					}else{
						//No logro crear la cuenta y contado
						//Crear cuenta 
						$this->CreateAccountContac($args);
						$Account=$this->RandonAssigned($args);
						if (!empty($Account->id)) {
							//Nueva instancia del bean de account para traer nueva info
							$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
							$this->DateVinculacion($Account);

							$ResponseArray["Mensaje"]="M4.2";
							$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
							$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
							$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
							$ResponseArray["Datos"]["IDCliente"]=$Account->id;
							$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}else{
							//Error
							$ResponseArray["Mensaje"]="M4.3";
							$ResponseArray["Datos"]["IDAsesor"]='';
							$ResponseArray["Datos"]["NombreAsesor"]='';
							$ResponseArray["Datos"]["CorreoAsesor"]='';
							$ResponseArray["Datos"]["IDCliente"]='';
							$ResponseArray["Datos"]["NombreCliente"]='';
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}
					}

				}else{
					if($this->ConvertCPToC($lead)){
						//Responder con el asesor aleatorio creado
						$Account=$this->RandonAssigned($args);
						if (!empty($Account->id)) {
							//Nueva instancia del bean de account para traer nueva info
							$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
							$this->DateVinculacion($Account);
							//Crear tarea
							//$this->CreateTaskToAdviser($lead);
							$ResponseArray["Mensaje"]="M5";
							$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
							$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
							$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
							$ResponseArray["Datos"]["IDCliente"]=$Account->id;
							$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}else{
							//No encontro el registro creado en la conversion
							//Crear cuenta 
							$this->CreateAccountContac($args);
							$Account=$this->RandonAssigned($args);
							if (!empty($Account->id)) {
							//Nueva instancia del bean de account para traer nueva info
								$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
								$this->DateVinculacion($Account);

								$ResponseArray["Mensaje"]="M6";
								$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
								$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
								$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
								$ResponseArray["Datos"]["IDCliente"]=$Account->id;
								$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
								$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
								return $ResponseArray;
							}else{
								//Error
								$ResponseArray["Mensaje"]="M7";
								$ResponseArray["Datos"]["IDAsesor"]='';
								$ResponseArray["Datos"]["NombreAsesor"]='';
								$ResponseArray["Datos"]["CorreoAsesor"]='';
								$ResponseArray["Datos"]["IDCliente"]='';
								$ResponseArray["Datos"]["NombreCliente"]='';
								$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
								return $ResponseArray;
							}
						}	
					}else{
						//No logro crear la cuenta y contado
						//Crear cuenta 
						$this->CreateAccountContac($args);
						$Account=$this->RandonAssigned($args);
						if (!empty($Account->id)) {
							//Nueva instancia del bean de account para traer nueva info
							$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
							$this->DateVinculacion($Account);

							$ResponseArray["Mensaje"]="M8";
							$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
							$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
							$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
							$ResponseArray["Datos"]["IDCliente"]=$Account->id;
							$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}else{
							//Error
							$ResponseArray["Mensaje"]="M9";
							$ResponseArray["Datos"]["IDAsesor"]='';
							$ResponseArray["Datos"]["NombreAsesor"]='';
							$ResponseArray["Datos"]["CorreoAsesor"]='';
							$ResponseArray["Datos"]["IDCliente"]='';
							$ResponseArray["Datos"]["NombreCliente"]='';
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}
					}
				}
			}else{
				//Tarea 16167 Desarrollo para consultar si existe el cliente en Clientes Potenciales con Nombres y Apellidos o correo electrónico o número telefónico.
				$NombreCompleto=$args['Nombre_1']." ".$args['Nombre_2'];
				$Apellido=$args['Apellido_1']." ".$args['Apellido_2'];
				$query="SELECT leads.id FROM leads LEFT JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=leads.id LEFT JOIN email_addresses ON email_addresses.id=email_addr_bean_rel.email_address_id INNER JOIN leads_cstm ON leads_cstm.id_c = leads.id WHERE leads.first_name = '{$NombreCompleto}' AND leads.last_name = '{$Apellido}' OR leads_cstm.sasa_telefono_c = '{$args['Telefono']}' OR leads.phone_mobile = '{$args['Movil']}' OR email_addresses.email_address='{$args['Correo_electronico']}' OR email_addresses.email_address_caps='{$args['Correo_electronico']}' AND leads.deleted=0 ORDER BY leads.date_modified DESC";

				$result = $db->query($query);  
				while($row = $db->fetchByAssoc($result)){
					
					if(!empty($row['id'])){
						$LeadByNACN = BeanFactory::getBean("Leads", $row['id'], array('disable_row_level_security' => true));
						$this->ActualizarDato($LeadByNACN,$args);
					//Se encontro por nombre apellido o correo
					//Si cuenta con usuario Notificar al asesor mediante la creacion de una tarea 
						if($this->CheckAssignedUser($LeadByNACN)){

							$this->ConvertCPToC($LeadByNACN);

							$this->CreateTaskToAdviser($LeadByNACN);
					//Retonar a quien se le creo la tarea
							$ResponseArray["Mensaje"]="M10";
							$ResponseArray["Datos"]["IDAsesor"]=$LeadByNACN->assigned_user_id;
							$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($LeadByNACN->assigned_user_id);
							$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($LeadByNACN->assigned_user_id);
							$ResponseArray["Datos"]["IDCliente"]=$LeadByNACN->id;
							$ResponseArray["Datos"]["NombreCliente"]=$LeadByNACN->name;
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;

						}else{
							if($this->ConvertCPToC($LeadByNACN)){
								$Account=$this->RandonAssigned($args);
							//Nueva instancia del bean de account para traer nueva info
								$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
								$this->DateVinculacion($Account);

							//creacion de tarea
							//$this->CreateTaskToAdviser($Account);
								//el lead debe contener el tipo de identificacion y numero
								if (!empty($Account->id)) {
									$ResponseArray["Mensaje"]="M11";
									$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
									$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
									$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
									$ResponseArray["Datos"]["IDCliente"]=$Account->id;
									$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
									$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
									return $ResponseArray;
								}else{
								//Error
									$ResponseArray["Mensaje"]="M12";
									$ResponseArray["Datos"]["IDAsesor"]='';
									$ResponseArray["Datos"]["NombreAsesor"]='';
									$ResponseArray["Datos"]["CorreoAsesor"]='';
									$ResponseArray["Datos"]["IDCliente"]='';
									$ResponseArray["Datos"]["NombreCliente"]='';
									$ResponseArray["Datos"]["RegistrosRelacionados"]='';
									return $ResponseArray;
								}
							}else{
								//No logro crear la cuenta y contado
								//Crear cuenta 
								$this->CreateAccountContac($args);
								$Account=$this->RandonAssigned($args);
								if (!empty($Account->id)) {
								//Nueva instancia del bean de account para traer nueva info
									$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
									$this->DateVinculacion($Account);

									$ResponseArray["Mensaje"]="M11";
									$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
									$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
									$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
									$ResponseArray["Datos"]["IDCliente"]=$Account->id;
									$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
									$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
									return $ResponseArray;
								}else{
								//Error
									$ResponseArray["Mensaje"]="M12";
									$ResponseArray["Datos"]["IDAsesor"]='';
									$ResponseArray["Datos"]["NombreAsesor"]='';
									$ResponseArray["Datos"]["CorreoAsesor"]='';
									$ResponseArray["Datos"]["IDCliente"]='';
									$ResponseArray["Datos"]["NombreCliente"]='';
									$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
									return $ResponseArray;
								}

							}
						}
					}else{
					//Crear cuenta prospecto/Contacto 
						$this->CreateAccountContac($args);
					//assiganaccion aleatoria
						$Account=$this->RandonAssigned($args);
					//Responder con el asesor aleatorio creado
						//Nueva instancia del bean de account para traer nueva info
						$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
						$this->DateVinculacion($Account);
						//creacion de tarea
						$this->CreateTaskToAdviser($Account);

						if (!empty($Account->id)) {
							$ResponseArray["Mensaje"]="M13";
							$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
							$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
							$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
							$ResponseArray["Datos"]["IDCliente"]=$Account->id;
							$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
							$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
							return $ResponseArray;
						}else{
							//Error
							$ResponseArray["Mensaje"]="M14";
							$ResponseArray["Datos"]["IDAsesor"]='';
							$ResponseArray["Datos"]["NombreAsesor"]='';
							$ResponseArray["Datos"]["CorreoAsesor"]='';
							$ResponseArray["Datos"]["IDCliente"]='';
							$ResponseArray["Datos"]["NombreCliente"]='';
							$ResponseArray["Datos"]["RegistrosRelacionados"]='';
							return $ResponseArray;
						}
					}
				}
				//No retorno nada la consulta
				//Crear cuenta prospecto/Contacto 
				$this->CreateAccountContac($args);
					//assiganaccion aleatoria
				$Account=$this->RandonAssigned($args);
					//Responder con el asesor aleatorio creado
						//Nueva instancia del bean de account para traer nueva info
				$Account = BeanFactory::getBean("Accounts", $Account->id, array('disable_row_level_security' => true));
				$this->DateVinculacion($Account);
						//creacion de tarea
				$this->CreateTaskToAdviser($Account);

				if (!empty($Account->id)) {
					$ResponseArray["Mensaje"]="M16";
					$ResponseArray["Datos"]["IDAsesor"]=$Account->assigned_user_id;
					$ResponseArray["Datos"]["NombreAsesor"]=$this->GetName($Account->assigned_user_id);
					$ResponseArray["Datos"]["CorreoAsesor"]=$this->LookForAEmailAddress($Account->assigned_user_id);
					$ResponseArray["Datos"]["IDCliente"]=$Account->id;
					$ResponseArray["Datos"]["NombreCliente"]=$Account->name;
					$ResponseArray["Datos"]["RegistrosRelacionados"]='No Aplica Consulta';
					return $ResponseArray;
				}else{
							//Error
					$ResponseArray["Mensaje"]="M17";
					$ResponseArray["Datos"]["IDAsesor"]='';
					$ResponseArray["Datos"]["NombreAsesor"]='';
					$ResponseArray["Datos"]["CorreoAsesor"]='';
					$ResponseArray["Datos"]["IDCliente"]='';
					$ResponseArray["Datos"]["NombreCliente"]='';
					$ResponseArray["Datos"]["RegistrosRelacionados"]='';
					return $ResponseArray;
				}
			}

		}
	}

	public function RandonAssigned($args){
		//["Numero_de_identificacion"],$args["Tipo_de_indentificacion"]
	//Funcion marcar y asignar aleatoriamente
		$Accounts = BeanFactory::newBean("Accounts");
		$Accounts->retrieve_by_string_fields(
			array(
				'sasa_nroidentificacion_c' => $args["Numero_de_identificacion"],
				'sasa_tipoidentificacion_c'=>$args["Tipo_de_indentificacion"]
			)
		);
		if(!empty($Accounts->id)){
			$Account = BeanFactory::getBean("Accounts", $Accounts->id, array('disable_row_level_security' => true));
			//Flag para asignaccion aleatoria de la cuenta
			$Account->sasa_asignacionaleatoriavi_c=true;
			//agregar a la cuenta los ingresos
			$Account->sasa_ingresosventasmensual_c=$args["Ingresos"];
			//Agregar tipo de persona
			$Account->sasa_tipopersona_c=$args["Tipo_de_Persona"];
			//Agregar regional
			$Account->sasa_regional_control_c=$args["Regional"];
			//patrimonio_Reportado
			$Account->sasa_patrimonio_c=$args["patrimonio_Reportado"];
			$Account->save();
			return $Account;
		}else{
			return false;//"Un error a ocurrido no existe la cuenta"
		}

	}

	public function CheckAssignedUser($Lead){
	//Validar si cuenta con estado de No Asignado/Libre
	// en caso de estar con No Asignado/Libre
		if($Lead->assigned_user_name=='No Asignado/Libre' || $Lead->assigned_user_name==''){
			return false;
		}else{
			return true;
		}
	}

	public function CreateTaskToAdviser($lead){

	//Fechas
		$hoy = date("Y-m-d H:i:s");
		$PasadoManana=date('Y-m-d H:i:s', strtotime($hoy. ' + 2 days'));   

	//Crear Tarea que notificara al asesor 
		$Task = BeanFactory::newBean('Tasks');
	//nombre de la tarea
		$Task->name = 'Gestionar cliente Vinculación Digital';
	//parent_id
		$Task->parent_id=$lead->id;
	//parent_type
		$Task->parent_type=$lead->module_name;
	//assigned_user_id
		$Task->assigned_user_id=$lead->assigned_user_id;
	//description
		$Task->description='Realizar gestión para cliente que realizó el proceso de vinculación digital';
	//date_entered
		$Task->date_entered=$hoy;
	//date_modified
		$Task->date_modified=$hoy;
	//date_due
		$Task->date_due=$PasadoManana;
	//date_start
		$Task->date_start=$hoy;
	//priority
		$Task->priority='High';
	//* sasa_responsableejecucion_c
		$Task->sasa_responsableejecucion_c='Comercial';
	//Save
		$Task->save();

	}

	public function ConvertCPToC($lead){
	//Crear registro Cuenta prospeto(Convertir) y contacto y asociarlos
	//$bean->status == "In Process" and $bean->converted == false and empty($bean->account_id) and empty($bean->contact_id)
	//Se usa un desarrollo ya existente para este paso
		if($lead->status!="Converted" and $lead->converted == false and empty($lead->account_id) and empty($lead->contact_id)){
			$lead->status="In Process";
			$lead->save();
			return true;
		}else{
			return false;//"Aca no deberia entrar ni por el pu**";
		}


	}

	public function LookForAEmailAddress($id){
		global $db;
	//Consultar el corre apartir del id de un modulo
	//email_addr_bean_rel
	//email_addresses
		$query="SELECT email_address_id FROM email_addr_bean_rel WHERE bean_id='{$id}' AND primary_address=1 LIMIT 1";
		$result = $db->query($query);  
		while($row = $db->fetchByAssoc($result)){
			$EAID=$row['email_address_id'];
			$QueryCorreos="SELECT email_address FROM email_addresses WHERE id='{$EAID}' LIMIT 1";
			$resultwo=$db->query($QueryCorreos);
			while($rowtwo = $db->fetchByAssoc($resultwo)){
				return $rowtwo['email_address'];
			}
		}
	}

	public function CreateAccountContac($args){
		//Crear cuenta/Contacto prospecto
		$Account = new Account();
		$Contact = new Contact();

		foreach ($args as $key => $value) {
			if ($key=='Tipo_de_indentificacion') {
				$Account->sasa_tipoidentificacion_c=$value;
				$Contact->sasa_tipoidentificacion_c=$value;
			}
			if ($key=='Numero_de_identificacion') {
				$Account->sasa_nroidentificacion_c=$value;
				$Contact->sasa_nroidentificacion_c=$value;
			}
			if ($key=='Correo_electronico') {
				$Account->email1=$value;
				$Contact->email1=$value;
			}

			if ($key=='Telefono') {
				$Account->sasa_telefono_c=$value;
				$Contact->sasa_telefono_c=$value;
			}
			if ($key=='Movil') {
				$Contact->phone_mobile=$value;
			}

			if ($key=='Ingresos') {
				$Account->sasa_ingresosventasanuales_c=$value;
			}
			if ($key=='Tipo_de_Persona') {
				$Account->sasa_tipopersona_c=$value;
				//$Contact->name=$value; Campo no existe
			}
			if ($key=='Regional') {
				$Account->sasa_regional_control_c=$value;
				//$Contact->sasa_regional_c=$value;sasa_regional_control_c
			}
			if ($key=='patrimonio_Reportado') {
				$Account->sasa_patrimonio_c=$value;
				//$Contact->sasa_regional_c=$value;sasa_regional_control_c
			}
		}
		if ($args["Tipo_de_Persona"]=='Juridica' and $args["Razon_Social"]!="") {
			$Account->name=$args["Razon_Social"];
			//account_name
		}else{
			$Account->name=$args["Nombre_1"]." ".$args["Nombre_2"]." ".$args["Apellido_1"]." ".$args["Apellido_2"];
		}

		$Contact->first_name=$args["Nombre_1"]." ".$args["Nombre_2"];
		$Contact->last_name=$args["Apellido_1"]." ".$args["Apellido_2"];
		$Account->account_type = "Prospect"; //requerimiento de negocio	
		
		$Account->save(); //Se debe pasar un Objeto con id
		$Contact->save(); //Se debe pasar un Objeto con id

		$Account->load_relationship("contacts"); //Ya se debe tener id de cuenta y contacto
		$Account->contacts->add($Contact);


	}

	public function LookUpAssignedUserInRelatedFilds($parent_id){	
		global $db;
		//Llamadas, visitas, tareas y correos
		$QueryModuls=array('Tasks','Meetings','Calls','Emails');
		$UsuarioAsignado=array();
		foreach ($QueryModuls as $value) {
			$sugarQuery = new SugarQuery();
			$sugarQuery->from(BeanFactory::newBean($value));
			$sugarQuery->where()->contains('parent_id', $parent_id);
			$result = $sugarQuery->execute();

			foreach ($result as $key => $value2) {
				if(!in_array($value2['assigned_user_id'], $UsuarioAsignado, true)){
					array_push($UsuarioAsignado,$value2['assigned_user_id']);
					$UsuarioConsultar=$value2['assigned_user_id'];
					$query="SELECT * FROM `users` WHERE id='{$UsuarioConsultar}' AND status='Active' AND id<>'026932c0-66a1-11e8-ad97-0699afa2cd79'";
					$result = $db->query($query);  
					while($row = $db->fetchByAssoc($result)){
						if(!empty($row['id'])){
							$Nombre.=$row['user_name'].",";
							$ID.=$row['id'].",";
						}
					}
				}
			}
		}
		//Consultar si el nombre en usuarios es el correo
		if (!empty($Nombre)) {
			return array('Correo'=>$Nombre,'IDs'=> $ID);
		}else{
			return false;
		}
		
	}

	public function GetName($NameID){
		global $db;
		$query="SELECT * FROM `users` WHERE id='{$NameID}'";
		$result = $db->query($query);
		while($row = $db->fetchByAssoc($result)){
			if(!empty($row['id'])){
				return $row['first_name']." ".$row['last_name'];
			}
		}
		return "Nombre no encontrado";
		
	}
	public function ActualizarDato($Lead,$args){	

		if (empty($Lead->sasa_nroidentificacion_c)) {
			// actualizar
			$Lead->sasa_nroidentificacion_c=$args["Numero_de_identificacion"];
			//$lead->save();
		}

		if (empty($Lead->sasa_tipoidentificacion_c)) {
			// actualizar
			$Lead->sasa_tipoidentificacion_c=$args["Tipo_de_indentificacion"];
			//$lead->save();
		}

		$Lead->account_name=$args["Razon_Social"];

		//patrimonio 

		$Lead->sasa_patrimonio_c=$args["patrimonio_Reportado"];

		$Lead->save();
	}

	public function DateVinculacion($Modulo){
		//Funcion para poblar el campo con la fecha actual

		$date = date("Y-m-d H:i:s");
		$date = new DateTime($date);
		$dateformat = $date->format('Y-m-d');
		$Modulo->sasa_fechavinculaci_ndigital_c=$dateformat;
		$Modulo->save();
	}

}

//account_name campo a poblar con razon social

?>
