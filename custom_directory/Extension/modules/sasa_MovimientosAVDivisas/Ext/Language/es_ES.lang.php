<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_MOVIMIENTOSAVDIVISAS_FOCUS_DRAWER_DASHBOARD'] = 'Movimientos AV (Divisas) Panel de enfoque';
$mod_strings['LBL_SASA_MOVIMIENTOSAVDIVISAS_RECORD_DASHBOARD'] = 'Movimientos AV (Divisas) Cuadro de mando del registro';
$mod_strings['LNK_LIST'] = 'Vista Movimientos AV Divisas';
$mod_strings['LBL_MODULE_NAME'] = 'Movimientos AV Divisas';
$mod_strings['LNK_IMPORT_SASA_MOVIMIENTOSAVDIVISAS'] = 'Importar Movimientos AV Divisas';
$mod_strings['LBL_SASA_MOVIMIENTOSAVDIVISAS_SUBPANEL_TITLE'] = 'Movimientos AV Divisas';
$mod_strings['LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE'] = 'Productos AFAV';
