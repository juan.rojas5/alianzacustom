<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_name.php

 // created: 2018-05-29 19:06:39
$dictionary['Task']['fields']['name']['audited']=true;
$dictionary['Task']['fields']['name']['massupdate']=false;
$dictionary['Task']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.45',
  'searchable' => true,
);
$dictionary['Task']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-05-29 19:07:04
$dictionary['Task']['fields']['date_entered']['audited']=true;
$dictionary['Task']['fields']['date_entered']['comments']='Date record created';
$dictionary['Task']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_entered']['unified_search']=false;
$dictionary['Task']['fields']['date_entered']['calculated']=false;
$dictionary['Task']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-05-29 19:07:34
$dictionary['Task']['fields']['date_modified']['audited']=true;
$dictionary['Task']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Task']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Task']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_modified']['unified_search']=false;
$dictionary['Task']['fields']['date_modified']['calculated']=false;
$dictionary['Task']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_description.php

 // created: 2018-05-29 19:08:28
$dictionary['Task']['fields']['description']['audited']=true;
$dictionary['Task']['fields']['description']['massupdate']=false;
$dictionary['Task']['fields']['description']['comments']='Full text of the note';
$dictionary['Task']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['description']['merge_filter']='disabled';
$dictionary['Task']['fields']['description']['unified_search']=false;
$dictionary['Task']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.56',
  'searchable' => true,
);
$dictionary['Task']['fields']['description']['calculated']=false;
$dictionary['Task']['fields']['description']['rows']='6';
$dictionary['Task']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_status.php

 // created: 2018-05-29 19:09:18
$dictionary['Task']['fields']['status']['audited']=true;
$dictionary['Task']['fields']['status']['massupdate']=true;
$dictionary['Task']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['status']['merge_filter']='disabled';
$dictionary['Task']['fields']['status']['unified_search']=false;
$dictionary['Task']['fields']['status']['full_text_search']=array (
);
$dictionary['Task']['fields']['status']['calculated']=false;
$dictionary['Task']['fields']['status']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_due.php

 // created: 2018-05-30 14:17:32
$dictionary['Task']['fields']['date_due']['audited']=true;
$dictionary['Task']['fields']['date_due']['massupdate']=true;
$dictionary['Task']['fields']['date_due']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_due']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_due']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_due']['unified_search']=false;
$dictionary['Task']['fields']['date_due']['full_text_search']=array (
);
$dictionary['Task']['fields']['date_due']['calculated']=false;
$dictionary['Task']['fields']['date_due']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_date_start.php

 // created: 2018-05-30 14:18:06
$dictionary['Task']['fields']['date_start']['audited']=true;
$dictionary['Task']['fields']['date_start']['massupdate']=true;
$dictionary['Task']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Task']['fields']['date_start']['unified_search']=false;
$dictionary['Task']['fields']['date_start']['calculated']=false;
$dictionary['Task']['fields']['date_start']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_ctrlwf_c.php

 // created: 2018-06-24 15:01:04
$dictionary['Task']['fields']['sasa_ctrlwf_c']['labelValue']='Ctrl WF tipo tarea';
$dictionary['Task']['fields']['sasa_ctrlwf_c']['dependency']='';
$dictionary['Task']['fields']['sasa_ctrlwf_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_responsableejecucion_c.php

 // created: 2018-07-10 22:27:02
$dictionary['Task']['fields']['sasa_responsableejecucion_c']['labelValue']='Responsable de la Ejecución';
$dictionary['Task']['fields']['sasa_responsableejecucion_c']['dependency']='';
$dictionary['Task']['fields']['sasa_responsableejecucion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_vencida_c.php

 // created: 2018-07-21 21:02:25
$dictionary['Task']['fields']['sasa_vencida_c']['labelValue']='Vencida';
$dictionary['Task']['fields']['sasa_vencida_c']['dependency']='';
$dictionary['Task']['fields']['sasa_vencida_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2018-07-26 03:43:07
$dictionary['Task']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Task']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Task']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2018-07-26 03:44:13
$dictionary['Task']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Task']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Task']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/rli_link_workflow.php

$dictionary['Task']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Task']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_ctrl_sec_c.php

 // created: 2022-09-15 16:44:49
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['labelValue']='Ctrl Sec';
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['enforced']='';
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['dependency']='';
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_ctrl_sec_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_tipo_tarea_list_c.php

 // created: 2022-09-15 16:49:31
$dictionary['Task']['fields']['sasa_tipo_tarea_list_c']['labelValue']='Tipo de Tarea';
$dictionary['Task']['fields']['sasa_tipo_tarea_list_c']['dependency']='';
$dictionary['Task']['fields']['sasa_tipo_tarea_list_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_tipo_tarea_list_c']['readonly_formula']='';
$dictionary['Task']['fields']['sasa_tipo_tarea_list_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_lista_respuesta_c.php

 // created: 2022-09-15 16:51:10
$dictionary['Task']['fields']['sasa_lista_respuesta_c']['labelValue']='Respuesta (lista)';
$dictionary['Task']['fields']['sasa_lista_respuesta_c']['dependency']='equal($sasa_tipo_tarea_list_c,1)';
$dictionary['Task']['fields']['sasa_lista_respuesta_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_lista_respuesta_c']['readonly_formula']='';
$dictionary['Task']['fields']['sasa_lista_respuesta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_control_status_c.php

 // created: 2022-09-16 13:30:42
$dictionary['Task']['fields']['sasa_control_status_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_control_status_c']['labelValue']='Control Status';
$dictionary['Task']['fields']['sasa_control_status_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_control_status_c']['calculated']='true';
$dictionary['Task']['fields']['sasa_control_status_c']['formula']='concat($status,$sasa_ctrl_sec_c)';
$dictionary['Task']['fields']['sasa_control_status_c']['enforced']='true';
$dictionary['Task']['fields']['sasa_control_status_c']['dependency']='';
$dictionary['Task']['fields']['sasa_control_status_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_control_status_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_controlresplist_c.php

 // created: 2022-09-16 13:32:35
$dictionary['Task']['fields']['sasa_controlresplist_c']['duplicate_merge_dom_value']=0;
$dictionary['Task']['fields']['sasa_controlresplist_c']['labelValue']='Control Lista resp';
$dictionary['Task']['fields']['sasa_controlresplist_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_controlresplist_c']['calculated']='1';
$dictionary['Task']['fields']['sasa_controlresplist_c']['formula']='concat($status,$sasa_ctrl_sec_c,$sasa_lista_respuesta_c)';
$dictionary['Task']['fields']['sasa_controlresplist_c']['enforced']='1';
$dictionary['Task']['fields']['sasa_controlresplist_c']['dependency']='equal($sasa_tipo_tarea_list_c,"1")';
$dictionary['Task']['fields']['sasa_controlresplist_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_controlresplist_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_tipoproceso_c.php

 // created: 2022-12-02 16:48:59
$dictionary['Task']['fields']['sasa_tipoproceso_c']['labelValue']='Tipo proceso';
$dictionary['Task']['fields']['sasa_tipoproceso_c']['dependency']='';
$dictionary['Task']['fields']['sasa_tipoproceso_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_tipoproceso_c']['readonly_formula']='';
$dictionary['Task']['fields']['sasa_tipoproceso_c']['visibility_grid']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/sugarfield_sasa_cfv_c.php

 // created: 2022-12-09 20:40:20
$dictionary['Task']['fields']['sasa_cfv_c']['labelValue']='Control fecha vencimiento';
$dictionary['Task']['fields']['sasa_cfv_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Task']['fields']['sasa_cfv_c']['enforced']='';
$dictionary['Task']['fields']['sasa_cfv_c']['dependency']='';
$dictionary['Task']['fields']['sasa_cfv_c']['required_formula']='';
$dictionary['Task']['fields']['sasa_cfv_c']['readonly_formula']='';

 
?>
