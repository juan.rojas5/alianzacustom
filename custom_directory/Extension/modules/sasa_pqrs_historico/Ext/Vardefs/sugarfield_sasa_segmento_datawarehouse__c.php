<?php
 // created: 2022-11-18 20:29:13
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['labelValue']='Segmento_DataWareHouse';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_segmento_datawarehouse__c']['readonly_formula']='';

 ?>