<?php
// created: 2022-10-31 07:45:21
$viewdefs['Accounts']['mobile']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_estadocuenta_c',
          'label' => 'LBL_SASA_ESTADOCUENTA_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'account_type',
          'label' => 'LBL_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_tipopersona_c',
          'label' => 'LBL_SASA_TIPOPERSONA_C',
          'enabled' => true,
          'default' => false,
        ),
        6 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => false,
        ),
        7 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
          'enabled' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);