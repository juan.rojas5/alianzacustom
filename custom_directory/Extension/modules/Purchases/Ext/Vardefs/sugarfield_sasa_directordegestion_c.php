<?php
 // created: 2023-02-01 19:13:10
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['labelValue']='director de gestión';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['readonly_formula']='';

 ?>