<?php
// created: 2023-04-25 03:53:10
$mod_strings = array (
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_TAGS_LINK' => 'العلامات',
  'LBL_TAGS' => 'العلامات',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_EXPORT_CREATED_BY_NAME' => 'اسم جهة الإنشاء',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio القائمة',
  'LBL_MODULE_NAME' => 'Unidades de negocio',
  'LBL_MODULE_TITLE' => 'Unidades de negocio',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidades de negocio',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Unidades de negocio',
  'LNK_NEW_RECORD' => 'إنشاء Unidades de negocio',
  'LNK_LIST' => 'عرض Unidades de negocio',
  'LNK_IMPORT_SASA2_UNIDADES_DE_NEGOCIO' => 'Importar Unidades de negocio',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Unidades de negocio',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط الكلي',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_SUBPANEL_TITLE' => 'Unidades de negocio',
  'LBL_NEW_FORM_TITLE' => 'جديد Unidades de negocio',
  'LNK_IMPORT_VCARD' => 'Importar Unidades de negocio vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidades de negocio record by importing a vCard from your file system.',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio Panel de enfoque',
  'LBL_SASA2_UNIDADES_DE_NEGOCIO_RECORD_DASHBOARD' => 'Unidades de negocio Cuadro de mando del registro',
);