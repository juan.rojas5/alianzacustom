<?php
$dependencies['Leads']['SetOptionDepartamento31'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Valle del Cauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","La Union","San Pedro","Bolivar","Restrepo","Argelia","La Victoria","Candelaria","Alcala","Andalucia","Ansermanuevo","Buenaventura","Bugalagrande","Caicedonia","Cali","Calima","Cartago","Dagua","El aguila","El Cairo","El Cerrito","El Dovio","Florida","Ginebra","Guacari","Guadalajara de Buga","Jamundi","La Cumbre","Obando","Palmira","Pradera","Riofrio","Roldanillo","Sevilla","Toro","Trujillo","Tulua","Ulloa","Versalles","Vijes","Yotoco","Yumbo","Zarzal")',
           'labels' => 'createList("","La Unión","San Pedro","Bolívar","Restrepo","Argelia","La Victoria","Candelaria","Alcalá","Andalucía","Ansermanuevo","Buenaventura","Bugalagrande","Caicedonia","Cali","Calima","Cartago","Dagua","El Águila","El Cairo","El Cerrito","El Dovio","Florida","Ginebra","Guacarí","Guadalajara de Buga","Jamundí","La Cumbre","Obando","Palmira","Pradera","Riofrío","Roldanillo","Sevilla","Toro","Trujillo","Tuluá","Ulloa","Versalles","Vijes","Yotoco","Yumbo","Zarzal")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
