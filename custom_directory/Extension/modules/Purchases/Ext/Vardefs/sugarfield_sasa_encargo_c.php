<?php
 // created: 2023-02-01 15:52:59
$dictionary['Purchase']['fields']['sasa_encargo_c']['labelValue']='Número Encargo';
$dictionary['Purchase']['fields']['sasa_encargo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_encargo_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_encargo_c']['readonly_formula']='';

 ?>