<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['LBL_SASA_RESULTADOLLAMADA_C'] = 'Resultado de la Llamada';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Información del Registro';
$mod_strings['LBL_CALENDAR_END_DATE'] = 'Fecha de Finalización';
$mod_strings['LBL_CALENDAR_START_DATE'] = 'Fecha de inicio';
$mod_strings['LBL_PARENT_TYPE'] = 'Tipo Padre';
$mod_strings['LBL_SASA_FECHAPROXIMALLAMADA_C'] = 'Fecha de la Próxima Llamada';
$mod_strings['LBL_SASA_COMPROMISO_C'] = 'Compromiso';
$mod_strings['LBL_SASA_RESPONSABLEEJECUCION_C'] = 'Responsable de la Ejecución';
$mod_strings['LBL_SASA_OBJETIVOLLAMADA_C'] = 'Objetivo de la Llamada';
$mod_strings['LBL_SASA_VENCIDA_C'] = 'Vencida';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_AGENDAR'] = 'Agendar';
$mod_strings['LBL_SASA_PROGRAMARLLAMADA'] = '¿Programar visita o llamada?';
