<?php
 // created: 2022-09-15 22:13:57
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['labelValue']='Resultado de la Visita';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['visibility_grid']=array (
  'trigger' => 'status',
  'values' => 
  array (
    'Planned' => 
    array (
    ),
    'Held' => 
    array (
      0 => '',
      1 => 'Interesado',
      2 => 'Efectivo',
      3 => 'Negocio en Tramite',
      4 => 'Descartado',
      5 => 'Postergado',
    ),
    'Not Held' => 
    array (
    ),
  ),
);

 ?>