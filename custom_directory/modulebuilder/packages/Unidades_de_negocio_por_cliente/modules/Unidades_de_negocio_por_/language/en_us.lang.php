<?php
// created: 2023-04-25 03:53:09
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Created By Name',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio por cliente List',
  'LBL_MODULE_NAME' => 'Unidades de negocio por cliente',
  'LBL_MODULE_TITLE' => 'Unidades de negocio por cliente',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidad de negocio por cliente',
  'LBL_HOMEPAGE_TITLE' => 'My Unidades de negocio por cliente',
  'LNK_NEW_RECORD' => 'Create Unidad de negocio por cliente',
  'LNK_LIST' => 'View Unidades de negocio por cliente',
  'LNK_IMPORT_SASA1_UNIDADES_DE_NEGOCIO_POR_' => 'Importar Unidades de negocio por cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Search Unidad de negocio por cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__SUBPANEL_TITLE' => 'Unidades de negocio por cliente',
  'LBL_NEW_FORM_TITLE' => 'New Unidad de negocio por cliente',
  'LNK_IMPORT_VCARD' => 'Importar Unidad de negocio por cliente vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio por cliente',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidad de negocio por cliente record by importing a vCard from your file system.',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio por cliente Panel de enfoque',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__RECORD_DASHBOARD' => 'Unidades de negocio por cliente Cuadro de mando del registro',
);