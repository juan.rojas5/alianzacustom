<?php
 // created: 2022-05-18 16:40:40

$app_list_strings['sasa_genero_c_list']=array (
  '' => '',
  'Femenino' => 'Femenino',
  'Masculino' => 'Masculino',
  'Otro' => 'Otro',
  'Trans' => 'Trans',
  'No aplica' => 'No aplica',
  'No binario' => 'No binario',
);