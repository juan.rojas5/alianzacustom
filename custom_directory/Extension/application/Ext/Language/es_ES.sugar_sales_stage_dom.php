<?php
 // created: 2018-08-08 17:49:07

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospección',
  'Qualification' => 'Calificación',
  'Needs Analysis' => 'Análisis de necesidades',
  'Value Proposition' => 'Propuesta de valor',
  'Id. Decision Makers' => 'Identificación de responsables',
  'Perception Analysis' => 'Análisis de percepción',
  'Proposal/Price Quote' => 'Propuesta/Presupuesto',
  'Negotiation/Review' => 'Negociación/Revisión',
  'Closed Won' => 'Cerrado',
  'Closed Lost' => 'Perdida',
);