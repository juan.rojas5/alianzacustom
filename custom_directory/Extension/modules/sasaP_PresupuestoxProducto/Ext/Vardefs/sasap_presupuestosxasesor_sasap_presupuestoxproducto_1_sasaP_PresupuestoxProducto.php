<?php
// created: 2019-03-08 18:56:45
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestosxAsesor',
  'bean_name' => 'sasaP_PresupuestosxAsesor',
  'side' => 'right',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link-type' => 'one',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name"] = array (
  'name' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOSXASESOR_TITLE',
  'save' => true,
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'table' => 'sasap_presupuestosxasesor',
  'module' => 'sasaP_PresupuestosxAsesor',
  'rname' => 'name',
);
$dictionary["sasaP_PresupuestoxProducto"]["fields"]["sasap_pres107bxasesor_ida"] = array (
  'name' => 'sasap_pres107bxasesor_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASAP_PRESUPUESTOSXASESOR_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE_ID',
  'id_name' => 'sasap_pres107bxasesor_ida',
  'link' => 'sasap_presupuestosxasesor_sasap_presupuestoxproducto_1',
  'table' => 'sasap_presupuestosxasesor',
  'module' => 'sasaP_PresupuestosxAsesor',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
