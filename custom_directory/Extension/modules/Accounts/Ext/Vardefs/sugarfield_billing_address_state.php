<?php
 // created: 2019-07-05 21:34:38
$dictionary['Account']['fields']['billing_address_state']['required']=false;
$dictionary['Account']['fields']['billing_address_state']['audited']=true;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['importable']='false';

 ?>