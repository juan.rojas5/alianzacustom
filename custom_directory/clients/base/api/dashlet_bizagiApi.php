<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class dashlet_bizagiApi extends SugarApi
{
	var $data;
	var $token;
	var $respuesta;
	var $file_post;
	public function registerApiRest()
	{
		return array(
		    //GET & POST
		    'dashlet_bizagi' => array(
			//request type
			'reqType' => array('GET','POST'),

			//set authentication
			'noLoginRequired' => false,

			//endpoint path
			'path' => array('dashlet_bizagi'),

			//endpoint variables
			'pathVars' => array(''),

			//method to call
			'method' => 'dashlet_bizagi',

			//short help string to be displayed in the help documentation
			'shortHelp' => 'dashlet_bizagi',

			//long help to be displayed in the help documentation
			//'longHelp' => 'custom/clients/base/api/help/dashlet_bizagiApi_help.html',
		    ),
		);
	}

    /**
     * Method to be used for my MyEndpoint dashlet_bizagi
     */
	public function dashlet_bizagi($api, $args)
	{ 			
		$retorno="";
		try{
			require_once 'modules/Administration/Administration.php';
			$Administration = new Administration();
			$Administration->retrieveSettings("PAI");
			$settings = $Administration->settings["PAI_dashlet_bizagi"];
			
			if($args['data'][0]["url"] == "**PAI**") $args['data'][0]["url"]=$settings["url"];
			if($args['data'][0]["ws"] == "**PAI**") $args['data'][0]["ws"]= $settings["ws"];
			if($args['data'][0]["Username"] == "**PAI**") $args['data'][0]["Username"]= $settings["Username"];
			if($args['data'][0]["Password"] == "**PAI**") $args['data'][0]["Password"]= $settings["Password"];
			
			$this->data = $args['data'];			
			$retorno=$this->dashlet_bizagi_data();
		}
		catch (Exception $e) {      
			$retorno="dashlet_bizagi->. ERROR1:".$e->getMessage(); 
		}
		$GLOBALS['log']->security("\nRetorno:\n");
		$GLOBALS['log']->security($retorno);
		$GLOBALS['log']->security("\n\n*************************************\n\n");
		return $retorno;
	}	

    /**
     * Custom logic of dashlet_bizagi_data
     */
	function dashlet_bizagi_data(){		
		$retorno="";
		try{	
			$data = $this->get_bizagi_body($this->data[1]);
			$result=$this->dashlet_bizagi_call($this->data[0]["url"],$this->data[0]["ws"],$this->data[0]["tipo"],$data,$this->data[0]["Username"],$this->data[0]["Password"]);		
			$retorno = $result;
			$retorno = $result["S:Envelope"]["S:Body"]["ns2:{$this->data[1]["web"]}Response"]["return"];
		$GLOBALS['log']->security("\ndata:\n".$data);			
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_bizagi->. ERROR2:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	/*
	Body XML del WS
	*/
	function get_bizagi_body($data){	
		$retorno="";
		try{	
			$retorno .= "<soapenv:Envelope {$data["xmlns:soapenv"]} {$data["xmlns:web"]}>\n";
			$retorno .= "	<soapenv:Header/>\n";
			$retorno .= "	<soapenv:Body>\n";
			$retorno .= "		<web:{$data["web"]}>\n";
			$retorno .= "			<arg0>\n";
			if($data["web"] =="crearSolicitud"){
				$retorno .= "				<categoria>{$data["categoria"]}</categoria>\n";
				$retorno .= "				<ciudadRadicacion>{$data["ciudadRadicacion"]}</ciudadRadicacion>\n";
				$retorno .= "				<description>{$data["description"]}</description>\n";
				$retorno .= "				<nombreCliente>{$data["nombreCliente"]}</nombreCliente>\n";
				$retorno .= "				<numeroDocumento>{$data["numeroDocumento"]}</numeroDocumento>\n";
				$retorno .= "				<receptionMean>{$data["receptionMean"]}</receptionMean>\n";
				$retorno .= "				<SEmpresaPortal>{$data["SEmpresaPortal"]}</SEmpresaPortal>\n";
				$retorno .= "				<solucionInmediata>{$data["solucionInmediata"]}</solucionInmediata>\n";
				$retorno .= "				<tipificacion1>{$data["tipificacion1"]}</tipificacion1>\n";
				$retorno .= "				<tipificacion2>{$data["tipificacion2"]}</tipificacion2>\n";
				$retorno .= "				<tipificacion3>{$data["tipificacion3"]}</tipificacion3>\n";
				$retorno .= "				<tipodeSolicitud>{$data["tipodeSolicitud"]}</tipodeSolicitud>\n";
			}
			else if($data["web"] =="consultaSolicitud"){
				$retorno .= "				<canalRecepcion>{$data["canalRecepcion"]}</canalRecepcion>\n";
				$retorno .= "				<estado>{$data["estado"]}</estado>\n";
				$retorno .= "				<fechaAperturaFin>{$data["fechaAperturaFin"]}</fechaAperturaFin>\n";
				$retorno .= "				<fechaAperturaInicio>{$data["fechaAperturaInicio"]}</fechaAperturaInicio>\n";
				$retorno .= "				<identificacionCliente>{$data["identificacionCliente"]}</identificacionCliente>\n";
				$retorno .= "				<numeroCaso>{$data["numeroCaso"]}</numeroCaso>\n";
			}
			else if($data["web"] =="listarParametrica"){
				$retorno .= "				<codigoFiltro>{$data["codigoFiltro"]}</codigoFiltro>\n";
				$retorno .= "				<externo>{$data["externo"]}</externo>\n";
				$retorno .= "				<fiduciaria>{$data["fiduciaria"]}</fiduciaria>\n";
				$retorno .= "				<valores>{$data["valores"]}</valores>\n";
				$retorno .= "				<tipoParametrica>{$data["tipoParametrica"]}</tipoParametrica>\n";
			}
			else if($data["web"] =="listarVinculacion"){				
            			$retorno .= "				<identificacion>{$data["identificacion"]}</identificacion>\n";
           			$retorno .= "				<numeroCaso>{$data["numeroCaso"]}</numeroCaso>\n";
			}
			$retorno .= "			</arg0>\n";
			$retorno .= "		</web:{$data["web"]}>\n";
			$retorno .= "	</soapenv:Body>\n";
			$retorno .= "</soapenv:Envelope>\n";
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_bizagi->. ERROR6:".$e->getMessage(); 
		}	
		//$GLOBALS['log']->security($response);
		return $retorno;
	}
	
	/*
	Llamar WS bizagi
	*/
	function dashlet_bizagi_call($url,$ws,$tipo,$data,$Username,$Password){
		$retorno="";
		try{	
			$curl = curl_init();

			curl_setopt_array($curl, 
				array(
					CURLOPT_PORT => "443",
					CURLOPT_URL => "{$url}{$ws}",
					CURLOPT_SSL_VERIFYPEER=>false,			
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30000,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => $tipo,
					CURLOPT_POSTFIELDS => $data,
					CURLOPT_HTTPHEADER => array(
						"Content-Type: text/xml",
						"Password: {$Password}",
						"Username: {$Username}",
						"cache-control: no-cache"
					  ),
				)
			);

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				$retorno="dashlet_bizagi->. ERROR3: cURL Error #{$err}";
			}
			else {	
				$retorno = $this->xml2array($response, $get_attributes = 3, $priority = 'tag');
			}
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_bizagi->. ERROR2:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	/*
	Funcion para pasar XML a Array 
	Fuente https://stackoverflow.com/questions/6578832/how-to-convert-xml-into-array-in-php
	*/
	function xml2array($contents, $get_attributes = 1, $priority = 'tag')
	{
		if (!$contents) return array();
		if (!function_exists('xml_parser_create')) {
			// print "'xml_parser_create()' function not found!";
			return array();
		}
		// Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); // http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents) , $xml_values);
		xml_parser_free($parser);
		if (!$xml_values) return; //Hmm...
		// Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();
		$current = & $xml_array; //Refference
		// Go through the tags.
		$repeated_tag_index = array(); //Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
		unset($attributes, $value); //Remove existing values, or there will be trouble
		// This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data); //We could use the array by itself, but this cooler.
		$result = array();
		$attributes_data = array();
		if (isset($value)) {
		if ($priority == 'tag') $result = $value;
		else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
		}
		// Set the attributes too.
		if (isset($attributes) and $get_attributes) {
		foreach($attributes as $attr => $val) {                                   
		if ( $attr == 'ResStatus' ) {
		$current[$attr][] = $val;
		}
		if ($priority == 'tag') $attributes_data[$attr] = $val;
		else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
		}
		}
		// See tag status and do the needed.
		//echo"<br/> Type:".$type;
		if ($type == "open") { //The starting of the tag '<tag>'
		$parent[$level - 1] = & $current;
		if (!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
		$current[$tag] = $result;
		if ($attributes_data) $current[$tag . '_attr'] = $attributes_data;
		//print_r($current[$tag . '_attr']);
		$repeated_tag_index[$tag . '_' . $level] = 1;
		$current = & $current[$tag];
		}
		else { //There was another element with the same tag name
		if (isset($current[$tag][0])) { //If there is a 0th element it is already an array
		$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
		$repeated_tag_index[$tag . '_' . $level]++;
		}
		else { //This section will make the value an array if multiple tags with the same name appear together
		$current[$tag] = array(
		$current[$tag],
		$result
		); //This will combine the existing item and the new item together to make an array
		$repeated_tag_index[$tag . '_' . $level] = 2;
		if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
		$current[$tag]['0_attr'] = $current[$tag . '_attr'];
		unset($current[$tag . '_attr']);
		}
		}
		$last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
		$current = & $current[$tag][$last_item_index];
		}
		}
		elseif ($type == "complete") { //Tags that ends in 1 line '<tag />'
		// See if the key is already taken.
		if (!isset($current[$tag])) { //New Key
		$current[$tag] = $result;
		$repeated_tag_index[$tag . '_' . $level] = 1;
		if ($priority == 'tag' and $attributes_data) $current[$tag . '_attr'] = $attributes_data;
		}
		else { //If taken, put all things inside a list(array)
		if (isset($current[$tag][0]) and is_array($current[$tag])) { //If it is already an array...
		// ...push the new element into that array.
		$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
		if ($priority == 'tag' and $get_attributes and $attributes_data) {
		$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
		}
		$repeated_tag_index[$tag . '_' . $level]++;
		}
		else { //If it is not an array...
		$current[$tag] = array(
		$current[$tag],
		$result
		); //...Make it an array using using the existing value and the new value
		$repeated_tag_index[$tag . '_' . $level] = 1;
		if ($priority == 'tag' and $get_attributes) {
		if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
		$current[$tag]['0_attr'] = $current[$tag . '_attr'];
		unset($current[$tag . '_attr']);
		}
		if ($attributes_data) {
		$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
		}
		}
		$repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
		}
		}
		}
		elseif ($type == 'close') { //End of tag '</tag>'
		$current = & $parent[$level - 1];
		}
		}
		return ($xml_array);
	}
}
?>
