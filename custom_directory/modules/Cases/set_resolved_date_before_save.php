<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

use function PHPSTORM_META\type;

class set_resolved_date_before_save_class
{
    function set_resolved_date_before_save_method($bean, $event, $arguments)
    {
        // ignore para evitar loop despues de usar el método save()
        $ignore = $bean->ignore_set_case_name_from_cases;
        global $app_list_strings;

        if (!isset($ignore) || $ignore === false) 
        {
            $ignore = true;
            $bean->ignore_set_case_name_from_cases = $ignore;

            date_default_timezone_set('America/Bogota');

            $GLOBALS['log']->security("\n\n");
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Inicio set resolved date en módulo Cases -> " . date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");

            $tipo_solicitud = $bean->sasa_tipodesolicitud_c;
            $medio_recepcion = $bean->source;
            $checkprorroga  = $bean->sasa_checkprorroga_c;
            $ansQueja = ($medio_recepcion == 'Defensor del Consumidor') ? 8 : 15;
            $fecha_inicio = (!empty($bean->date_entered)) ? date('Y-m-d', strtotime($bean->date_entered)) : date('Y-m-d');

            if( !isset($bean->fetched_row['id']) )
            {
                $GLOBALS['log']->security("Registro nuevo");

                if( $tipo_solicitud == 'quejas')
                {
                    if (!empty($medio_recepcion)) 
                    {
                        $fecha_fin = $this->getDateResolved($fecha_inicio, $ansQueja, $checkprorroga, true, $tipo_solicitud);
                        $GLOBALS['log']->security("fecha_fin: ".$fecha_fin);
                        $bean->resolved_datetime =  $fecha_fin;
                    }
                }

            } else {

                $GLOBALS['log']->security("Registro existente");

                $checkprorroga_antes = $bean->fetched_row['sasa_checkprorroga_c'];
                $existente_sin_fecha = ( !empty($bean->resolved_datetime) ) ? false : true;

                // $fecha_vencimiento_antes = ( !empty($bean->resolved_datetime) ) ? date('Y-m-d', strtotime($bean->resolved_datetime)) : $fecha_inicio;

                $GLOBALS['log']->security("check antes:".$checkprorroga_antes);
                $GLOBALS['log']->security("check ahora:".$checkprorroga);
                $GLOBALS['log']->security("existente sin fe:".$existente_sin_fecha);
                $GLOBALS['log']->security("fecha inic :".$fecha_inicio);
                $GLOBALS['log']->security("fecha venc ant:".$bean->resolved_datetime);

                if( $tipo_solicitud == 'quejas')
                {
                    $medio_recepcion_antes = $bean->fetched_row['source'];   

                    $fecha_vencimiento_antes = ( !empty($bean->resolved_datetime) ) ? $this->calcularFechaVencimientoAntes($bean->resolved_datetime) : $fecha_inicio;

                    $GLOBALS['log']->security("fecha venc ant 2:".$fecha_vencimiento_antes);

                    if ($medio_recepcion_antes != $medio_recepcion || $checkprorroga_antes != $checkprorroga ) 
                    {
                        $cambio_check = ( $checkprorroga_antes != $checkprorroga ) ? true : false;
                        $cambio_medio = ( $medio_recepcion_antes != $medio_recepcion ) ? true : false;
                        $ansQuejaAntes = ( $bean->fetched_row['source'] == 'Defensor del Consumidor' ) ? 8 : 15;

                        $fecha_fin = $this->getDateResolved($fecha_vencimiento_antes, $ansQueja, $checkprorroga, null, $tipo_solicitud, $existente_sin_fecha, $cambio_medio, $ansQuejaAntes, $cambio_check);

                        $GLOBALS['log']->security("fecha_fin: ".$fecha_fin);

                        $bean->resolved_datetime =  $fecha_fin;

                        $GLOBALS['log']->security("fecha_fin 2: ".$bean->resolved_datetime);
                    }
                }

                if( $tipo_solicitud == 'peticion')
                {
                    if( $checkprorroga == 1 && ( $checkprorroga_antes != $checkprorroga ) )
                    {
                        $proceso = $bean->sasa_proceso_c;
                        $subproceso = $bean->sasa_subproceso_c;
                        $detalle = ( !empty($bean->sasa_detalle_c) ) ? $bean->sasa_detalle_c : '161';

                        $consultarAns = $this->consultaAnsPeticiones($tipo_solicitud, $proceso, $subproceso, $detalle);

                        while( $rowAns = $GLOBALS['db']->fetchByAssoc($consultarAns) )
                        {
                            $ansPeticion = $rowAns['sasa_ans_c']; // cantidad de días a sumar

                            $fecha_vencimiento_antes = ( !empty($bean->resolved_datetime) ) ? $this->calcularFechaVencimientoAntes($bean->resolved_datetime) : $fecha_inicio;

                            if ( !empty($ansPeticion) && !empty($fecha_vencimiento_antes) ) 
                            {
                                $fecha_fin = $this->getDateResolved($fecha_vencimiento_antes, $ansPeticion, $checkprorroga);

                                $GLOBALS['log']->security("fecha_fin: ".$fecha_fin);

                                $bean->resolved_datetime =  $fecha_fin;
                            }
                        }
                    }
                }
            }

            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Fin set resolved date en módulo Cases -> " . date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
        }
    }

    private function getDateResolved($fecha_inicio, $ans, $checkprorroga, $es_nuevo = false, $tipo_solicitud = 'omitir', $existente_sin_fecha_vencimiento = false, $cambio_medio_recepcion = false, $ansAntes = null, $cambio_check = false)
    {
        $GLOBALS['log']->security("fecha inicio:".$fecha_inicio);
        $GLOBALS['log']->security("ans:".$ans);
        $GLOBALS['log']->security("check prorro:".$checkprorroga);
        $GLOBALS['log']->security("es nuevo:".$es_nuevo);
        $GLOBALS['log']->security("tipo solicitud:".$tipo_solicitud);
        $GLOBALS['log']->security("sin fecha de venci:".$existente_sin_fecha_vencimiento);
        $GLOBALS['log']->security("cambio recep:".$cambio_medio_recepcion);
        $GLOBALS['log']->security("ans antes:".$ansAntes);
        $GLOBALS['log']->security("cambio check:".$cambio_check);

        $diff = $this->calcularDifDateResolved();

        if( $checkprorroga == 1 && $tipo_solicitud != 'quejas' )
        {
            $fecha_obj = new DateTime($fecha_inicio);
            $fecha_obj->modify("+$ans day");
            $fecha_obj->setTime(23, 59, 59);
            $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
            $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');
        }

        if( $tipo_solicitud == 'quejas' )
        {
            $fecha_obj = new DateTime($fecha_inicio);

            if( $checkprorroga == 1 && $cambio_medio_recepcion )
            {
                $GLOBALS['log']->security("check = 1, cambio medio recep");
                $ans2 = ( $ans > $ansAntes ) ? $ans - $ansAntes : $ansAntes - $ans;

                if( $cambio_check )
                {
                    $fecha_obj->modify("+$ans day");
                    $fecha_obj = ( $ans > $ansAntes ) ? $fecha_obj->modify("+$ans2 day") : $fecha_obj->modify("-$ans2 day");
                
                } else {

                    $fecha_obj = ( $ans > $ansAntes ) ? $fecha_obj->modify("+$ans2 day") : $fecha_obj->modify("-$ans2 day");                    
                }

                $fecha_obj->setTime(23, 59, 59);
                $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');
            }

            if( $checkprorroga == 1 && !$cambio_medio_recepcion && !$es_nuevo )
            {
                $GLOBALS['log']->security("check = 1, no cambio medio recep, no es nuevo");
                if( $cambio_check )
                {
                    $fecha_obj->modify("+$ans day");
                    $fecha_obj->setTime(23, 59, 59);
                    $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                    $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');
                } else {
                    $fecha_obj->setTime(23, 59, 59);
                    $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                    $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');  
                }
            }

            if( $checkprorroga != 1 && $cambio_medio_recepcion )
            {
                $GLOBALS['log']->security("check != 1, cambio medio recep");
                $ans2 = ( $ans > $ansAntes ) ? $ans - $ansAntes : $ansAntes - $ans;
                $fecha_obj = ( $ans > $ansAntes ) ? $fecha_obj->modify("+$ans2 day") : $fecha_obj->modify("-$ans2 day");
                $fecha_obj->setTime(23, 59, 59);
                $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');   
            }

            if( $checkprorroga != 1 && !$cambio_medio_recepcion && !$es_nuevo )
            {
                $GLOBALS['log']->security("check != 1, no cambio medio recep, no es nuevo");
                if( $cambio_check )
                {
                    $fecha_obj->setTime(23, 59, 59);
                    $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                    $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');  
                }
            }  
            
            if( $es_nuevo )
            {

                $GLOBALS['log']->security("Es nuevo");
                if( $checkprorroga == 1 )
                {
                    $ans2 = $ans * 2;
                    $fecha_obj->modify("+$ans2 day");
                    
                } else {

                    $fecha_obj->modify("+$ans day");
                }

                $fecha_obj->setTime(23, 59, 59);
                $fecha_obj->add(new DateInterval('PT'.$diff.'H'));
                $fecha_fin = $fecha_obj->format('Y-m-d H:i:s');                
            }
        }

        $GLOBALS['log']->security("Fecha fin: $fecha_fin");
        return $fecha_fin;
    }

    private function calcularDifDateResolved()
    {
        date_default_timezone_set('UTC'); // configurando por defecto zona horaria Central
        $dateCentral = date('Y-m-d H:i:s'); // facha y hora central

        $timeZoneBogota = 'America/Bogota';
        $dateColombia = new DateTime("now", new DateTimeZone($timeZoneBogota)); // fecha y hora de Colombia

        $dayCentral = intval(date('Ymd', strtotime($dateCentral))); // valor entero de añomesdia de fecha central
        $dayColombia = intval($dateColombia->format('Ymd')); // valor entero de añomesdia de fecha de colombia

        if ( $dayCentral > $dayColombia ) 
        {
            $GLOBALS['log']->security("Día de fecha central es mayor");
            $hourCentral = intval(date('h', strtotime($dateCentral))) + 12;
            $hourColombia = intval($dateColombia->format('h'));
            $dif = $hourCentral - $hourColombia;
            $GLOBALS['log']->security("Diferencia horaria: ".$dif);
            return $dif;
        }
        else if( $dayCentral == $dayColombia ) 
        {
            $GLOBALS['log']->security("Días iguales");
            $hourCentral = intval(date('H:i:s', strtotime($dateCentral)));
            $hourColombia = intval($dateColombia->format('H:i:s'));
            $dif = $hourCentral - $hourColombia;
            return $dif;
        } 
        else 
        {
            $GLOBALS['log']->security("Algo paso");
            $dif = 0;
            $GLOBALS['log']->security("Diferencia horaria: 0");
            return $dif;
        }
    }

    private function consultaAnsPeticiones($tipo, $proceso, $subproceso, $detalle)
    {
        $query = "SELECT tc.id_c, tc.sasa_ans_c
            FROM sasa_sasa_tipificaciones AS t
            INNER JOIN sasa_sasa_tipificaciones_cstm AS tc ON tc.id_c = t.id
            WHERE tc.sasa_motivo_c = '{$tipo}'
            AND tc.sasa_categoria_c = '{$proceso}'
            AND tc.sasa_proceso_c = '{$subproceso}'
            AND tc.sasa_detalle_c = '{$detalle}'
            AND t.deleted = 0
        LIMIT 1;";

        $resultQuery = $GLOBALS['db']->query($query);
        return $resultQuery;
    }

    // private function calcularFechaVencimientoAntes($fecha, $ans)
    private function calcularFechaVencimientoAntes($fecha)
    {
        $diff = $this->calcularDifDateResolved();
        $fecha_obj = new DateTime($fecha);
        $fecha_obj->sub(new DateInterval('PT'.$diff.'H'));
        $fecha_fin = $fecha_obj->format('Y-m-d');
        return $fecha_fin;
    }
}
