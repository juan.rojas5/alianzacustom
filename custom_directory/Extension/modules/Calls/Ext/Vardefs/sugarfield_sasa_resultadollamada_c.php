<?php
 // created: 2018-08-21 01:27:07
$dictionary['Call']['fields']['sasa_resultadollamada_c']['labelValue']='Resultado de la Llamada';
$dictionary['Call']['fields']['sasa_resultadollamada_c']['dependency']='';
$dictionary['Call']['fields']['sasa_resultadollamada_c']['visibility_grid']=array (
  'trigger' => 'status',
  'values' => 
  array (
    'Planned' => 
    array (
    ),
    'Held' => 
    array (
      0 => '',
      1 => 'Interesado',
      2 => 'Efectivo',
      3 => 'Negocio en Tramite',
      4 => 'Descartado',
      5 => 'Postergado',
    ),
    'Not Held' => 
    array (
    ),
  ),
);

 ?>