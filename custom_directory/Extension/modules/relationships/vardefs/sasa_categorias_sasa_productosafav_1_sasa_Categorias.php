<?php
// created: 2019-01-04 21:22:57
$dictionary["sasa_Categorias"]["fields"]["sasa_categorias_sasa_productosafav_1"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_CATEGORIAS_TITLE',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link-type' => 'many',
  'side' => 'left',
);
