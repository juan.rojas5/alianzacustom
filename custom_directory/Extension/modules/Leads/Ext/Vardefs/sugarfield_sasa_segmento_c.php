<?php
 // created: 2018-06-07 00:31:55
$dictionary['Lead']['fields']['sasa_segmento_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['sasa_segmento_c']['labelValue']='Segmento';
$dictionary['Lead']['fields']['sasa_segmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_segmento_c']['calculated']=true;
$dictionary['Lead']['fields']['sasa_segmento_c']['formula']='related($assigned_user_link,"sasa_segmento_c")';
$dictionary['Lead']['fields']['sasa_segmento_c']['enforced']='true';
$dictionary['Lead']['fields']['sasa_segmento_c']['dependency']='';

 ?>