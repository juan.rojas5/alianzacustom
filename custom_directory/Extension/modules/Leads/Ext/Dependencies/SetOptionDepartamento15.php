<?php
$dependencies['Leads']['SetOptionDepartamento15'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cordoba")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Ayapel","Buenavista","Canalete","Cerete","Chima","Chinu","Cienaga de Oro","Cotorra","La Apartada","Lorica","Los Cordobas","Momil","Montelibano","Monteria","Monitos","Planeta Rica","Pueblo Nuevo","Puerto Escondido","Puerto Libertador","Purisima","Sahagun","San Andres Sotavento","San Antero","San Bernardo del Viento","San Carlos","San Jose de Ure","San Pelayo","Tierralta","Tuchin","Valencia")',
           'labels' => 'createList("","Ayapel","Buenavista","Canalete","Cereté","Chimá","Chinú","Ciénaga de Oro","Cotorra","La Apartada","Lorica","Los Córdobas","Momil","Montelíbano","Montería","Moñitos","Planeta Rica","Pueblo Nuevo","Puerto Escondido","Puerto Libertador","Purísima","Sahagún","San Andrés Sotavento","San Antero","San Bernardo del Viento","San Carlos","San José de Uré","San Pelayo","Tierralta","Tuchín","Valencia")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
