<?php

$dependencies['Leads']['SetVisibilityLeads'] = array(
        'hooks' => array("all"),
        'triggerFields' => array('sasa_sector_c','sasa_tipopersona_c','account_name','sasa_pais_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_sector_c',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_sector_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'account_name',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'account_name',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_actividadeconomica_c',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_actividadeconomica_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'department',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'department',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'birthdate',
                    'value' => 'equal($sasa_tipopersona_c,"Natural")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'birthdate',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'title',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'title',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'refered_by',
                    'value' => 'equal($lead_source,"Word of mouth")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'refered_by',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_departamento_c',
                    'value' => 'or(equal($sasa_pais_c,"Colombia"),equal($sasa_pais_c,"No Registra"))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_departamento_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_departamentointer_c',
                    'value' => 'and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_departamentointer_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_municipiointer_c',
                    'value' => 'and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_municipiointer_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'sasa_municipio_c',
                    'value' => 'not($equal($sasa_departamento_c,""))',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'sasa_municipio_c',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
            array(
                'name' => 'SetVisibility',
                'params' => array(
                    'target' => 'website',
                    'value' => 'equal($sasa_tipopersona_c,"Juridica")',
                ),
            ),
            array(
                'name' => 'ReadOnly',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'website',
                    'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
                ),
            ),
        ),
    );