<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/accounts_mirror_primary_street.php

// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
	// position, file, function 
	/*Georeferencing Accounts*/
	$hook_array['before_save'][] = Array(97,'Mirror field georeferencing','custom/modules/Accounts/georeferencer_hook.php','Georeferencer_Accounts','before_save');


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/mover_actividades_asignado_ceutnas.php

$hook_array['before_save'][] = Array(
	//Processing index. For sorting the array.
	10,

	//Label. A string value to identify the hook.
	'mover_actividades_asignado_ceutnas',

	//The PHP file where your class is located.
	'custom/modules/Accounts/mover_actividades_asignado_ceutnas.php',

	//The class the method is in.
	'mover_actividades_asignado_ceutnas',

	//The method to call.
	'before_save'
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/BeforeSaveNumIdUnico.php


$hook_array['before_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'BeforeSaveNumIdUnico',

  //The PHP file where your class is located.
  'custom/modules/Leads/BeforeSaveNumIdUnico.php',

  //The class the method is in.
  'BeforeSaveNumIdUnico',

  //The method to call.
  'before_save'
);


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/denorm_field_hook.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// Relate Field Denormalization hook

$hook_array['before_save'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleBeforeUpdate',
];

$hook_array['after_save'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleAfterUpdate',
];

$hook_array['before_relationship_delete'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleDeleteRelationship',
];

$hook_array['after_relationship_add'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleAddRelationship',
];

?>
<?php
// Merged from modules/Accounts/Ext/LogicHooks/hint_hook.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$hook_array['after_delete'][] = [
    1,
    'Hook description',
    null,
    \Sugarcrm\Sugarcrm\modules\Accounts\HintAccountsHook::class,
    'afterDelete',
];

$hook_array['after_save'][] = [
    1,
    'Hook description',
    null,
    \Sugarcrm\Sugarcrm\modules\Accounts\HintAccountsHook::class,
    'afterSave',
];

$hook_array['after_relationship_add'][] = [
    1,
    'Hook description',
    null,
    \Sugarcrm\Sugarcrm\modules\Accounts\HintAccountsHook::class,
    'afterRelationshipAdd',
];

$hook_array['after_relationship_delete'][] = [
    1,
    'Hook description',
    null,
    \Sugarcrm\Sugarcrm\modules\Accounts\HintAccountsHook::class,
    'afterRelationshipDelete',
];

?>
