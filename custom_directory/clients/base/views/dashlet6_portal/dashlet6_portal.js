({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	url:'**PAI**',
	ws_auth:'**PAI**',
	authorization: "**PAI**",
	xmlns_wsdl: '**PAI**',
	xmlns_xsd:'**PAI**',
	wsdl:'**PAI**',
	initialize: function(options) {
		var self = this;		
		if (typeof app === 'undefined') app = App;			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() { 
			if ($('#dashlet6_consultar').length) {
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet6_id_cliente").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet6_id_cliente").css("border", "none"); 
						$("#dashlet6_id_cliente").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet6_id_cliente").attr('readonly', true);
					}
					$("#dashlet6_consultar").click( function(){
						self.dashlet_cadsign();	
					});		
					$("#dahslet6_encargos_list").change( function(){
						self.dahslet6_encargos_list_change();	
					});					
					$("#dashlet6_consultar").show();
					$("#dashlet6_consultar").click( function(){
						self.dashlet_cadsign();	
					});
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dahslet6_encargos_list_change: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		
		app.alert.show('dahslet6_encargos_list_change_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		$("#dashlet6_encargo").empty();//limiar lista primero
		$("#dashlet6_tabla").empty();//limiar lista primero
		

		var i=$("#dahslet6_encargos_list").val();
		var infoCuenta =self.respuesta.infoCuentas[i];
		if(infoCuenta.comentario == 'null') infoCuenta.comentario = '--';
		//infoCuenta.comentario.replace("\n", "<br>","g").replace(/\n/g, "<br>")
		$("#dashlet6_instrucciones").html(infoCuenta.comentario);

				

		if(!_.isEmpty(infoCuenta.infoFirmas)){
			if(!$.isArray(infoCuenta.infoFirmas)){//cuando solo retorna un elemnto, se obtiene un objeto en lugar de un array multinivel
				var temp = infoCuenta.infoFirmas;
				infoCuenta.infoFirmas = [];
				infoCuenta.infoFirmas.push(temp);				
			}
			$.each(infoCuenta.infoFirmas, function( i, infoFirmas ) {
				if(infoFirmas.comentariofirma == 'null') infoFirmas.comentariofirma = '--';
				$("#dashlet6_tabla").append('<tr><td>'+infoFirmas.nombrefirma+'</td><td>'+infoFirmas.tipoidenti+' '+infoFirmas.identi+'</td><td>'+infoFirmas.comentariofirma+'</td></tr>');
			});			
		}
		else{
			app.alert.show('dashlet_cadsign_warning', {
				level: 'warning',
				title: 'No hay infoFirmas' ,
				autoClose: false,
			});
		}

				
				
		app.alert.dismiss('dahslet6_encargos_list_change_procesando');
	},
	dashlet_cadsign: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet_cadsign_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		$("#dashlet6_instrucciones").empty();//limiar lista primero
		$("#dahslet6_encargos_list").empty();//limiar lista primero
		self.respuesta=null;//limiar lista primero
		
		self.dashlet_cadsign_call();		
		console.log("Response dashlet_cadsign_call", self.respuesta);
		
		if(self.respuesta.codigo == 'OK'){	
			if(!$.isArray(self.respuesta.infoCuentas)){//cuando solo retorna un elemnto, se obtiene un objeto en lugar de un array multinivel
				var temp = self.respuesta.infoCuentas;
				self.respuesta.infoCuentas = [];
				self.respuesta.infoCuentas.push(temp);				
			}		
			$.each(self.respuesta.infoCuentas, function( i, infoCuenta ) {
				$("#dahslet6_encargos_list").append('<option value="'+i+'">'+infoCuenta.encargo+'</option>');
			});
			self.dahslet6_encargos_list_change();//para que poble la tabla por primera vez
		}
		else{
			app.alert.show('dashlet_cadsign_error'+self.respuesta.codigo, {
				level: 'error',
				title: self.respuesta.mensaje,
				autoClose: false,
			});
		}	
	},
	dashlet_cadsign_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		console.log('Consultando cadsign...');
		var ID_Cliente = $("#dashlet6_id_cliente").val();		
		var method  = "dashlet_cadsign";		
		var data = [
			{
				'url':self.url,
				'ws_auth':self.ws_auth,
				'authorization':self.authorization,
			},
			{
				'xmlns_wsdl': self.xmlns_wsdl,
				'xmlns_xsd':self.xmlns_xsd,
				'wsdl':self.wsdl,
				'Encargo': '',
				'ID_Cliente': ID_Cliente,
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_cadsign_procesando');
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_cadsign_procesando');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + e ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	}
})
