<?php
// created: 2023-02-03 09:36:51
$viewdefs['Users']['DetailView'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'form' => 
    array (
      'headerTpl' => 'modules/Users/tpls/DetailViewHeader.tpl',
      'footerTpl' => 'modules/Users/tpls/DetailViewFooter.tpl',
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'LBL_USER_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_EMPLOYEE_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
  ),
  'panels' => 
  array (
    'LBL_USER_INFORMATION' => 
    array (
      0 => 
      array (
        0 => 'full_name',
        1 => 'user_name',
      ),
      1 => 
      array (
        0 => 'status',
        1 => 
        array (
          'name' => 'UserType',
          'customCode' => '{$USER_TYPE_READONLY}',
        ),
      ),
      2 => 
      array (
        0 => 'picture',
        1 => 
        array (
          'name' => 'sasa_envacaciones_c',
          'label' => 'LBL_SASA_ENVACACIONES_C',
        ),
      ),
      3 => 
      array (
        0 => '',
        1 => 
        array (
          'name' => 'sasa_fechainicio_c',
          'label' => 'LBL_SASA_FECHAINICIO_C',
        ),
      ),
      4 => 
      array (
        0 => 
        array (
          'name' => 'sasa_usuarioreemplazante_c',
          'studio' => 'visible',
          'label' => 'LBL_SASA_USUARIOREEMPLAZANTE_C',
        ),
        1 => 
        array (
          'name' => 'sasa_fechafin_c',
          'label' => 'LBL_SASA_FECHAFIN_C',
        ),
      ),
    ),
    'LBL_EMPLOYEE_INFORMATION' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'sasa_compania_c',
          'label' => 'LBL_SASA_COMPANIA_C',
        ),
        1 => '',
      ),
      1 => 
      array (
        0 => 'employee_status',
        1 => 'show_on_employees',
      ),
      2 => 
      array (
        0 => 'title',
        1 => 'phone_work',
      ),
      3 => 
      array (
        0 => 'department',
        1 => 'phone_mobile',
      ),
      4 => 
      array (
        0 => 'reports_to_name',
        1 => 'phone_other',
      ),
      5 => 
      array (
        0 => 
        array (
          'name' => 'sasa_regional_c',
          'label' => 'LBL_SASA_REGIONAL_C',
        ),
        1 => 'phone_fax',
      ),
      6 => 
      array (
        0 => 
        array (
          'name' => 'sasa_segmento_c',
          'label' => 'LBL_SASA_SEGMENTO_C',
        ),
        1 => 'phone_home',
      ),
      7 => 
      array (
        0 => 
        array (
          'name' => 'sasa_superior1_c',
          'label' => 'LBL_SASA_SUPERIOR1_C',
        ),
        1 => 
        array (
          'name' => 'sasa_superior2_c',
          'label' => 'LBL_SASA_SUPERIOR2_C',
        ),
      ),
      8 => 
      array (
        0 => 'messenger_type',
        1 => 'messenger_id',
      ),
      9 => 
      array (
        0 => 'address_street',
        1 => 'address_city',
      ),
      10 => 
      array (
        0 => 'address_state',
        1 => 'address_postalcode',
      ),
      11 => 
      array (
        0 => 'address_country',
      ),
      12 => 
      array (
        0 => 'description',
      ),
    ),
  ),
);