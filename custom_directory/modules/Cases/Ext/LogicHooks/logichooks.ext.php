<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from modules/Cases/Ext/LogicHooks/RelationshipHook.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$hook_array['before_relationship_delete'][] = [
    1,
    'beforeRelationshipDelete',
    'modules/Cases/CasesHooks.php',
    'CasesHooks',
    'beforeRelationshipDelete',
];

$hook_array['after_relationship_add'][] = [
    1,
    'afterRelationshipAdd',
    'modules/Cases/CasesHooks.php',
    'CasesHooks',
    'afterRelationshipAdd',
];

$hook_array['after_relationship_delete'][] = [
    1,
    'afterRelationshipDelete',
    'modules/Cases/CasesHooks.php',
    'CasesHooks',
    'afterRelationshipDelete',
];

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/denorm_field_hook.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// Relate Field Denormalization hook

$hook_array['before_save'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleBeforeUpdate',
];

$hook_array['after_save'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleAfterUpdate',
];

$hook_array['before_relationship_delete'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleDeleteRelationship',
];

$hook_array['after_relationship_add'][] = [
    1,
    'denorm_field_watcher',
    null,
    '\\Sugarcrm\\Sugarcrm\\Denormalization\\Relate\\Hook',
    'handleAddRelationship',
];

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/SfcComplaints.php


    $hook_array['before_save'][] = array(
        1,
        'Hook description',
        'custom/modules/Cases/logicHooks/SfcComplaints.php',
        'SfcComplaints',
        'sfcComplaintsBeforeSave'
    );

?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/before_save_relate_typing_to_cases.php


$hook_array['before_save'][] = Array(
    3, 
    'relate typing to cases', 
    'custom/modules/Cases/relate_typing_to_cases.php', 
    'relate_typing_to_cases_class', 
    'relate_typing_to_cases_method'
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/before_save_CalcularFechaVencimientoCasosM1.php


$hook_array['before_save'][] = Array(
    4, 
    'Calculate expiration date to cases in moment 1', 
    'custom/modules/Cases/CalcularFechaVencimientoCasosM1.php', 
    'CalcularFechaVencimientoCasosM1_class', 
    'CalcularFechaVencimientoCasosM1_method'
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/set_case_name_after_save.php


$hook_array['after_save'][] = Array(
    3, 
    'Set case name after save', 
    'custom/modules/Cases/set_case_name_after_save.php', 
    'set_case_name_after_save_class', 
    'set_case_name_after_save_method'
);
?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/AlianzaCasesAfterSave.php


$hook_array['after_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'AlianzaCasesAfterSave',

  //The PHP file where your class is located.
  'custom/modules/Cases/AlianzaCasesAfterSave.php',

  //The class the method is in.
  'after_save',

  //The method to call.
  'after_save'
);


?>
<?php
// Merged from custom/Extension/modules/Cases/Ext/LogicHooks/set_resolved_date_before_save.php


$hook_array['before_save'][] = Array(
    7, 
    'Set resolved date before save', 
    'custom/modules/Cases/set_resolved_date_before_save.php', 
    'set_resolved_date_before_save_class', 
    'set_resolved_date_before_save_method'
);
?>
