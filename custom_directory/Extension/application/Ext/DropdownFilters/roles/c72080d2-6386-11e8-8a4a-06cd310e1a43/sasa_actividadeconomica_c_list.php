<?php
// created: 2018-08-24 15:09:12
$role_dropdown_filters['sasa_actividadeconomica_c_list'] = array (
  '' => true,
  'Agroindustria' => true,
  'Comercial' => true,
  'Comunicacion' => true,
  'Construccion' => true,
  'Educacion' => true,
  'Financiero' => true,
  'Industrial' => true,
  'Salud' => true,
  'Servicios' => true,
  'Transporte' => true,
  'Otros' => true,
  'No Registra' => false,
);