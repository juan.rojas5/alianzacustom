<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

use function PHPSTORM_META\type;

class set_case_name_after_save_class
{
    function set_case_name_after_save_method($bean, $event, $arguments)
    {
        // ignore para evitar loop despues de usar el método save()
        $ignore = $bean->ignore_set_case_name_from_cases;
        global $app_list_strings;

        if (!isset($ignore) || $ignore === false){

            $ignore = true;
            $bean->ignore_set_case_name_from_cases = $ignore;

            $GLOBALS['log']->security("\n\n");
            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Inicio acción after save set name en módulo Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");
            
            if ( !isset($bean->fetched_row['id']) ) 
            {
                $GLOBALS['log']->security("Registro nuevo");

                $tipo_solicitud = $bean->sasa_tipodesolicitud_c;
                $case_number = $bean->case_number;
                $name = '';

                if( $tipo_solicitud == 'peticion' )
                {
                    $proceso = $bean->sasa_proceso_c;
                    $subproceso = $bean->sasa_subproceso_c;
                    $detalle = $bean->sasa_detalle_c;
                    $tipificacion = '';

                    if( !empty($detalle) )
                    {
                        $tipificacion = $this->buscarTipificacionConDetalle($tipo_solicitud, $proceso, $subproceso, $detalle);
                    } else {
                        $detalle = '161';
                        $tipificacion = $this->buscarTipificacionConDetalle($tipo_solicitud, $proceso, $subproceso, $detalle);
                    }

                    if( $tipificacion != '' && $tipificacion != null ){

                        $name = "$case_number - $tipificacion";

                    } else if( empty($tipificacion) ){
                        
                        $name = "$case_number";
                    }

                    $this->actualizarNameCase($bean->id, $name);

                    $GLOBALS['log']->security("Case number: ".$case_number);
                    $GLOBALS['log']->security("Tipificación 2: ".$tipificacion);
                    $GLOBALS['log']->security("Name: ".$name);
                }

                if( $tipo_solicitud != 'quejas' && $tipo_solicitud != 'peticion' )
                {
                    $name = "$case_number - $tipo_solicitud";
 
                    $this->actualizarNameCase($bean->id, $name);

                    $GLOBALS['log']->security("Case number: ".$case_number);
                    $GLOBALS['log']->security("Tipo solicitud: ".$tipo_solicitud);
                    $GLOBALS['log']->security("Name: ".$name);
                }

            }

            $GLOBALS['log']->security("========================================================================");
            $GLOBALS['log']->security("Termino acción after save set name en módulo Cases -> ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("========================================================================");

        }
    }

    function buscarTipificacion($id)
    {
        $query = "SELECT id, sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida, sasa_sasa_tipificaciones_cases_1cases_idb
            FROM sasa_sasa_tipificaciones_cases_1_c
            WHERE sasa_sasa_tipificaciones_cases_1cases_idb = '{$id}'
            AND deleted = 0
        LIMIT 1";

        $resultQuery = $GLOBALS['db']->query($query);

        while( $row = $GLOBALS['db']->fetchByAssoc($resultQuery) )
        {
            return $row['id'];
        }
    }

    function buscarTipificacionConDetalle($tipo, $proceso, $subproceso, $detalle)
    {
        $queryTipificacion = "SELECT tc.id_c, t.name, tc.sasa_motivo_c, tc.sasa_categoria_c, 
            tc.sasa_proceso_c, tc.sasa_detalle_c
            FROM sasa_sasa_tipificaciones AS t
            INNER JOIN sasa_sasa_tipificaciones_cstm AS tc ON tc.id_c = t.id
            WHERE tc.sasa_motivo_c = '{$tipo}'
            AND tc.sasa_categoria_c = '{$proceso}'
            AND tc.sasa_proceso_c = '{$subproceso}'
            AND tc.sasa_detalle_c = '{$detalle}'
            AND t.deleted = 0
        LIMIT 1";

        $tipificacionResult = '';
        $resultTipificacion = $GLOBALS['db']->query($queryTipificacion);

        while( $row = $GLOBALS['db']->fetchByAssoc($resultTipificacion) )
        {
            if( !empty($row['id_c']) )
            {
                $tipificacionResult = $row['name'];
            }
        }

        return $tipificacionResult;
    }

    function actualizarNameCase($idCase, $name)
    {
        $queryCase = "UPDATE cases SET name = '$name' 
        WHERE id = '{$idCase}'";

        $resultQueryCase = $GLOBALS['db']->query($queryCase);
    }
}