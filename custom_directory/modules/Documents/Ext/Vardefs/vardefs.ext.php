<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-06-22 19:54:24
$dictionary['Document']['fields']['date_modified']['audited']=false;
$dictionary['Document']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Document']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Document']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Document']['fields']['date_modified']['calculated']=false;
$dictionary['Document']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/rli_link_workflow.php

$dictionary['Document']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Document']['full_text_search']=false;

?>
