<?php
 // created: 2018-07-06 21:53:58
$dictionary['Meeting']['fields']['date_entered']['audited']=true;
$dictionary['Meeting']['fields']['date_entered']['comments']='Date record created';
$dictionary['Meeting']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_entered']['calculated']=false;
$dictionary['Meeting']['fields']['date_entered']['enable_range_search']='1';

 ?>