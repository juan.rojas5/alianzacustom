<?php
 // created: 2023-02-20 21:38:36
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa1_unidades_de_negocio_por__1'] = array (
  'order' => 100,
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'get_subpanel_data' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
