<?php
 // created: 2018-08-08 17:49:07

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Qualification',
  'Needs Analysis' => 'Needs Analysis',
  'Value Proposition' => 'Value Proposition',
  'Id. Decision Makers' => 'Id. Decision Makers',
  'Perception Analysis' => 'Perception Analysis',
  'Proposal/Price Quote' => 'Proposal/Price Quote',
  'Negotiation/Review' => 'Negotiation/Review',
  'Closed Won' => 'Closed Won',
  'Closed Lost' => 'Closed Lost',
);