<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Language/en_us.afuserscustomfieldsv001-201805281106.php
 

    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';


?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Language/en_us.afuserscustomfieldsv001-201805310904.php
 

    $mod_strings['LBL_SASA_SEGMENTO_C'] = 'Segmento';
    $mod_strings['LBL_SASA_REGIONAL_C'] = 'Regional';


?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Language/en_us.cci.lang.php

include('custom/include/en_us.cci.lang.php');

// $mod_strings['LBL_CCSYNCED'] = 'Sync with Constant Contact';
// $mod_strings['LBL_CCID'] = 'Constant Contact ID';
// $mod_strings['LBL_CCILOG_SUBPANEL'] = 'Constant Contact Audit Log';
// $mod_strings['LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE'] = 'Constant Contact Target Lists';
// $mod_strings['LBL_PROSPECTLISTS_TARGET_LIST_TITLE'] = 'Target Lists';

?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Language/en_us.afuserscustomfieldsv003-202108060607.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_COMPANIA_C'] = 'Compañía';
$mod_strings['LBL_SASA_SUPERIOR1_C'] = 'Superior 1';
$mod_strings['LBL_SASA_SUPERIOR2_C'] = 'Superior 2';
$mod_strings['LBL_ADDRESS_POSTALCODE'] = 'Código Postal de dirección';
$mod_strings['LBL_HOME_PHONE'] = 'Tel. Casa:';
$mod_strings['LBL_SASA_NOMBRECOMPLETO_C'] = 'Nombre completo';
$mod_strings['EN VACACIONES'] = 'En vacaciones';
$mod_strings['LBL_SASA_FECHAINICIO_C'] = 'Fecha inicio';
$mod_strings['LBL_SASA_FECHAFIN_C'] = 'Fecha fin';
$mod_strings['LBL_SASA_ENVACACIONES_C'] = 'En vacaciones';
$mod_strings['LBL_SASA_USUARIOREEMPLAZANTE_C_USER_ID'] = 'Usuario reemplazante (relacionado Usuario ID)';
$mod_strings['LBL_SASA_USUARIOREEMPLAZANTE_C'] = 'Usuario reemplazante';
$mod_strings['LBL_SASA_CONTROLVACACIONES_C'] = 'Control vacaciones';

?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Language/en_us.customusers_sasa1_unidades_de_negocio_por__1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE'] = 'Unidades de negocio por cliente';
$mod_strings['LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_USERS_TITLE'] = 'Unidades de negocio por cliente';

?>
