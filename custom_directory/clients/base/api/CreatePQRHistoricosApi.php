<?php

use function PHPSTORM_META\type;

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class CreatePQRHistoricosApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'CreateSale' => array(
                'reqType' => array('POST'), //request type

                'noLoginRequired' => true, //set authentication

                'path' => array('create-pqr'), //endpoint path

                'pathVars' => array(''), //endpoint variables
                
                'method' => 'CreatePQRMethod',//method to call

                'shortHelp' => 'An api to create sales', //short help string to be displayed in the help documentation

                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_CreateSale_help.html', //long help to be displayed in the help documentation
            ),
            'UpdateSale' => array(
                'reqType' => array('POST'), //request type

                'noLoginRequired' => true, //set authentication

                'path' => array('update-pqr'), //endpoint path

                'pathVars' => array(''), //endpoint variables
                
                'method' => 'UpdateSaleMethod',//method to call

                'shortHelp' => 'An api to create sales', //short help string to be displayed in the help documentation

                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_CreateSale_help.html', //long help to be displayed in the help documentation
            ),
            'UpdateSaleTwo' => array(
                'reqType' => array('POST'), //request type

                'noLoginRequired' => true, //set authentication

                'path' => array('update-pqr-two'), //endpoint path

                'pathVars' => array(''), //endpoint variables
                
                'method' => 'UpdateSaleMethodTwo',//method to call

                'shortHelp' => 'An api to create sales', //short help string to be displayed in the help documentation

                'longHelp' => 'custom/clients/base/api/help/MyEndPoint_CreateSale_help.html', //long help to be displayed in the help documentation
            ),
        );
    }
    
    // ========== Inicio de Gestión de PQRs historicos ========== 
        public function CreatePQRMethod($api, $args)
        {
            $GLOBALS['log']->security("==============================================================================");
            $GLOBALS['log']->security("Comienzo para crear registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            
            foreach ($args['data'] as $data) {

                $cuenta = $this->obtenerCuenta($data['Numero_Documento_Cliente']); 

                if ( !empty($cuenta) ) 
                {
                    $this->crearPQR($data, $cuenta);
                    
                }else{
                    $this->crearPQR($data, null);
                }
            }

            $GLOBALS['log']->security("Finalizo creación de registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("==============================================================================");

            return "Finalizo inserciones de PQRs historico... ".date('Y-m-d H:i:s');
        }

        public function obtenerCuenta($identificacion)
        {
            $cuenta = BeanFactory::newBean("Accounts");
            $cuenta->retrieve_by_string_fields(
                array(
                    'sasa_nroidentificacion_c' => $identificacion,
                )
            );
            if ( !empty($cuenta->id) ) 
            {
                // $GLOBALS['log']->security("Se encontro cuenta...");
                return $cuenta->id;
            }else{
                // $GLOBALS['log']->security("No se encontro cuenta.");
            }
        }

        public function validateToNull($value)
        {
            if( $value == '' || $value == 'NULL' || $value == 'Null' || $value == 'null' || $value == null )
            {
                return null;
            }else {
                return $value;
            }
        }

        public function validateDate($date = null)
        {
            if( !empty($date) )
            {
                $date1 = date('m/d/Y', strtotime($date));
                $date2 = date('Y-m-d', strtotime($date1));
                return $date2;
            }else {
                return $date;
            }
        }

        public function crearPQR($data, $cuenta = null)
        {
            try {
                
                if ( !empty($this->validateToNull($data['Case_Number'])) || !empty($this->validateToNull($data['Tipificacion_2'])) ) 
                {
                    $pqr = BeanFactory::newBean("sasa_pqrs_historico");

                    if(  !empty($this->validateToNull($data['Case_Number'])) &&  !empty($this->validateToNull($data['Tipificacion_2'])) )
                    {
                        $pqr->name = $data['Case_Number']." - ".$data['Tipificacion_2'];
                    }
                    
                    if(  !empty($this->validateToNull($data['Case_Number'])) && empty($this->validateToNull($data['Tipificacion_2'])) )
                    {
                        $pqr->name = $data['Case_Number'];
                    }

                    if( empty($this->validateToNull($data['Case_Number'])) &&  !empty($this->validateToNull($data['Tipificacion_2'])) )
                    {
                        $pqr->name = $data['Tipificacion_2'];
                    }
                    
                    $pqr->sasa_case_number_c = $this->validateToNull($data['Case_Number']);
                    $pqr->sasa_fecha_de_vencimiento_c = $this->validateToNull($this->validateDate($data['Fecha_de_Vencimiento']));
                    $pqr->sasa_tipificacion_1_c = $this->validateToNull($data['Tipificacion_1']);
                    $pqr->sasa_tipificacion_1_codigo_c = $this->validateToNull($data['Tipificacion_1_codigo']);
                    $pqr->sasa_tipificacion_2_c = $this->validateToNull($data['Tipificacion_2']);
                    $pqr->sasa_tipificacion_2_codigo_c = $this->validateToNull($data['Tipificacion_2_codigo']);
                    $pqr->sasa_tipificacion_3_c = $this->validateToNull($data['Tipificacion_3']);
                    $pqr->sasa_tipificacion_3_codigo_c = $this->validateToNull($data['Tipificacion_3_codigo']);
                    $pqr->sasa_fecha_creacion_c = $this->validateToNull($this->validateDate($data['Fecha_creacion']));
                    $pqr->sasa_hora_c = $this->validateToNull($data['Hora']);
                    $pqr->sasa_creado_por_nombre_compl_c = $this->validateToNull($data['Creado_por_Nombre_Completo']);
                    $pqr->sasa_nombres_cliente_c = $this->validateToNull($data['Nombres_Cliente']);
                    $pqr->sasa_apellidos_cliente_c = $this->validateToNull($data['Apellidos_Cliente']);
                    $pqr->sasa_numero_documento_client_c = $this->validateToNull($data['Numero_Documento_Cliente']);
                    $pqr->sasa_medio_de_recepcion_c = $this->validateToNull($data['Medio_de_Recepcion']);
                    $pqr->sasa_segmento_datawarehouse__c = $this->validateToNull($data['Segmento_DataWareHouse']);
                    $pqr->sasa_tipo_de_solicitud_c = $this->validateToNull($data['Tipo_de_Solicitud']);
                    $pqr->sasa_tipo_de_solicitud_codig_c = $this->validateToNull($data['Tipo_de_Solicitud_codigo']);
                    $pqr->sasa_categoria_c = $this->validateToNull($data['Categoria']);
                    $pqr->sasa_categoria_codigo_c = $this->validateToNull($data['Categoria_codigo']);
                    $pqr->sasa_estado_de_caso_c = $this->validateToNull($data['Estado_de_caso']);
                    $pqr->sasa_estado_de_caso_codigo_c = $this->validateToNull($data['Estado_de_caso_codigo']);
                    $pqr->sasa_encargo_c = $this->validateToNull($data['Encargo']);
                    $pqr->sasa_respuesta_c = $this->validateToNull($data['Respuesta']);
                    $pqr->sasa_solucionador_inmediato__c = $this->validateToNull($data['Solucionador_Inmediato']);
                    $pqr->sasa_documentado_c = $this->validateToNull($data['Documentado']);
                    $pqr->sasa_escalado_c = $this->validateToNull($data['Escalado']);
                    $pqr->sasa_vencido_c = $this->validateToNull($data['Vencido']);
                    // $pqr->sasa_descripcion_de_la_solic_c = $this->validateToNull($data['Descripcion_de_la_Solicitud']);
                    $pqr->description = $this->validateToNull($data['Descripcion_de_la_Solicitud']);
                    $pqr->sasa_solucionador_c = $this->validateToNull($data['Solucionador']);
                    $pqr->sasa_fecha_solucion_c = $this->validateToNull($this->validateDate($data['Fecha_solucion']));
                    $pqr->sasa_usuario_asistente_encar_c = $this->validateToNull($data['Usuario_Asistente_Encargo']);
                    $pqr->sasa_numerounico_c = $this->validateToNull($data['id_register']);

                    if( !empty($cuenta) )
                    {
                        $pqr->accounts_sasa_pqrs_historico_1accounts_ida = $cuenta;
                    }

                    $pqr->save();

                    $GLOBALS['log']->security("Registro PQR Historíco creado -> ".$pqr->id);

                    if( $pqr->load_relationship('commentlog_link') )
                    {
                        // $GLOBALS['log']->security("Se va a agregar comentario...");
                        $comment = BeanFactory::newBean('CommentLog');
                        $comment->entry = print_r($this->validateToNull($data['Respuesta']),true);
                        $comment->set_created_by = false;
                        $comment->update_modified_by = false;
                        $comment->created_by = '1';
                        $comment->modified_user_id = '1';
                        $comment->name = 'Respuesta';
                        $comment->description = "Respuesta a pqr";
                        $comment->save();

                        $pqr->commentlog_link->add($comment->id);
                        $pqr->save();
                    }
                }

            } catch (\Throwable $th) {
                //throw $th;
                $GLOBALS['log']->security("Error al crear venta: ".date('Y-m-d H:i:s')." ".$th);

            }
        }
    // ========== Fin de Gestión de PQRs historicos =============


    // ========== Inicio de Gestión de PQRs historicos ========== 
        public function UpdateSaleMethod($api, $args)
        {
            $GLOBALS['log']->security("==================================================================================");
            $GLOBALS['log']->security("Comienzo actualización registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            
            foreach ($args['data'] as $data) 
            {
                $this->UpdateHistorico($data);
            }

            $GLOBALS['log']->security("Finalizo actualización registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("==================================================================================");

            return "Finalizo inserciones de PQRs historico... ".date('Y-m-d H:i:s');
        }

        public function UpdateHistorico($case)
        {
            $GLOBALS['log']->security("Caso -> ".$case['Case_Number']);

            $updateCase = BeanFactory::newBean("sasa_pqrs_historico");
            $updateCase->retrieve_by_string_fields(
                array(
                    'sasa_case_number_c' => $case["Case_Number"],
                    'sasa_tipificacion_2_c' => $case['Tipificacion_2']
                )
            );

            if( !empty($updateCase->id) )
            {
                $updateCase->description = $case['Descripcion_de_la_Solicitud'];
                $updateCase->sasa_numero_documento_client_c = $case['Numero_Documento_Cliente'];
                $updateCase->save();
                $GLOBALS['log']->security("PQR Historico actualizado: ".$updateCase->sasa_case_number_c." - ".$updateCase->sasa_tipificacion_2_c." --> ".$updateCase->id);
            }
        }


    // ========== Fin de Gestión de PQRs historicos ============= 

    // ========== Inicio de Gestión de PQRs historicos ========== 
        public function UpdateSaleMethodTwo($api, $args)
        {
            $GLOBALS['log']->security("====================================================================================");
            $GLOBALS['log']->security("Comienzo actualización 2 registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            
            foreach ($args['data'] as $data) 
            {
                $this->UpdateHistoricoTwo($data);
            }

            $GLOBALS['log']->security("Finalizo actualización 2 registros en PQR historicos de Alianza ".date('Y-m-d H:i:s'));
            $GLOBALS['log']->security("====================================================================================");

            return "Finalizo inserciones de PQRs historico... ".date('Y-m-d H:i:s');
        }

        public function UpdateHistoricoTwo($case)
        {
            $GLOBALS['log']->security("Caso -> ".$case['Case_Number']);

            $updateCase = BeanFactory::newBean("sasa_pqrs_historico");
            $updateCase->retrieve_by_string_fields(
                array(
                    'sasa_case_number_c' => $case["Case_Number"],
                    'sasa_fecha_creacion_c' => $case['Fecha_creacion']
                )
            );

            if( !empty($updateCase->id) )
            {
                $updateCase->description = $case['Descripcion_de_la_Solicitud'];
                $updateCase->sasa_numero_documento_client_c = $case['Numero_Documento_Cliente'];
                $updateCase->save();
                $GLOBALS['log']->security("PQR Historico actualizado: ".$updateCase->sasa_case_number_c." - ".$updateCase->sasa_tipificacion_2_c." --> ".$updateCase->id);
            }
        }


    // ========== Fin de Gestión de PQRs historicos ============= 

}

?>