<?php
 // created: 2022-09-28 14:17:23
$dictionary['Case']['fields']['sasa_radalianzam2_c']['labelValue']='Rad Alianza M2';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radalianzam2_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['dependency']='equal($sasa_tipodesolicitud_c,"quejas")';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radalianzam2_c']['readonly_formula']='';

 ?>