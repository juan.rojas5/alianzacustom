<?php
// created: 2019-01-04 21:52:59
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_productosafav_sasa_saldosaf_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1SASA_PRODUCTOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'sasa_nombrefondo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NOMBREFONDO_C',
    'width' => 10,
  ),
  'sasa_nombresubcuenta_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_NOMBRESUBCUENTA_C',
    'width' => 10,
  ),
  'sasa_encargo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_ENCARGO_C',
    'width' => 10,
  ),
  'sasa_inversiones_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_INVERSIONES_C',
    'width' => 10,
  ),
  'sasa_digito_c' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_SASA_DIGITO_C',
    'width' => 10,
  ),
  'sasa_plan_c' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_SASA_PLAN_C',
    'width' => 10,
  ),
  'sasa_numerounid_c' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_SASA_NUMEROUNID_C',
    'width' => 10,
  ),
  'sasa_valorunid_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_VALORUNID_C',
    'width' => 10,
  ),
  'sasa_saldoactual_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_SALDOACTUAL_C',
    'width' => 10,
  ),
  'sasa_fechasaldo_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHASALDO_C',
    'width' => 10,
    'default' => true,
  ),
  'sasa_ingresosdia_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_INGRESOSDIA_C',
    'width' => 10,
  ),
  'sasa_egresosdia_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_EGRESOSDIA_C',
    'width' => 10,
  ),
  'sasa_retcont_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_RETCONT_C',
    'width' => 10,
  ),
  'sasa_retrend_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_RETREND_C',
    'width' => 10,
  ),
  'sasa_canje_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_CANJE_C',
    'width' => 10,
  ),
  'sasa_ctrlcancel_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CTRLCANCEL_C',
    'width' => 10,
  ),
  'sasa_reservado_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_RESERVADO_C',
    'width' => 10,
  ),
  'sasa_4x1000_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_4X1000_C',
    'width' => 10,
  ),
  'sasa_sinconsol_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_SINCONSOL_C',
    'width' => 10,
  ),
  'sasa_maximoretiro_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_MAXIMORETIRO_C',
    'width' => 10,
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'readonly' => true,
    'vname' => 'LBL_MODIFIED',
    'id' => 'MODIFIED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'modified_user_id',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);