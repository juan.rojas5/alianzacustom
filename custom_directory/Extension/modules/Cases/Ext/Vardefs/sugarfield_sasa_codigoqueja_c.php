<?php
 // created: 2022-05-24 19:46:34
$dictionary['Case']['fields']['sasa_codigoqueja_c']['labelValue']='Código de la queja o reclamo';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_codigoqueja_c']['enforced']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['dependency']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_codigoqueja_c']['readonly_formula']='';

 ?>