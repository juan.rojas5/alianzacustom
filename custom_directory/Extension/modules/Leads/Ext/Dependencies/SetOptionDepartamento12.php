<?php
$dependencies['Leads']['SetOptionDepartamento12'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Cauca")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Florencia","Paez","Morales","Santa Rosa","Almaguer","Argelia","Balboa","Bolivar","Buenos Aires","Cajibio","Caldono","Caloto","Corinto","El Tambo","Guachene","Guapi","Inza","Jambalo","La Sierra","La Vega","Lopez","Mercaderes","Miranda","Padilla","Patia","Piamonte","Piendamo","Popayan","Puerto Tejada","Purace","Rosas","San Sebastian","Santander de Quilichao","Silvia","Sotara","Suarez","Sucre","Timbio","Timbiqui","Toribio","Totoro","Villa Rica")',
           'labels' => 'createList("","Florencia","Páez","Morales","Santa Rosa","Almaguer","Argelia","Balboa","Bolívar","Buenos Aires","Cajibío","Caldono","Caloto","Corinto","El Tambo","Guachené","Guapi","Inzá","Jambaló","La Sierra","La Vega","López","Mercaderes","Miranda","Padilla","Patía","Piamonte","Piendamó","Popayán","Puerto Tejada","Puracé","Rosas","San Sebastián","Santander de Quilichao","Silvia","Sotara","Suárez","Sucre","Timbío","Timbiquí","Toribio","Totoró","Villa Rica")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
