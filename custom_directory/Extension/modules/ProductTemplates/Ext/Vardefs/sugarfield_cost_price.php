<?php
 // created: 2018-07-07 16:03:43
$dictionary['ProductTemplate']['fields']['cost_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['cost_price']['len']=26;
$dictionary['ProductTemplate']['fields']['cost_price']['audited']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['comments']='Product cost ("Cost" in Quote)';
$dictionary['ProductTemplate']['fields']['cost_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['cost_price']['duplicate_merge']='enabled';
$dictionary['ProductTemplate']['fields']['cost_price']['duplicate_merge_dom_value']='1';
$dictionary['ProductTemplate']['fields']['cost_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['cost_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['enable_range_search']=false;

 ?>