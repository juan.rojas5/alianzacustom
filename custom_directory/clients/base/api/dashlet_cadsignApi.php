<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class dashlet_cadsignApi extends SugarApi
{
	var $data;
	public function registerApiRest()
	{
		return array(
		    //GET & POST
		    'dashlet_cadsign' => array(
			//request type
			'reqType' => array('GET','POST'),

			//set authentication
			'noLoginRequired' => true,

			//endpoint path
			'path' => array('dashlet_cadsign'),

			//endpoint variables
			'pathVars' => array(''),

			//method to call
			'method' => 'dashlet_cadsign',

			//short help string to be displayed in the help documentation
			'shortHelp' => 'dashlet_cadsign',

			//long help to be displayed in the help documentation
			//'longHelp' => 'custom/clients/base/api/help/dashlet_cadsignApi_help.html',
		    ),
		);
	}

    /**
     * Method to be used for my MyEndpoint dashlet_cadsign
     */
	public function dashlet_cadsign($api, $args)
	{ 			
		$retorno="";
		try{
			require_once 'modules/Administration/Administration.php';
			$Administration = new Administration();
			$Administration->retrieveSettings("PAI");
			$settings = $Administration->settings["PAI_dashlet_cadsign"];	
			
			if($args['data'][0]["url"] == "**PAI**") $args['data'][0]["url"]=$settings["url"];
			if($args['data'][0]["ws_auth"] == "**PAI**") $args['data'][0]["ws_auth"]=$settings["ws_auth"];
			if($args['data'][0]["authorization"] == "**PAI**") $args['data'][0]["authorization"]="Basic ".base64_encode("{$settings["username"]}:{$settings["password"]}");
			if($args['data'][1]["xmlns_wsdl"] == "**PAI**") $args['data'][1]["xmlns_wsdl"]=$settings["xmlns_wsdl"];
			if($args['data'][1]["xmlns_xsd"] == "**PAI**") $args['data'][1]["xmlns_xsd"]=$settings["url"].$settings["xmlns_xsd"];
			if($args['data'][1]["wsdl"] == "**PAI**") $args['data'][1]["wsdl"]=$settings["wsdl"];
			
			$this->data = $args['data'];			
			$retorno=$retorno=$this->dashlet_cadsign_data();
		}
		catch (Exception $e) {      
			$retorno="dashlet_cadsign->. ERROR1:".$e->getMessage(); 
		}
		return $retorno;
	}	

    /**
     * Custom logic of dashlet_cadsign_data
     */
	function dashlet_cadsign_data(){		
		$retorno="";
		try{			
			/*			
			https://alianzawsprue.alianza.com.co/Ws_CadSign/CadSign?xsd=1  
			Es necesario alguno de los dos parámetros. 

			Encargo ->
			010020007580
			060031002531
			060031002540
			080030003979
			060030007525
			060030007524
			060031002552
			060094006774
			060030007528
			060030007526
			080030003984
			010030015099
			010030015100

			ID_Cliente ->
			2866258
			15347431
			16630800
			
			//mas de un registro
			93129771
			71365731
			8259323
			43252032
			16772002
			
			//un solo registro
			40990985
			52063834
			79901251
			80100152
			16730478
			71555638
			43748470
			16706061

			*/
			/*
			$this->data[1]["xmlns_wsdl"]="http://services.alianza.com/";
			$this->data[1]["xmlns_xsd"]="https://alianzawsprue.alianza.com.co:443/Ws_Siged/Siged?xsd=1";
			$this->data[1]["wsdl"] = "GetInformacionCasos";
			$this->data[1]["Encargo"] = "";
			$this->data[1]["ID_Cliente"] = "40990985";
			$this->data[0]["authorization"] = "Basic QzRkczFnbjk/OCQjOjU0S0wmJmMxcw==";
			$this->data[0]["url"] = "https://alianzawsprue.alianza.com.co";
			$this->data[0]["ws_auth"]="/Ws_CadSign/CadSign?WSDL=";
			*/
			
			
			$result=$this->dashlet_cadsign_autenticar($this->data[0]["url"],$this->data[0]["ws_auth"],$this->get_cadsign_body(),$this->data[0]["authorization"]);		
			$retorno = $result["S:Envelope"]["S:Body"]["ns2:GetInformacionCasosResponse"]["return"];			
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_cadsign->. ERROR2:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	/*
	Body XML del WS
	*/
	function get_cadsign_body(){	
		$retorno="";
		try{		
			$retorno .= "<env:Envelope xmlns:wsdl=\"{$this->data[1]["xmlns_wsdl"]}\" ";
			$retorno .= "xmlns:xsd=\"{$this->data[1]["xmlns_xsd"]}\" ";
			$retorno .= "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
			$retorno .= "xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">\n\t";
			$retorno .= "<env:Body>\n\t\t<wsdl:{$this->data[1]["wsdl"]}>\n\t\t\t";
			$retorno .= "<Encargo>{$this->data[1]["Encargo"]}</Encargo>\n\t\t\t";
			$retorno .= "<ID_Cliente>{$this->data[1]["ID_Cliente"]}</ID_Cliente>\n\t\t\t";
			$retorno .= "</wsdl:{$this->data[1]["wsdl"]}>\n\t</env:Body>\n</env:Envelope>";
			} 
		catch (Exception $e) {      
			$retorno = "dashlet_cadsign->. ERROR6:".$e->getMessage(); 
		}	
		//$GLOBALS['log']->security($response);
		return $retorno;
	}
	
	/*
	Llamar WS cadsign
	*/
	function dashlet_cadsign_autenticar($url,$ws_auth,$data,$authorization){
		$retorno="";
		try{					
			$curl = curl_init();
			curl_setopt_array(
				$curl, array(
					CURLOPT_URL => "{$url}{$ws_auth}",
					CURLOPT_SSL_VERIFYPEER=>false,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 1000,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => $data,
					CURLOPT_HTTPHEADER => array(
						"authorization: {$authorization}",
						"cache-control: no-cache",
						"content-type: text/xml",
					),
				)
			);
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				$retorno="dashlet_cadsign->. ERROR3: cURL Error #{$err}";
			}
			else {	
				$response= $this->xml2array($response, $get_attributes = 3, $priority = 'tag');
				
				$retorno = $response;
			}
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_cadsign->. ERROR2:".$e->getMessage(); 
		}	
		//$GLOBALS['log']->security($response);
		return $retorno;
	}
	
	/*
	Funcion para pasar XML a Array 
	Fuente https://stackoverflow.com/questions/6578832/how-to-convert-xml-into-array-in-php
	*/
	function xml2array($contents, $get_attributes = 1, $priority = 'tag')
    {
        if (!$contents) return array();
        if (!function_exists('xml_parser_create')) {
            // print "'xml_parser_create()' function not found!";
            return array();
        }
        // Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); // http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents) , $xml_values);
        xml_parser_free($parser);
        if (!$xml_values) return; //Hmm...
        // Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();
        $current = & $xml_array; //Refference
        // Go through the tags.
        $repeated_tag_index = array(); //Multiple tags with same name will be turned into an array
        foreach($xml_values as $data) {
            unset($attributes, $value); //Remove existing values, or there will be trouble
            // This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data); //We could use the array by itself, but this cooler.
            $result = array();
            $attributes_data = array();
            if (isset($value)) {
                if ($priority == 'tag') $result = $value;
                else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
            }
            // Set the attributes too.
            if (isset($attributes) and $get_attributes) {
                foreach($attributes as $attr => $val) {                                   
                                    if ( $attr == 'ResStatus' ) {
                                        $current[$attr][] = $val;
                                    }
                    if ($priority == 'tag') $attributes_data[$attr] = $val;
                    else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
            // See tag status and do the needed.
                        //echo"<br/> Type:".$type;
            if ($type == "open") { //The starting of the tag '<tag>'
                $parent[$level - 1] = & $current;
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                    $current[$tag] = $result;
                    if ($attributes_data) $current[$tag . '_attr'] = $attributes_data;
                                        //print_r($current[$tag . '_attr']);
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = & $current[$tag];
                }
                else { //There was another element with the same tag name
                    if (isset($current[$tag][0])) { //If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else { //This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        ); //This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                        if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = & $current[$tag][$last_item_index];
                }
            }
            elseif ($type == "complete") { //Tags that ends in 1 line '<tag />'
                // See if the key is already taken.
                if (!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data) $current[$tag . '_attr'] = $attributes_data;
                }
                else { //If taken, put all things inside a list(array)
                    if (isset($current[$tag][0]) and is_array($current[$tag])) { //If it is already an array...
                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else { //If it is not an array...
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        ); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            }
            elseif ($type == 'close') { //End of tag '</tag>'
                $current = & $parent[$level - 1];
            }
        }
        return ($xml_array);
    }
}
?>
