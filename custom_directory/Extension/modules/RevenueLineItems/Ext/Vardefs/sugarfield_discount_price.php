<?php
 // created: 2018-07-07 16:31:00
$dictionary['RevenueLineItem']['fields']['discount_price']['default']=0.0;
$dictionary['RevenueLineItem']['fields']['discount_price']['len']=26;
$dictionary['RevenueLineItem']['fields']['discount_price']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['discount_price']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['discount_price']['enable_range_search']=false;

 ?>