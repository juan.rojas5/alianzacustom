<?php
$mod_strings['LBL_CC_SYNCED'] = 'Linked with Constant Contact';
$mod_strings['LBL_CCID'] = 'Constant Contact ID';
$mod_strings['LBL_CCILOG_SUBPANEL'] = 'Constant Contact Audit Log';
$mod_strings['LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE'] = 'Constant Contact Target Lists';
$mod_strings['LBL_PROSPECTLISTS_TARGET_LIST_TITLE'] = 'Target Lists';
$mod_strings["LBL_CCLISTS"] = 'Constant Contact Target Lists';
