<?php
$viewdefs['Contacts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'hint-contacts-photo',
                'size' => 'large',
                'dismiss_label' => true,
                'white_list' => true,
                'related_fields' => 
                array (
                  0 => 'hint_contact_pic',
                ),
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'dismiss_label' => true,
                'type' => 'fullname',
                'fields' => 
                array (
                  0 => 'salutation',
                  1 => 'first_name',
                  2 => 'last_name',
                ),
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sasa_parentescorelacion_c',
                'label' => 'LBL_SASA_PARENTESCORELACION_C',
              ),
              1 => 
              array (
                'name' => 'sasa_relacioncuenta_c',
                'label' => 'LBL_SASA_RELACIONCUENTA_C',
              ),
              2 => 
              array (
                'name' => 'account_name',
              ),
              3 => 
              array (
                'span' => 12,
              ),
              4 => 
              array (
                'name' => 'sasa_tipoidentificacion_c',
                'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
              ),
              5 => 
              array (
                'name' => 'sasa_nroidentificacion_c',
                'label' => 'LBL_SASA_NROIDENTIFICACION_C',
              ),
              6 => 'department',
              7 => 'title',
              8 => 'phone_mobile',
              9 => 
              array (
                'name' => 'do_not_call',
              ),
              10 => 
              array (
                'name' => 'assistant',
                'comment' => 'Name of the assistant of the contact',
                'label' => 'LBL_ASSISTANT',
              ),
              11 => 
              array (
                'name' => 'sasa_noenviarinfocliente_c',
                'label' => 'LBL_SASA_NOENVIARINFOCLIENTE_C',
              ),
              12 => 
              array (
                'name' => 'email',
              ),
              13 => 
              array (
                'span' => 12,
              ),
              14 => 
              array (
                'name' => 'sasa_infoquiererecibir_c',
                'label' => 'LBL_SASA_INFOQUIERERECIBIR_C',
              ),
              15 => 
              array (
                'name' => 'sasa_tipoinfoperiodicidad_c',
                'label' => 'LBL_SASA_TIPOINFOPERIODICIDAD_C',
              ),
              16 => 
              array (
                'name' => 'lead_source',
              ),
              17 => 
              array (
                'name' => 'sasa_quienrefirio_c',
                'label' => 'LBL_SASA_QUIENREFIRIO_C',
              ),
              18 => 
              array (
                'name' => 'description',
              ),
              19 => 
              array (
                'span' => 12,
              ),
              20 => 
              array (
                'name' => 'business_center_name',
              ),
              21 => 
              array (
                'span' => 12,
              ),
              22 => 'market_score',
            ),
          ),
          2 => 
          array (
            'columns' => 2,
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'collapsed',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'sasa_genero_c',
                'label' => 'LBL_SASA_GENERO_C',
              ),
              1 => 
              array (
                'name' => 'sasa_estadocivil_c',
                'label' => 'LBL_SASA_ESTADOCIVIL_C',
              ),
              2 => 
              array (
                'name' => 'sasa_pais_c',
                'label' => 'LBL_SASA_PAIS_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_condicionespecial_c',
                'label' => 'LBL_SASA_CONDICIONESPECIAL_C',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_lgbtiq_c',
                'label' => 'LBL_SASA_LGBTIQ_C',
              ),
              5 => 
              array (
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'sasa_departamentointer_c',
                'label' => 'LBL_SASA_DEPARTAMENTOINTER_C',
              ),
              7 => 
              array (
                'name' => 'sasa_municipiointer_c',
                'label' => 'LBL_SASA_MUNICIPIOINTER_C',
              ),
              8 => 
              array (
                'name' => 'sasa_departamento_c',
                'label' => 'LBL_SASA_DEPARTAMENTO_C',
              ),
              9 => 
              array (
                'name' => 'sasa_municipio_c',
                'label' => 'LBL_SASA_MUNICIPIO_C',
              ),
              10 => 
              array (
                'name' => 'primary_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_PRIMARY_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'primary_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'primary_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'primary_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'primary_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'primary_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
                  ),
                ),
              ),
              11 => 
              array (
                'name' => 'alt_address',
                'type' => 'fieldset',
                'css_class' => 'address',
                'label' => 'LBL_ALT_ADDRESS',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'alt_address_street',
                    'css_class' => 'address_street',
                    'placeholder' => 'LBL_ALT_ADDRESS_STREET',
                  ),
                  1 => 
                  array (
                    'name' => 'alt_address_city',
                    'css_class' => 'address_city',
                    'placeholder' => 'LBL_ALT_ADDRESS_CITY',
                  ),
                  2 => 
                  array (
                    'name' => 'alt_address_state',
                    'css_class' => 'address_state',
                    'placeholder' => 'LBL_ALT_ADDRESS_STATE',
                  ),
                  3 => 
                  array (
                    'name' => 'alt_address_postalcode',
                    'css_class' => 'address_zip',
                    'placeholder' => 'LBL_ALT_ADDRESS_POSTALCODE',
                  ),
                  4 => 
                  array (
                    'name' => 'alt_address_country',
                    'css_class' => 'address_country',
                    'placeholder' => 'LBL_ALT_ADDRESS_COUNTRY',
                  ),
                  5 => 
                  array (
                    'name' => 'copy',
                    'label' => 'NTC_COPY_PRIMARY_ADDRESS',
                    'type' => 'copy',
                    'mapping' => 
                    array (
                      'primary_address_street' => 'alt_address_street',
                      'primary_address_city' => 'alt_address_city',
                      'primary_address_state' => 'alt_address_state',
                      'primary_address_postalcode' => 'alt_address_postalcode',
                      'primary_address_country' => 'alt_address_country',
                    ),
                  ),
                ),
              ),
              12 => 
              array (
                'name' => 'sasa_telefono_c',
                'label' => 'LBL_SASA_TELEFONO_C',
              ),
              13 => 
              array (
                'name' => 'phone_home',
                'comment' => 'Home phone number of the contact',
                'label' => 'LBL_HOME_PHONE',
              ),
              14 => 
              array (
                'name' => 'sasa_telefono2_c',
                'label' => 'LBL_SASA_TELEFONO2_C',
              ),
              15 => 
              array (
                'name' => 'assistant_phone',
                'comment' => 'Phone number of the assistant of the contact',
                'label' => 'LBL_ASSISTANT_PHONE',
              ),
              16 => 
              array (
                'name' => 'phone_fax',
              ),
              17 => 
              array (
                'span' => 12,
              ),
              18 => 
              array (
                'name' => 'sasa_fondocesantias_c',
                'label' => 'LBL_SASA_FONDOCESANTIAS_C',
              ),
              19 => 
              array (
                'name' => 'sasa_fondopensiones_c',
                'label' => 'LBL_SASA_FONDOPENSIONES_C',
              ),
              20 => 
              array (
                'name' => 'birthdate',
                'comment' => 'The birthdate of the contact',
                'label' => 'LBL_BIRTHDATE',
              ),
              21 => 
              array (
                'name' => 'sasa_hobbiesaficionesdepor_c',
                'label' => 'LBL_SASA_HOBBIESAFICIONESDEPOR_C',
              ),
              22 => 
              array (
                'name' => 'sasa_nrohijos_c',
                'label' => 'LBL_SASA_NROHIJOS_C',
              ),
              23 => 
              array (
                'name' => 'preferred_language',
                'type' => 'language',
              ),
              24 => 
              array (
                'name' => 'facebook',
                'comment' => 'The facebook name of the user',
                'label' => 'LBL_FACEBOOK',
              ),
              25 => 'twitter',
              26 => 
              array (
                'name' => 'googleplus',
                'comment' => 'The google plus id of the user',
                'label' => 'LBL_GOOGLEPLUS',
              ),
              27 => 
              array (
                'span' => 12,
              ),
              28 => 
              array (
                'name' => 'campaign_name',
              ),
              29 => 
              array (
                'name' => 'report_to_name',
              ),
              30 => 
              array (
                'name' => 'portal_user_company_name',
              ),
              31 => 
              array (
                'span' => 12,
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'collapsed',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              3 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              4 => 
              array (
                'name' => 'sasa_fechadevinculacionsc_c',
                'label' => 'LBL_SASA_FECHADEVINCULACIONSC_C',
              ),
              5 => 
              array (
                'name' => 'sasa_fechadeactualizacionsc_c',
                'label' => 'LBL_SASA_FECHADEACTUALIZACIONSC',
              ),
              6 => 
              array (
                'name' => 'sasa_fechademodificacionsc_c',
                'label' => 'LBL_SASA_FECHADEMODIFICACIONSC_C',
              ),
              7 => 
              array (
                'name' => 'tag',
              ),
              8 => 
              array (
                'name' => 'hint_account_website',
                'type' => 'stage2_url',
                'white_list' => true,
              ),
              9 => 'hint_education',
              10 => 
              array (
                'name' => 'hint_education_2',
                'parent_key' => 'hint_education',
              ),
              11 => 'hint_job_2',
              12 => 'hint_account_size',
              13 => 'hint_account_industry',
              14 => 'hint_account_location',
              15 => 
              array (
                'name' => 'hint_account_description',
                'account_key' => 'description',
              ),
              16 => 'hint_account_founded_year',
              17 => 
              array (
                'name' => 'hint_industry_tags',
                'account_key' => 'hint_account_industry_tags',
              ),
              18 => 'hint_account_naics_code_lbl',
              19 => 
              array (
                'name' => 'hint_account_sic_code_label',
                'account_key' => 'sic_code',
              ),
              20 => 'hint_account_fiscal_year_end',
              21 => 
              array (
                'name' => 'hint_account_annual_revenue',
                'account_key' => 'annual_revenue',
              ),
              22 => 
              array (
                'name' => 'hint_facebook',
                'type' => 'stage2_url',
              ),
              23 => 
              array (
                'name' => 'hint_twitter',
                'type' => 'stage2_url',
              ),
              24 => 
              array (
                'name' => 'hint_account_facebook_handle',
                'type' => 'stage2_url',
              ),
              25 => 
              array (
                'name' => 'hint_account_twitter_handle',
                'type' => 'stage2_url',
                'account_key' => 'twitter',
              ),
              26 => 
              array (
                'name' => 'phone_other',
                'type' => 'phone',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PREVIEWVIEW_PANEL2',
            'label' => 'LBL_PREVIEWVIEW_PANEL2',
            'columns' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => false,
                'name' => 'sasa_nombrem4_c',
                'label' => 'LBL_SASA_NOMBREM4',
              ),
              1 => 
              array (
                'readonly' => false,
                'name' => 'sasa_apellidom4_c',
                'label' => 'LBL_SASA_APELLIDOM4',
              ),
              2 => 
              array (
                'readonly' => false,
                'name' => 'sasa_nroidentificacionm4_c',
                'label' => 'LBL_SASA_NROIDENTIFICACIONM4_C',
              ),
              3 => 
              array (
                'readonly' => false,
                'name' => 'sasa_tipoidentificacionm4_c',
                'label' => 'LBL_SASA_TIPOIDENTIFICACIONM4_C',
              ),
              4 => 
              array (
                'readonly' => false,
                'name' => 'sasa_correom4_c',
                'label' => 'LBL_SASA_CORREOM4',
              ),
              5 => 
              array (
                'readonly' => false,
                'name' => 'sasa_celularm4_c',
                'label' => 'LBL_SASA_CELULARM4',
              ),
              6 => 
              array (
                'readonly' => false,
                'name' => 'sasa_direccionprincm4_c',
                'studio' => 'visible',
                'label' => 'LBL_SASA_DIRECCIONPRINCM4',
              ),
              7 => 
              array (
                'readonly' => false,
                'name' => 'sasa_municipiom4_c',
                'label' => 'LBL_SASA_MUNICIPIOM4_C',
              ),
              8 => 
              array (
                'readonly' => false,
                'name' => 'sasa_departamentom4_c',
                'label' => 'LBL_SASA_DEPARTAMENTOM4_C',
              ),
              9 => 
              array (
                'readonly' => false,
                'name' => 'sasa_fechanacm4_c',
                'label' => 'LBL_SASA_FECHANACM4_C',
              ),
              10 => 
              array (
                'readonly' => false,
                'name' => 'sasa_razon_socialm4_c',
                'label' => 'LBL_SASA_RAZON_SOCIALM4_C',
              ),
              11 => 
              array (
                'span' => 12,
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
