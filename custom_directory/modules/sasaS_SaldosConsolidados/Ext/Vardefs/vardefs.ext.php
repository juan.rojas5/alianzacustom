<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasaS_SaldosConsolidados/Ext/Vardefs/accounts_sasas_saldosconsolidados_1_sasaS_SaldosConsolidados.php

// created: 2019-01-04 22:23:30
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'accounts_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1_name"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link' => 'accounts_sasas_saldosconsolidados_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["accounts_sasas_saldosconsolidados_1accounts_ida"] = array (
  'name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE_ID',
  'id_name' => 'accounts_sasas_saldosconsolidados_1accounts_ida',
  'link' => 'accounts_sasas_saldosconsolidados_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasaS_SaldosConsolidados/Ext/Vardefs/sasa_categorias_sasas_saldosconsolidados_1_sasaS_SaldosConsolidados.php

// created: 2019-01-04 22:24:53
$dictionary["sasaS_SaldosConsolidados"]["fields"]["sasa_categorias_sasas_saldosconsolidados_1"] = array (
  'name' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'source' => 'non-db',
  'module' => 'sasa_Categorias',
  'bean_name' => 'sasa_Categorias',
  'side' => 'right',
  'vname' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'id_name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'link-type' => 'one',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["sasa_categorias_sasas_saldosconsolidados_1_name"] = array (
  'name' => 'sasa_categorias_sasas_saldosconsolidados_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASA_CATEGORIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'name',
);
$dictionary["sasaS_SaldosConsolidados"]["fields"]["sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida"] = array (
  'name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE_ID',
  'id_name' => 'sasa_categorias_sasas_saldosconsolidados_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasas_saldosconsolidados_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasaS_SaldosConsolidados/Ext/Vardefs/sugarfield_sasa_saldototal_c.php

 // created: 2019-01-08 21:55:34
$dictionary['sasaS_SaldosConsolidados']['fields']['sasa_saldototal_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasaS_SaldosConsolidados/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasaS_SaldosConsolidados']['full_text_search']=true;

?>
