<?php
 // created: 2018-06-12 20:24:59
$dictionary['Account']['fields']['twitter']['audited']=false;
$dictionary['Account']['fields']['twitter']['massupdate']=false;
$dictionary['Account']['fields']['twitter']['comments']='The twitter name of the company';
$dictionary['Account']['fields']['twitter']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['twitter']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['twitter']['merge_filter']='disabled';
$dictionary['Account']['fields']['twitter']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['twitter']['calculated']=false;

 ?>