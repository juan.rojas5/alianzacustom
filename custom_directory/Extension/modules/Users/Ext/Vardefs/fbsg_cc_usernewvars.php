<?php
/**
 * ATTENTION!!!!!!!!!!
 * ANY CHANGES MADE HERE MUST BE REFLECTED IN THE pre_execute.php FILE
 *
 */
$cc_module = 'User';

global $sugar_version;
$is7 = !!(preg_match('/^7/', $sugar_version));

$dictionary[$cc_module]['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'reportable' => true,
    'importable' => false,
    'len' => 255,
);

$dictionary[$cc_module]['fields']['cc_synced'] = array(
    'required' => false,
    'name' => 'cc_synced',
    'vname' => 'LBL_CC_SYNCED',
    'type' => 'bool',
    'default' => false,
    'importable' => false,
    'reportable' => true,
    'duplicate_merge' => 'disabled',
);
