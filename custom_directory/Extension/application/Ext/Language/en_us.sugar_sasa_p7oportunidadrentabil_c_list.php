<?php
 // created: 2018-06-06 22:00:00

$app_list_strings['sasa_p7oportunidadrentabil_c_list']=array (
  '' => '',
  3 => 'a. No estaría dispuesto a asumir mayor riesgo',
  5 => 'b. Estaría dispuesto a asumir un poco más de riesgo tratando de no incurrir en pérdidas de capital',
  7 => 'c. Estaría dispuesto a asumir mucho más riesgo para aumentar mi rentabilidad',
  9 => 'd. Me endeudaría o buscaría opciones de apalancamiento para obtener un retorno más interesante',
);