<?php
$dictionary['Prospect']['fields']['fbsg_ccintegrationlog_prospects'] = array(
    'name' => 'fbsg_ccintegrationlog_prospects',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_prospects',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Prospect"]["fields"]["prospect_list_prospects"] = array(
    'name' => 'prospect_list_prospects',
    'type' => 'link',
    'relationship' => 'prospect_list_prospects',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Prospect';
include('custom/include/fbsg_cc_newvars.php');
