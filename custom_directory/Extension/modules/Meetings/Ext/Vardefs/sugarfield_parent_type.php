<?php
 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_type']['len']=255;
$dictionary['Meeting']['fields']['parent_type']['required']=true;
$dictionary['Meeting']['fields']['parent_type']['audited']=false;
$dictionary['Meeting']['fields']['parent_type']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_type']['calculated']=false;
$dictionary['Meeting']['fields']['parent_type']['options']='';

 ?>