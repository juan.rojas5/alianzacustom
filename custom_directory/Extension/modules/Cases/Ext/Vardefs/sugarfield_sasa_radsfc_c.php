<?php
 // created: 2022-03-14 09:44:21
$dictionary['Case']['fields']['sasa_radsfc_c']['labelValue']='Rad SFC';
$dictionary['Case']['fields']['sasa_radsfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_radsfc_c']['enforced']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['dependency']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_radsfc_c']['readonly_formula']='';

 ?>