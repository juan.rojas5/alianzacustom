<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_saldos_av_ultimo_registro';
function tarea_saldos_av_ultimo_registro(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_saldos_av_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;  
    /******Saldos AV******/
    //285
    $querySAV = "    
	UPDATE sasa_saldosav LEFT JOIN (select sasa_saldosav.name, sasa_saldosav.id Idsaldo, accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida from sasa_saldosav inner join accounts_sasa_saldosav_1_c ON sasa_saldosav.id=accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb GROUP BY sasa_saldosav.name, sasa_saldosav.assigned_user_id, accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida ORDER BY `sasa_saldosav`.`sasa_fecharegistro_c` DESC)As tabla ON sasa_saldosav.id=tabla.Idsaldo WHERE sasa_saldosav.deleted=0 AND sasa_saldosav.team_id != '' AND sasa_saldosav.team_set_id !='' AND tabla.Idsaldo IS NULL
    ";
    $resultSAV = $db->query($querySAV);
    $getAffectedRowCount = $GLOBALS['db']->getAffectedRowCount($resultSAV)." ".$GLOBALS['db']->affected_rows;
    $GLOBALS['log']->security("- {$getAffectedRowCount}= SaldosAV Actualizados");
    
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_saldos_av_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
