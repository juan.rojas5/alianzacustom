<?php  
/**
 * Esta clase define el objeto para las llamadas RestApi de la SFC para los momentos 1, 2 y 3
 * @author Juan F. Zuluaga - juanzuluaga@sasaconsultoria.com
 */

class SfcRestApi {
	
	private static $SHA = 'sha256';
	private $urlBase;
	private $user;
	private $password;
	private $secretKey;

	function __construct($business, $config) {
		
		$this->urlBase = $config['rest']['urlBase'];

		switch ($business) {
			case 'fiduciaria':
				$this->user = $config['rest']['oauth']['alianzaF']['username'];
				$this->password = $config['rest']['oauth']['alianzaF']['password'];
				$this->secretKey = $config['rest']['oauth']['alianzaF']['secretKey'];
				break;
			case 'valores':
				$this->user = $config['rest']['oauth']['alianzaV']['username'];
				$this->password = $config['rest']['oauth']['alianzaV']['password'];
				$this->secretKey = $config['rest']['oauth']['alianzaV']['secretKey'];
				break;				
			
			default:
				$errorMessage = "SFC: {$business} doesn't exist SfcRestApi.__construct()";
				throw new Exception($errorMessage);
				break;
		}
	}

	public function uploadFiles($arrayWithFileToSend) {
		if ($arrayWithFileToSend == NULL OR sizeof($arrayWithFileToSend) == 0) {
			$errorMessage = "SFC: arrayWithFileToSend can't be empty uploadFiles-POST.";
			throw new Exception($errorMessage);
		}
		$uploadFilesUrl = "storage/";
		$restVerb = 'POST';
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);
		$bodyToSend = array(
			'file' => new CURLFILE($arrayWithFileToSend['path'], $arrayWithFileToSend['mime'], $arrayWithFileToSend['name']),
			'codigo_queja' => $arrayWithFileToSend['codigo_queja'],
			'type' => $arrayWithFileToSend['type']
		);
		$bodySinged = $this->signData(
			$this->bodyArrayToJsonString(
				array(
					'codigo_queja' => $arrayWithFileToSend['codigo_queja'], 
					'type' => $arrayWithFileToSend['type']
				)
			), $restVerb
		); 
		$responseApi = $this->generalRestCall(
			$uploadFilesUrl,
			$restVerb,
			$bodyToSend,
			$bodySinged,
			$oauth->access
		);
		return $responseApi;
	}

	public function updateComplaintInSfc($complaintArray = array(), $complaintId) {
		if (empty($complaintArray)) {
			$errorMessage = "SFC: complaintArray can't be empty updateComplaintInSfc-PUT.";
			throw new Exception($errorMessage);
		}

		$updateComplaintUrl = "queja/{$complaintId}/";
		$restVerb = 'PUT';
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);
		$bodyJsonString = $this->bodyArrayToJsonString($complaintArray, 2);
		$responseApi = $this->generalRestCall(
			$updateComplaintUrl,
			$restVerb,
			$bodyJsonString,
			$this->signData($bodyJsonString, $restVerb),
			$oauth->access
		);

		return $responseApi;
	}

	public function postCreateNewComplaintInSfc($complaintArray = array()) {
		if (empty($complaintArray)) {
			$errorMessage = "SFC: complaintArray can't be empty createNewComplaintInSfc-POST.";
			throw new Exception($errorMessage);
		}

		$createComplaintUrl = "queja/";
		$restVerb = 'POST';
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);
		$bodyJsonString = $this->bodyArrayToJsonString($complaintArray, 2);
		$responseApi = $this->generalRestCall(
			$createComplaintUrl,
			$restVerb,
			$bodyJsonString,
			$this->signData($bodyJsonString, $restVerb),
			$oauth->access
		);

		return $responseApi;
	}

	public function getFilesComplaint($complaintId) {
		if (empty($complaintId)) {
			$errorMessage = "SFC: complaintId can't be empty fileComplaint-GET";
			throw new Exception($errorMessage);
		}

		$fileComplaintUrl = "storage/?codigo_queja__codigo_queja={$complaintId}";
		$restVerb = "GET";
		$urlToEncrypt = $this->urlBase.$fileComplaintUrl;

		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);

		$responseApi = $this->generalRestCall(
			$fileComplaintUrl,
			$restVerb,
			NULL,
			$this->signData($urlToEncrypt, $restVerb),
			$oauth->access
		);

		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in fileComplaint-GET " 
			. $responseApi['info']['http_code']
			. 'response: ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}

		return $responseApi;

	}

	public function postAck($arrayWithComplaintIds = NULL) {
		if (empty($arrayWithComplaintIds)) {
			$errorMessage = "SFC: bodyWithComplaintId can't be empty ack-POST";
			throw new Exception($errorMessage);
		}

		require_once('models/api/ComplaintAckRequestDto.php');

		$ackUrl = "complaint/ack";
		$restVerb = 'POST';
		$complaintAckRequestDto = new ComplaintAckRequestDto($arrayWithComplaintIds);
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);
		$bodyJsonString = $this->bodyArrayToJsonString($complaintAckRequestDto);
		$responseApi = $this->generalRestCall(
			$ackUrl,
			$restVerb,
			$bodyJsonString,
			$this->signData($bodyJsonString, $restVerb),
			$oauth->access
		);

		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in ack-POST " . $responseApi['info']['http_code']. ' ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}

		return $responseApi;
	}

	public function getComplaints() {
		$casesUrl = 'queja/';
		$restVerb = 'GET';
		$urlToEncrypt = $this->urlBase.$casesUrl;
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);

		$responseApi = $this->generalRestCall(
			$casesUrl, 
			$restVerb, 
			NULL, 
			$this->signData($urlToEncrypt, $restVerb), 
			$oauth->access);
		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in complaint-GET " 
			. $responseApi['info']['http_code']
			. ' Result: ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}
		return $responseApi;
	}

	public function userAck($arrayWithComplaintIds = NULL) {
		if (empty($arrayWithComplaintIds)) {
			$errorMessage = "SFC: bodyWithComplaintId can't be empty ack-USER";
			throw new Exception($errorMessage);
		}

		require_once('models/api/UserAckRequestDto.php');

		$ackUrl = "usuarios/ack/";
		$restVerb = 'POST';
		$UserAckRequestDto = new UserAckRequestDto($arrayWithComplaintIds);
		
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);
		$bodyJsonString = $this->bodyArrayToJsonString($UserAckRequestDto);
		$responseApi = $this->generalRestCall(
			$ackUrl,
			$restVerb,
			$bodyJsonString,
			$this->signData($bodyJsonString, $restVerb),
			$oauth->access
		);

		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in ack-USER.. " . $responseApi['info']['http_code']. ' ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}

		return $responseApi;
	}
	//Traer usuarios de la super.
	public function getUserInfo() {
		$GLOBALS['log']->security("Tarea:GET user info:");
		
		$userUrl = 'usuarios/info/';
		$restVerb = 'GET';
		$urlToEncrypt = $this->urlBase.$userUrl;
		$oauthResponse = $this->postLogin();
		$oauth = json_decode($oauthResponse['response']);

		$responseApi = $this->generalRestCall(
			$userUrl, 
			$restVerb, 
			NULL, 
			$this->signData($urlToEncrypt, $restVerb), 
			$oauth->access);
		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in complaint-GET " 
			. $responseApi['info']['http_code']
			. ' Result: ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}
		return $responseApi;
	}

	private function postLogin() {
		$loginUrl = 'login/';
		$restVerb = 'POST';
		$body = array(
			"username"=> $this->user, 
			"password" => $this->password
		);
		$bodyString = $this->bodyArrayToJsonString($body);
		$responseApi = $this->generalRestCall(
			$loginUrl, 
			$restVerb, 
			$bodyString, 
			$this->signData($bodyString, $restVerb));
		if ($responseApi['info']['http_code'] < 200 OR $responseApi['info']['http_code'] > 206) {
			$errorMessage = "SFC: BAD_STATUS_CODE in oatuh-POST " . $responseApi['info']['http_code'] . ' ' . $responseApi['response'];
			throw new Exception($errorMessage);
		}

		return $responseApi;
	}

	private function generalRestCall($url = '', $method = '', $body, $signData = '', $token = '') {

		if (empty($url) || empty($method)) {
			$errorMessage = "SFC: check necesary restApy args.";
			throw new Exception($errorMessage);
		}

		$GLOBALS['log']->security("URL: ");
		$GLOBALS['log']->security($url);

		$GLOBALS['log']->security("METHOD: ");
		$GLOBALS['log']->security($method);

		$GLOBALS['log']->security("BODY: ");
		$GLOBALS['log']->security(print_r($body, TRUE));

		$GLOBALS['log']->security("SIGNDATA: ");
		$GLOBALS['log']->security($signData);

		$curl = curl_init();

		$curlSetOpt = array(
		  CURLOPT_URL => $this->urlBase.$url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $method,
		  CURLOPT_HTTPHEADER => array(
		  	"X-SFC-Signature: $signData")
		);
		$GLOBALS['log']->security("signData: " .$signData);
		if (!empty($token)) {
			$curlSetOpt[CURLOPT_HTTPHEADER][] = "Authorization: Bearer {$token}";
		}

		if (is_array($body)) {
			$curlSetOpt[CURLOPT_HTTPHEADER][] = "Content-type: multipart/form-data"; 
		} else {
			$curlSetOpt[CURLOPT_HTTPHEADER][] = "Content-Type: application/json"; 
		}

		if (($method == 'POST' || $method == 'PUT') && !empty($body)) {
			
			$curlSetOpt[CURLOPT_POSTFIELDS] = $body;
		} 

		curl_setopt_array($curl, $curlSetOpt);
		$response = curl_exec($curl);
		$errorCurl = curl_errno($curl);
		if(!$errorCurl){

		  $info = curl_getinfo($curl);
		  $GLOBALS['log']->security('Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url']);
		  curl_close($curl);
		  return array('info' => $info, 'response' => $response);
		} else {
			$GLOBALS['log']->security("ERROR CURL: ". $errorCurl);
			
			$errorMessage = 'Curl error: ' .  $errorCurl;
			curl_close($curl);
			throw new Exception($errorMessage);
		}
	}

	private function signData($toEncrypt, $method) {

		if (empty($toEncrypt) || empty($method)) {
			$errorMessage = "SFC: check args can't be empty in signData.";
			throw new Exception($errorMessage);
		}

		$encrypted = '';

		if ($method == 'POST' || $method == 'PUT') {
			$encrypted = strtoupper(hash_hmac(self::$SHA, $toEncrypt, $this->secretKey));
		} else {
			$encrypted = strtoupper(hash_hmac(self::$SHA, utf8_encode($toEncrypt), $this->secretKey));
		}

		if (empty($encrypted)) {
			$errorMessage = "SFC: data could not be signed.";
			throw new Exception($errorMessage);
		} else {
			return $encrypted;
		}
	}

	private function bodyArrayToJsonString($bodyArray = NULL, $moment = 0) {
		if (empty($bodyArray)) {
			$errorMessage = "SFC: body can't be converted to JsonString";
			throw new Exception($errorMessage);
		}

		if ($moment == 2) {
			$search = array( "\n", '	{    "', ',    ', '{    "' );
			$replace = array( "", '{"', ', ', '{"');
			return str_replace($search, $replace, json_encode($bodyArray, JSON_PRETTY_PRINT));
		} else {
			$search  = array('","', '":{', '":', '":["');
			$replace = array('", "', '": {', '": ', '": ["');
			return str_replace($search, $replace, json_encode($bodyArray, JSON_UNESCAPED_SLASHES));
		}
		
	}

}

?>