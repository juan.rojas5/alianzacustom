<?php
$dependencies['Leads']['SetOption'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_tipopersona_c,"Natural")',
   'triggerFields' => array('sasa_tipopersona_c','sasa_tipoidentificacion_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_tipoidentificacion_c',
           'keys' => 'createList("","Cedula de Ciudadania","Cedula de Extranjeria","Pasaporte","NUIP","Registro Civil","Tarjeta de identidad","Carne Diplomatico","Fideicomiso","Sociedad Extranjera","Otro")',
           'labels' => 'createList("","Cédula de Ciudadanía","Cédula de Extranjería","Pasaporte","NUIP","Registro Civil","Tarjeta de Identidad","Carné Diplomatico","Fideicomiso","Sociedad Extranjera","Otro")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_tipoidentificacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_ocupacion_c',
           'keys' => 'createList("","Asalariado","Empleado","Estudiante","Hogar","Independiente","Militar","Pensionado","Religioso","Rentista","Socio","Otros")',
           'labels' => 'createList("","Asalariado","Empleado","Estudiante","Hogar","Independiente","Militar","Pensionado","Religioso","Rentista","Socio","Otros")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_ocupacion_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
