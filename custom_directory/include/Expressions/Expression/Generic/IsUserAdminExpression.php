<?php
/**
 * Project: SugarCRM Logic Expression for checking if user is admin
 * Original Dev: Antonio Musarra, Oct 2014
 * @2009-2014 Antonio Musarra <antonio.musarra[at]gmail.com>
 *
 * Desc: SugarCRM Logic Expression ext class
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */    
    require_once('include/Expressions/Expression/Generic/GenericExpression.php');
 
    /**
     * <b>IsUserAdmin()</b><br>
     * Returns true if current user is admin<br/>
     * ex: <i>isUserAdmin()</i>
     */
    class IsUserAdminExpression extends GenericExpression
    {
        function evaluate()
        {
            $params = $this->getParameters();
            $userId = $params[0]->evaluate();

            global $current_user;
            
            $GLOBALS['log']->info("Check if $current_user->id is admin...");

            if($current_user->is_admin)
            {
                $GLOBALS['log']->info("Check if $current_user->id is admin...[TRUE]");
                return AbstractExpression::$TRUE;   
            } else {
                $GLOBALS['log']->info("Check if $current_user->id is admin...[FALSE]");
                return AbstractExpression::$FALSE;
            }
        }

        /**
         * Returns the JS Equivalent of the evaluate function.
         */
        static function getJSEvaluate() {
            return <<<EOQ
                var userId = this.getParameters();
                var type = SUGAR.App.user.get('type'); 
                
                if(type === "admin")
                {
                    return SUGAR.expressions.Expression.TRUE;
                } else {
                    return SUGAR.expressions.Expression.FALSE;
                }
EOQ;
        }

        /**
         * Returns the maximum number of parameters needed.
         */
        static function getParameterCount()
        {
            return 1;
        }

        /**
         * Returns the opreation name that this Expression should be
         * called by.
         */
        static function getOperationName()
        {
            return "isUserAdmin"; 
        }

        /**
         * The first parameter is a string.
         */
        static function getParameterTypes() {
            return AbstractExpression::$STRING_TYPE;
        }
    
        /**
         * Returns the String representation of this Expression.
         */
        function toString() {
        }
    }
?>