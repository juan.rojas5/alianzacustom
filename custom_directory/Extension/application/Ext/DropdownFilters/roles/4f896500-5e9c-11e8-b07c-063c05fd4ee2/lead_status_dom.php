<?php
// created: 2018-07-21 20:33:46
$role_dropdown_filters['lead_status_dom'] = array (
  '' => false,
  'New' => false,
  'Assigned' => true,
  'In Process' => false,
  'Converted' => false,
  'Recycled' => false,
  'Dead' => true,
);