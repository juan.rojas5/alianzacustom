<?php
 // created: 2023-02-02 19:30:49
$dictionary['Case']['fields']['sasa_detalle_c']['labelValue']='Detalle';
$dictionary['Case']['fields']['sasa_detalle_c']['dependency']='';
$dictionary['Case']['fields']['sasa_detalle_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_detalle_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_detalle_c']['visibility_grid']=array (
  'trigger' => 'sasa_subproceso_c',
  'values' => 
  array (
    1 => 
    array (
      0 => '',
      1 => '1',
    ),
    2 => 
    array (
      0 => '',
      1 => '2',
      2 => '3',
    ),
    3 => 
    array (
      0 => '',
      1 => '4',
      2 => '5',
      3 => '6',
      4 => '10',
      5 => '12',
      6 => '13',
      7 => '14',
      8 => '15',
      9 => '2',
    ),
    4 => 
    array (
      0 => '',
      1 => '7',
      2 => '8',
      3 => '9',
      4 => '35',
    ),
    5 => 
    array (
      0 => '',
      1 => '11',
    ),
    6 => 
    array (
      0 => '',
      1 => '16',
    ),
    7 => 
    array (
      0 => '',
      1 => '17',
      2 => '18',
    ),
    8 => 
    array (
      0 => '',
      1 => '20',
      2 => '21',
      3 => '22',
      4 => '23',
      5 => '24',
      6 => '19',
      7 => '25',
      8 => '66',
      9 => '67',
      10 => '28',
      11 => '162',
      12 => '163',
    ),
    9 => 
    array (
      0 => '',
      1 => '26',
      2 => '27',
      3 => '28',
      4 => '29',
    ),
    10 => 
    array (
      0 => '',
      1 => '37',
      2 => '38',
      3 => '39',
    ),
    11 => 
    array (
      0 => '',
      1 => '40',
    ),
    12 => 
    array (
      0 => '',
      1 => '41',
      2 => '42',
      3 => '43',
      4 => '47',
      5 => '56',
      6 => '58',
      7 => '49',
      8 => '63',
      9 => '66',
      10 => '62',
      11 => '70',
    ),
    13 => 
    array (
      0 => '',
      1 => '47',
      2 => '56',
      3 => '58',
      4 => '59',
      5 => '60',
      6 => '61',
    ),
    14 => 
    array (
      0 => '',
      1 => '62',
      2 => '63',
      3 => '64',
    ),
    15 => 
    array (
      0 => '',
      1 => '102',
      2 => '19',
      3 => '70',
      4 => '71',
      5 => '21',
    ),
    16 => 
    array (
      0 => '',
      1 => '19',
      2 => '75',
      3 => '77',
      4 => '21',
      5 => '78',
      6 => '79',
    ),
    17 => 
    array (
      0 => '',
      1 => '80',
      2 => '81',
      3 => '82',
      4 => '83',
      5 => '84',
      6 => '85',
      7 => '86',
    ),
    18 => 
    array (
      0 => '',
      1 => '87',
      2 => '88',
      3 => '89',
    ),
    19 => 
    array (
      0 => '',
      1 => '90',
      2 => '91',
      3 => '92',
      4 => '93',
      5 => '94',
    ),
    20 => 
    array (
      0 => '',
      1 => '95',
      2 => '96',
      3 => '97',
      4 => '98',
    ),
    21 => 
    array (
      0 => '',
      1 => '99',
      2 => '100',
      3 => '101',
    ),
    22 => 
    array (
      0 => '',
      1 => '95',
      2 => '102',
      3 => '88',
      4 => '103',
      5 => '96',
    ),
    23 => 
    array (
      0 => '',
      1 => '104',
      2 => '105',
      3 => '106',
    ),
    24 => 
    array (
      0 => '',
      1 => '107',
      2 => '108',
    ),
    25 => 
    array (
      0 => '',
      1 => '109',
    ),
    26 => 
    array (
      0 => '',
      1 => '110',
      2 => '111',
      3 => '112',
      4 => '113',
      5 => '114',
      6 => '95',
      7 => '169',
      8 => '170',
    ),
    27 => 
    array (
      0 => '',
      1 => '110',
      2 => '115',
      3 => '116',
      4 => '95',
      5 => '112',
      6 => '117',
      7 => '113',
      8 => '118',
      9 => '119',
      10 => '120',
      11 => '122',
      12 => '170',
      13 => '169',
    ),
    28 => 
    array (
      0 => '',
      1 => '110',
      2 => '116',
      3 => '95',
      4 => '123',
      5 => '124',
    ),
    29 => 
    array (
      0 => '',
      1 => '70',
    ),
    30 => 
    array (
      0 => '',
      1 => '125',
      2 => '126',
      3 => '127',
    ),
    31 => 
    array (
      0 => '',
      1 => '66',
    ),
    32 => 
    array (
      0 => '',
      1 => '128',
      2 => '129',
    ),
    33 => 
    array (
      0 => '',
      1 => '130',
    ),
    34 => 
    array (
      0 => '',
      1 => '95',
      2 => '131',
      3 => '132',
      4 => '133',
      5 => '134',
      6 => '135',
    ),
    35 => 
    array (
      0 => '',
      1 => '136',
      2 => '137',
    ),
    36 => 
    array (
      0 => '',
      1 => '138',
    ),
    37 => 
    array (
      0 => '',
      1 => '139',
    ),
    38 => 
    array (
      0 => '',
      1 => '141',
      2 => '66',
    ),
    39 => 
    array (
      0 => '',
      1 => '142',
      2 => '143',
    ),
    40 => 
    array (
      0 => '',
      1 => '144',
      2 => '145',
      3 => '146',
      4 => '148',
      5 => '147',
    ),
    41 => 
    array (
      0 => '',
      1 => '59',
      2 => '149',
      3 => '150',
      4 => '151',
    ),
    42 => 
    array (
      0 => '',
      1 => '154',
      2 => '152',
      3 => '153',
      4 => '168',
    ),
    43 => 
    array (
      0 => '',
      1 => '155',
      2 => '156',
    ),
    44 => 
    array (
      0 => '',
      1 => '157',
      2 => '158',
      3 => '159',
    ),
    45 => 
    array (
      0 => '',
      1 => '19',
      2 => '21',
    ),
    46 => 
    array (
      0 => '',
      1 => '164',
      2 => '165',
      3 => '166',
      4 => '167',
    ),
    '' => 
    array (
      0 => '',
    ),
  ),
);

 ?>