<?php
// created: 2023-02-21 14:07:23
$viewdefs['Purchases']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'quicksearch_field' => 
  array (
    0 => 'name',
  ),
  'quicksearch_priority' => 2,
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'sasa_nombredenegocio_c' => 
    array (
    ),
    'sasa_tipotitular_c' => 
    array (
    ),
    'sasa_ejecutivocomercial_c' => 
    array (
    ),
    'sasa_estado_c' => 
    array (
    ),
    'sasa_estadonegocio_c' => 
    array (
    ),
    'sasa_codigonegocio_c' => 
    array (
    ),
    'sasa_asistentedegestion_c' => 
    array (
    ),
    'sasa_fechacreacion_c' => 
    array (
    ),
    'end_date' => 
    array (
    ),
    'sasa_fechacancelacion_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
  ),
);