<?php
$dictionary['Lead']['fields']['fbsg_ccintegrationlog_leads'] = array(
    'name' => 'fbsg_ccintegrationlog_leads',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_leads',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Lead"]["fields"]["prospect_list_leads"] = array(
    'name' => 'prospect_list_leads',
    'type' => 'link',
    'relationship' => 'prospect_list_leads',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Lead';
include('custom/include/fbsg_cc_newvars.php');
