<?php
$module_name = 'sasa_SaldosAF';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'accounts_sasa_saldosaf_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_ACCOUNTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_productosafav_sasa_saldosaf_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_nombrefondo_c',
                'label' => 'LBL_SASA_NOMBREFONDO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_nombresubcuenta_c',
                'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
              ),
              6 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
              ),
              7 => 
              array (
                'name' => 'sasa_inversiones_c',
                'label' => 'LBL_SASA_INVERSIONES_C',
              ),
              8 => 
              array (
                'name' => 'sasa_digito_c',
                'label' => 'LBL_SASA_DIGITO_C',
              ),
              9 => 
              array (
                'name' => 'sasa_plan_c',
                'label' => 'LBL_SASA_PLAN_C',
              ),
              10 => 
              array (
                'name' => 'sasa_numerounid_c',
                'label' => 'LBL_SASA_NUMEROUNID_C',
              ),
              11 => 
              array (
                'name' => 'sasa_valorunid_c',
                'label' => 'LBL_SASA_VALORUNID_C',
              ),
              12 => 
              array (
                'name' => 'sasa_saldoactual_c',
                'label' => 'LBL_SASA_SALDOACTUAL_C',
              ),
              13 => 
              array (
                'name' => 'sasa_fechasaldo_c',
                'label' => 'LBL_SASA_FECHASALDO_C',
              ),
              14 => 
              array (
                'name' => 'sasa_ingresosdia_c',
                'label' => 'LBL_SASA_INGRESOSDIA_C',
              ),
              15 => 
              array (
                'name' => 'sasa_egresosdia_c',
                'label' => 'LBL_SASA_EGRESOSDIA_C',
              ),
              16 => 
              array (
                'name' => 'sasa_canje_c',
                'label' => 'LBL_SASA_CANJE_C',
              ),
              17 => 
              array (
                'name' => 'sasa_retcont_c',
                'label' => 'LBL_SASA_RETCONT_C',
              ),
              18 => 
              array (
                'name' => 'sasa_retrend_c',
                'label' => 'LBL_SASA_RETREND_C',
              ),
              19 => 
              array (
                'name' => 'sasa_ctrlcancel_c',
                'label' => 'LBL_SASA_CTRLCANCEL_C',
              ),
              20 => 
              array (
                'name' => 'sasa_reservado_c',
                'label' => 'LBL_SASA_RESERVADO_C',
              ),
              21 => 
              array (
                'name' => 'sasa_4x1000_c',
                'label' => 'LBL_SASA_4X1000_C',
              ),
              22 => 
              array (
                'name' => 'sasa_sinconsol_c',
                'label' => 'LBL_SASA_SINCONSOL_C',
              ),
              23 => 
              array (
                'name' => 'sasa_maximoretiro_c',
                'label' => 'LBL_SASA_MAXIMORETIRO_C',
              ),
              24 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              25 => 
              array (
                'name' => 'tag',
                'studio' => 
                array (
                  'portal' => false,
                  'base' => 
                  array (
                    'popuplist' => false,
                  ),
                  'mobile' => 
                  array (
                    'wirelesseditview' => true,
                    'wirelessdetailview' => true,
                  ),
                ),
                'label' => 'LBL_TAGS',
              ),
              26 => 'assigned_user_name',
              27 => 'team_name',
              28 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              29 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              30 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              31 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
