<?php

$dependencies['Leads']['set_value_nid'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'and(equal($sasa_tipopersona_c,"Juridica"),greaterThan(strlen($sasa_nroidentificacion_c),9))',
    'triggerFields' => array('sasa_tipopersona_c','sasa_nroidentificacion_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
        array(
            'name' => 'SetValue',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'sasa_nroidentificacion_c',
                'value' => '',
            )
        )
    )
);



