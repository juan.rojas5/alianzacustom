<?php
// created: 2018-08-08 18:12:13
$viewdefs['Opportunities']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'amount' => 
    array (
    ),
    'lead_source' => 
    array (
    ),
    'opportunity_type' => 
    array (
    ),
    'date_closed' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'tag' => 
    array (
    ),
    'sasa_superior1_c' => 
    array (
    ),
    'sasa_superior2_c' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'sales_status' => 
    array (
    ),
  ),
);