<?php
 // created: 2018-06-12 16:11:52
$dictionary['Account']['fields']['phone_fax']['len']='100';
$dictionary['Account']['fields']['phone_fax']['audited']=false;
$dictionary['Account']['fields']['phone_fax']['massupdate']=false;
$dictionary['Account']['fields']['phone_fax']['comments']='The fax phone number of this company';
$dictionary['Account']['fields']['phone_fax']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_fax']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['phone_fax']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_fax']['reportable']=false;
$dictionary['Account']['fields']['phone_fax']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.04',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_fax']['calculated']=false;

 ?>