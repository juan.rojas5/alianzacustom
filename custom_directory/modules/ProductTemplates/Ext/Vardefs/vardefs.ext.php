<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ProductTemplates/Ext/Vardefs/sugarfield_sasa_porcentajecomosion_c.php

 // created: 2018-06-25 14:28:29
$dictionary['ProductTemplate']['fields']['sasa_porcentajecomosion_c']['labelValue']='% Comisión';
$dictionary['ProductTemplate']['fields']['sasa_porcentajecomosion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['ProductTemplate']['fields']['sasa_porcentajecomosion_c']['enforced']='';
$dictionary['ProductTemplate']['fields']['sasa_porcentajecomosion_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/ProductTemplates/Ext/Vardefs/sugarfield_list_price.php

 // created: 2018-07-07 16:02:33
$dictionary['ProductTemplate']['fields']['list_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['list_price']['len']=26;
$dictionary['ProductTemplate']['fields']['list_price']['audited']=false;
$dictionary['ProductTemplate']['fields']['list_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['list_price']['comments']='List price of product ("List" in Quote)';
$dictionary['ProductTemplate']['fields']['list_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['list_price']['duplicate_merge']='enabled';
$dictionary['ProductTemplate']['fields']['list_price']['duplicate_merge_dom_value']='1';
$dictionary['ProductTemplate']['fields']['list_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['list_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['list_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['list_price']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ProductTemplates/Ext/Vardefs/sugarfield_cost_price.php

 // created: 2018-07-07 16:03:43
$dictionary['ProductTemplate']['fields']['cost_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['cost_price']['len']=26;
$dictionary['ProductTemplate']['fields']['cost_price']['audited']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['comments']='Product cost ("Cost" in Quote)';
$dictionary['ProductTemplate']['fields']['cost_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['cost_price']['duplicate_merge']='enabled';
$dictionary['ProductTemplate']['fields']['cost_price']['duplicate_merge_dom_value']='1';
$dictionary['ProductTemplate']['fields']['cost_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['cost_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['cost_price']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ProductTemplates/Ext/Vardefs/sugarfield_discount_price.php

 // created: 2018-07-07 16:22:38
$dictionary['ProductTemplate']['fields']['discount_price']['default']=0.0;
$dictionary['ProductTemplate']['fields']['discount_price']['len']=26;
$dictionary['ProductTemplate']['fields']['discount_price']['audited']=true;
$dictionary['ProductTemplate']['fields']['discount_price']['massupdate']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['comments']='Discounted price ("Unit Price" in Quote)';
$dictionary['ProductTemplate']['fields']['discount_price']['importable']='true';
$dictionary['ProductTemplate']['fields']['discount_price']['duplicate_merge']='disabled';
$dictionary['ProductTemplate']['fields']['discount_price']['duplicate_merge_dom_value']='0';
$dictionary['ProductTemplate']['fields']['discount_price']['merge_filter']='disabled';
$dictionary['ProductTemplate']['fields']['discount_price']['unified_search']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['calculated']=false;
$dictionary['ProductTemplate']['fields']['discount_price']['enable_range_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/ProductTemplates/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['ProductTemplate']['full_text_search']=false;

?>
