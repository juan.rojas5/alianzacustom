<?php
// created: 2023-02-17 20:29:36
$dictionary["sasa2_Unidades_de_negocio"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'bean_name' => 'sasa1_Unidades_de_negocio_por_',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link-type' => 'many',
  'side' => 'left',
);
