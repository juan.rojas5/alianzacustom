<?php
 // created: 2019-01-04 21:28:46
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasa_movimientosav_1'] = array (
  'order' => 100,
  'module' => 'sasa_MovimientosAV',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasa_movimientosav_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
