<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Include/fbsg_cci.php


$bwcModules[] = 'fbsg_ConstantContactIntegration';

?>
<?php
// Merged from custom/Extension/application/Ext/Include/ConstantContactIntegration.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$beanFiles['fbsg_ConstantContactIntegration'] = 'modules/fbsg_ConstantContactIntegration/fbsg_ConstantContactIntegration.php';
$modules_exempt_from_availability_check['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$report_include_modules['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$modInvisList[] = 'fbsg_ConstantContactIntegration';
$beanList['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$beanFiles['fbsg_CCIErrors'] = 'modules/fbsg_CCIErrors/fbsg_CCIErrors.php';
$modules_exempt_from_availability_check['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$report_include_modules['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$modInvisList[] = 'fbsg_CCIErrors';
$beanList['fbsg_CCIntegrationLog'] = 'fbsg_CCIntegrationLog';
$beanFiles['fbsg_CCIntegrationLog'] = 'modules/fbsg_CCIntegrationLog/fbsg_CCIntegrationLog.php';
$moduleList[] = 'fbsg_CCIntegrationLog';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/rli_unhide.ext.php

$moduleList[] = 'RevenueLineItems';
if (isset($modInvisList) && is_array($modInvisList)) {
    foreach ($modInvisList as $key => $mod) {
        if ($mod === 'RevenueLineItems') {
            unset($modInvisList[$key]);
        }
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/Include/SaldosyMovimientosAFV.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasa_SaldosAV'] = 'sasa_SaldosAV';
$beanFiles['sasa_SaldosAV'] = 'modules/sasa_SaldosAV/sasa_SaldosAV.php';
$moduleList[] = 'sasa_SaldosAV';
$beanList['sasa_MovimientosAV'] = 'sasa_MovimientosAV';
$beanFiles['sasa_MovimientosAV'] = 'modules/sasa_MovimientosAV/sasa_MovimientosAV.php';
$moduleList[] = 'sasa_MovimientosAV';
$beanList['sasa_MovimientosAF'] = 'sasa_MovimientosAF';
$beanFiles['sasa_MovimientosAF'] = 'modules/sasa_MovimientosAF/sasa_MovimientosAF.php';
$moduleList[] = 'sasa_MovimientosAF';
$beanList['sasa_SaldosAF'] = 'sasa_SaldosAF';
$beanFiles['sasa_SaldosAF'] = 'modules/sasa_SaldosAF/sasa_SaldosAF.php';
$moduleList[] = 'sasa_SaldosAF';
$beanList['sasa_MovimientosAVDivisas'] = 'sasa_MovimientosAVDivisas';
$beanFiles['sasa_MovimientosAVDivisas'] = 'modules/sasa_MovimientosAVDivisas/sasa_MovimientosAVDivisas.php';
$moduleList[] = 'sasa_MovimientosAVDivisas';
$beanList['sasa_Categorias'] = 'sasa_Categorias';
$beanFiles['sasa_Categorias'] = 'modules/sasa_Categorias/sasa_Categorias.php';
$moduleList[] = 'sasa_Categorias';
$beanList['sasa_ProductosAFAV'] = 'sasa_ProductosAFAV';
$beanFiles['sasa_ProductosAFAV'] = 'modules/sasa_ProductosAFAV/sasa_ProductosAFAV.php';
$moduleList[] = 'sasa_ProductosAFAV';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/SaldosConsolidados.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasaS_SaldosConsolidados'] = 'sasaS_SaldosConsolidados';
$beanFiles['sasaS_SaldosConsolidados'] = 'modules/sasaS_SaldosConsolidados/sasaS_SaldosConsolidados.php';
$moduleList[] = 'sasaS_SaldosConsolidados';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/PresupuestosxIngresos.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasaP_PresupuestoxProducto'] = 'sasaP_PresupuestoxProducto';
$beanFiles['sasaP_PresupuestoxProducto'] = 'modules/sasaP_PresupuestoxProducto/sasaP_PresupuestoxProducto.php';
$moduleList[] = 'sasaP_PresupuestoxProducto';
$beanList['sasaP_PresupuestosxAsesor'] = 'sasaP_PresupuestosxAsesor';
$beanFiles['sasaP_PresupuestosxAsesor'] = 'modules/sasaP_PresupuestosxAsesor/sasaP_PresupuestosxAsesor.php';
$moduleList[] = 'sasaP_PresupuestosxAsesor';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/sasa_tipificaciones.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasa_sasa_tipificaciones'] = 'sasa_sasa_tipificaciones';
$beanFiles['sasa_sasa_tipificaciones'] = 'modules/sasa_sasa_tipificaciones/sasa_sasa_tipificaciones.php';
$moduleList[] = 'sasa_sasa_tipificaciones';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/PQRsHistorico.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasa_pqrs_historico'] = 'sasa_pqrs_historico';
$beanFiles['sasa_pqrs_historico'] = 'modules/sasa_pqrs_historico/sasa_pqrs_historico.php';
$moduleList[] = 'sasa_pqrs_historico';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Unidades_de_negocio_por_cliente.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasa1_Unidades_de_negocio_por_'] = 'sasa1_Unidades_de_negocio_por_';
$beanFiles['sasa1_Unidades_de_negocio_por_'] = 'modules/sasa1_Unidades_de_negocio_por_/sasa1_Unidades_de_negocio_por_.php';
$moduleList[] = 'sasa1_Unidades_de_negocio_por_';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Unidades_de_negocio.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['sasa2_Unidades_de_negocio'] = 'sasa2_Unidades_de_negocio';
$beanFiles['sasa2_Unidades_de_negocio'] = 'modules/sasa2_Unidades_de_negocio/sasa2_Unidades_de_negocio.php';
$moduleList[] = 'sasa2_Unidades_de_negocio';


?>
