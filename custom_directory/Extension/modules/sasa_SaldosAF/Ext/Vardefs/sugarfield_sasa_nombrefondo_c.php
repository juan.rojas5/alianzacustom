<?php
 // created: 2018-11-21 20:58:25
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['importable']='false';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['calculated']='true';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['formula']='related($sasa_productosafav_sasa_saldosaf_1,"sasa_producto_c")';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['enforced']=true;

 ?>