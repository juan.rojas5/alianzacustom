<?php
$viewdefs['Leads']['base']['view']['recordlist']['rowactions']['actions']=array(
	array(
		'type' => 'rowaction',
		'css_class' => 'btn',
		'tooltip' => 'LBL_PREVIEW',
		'event' => 'list:preview:fire',
		'icon' => 'fa-eye',
		'acl_action' => 'view',
	),
	array(
		'type' => 'rowaction',
		'name' => 'edit_button',
		'icon' => 'fa-pencil',
		'label' => 'LBL_EDIT_BUTTON',
		'event' => 'list:editrow:fire',
		'acl_action' => 'edit',
	),
	array(
		'type' => 'follow',
		'name' => 'follow_button',
		'event' => 'list:follow:fire',
		'acl_action' => 'view',
	),
	array(
		'type' => 'rowaction',
		'name' => 'delete_button',
		'icon' => 'fa-trash-o',
		'event' => 'list:deleterow:fire',
		'label' => 'LBL_DELETE_BUTTON',
		'acl_action' => 'delete',
	),
	/*
	array(
		'type' => 'convertbutton',
		'name' => 'lead_convert_button',
		'label' => 'LBL_CONVERT_BUTTON_LABEL',
		'acl_action' => 'edit',
	),
	*/
);
