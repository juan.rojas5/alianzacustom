<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_moduleList.php

 //created: 2018-01-29 21:13:46

$app_list_strings['moduleList']['RevenueLineItems']='Artículos de Línea de Ganancia';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_parent_type_display.php

 // created: 2018-01-29 21:13:46

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Oportunidad',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Errores',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display.php

 // created: 2018-01-29 21:13:46

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cuenta',
  'Opportunities' => 'Oportunidad',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'Prospects' => 'Público Objetivo',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display_notes.php

 // created: 2018-01-29 21:13:46

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Oportunidad',
  'Tasks' => 'Tarea',
  'ProductTemplates' => 'Catálogo de Productos',
  'Quotes' => 'Cotizacion',
  'Products' => 'Partida Individual Cotizada',
  'Contracts' => 'Contrato',
  'Emails' => 'Correo Electrónico',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Meetings' => 'Reunión',
  'Calls' => 'Llamada',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_salutation_list.php

 // created: 2018-05-22 20:52:36

$app_list_strings['salutation_list']=array (
  '' => '',
  'Sr' => 'Sr.',
  'Sra' => 'Sra.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_status_list.php

 // created: 2018-05-22 21:49:19

$app_list_strings['status_list']=array (
  '' => '',
  'Nuevo' => 'Nuevo',
  'Libre' => 'Libre',
  'Asignado' => 'Asignado',
  'Reciclado' => 'Reciclado',
  'Convertido' => 'Convertido',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_cc_list_dom.php

 //created: 2018-08-10 23:19:22

$app_list_strings['cc_list_dom']['1599638647']='Distribución Rendimientos 2';
$app_list_strings['cc_list_dom']['1084171821']='Distribución Rendimientos 6';
$app_list_strings['cc_list_dom']['1353411765']='Distribución Rendimientos 8';
$app_list_strings['cc_list_dom']['1793310507']='BOLETÍN 2';
$app_list_strings['cc_list_dom']['1294481516']='BOLETÍN 1';
$app_list_strings['cc_list_dom']['1123085133']='Distribución Rendimientos 9';
$app_list_strings['cc_list_dom']['1663554654']='Distribución Rendimientos 3';
$app_list_strings['cc_list_dom']['1229922910']='BOLETÍN 5';
$app_list_strings['cc_list_dom']['1025596470']='TODOS AV-AF (JUNIO)';
$app_list_strings['cc_list_dom']['1567967628']='Distribución de Rendimientos 4';
$app_list_strings['cc_list_dom']['1227188177']='Distribución Rendimientos 10';
$app_list_strings['cc_list_dom']['1363018222']='BOLETÍN 4';
$app_list_strings['cc_list_dom']['1567402521']='BOLETÍN 3';
$app_list_strings['cc_list_dom']['1191530132']='Distribución Rendimientos 7';
$app_list_strings['cc_list_dom']['1121327165']='Distribución Rendimientos 5';
$app_list_strings['cc_list_dom']['2003796571']='Distribución Rendimientos 1';
$app_list_strings['cc_list_dom']['1763210728']='PRUEBA CXC';
$app_list_strings['cc_list_dom']['1119624345']='Comunicado Fondo CXC';
$app_list_strings['cc_list_dom']['1186151834']='BOLETÍN 6';
$app_list_strings['cc_list_dom']['1427111953']='Comunicado Fondo CXC 2';
$app_list_strings['cc_list_dom']['1880699397']='BOLETÍN 7';
$app_list_strings['cc_list_dom']['1982935136']='BOGOTA AV';
$app_list_strings['cc_list_dom']['1875240973']='RENTA FIJA';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sasa_tipificaciones.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_sasa_tipificaciones'] = 'Tipificaciones';
$app_list_strings['moduleListSingular']['sasa_sasa_tipificaciones'] = 'Tipificación';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_moduleIconList.php

// created: 2023-02-03 09:39:51
$app_list_strings['moduleIconList']['Home'] = 'In';
$app_list_strings['moduleIconList']['Contacts'] = 'Co';
$app_list_strings['moduleIconList']['Accounts'] = 'Cu';
$app_list_strings['moduleIconList']['Opportunities'] = 'Op';
$app_list_strings['moduleIconList']['Cases'] = 'Ca';
$app_list_strings['moduleIconList']['Notes'] = 'No';
$app_list_strings['moduleIconList']['Calls'] = 'Ll';
$app_list_strings['moduleIconList']['Emails'] = 'CE';
$app_list_strings['moduleIconList']['Meetings'] = 'Re';
$app_list_strings['moduleIconList']['Tasks'] = 'Ta';
$app_list_strings['moduleIconList']['Calendar'] = 'Ca';
$app_list_strings['moduleIconList']['Leads'] = 'CP';
$app_list_strings['moduleIconList']['Currencies'] = 'Mo';
$app_list_strings['moduleIconList']['Contracts'] = 'Co';
$app_list_strings['moduleIconList']['Quotes'] = 'Co';
$app_list_strings['moduleIconList']['Products'] = 'PI';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'WL';
$app_list_strings['moduleIconList']['ProductCategories'] = 'Cd';
$app_list_strings['moduleIconList']['ProductTypes'] = 'Td';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'Cd';
$app_list_strings['moduleIconList']['ProductBundles'] = 'Pd';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'Nd';
$app_list_strings['moduleIconList']['Reports'] = 'In';
$app_list_strings['moduleIconList']['Forecasts'] = 'Pr';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'Hd';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'Hd';
$app_list_strings['moduleIconList']['Quotas'] = 'Co';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'Cv';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'Cd';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Eq';
$app_list_strings['moduleIconList']['TeamNotices'] = 'Nd';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Fa';
$app_list_strings['moduleIconList']['Activities'] = 'Ac';
$app_list_strings['moduleIconList']['Comments'] = 'Co';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Su';
$app_list_strings['moduleIconList']['Bugs'] = 'Er';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'MS';
$app_list_strings['moduleIconList']['TimePeriods'] = 'Pd';
$app_list_strings['moduleIconList']['TaxRates'] = 'Im';
$app_list_strings['moduleIconList']['ContractTypes'] = 'Td';
$app_list_strings['moduleIconList']['Schedulers'] = 'Pl';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'Td';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ca';
$app_list_strings['moduleIconList']['CampaignLog'] = 'Rd';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'Sd';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'Rd';
$app_list_strings['moduleIconList']['Connectors'] = 'Co';
$app_list_strings['moduleIconList']['Notifications'] = 'No';
$app_list_strings['moduleIconList']['Sync'] = 'Si';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'Ue';
$app_list_strings['moduleIconList']['ReportMaker'] = 'IA';
$app_list_strings['moduleIconList']['DataSets'] = 'Fd';
$app_list_strings['moduleIconList']['CustomQueries'] = 'CP';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'Dd';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'Pd';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'Cd';
$app_list_strings['moduleIconList']['Shifts'] = 'Tu';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'Ed';
$app_list_strings['moduleIconList']['Purchases'] = 'Ad';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'PI';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Di';
$app_list_strings['moduleIconList']['PushNotifications'] = 'No';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'Cd';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'Rd';
$app_list_strings['moduleIconList']['WorkFlow'] = 'Dd';
$app_list_strings['moduleIconList']['EAPM'] = 'Ce';
$app_list_strings['moduleIconList']['Worksheet'] = 'Hd';
$app_list_strings['moduleIconList']['Users'] = 'Us';
$app_list_strings['moduleIconList']['Employees'] = 'Em';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'CE';
$app_list_strings['moduleIconList']['Releases'] = 'Pu';
$app_list_strings['moduleIconList']['Prospects'] = 'PO';
$app_list_strings['moduleIconList']['Queues'] = 'Co';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'Mp';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['SNIP'] = 'Ad';
$app_list_strings['moduleIconList']['ProspectLists'] = 'Ld';
$app_list_strings['moduleIconList']['SavedSearch'] = 'BG';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'Ad';
$app_list_strings['moduleIconList']['Trackers'] = 'Se';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'Rd';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'Sd';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'Cd';
$app_list_strings['moduleIconList']['FAQ'] = 'PF';
$app_list_strings['moduleIconList']['Newsletters'] = 'BI';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'AP';
$app_list_strings['moduleIconList']['DataArchiver'] = 'Ad';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'AE';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'Cd';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'TO';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'Fd';
$app_list_strings['moduleIconList']['Shippers'] = 'Pd';
$app_list_strings['moduleIconList']['Styleguide'] = 'Gd';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Re';
$app_list_strings['moduleIconList']['Tags'] = 'Et';
$app_list_strings['moduleIconList']['Categories'] = 'Ca';
$app_list_strings['moduleIconList']['Dashboards'] = 'Ta';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'Od';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'Pd';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'Pd';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'Pd';
$app_list_strings['moduleIconList']['CommentLog'] = 'Rd';
$app_list_strings['moduleIconList']['Holidays'] = 'DF';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'Ct';
$app_list_strings['moduleIconList']['Metrics'] = 'Mé';
$app_list_strings['moduleIconList']['Messages'] = 'Me';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'Ad';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'Sd';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'Gi';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'Fd';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'Wd';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'Ad';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'Pd';
$app_list_strings['moduleIconList']['Library'] = 'Bi';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'Dd';
$app_list_strings['moduleIconList']['Words'] = 'Pa';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'Bd';
$app_list_strings['moduleIconList']['KBContents'] = 'Bd';
$app_list_strings['moduleIconList']['KBArticles'] = 'Ad';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'Pd';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'AI';
$app_list_strings['moduleIconList']['fbsg_ConstantContactIntegration'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_TableTest'] = 'Ta';
$app_list_strings['moduleIconList']['fbsg_test'] = 'CE';
$app_list_strings['moduleIconList']['fbsg_CCErrors'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIErrors'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIContactLog'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIntegrationLog'] = 'CC';
$app_list_strings['moduleIconList']['sasa_SaldosAF'] = 'SA';
$app_list_strings['moduleIconList']['sasa_SaldosAV'] = 'SA';
$app_list_strings['moduleIconList']['sasa_MovimientosAF'] = 'MA';
$app_list_strings['moduleIconList']['sasa_MovimientosAV'] = 'MA';
$app_list_strings['moduleIconList']['sasa_MovimientosAVDivisas'] = 'MA';
$app_list_strings['moduleIconList']['sasa_ProductosAFAV'] = 'PA';
$app_list_strings['moduleIconList']['sasa_Categorias'] = 'Ca';
$app_list_strings['moduleIconList']['sasaS_SaldosConsolidados'] = 'SC';
$app_list_strings['moduleIconList']['sasaP_PresupuestosxAsesor'] = 'Px';
$app_list_strings['moduleIconList']['sasaP_PresupuestoxProducto'] = 'Px';
$app_list_strings['moduleIconList']['sasa_sasa_tipificaciones'] = 'Ti';
$app_list_strings['moduleIconList']['sasa_pqrs_historico'] = 'PH';
$app_list_strings['moduleIconList']['sasa1_Unidades_de_negocio_por_'] = 'Ud';
$app_list_strings['moduleIconList']['sasa2_Unidades_de_negocio'] = 'Ud';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['Roles'] = 'Ro';

?>
