<?php
// created: 2019-03-08 18:57:52
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestoxProducto',
  'bean_name' => 'sasaP_PresupuestoxProducto',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);
