<?php
 // created: 2018-09-12 21:53:48
$dictionary['Contact']['fields']['phone_work']['len']='100';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.08',
  'searchable' => false,
);
$dictionary['Contact']['fields']['phone_work']['calculated']=false;
$dictionary['Contact']['fields']['phone_work']['audited']=false;
$dictionary['Contact']['fields']['phone_work']['importable']='false';
$dictionary['Contact']['fields']['phone_work']['reportable']=false;
$dictionary['Contact']['fields']['phone_work']['pii']=false;

 ?>