<?php
// created: 2018-06-17 22:52:58
$viewdefs['Contacts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'first_name' => 
    array (
    ),
    'last_name' => 
    array (
    ),
    'sasa_pais_c' => 
    array (
    ),
    'sasa_departamento_c' => 
    array (
    ),
    'sasa_municipio_c' => 
    array (
    ),
    'sasa_tipoidentificacion_c' => 
    array (
    ),
    'sasa_nroidentificacion_c' => 
    array (
    ),
    'title' => 
    array (
    ),
    'sasa_relacioncuenta_c' => 
    array (
    ),
    'account_name' => 
    array (
    ),
    'lead_source' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'tag' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'assigned_user_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);