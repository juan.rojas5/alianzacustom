<?php

	if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
	require_once './modules/Administration/Administration.php';
	require_once 'data/SugarBean.php';
	require_once("clients/base/api/ModuleApi.php");

	
	global $db;
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<br>

<h2 style="text-align: center;">
	<strong>Parameters Advanced Instance</strong>
</h2>
<p style="text-align: center; color: blue;">
	<strong>Development in Process!</strong>
</p>
<hr>

<form action="index.php?entryPoint=PAI" method="POST" name="CustomForm" id="form">
	<div style="text-align: center;">
		<table style="width: 100%;" border="0" cellspacing="2">
			<tbody>
				<tr>
					<td width="5%"></td>
					<td width="90%">
						<table style="width: 100%;" border="1" cellspacing="2">
							<tbody>
								<tr bgcolor="#E9ECEF" style="text-align: center;">
									<td width="10%"><strong>Name</strong></td>
									<td width="80%"><strong>Value</strong></td>
									<td width="5%"><strong>Edit</strong></td>
									<td width="5%"><strong>Deleted</strong></td>
								</tr>

<?php

	$query = "SELECT * FROM config WHERE category = 'PAI' ORDER BY name ASC;";
	$result = $db->query($query);

	$i = 0;
	while ($row = $db->fetchByAssoc($result)){
		$category = $row['category'];
		$name = $row['name'];
		$value = $row['value'];

		$htmlscd = htmlspecialchars_decode($value);

		$i++;

		echo '
								<tr style="text-align: center;">
									<td>
										<input type="text" class="form-control" id="'.$name.$i.'n" name="'.$name.$i.'n" value="'.$name.'" readonly="true" style="text-align: center;">
									</td>
									<td>
										<input type="text" class="form-control" id="'.$name.$i.'v" name="'.$name.$i.'v" value="'.$value.'" readonly="true">
									</td>
									<td>
										<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#'.$name.$i.'" id="EditButton'.$name.$i.'" name="EditButton">&#x02710;</button>
										<div class="modal fade" id="'.$name.$i.'" tabindex="-1" role="dialog" aria-labelledby="'.$name.$i.'Label" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="'.$name.$i.'Label">
															<strong>Edit: '.$name.'</strong>
														</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<form>
															<div class="form-group">
																<label for="recipient-name" class="col-form-label">Nombre:</label>
																<input type="text" class="form-control" id="recipient-name" value="'.$name.'" readonly="true" style="text-align: center;">
															</div>
															<div class="form-group">
																<label for="message-text" class="col-form-label">Value:</label>
																<input id="json'.$name.'" type="text" class="form-control" value="'.$value.'"></input>
															</div>
														</form>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
														<button id="SaveButton'.$name.$i.'" class="btn btn-primary" type="submit" name="SaveButton" onclick="editConfig(\''.$name.'\')">Save</button>
													</div>
												</div>
											</div>
										</div>
									</td>
									<td>
										<button id="DeleteButton'.$name.$i.'" class="btn btn-outline-danger" type="submit" name="DeleteButton" onclick="deleteConfig(\''.$name.'\')">&#x02717;</button>
									</td>
								</tr>
		';
	}

?>
							</tbody>
						</table>
					</td>
					<td width="5%"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<hr>
	<div style="text-align: center;">
		<button id="idNew" class="btn btn-primary" type="button" data-toggle="modal" data-target="#newrecord" name="NewButton" >New</button>
		<div class="modal fade" id="newrecord" tabindex="-1" role="dialog" aria-labelledby="newrecordLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="newrecordLabel">
							<strong>New</strong>
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Nombre:</label>
								<input type="text" class="form-control" id="newname" name="newname" value="" style="text-align: center;">
							</div>
							<div class="form-group">
								<label for="message-text" class="col-form-label">Value:</label>
								<input type="text" class="form-control" id="newvalue" name="newvalue" value=""></input>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-secondary">Cancel</button>
						<button type="submit" class="btn btn-primary" onclick="newConfig(newname,newvalue)">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		console.log("Esta listo el DOM", this.App);
	});
	
	function newConfig(newname,newvalue) {
		var newname = $('#newname').val();
		var newvalue = $('#newvalue').val();
		$.ajax({
			url: './rest/v11/PAIEndpoint',
			type:'post',
			data: {
				method: "post",
				name: newname,
				value: newvalue
			},
			success: function(response) {
				console.log(response);
				alert(response['message']);
			},
			error: function(error) {
				console.log("No se ha podido obtener la información1");
			},
			cache: false
		});
	}

	function editConfig(name) {
		var json = $('#json'+name).val();
		//console.log(json);
		$.ajax({
			url: './rest/v11/PAIEndpoint',
			type: 'post',
			data: {
				method: "update",
				name: name,
				value: json
			},
			success: function(response) {
				console.log(response);
				alert(response['message']);
			},
			error: function(error) {
				console.log("No se ha podido obtener la información2");
			},
			cache: false
		});
	}

	function deleteConfig(name) {
		$.ajax({
			url: './rest/v11/PAIEndpoint',
			type: 'post',
			data: {
				method: "delete",
				name: name
			},
			success: function(response) {
				console.log(response);
				alert(response['message']);
			},
			error: function(error) {
				console.log("No se ha podido obtener la información3");
			},
			cache: false
		});
	}

</script>


