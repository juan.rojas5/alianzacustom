<?php
 // created: 2019-03-20 09:02:14
$dictionary['Contact']['fields']['primary_address_city']['required']=true;
$dictionary['Contact']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_city']['calculated']=false;

 ?>