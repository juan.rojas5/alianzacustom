<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sasa_saldosav_after_save_class
{
  function after_save($bean, $event, $arguments)
  {
    global $current_user, $db;
    
    //loop prevention check
    if (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false)
    {
      $bean->ignore_update_c = true;
      
      $ProductoAFAV = BeanFactory::retrieveBean('sasa_ProductosAFAV', $bean->sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida);
      $Account = BeanFactory::retrieveBean('Accounts', $bean->accounts_sasa_saldosav_1accounts_ida);
      $idProducto = $ProductoAFAV->id;
      $idAccount = $Account->id;
      $idUserAccount = $Account->assigned_user_id;
      
      $yearSaldo = date("Y", strtotime($bean->sasa_fecharegistro_c));
      $monthSaldo = date("m", strtotime($bean->sasa_fecharegistro_c));
      
/******Old******

      $query = "
SELECT
    SUM(saldo_total) AS saldoTotal,
    nameAccounts,
    idpxp
FROM
    (
    SELECT
        users.id AS idUsuarioCuenta,
        sasa_productosafav.id AS idProducto,
        users.user_name AS usuarioCuenta,
        sasa_productosafav.name AS producto,
        sasa_saldosav.name AS namesaldoav,
        accounts.name AS nameAccounts,
        sasap_presupuestoxproducto.id AS idpxp,
        SUBSTRING_INDEX(
            GROUP_CONCAT(
                sasa_saldosav.sasa_saldototal_c
            ORDER BY
                sasa_saldosav.sasa_fecharegistro_c
            DESC
            ),
            ',',
            1
        ) AS saldo_total,
        SUBSTRING_INDEX(
            GROUP_CONCAT(
                sasa_saldosav.sasa_fecharegistro_c
            ORDER BY
                sasa_saldosav.sasa_fecharegistro_c
            DESC
            ),
            ',',
            1
        ) AS fecha_registro
    FROM
        sasa_saldosav
    LEFT JOIN accounts_sasa_saldosav_1_c ON sasa_saldosav.id = accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb
    LEFT JOIN accounts ON accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida = accounts.id
    LEFT JOIN users ON users.id = accounts.assigned_user_id
    LEFT JOIN sasa_productosafav_sasa_saldosav_1_c ON sasa_saldosav.id = sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb
    LEFT JOIN sasa_productosafav ON sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida = sasa_productosafav.id
    LEFT JOIN sasa_productosafav_sasap_presupuestoxproducto_1_c ON sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produff64tosafav_ida = sasa_productosafav.id
    LEFT JOIN sasap_presupuestoxproducto ON sasap_presupuestoxproducto.id = sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produ6f56roducto_idb
    LEFT JOIN sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c ON sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres4d00roducto_idb = sasap_presupuestoxproducto.id
    LEFT JOIN sasap_presupuestosxasesor ON sasap_presupuestosxasesor.id = sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres107bxasesor_ida
    WHERE
        sasa_saldosav.deleted = 0 AND
        accounts_sasa_saldosav_1_c.deleted = 0 AND
        sasa_productosafav_sasa_saldosav_1_c.deleted = 0 AND
        accounts.deleted = 0 AND
        sasap_presupuestoxproducto.deleted = 0 AND
        sasa_productosafav.deleted = 0 AND
        sasa_saldosav.sasa_fecharegistro_c LIKE '{$yearSaldo}-{$monthSaldo}%' AND
        sasa_productosafav.id = '{$idProducto}' AND
        users.id = '{$idUserAccount}' AND
        sasap_presupuestoxproducto.assigned_user_id = '{$idUserAccount}' AND
        sasap_presupuestosxasesor.sasa_year_c = '{$yearSaldo}' AND
        sasap_presupuestosxasesor.sasa_mes_c = '{$monthSaldo}'
    GROUP BY
        namesaldoav
    ORDER BY
        idUsuarioCuenta,
        idProducto,
        namesaldoav
    DESC
) tbl_saldos GROUP BY idUsuarioCuenta, idProducto;
      ";
      
******Old******/

      $query = "
        SELECT
            idpxp,
            SUM(saldoActual) AS saldoActual
        FROM
            (
            SELECT
                sasa_saldosav.id,
                sasa_saldosav.name AS nameSaldo,
                sasa_saldosav.sasa_saldototal_c,
                sasa_saldosav.sasa_fecharegistro_c,
                accounts.name AS Cuenta,
                users.user_name AS Usuario,
                sasa_productosafav.name AS Producto,
                sasap_presupuestoxproducto.id AS idpxp,
                AVG(sasa_saldototal_c) AS saldoActual
            FROM
                sasa_saldosav
            LEFT JOIN accounts_sasa_saldosav_1_c ON accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id
            LEFT JOIN accounts ON accounts.id = accounts_sasa_saldosav_1_c.accounts_sasa_saldosav_1accounts_ida
            LEFT JOIN users ON users.id = accounts.assigned_user_id
            LEFT JOIN sasa_productosafav_sasa_saldosav_1_c ON sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb = sasa_saldosav.id
            LEFT JOIN sasa_productosafav ON sasa_productosafav.id = sasa_productosafav_sasa_saldosav_1_c.sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida
            LEFT JOIN sasa_productosafav_sasap_presupuestoxproducto_1_c ON sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produff64tosafav_ida = sasa_productosafav.id
            LEFT JOIN sasap_presupuestoxproducto ON sasap_presupuestoxproducto.id = sasa_productosafav_sasap_presupuestoxproducto_1_c.sasa_produ6f56roducto_idb
            LEFT JOIN sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c ON sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres4d00roducto_idb = sasap_presupuestoxproducto.id
            LEFT JOIN sasap_presupuestosxasesor ON sasap_presupuestosxasesor.id = sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.sasap_pres107bxasesor_ida
            WHERE
                sasa_saldosav.deleted = 0 AND
                accounts_sasa_saldosav_1_c.deleted = 0 AND
                accounts.deleted = 0 AND
                users.deleted = 0 AND
                sasa_productosafav_sasa_saldosav_1_c.deleted = 0 AND
                sasa_productosafav.deleted = 0 AND
                sasa_productosafav_sasap_presupuestoxproducto_1_c.deleted = 0 AND
                sasap_presupuestoxproducto.deleted = 0 AND
                sasap_presupuestosxasesor.deleted = 0 AND
                sasap_presupuestosxasesor_sasap_presupuestoxproducto_1_c.deleted = 0 AND
                sasa_saldosav.sasa_fecharegistro_c LIKE '{$yearSaldo}-{$monthSaldo}%' AND
                sasa_productosafav.id = '{$idProducto}' AND
                users.id = '{$idUserAccount}' AND
                sasap_presupuestoxproducto.assigned_user_id = '{$idUserAccount}' AND
                sasap_presupuestosxasesor.sasa_year_c = '{$yearSaldo}' AND
                sasap_presupuestosxasesor.sasa_mes_c = '{$monthSaldo}'
            GROUP BY
                sasa_saldosav.name
        ) tbl_saldos;
      ";
      
      $result = $db->query($query);
      //$bean->description = $query;
      //$bean->save();
      while($row = $db->fetchByAssoc($result) )
      {
        $idPxP = $row['idpxp'];
        $saldoTotal = $row['saldoActual'];
        //$nameAccounts = $row['nameAccounts'];
      }
      
      if (!empty($idPxP))
      {
        $PresupuestoxProducto = BeanFactory::retrieveBean('sasaP_PresupuestoxProducto', $idPxP);
        //$PresupuestoxProducto->description = $nameAccounts." | ".$query;
        $PresupuestoxProducto->sasa_saldoconsolidado_c = $saldoTotal;
        $PresupuestoxProducto->save();
      }
      
    }
    
  }
}
