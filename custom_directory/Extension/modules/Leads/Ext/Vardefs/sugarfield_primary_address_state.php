<?php
 // created: 2018-08-15 19:46:50
$dictionary['Lead']['fields']['primary_address_state']['required']=false;
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;

 ?>