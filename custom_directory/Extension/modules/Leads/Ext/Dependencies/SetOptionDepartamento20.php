<?php
$dependencies['Leads']['SetOptionDepartamento20'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"La Guajira")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Manaure","Villanueva","Albania","Barrancas","Dibula","Distraccion","El Molino","Fonseca","Hatonuevo","La Jagua del Pilar","Maicao","Riohacha","San Juan del Cesar","Uribia","Urumita")',
           'labels' => 'createList("","Manaure","Villanueva","Albania","Barrancas","Dibula","Distracción","El Molino","Fonseca","Hatonuevo","La Jagua del Pilar","Maicao","Riohacha","San Juan del Cesar","Uribia","Urumita")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
