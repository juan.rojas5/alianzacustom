<?php
$dependencies['Leads']['SetOptionDepartamento21'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Magdalena")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Salamina","Algarrobo","Aracataca","Ariguani","Cerro San Antonio","Chivolo","Cienaga","Concordia","El Banco","El Pinon","El Reten","Fundacion","Guamal","Nueva Granada","Pedraza","Pijino del Carmen","Pivijay","Plato","Pueblo Viejo","Remolino","Sabanas de San Angel","San Sebastian de Buenavista","San Zenon","Santa Ana","Santa Barbara de Pinto","Santa Marta","Sitionuevo","Tenerife","Zapayan","Zona Bananera")',
           'labels' => 'createList("","Salamina","Algarrobo","Aracataca","Ariguaní","Cerro San Antonio","Chivolo","Ciénaga","Concordia","El Banco","El Piñon","El Retén","Fundación","Guamal","Nueva Granada","Pedraza","Pijiño del Carmen","Pivijay","Plato","Pueblo Viejo","Remolino","Sabanas de San Angel","San Sebastián de Buenavista","San Zenón","Santa Ana","Santa Bárbara de Pinto","Santa Marta","Sitionuevo","Tenerife","Zapayán","Zona Bananera")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
