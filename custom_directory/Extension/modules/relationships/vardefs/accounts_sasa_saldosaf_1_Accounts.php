<?php
// created: 2019-01-04 21:36:33
$dictionary["Account"]["fields"]["accounts_sasa_saldosaf_1"] = array (
  'name' => 'accounts_sasa_saldosaf_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_saldosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAF',
  'bean_name' => 'sasa_SaldosAF',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_saldosaf_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
