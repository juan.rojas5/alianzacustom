<?php
// created: 2022-10-31 07:45:21
$viewdefs['Accounts']['mobile']['view']['edit'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '1',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
  ),
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_DEFAULT',
      'newTab' => false,
      'panelDefault' => 'expanded',
      'name' => 'LBL_PANEL_DEFAULT',
      'columns' => '1',
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'displayParams' => 
          array (
            'required' => true,
            'wireless_edit_only' => true,
          ),
        ),
        1 => 
        array (
          'name' => 'account_type',
          'comment' => 'The Company is of this type',
          'label' => 'LBL_TYPE',
        ),
        2 => 
        array (
          'name' => 'sasa_tipopersona_c',
          'label' => 'LBL_SASA_TIPOPERSONA_C',
        ),
        3 => 
        array (
          'name' => 'sasa_tipoidentificacion_c',
          'label' => 'LBL_SASA_TIPOIDENTIFICACION_C',
        ),
        4 => 
        array (
          'name' => 'sasa_nroidentificacion_c',
          'label' => 'LBL_SASA_NROIDENTIFICACION_C',
        ),
        5 => 
        array (
          'name' => 'sasa_telefono_c',
          'label' => 'LBL_SASA_TELEFONO_C',
        ),
        6 => 
        array (
          'name' => 'sasa_telefono2_c',
          'label' => 'LBL_SASA_TELEFONO2_C',
        ),
        7 => 'email',
        8 => 'billing_address_country',
        9 => 'billing_address_state',
        10 => 'billing_address_city',
        11 => 'billing_address_street',
        12 => 
        array (
          'name' => 'sasa_estadocuenta_c',
          'label' => 'LBL_SASA_ESTADOCUENTA_C',
        ),
        13 => 
        array (
          'name' => 'sasa_sector_c',
          'label' => 'LBL_SASA_SECTOR_C',
        ),
        14 => 
        array (
          'name' => 'sasa_fechaconstitucion_c',
          'label' => 'LBL_SASA_FECHACONSTITUCION_C',
        ),
        15 => 
        array (
          'name' => 'ownership',
          'comment' => '',
          'label' => 'LBL_OWNERSHIP',
        ),
        16 => 
        array (
          'name' => 'sasa_actividadeconomica_c',
          'label' => 'LBL_SASA_ACTIVIDADECONOMICA_C',
        ),
        17 => 
        array (
          'name' => 'sasa_ocupacion_c',
          'label' => 'LBL_SASA_OCUPACION_C',
        ),
        18 => 
        array (
          'name' => 'sasa_importadorexportador_c',
          'label' => 'LBL_SASA_IMPORTADOREXPORTADOR_C',
        ),
        19 => 
        array (
          'name' => 'sasa_operamonedaextranjera_c',
          'label' => 'LBL_SASA_OPERAMONEDAEXTRANJERA_C',
        ),
        20 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosoperaciones_c',
          'label' => 'LBL_SASA_INGRESOSOPERACIONES_C',
        ),
        21 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosventasmensual_c',
          'label' => 'LBL_SASA_INGRESOSVENTASMENSUAL_C',
        ),
        22 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_ingresosventasanuales_c',
          'label' => 'LBL_SASA_INGRESOSVENTASANUALES_C',
        ),
        23 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_activos_c',
          'label' => 'LBL_SASA_ACTIVOS_C',
        ),
        24 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_pasivos_c',
          'label' => 'LBL_SASA_PASIVOS_C',
        ),
        25 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'sasa_patrimonio_c',
          'label' => 'LBL_SASA_PATRIMONIO_C',
        ),
        26 => 
        array (
          'name' => 'sasa_segmento_c',
          'label' => 'LBL_SASA_SEGMENTO_C',
        ),
        27 => 
        array (
          'name' => 'employees',
          'comment' => 'Number of employees, varchar to accomodate for both number (100) or range (50-100)',
          'label' => 'LBL_EMPLOYEES',
        ),
        28 => 
        array (
          'name' => 'sasa_fechaactualizacion_c',
          'label' => 'LBL_SASA_FECHAACTUALIZACION_C',
        ),
        29 => 
        array (
          'name' => 'description',
          'comment' => 'Full text of the note',
          'label' => 'LBL_DESCRIPTION',
        ),
        30 => 
        array (
          'name' => 'website',
          'displayParams' => 
          array (
            'type' => 'link',
          ),
        ),
        31 => 'assigned_user_name',
        32 => 'tag',
      ),
    ),
  ),
);