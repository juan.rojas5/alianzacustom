<?php
 // created: 2022-11-18 20:52:19
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['labelValue']='Solucionador';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_solucionador_c']['readonly_formula']='';

 ?>