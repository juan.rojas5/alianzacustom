<?php
// WARNING: The contents of this file are auto-generated.


// created: 2023-02-20 21:38:36
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  ),
);

// created: 2019-01-04 21:39:25
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_SASA_MOVIMIENTOSAF_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosaf_1',
  ),
);

// created: 2019-01-04 21:40:08
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosav_1',
  ),
);

// created: 2019-01-04 21:41:00
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_MOVIMIENTOSAVDIVISAS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_movimientosavdivisas_1',
  ),
);

// created: 2022-11-21 20:51:44
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_pqrs_historico_1',
  ),
);

// created: 2019-01-04 21:36:33
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_saldosaf_1',
  ),
);

// created: 2019-01-04 21:37:35
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasa_saldosav_1',
  ),
);

// created: 2019-01-04 22:23:30
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'context' => 
  array (
    'link' => 'accounts_sasas_saldosconsolidados_1',
  ),
);

 // created: 2012-06-13 21:08:52
$layout_defs["Accounts"]["subpanel_setup"]['fbsg_ccintegrationlog_accounts'] = array (
  'order' => 100,
  'module' => 'fbsg_CCIntegrationLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'CC Integration Log',
  'get_subpanel_data' => 'fbsg_ccintegrationlog_accounts',
  'top_buttons' =>
  array (
  ),
);


$viewdefs["Accounts"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_CCILOG_SUBPANEL',
  'context' => array(
    'link' => 'fbsg_ccintegrationlog_accounts',
  ),
);


$viewdefs["Accounts"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
  'context' => array(
    'link' => 'prospect_list_accounts',
  ),
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_movimientosaf_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_movimientosaf_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_movimientosav_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_movimientosav_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_movimientosavdivisas_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_movimientosavdivisas_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_saldosaf_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_saldosaf_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'accounts_sasa_saldosav_1',
  'view' => 'subpanel-for-accounts-accounts_sasa_saldosav_1',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts',
  'view' => 'subpanel-for-accounts-contacts',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'fbsg_ccintegrationlog_accounts',
  'view' => 'subpanel-for-accounts-fbsg_ccintegrationlog_accounts',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'leads',
  'view' => 'subpanel-for-accounts-leads',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunities',
  'view' => 'subpanel-for-accounts-opportunities',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prospect_list_accounts',
  'view' => 'subpanel-for-accounts-prospect_list_accounts',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'revenuelineitems',
  'view' => 'subpanel-for-accounts-revenuelineitems',
);


//auto-generated file DO NOT EDIT
$viewdefs['Accounts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'tasks',
  'view' => 'subpanel-for-accounts-tasks',
);
