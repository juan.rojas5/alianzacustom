<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SASA_PRODUCTO_COD_C'] = 'Producto';
$mod_strings['LBL_SASA_ENTE_CONTROL_C'] = 'Ente de Control';
$mod_strings['LBL_SASA_DESISTIMIENTO_C'] = 'Desistimiento';
$mod_strings['LBL_SASA_RECEPCION_C'] = 'Recepción';
$mod_strings['LBL_SASA_PUNTORECEPCION_C'] = 'Punto de recepción';
$mod_strings['LBL_SASA_ADMISION_C'] = 'Admisión';
$mod_strings['LBL_SASA_FAVORAVILIDAD_C'] = 'Favoravilidad';
$mod_strings['LBL_SASA_ACEPTACION_C'] = 'Aceptación';
$mod_strings['LBL_SASA_RECTIFICACION_C'] = 'Rectificacion';
$mod_strings['LBL_SASA_MARCACION_C'] = 'Marcación';
$mod_strings['LBL_SASA_CODIGOENTIDAD_C'] = 'Código de Entidad';
$mod_strings['LBL_SASA_TUTELA_C'] = 'Tutela';
$mod_strings['LBL_SASA_ESCALAMIENTO_DCF_C'] = 'Escalamiento del Defensor';
$mod_strings['LBL_SASA_REPLICA_C'] = 'Replica';
$mod_strings['LBL_SASA_QUEJAEXPRES_C'] = 'Queja Exprés';
$mod_strings['LBL_SASA_DOCRESPFINAL_C'] = 'Documentación de respuesta final';
$mod_strings['LBL_SASA_PRODUCTODIGITAL_C'] = 'Producto Digital';
$mod_strings['LBL_SASA_PRORROGA_C'] = 'Prórroga';
$mod_strings['LBL_SASA_MOTIVO_C'] = 'Motivo';
$mod_strings['LBL_SASA_CODIGOQUEJA_C'] = 'Código de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXOQUEJA_C'] = 'Anexos de la queja o reclamo';
$mod_strings['LBL_SASA_ANEXORESFINAL_C'] = 'Anexos a la respuesta final';
$mod_strings['LBL_SOURCE'] = 'Fuente';
$mod_strings['LBL_STATUS'] = 'Estado de la queja o reclamo';
$mod_strings['LBL_WORK_LOG'] = 'Detalle del producto';
$mod_strings['LBL_DESCRIPTION'] = 'Texto de la queja o Reclamo';
$mod_strings['LBL_RESOLUTION'] = 'Argumento réplica';
$mod_strings['LBL_SASA_IS_ESCALATED_C'] = 'Anexos de la queja o Reclamo';
$mod_strings['LBL_SASA_CANAL_C'] = 'Canal';
$mod_strings['LBL_SASA_TIPOENTIDAD_C'] = 'Tipo de Entidad';
$mod_strings['LBL_SASA_RADSFC_C'] = 'Rad SFC';
$mod_strings['LBL_SASA_RADDEFENSORC_C'] = 'Rad Defensor del Consumidor ';
$mod_strings['LBL_SASA_RADALIANZA_C'] = 'Rad Alianza ';
$mod_strings['LBL_SASA_TIPONEGOCIO_C'] = 'Tipo de Negocio';
$mod_strings['LBL_SASA_SFC_RESPONSE_API_C'] = 'Respuesta SFC API';
$mod_strings['LBL_SASA_SFCFECHACREACION_C'] = 'Fecha Creación SFC';
$mod_strings['LBL_SASA_FECHAACTUALIZACIONSFC_C'] = 'Fecha de actualización SFC';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Respuestas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Fechas Quejas SFC';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información del registro';
$mod_strings['LBL_RESOLVED_DATETIME'] = 'Fecha de Vencimiento';
$mod_strings['LBL_SASA_LISTAPRUEBA_C'] = 'Listas de pruebas';
$mod_strings['LBL_SASA_NEGOCIO_C'] = 'Negocio';
$mod_strings['LBL_SASA_RADALIANZAM2_C'] = 'Rad Alianza M2';