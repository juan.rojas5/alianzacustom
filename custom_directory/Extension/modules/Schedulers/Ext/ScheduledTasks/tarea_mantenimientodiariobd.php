<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_eliminado_diario';
function tarea_eliminado_diario(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_Mantenimiento_diario. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{

    global $db;
    
        $queries = array("TRUNCATE TABLE activities",
          "TRUNCATE TABLE activities_users",
          "TRUNCATE TABLE sasa_saldosaf_audit",
          "TRUNCATE TABLE sasa_saldosav_audit",
          "TRUNCATE TABLE sasa_movimientosav_audit",
          "TRUNCATE TABLE sasa_movimientosaf_audit",
          "DELETE FROM tracker where tracker.date_modified <= date_sub(curdate(),INTERVAL 7 DAY)",
          "DELETE IGNORE  FROM sasa_movimientosaf WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE  FROM sasa_movimientosav WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE  FROM sasa_saldosaf WHERE date_entered < date_sub(curdate(), interval 30 DAY)",
          "DELETE IGNORE FROM accounts_audit WHERE date_created < date_sub(curdate(), interval 6 MONTH)",
        );

        //forecah para empezar a ejecutar los queries del array $queries
        foreach ($queries as $query) {
          $querytotal = str_replace(array("\n","\t"),array(" "," "), trim($query));
          $result = $db->query($querytotal);
          $lastDbError = $db->lastDbError();        
          if(!empty($lastDbError)){
            $GLOBALS['log']->fatal("Error de base de datos: {$lastDbError} -> Query {$querytotal}"); 
          }
        }
    
    $GLOBALS['log']->security("****Hola Diario**********************\n");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_Mantenimiento_diario. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
