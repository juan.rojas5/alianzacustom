<?php
// created: 2019-01-04 22:02:18
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_tipo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_TIPO_C',
    'width' => 10,
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'accounts_sasa_movimientosavdivisas_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SASA_MOVIMIENTOSAVDIVISAS_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_sasa_movimientosavdivisas_1accounts_ida',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);