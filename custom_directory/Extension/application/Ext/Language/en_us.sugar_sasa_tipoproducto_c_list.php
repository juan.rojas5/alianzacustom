<?php
 // created: 2022-09-08 20:04:16

$app_list_strings['sasa_tipoproducto_c_list']=array (
  1 => 'Fondo Abierto Alianza sin Pacto de Permanencia Mínima por Compartimientos',
  2 => 'Fondo Liquidez',
  3 => 'Fondo de Inversiones Colectiva Abierto con Pacto de Permanencia Alianza Acciones Colombia',
  4 => 'FIC Alianza Renta Fija 90',
  5 => 'Fondo Alianza Liquidez Dólar',
  6 => 'Fondo Alianza Renta Fija Mercados Emergentes',
  7 => 'FIC mercado Monetario Alianza',
  8 => 'FIC Adcap Multiplazos',
  9 => 'FIC Adcap Renta fija Colombia',
  10 => 'FIC Invertir Gestionado',
  11 => 'Fondo Abierto con pacto de permanencia Renovable Alternativos Alianza',
  12 => 'FIC Cerrado Sentencias Nación Alianza',
  13 => 'Cartera Colectiva Abierta con Pacto de Permanencia CxC',
  14 => 'Fondo Abierto Cahs Conservador',
  15 => 'Fondo de Pensiones de Jubilación e Invalidez Visión',
  16 => 'Fondo capital privado',
  17 => 'Inmobiliaria',
  18 => 'Administración',
  19 => 'Contrato de comisión',
  '' => '',
);