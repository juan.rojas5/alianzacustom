<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/5eb0eb2e-2f81-11ed-8746-0286beac7abe/source_dom.php

// created: 2022-09-09 19:32:50
$role_dropdown_filters['source_dom'] = array (
  '' => true,
  'Internal' => false,
  'Forum' => false,
  'Web' => false,
  'InboundEmail' => true,
  'Twitter' => false,
  'SFC' => false,
  'Defensor del Consumidor' => false,
  'Carta' => false,
  'portal publico' => true,
  'portal transaccional' => true,
  'Telefono' => true,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/5eb0eb2e-2f81-11ed-8746-0286beac7abe/status_list.php

// created: 2022-09-09 19:54:08
$role_dropdown_filters['status_list'] = array (
  '' => true,
  10 => false,
  1 => true,
  2 => true,
  3 => false,
  4 => true,
  5 => true,
);
?>
