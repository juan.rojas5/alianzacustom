<?php
// created: 2023-02-20 21:38:36
$dictionary["accounts_sasa1_unidades_de_negocio_por__1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_sasa1_unidades_de_negocio_por__1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa1_Unidades_de_negocio_por_',
      'rhs_table' => 'sasa1_unidades_de_negocio_por_',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_sasa1_unidades_de_negocio_por__1_c',
      'join_key_lhs' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
      'join_key_rhs' => 'accounts_sea5eio_por__idb',
    ),
  ),
  'table' => 'accounts_sasa1_unidades_de_negocio_por__1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_sasa1_unidades_de_negocio_por__1accounts_ida' => 
    array (
      'name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
      'type' => 'id',
    ),
    'accounts_sea5eio_por__idb' => 
    array (
      'name' => 'accounts_sea5eio_por__idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_accounts_sasa1_unidades_de_negocio_por__1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_accounts_sasa1_unidades_de_negocio_por__1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_accounts_sasa1_unidades_de_negocio_por__1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_sea5eio_por__idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'accounts_sasa1_unidades_de_negocio_por__1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_sea5eio_por__idb',
      ),
    ),
  ),
);