<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class leads_valor_minimo_invercion
{
	function before_save($bean, $event, $arguments)
	{
		try{
			/*
			* Caso sasa 5487 Linea de Ingreso - Valor superior al campo "Valor Mínimo de Inversión"
			* Se requiere en la instancia de Alianza, en el módulo de Cuentas, cuando cambie el usuario asignado, se deben reasignar todos los registros asociados a dicha cuenta para el usuario reasignado a la misma.
			* Los registros que deben reasignarse son los que se encuentren pendiente de gestión, es decir, en un estado diferente a Completado/Realizado/Cerrado.
			*/	
			if(!empty($bean->product_template_id) and !empty($bean->likely_case)){			
				$ProductTemplates = BeanFactory::getBean("ProductTemplates", $bean->product_template_id, array('disable_row_level_security' => true));				
				if( $bean->likely_case*1 < $ProductTemplates->discount_price*1){
					$bean->likely_case =$ProductTemplates->discount_price*1;
				}
			}
		} 
		catch (Exception $e) {
		    	$GLOBALS['log']->security("ERROR: error al convertir ".$e->getMessage()); 
		}
	}}
?>
