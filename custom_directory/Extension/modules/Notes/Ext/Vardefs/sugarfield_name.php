<?php
 // created: 2018-05-30 14:55:41
$dictionary['Note']['fields']['name']['audited']=false;
$dictionary['Note']['fields']['name']['massupdate']=false;
$dictionary['Note']['fields']['name']['comments']='Name of the note';
$dictionary['Note']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Note']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Note']['fields']['name']['merge_filter']='disabled';
$dictionary['Note']['fields']['name']['reportable']=false;
$dictionary['Note']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.83',
  'searchable' => true,
);
$dictionary['Note']['fields']['name']['calculated']=false;

 ?>