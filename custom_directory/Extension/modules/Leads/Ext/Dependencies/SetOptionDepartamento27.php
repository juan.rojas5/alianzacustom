<?php
$dependencies['Leads']['SetOptionDepartamento27'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Risaralda")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Balboa","Apia","Belen de Umbria","Dosquebradas","Guatica","La Celia","La Virginia","Marsella","Mistrato","Pereira","Pueblo Rico","Quinchia","Santa Rosa de Cabal","Santuario")',
           'labels' => 'createList("","Balboa","Apía","Belén de Umbría","Dosquebradas","Guática","La Celia","La Virginia","Marsella","Mistrató","Pereira","Pueblo Rico","Quinchía","Santa Rosa de Cabal","Santuario")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
