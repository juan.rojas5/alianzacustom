<?php
 // created: 2018-11-21 20:57:48
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_saldosaf_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['enforced']=true;

 ?>