<?php
 // created: 2023-02-01 15:55:55
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['labelValue']='Código negocio';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['readonly_formula']='';

 ?>