<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_compania_c_list.php

 // created: 2018-05-30 12:26:34

$app_list_strings['sasa_compania_c_list']=array (
  'Alianza Fiduciaria' => 'Alianza Fiduciaria',
  'Alianza Valores' => 'Alianza Valores',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_salutation_list.php

 // created: 2018-05-22 20:52:36

$app_list_strings['salutation_list']=array (
  '' => '',
  'Sr' => 'Sr.',
  'Sra' => 'Sra.',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p3horizontetiempoinve_c_list.php

 // created: 2018-06-06 21:20:22

$app_list_strings['sasa_p3horizontetiempoinve_c_list']=array (
  '' => '',
  4 => 'a. Máximo 3 años',
  5 => 'b. Entre 3 y 5 años',
  6 => 'c. Más de 5 Años',
  7 => 'd. Más de 10 años',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p4dondeprovienedinero_c_list.php

 // created: 2018-06-06 21:41:02

$app_list_strings['sasa_p4dondeprovienedinero_c_list']=array (
  '' => '',
  3 => 'a. De mi trabajo',
  5 => 'b. De mi trabajo más algunos ahorros',
  7 => 'c. De rentas no relacionadas con mi trabajo',
  9 => 'd. Herencia o ganancia ocasional no esperada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p1rangoedadcalidad_c_list.php

 // created: 2018-06-02 00:05:13

$app_list_strings['sasa_p1rangoedadcalidad_c_list']=array (
  '' => '',
  10 => 'a. Menor a 35 años',
  7 => 'b. Entre 35 y 50 años',
  5 => 'c. Mayor a 50 años',
  2 => 'd. Pensionado-Banca privada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p8supongainversionesp_c_list.php

 // created: 2018-06-06 22:05:31

$app_list_strings['sasa_p8supongainversionesp_c_list']=array (
  '' => '',
  5 => 'a. Vendería todas mis inversiones',
  9 => 'b. Mantendría mis inversiones un poco más, esperando que se recuperen',
  13 => 'c. Aumentaría mis inversiones esperando que los precios regresen a niveles anteriores',
  1 => 'd. Me molestaría al punto de quejarme ante el regulador',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p7oportunidadrentabil_c_list.php

 // created: 2018-06-06 22:00:00

$app_list_strings['sasa_p7oportunidadrentabil_c_list']=array (
  '' => '',
  3 => 'a. No estaría dispuesto a asumir mayor riesgo',
  5 => 'b. Estaría dispuesto a asumir un poco más de riesgo tratando de no incurrir en pérdidas de capital',
  7 => 'c. Estaría dispuesto a asumir mucho más riesgo para aumentar mi rentabilidad',
  9 => 'd. Me endeudaría o buscaría opciones de apalancamiento para obtener un retorno más interesante',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p6experienciainversio_c_list.php

 // created: 2018-06-06 21:51:35

$app_list_strings['sasa_p6experienciainversio_c_list']=array (
  '' => '',
  2 => 'a. Poca',
  5 => 'b. Alguna, preferiría recibir orientación y asesoria en los temas de inversiones',
  7 => 'c. Mucha, me siento seguro y tranquilo para entender los riesgos asociados a cada inversión y tomar decisiones',
  10 => 'd. Soy inversionista profesional',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p5porcentajeactivosah_c_list.php

 // created: 2018-06-06 21:46:07

$app_list_strings['sasa_p5porcentajeactivosah_c_list']=array (
  '' => '',
  7 => 'a. Hasta 10%',
  6 => 'b. Entre 10% y 30%',
  5 => 'c. Entre 30% y 50%',
  4 => 'd. Más del 50%',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_p2propositoahorroinve_c_list.php

 // created: 2018-06-02 00:10:15

$app_list_strings['sasa_p2propositoahorroinve_c_list']=array (
  '' => '',
  9 => 'a. Acumular capital a mediano plazo con un fin específico (estudios, viajes, etc.)',
  5 => 'b. Ahorrar para pagar gastos del corto plazo',
  1 => 'c. Contar con una inversión que me permita tener una renta en la vejez',
  13 => 'd. Especular en el mercado de capitales, aceptando las posibles pérdidas que tenga',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_sectoreconomico_c_list.php

 // created: 2018-05-23 21:08:29

$app_list_strings['sasa_sectoreconomico_c_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_operamonedaextranjera_c_list.php

 // created: 2018-06-12 13:52:51

$app_list_strings['sasa_operamonedaextranjera_c_list']=array (
  '' => '',
  'Si' => 'Si',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_nrohijos_c_list.php

 // created: 2018-06-12 14:27:45

$app_list_strings['sasa_nrohijos_c_list']=array (
  '' => '',
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_relacioncuenta_c_list.php

 // created: 2018-06-12 14:42:01

$app_list_strings['sasa_relacioncuenta_c_list']=array (
  '' => '',
  'Influenciador' => 'Influenciador',
  'Tomador de decisiones' => 'Tomador de decisiones',
  'Manejador de la relacion' => 'Manejador de la relación',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_periodicidad_c_list.php

 // created: 2018-06-14 19:23:22

$app_list_strings['sasa_periodicidad_c_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_infointeres_c_list.php

 // created: 2018-06-14 19:26:19

$app_list_strings['sasa_infointeres_c_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivoperdido_c_list.php

 // created: 2018-06-20 13:23:54

$app_list_strings['sasa_motivoperdido_c_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_opportunity_type_dom.php

 // created: 2018-06-20 13:35:31

$app_list_strings['opportunity_type_dom']=array (
  '' => '',
  'Existing Business' => 'Existing Business',
  'New Business' => 'New Business',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_source_list.php

 // created: 2018-06-20 20:08:35

$app_list_strings['lead_source_list']=array (
  '' => '',
  'Accionista' => 'Accionista',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipocontacto_c_list.php

 // created: 2018-06-21 13:35:33

$app_list_strings['sasa_tipocontacto_c_list']=array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ConstantContactIntegration.php

/*********************************************************************************
 * The contents of this file are subject to the SugarCRM Master Subscription
 * Agreement ("License") which can be viewed at
 * http://www.sugarcrm.com/crm/en/msa/master_subscription_agreement_11_April_2011.pdf
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * among other things: 1) sublicense, resell, rent, lease, redistribute, assign
 * or otherwise transfer Your rights to the Software, and 2) use the Software
 * for timesharing or service bureau purposes such as hosting the Software for
 * commercial gain and/or for the benefit of a third party.  Use of the Software
 * may be subject to applicable fees and any use of the Software without first
 * paying applicable fees is strictly prohibited.  You do not have the right to
 * remove SugarCRM copyrights from the source code or user interface.
 *
 * All copies of the Covered Code must include on each user interface screen:
 *  (i) the "Powered by SugarCRM" logo and
 *  (ii) the SugarCRM copyright notice
 * in the same form as they appear in the distribution.  See full license for
 * requirements.
 *
 * Your Warranty, Limitations of liability and Indemnity are expressly stated
 * in the License.  Please refer to the License for the specific language
 * governing these rights and limitations under the License.  Portions created
 * by SugarCRM are Copyright (C) 2004-2011 SugarCRM, Inc.; All Rights Reserved.
 ********************************************************************************/


$app_list_strings['moduleList']['fbsg_ConstantContactIntegration'] = 'Constant Contact Integration Configuration';
$app_list_strings['moduleList']['fbsg_TableTest'] = 'TableTest';
$app_list_strings['moduleList']['fbsg_test'] = 'CC Errors';
$app_list_strings['moduleList']['fbsg_CCErrors'] = 'Constant Contact Integration Errors';
$app_list_strings['moduleList']['fbsg_CCIErrors'] = 'Constant Contact Integration Errors';
$app_list_strings['log_level_list']['debug'] = 'Debug';
$app_list_strings['log_level_list']['error'] = 'Error';
$app_list_strings['log_level_list']['fatal'] = 'Fatal';
$app_list_strings['log_level_list']['warn'] = 'Warning';
$app_list_strings['log_level_list']['info'] = 'Information';
$app_list_strings['bool_list']['0'] = 'No';
$app_list_strings['bool_list']['1'] = 'Yes';

$app_list_strings['moduleList']['fbsg_CCIContactLog'] = 'Constant Contact Integration Log';
$app_list_strings['moduleList']['fbsg_CCIntegrationLog'] = 'Constant Contact Integration Log';

$app_list_strings['cc_list_dom'] = array('' => '');

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_campaign_status_dom.php

 //created: 2018-06-22 19:22:11

$app_list_strings['campaign_status_dom']['Sent']='Sent';
$app_list_strings['campaign_status_dom']['Scheduled']='Scheduled';
$app_list_strings['campaign_status_dom']['Draft']='Draft';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_resultadollamada_c_list.php

 // created: 2018-06-23 17:39:18

$app_list_strings['sasa_resultadollamada_c_list']=array (
  '' => '',
  'No Contactado' => 'No Contactado',
  'Datos Errados' => 'Datos Errados',
  'Interesado' => 'Interesado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_infoquiererecibir_c_list.php

 // created: 2018-06-26 16:21:46

$app_list_strings['sasa_infoquiererecibir_c_list']=array (
  '' => '',
  'Renta Fija' => 'Renta Fija',
  'Renta Variable' => 'Renta Variable',
  'Saturday Live' => 'Saturday Live',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoinfoperiodicidad_c_list.php

 // created: 2018-06-26 16:26:40

$app_list_strings['sasa_tipoinfoperiodicidad_c_list']=array (
  '' => '',
  'English man en Bogota trimestral' => 'English-man en Bogotá (trimestral)',
  'Tenedores de TES mensual' => 'Tenedores de TES (mensual)',
  'Emisiones corporativas deuda privada trimestral' => 'Emisiones corporativas deuda privada (trimestral)',
  'Informe del PEI mensual' => 'Informe del PEI (mensual)',
  'Mensual y flujos de acciones mensual' => 'Mensual y flujos de acciones (mensual)',
  'Informes corporativos trimestral' => 'Informes corporativos (trimestral)',
  'Rebalanceo del indice COLCAP trimestral' => 'Rebalanceo del índice COLCAP (trimestral)',
  'Pasajeros de Avianca mensual' => 'Pasajeros de Avianca (mensual)',
  'Saturday Live Semanal los sabados' => 'Saturday Live (Semanal los sábados)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_source_dom.php

 // created: 2018-06-26 19:49:50

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold Call',
  'Existing Customer' => 'Existing Customer',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employee',
  'Partner' => 'Partner',
  'Public Relations' => 'Public Relations',
  'Direct Mail' => 'Direct Mail',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Web Site' => 'Web Site',
  'Word of mouth' => 'Word of mouth',
  'Email' => 'Email',
  'Campaign' => 'Campaign',
  'Support Portal User Registration' => 'Support Portal User Registration',
  'Other' => 'Other',
  'Recomendacion' => 'Recomendación',
  'Evento Social' => 'Evento Social',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_parentescorelacion_c_list.php

 // created: 2018-07-04 16:47:41

$app_list_strings['sasa_parentescorelacion_c_list']=array (
  '' => '',
  'Otro' => 'Otro',
  'Hija o' => 'Hija (o)',
  'Esposa o' => 'Esposa (a)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ctrlwf_c_list.php

 // created: 2018-07-09 02:16:44

$app_list_strings['sasa_ctrlwf_c_list']=array (
  '' => '',
  'Proceso Fecha Actualizacion' => 'Proceso Fecha Actualización',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_responsableejecucion_c_list.php

 // created: 2018-07-10 22:26:26

$app_list_strings['sasa_responsableejecucion_c_list']=array (
  '' => '',
  'Cliente' => 'Cliente',
  'Comercial' => 'Comercial',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_estadoinfocp_c_list.php

 // created: 2018-07-17 23:37:26

$app_list_strings['sasa_estadoinfocp_c_list']=array (
  'Informacion Incompleta' => 'Información Incompleta',
  'Informacion Completa' => 'Información Completa',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ctrlconversion_c_list.php

 // created: 2018-07-20 16:06:20

$app_list_strings['sasa_ctrlconversion_c_list']=array (
  '' => '',
  'Conversion II' => 'Conversión II',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_vencida_c_list.php

 // created: 2018-07-21 20:59:54

$app_list_strings['sasa_vencida_c_list']=array (
  'No' => 'No',
  'Si' => 'Sí',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_nombreproducto_c_list.php

 // created: 2018-08-08 03:27:45

$app_list_strings['sasa_nombreproducto_c_list']=array (
  '' => '',
  'Fondo Alianza Acciones' => 'Fondo Alianza Acciones',
  'Fondo Alianza Liquidez Dolar' => 'Fondo Alianza Liquidez Dólar',
  'Fondo Alianza Renta Fija 90' => 'Fondo Alianza Renta Fija 90',
  'Fondo Abierto Alianza' => 'Fondo Abierto Alianza',
  'Fondo Abierto Alianza Gobierno' => 'Fondo Abierto Alianza Gobierno',
  'Fondo Abierto con Pacto de Permanencia CXC' => 'Fondo Abierto con Pacto de Permanencia CXC',
  'Fondo Cerrado Inmobiliario Alianza' => 'Fondo Cerrado Inmobiliario Alianza',
  'Fondo de Inversion Colectiva Inmubeles Fiducor Compartimento Aptos Cll 92' => 'Fondo de Inversion Colectiva Inmubeles Fiducor Compartimento Aptos Cll 92',
  'Fondo Mas Colombia Oportunity' => 'Fondo Más Colombia Oportunity',
  'Fondo Alianza Renta Fija Mercados Emergentes' => 'Fondo Alianza Renta Fija Mercados Emergentes',
  'Bonos' => 'Bonos',
  'CDT' => 'CDT',
  'Derivados' => 'Derivados',
  'Repos' => 'Repos',
  'Simultaneas' => 'Simultaneas',
  'Renta Fija  Internacional' => 'Renta Fija - Internacional',
  'Colocacion Bonos' => 'Colocacion Bonos',
  'TTV' => 'TTV',
  'Otros' => 'Otros',
  'Acciones' => 'Acciones',
  'OPA' => 'OPA',
  'Custodia' => 'Custodia',
  'USD' => 'USD',
  'EFG' => 'EFG',
  'Administracion de Inversiones de Fondos Mutuos de Inversion' => 'Administración de Inversiones de Fondos Mutuos de Inversión',
  'APT' => 'APT',
  'Contrato de Administracion de Valores' => 'Contrato de Administración de Valores',
  'CXC Parte Activa' => 'CXC Parte Activa',
  'FCP OXO Propiedades Sostenibles' => 'FCP OXO Propiedades Sostenibles',
  'FCP OXO Propiedades Sostenibles COMP I Cartagena' => 'FCP OXO Propiedades Sostenibles COMP I Cartagena',
  'FCP OXO Propiedades Sostenibles COMP I Proyecto General' => 'FCP OXO Propiedades Sostenibles COMP I Proyecto General',
  'FCP OXO Propiedades Sostenibles COMP II Barranquilla' => 'FCP OXO Propiedades Sostenibles COMP II Barranquilla',
  'FCP OXO Propiedades Sostenibles COMP II OXO69' => 'FCP OXO Propiedades Sostenibles COMP II OXO69',
  'Fideicomisos de Inversion con destinacion Especifica' => 'Fideicomisos de Inversión con destinación Específica',
  'Fondo Abierto Libranza' => 'Fondo Abierto Libranza',
  'Fondo de Pensiones Voluntarias Vision' => 'Fondo de Pensiones Voluntarias Visión',
  'FOREX' => 'FOREX',
  'Pagos Giros' => 'Pagos - Giros',
  'Patrimonio Autonomo de Terceros' => 'Patrimonio Autónomo de Terceros',
  'PEI Renta Fija' => 'PEI (Renta Fija)',
  'Recaudo' => 'Recaudo',
  'Renta Fija' => 'Renta Fija',
  'Renta Variable' => 'Renta Variable',
  'Giros' => 'Giros',
  'Monetizacion' => 'Monetización',
  'Planes Empresariales' => 'Planes Empresariales',
  'Tidis' => 'Tidis',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_stage_dom.php

 // created: 2018-08-08 17:49:07

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Qualification',
  'Needs Analysis' => 'Needs Analysis',
  'Value Proposition' => 'Value Proposition',
  'Id. Decision Makers' => 'Id. Decision Makers',
  'Perception Analysis' => 'Perception Analysis',
  'Proposal/Price Quote' => 'Proposal/Price Quote',
  'Negotiation/Review' => 'Negotiation/Review',
  'Closed Won' => 'Closed Won',
  'Closed Lost' => 'Closed Lost',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php

 //created: 2018-08-08 18:10:42

$app_list_strings['moduleList']['RevenueLineItems']='Revenue Line Items';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display.php

 // created: 2018-08-08 18:11:54

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Contacts' => 'Contacts',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Project Task',
  'Tasks' => 'Task',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display_notes.php

 // created: 2018-08-08 18:11:54

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Task',
  'ProductTemplates' => 'Product Catalog',
  'Quotes' => 'Quote',
  'Products' => 'Quoted Line Item',
  'Contracts' => 'Contract',
  'Emails' => 'Email',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Meetings' => 'Meeting',
  'Calls' => 'Call',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_parent_type_display.php

 // created: 2018-08-08 18:11:54

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_etapaventas_c_list.php

 // created: 2018-08-08 18:17:57

$app_list_strings['sasa_etapaventas_c_list']=array (
  'Perception Analysis' => 'En Progreso',
  'Closed Lost' => 'Perdido',
  'Closed Won' => 'Ganado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sales_status_dom.php

 // created: 2018-08-08 18:18:14

$app_list_strings['sales_status_dom']=array (
  'New' => 'New',
  'In Progress' => 'In Progress',
  'Closed Won' => 'Closed Won',
  'Closed Lost' => 'Closed Lost',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_industry_dom.php

 // created: 2018-08-11 20:16:40

$app_list_strings['industry_dom']=array (
  '' => '',
  'Apparel' => 'Apparel',
  'Banking' => 'Banking',
  'Biotechnology' => 'Biotechnology',
  'Chemicals' => 'Chemicals',
  'Communications' => 'Communications',
  'Construction' => 'Construction',
  'Consulting' => 'Consulting',
  'Education' => 'Education',
  'Electronics' => 'Electronics',
  'Energy' => 'Energy',
  'Engineering' => 'Engineering',
  'Entertainment' => 'Entertainment',
  'Environmental' => 'Environmental',
  'Finance' => 'Finance',
  'Government' => 'Government',
  'Healthcare' => 'Healthcare',
  'Hospitality' => 'Hospitality',
  'Insurance' => 'Insurance',
  'Machinery' => 'Machinery',
  'Manufacturing' => 'Manufacturing',
  'Media' => 'Media',
  'Not For Profit' => 'Not For Profit',
  'Recreation' => 'Recreation',
  'Retail' => 'Retail',
  'Shipping' => 'Shipping',
  'Technology' => 'Technology',
  'Telecommunications' => 'Telecommunications',
  'Transportation' => 'Transportation',
  'Utilities' => 'Utilities',
  'Other' => 'Other',
  'Agropecuario' => 'Agropecuario',
  'Ciencia y Tecnica' => 'Ciencia y Técnica',
  'Floricultura' => 'Floricultura',
  'Minera y Petroleos' => 'Minera y Petroleos',
  'Saneamiento Ambiental' => 'Saneamiento Ambiental',
  'Atencion Sanitaria' => 'Atención Sanitaria',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_perfilriesgo_c_list.php

 // created: 2018-08-11 20:22:46

$app_list_strings['sasa_perfilriesgo_c_list']=array (
  '' => '',
  'Conservador' => 'Conservador',
  'Mayor Riesgo' => 'Mayor Riesgo',
  'Moderado' => 'Moderado',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_sector_c_list.php

 // created: 2018-08-11 20:23:20

$app_list_strings['sasa_sector_c_list']=array (
  '' => '',
  'Privada' => 'Privada',
  'Publica' => 'Pública',
  'Mixta' => 'Mixta',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ocupacion_c_list.php

 // created: 2018-08-11 20:43:13

$app_list_strings['sasa_ocupacion_c_list']=array (
  '' => '',
  'Asalariado' => 'Asalariado',
  'Comercial' => 'Comercial',
  'Construccion' => 'Construcción',
  'Empleado' => 'Empleado',
  'Estudiante' => 'Estudiante',
  'Financiero' => 'Financiero',
  'Hogar' => 'Hogar',
  'Independiente' => 'Independiente',
  'Industrial' => 'Industrial',
  'Militar' => 'Militar',
  'Pensionado' => 'Pensionado',
  'Religioso' => 'Religioso',
  'Rentista' => 'Rentista',
  'Servicios' => 'Servicios',
  'Socio' => 'Socio',
  'Transporte' => 'Transporte',
  'Otros' => 'Otros',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_importadorexportador_c_list.php

 // created: 2018-08-11 20:46:39

$app_list_strings['sasa_importadorexportador_c_list']=array (
  '' => '',
  'Importador' => 'Importador',
  'Exportador' => 'Exportador',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_call_status_dom.php

 // created: 2018-08-11 21:48:06

$app_list_strings['call_status_dom']=array (
  'Planned' => 'Scheduled',
  'Held' => 'Held',
  'Not Held' => 'Canceled',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipopersona_c_list.php

 // created: 2018-08-11 23:18:56

$app_list_strings['sasa_tipopersona_c_list']=array (
  '' => '',
  'Natural' => 'Natural',
  'Juridica' => 'Jurídica',
  'Fideicomiso' => 'Fideicomiso',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ctrlpostergado_c_list.php

 // created: 2018-08-15 20:08:36

$app_list_strings['sasa_ctrlpostergado_c_list']=array (
  '' => '',
  'Postergar' => 'Postergar',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_objetivovisita_c_list.php

 // created: 2018-08-17 15:39:52

$app_list_strings['sasa_objetivovisita_c_list']=array (
  '' => '',
  'Vincular' => 'Vincular',
  'Profundizacion' => 'Profundización',
  'Incrementar Depositos' => 'Incrementar Depósitos',
  'Seguimiento Mantenimiento' => 'Seguimiento/Mantenimiento',
  'PQR' => 'PQR',
  'Otro' => 'Otro',
  'Indagar Necesidades' => 'Indagar Necesidades',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_departamento_c_list.php

 // created: 2018-06-23 15:04:51

$app_list_strings['sasa_departamento_c_list']=array (
  '' => '',
  'Amazonas' => 'Amazonas',
  'Antioquia' => 'Antioquia',
  'Arauca' => 'Arauca',
  'Archipielago de San Andres Providencia y Santa Catalina' => 'Archipiélago de San Andrés, Providencia y Santa Catalina',
  'Atlantico' => 'Atlántico',
  'Bogota D.C.' => 'Bogotá D.C.',
  'Bolivar' => 'Bolívar',
  'Boyaca' => 'Boyacá',
  'Caldas' => 'Caldas',
  'Caqueta' => 'Caquetá',
  'Casanare' => 'Casanare',
  'Cauca' => 'Cauca',
  'Cesar' => 'Cesar',
  'Choco' => 'Chocó',
  'Cundinamarca' => 'Cundinamarca',
  'Cordoba' => 'Córdoba',
  'Guainia' => 'Guainía',
  'Guaviare' => 'Guaviare',
  'Huila' => 'Huila',
  'La Guajira' => 'La Guajira',
  'Magdalena' => 'Magdalena',
  'Meta' => 'Meta',
  'Narino' => 'Nariño',
  'Norte de Santander' => 'Norte de Santander',
  'Putumayo' => 'Putumayo',
  'Quindio' => 'Quindío',
  'Risaralda' => 'Risaralda',
  'Santander' => 'Santander',
  'Sucre' => 'Sucre',
  'Tolima' => 'Tolima',
  'Valle del Cauca' => 'Valle del Cauca',
  'Vaupes' => 'Vaupés',
  'Vichada' => 'Vichada',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_actividadeconomica_c_list.php

 // created: 2018-08-24 14:04:44

$app_list_strings['sasa_actividadeconomica_c_list']=array (
  '' => '',
  'Comercial' => 'Comercial',
  'Industrial' => 'Industrial',
  'Transporte' => 'Transporte',
  'Construccion' => 'Construcción',
  'Servicios' => 'Servicios',
  'Financiero' => 'Financiero',
  'Otros' => 'Otros',
  'Agroindustria' => 'Agroindustria',
  'Comunicacion' => 'Comunicación',
  'Educacion' => 'Educación',
  'Salud' => 'Salud',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_estadocivil_c_list.php

 // created: 2018-09-19 22:32:13

$app_list_strings['sasa_estadocivil_c_list']=array (
  '' => '',
  'Soltero' => 'Soltero',
  'Casado' => 'Casado',
  'Divorsiado' => 'Divorsiado',
  'Union libre' => 'Unión libre',
  'Viudo' => 'Viudo',
  'Separado' => 'Separado',
  'Otro' => 'Otro',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_regional_c_list.php

 // created: 2018-10-04 19:36:15

$app_list_strings['sasa_regional_c_list']=array (
  '' => '',
  'Barranquilla' => 'Barranquilla',
  'Bogota' => 'Bogotá',
  'Bucaramanga' => 'Bucaramanga',
  'Cali' => 'Cali',
  'Cartagena' => 'Cartagena',
  'Manizales' => 'Manizales',
  'Medellin' => 'Medellín',
  'Pereira' => 'Pereira',
  'Eje Cafetero' => 'Eje Cafetero',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_resultadovisita_c_list.php

 // created: 2018-10-16 22:35:37

$app_list_strings['sasa_resultadovisita_c_list']=array (
  '' => '',
  'Interesado' => 'Interesado',
  'Efectivo' => 'Efectivo',
  'Negocio en Tramite' => 'Negocio en Trámite',
  'Descartado' => 'Descartado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.SaldosyMovimientosAFV.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_SaldosAF'] = 'Saldos AF';
$app_list_strings['moduleList']['sasa_SaldosAV'] = 'Saldos AV';
$app_list_strings['moduleList']['sasa_MovimientosAF'] = 'Movimientos AF';
$app_list_strings['moduleList']['sasa_MovimientosAV'] = 'Movimientos AV';
$app_list_strings['moduleList']['sasa_MovimientosAVDivisas'] = 'Movimientos AV (Divisas)';
$app_list_strings['moduleList']['sasa_ProductosAFAV'] = 'Productos AF/AV';
$app_list_strings['moduleList']['sasa_Categorias'] = 'Categorias';
$app_list_strings['moduleListSingular']['sasa_SaldosAF'] = 'Saldo AF';
$app_list_strings['moduleListSingular']['sasa_SaldosAV'] = 'Saldo AV';
$app_list_strings['moduleListSingular']['sasa_MovimientosAF'] = 'Movimiento AF';
$app_list_strings['moduleListSingular']['sasa_MovimientosAV'] = 'Movimiento AV';
$app_list_strings['moduleListSingular']['sasa_MovimientosAVDivisas'] = 'Movimiento AV (Divisas)';
$app_list_strings['moduleListSingular']['sasa_ProductosAFAV'] = 'Producto AF/AV';
$app_list_strings['moduleListSingular']['sasa_Categorias'] = 'Categoria';
$app_list_strings['sasa_categoria_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.SaldosConsolidados.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasaS_SaldosConsolidados'] = 'Saldos Consolidados';
$app_list_strings['moduleListSingular']['sasaS_SaldosConsolidados'] = 'Saldo Consolidado';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.PresupuestosxIngresos.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasaP_PresupuestosxAsesor'] = 'Presupuestos x Asesor';
$app_list_strings['moduleList']['sasaP_PresupuestoxProducto'] = 'Presupuestos x Producto';
$app_list_strings['moduleListSingular']['sasaP_PresupuestosxAsesor'] = 'Presupuesto x Asesor';
$app_list_strings['moduleListSingular']['sasaP_PresupuestoxProducto'] = 'Presupuesto x Producto';
$app_list_strings['sasa_year_c_list'][2018] = '2018';
$app_list_strings['sasa_year_c_list'][2019] = '2019';
$app_list_strings['sasa_year_c_list'][2020] = '2020';
$app_list_strings['sasa_year_c_list'][''] = '';
$app_list_strings['sasa_mes_c_list']['01'] = 'Enero';
$app_list_strings['sasa_mes_c_list']['02'] = 'Febrero';
$app_list_strings['sasa_mes_c_list'][''] = '';
$app_list_strings['account_type_dom']['Customer'] = 'Cliente';
$app_list_strings['account_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['account_type_dom']['No Interesado'] = 'No Interesado';
$app_list_strings['account_type_dom']['No Registra'] = 'No Registra';
$app_list_strings['account_type_dom'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_municipio_c_list.php

 // created: 2018-05-25 13:11:59

$app_list_strings['sasa_municipio_c_list']=array (
''=>'',
'Abejorral'=>'Abejorral',
'Abrego'=>'Abrego',
'Abriaqui'=>'Abriaquí',
'Acacias'=>'Acacias',
'Acandi'=>'Acandí',
'Acevedo'=>'Acevedo',
'Achi'=>'Achí',
'Agrado'=>'Agrado',
'Agua de Dios'=>'Agua de Dios',
'Aguachica'=>'Aguachica',
'Aguada'=>'Aguada',
'Aguadas'=>'Aguadas',
'Aguazul'=>'Aguazul',
'Agustin Codazzi'=>'Agustín Codazzi',
'Aipe'=>'Aipe',
'Alban'=>'Albán',
'Albania'=>'Albania',
'Alcala'=>'Alcalá',
'Aldana'=>'Aldana',
'Alejandria'=>'Alejandría',
'Algarrobo'=>'Algarrobo',
'Algeciras'=>'Algeciras',
'Almaguer'=>'Almaguer',
'Almeida'=>'Almeida',
'Alpujarra'=>'Alpujarra',
'Altamira'=>'Altamira',
'Alto Baudo'=>'Alto Baudo',
'Altos del Rosario'=>'Altos del Rosario',
'Alvarado'=>'Alvarado',
'Amaga'=>'Amagá',
'Amalfi'=>'Amalfi',
'Ambalema'=>'Ambalema',
'Anapoima'=>'Anapoima',
'Ancuya'=>'Ancuyá',
'Andalucia'=>'Andalucía',
'Andes'=>'Andes',
'Angelopolis'=>'Angelópolis',
'Angostura'=>'Angostura',
'Anolaima'=>'Anolaima',
'Anori'=>'Anorí',
'Anserma'=>'Anserma',
'Ansermanuevo'=>'Ansermanuevo',
'Anza'=>'Anza',
'Anzoategui'=>'Anzoátegui',
'Apartado'=>'Apartadó',
'Apia'=>'Apía',
'Apulo'=>'Apulo',
'Aquitania'=>'Aquitania',
'Aracataca'=>'Aracataca',
'Aranzazu'=>'Aranzazu',
'Aratoca'=>'Aratoca',
'Arauca'=>'Arauca',
'Arauquita'=>'Arauquita',
'Arbelaez'=>'Arbeláez',
'Arboleda'=>'Arboleda',
'Arboledas'=>'Arboledas',
'Arboletes'=>'Arboletes',
'Arcabuco'=>'Arcabuco',
'Arenal'=>'Arenal',
'Argelia'=>'Argelia',
'Ariguani'=>'Ariguaní',
'Arjona'=>'Arjona',
'Armenia'=>'Armenia',
'Armero'=>'Armero',
'Arroyohondo'=>'Arroyohondo',
'Astrea'=>'Astrea',
'Ataco'=>'Ataco',
'Atrato'=>'Atrato',
'Ayapel'=>'Ayapel',
'Bagado'=>'Bagadó',
'Bahia Solano'=>'Bahía Solano',
'Bajo Baudo'=>'Bajo Baudó',
'Balboa'=>'Balboa',
'Baranoa'=>'Baranoa',
'Baraya'=>'Baraya',
'Barbacoas'=>'Barbacoas',
'Barbosa'=>'Barbosa',
'Barichara'=>'Barichara',
'Barranca de Upia'=>'Barranca de Upía',
'Barrancabermeja'=>'Barrancabermeja',
'Barrancas'=>'Barrancas',
'Barranco de Loba'=>'Barranco de Loba',
'Barranco Minas'=>'Barranco Minas',
'Barranquilla'=>'Barranquilla',
'Becerril'=>'Becerril',
'Belalcazar'=>'Belalcázar',
'Belen'=>'Belén',
'Belen de Bajira'=>'Belén de Bajira',
'Belen de Los Andaquies'=>'Belén de Los Andaquies',
'Belen de Umbria'=>'Belén de Umbría',
'Bello'=>'Bello',
'Belmira'=>'Belmira',
'Beltran'=>'Beltrán',
'Berbeo'=>'Berbeo',
'Betania'=>'Betania',
'Beteitiva'=>'Betéitiva',
'Betulia'=>'Betulia',
'Bituima'=>'Bituima',
'Boavita'=>'Boavita',
'Bochalema'=>'Bochalema',
'Bogota D.C.'=>'Bogotá D.C.',
'Bojaca'=>'Bojacá',
'Bojaya'=>'Bojaya',
'Bolivar'=>'Bolívar',
'Bosconia'=>'Bosconia',
'Boyaca'=>'Boyacá',
'Briceno'=>'Briceño',
'Bucaramanga'=>'Bucaramanga',
'Bucarasica'=>'Bucarasica',
'Buena Vista'=>'Buena Vista',
'Buenaventura'=>'Buenaventura',
'Buenavista'=>'Buenavista',
'Buenos Aires'=>'Buenos Aires',
'Buesaco'=>'Buesaco',
'Bugalagrande'=>'Bugalagrande',
'Buritica'=>'Buriticá',
'Busbanza'=>'Busbanzá',
'Cabrera'=>'Cabrera',
'Cabuyaro'=>'Cabuyaro',
'Cacahual'=>'Cacahual',
'Caceres'=>'Cáceres',
'Cachipay'=>'Cachipay',
'Cachira'=>'Cachirá',
'Cacota'=>'Cácota',
'Caicedo'=>'Caicedo',
'Caicedonia'=>'Caicedonia',
'Caimito'=>'Caimito',
'Cajamarca'=>'Cajamarca',
'Cajibio'=>'Cajibío',
'Cajica'=>'Cajicá',
'Calamar'=>'Calamar',
'Calarca'=>'Calarcá',
'Caldas'=>'Caldas',
'Caldono'=>'Caldono',
'Cali'=>'Cali',
'California'=>'California',
'Calima'=>'Calima',
'Caloto'=>'Caloto',
'Campamento'=>'Campamento',
'Campo de La Cruz'=>'Campo de La Cruz',
'Campoalegre'=>'Campoalegre',
'Campohermoso'=>'Campohermoso',
'Canalete'=>'Canalete',
'Candelaria'=>'Candelaria',
'Cantagallo'=>'Cantagallo',
'Canasgordas'=>'Cañasgordas',
'Caparrapi'=>'Caparrapí',
'Capitanejo'=>'Capitanejo',
'Caqueza'=>'Caqueza',
'Caracoli'=>'Caracolí',
'Caramanta'=>'Caramanta',
'Carcasi'=>'Carcasí',
'Carepa'=>'Carepa',
'Carmen de Apicala'=>'Carmen de Apicala',
'Carmen de Carupa'=>'Carmen de Carupa',
'Carmen del Darien'=>'Carmen del Darien',
'Carolina'=>'Carolina',
'Cartagena'=>'Cartagena',
'Cartagena del Chaira'=>'Cartagena del Chairá',
'Cartago'=>'Cartago',
'Caruru'=>'Caruru',
'Casabianca'=>'Casabianca',
'Castilla la Nueva'=>'Castilla la Nueva',
'Caucasia'=>'Caucasia',
'Cepita'=>'Cepitá',
'Cerete'=>'Cereté',
'Cerinza'=>'Cerinza',
'Cerrito'=>'Cerrito',
'Cerro San Antonio'=>'Cerro San Antonio',
'Certegui'=>'Cértegui',
'Chachagüi'=>'Chachagüí',
'Chaguani'=>'Chaguaní',
'Chalan'=>'Chalán',
'Chameza'=>'Chámeza',
'Chaparral'=>'Chaparral',
'Charala'=>'Charalá',
'Charta'=>'Charta',
'Chia'=>'Chía',
'Chigorodo'=>'Chigorodó',
'Chima'=>'Chimá',
'Chimichagua'=>'Chimichagua',
'Chinacota'=>'Chinácota',
'Chinavita'=>'Chinavita',
'Chinchina'=>'Chinchiná',
'Chinu'=>'Chinú',
'Chipaque'=>'Chipaque',
'Chipata'=>'Chipatá',
'Chiquinquira'=>'Chiquinquirá',
'Chiquiza'=>'Chíquiza',
'Chiriguana'=>'Chiriguaná',
'Chiscas'=>'Chiscas',
'Chita'=>'Chita',
'Chitaga'=>'Chitagá',
'Chitaraque'=>'Chitaraque',
'Chivata'=>'Chivatá',
'Chivolo'=>'Chivolo',
'Chivor'=>'Chivor',
'Choachi'=>'Choachí',
'Choconta'=>'Chocontá',
'Cicuco'=>'Cicuco',
'Cienaga'=>'Ciénaga',
'Cienaga de Oro'=>'Ciénaga de Oro',
'Cienega'=>'Ciénega',
'Cimitarra'=>'Cimitarra',
'Circasia'=>'Circasia',
'Cisneros'=>'Cisneros',
'Ciudad Bolivar'=>'Ciudad Bolívar',
'Clemencia'=>'Clemencia',
'Cocorna'=>'Cocorná',
'Coello'=>'Coello',
'Cogua'=>'Cogua',
'Colombia'=>'Colombia',
'Colon'=>'Colón',
'Coloso'=>'Coloso',
'Combita'=>'Cómbita',
'Concepcion'=>'Concepción',
'Concordia'=>'Concordia',
'Condoto'=>'Condoto',
'Confines'=>'Confines',
'Consaca'=>'Consaca',
'Contadero'=>'Contadero',
'Contratacion'=>'Contratación',
'Convencion'=>'Convención',
'Copacabana'=>'Copacabana',
'Coper'=>'Coper',
'Cordoba'=>'Córdoba',
'Corinto'=>'Corinto',
'Coromoro'=>'Coromoro',
'Corozal'=>'Corozal',
'Corrales'=>'Corrales',
'Cota'=>'Cota',
'Cotorra'=>'Cotorra',
'Covarachia'=>'Covarachía',
'Covenas'=>'Coveñas',
'Coyaima'=>'Coyaima',
'Cravo Norte'=>'Cravo Norte',
'Cuaspud'=>'Cuaspud',
'Cubara'=>'Cubará',
'Cubarral'=>'Cubarral',
'Cucaita'=>'Cucaita',
'Cucunuba'=>'Cucunubá',
'Cucuta'=>'Cúcuta',
'Cucutilla'=>'Cucutilla',
'Cuitiva'=>'Cuítiva',
'Cumaral'=>'Cumaral',
'Cumaribo'=>'Cumaribo',
'Cumbal'=>'Cumbal',
'Cumbitara'=>'Cumbitara',
'Cunday'=>'Cunday',
'Curillo'=>'Curillo',
'Curiti'=>'Curití',
'Curumani'=>'Curumaní',
'Dabeiba'=>'Dabeiba',
'Dagua'=>'Dagua',
'Dibula'=>'Dibula',
'Distraccion'=>'Distracción',
'Dolores'=>'Dolores',
'Don Matias'=>'Don Matías',
'Dosquebradas'=>'Dosquebradas',
'Duitama'=>'Duitama',
'Durania'=>'Durania',
'Ebejico'=>'Ebéjico',
'El aguila'=>'El Águila',
'El Bagre'=>'El Bagre',
'El Banco'=>'El Banco',
'El Cairo'=>'El Cairo',
'El Calvario'=>'El Calvario',
'El Canton del San Pablo'=>'El Cantón del San Pablo',
'El Carmen'=>'El Carmen',
'El Carmen de Atrato'=>'El Carmen de Atrato',
'El Carmen de Bolivar'=>'El Carmen de Bolívar',
'El Carmen de Chucuri'=>'El Carmen de Chucurí',
'El Carmen de Viboral'=>'El Carmen de Viboral',
'El Castillo'=>'El Castillo',
'El Cerrito'=>'El Cerrito',
'El Charco'=>'El Charco',
'El Cocuy'=>'El Cocuy',
'El Colegio'=>'El Colegio',
'El Copey'=>'El Copey',
'El Doncello'=>'El Doncello',
'El Dorado'=>'El Dorado',
'El Dovio'=>'El Dovio',
'El Encanto'=>'El Encanto',
'El Espino'=>'El Espino',
'El Guacamayo'=>'El Guacamayo',
'El Guamo'=>'El Guamo',
'El Litoral del San Juan'=>'El Litoral del San Juan',
'El Molino'=>'El Molino',
'El Paso'=>'El Paso',
'El Paujil'=>'El Paujil',
'El Penol'=>'El Peñol',
'El Penon'=>'El Peñón',
'El Pinon'=>'El Piñon',
'El Playon'=>'El Playón',
'El Reten'=>'El Retén',
'El Retorno'=>'El Retorno',
'El Roble'=>'El Roble',
'El Rosal'=>'El Rosal',
'El Rosario'=>'El Rosario',
'El Santuario'=>'El Santuario',
'El Tablon de Gomez'=>'El Tablón de Gómez',
'El Tambo'=>'El Tambo',
'El Tarra'=>'El Tarra',
'El Zulia'=>'El Zulia',
'Elias'=>'Elías',
'Encino'=>'Encino',
'Enciso'=>'Enciso',
'Entrerrios'=>'Entrerrios',
'Envigado'=>'Envigado',
'Espinal'=>'Espinal',
'Facatativa'=>'Facatativá',
'Falan'=>'Falan',
'Filadelfia'=>'Filadelfia',
'Filandia'=>'Filandia',
'Firavitoba'=>'Firavitoba',
'Flandes'=>'Flandes',
'Florencia'=>'Florencia',
'Floresta'=>'Floresta',
'Florian'=>'Florián',
'Florida'=>'Florida',
'Floridablanca'=>'Floridablanca',
'Fomeque'=>'Fomeque',
'Fonseca'=>'Fonseca',
'Fortul'=>'Fortul',
'Fosca'=>'Fosca',
'Francisco Pizarro'=>'Francisco Pizarro',
'Fredonia'=>'Fredonia',
'Fresno'=>'Fresno',
'Frontino'=>'Frontino',
'Fuente de Oro'=>'Fuente de Oro',
'Fundacion'=>'Fundación',
'Funes'=>'Funes',
'Funza'=>'Funza',
'Fuquene'=>'Fúquene',
'Fusagasuga'=>'Fusagasugá',
'Gachala'=>'Gachala',
'Gachancipa'=>'Gachancipá',
'Gachantiva'=>'Gachantivá',
'Gacheta'=>'Gachetá',
'Galan'=>'Galán',
'Galapa'=>'Galapa',
'Galeras'=>'Galeras',
'Gama'=>'Gama',
'Gamarra'=>'Gamarra',
'Gambita'=>'Gambita',
'Gameza'=>'Gameza',
'Garagoa'=>'Garagoa',
'Garzon'=>'Garzón',
'Genova'=>'Génova',
'Gigante'=>'Gigante',
'Ginebra'=>'Ginebra',
'Giraldo'=>'Giraldo',
'Girardot'=>'Girardot',
'Girardota'=>'Girardota',
'Giron'=>'Girón',
'Gomez Plata'=>'Gómez Plata',
'Gonzalez'=>'González',
'Gramalote'=>'Gramalote',
'Granada'=>'Granada',
'Guaca'=>'Guaca',
'Guacamayas'=>'Guacamayas',
'Guacari'=>'Guacarí',
'Guachene'=>'Guachené',
'Guacheta'=>'Guachetá',
'Guachucal'=>'Guachucal',
'Guadalajara de Buga'=>'Guadalajara de Buga',
'Guadalupe'=>'Guadalupe',
'Guaduas'=>'Guaduas',
'Guaitarilla'=>'Guaitarilla',
'Gualmatan'=>'Gualmatán',
'Guamal'=>'Guamal',
'Guamo'=>'Guamo',
'Guapi'=>'Guapi',
'Guapota'=>'Guapotá',
'Guaranda'=>'Guaranda',
'Guarne'=>'Guarne',
'Guasca'=>'Guasca',
'Guatape'=>'Guatapé',
'Guataqui'=>'Guataquí',
'Guatavita'=>'Guatavita',
'Guateque'=>'Guateque',
'Guatica'=>'Guática',
'Guavata'=>'Guavatá',
'Guayabal de Siquima'=>'Guayabal de Siquima',
'Guayabetal'=>'Guayabetal',
'Guayata'=>'Guayatá',
'Güepsa'=>'Güepsa',
'Güican'=>'Güicán',
'Gutierrez'=>'Gutiérrez',
'Hacari'=>'Hacarí',
'Hatillo de Loba'=>'Hatillo de Loba',
'Hato'=>'Hato',
'Hato Corozal'=>'Hato Corozal',
'Hatonuevo'=>'Hatonuevo',
'Heliconia'=>'Heliconia',
'Herran'=>'Herrán',
'Herveo'=>'Herveo',
'Hispania'=>'Hispania',
'Hobo'=>'Hobo',
'Honda'=>'Honda',
'Ibague'=>'Ibagué',
'Icononzo'=>'Icononzo',
'Iles'=>'Iles',
'Imues'=>'Imués',
'Inirida'=>'Inírida',
'Inza'=>'Inzá',
'Ipiales'=>'Ipiales',
'Iquira'=>'Iquira',
'Isnos'=>'Isnos',
'Istmina'=>'Istmina',
'Itagui'=>'Itagui',
'Ituango'=>'Ituango',
'Iza'=>'Iza',
'Jambalo'=>'Jambaló',
'Jamundi'=>'Jamundí',
'Jardin'=>'Jardín',
'Jenesano'=>'Jenesano',
'Jerico'=>'Jericó',
'Jerusalen'=>'Jerusalén',
'Jesus Maria'=>'Jesús María',
'Jordan'=>'Jordán',
'Juan de Acosta'=>'Juan de Acosta',
'Junin'=>'Junín',
'Jurado'=>'Juradó',
'La Apartada'=>'La Apartada',
'La Argentina'=>'La Argentina',
'La Belleza'=>'La Belleza',
'La Calera'=>'La Calera',
'La Capilla'=>'La Capilla',
'La Ceja'=>'La Ceja',
'La Celia'=>'La Celia',
'La Chorrera'=>'La Chorrera',
'La Cruz'=>'La Cruz',
'La Cumbre'=>'La Cumbre',
'La Dorada'=>'La Dorada',
'La Esperanza'=>'La Esperanza',
'La Estrella'=>'La Estrella',
'La Florida'=>'La Florida',
'La Gloria'=>'La Gloria',
'La Guadalupe'=>'La Guadalupe',
'La Jagua de Ibirico'=>'La Jagua de Ibirico',
'La Jagua del Pilar'=>'La Jagua del Pilar',
'La Llanada'=>'La Llanada',
'La Macarena'=>'La Macarena',
'La Merced'=>'La Merced',
'La Mesa'=>'La Mesa',
'La Montanita'=>'La Montañita',
'La Palma'=>'La Palma',
'La Paz'=>'La Paz',
'La Pedrera'=>'La Pedrera',
'La Pena'=>'La Peña',
'La Pintada'=>'La Pintada',
'La Plata'=>'La Plata',
'La Playa'=>'La Playa',
'La Primavera'=>'La Primavera',
'La Salina'=>'La Salina',
'La Sierra'=>'La Sierra',
'La Tebaida'=>'La Tebaida',
'La Tola'=>'La Tola',
'La Union'=>'La Unión',
'La Uvita'=>'La Uvita',
'La Vega'=>'La Vega',
'La Victoria'=>'La Victoria',
'La Virginia'=>'La Virginia',
'Labateca'=>'Labateca',
'Labranzagrande'=>'Labranzagrande',
'Landazuri'=>'Landázuri',
'Lebrija'=>'Lebríja',
'Leguizamo'=>'Leguízamo',
'Leiva'=>'Leiva',
'Lejanias'=>'Lejanías',
'Lenguazaque'=>'Lenguazaque',
'Lerida'=>'Lérida',
'Leticia'=>'Leticia',
'Libano'=>'Líbano',
'Liborina'=>'Liborina',
'Linares'=>'Linares',
'Lloro'=>'Lloró',
'Lopez'=>'López',
'Lorica'=>'Lorica',
'Los Andes'=>'Los Andes',
'Los Cordobas'=>'Los Córdobas',
'Los Palmitos'=>'Los Palmitos',
'Los Patios'=>'Los Patios',
'Los Santos'=>'Los Santos',
'Lourdes'=>'Lourdes',
'Luruaco'=>'Luruaco',
'Macanal'=>'Macanal',
'Macaravita'=>'Macaravita',
'Maceo'=>'Maceo',
'Macheta'=>'Macheta',
'Madrid'=>'Madrid',
'Magangue'=>'Magangué',
'Magüi'=>'Magüí',
'Mahates'=>'Mahates',
'Maicao'=>'Maicao',
'Majagual'=>'Majagual',
'Malaga'=>'Málaga',
'Malambo'=>'Malambo',
'Mallama'=>'Mallama',
'Manati'=>'Manatí',
'Manaure'=>'Manaure',
'Mani'=>'Maní',
'Manizales'=>'Manizales',
'Manta'=>'Manta',
'Manzanares'=>'Manzanares',
'Mapiripan'=>'Mapiripán',
'Mapiripana'=>'Mapiripana',
'Margarita'=>'Margarita',
'Maria la Baja'=>'María la Baja',
'Marinilla'=>'Marinilla',
'Maripi'=>'Maripí',
'Mariquita'=>'Mariquita',
'Marmato'=>'Marmato',
'Marquetalia'=>'Marquetalia',
'Marsella'=>'Marsella',
'Marulanda'=>'Marulanda',
'Matanza'=>'Matanza',
'Medellin'=>'Medellín',
'Medina'=>'Medina',
'Medio Atrato'=>'Medio Atrato',
'Medio Baudo'=>'Medio Baudó',
'Medio San Juan'=>'Medio San Juan',
'Melgar'=>'Melgar',
'Mercaderes'=>'Mercaderes',
'Mesetas'=>'Mesetas',
'Milan'=>'Milán',
'Miraflores'=>'Miraflores',
'Miranda'=>'Miranda',
'Miriti Parana'=>'Miriti Paraná',
'Mistrato'=>'Mistrató',
'Mitu'=>'Mitú',
'Mocoa'=>'Mocoa',
'Mogotes'=>'Mogotes',
'Molagavita'=>'Molagavita',
'Momil'=>'Momil',
'Mompos'=>'Mompós',
'Mongua'=>'Mongua',
'Mongui'=>'Monguí',
'Moniquira'=>'Moniquirá',
'Montebello'=>'Montebello',
'Montecristo'=>'Montecristo',
'Montelibano'=>'Montelíbano',
'Montenegro'=>'Montenegro',
'Monteria'=>'Montería',
'Monterrey'=>'Monterrey',
'Monitos'=>'Moñitos',
'Morales'=>'Morales',
'Morelia'=>'Morelia',
'Morichal'=>'Morichal',
'Morroa'=>'Morroa',
'Mosquera'=>'Mosquera',
'Motavita'=>'Motavita',
'Murillo'=>'Murillo',
'Murindo'=>'Murindó',
'Mutata'=>'Mutatá',
'Mutiscua'=>'Mutiscua',
'Muzo'=>'Muzo',
'Narino'=>'Nariño',
'Nataga'=>'Nátaga',
'Natagaima'=>'Natagaima',
'Nechi'=>'Nechí',
'Necocli'=>'Necoclí',
'Neira'=>'Neira',
'Neiva'=>'Neiva',
'Nemocon'=>'Nemocón',
'Nilo'=>'Nilo',
'Nimaima'=>'Nimaima',
'Nobsa'=>'Nobsa',
'Nocaima'=>'Nocaima',
'Norcasia'=>'Norcasia',
'Norosi'=>'Norosí',
'Novita'=>'Nóvita',
'Nueva Granada'=>'Nueva Granada',
'Nuevo Colon'=>'Nuevo Colón',
'Nunchia'=>'Nunchía',
'Nuqui'=>'Nuquí',
'Obando'=>'Obando',
'Ocamonte'=>'Ocamonte',
'Ocana'=>'Ocaña',
'Oiba'=>'Oiba',
'Oicata'=>'Oicatá',
'Olaya'=>'Olaya',
'Olaya Herrera'=>'Olaya Herrera',
'Onzaga'=>'Onzaga',
'Oporapa'=>'Oporapa',
'Orito'=>'Orito',
'Orocue'=>'Orocué',
'Ortega'=>'Ortega',
'Ospina'=>'Ospina',
'Otanche'=>'Otanche',
'Ovejas'=>'Ovejas',
'Pachavita'=>'Pachavita',
'Pacho'=>'Pacho',
'Pacoa'=>'Pacoa',
'Pacora'=>'Pácora',
'Padilla'=>'Padilla',
'Paez'=>'Páez',
'Paicol'=>'Paicol',
'Pailitas'=>'Pailitas',
'Paime'=>'Paime',
'Paipa'=>'Paipa',
'Pajarito'=>'Pajarito',
'Palermo'=>'Palermo',
'Palestina'=>'Palestina',
'Palmar'=>'Palmar',
'Palmar de Varela'=>'Palmar de Varela',
'Palmas del Socorro'=>'Palmas del Socorro',
'Palmira'=>'Palmira',
'Palmito'=>'Palmito',
'Palocabildo'=>'Palocabildo',
'Pamplona'=>'Pamplona',
'Pamplonita'=>'Pamplonita',
'Pana Pana'=>'Pana Pana',
'Pandi'=>'Pandi',
'Panqueba'=>'Panqueba',
'Papunaua'=>'Papunaua',
'Paramo'=>'Páramo',
'Paratebueno'=>'Paratebueno',
'Pasca'=>'Pasca',
'Pasto'=>'Pasto',
'Patia'=>'Patía',
'Pauna'=>'Pauna',
'Paya'=>'Paya',
'Paz de Ariporo'=>'Paz de Ariporo',
'Paz de Rio'=>'Paz de Río',
'Pedraza'=>'Pedraza',
'Pelaya'=>'Pelaya',
'Pensilvania'=>'Pensilvania',
'Penol'=>'Peñol',
'Peque'=>'Peque',
'Pereira'=>'Pereira',
'Pesca'=>'Pesca',
'Piamonte'=>'Piamonte',
'Piedecuesta'=>'Piedecuesta',
'Piedras'=>'Piedras',
'Piendamo'=>'Piendamó',
'Pijao'=>'Pijao',
'Pijino del Carmen'=>'Pijiño del Carmen',
'Pinchote'=>'Pinchote',
'Pinillos'=>'Pinillos',
'Piojo'=>'Piojó',
'Pisba'=>'Pisba',
'Pital'=>'Pital',
'Pitalito'=>'Pitalito',
'Pivijay'=>'Pivijay',
'Planadas'=>'Planadas',
'Planeta Rica'=>'Planeta Rica',
'Plato'=>'Plato',
'Policarpa'=>'Policarpa',
'Polonuevo'=>'Polonuevo',
'Ponedera'=>'Ponedera',
'Popayan'=>'Popayán',
'Pore'=>'Pore',
'Potosi'=>'Potosí',
'Pradera'=>'Pradera',
'Prado'=>'Prado',
'Providencia'=>'Providencia',
'Pueblo Bello'=>'Pueblo Bello',
'Pueblo Nuevo'=>'Pueblo Nuevo',
'Pueblo Rico'=>'Pueblo Rico',
'Pueblo Viejo'=>'Pueblo Viejo',
'Pueblorrico'=>'Pueblorrico',
'Puente Nacional'=>'Puente Nacional',
'Puerres'=>'Puerres',
'Puerto Alegria'=>'Puerto Alegría',
'Puerto Arica'=>'Puerto Arica',
'Puerto Asis'=>'Puerto Asís',
'Puerto Berrio'=>'Puerto Berrío',
'Puerto Boyaca'=>'Puerto Boyacá',
'Puerto Caicedo'=>'Puerto Caicedo',
'Puerto Carreno'=>'Puerto Carreño',
'Puerto Colombia'=>'Puerto Colombia',
'Puerto Concordia'=>'Puerto Concordia',
'Puerto Escondido'=>'Puerto Escondido',
'Puerto Gaitan'=>'Puerto Gaitán',
'Puerto Guzman'=>'Puerto Guzmán',
'Puerto Libertador'=>'Puerto Libertador',
'Puerto Lleras'=>'Puerto Lleras',
'Puerto Lopez'=>'Puerto López',
'Puerto Nare'=>'Puerto Nare',
'Puerto Narino'=>'Puerto Nariño',
'Puerto Parra'=>'Puerto Parra',
'Puerto Rico'=>'Puerto Rico',
'Puerto Rondon'=>'Puerto Rondón',
'Puerto Salgar'=>'Puerto Salgar',
'Puerto Santander'=>'Puerto Santander',
'Puerto Tejada'=>'Puerto Tejada',
'Puerto Triunfo'=>'Puerto Triunfo',
'Puerto Wilches'=>'Puerto Wilches',
'Puli'=>'Pulí',
'Pupiales'=>'Pupiales',
'Purace'=>'Puracé',
'Purificacion'=>'Purificación',
'Purisima'=>'Purísima',
'Quebradanegra'=>'Quebradanegra',
'Quetame'=>'Quetame',
'Quibdo'=>'Quibdó',
'Quimbaya'=>'Quimbaya',
'Quinchia'=>'Quinchía',
'Quipama'=>'Quípama',
'Quipile'=>'Quipile',
'Ragonvalia'=>'Ragonvalia',
'Ramiriqui'=>'Ramiriquí',
'Raquira'=>'Ráquira',
'Recetor'=>'Recetor',
'Regidor'=>'Regidor',
'Remedios'=>'Remedios',
'Remolino'=>'Remolino',
'Repelon'=>'Repelón',
'Restrepo'=>'Restrepo',
'Retiro'=>'Retiro',
'Ricaurte'=>'Ricaurte',
'Rio Blanco'=>'Rio Blanco',
'Rio de Oro'=>'Río de Oro',
'Rio Iro'=>'Río Iro',
'Rio Quito'=>'Río Quito',
'Rio Viejo'=>'Río Viejo',
'Riofrio'=>'Riofrío',
'Riohacha'=>'Riohacha',
'Rionegro'=>'Rionegro',
'Riosucio'=>'Riosucio',
'Risaralda'=>'Risaralda',
'Rivera'=>'Rivera',
'Roberto Payan'=>'Roberto Payán',
'Roldanillo'=>'Roldanillo',
'Roncesvalles'=>'Roncesvalles',
'Rondon'=>'Rondón',
'Rosas'=>'Rosas',
'Rovira'=>'Rovira',
'Sabana de Torres'=>'Sabana de Torres',
'Sabanagrande'=>'Sabanagrande',
'Sabanalarga'=>'Sabanalarga',
'Sabanas de San Angel'=>'Sabanas de San Angel',
'Sabaneta'=>'Sabaneta',
'Saboya'=>'Saboyá',
'Sacama'=>'Sácama',
'Sachica'=>'Sáchica',
'Sahagun'=>'Sahagún',
'Saladoblanco'=>'Saladoblanco',
'Salamina'=>'Salamina',
'Salazar'=>'Salazar',
'Saldana'=>'Saldaña',
'Salento'=>'Salento',
'Salgar'=>'Salgar',
'Samaca'=>'Samacá',
'Samana'=>'Samaná',
'Samaniego'=>'Samaniego',
'Sampues'=>'Sampués',
'San Agustin'=>'San Agustín',
'San Alberto'=>'San Alberto',
'San Andres'=>'San Andrés',
'San Andres de Cuerquia'=>'San Andrés de Cuerquía',
'San Andres de Tumaco'=>'San Andrés de Tumaco',
'San Andres Sotavento'=>'San Andrés Sotavento',
'San Antero'=>'San Antero',
'San Antonio'=>'San Antonio',
'San Antonio del Tequendama'=>'San Antonio del Tequendama',
'San Benito'=>'San Benito',
'San Benito Abad'=>'San Benito Abad',
'San Bernardo'=>'San Bernardo',
'San Bernardo del Viento'=>'San Bernardo del Viento',
'San Calixto'=>'San Calixto',
'San Carlos'=>'San Carlos',
'San Carlos de Guaroa'=>'San Carlos de Guaroa',
'San Cayetano'=>'San Cayetano',
'San Cristobal'=>'San Cristóbal',
'San Diego'=>'San Diego',
'San Eduardo'=>'San Eduardo',
'San Estanislao'=>'San Estanislao',
'San Felipe'=>'San Felipe',
'San Fernando'=>'San Fernando',
'San Francisco'=>'San Francisco',
'San Gil'=>'San Gil',
'San Jacinto'=>'San Jacinto',
'San Jacinto del Cauca'=>'San Jacinto del Cauca',
'San Jeronimo'=>'San Jerónimo',
'San Joaquin'=>'San Joaquín',
'San Jose'=>'San José',
'San Jose de La Montana'=>'San José de La Montaña',
'San Jose de Miranda'=>'San José de Miranda',
'San Jose de Pare'=>'San José de Pare',
'San Jose de Ure'=>'San José de Uré',
'San Jose del Fragua'=>'San José del Fragua',
'San Jose del Guaviare'=>'San José del Guaviare',
'San Jose del Palmar'=>'San José del Palmar',
'San Juan de Arama'=>'San Juan de Arama',
'San Juan de Betulia'=>'San Juan de Betulia',
'San Juan de Rio Seco'=>'San Juan de Río Seco',
'San Juan de Uraba'=>'San Juan de Urabá',
'San Juan del Cesar'=>'San Juan del Cesar',
'San Juan Nepomuceno'=>'San Juan Nepomuceno',
'San Juanito'=>'San Juanito',
'San Lorenzo'=>'San Lorenzo',
'San Luis'=>'San Luis',
'San Luis de Gaceno'=>'San Luis de Gaceno',
'San Luis de Since'=>'San Luis de Sincé',
'San Marcos'=>'San Marcos',
'San Martin'=>'San Martín',
'San Martin de Loba'=>'San Martín de Loba',
'San Mateo'=>'San Mateo',
'San Miguel'=>'San Miguel',
'San Miguel de Sema'=>'San Miguel de Sema',
'San Onofre'=>'San Onofre',
'San Pablo'=>'San Pablo',
'San Pablo de Borbur'=>'San Pablo de Borbur',
'San Pedro'=>'San Pedro',
'San Pedro de Cartago'=>'San Pedro de Cartago',
'San Pedro de Uraba'=>'San Pedro de Uraba',
'San Pelayo'=>'San Pelayo',
'San Rafael'=>'San Rafael',
'San Roque'=>'San Roque',
'San Sebastian'=>'San Sebastián',
'San Sebastian de Buenavista'=>'San Sebastián de Buenavista',
'San Vicente'=>'San Vicente',
'San Vicente de Chucuri'=>'San Vicente de Chucurí',
'San Vicente del Caguan'=>'San Vicente del Caguán',
'San Zenon'=>'San Zenón',
'Sandona'=>'Sandoná',
'Santa Ana'=>'Santa Ana',
'Santa Barbara'=>'Santa Bárbara',
'Santa Barbara de Pinto'=>'Santa Bárbara de Pinto',
'Santa Catalina'=>'Santa Catalina',
'Socha'=>'Socha',
'Santa Helena del Opon'=>'Santa Helena del Opón',
'Santa Isabel'=>'Santa Isabel',
'Santa Lucia'=>'Santa Lucía',
'Santa Maria'=>'Santa María',
'Santa Marta'=>'Santa Marta',
'Santa Rosa'=>'Santa Rosa',
'Santa Rosa de Cabal'=>'Santa Rosa de Cabal',
'Santa Rosa de Osos'=>'Santa Rosa de Osos',
'Santa Rosa de Viterbo'=>'Santa Rosa de Viterbo',
'Santa Rosa del Sur'=>'Santa Rosa del Sur',
'Santa Rosalia'=>'Santa Rosalía',
'Santa Sofia'=>'Santa Sofía',
'Santacruz'=>'Santacruz',
'Santafe de Antioquia'=>'Santafé de Antioquia',
'Santana'=>'Santana',
'Santander de Quilichao'=>'Santander de Quilichao',
'Santiago'=>'Santiago',
'Santiago de Tolu'=>'Santiago de Tolú',
'Santo Domingo'=>'Santo Domingo',
'Santo Tomas'=>'Santo Tomás',
'Santuario'=>'Santuario',
'Sapuyes'=>'Sapuyes',
'Saravena'=>'Saravena',
'Sardinata'=>'Sardinata',
'Sasaima'=>'Sasaima',
'Sativanorte'=>'Sativanorte',
'Sativasur'=>'Sativasur',
'Segovia'=>'Segovia',
'Sesquile'=>'Sesquilé',
'Sevilla'=>'Sevilla',
'Siachoque'=>'Siachoque',
'Sibate'=>'Sibaté',
'Sibundoy'=>'Sibundoy',
'Silos'=>'Silos',
'Silvania'=>'Silvania',
'Silvia'=>'Silvia',
'Simacota'=>'Simacota',
'Simijaca'=>'Simijaca',
'Simiti'=>'Simití',
'Sincelejo'=>'Sincelejo',
'Sipi'=>'Sipí',
'Sitionuevo'=>'Sitionuevo',
'Soacha'=>'Soacha',
'Soata'=>'Soatá',
'Socorro'=>'Socorro',
'Socota'=>'Socotá',
'Sogamoso'=>'Sogamoso',
'Solano'=>'Solano',
'Soledad'=>'Soledad',
'Solita'=>'Solita',
'Somondoco'=>'Somondoco',
'Sonson'=>'Sonsón',
'Sopetran'=>'Sopetrán',
'Soplaviento'=>'Soplaviento',
'Sopo'=>'Sopó',
'Sora'=>'Sora',
'Soraca'=>'Soracá',
'Sotaquira'=>'Sotaquirá',
'Sotara'=>'Sotara',
'Suaita'=>'Suaita',
'Suan'=>'Suan',
'Suarez'=>'Suárez',
'Suaza'=>'Suaza',
'Subachoque'=>'Subachoque',
'Sucre'=>'Sucre',
'Suesca'=>'Suesca',
'Supata'=>'Supatá',
'Supia'=>'Supía',
'Surata'=>'Suratá',
'Susa'=>'Susa',
'Susacon'=>'Susacón',
'Sutamarchan'=>'Sutamarchán',
'Sutatausa'=>'Sutatausa',
'Sutatenza'=>'Sutatenza',
'Tabio'=>'Tabio',
'Tado'=>'Tadó',
'Talaigua Nuevo'=>'Talaigua Nuevo',
'Tamalameque'=>'Tamalameque',
'Tamara'=>'Támara',
'Tame'=>'Tame',
'Tamesis'=>'Támesis',
'Taminango'=>'Taminango',
'Tangua'=>'Tangua',
'Taraira'=>'Taraira',
'Tarapaca'=>'Tarapacá',
'Taraza'=>'Tarazá',
'Tarqui'=>'Tarqui',
'Tarso'=>'Tarso',
'Tasco'=>'Tasco',
'Tauramena'=>'Tauramena',
'Tausa'=>'Tausa',
'Tello'=>'Tello',
'Tena'=>'Tena',
'Tenerife'=>'Tenerife',
'Tenjo'=>'Tenjo',
'Tenza'=>'Tenza',
'Teorama'=>'Teorama',
'Teruel'=>'Teruel',
'Tesalia'=>'Tesalia',
'Tibacuy'=>'Tibacuy',
'Tibana'=>'Tibaná',
'Tibasosa'=>'Tibasosa',
'Tibirita'=>'Tibirita',
'Tibu'=>'Tibú',
'Tierralta'=>'Tierralta',
'Timana'=>'Timaná',
'Timbio'=>'Timbío',
'Timbiqui'=>'Timbiquí',
'Tinjaca'=>'Tinjacá',
'Tipacoque'=>'Tipacoque',
'Tiquisio'=>'Tiquisio',
'Titiribi'=>'Titiribí',
'Toca'=>'Toca',
'Tocaima'=>'Tocaima',
'Tocancipa'=>'Tocancipá',
'Togüi'=>'Togüí',
'Toledo'=>'Toledo',
'Tolu Viejo'=>'Tolú Viejo',
'Tona'=>'Tona',
'Topaga'=>'Tópaga',
'Topaipi'=>'Topaipí',
'Toribio'=>'Toribio',
'Toro'=>'Toro',
'Tota'=>'Tota',
'Totoro'=>'Totoró',
'Trinidad'=>'Trinidad',
'Trujillo'=>'Trujillo',
'Tubara'=>'Tubará',
'Tuchin'=>'Tuchín',
'Tulua'=>'Tuluá',
'Tunja'=>'Tunja',
'Tunungua'=>'Tununguá',
'Tuquerres'=>'Túquerres',
'Turbaco'=>'Turbaco',
'Turbana'=>'Turbaná',
'Turbo'=>'Turbo',
'Turmeque'=>'Turmequé',
'Tuta'=>'Tuta',
'Tutaza'=>'Tutazá',
'Ubala'=>'Ubalá',
'Ubaque'=>'Ubaque',
'Ulloa'=>'Ulloa',
'Umbita'=>'Umbita',
'Une'=>'Une',
'Unguia'=>'Unguía',
'Union Panamericana'=>'Unión Panamericana',
'Uramita'=>'Uramita',
'Uribe'=>'Uribe',
'Uribia'=>'Uribia',
'Urrao'=>'Urrao',
'Urumita'=>'Urumita',
'Usiacuri'=>'Usiacurí',
'utica'=>'Útica',
'Valdivia'=>'Valdivia',
'Valencia'=>'Valencia',
'Valle de Guamez'=>'Valle de Guamez',
'Valle de San Jose'=>'Valle de San José',
'Valle de San Juan'=>'Valle de San Juan',
'Valledupar'=>'Valledupar',
'Valparaiso'=>'Valparaíso',
'Vegachi'=>'Vegachí',
'Velez'=>'Vélez',
'Venadillo'=>'Venadillo',
'Venecia'=>'Venecia',
'Ventaquemada'=>'Ventaquemada',
'Vergara'=>'Vergara',
'Versalles'=>'Versalles',
'Vetas'=>'Vetas',
'Viani'=>'Vianí',
'Victoria'=>'Victoria',
'Vigia del Fuerte'=>'Vigía del Fuerte',
'Vijes'=>'Vijes',
'Villa Caro'=>'Villa Caro',
'Villa de Leyva'=>'Villa de Leyva',
'Villa de San Diego de Ubate'=>'Villa de San Diego de Ubate',
'Villa del Rosario'=>'Villa del Rosario',
'Villa Rica'=>'Villa Rica',
'Villagarzon'=>'Villagarzón',
'Villagomez'=>'Villagómez',
'Villahermosa'=>'Villahermosa',
'Villamaria'=>'Villamaría',
'Villanueva'=>'Villanueva',
'Villapinzon'=>'Villapinzón',
'Villarrica'=>'Villarrica',
'Villavicencio'=>'Villavicencio',
'Villavieja'=>'Villavieja',
'Villeta'=>'Villeta',
'Viota'=>'Viotá',
'Viracacha'=>'Viracachá',
'Vista Hermosa'=>'Vista Hermosa',
'Viterbo'=>'Viterbo',
'Yacopi'=>'Yacopí',
'Yacuanquer'=>'Yacuanquer',
'Yaguara'=>'Yaguará',
'Yali'=>'Yalí',
'Yarumal'=>'Yarumal',
'Yavarate'=>'Yavaraté',
'Yolombo'=>'Yolombó',
'Yondo'=>'Yondó',
'Yopal'=>'Yopal',
'Yotoco'=>'Yotoco',
'Yumbo'=>'Yumbo',
'Zambrano'=>'Zambrano',
'Zapatoca'=>'Zapatoca',
'Zapayan'=>'Zapayán',
'Zaragoza'=>'Zaragoza',
'Zarzal'=>'Zarzal',
'Zetaquira'=>'Zetaquira',
'Zipacon'=>'Zipacón',
'Zipaquira'=>'Zipaquirá',
'Zona Bananera'=>'Zona Bananera',
 );
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_productosofrecidos_c_list.php

 // created: 2021-06-01 16:41:25

$app_list_strings['sasa_productosofrecidos_c_list']=array (
  '' => '',
  'APT' => 'APT',
  'Contrato de administracion de valores' => 'Contrato de Administraciòn de Valores',
  'EFG' => 'EFG',
  'Fideicomisos de Inversion con Destinacion Especifica' => 'Fideicomisos de Inversión con Destinación Específica',
  'Administracion de Inversiones de Fondos Mutuos de Inversion' => 'Administración de Inversiones de Fondos Mutuos de Inversión',
  'FCP OXO Propiedades Sostenibles' => 'FCP OXO Propiedades Sostenibles',
  'FCP OXO Propiedades Sostenibles COMP I Proyecto General' => 'FCP OXO Propiedades Sostenibles COMP I Proyecto General',
  'FCP OXO Propiedades Sostenibles COMP I Cartagena' => 'FCP OXO Propiedades Sostenibles COMP I Cartagena',
  'FCP OXO Propiedades Sostenibles COMP II OXO69' => 'FCP OXO Propiedades Sostenibles COMP II OXO69',
  'FCP OXO Propiedades Sostenibles COMP II Barranquilla' => 'FCP OXO Propiedades Sostenibles COMP II Barranquilla',
  'Fondo Abierto Libranza' => 'Fondo Abierto Libranza',
  'Fondo de Pensiones Voluntarias Vision' => 'Fondo de Pensiones Voluntarias Visión',
  'CXC Parte Activa' => 'CXC Parte Activa',
  'Recaudo' => 'Recaudo',
  'Pagos  Giros' => 'Pagos - Giros',
  'Fondo Cerrado mas Colombia Opportunity' => 'Fondo Cerrado mas Colombia Opportunity',
  'Fondo Alianza Acciones' => 'Fondo Alianza Acciones',
  'Fondo Alianza Liquidez Dolar' => 'Fondo Alianza Liquidez Dólar',
  'Fondo Alianza Renta Fija 90' => 'Fondo Alianza Renta Fija 90',
  'Fondo Abierto Alianza' => 'Fondo Abierto Alianza',
  'Fondo Abierto Alianza Gobierno' => 'Fondo Abierto Alianza Gobierno',
  'Fondo Abierto con Pacto de Permanencia CXC' => 'Fondo Abierto con Pacto de Permanencia CXC',
  'Fondo Cerrado Inmobiliario Alianza' => 'Fondo Cerrado Inmobiliario Alianza',
  'Fondo de Inversion Colectiva Inmuebles Fiducor Compartimento Aptos cll 92' => 'Fondo de Inversión Colectiva Inmuebles Fiducor Compartimento Aptos cll 92',
  'Fondo Alianza Renta Fija Mercados Emergentes' => 'Fondo Alianza Renta Fija Mercados Emergentes',
  'Custodia' => 'Custodia',
  'USD' => 'USD',
  'Derivados' => 'Derivados',
  'FOREX' => 'FOREX',
  'Patrimonio Autonomo de Terceros' => 'Patrimonio Autónomo de Terceros',
  'Bonos' => 'Bonos',
  'CDT' => 'CDT',
  'Repos' => 'Repos',
  'Simultaneas' => 'Simultaneas',
  'Renta Fija  Internacional' => 'Renta Fija - Internacional',
  'Colocacion Bonos' => 'Colocación Bonos',
  'TTV' => 'TTV',
  'Renta fija' => 'Renta fija',
  'PEI renta fija' => 'PEI (Renta Fija)',
  'Acciones' => 'Acciones',
  'OPA' => 'OPA',
  'Renta Variable' => 'Renta Variable',
  'Otros' => 'Otros',
  'DCEs' => 'DCE´s',
  'Factoring' => 'Factoring',
  'Confirming' => 'Confirming',
  'Flujos' => 'Flujos',
  'Pagares' => 'Pagares',
  'Sentencias Nacion Alianza' => 'Sentencias Nación Alianza',
  'Fic Alternativos Alianza' => 'Fic Alternativos Alianza',
  'Fideicomisos' => 'Fideicomisos',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_producto_cod_c_list.php

 // created: 2022-03-08 08:53:05

$app_list_strings['sasa_producto_cod_c_list']=array (
  401 => 'Fiducia de inversión',
  402 => 'Fiducia inmobiliaria',
  403 => 'Fiducia de administración',
  404 => 'Fiducia en garantía',
  405 => 'Negocios fiduciarios con entidades públicas',
  406 => 'Fiducia con recursos del sistema general de seguridad social y otros relacionados',
  407 => 'Fondos de Inversión Colectiva (FIC)',
  408 => 'Fondos de Capital Privado',
  409 => 'Custodia de Valores',
  410 => 'Fondos de Pensiones Voluntarias',
  498 => 'Otros productos de Fiduciarias',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ente_control_c_list.php

 // created: 2022-03-08 09:04:58

$app_list_strings['sasa_ente_control_c_list']=array (
  '' => '',
  1 => 'Procuraduría',
  2 => 'Contraloría',
  3 => 'Defensoría del pueblo',
  4 => 'Personerías',
  99 => 'Otros',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_desistimiento_c_list.php

 // created: 2022-03-08 09:18:01

$app_list_strings['sasa_desistimiento_c_list']=array (
  '' => '',
  1 => 'Queja o reclamo desistida por el CF',
  2 => 'Queja o reclamo no desistida por el CF',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_recepcion_c_list.php

 // created: 2022-03-08 09:30:44

$app_list_strings['sasa_recepcion_c_list']=array (
  '' => '',
  1 => 'Superintendencia Financiera de Colombia',
  2 => 'Entidad vigilada',
  3 => 'Defensor del consumidor financiero',
  9 => 'Otra (remisión por competencia)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_admision_c_list.php

 // created: 2022-03-08 09:39:57

$app_list_strings['sasa_admision_c_list']=array (
  '' => '',
  1 => 'Queja o reclamo inadmitida y/o rechazada por el DCF',
  2 => 'Queja o reclamo admitida por el DCF',
  9 => 'No Aplica',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_favoravilidad_c_list.php

 // created: 2022-03-08 09:43:25

$app_list_strings['sasa_favoravilidad_c_list']=array (
  '' => '',
  1 => 'Favorable',
  2 => 'Parcialmente favorable',
  3 => 'No favorable',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_aceptacion_c_list.php

 // created: 2022-03-08 09:47:31

$app_list_strings['sasa_aceptacion_c_list']=array (
  '' => '',
  1 => 'Respuesta final a favor del consumidor financiero aceptadas por la entidad',
  2 => 'Respuesta final a favor del consumidor financiero no aceptadas por la entidad',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_rectificacion_c_list.php

 // created: 2022-03-08 09:54:59

$app_list_strings['sasa_rectificacion_c_list']=array (
  '' => '',
  1 => 'Queja o reclamo rectificada por la entidad vigilada antes de la decisión del DCF',
  2 => 'Queja o reclamo no rectificada por la entidad vigilada antes de la decisión del DCF',
  3 => 'Queja o reclamo rectificada por la entidad vigilada después de la decisión del DCF',
  4 => 'Queja o reclamo no rectificada por la entidad vigilada después de la decisión del DCF',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_marcacion_c_list.php

 // created: 2022-03-08 11:32:58

$app_list_strings['sasa_marcacion_c_list']=array (
  '' => '',
  1 => 'Si la queja o reclamo corresponde a una réplica, es decir, si el consumidor financiero solicita reabrir la queja o reclamo por inconformidad con la respuesta dada por la entidad vigilada y/o el defensor del consumidor financiero, dentro de los dos (2) meses siguientes a la fecha final de cierre de la última respuesta',
  2 => 'Si la queja o reclamo fue reclasificada por la entidad vigilada respecto de lo que fue remitido a la superintendencia Financiera de Colombia a través del Formato “Smartsupervision - Interposición de queja o reclamo”',
  3 => 'Si el caso fue cerrado por la entidad vigilada por no ser una queja o reclamo sino otro tipo de petición ante la entidad vigilada',
  4 => 'Si la queja o reclamo fue cerrada por falta de competencia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_docrespfinal_c_list.php

 // created: 2022-03-08 15:02:08

$app_list_strings['sasa_docrespfinal_c_list']=array (
  '' => '',
  1 => 'Si',
  2 => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoidentificacion_c_list.php

 // created: 2022-03-10 09:23:04

$app_list_strings['sasa_tipoidentificacion_c_list']=array (
  '' => '',
  'Cedula de Ciudadania' => 'Cédula de Ciudadanía',
  'Cedula de Extranjeria' => 'Cédula de Extranjería',
  'Pasaporte' => 'Pasaporte',
  'Nit' => 'Nit',
  'NUIP' => 'NUIP',
  'Registro Civil' => 'Registro Civil',
  'Tarjeta de identidad' => 'Tarjeta de Identidad',
  'Otro' => 'Otro',
  'Carne Diplomatico' => 'Carné Diplomatico',
  'Fideicomiso' => 'Fideicomiso',
  'Sociedad Extranjera' => 'Sociedad Extranjera',
  'No Registra' => 'No Registra',
  8 => 'Permiso especial de permanencia (PEP)',
  10 => 'Permiso por protección temporal (PPT)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoentidad_c_list.php

 // created: 2022-03-14 09:25:31

$app_list_strings['sasa_tipoentidad_c_list']=array (
  '' => '',
  5 => 'Alianza Fiduciaria',
  85 => 'Alianza Valores',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_codigoentidad_c_list.php

 // created: 2022-03-14 09:36:30

$app_list_strings['sasa_codigoentidad_c_list']=array (
  '' => '',
  51 => 'Alianza Valores',
  16 => 'Alianza Fiduciaria',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_lead_status_dom.php

 // created: 2018-05-23 16:05:07

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'New',
  'Assigned' => 'Assigned',
  'In Process' => 'In Process',
  'Converted' => 'Converted',
  'Recycled' => 'Recycled',
  'Dead' => 'Dead',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_segmentocuentas_c_list.php

 // created: 2021-06-01 16:43:02

$app_list_strings['sasa_segmentocuentas_c_list']=array (
  '' => '',
  'Preferenciales' => 'Preferenciales',
  'Renta Alta' => 'Renta Alta',
  'Banca Privada' => 'Banca Privada',
  'PYME' => 'PYME',
  'Empresarial' => 'Empresarial',
  'Banca Corporativa' => 'Banca Corporativa',
  'Institucional' => 'Institucional',
  'No Registra' => 'No Registra',
  'Retail' => 'Retail',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_pais_c_list.php

 // created: 2018-06-07 19:49:51

$app_list_strings['sasa_pais_c_list']=array (
  	'' => '',
  	'Colombia' => 'Colombia',
  	'Afganistan'=>'Afganistán',
	'Albania'=>'Albania',
	'Alemania'=>'Alemania',
	'Andorra'=>'Andorra',
	'Angola'=>'Angola',
	'Antigua y Barbuda'=>'Antigua y Barbuda',
	'Arabia Saudita'=>'Arabia Saudita',
	'Argelia'=>'Argelia',
	'Argentina'=>'Argentina',
	'Armenia'=>'Armenia',
	'Australia'=>'Australia',
	'Austria'=>'Austria',
	'Azerbaiyan'=>'Azerbaiyán',
	'Bahamas'=>'Bahamas',
	'Banglades'=>'Bangladés',
	'Barbados'=>'Barbados',
	'Barein'=>'Baréin',
	'Belgica'=>'Bélgica',
	'Belice'=>'Belice',
	'Benin'=>'Benín',
	'Bielorrusia'=>'Bielorrusia',
	'Birmania'=>'Birmania',
	'Bolivia'=>'Bolivia',
	'Bosnia y Herzegovina'=>'Bosnia y Herzegovina',
	'Botsuana'=>'Botsuana',
	'Brasil'=>'Brasil',
	'Brunei'=>'Brunéi',
	'Bulgaria'=>'Bulgaria',
	'Burkina Faso'=>'Burkina Faso',
	'Burundi'=>'Burundi',
	'Butan'=>'Bután',
	'Cabo Verde'=>'Cabo Verde',
	'Camboya'=>'Camboya',
	'Camerun'=>'Camerún',
	'Canada'=>'Canadá',
	'Catar'=>'Catar',
	'Chad'=>'Chad',
	'Chile'=>'Chile',
	'China'=>'China',
	'Chipre'=>'Chipre',
	'Ciudad del Vaticano'=>'Ciudad del Vaticano',
	'Comoras'=>'Comoras',
	'Corea del Norte'=>'Corea del Norte',
	'Corea del Sur'=>'Corea del Sur',
	'Costa de Marfil'=>'Costa de Marfil',
	'Costa Rica'=>'Costa Rica',
	'Croacia'=>'Croacia',
	'Cuba'=>'Cuba',
	'Dinamarca'=>'Dinamarca',
	'Dominica'=>'Dominica',
	'Ecuador'=>'Ecuador',
	'Egipto'=>'Egipto',
	'El Salvador'=>'El Salvador',
	'Emiratos Arabes Unidos'=>'Emiratos Árabes Unidos',
	'Eritrea'=>'Eritrea',
	'Eslovaquia'=>'Eslovaquia',
	'Eslovenia'=>'Eslovenia',
	'España'=>'España',
	'Estados Unidos'=>'Estados Unidos',
	'Estonia'=>'Estonia',
	'Etiopia'=>'Etiopía',
	'Filipinas'=>'Filipinas',
	'Finlandia'=>'Finlandia',
	'Fiyi'=>'Fiyi',
	'Francia'=>'Francia',
	'Gabon'=>'Gabón',
	'Gambia'=>'Gambia',
	'Georgia'=>'Georgia',
	'Ghana'=>'Ghana',
	'Granada'=>'Granada',
	'Grecia'=>'Grecia',
	'Guatemala'=>'Guatemala',
	'Guyana'=>'Guyana',
	'Guinea'=>'Guinea',
	'Guinea ecuatorial'=>'Guinea ecuatorial',
	'Guinea-Bisau'=>'Guinea-Bisáu',
	'Haiti'=>'Haití',
	'Honduras'=>'Honduras',
	'Hungria'=>'Hungría',
	'India'=>'India',
	'Indonesia'=>'Indonesia',
	'Irak'=>'Irak',
	'Iran'=>'Irán',
	'Irlanda'=>'Irlanda',
	'Islandia'=>'Islandia',
	'Islas Marshall'=>'Islas Marshall',
	'Islas Salomon'=>'Islas Salomón',
	'Israel'=>'Israel',
	'Italia'=>'Italia',
	'Jamaica'=>'Jamaica',
	'Japon'=>'Japón',
	'Jordania'=>'Jordania',
	'Kazajistan'=>'Kazajistán',
	'Kenia'=>'Kenia',
	'Kirguistan'=>'Kirguistán',
	'Kiribati'=>'Kiribati',
	'Kuwait'=>'Kuwait',
	'Laos'=>'Laos',
	'Lesoto'=>'Lesoto',
	'Letonia'=>'Letonia',
	'Libano'=>'Líbano',
	'Liberia'=>'Liberia',
	'Libia'=>'Libia',
	'Liechtenstein'=>'Liechtenstein',
	'Lituania'=>'Lituania',
	'Luxemburgo'=>'Luxemburgo',
	'Madagascar'=>'Madagascar',
	'Malasia'=>'Malasia',
	'Malaui'=>'Malaui',
	'Maldivas'=>'Maldivas',
	'Mali'=>'Malí',
	'Malta'=>'Malta',
	'Marruecos'=>'Marruecos',
	'Mauricio'=>'Mauricio',
	'Mauritania'=>'Mauritania',
	'Mexico'=>'México',
	'Micronesia'=>'Micronesia',
	'Moldavia'=>'Moldavia',
	'Monaco'=>'Mónaco',
	'Mongolia'=>'Mongolia',
	'Montenegro'=>'Montenegro',
	'Mozambique'=>'Mozambique',
	'Namibia'=>'Namibia',
	'Nauru'=>'Nauru',
	'Nepal'=>'Nepal',
	'Nicaragua'=>'Nicaragua',
	'Niger'=>'Níger',
	'Nigeria'=>'Nigeria',
	'Noruega'=>'Noruega',
	'Nueva Zelanda'=>'Nueva Zelanda',
	'Oman'=>'Omán',
	'Paises Bajos'=>'Países Bajos',
	'Pakistan'=>'Pakistán',
	'Palaos'=>'Palaos',
	'Panama'=>'Panamá',
	'Papua Nueva Guinea'=>'Papúa Nueva Guinea',
	'Paraguay'=>'Paraguay',
	'Peru'=>'Perú',
	'Polonia'=>'Polonia',
	'Portugal'=>'Portugal',
	'Reino Unido'=>'Reino Unido',
	'Republica Centroafricana'=>'República Centroafricana',
	'Republica Checa'=>'República Checa',
	'Republica de Macedonia'=>'República de Macedonia',
	'Republica del Congo'=>'República del Congo',
	'Republica Democratica del Congo'=>'República Democrática del Congo',
	'Republica Dominicana'=>'República Dominicana',
	'Republica Sudafricana'=>'República Sudafricana',
	'Ruanda'=>'Ruanda',
	'Rumania'=>'Rumanía',
	'Rusia'=>'Rusia',
	'Samoa'=>'Samoa',
	'San Cristobal y Nieves'=>'San Cristóbal y Nieves',
	'San Marino'=>'San Marino',
	'San Vicente y las Granadinas'=>'San Vicente y las Granadinas',
	'Santa Lucia'=>'Santa Lucía',
	'Santo Tome y Principe'=>'Santo Tomé y Príncipe',
	'Senegal'=>'Senegal',
	'Serbia'=>'Serbia',
	'Seychelles'=>'Seychelles',
	'Sierra Leona'=>'Sierra Leona',
	'Singapur'=>'Singapur',
	'Siria'=>'Siria',
	'Somalia'=>'Somalia',
	'Sri Lanka'=>'Sri Lanka',
	'Suazilandia'=>'Suazilandia',
	'Sudan'=>'Sudán',
	'Sudan del Sur'=>'Sudán del Sur',
	'Suecia'=>'Suecia',
	'Suiza'=>'Suiza',
	'Surinam'=>'Surinam',
	'Tailandia'=>'Tailandia',
	'Tanzania'=>'Tanzania',
	'Tayikistan'=>'Tayikistán',
	'Timor Oriental'=>'Timor Oriental',
	'Togo'=>'Togo',
	'Tonga'=>'Tonga',
	'Trinidad y Tobago'=>'Trinidad y Tobago',
	'Tunez'=>'Túnez',
	'Turkmenistan'=>'Turkmenistán',
	'Turquia'=>'Turquía',
	'Tuvalu'=>'Tuvalu',
	'Ucrania'=>'Ucrania',
	'Uganda'=>'Uganda',
	'Uruguay'=>'Uruguay',
	'Uzbekistan'=>'Uzbekistán',
	'Vanuatu'=>'Vanuatu',
	'Venezuela'=>'Venezuela',
	'Vietnam'=>'Vietnam',
	'Yemen'=>'Yemen',
	'Yibuti'=>'Yibuti',
	'Zambia'=>'Zambia',
	'Zimbabue'=>'Zimbabue',
	'No Registra' => 'No Registra',
  	'Otro' => 'Otro',
  	'Anguila' => 'Anguila',
    'Antillas Neerlandesas' => 'Antillas Neerlandesas',
    'Antartida' => 'Antártida',
    'Samoa Americana' => 'Samoa Americana',
    'Aruba' => 'Aruba',
    'Islas Aland' => 'Islas Áland',
    'San Bartolome' => 'San Bartolomé',
    'Bermudas' => 'Bermudas',
    'Bhutan' => 'Bhután',
    'Isla Bouvet' => 'Isla Bouvet',
    'Belarus' => 'Belarús',
    'Islas Cocos' => 'Islas Cocos',
    'Congo' => 'Congo',
    'Islas Cook' => 'Islas Cook',
    'Islas Christmas' => 'Islas Christmas',
    'Sahara Occidental' => 'Sahara Occidental',
    'Islas Malvinas' => 'Islas Malvinas',
    'Islas Faroe' => 'Islas Faroe',
    'Guayana Francesa' => 'Guayana Francesa',
    'Guernsey' => 'Guernsey',
    'Gibraltar' => 'Gibraltar',
    'Groenlandia' => 'Groenlandia',
    'Guadalupe' => 'Guadalupe',
    'Georgia del Sur e Islas Sandwich del Sur' => 'Georgia del Sur e Islas Sandwich del Sur',
    'Guam' => 'Guam',
    'Hong Kong' => 'Hong Kong',
    'Islas Heard y McDonald' => 'Islas Heard y McDonald',
    'Isla de Man' => 'Isla de Man',
    'Territorio Britanico del Oceano Indico' => 'Territorio Británico del Océano Índico',
    'Jersey' => 'Jersey',
    'Islas Caiman' => 'Islas Caimán',
    'Macedonia' => 'Macedonia',
    'Myanmar' => 'Myanmar',
    'Macao' => 'Macao',
    'Martinica' => 'Martinica',
    'Montserrat' => 'Montserrat',
    'Nueva Caledonia' => 'Nueva Caledonia',
    'Islas Norkfolk' => 'Islas Norkfolk',
    'Niue' => 'Niue',
    'Polinesia Francesa' => 'Polinesia Francesa',
    'San Pedro y Miquelon' => 'San Pedro y Miquelón',
    'Islas Pitcairn' => 'Islas Pitcairn',
    'Puerto Rico' => 'Puerto Rico',
    'Palestina' => 'Palestina',
    'Reunion' => 'Reunión',
    'Santa Elena' => 'Santa Elena',
    'Islas Svalbard y Jan Mayen' => 'Islas Svalbard y Jan Mayen',
    'Islas Turcas y Caicos' => 'Islas Turcas y Caicos',
    'Territorios Australes Franceses' => 'Territorios Australes Franceses',
    'Tokelau' => 'Tokelau',
    'Taiwan' => 'Taiwán',
    'Islas Virgenes Britanicas' => 'Islas Vírgenes Británicas',
    'Islas Virgenes de los Estados Unidos de America' => 'Islas Vírgenes de los Estados Unidos de América',
    'Wallis y Futuna' => 'Wallis y Futuna',
    'Mayotte' => 'Mayotte',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_genero_c_list.php

 // created: 2022-05-18 16:40:40

$app_list_strings['sasa_genero_c_list']=array (
  '' => '',
  'Femenino' => 'Femenino',
  'Masculino' => 'Masculino',
  'Otro' => 'Otro',
  'Trans' => 'Trans',
  'No aplica' => 'No aplica',
  'No binario' => 'No binario',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoentidad_c_list.Tipificaciones.php
 
$app_list_strings['sasa_tipoentidad_c_list'] = array (
  '' => '',
  5 => 'Alianza Fiduciaria',
  85 => 'Alianza Valores',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivotipi_c_list.Tipificaciones.php
 
$app_list_strings['sasa_motivotipi_c_list'] = array (
  '' => '',
  'Queja' => 'Queja',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.SaldosyMovimientosAFV.Tipificaciones.php
 
$app_list_strings['sasa_categoria_c_list'] = array (
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_categoria_c_list.Tipificaciones.php
 
$app_list_strings['sasa_categoria_c_list'] = array (
  '' => '',
  'trato inadecuado' => 'Trato Inadecuado',
  'inconsistencias' => 'Inconsistencias',
  'incumplimiento' => 'Incumplimiento',
  'informacion' => 'Información',
  'Aplazamiento' => 'Aplazamiento',
  'fallas y_o errores' => 'Fallas y/o Errores',
  'datos_personales' => 'Datos Personales',
  'reporte ante centrales' => 'Reportes antes Centrales',
  'otros_motivos' => 'Otros Motivos',
  'desvalorizacion_perdidas_diferencias' => 'Desvalorización Perdidas y Diferencias',
  'negativao_demora_en_el_pago' => 'Negativa o demora en el pago',
  'suplantacion_y_o_fraude' => 'Suplantación y/o Fraude',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivo_c_list.Tipificaciones.php
 
$app_list_strings['sasa_motivo_c_list'] = array (
  '' => '',
  401 => 'Cobro de penalidad por desistimiento',
  402 => 'Inconsistencia o falta de pago de la cláusula penal por incumplimiento o demora del proyecto inmobiliario',
  403 => 'Incumplimiento de inversiones con políticas definidas',
  404 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  405 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  406 => 'Inconsistencia en el valor de rendimientos',
  407 => 'Redención de derechos o participaciones - cancelacion del producto o servicio',
  408 => 'Incumplimiento del deber legal de rendición de cuentas (información periódica)',
  499 => 'Otros motivos',
  906 => 'Mal trato por parte de un funcionario',
  907 => 'Mal trato por parte del asesor comercial o proveedor',
  965 => 'Indebido deber de asesoría',
  938 => 'Inconsistencias en los pagos a terceros',
  961 => 'Inconsistencias en el movimiento y saldo total del producto',
  909 => 'Incumplimiento de los términos del contrato',
  902 => 'Dificultad en el acceso a la información',
  903 => 'Información o asesoría incompleta y/o errada',
  904 => 'Información inoportuna',
  945 => 'Dificultad o imposibilidad para realizar transacciones o consulta de información por el canal',
  949 => 'Errores en el contenido de la información en informes, extractos o reportes.',
  964 => 'Información sujeta a reserva',
  905 => 'Dificultad en la comunicación con la entidad',
  946 => 'Demora en la atención o en el servicio requerido',
  928 => 'Demora en la respuesta a quejas, reclamos o peticiones',
  942 => 'Demora o no aplicación del pago',
  921 => 'Demora o no devolución de saldos, aportes o primas',
  926 => 'No disponibilidad o fallas de los canales de atención',
  963 => 'Fallas o inoportunidad en el proceso de vinculación',
  943 => 'Error en la aplicación del pago',
  955 => 'Error en la facturación o cobro no pactado',
  929 => 'Errores en la resolución de quejas, reclamos o peticiones.',
  934 => 'Actualización equivocada de datos personales',
  933 => 'Demora o no modificación de datos personales',
  935 => 'Inadecuado tratamiento de datos personales',
  910 => 'Presunta suplantación de personas',
  908 => 'Presunta actuación fraudulenta o no ética del personal',
  931 => 'Reporte injustificado a centrales de riesgo',
  932 => 'No levantamiento de reporte negativo a centrales de riesgo',
  916 => 'Vinculación no autorizada',
  901 => 'Publicidad engañosa',
  917 => 'Condicionamiento a la adquisición de productos o servicios',
  962 => 'Inconformidad con procesos internos de conocimiento del cliente y SARLAFT',
  954 => 'Incrementos de tarifas no pactadas o informadas',
  950 => 'Limitación en la expedición de certificaciones',
  956 => 'Modificación de condiciones en contratos',
  918 => 'No cancelación o terminación de los productos',
  920 => 'No entrega de paz y salvo',
  930 => 'No resolución a quejas, peticiones y reclamos',
  927 => 'Obstáculo para la interposición de quejas, reclamos o peticiones',
  948 => 'Omisión o envío tardío o inoportuno de informes, extractos o reportes a los que esté obligada la entidad.',
  952 => 'Producto terminado o cancelado sin justificación',
  947 => 'Seguridad en canales',
  940 => 'Transacción no reconocida',
  513 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  514 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  515 => 'Inconsistencia en el valor de rendimientos',
  501 => 'Inconsistencia en la compra, intercambio, transferencia, traspaso y/o redención',
  505 => 'Incumplimiento en instrucción del cliente (ejecución operación)',
  512 => 'Incumplimiento de inversiones con políticas definidas',
  502 => 'Desvalorización por riesgos del mercado',
  511 => 'Perdida o desvalorización unidad',
  966 => 'Fallas en operaciones en moneda extranjera',
  967 => 'Diferencias en monetización',
  504 => 'Errores en la colocación y adjudicación de valores',
  510 => 'Negativa o demora en el pago',
  923 => 'Negación injustificada a la apertura del producto',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_parentesco_c_list.php

 // created: 2018-10-05 15:27:23

$app_list_strings['sasa_parentesco_c_list']=array (
  '' => '',
  'Padre' => 'Padre',
  'Madre' => 'Madre',
  'Esposo_a' => 'Cónyuge',
  'Hijo_a' => 'Hijo/a',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sasa_tipificaciones.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_sasa_tipificaciones'] = 'Tipificaciones';
$app_list_strings['moduleListSingular']['sasa_sasa_tipificaciones'] = 'Tipificación';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_segmento_c_list.php

 // created: 2022-06-25 19:24:49

$app_list_strings['sasa_segmento_c_list']=array (
  '' => '',
  'Banca Privada' => 'Banca Privada',
  'Canales' => 'Canales',
  'Corporativo' => 'Corporativo',
  'Empresarial' => 'Empresarial',
  'Pyme' => 'Pyme',
  'Farmers' => 'Farmers',
  'Preferencial' => 'Preferencial',
  'Renta Alta' => 'Renta Alta',
  'Retail Bogota' => 'Retail Bogotá',
  'Retail Nacional' => 'Retail Nacional',
  'Institucional' => 'Institucional',
  'Empresarial y Pyme' => 'Empresarial & Pyme',
  'Parte Activa CXC' => 'Parte Activa CXC',
  'Banca Privada 2' => 'Banca Privada 2',
  'Renta Alta 1' => 'Renta Alta 1',
  'Renta Alta 2' => 'Renta Alta 2',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_superior1_c_list.php

 // created: 2022-06-25 19:22:49

$app_list_strings['sasa_superior1_c_list']=array (
  '' => '',
  'Director Comercial de Canales Externos' => 'Director Comercial de Canales Externos',
  'Director Comercial Grupo Inversion 1' => 'Director Comercial Grupo Inversión 1',
  'Director Comercial Grupo Inversion 2' => 'Director Comercial Grupo Inversión 2',
  'Director Comercial Nacional Banca Corporativa' => 'Director Comercial Nacional Banca Corporativa',
  'Gerente Nacional Negocios de Inversion' => 'Gerente Nacional Negocios de Inversión',
  'Gerente Regional Barranquilla' => 'Gerente Regional Barranquilla',
  'Gerente Regional Cali' => 'Gerente Regional Cali',
  'Gerente Regional Medellin' => 'Gerente Regional Medellín',
  'Vicepresidente Distribucion y Ventas' => 'Vicepresidente Distribución y Ventas',
  'Gerente Regional Eje Cafetero' => 'Gerente Regional Eje Cafetero',
  'Gerente Fondos Alternativos' => 'Gerente Fondos Alternativos',
  'Director Comercial Grupo Inversion Renta Alta 1' => 'Director Comercial Grupo Inversión Renta Alta 1',
  'Director Comercial Grupo Inversion Renta Alta 2' => 'Director Comercial Grupo Inversión Renta Alta 2',
  'Gerencia SAC' => 'Gerencia SAC',
  'Gerente Banca Privada' => 'Gerente Banca Privada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_account_type_dom.php

 // created: 2022-06-28 09:42:54

$app_list_strings['account_type_dom']=array (
  '' => '',
  'Customer' => 'Cliente',
  'Prospect' => 'Prospecto',
  'No Interesado' => 'No Interesado',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_negocios_c_list.php

 // created: 2022-09-08 20:11:02

$app_list_strings['sasa_negocios_c_list']=array (
  'Inversiones' => 'Inversiones',
  'Fiduciaria' => 'Fiduciaria',
  'Valores' => 'Valores',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivoexp_c_list.php

 // created: 2022-09-01 20:14:53

$app_list_strings['sasa_motivoexp_c_list']=array (
  1 => 'Calidad servicio prestado',
  2 => 'Cumplimiento de horario',
  3 => 'Oportunidad en la atención',
  4 => 'Resolución a la solicitud',
  5 => 'Cumplimiento tiempos de respuesta',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_pqrs_c_list.php

 // created: 2022-09-09 13:38:48

$app_list_strings['sasa_pqrs_c_list']=array (
  'Si' => 'Si',
  'No' => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_presencial_c_list.php

 // created: 2022-09-15 22:18:34

$app_list_strings['sasa_presencial_c_list']=array (
  'Si' => 'Si',
  'No' => 'No',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_ctrl_sec_c_list.php

 // created: 2022-09-15 16:39:24

$app_list_strings['sasa_ctrl_sec_c_list']=array (
  '' => '',
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_lista_respuesta_c_list.php

 // created: 2022-09-15 16:46:35

$app_list_strings['sasa_lista_respuesta_c_list']=array (
  '' => '',
  0 => 'Si',
  1 => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_mostrardetalle_c_list.php

 // created: 2022-09-26 22:24:18

$app_list_strings['sasa_mostrardetalle_c_list']=array (
  1 => 'Si',
  2 => 'No',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipodesolicitud_c_list.php

 // created: 2022-08-25 21:22:09

$app_list_strings['sasa_tipodesolicitud_c_list']=array (
  'peticion' => 'Petición',
  'quejas' => 'Quejas',
  '' => '',
  'sugerencia' => 'Sugerencia',
  'felicitaciones' => 'Felicitaciones',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoproducto_c_list.php

 // created: 2022-09-08 20:04:16

$app_list_strings['sasa_tipoproducto_c_list']=array (
  1 => 'Fondo Abierto Alianza sin Pacto de Permanencia Mínima por Compartimientos',
  2 => 'Fondo Liquidez',
  3 => 'Fondo de Inversiones Colectiva Abierto con Pacto de Permanencia Alianza Acciones Colombia',
  4 => 'FIC Alianza Renta Fija 90',
  5 => 'Fondo Alianza Liquidez Dólar',
  6 => 'Fondo Alianza Renta Fija Mercados Emergentes',
  7 => 'FIC mercado Monetario Alianza',
  8 => 'FIC Adcap Multiplazos',
  9 => 'FIC Adcap Renta fija Colombia',
  10 => 'FIC Invertir Gestionado',
  11 => 'Fondo Abierto con pacto de permanencia Renovable Alternativos Alianza',
  12 => 'FIC Cerrado Sentencias Nación Alianza',
  13 => 'Cartera Colectiva Abierta con Pacto de Permanencia CxC',
  14 => 'Fondo Abierto Cahs Conservador',
  15 => 'Fondo de Pensiones de Jubilación e Invalidez Visión',
  16 => 'Fondo capital privado',
  17 => 'Inmobiliaria',
  18 => 'Administración',
  19 => 'Contrato de comisión',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipo_tarea_list_list.php

 // created: 2022-09-15 16:49:11

$app_list_strings['sasa_tipo_tarea_list_list']=array (
  '' => '',
  1 => 'Opción selección valor lista',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_puntorecepcion_c_list.php

 // created: 2022-03-08 09:34:55

$app_list_strings['sasa_puntorecepcion_c_list']=array (
  '' => '',
  3 => 'Correo electrónico',
  4 => 'Oficina',
  5 => 'Call center',
  99 => 'Otros Puntos de recepción',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_source_list.php
 
$app_list_strings['source_list'] = array (
  '' => '',
  5 => 'Centro de atención telefónica (Call center/Contac center)',
  9 => 'Corresponsales físicos propios',
  14 => 'Oficinas',
  1 => 'Aplicaciones móviles',
  13 => 'Internet',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_condicionespecial_c_list.php

 // created: 2022-06-30 15:52:11

$app_list_strings['sasa_condicionespecial_c_list']=array (
  '' => '',
  1 => 'Adulto mayor',
  2 => 'Pensionado',
  3 => 'Receptor de subsidio',
  4 => 'Discapacidad auditiva',
  5 => 'Discapacidad física',
  6 => 'Menor de edad',
  7 => 'Indígena',
  8 => 'Mujer embarazada',
  9 => 'Reinsertado',
  10 => 'Víctima del conflicto armado',
  11 => 'Afrocolombiano',
  12 => 'Desplazado',
  13 => 'Madre cabeza de familia',
  14 => 'Sordomudo',
  15 => 'Discapacidad cognitiva',
  16 => 'Discapacidad visual',
  17 => 'Periodista',
  90 => 'Otra',
  98 => 'No aplica',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_lgbtiq_c_list.php

 // created: 2022-04-29 20:31:58

$app_list_strings['sasa_lgbtiq_c_list']=array (
  '' => '',
  1 => 'Pertenece a LGBTIQ+',
  2 => 'No Pertenece a LGBTIQ+',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_departamento_c_list.cuentas2510.php
 
$app_list_strings['sasa_departamento_c_list'] = array (
  '' => '',
  'Amazonas' => 'Amazonas',
  'Antioquia' => 'Antioquia',
  'Arauca' => 'Arauca',
  'Archipielago de San Andres Providencia y Santa Catalina' => 'Archipiélago de San Andrés, Providencia y Santa Catalina',
  'Atlantico' => 'Atlántico',
  'Bogota D.C.' => 'Bogotá D.C.',
  'Bolivar' => 'Bolívar',
  'Boyaca' => 'Boyacá',
  'Caldas' => 'Caldas',
  'Caqueta' => 'Caquetá',
  'Casanare' => 'Casanare',
  'Cauca' => 'Cauca',
  'Cesar' => 'Cesar',
  'Choco' => 'Chocó',
  'Cundinamarca' => 'Cundinamarca',
  'Cordoba' => 'Córdoba',
  'Guainia' => 'Guainía',
  'Guaviare' => 'Guaviare',
  'Huila' => 'Huila',
  'La Guajira' => 'La Guajira',
  'Magdalena' => 'Magdalena',
  'Meta' => 'Meta',
  'Narino' => 'Nariño',
  'Norte de Santander' => 'Norte de Santander',
  'Putumayo' => 'Putumayo',
  'Quindio' => 'Quindío',
  'Risaralda' => 'Risaralda',
  'Santander' => 'Santander',
  'Sucre' => 'Sucre',
  'Tolima' => 'Tolima',
  'Valle del Cauca' => 'Valle del Cauca',
  'Vaupes' => 'Vaupés',
  'Vichada' => 'Vichada',
  'No Registra' => 'No Registra',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_municipio_c_list.cuentas2510.php
 
$app_list_strings['sasa_municipio_c_list'] = array (
  '' => '',
  'Abejorral' => 'Abejorral',
  'Abrego' => 'Abrego',
  'Abriaqui' => 'Abriaquí',
  'Acacias' => 'Acacias',
  'Acandi' => 'Acandí',
  'Acevedo' => 'Acevedo',
  'Achi' => 'Achí',
  'Agrado' => 'Agrado',
  'Agua de Dios' => 'Agua de Dios',
  'Aguachica' => 'Aguachica',
  'Aguada' => 'Aguada',
  'Aguadas' => 'Aguadas',
  'Aguazul' => 'Aguazul',
  'Agustin Codazzi' => 'Agustín Codazzi',
  'Aipe' => 'Aipe',
  'Alban' => 'Albán',
  'Albania' => 'Albania',
  'Alcala' => 'Alcalá',
  'Aldana' => 'Aldana',
  'Alejandria' => 'Alejandría',
  'Algarrobo' => 'Algarrobo',
  'Algeciras' => 'Algeciras',
  'Almaguer' => 'Almaguer',
  'Almeida' => 'Almeida',
  'Alpujarra' => 'Alpujarra',
  'Altamira' => 'Altamira',
  'Alto Baudo' => 'Alto Baudo',
  'Altos del Rosario' => 'Altos del Rosario',
  'Alvarado' => 'Alvarado',
  'Amaga' => 'Amagá',
  'Amalfi' => 'Amalfi',
  'Ambalema' => 'Ambalema',
  'Anapoima' => 'Anapoima',
  'Ancuya' => 'Ancuyá',
  'Andalucia' => 'Andalucía',
  'Andes' => 'Andes',
  'Angelopolis' => 'Angelópolis',
  'Angostura' => 'Angostura',
  'Anolaima' => 'Anolaima',
  'Anori' => 'Anorí',
  'Anserma' => 'Anserma',
  'Ansermanuevo' => 'Ansermanuevo',
  'Anza' => 'Anza',
  'Anzoategui' => 'Anzoátegui',
  'Apartado' => 'Apartadó',
  'Apia' => 'Apía',
  'Apulo' => 'Apulo',
  'Aquitania' => 'Aquitania',
  'Aracataca' => 'Aracataca',
  'Aranzazu' => 'Aranzazu',
  'Aratoca' => 'Aratoca',
  'Arauca' => 'Arauca',
  'Arauquita' => 'Arauquita',
  'Arbelaez' => 'Arbeláez',
  'Arboleda' => 'Arboleda',
  'Arboledas' => 'Arboledas',
  'Arboletes' => 'Arboletes',
  'Arcabuco' => 'Arcabuco',
  'Arenal' => 'Arenal',
  'Argelia' => 'Argelia',
  'Ariguani' => 'Ariguaní',
  'Arjona' => 'Arjona',
  'Armenia' => 'Armenia',
  'Armero' => 'Armero',
  'Arroyohondo' => 'Arroyohondo',
  'Astrea' => 'Astrea',
  'Ataco' => 'Ataco',
  'Atrato' => 'Atrato',
  'Ayapel' => 'Ayapel',
  'Bagado' => 'Bagadó',
  'Bahia Solano' => 'Bahía Solano',
  'Bajo Baudo' => 'Bajo Baudó',
  'Balboa' => 'Balboa',
  'Baranoa' => 'Baranoa',
  'Baraya' => 'Baraya',
  'Barbacoas' => 'Barbacoas',
  'Barbosa' => 'Barbosa',
  'Barichara' => 'Barichara',
  'Barranca de Upia' => 'Barranca de Upía',
  'Barrancabermeja' => 'Barrancabermeja',
  'Barrancas' => 'Barrancas',
  'Barranco de Loba' => 'Barranco de Loba',
  'Barranco Minas' => 'Barranco Minas',
  'Barranquilla' => 'Barranquilla',
  'Becerril' => 'Becerril',
  'Belalcazar' => 'Belalcázar',
  'Belen' => 'Belén',
  'Belen de Bajira' => 'Belén de Bajira',
  'Belen de Los Andaquies' => 'Belén de Los Andaquies',
  'Belen de Umbria' => 'Belén de Umbría',
  'Bello' => 'Bello',
  'Belmira' => 'Belmira',
  'Beltran' => 'Beltrán',
  'Berbeo' => 'Berbeo',
  'Betania' => 'Betania',
  'Beteitiva' => 'Betéitiva',
  'Betulia' => 'Betulia',
  'Bituima' => 'Bituima',
  'Boavita' => 'Boavita',
  'Bochalema' => 'Bochalema',
  'Bogota D.C.' => 'Bogotá D.C.',
  'Bojaca' => 'Bojacá',
  'Bojaya' => 'Bojaya',
  'Bolivar' => 'Bolívar',
  'Bosconia' => 'Bosconia',
  'Boyaca' => 'Boyacá',
  'Briceno' => 'Briceño',
  'Bucaramanga' => 'Bucaramanga',
  'Bucarasica' => 'Bucarasica',
  'Buena Vista' => 'Buena Vista',
  'Buenaventura' => 'Buenaventura',
  'Buenavista' => 'Buenavista',
  'Buenos Aires' => 'Buenos Aires',
  'Buesaco' => 'Buesaco',
  'Bugalagrande' => 'Bugalagrande',
  'Buritica' => 'Buriticá',
  'Busbanza' => 'Busbanzá',
  'Cabrera' => 'Cabrera',
  'Cabuyaro' => 'Cabuyaro',
  'Cacahual' => 'Cacahual',
  'Caceres' => 'Cáceres',
  'Cachipay' => 'Cachipay',
  'Cachira' => 'Cachirá',
  'Cacota' => 'Cácota',
  'Caicedo' => 'Caicedo',
  'Caicedonia' => 'Caicedonia',
  'Caimito' => 'Caimito',
  'Cajamarca' => 'Cajamarca',
  'Cajibio' => 'Cajibío',
  'Cajica' => 'Cajicá',
  'Calamar' => 'Calamar',
  'Calarca' => 'Calarcá',
  'Caldas' => 'Caldas',
  'Caldono' => 'Caldono',
  'Cali' => 'Cali',
  'California' => 'California',
  'Calima' => 'Calima',
  'Caloto' => 'Caloto',
  'Campamento' => 'Campamento',
  'Campo de La Cruz' => 'Campo de La Cruz',
  'Campoalegre' => 'Campoalegre',
  'Campohermoso' => 'Campohermoso',
  'Canalete' => 'Canalete',
  'Candelaria' => 'Candelaria',
  'Cantagallo' => 'Cantagallo',
  'Canasgordas' => 'Cañasgordas',
  'Caparrapi' => 'Caparrapí',
  'Capitanejo' => 'Capitanejo',
  'Caqueza' => 'Caqueza',
  'Caracoli' => 'Caracolí',
  'Caramanta' => 'Caramanta',
  'Carcasi' => 'Carcasí',
  'Carepa' => 'Carepa',
  'Carmen de Apicala' => 'Carmen de Apicala',
  'Carmen de Carupa' => 'Carmen de Carupa',
  'Carmen del Darien' => 'Carmen del Darien',
  'Carolina' => 'Carolina',
  'Cartagena' => 'Cartagena',
  'Cartagena del Chaira' => 'Cartagena del Chairá',
  'Cartago' => 'Cartago',
  'Caruru' => 'Caruru',
  'Casabianca' => 'Casabianca',
  'Castilla la Nueva' => 'Castilla la Nueva',
  'Caucasia' => 'Caucasia',
  'Cepita' => 'Cepitá',
  'Cerete' => 'Cereté',
  'Cerinza' => 'Cerinza',
  'Cerrito' => 'Cerrito',
  'Cerro San Antonio' => 'Cerro San Antonio',
  'Certegui' => 'Cértegui',
  'Chachagüi' => 'Chachagüí',
  'Chaguani' => 'Chaguaní',
  'Chalan' => 'Chalán',
  'Chameza' => 'Chámeza',
  'Chaparral' => 'Chaparral',
  'Charala' => 'Charalá',
  'Charta' => 'Charta',
  'Chia' => 'Chía',
  'Chigorodo' => 'Chigorodó',
  'Chima' => 'Chimá',
  'Chimichagua' => 'Chimichagua',
  'Chinacota' => 'Chinácota',
  'Chinavita' => 'Chinavita',
  'Chinchina' => 'Chinchiná',
  'Chinu' => 'Chinú',
  'Chipaque' => 'Chipaque',
  'Chipata' => 'Chipatá',
  'Chiquinquira' => 'Chiquinquirá',
  'Chiquiza' => 'Chíquiza',
  'Chiriguana' => 'Chiriguaná',
  'Chiscas' => 'Chiscas',
  'Chita' => 'Chita',
  'Chitaga' => 'Chitagá',
  'Chitaraque' => 'Chitaraque',
  'Chivata' => 'Chivatá',
  'Chivolo' => 'Chivolo',
  'Chivor' => 'Chivor',
  'Choachi' => 'Choachí',
  'Choconta' => 'Chocontá',
  'Cicuco' => 'Cicuco',
  'Cienaga' => 'Ciénaga',
  'Cienaga de Oro' => 'Ciénaga de Oro',
  'Cienega' => 'Ciénega',
  'Cimitarra' => 'Cimitarra',
  'Circasia' => 'Circasia',
  'Cisneros' => 'Cisneros',
  'Ciudad Bolivar' => 'Ciudad Bolívar',
  'Clemencia' => 'Clemencia',
  'Cocorna' => 'Cocorná',
  'Coello' => 'Coello',
  'Cogua' => 'Cogua',
  'Colombia' => 'Colombia',
  'Colon' => 'Colón',
  'Coloso' => 'Coloso',
  'Combita' => 'Cómbita',
  'Concepcion' => 'Concepción',
  'Concordia' => 'Concordia',
  'Condoto' => 'Condoto',
  'Confines' => 'Confines',
  'Consaca' => 'Consaca',
  'Contadero' => 'Contadero',
  'Contratacion' => 'Contratación',
  'Convencion' => 'Convención',
  'Copacabana' => 'Copacabana',
  'Coper' => 'Coper',
  'Cordoba' => 'Córdoba',
  'Corinto' => 'Corinto',
  'Coromoro' => 'Coromoro',
  'Corozal' => 'Corozal',
  'Corrales' => 'Corrales',
  'Cota' => 'Cota',
  'Cotorra' => 'Cotorra',
  'Covarachia' => 'Covarachía',
  'Covenas' => 'Coveñas',
  'Coyaima' => 'Coyaima',
  'Cravo Norte' => 'Cravo Norte',
  'Cuaspud' => 'Cuaspud',
  'Cubara' => 'Cubará',
  'Cubarral' => 'Cubarral',
  'Cucaita' => 'Cucaita',
  'Cucunuba' => 'Cucunubá',
  'Cucuta' => 'Cúcuta',
  'Cucutilla' => 'Cucutilla',
  'Cuitiva' => 'Cuítiva',
  'Cumaral' => 'Cumaral',
  'Cumaribo' => 'Cumaribo',
  'Cumbal' => 'Cumbal',
  'Cumbitara' => 'Cumbitara',
  'Cunday' => 'Cunday',
  'Curillo' => 'Curillo',
  'Curiti' => 'Curití',
  'Curumani' => 'Curumaní',
  'Dabeiba' => 'Dabeiba',
  'Dagua' => 'Dagua',
  'Dibula' => 'Dibula',
  'Distraccion' => 'Distracción',
  'Dolores' => 'Dolores',
  'Don Matias' => 'Don Matías',
  'Dosquebradas' => 'Dosquebradas',
  'Duitama' => 'Duitama',
  'Durania' => 'Durania',
  'Ebejico' => 'Ebéjico',
  'El aguila' => 'El Águila',
  'El Bagre' => 'El Bagre',
  'El Banco' => 'El Banco',
  'El Cairo' => 'El Cairo',
  'El Calvario' => 'El Calvario',
  'El Canton del San Pablo' => 'El Cantón del San Pablo',
  'El Carmen' => 'El Carmen',
  'El Carmen de Atrato' => 'El Carmen de Atrato',
  'El Carmen de Bolivar' => 'El Carmen de Bolívar',
  'El Carmen de Chucuri' => 'El Carmen de Chucurí',
  'El Carmen de Viboral' => 'El Carmen de Viboral',
  'El Castillo' => 'El Castillo',
  'El Cerrito' => 'El Cerrito',
  'El Charco' => 'El Charco',
  'El Cocuy' => 'El Cocuy',
  'El Colegio' => 'El Colegio',
  'El Copey' => 'El Copey',
  'El Doncello' => 'El Doncello',
  'El Dorado' => 'El Dorado',
  'El Dovio' => 'El Dovio',
  'El Encanto' => 'El Encanto',
  'El Espino' => 'El Espino',
  'El Guacamayo' => 'El Guacamayo',
  'El Guamo' => 'El Guamo',
  'El Litoral del San Juan' => 'El Litoral del San Juan',
  'El Molino' => 'El Molino',
  'El Paso' => 'El Paso',
  'El Paujil' => 'El Paujil',
  'El Penol' => 'El Peñol',
  'El Penon' => 'El Peñón',
  'El Pinon' => 'El Piñon',
  'El Playon' => 'El Playón',
  'El Reten' => 'El Retén',
  'El Retorno' => 'El Retorno',
  'El Roble' => 'El Roble',
  'El Rosal' => 'El Rosal',
  'El Rosario' => 'El Rosario',
  'El Santuario' => 'El Santuario',
  'El Tablon de Gomez' => 'El Tablón de Gómez',
  'El Tambo' => 'El Tambo',
  'El Tarra' => 'El Tarra',
  'El Zulia' => 'El Zulia',
  'Elias' => 'Elías',
  'Encino' => 'Encino',
  'Enciso' => 'Enciso',
  'Entrerrios' => 'Entrerrios',
  'Envigado' => 'Envigado',
  'Espinal' => 'Espinal',
  'Facatativa' => 'Facatativá',
  'Falan' => 'Falan',
  'Filadelfia' => 'Filadelfia',
  'Filandia' => 'Filandia',
  'Firavitoba' => 'Firavitoba',
  'Flandes' => 'Flandes',
  'Florencia' => 'Florencia',
  'Floresta' => 'Floresta',
  'Florian' => 'Florián',
  'Florida' => 'Florida',
  'Floridablanca' => 'Floridablanca',
  'Fomeque' => 'Fomeque',
  'Fonseca' => 'Fonseca',
  'Fortul' => 'Fortul',
  'Fosca' => 'Fosca',
  'Francisco Pizarro' => 'Francisco Pizarro',
  'Fredonia' => 'Fredonia',
  'Fresno' => 'Fresno',
  'Frontino' => 'Frontino',
  'Fuente de Oro' => 'Fuente de Oro',
  'Fundacion' => 'Fundación',
  'Funes' => 'Funes',
  'Funza' => 'Funza',
  'Fuquene' => 'Fúquene',
  'Fusagasuga' => 'Fusagasugá',
  'Gachala' => 'Gachala',
  'Gachancipa' => 'Gachancipá',
  'Gachantiva' => 'Gachantivá',
  'Gacheta' => 'Gachetá',
  'Galan' => 'Galán',
  'Galapa' => 'Galapa',
  'Galeras' => 'Galeras',
  'Gama' => 'Gama',
  'Gamarra' => 'Gamarra',
  'Gambita' => 'Gambita',
  'Gameza' => 'Gameza',
  'Garagoa' => 'Garagoa',
  'Garzon' => 'Garzón',
  'Genova' => 'Génova',
  'Gigante' => 'Gigante',
  'Ginebra' => 'Ginebra',
  'Giraldo' => 'Giraldo',
  'Girardot' => 'Girardot',
  'Girardota' => 'Girardota',
  'Giron' => 'Girón',
  'Gomez Plata' => 'Gómez Plata',
  'Gonzalez' => 'González',
  'Gramalote' => 'Gramalote',
  'Granada' => 'Granada',
  'Guaca' => 'Guaca',
  'Guacamayas' => 'Guacamayas',
  'Guacari' => 'Guacarí',
  'Guachene' => 'Guachené',
  'Guacheta' => 'Guachetá',
  'Guachucal' => 'Guachucal',
  'Guadalajara de Buga' => 'Guadalajara de Buga',
  'Guadalupe' => 'Guadalupe',
  'Guaduas' => 'Guaduas',
  'Guaitarilla' => 'Guaitarilla',
  'Gualmatan' => 'Gualmatán',
  'Guamal' => 'Guamal',
  'Guamo' => 'Guamo',
  'Guapi' => 'Guapi',
  'Guapota' => 'Guapotá',
  'Guaranda' => 'Guaranda',
  'Guarne' => 'Guarne',
  'Guasca' => 'Guasca',
  'Guatape' => 'Guatapé',
  'Guataqui' => 'Guataquí',
  'Guatavita' => 'Guatavita',
  'Guateque' => 'Guateque',
  'Guatica' => 'Guática',
  'Guavata' => 'Guavatá',
  'Guayabal de Siquima' => 'Guayabal de Siquima',
  'Guayabetal' => 'Guayabetal',
  'Guayata' => 'Guayatá',
  'Güepsa' => 'Güepsa',
  'Güican' => 'Güicán',
  'Gutierrez' => 'Gutiérrez',
  'Hacari' => 'Hacarí',
  'Hatillo de Loba' => 'Hatillo de Loba',
  'Hato' => 'Hato',
  'Hato Corozal' => 'Hato Corozal',
  'Hatonuevo' => 'Hatonuevo',
  'Heliconia' => 'Heliconia',
  'Herran' => 'Herrán',
  'Herveo' => 'Herveo',
  'Hispania' => 'Hispania',
  'Hobo' => 'Hobo',
  'Honda' => 'Honda',
  'Ibague' => 'Ibagué',
  'Icononzo' => 'Icononzo',
  'Iles' => 'Iles',
  'Imues' => 'Imués',
  'Inirida' => 'Inírida',
  'Inza' => 'Inzá',
  'Ipiales' => 'Ipiales',
  'Iquira' => 'Iquira',
  'Isnos' => 'Isnos',
  'Istmina' => 'Istmina',
  'Itagui' => 'Itagui',
  'Ituango' => 'Ituango',
  'Iza' => 'Iza',
  'Jambalo' => 'Jambaló',
  'Jamundi' => 'Jamundí',
  'Jardin' => 'Jardín',
  'Jenesano' => 'Jenesano',
  'Jerico' => 'Jericó',
  'Jerusalen' => 'Jerusalén',
  'Jesus Maria' => 'Jesús María',
  'Jordan' => 'Jordán',
  'Juan de Acosta' => 'Juan de Acosta',
  'Junin' => 'Junín',
  'Jurado' => 'Juradó',
  'La Apartada' => 'La Apartada',
  'La Argentina' => 'La Argentina',
  'La Belleza' => 'La Belleza',
  'La Calera' => 'La Calera',
  'La Capilla' => 'La Capilla',
  'La Ceja' => 'La Ceja',
  'La Celia' => 'La Celia',
  'La Chorrera' => 'La Chorrera',
  'La Cruz' => 'La Cruz',
  'La Cumbre' => 'La Cumbre',
  'La Dorada' => 'La Dorada',
  'La Esperanza' => 'La Esperanza',
  'La Estrella' => 'La Estrella',
  'La Florida' => 'La Florida',
  'La Gloria' => 'La Gloria',
  'La Guadalupe' => 'La Guadalupe',
  'La Jagua de Ibirico' => 'La Jagua de Ibirico',
  'La Jagua del Pilar' => 'La Jagua del Pilar',
  'La Llanada' => 'La Llanada',
  'La Macarena' => 'La Macarena',
  'La Merced' => 'La Merced',
  'La Mesa' => 'La Mesa',
  'La Montanita' => 'La Montañita',
  'La Palma' => 'La Palma',
  'La Paz' => 'La Paz',
  'La Pedrera' => 'La Pedrera',
  'La Pena' => 'La Peña',
  'La Pintada' => 'La Pintada',
  'La Plata' => 'La Plata',
  'La Playa' => 'La Playa',
  'La Primavera' => 'La Primavera',
  'La Salina' => 'La Salina',
  'La Sierra' => 'La Sierra',
  'La Tebaida' => 'La Tebaida',
  'La Tola' => 'La Tola',
  'La Union' => 'La Unión',
  'La Uvita' => 'La Uvita',
  'La Vega' => 'La Vega',
  'La Victoria' => 'La Victoria',
  'La Virginia' => 'La Virginia',
  'Labateca' => 'Labateca',
  'Labranzagrande' => 'Labranzagrande',
  'Landazuri' => 'Landázuri',
  'Lebrija' => 'Lebríja',
  'Leguizamo' => 'Leguízamo',
  'Leiva' => 'Leiva',
  'Lejanias' => 'Lejanías',
  'Lenguazaque' => 'Lenguazaque',
  'Lerida' => 'Lérida',
  'Leticia' => 'Leticia',
  'Libano' => 'Líbano',
  'Liborina' => 'Liborina',
  'Linares' => 'Linares',
  'Lloro' => 'Lloró',
  'Lopez' => 'López',
  'Lorica' => 'Lorica',
  'Los Andes' => 'Los Andes',
  'Los Cordobas' => 'Los Córdobas',
  'Los Palmitos' => 'Los Palmitos',
  'Los Patios' => 'Los Patios',
  'Los Santos' => 'Los Santos',
  'Lourdes' => 'Lourdes',
  'Luruaco' => 'Luruaco',
  'Macanal' => 'Macanal',
  'Macaravita' => 'Macaravita',
  'Maceo' => 'Maceo',
  'Macheta' => 'Macheta',
  'Madrid' => 'Madrid',
  'Magangue' => 'Magangué',
  'Magüi' => 'Magüí',
  'Mahates' => 'Mahates',
  'Maicao' => 'Maicao',
  'Majagual' => 'Majagual',
  'Malaga' => 'Málaga',
  'Malambo' => 'Malambo',
  'Mallama' => 'Mallama',
  'Manati' => 'Manatí',
  'Manaure' => 'Manaure',
  'Mani' => 'Maní',
  'Manizales' => 'Manizales',
  'Manta' => 'Manta',
  'Manzanares' => 'Manzanares',
  'Mapiripan' => 'Mapiripán',
  'Mapiripana' => 'Mapiripana',
  'Margarita' => 'Margarita',
  'Maria la Baja' => 'María la Baja',
  'Marinilla' => 'Marinilla',
  'Maripi' => 'Maripí',
  'Mariquita' => 'Mariquita',
  'Marmato' => 'Marmato',
  'Marquetalia' => 'Marquetalia',
  'Marsella' => 'Marsella',
  'Marulanda' => 'Marulanda',
  'Matanza' => 'Matanza',
  'Medellin' => 'Medellín',
  'Medina' => 'Medina',
  'Medio Atrato' => 'Medio Atrato',
  'Medio Baudo' => 'Medio Baudó',
  'Medio San Juan' => 'Medio San Juan',
  'Melgar' => 'Melgar',
  'Mercaderes' => 'Mercaderes',
  'Mesetas' => 'Mesetas',
  'Milan' => 'Milán',
  'Miraflores' => 'Miraflores',
  'Miranda' => 'Miranda',
  'Miriti Parana' => 'Miriti Paraná',
  'Mistrato' => 'Mistrató',
  'Mitu' => 'Mitú',
  'Mocoa' => 'Mocoa',
  'Mogotes' => 'Mogotes',
  'Molagavita' => 'Molagavita',
  'Momil' => 'Momil',
  'Mompos' => 'Mompós',
  'Mongua' => 'Mongua',
  'Mongui' => 'Monguí',
  'Moniquira' => 'Moniquirá',
  'Montebello' => 'Montebello',
  'Montecristo' => 'Montecristo',
  'Montelibano' => 'Montelíbano',
  'Montenegro' => 'Montenegro',
  'Monteria' => 'Montería',
  'Monterrey' => 'Monterrey',
  'Monitos' => 'Moñitos',
  'Morales' => 'Morales',
  'Morelia' => 'Morelia',
  'Morichal' => 'Morichal',
  'Morroa' => 'Morroa',
  'Mosquera' => 'Mosquera',
  'Motavita' => 'Motavita',
  'Murillo' => 'Murillo',
  'Murindo' => 'Murindó',
  'Mutata' => 'Mutatá',
  'Mutiscua' => 'Mutiscua',
  'Muzo' => 'Muzo',
  'Narino' => 'Nariño',
  'Nataga' => 'Nátaga',
  'Natagaima' => 'Natagaima',
  'Nechi' => 'Nechí',
  'Necocli' => 'Necoclí',
  'Neira' => 'Neira',
  'Neiva' => 'Neiva',
  'Nemocon' => 'Nemocón',
  'Nilo' => 'Nilo',
  'Nimaima' => 'Nimaima',
  'Nobsa' => 'Nobsa',
  'Nocaima' => 'Nocaima',
  'Norcasia' => 'Norcasia',
  'Norosi' => 'Norosí',
  'Novita' => 'Nóvita',
  'Nueva Granada' => 'Nueva Granada',
  'Nuevo Colon' => 'Nuevo Colón',
  'Nunchia' => 'Nunchía',
  'Nuqui' => 'Nuquí',
  'Obando' => 'Obando',
  'Ocamonte' => 'Ocamonte',
  'Ocana' => 'Ocaña',
  'Oiba' => 'Oiba',
  'Oicata' => 'Oicatá',
  'Olaya' => 'Olaya',
  'Olaya Herrera' => 'Olaya Herrera',
  'Onzaga' => 'Onzaga',
  'Oporapa' => 'Oporapa',
  'Orito' => 'Orito',
  'Orocue' => 'Orocué',
  'Ortega' => 'Ortega',
  'Ospina' => 'Ospina',
  'Otanche' => 'Otanche',
  'Ovejas' => 'Ovejas',
  'Pachavita' => 'Pachavita',
  'Pacho' => 'Pacho',
  'Pacoa' => 'Pacoa',
  'Pacora' => 'Pácora',
  'Padilla' => 'Padilla',
  'Paez' => 'Páez',
  'Paicol' => 'Paicol',
  'Pailitas' => 'Pailitas',
  'Paime' => 'Paime',
  'Paipa' => 'Paipa',
  'Pajarito' => 'Pajarito',
  'Palermo' => 'Palermo',
  'Palestina' => 'Palestina',
  'Palmar' => 'Palmar',
  'Palmar de Varela' => 'Palmar de Varela',
  'Palmas del Socorro' => 'Palmas del Socorro',
  'Palmira' => 'Palmira',
  'Palmito' => 'Palmito',
  'Palocabildo' => 'Palocabildo',
  'Pamplona' => 'Pamplona',
  'Pamplonita' => 'Pamplonita',
  'Pana Pana' => 'Pana Pana',
  'Pandi' => 'Pandi',
  'Panqueba' => 'Panqueba',
  'Papunaua' => 'Papunaua',
  'Paramo' => 'Páramo',
  'Paratebueno' => 'Paratebueno',
  'Pasca' => 'Pasca',
  'Pasto' => 'Pasto',
  'Patia' => 'Patía',
  'Pauna' => 'Pauna',
  'Paya' => 'Paya',
  'Paz de Ariporo' => 'Paz de Ariporo',
  'Paz de Rio' => 'Paz de Río',
  'Pedraza' => 'Pedraza',
  'Pelaya' => 'Pelaya',
  'Pensilvania' => 'Pensilvania',
  'Penol' => 'Peñol',
  'Peque' => 'Peque',
  'Pereira' => 'Pereira',
  'Pesca' => 'Pesca',
  'Piamonte' => 'Piamonte',
  'Piedecuesta' => 'Piedecuesta',
  'Piedras' => 'Piedras',
  'Piendamo' => 'Piendamó',
  'Pijao' => 'Pijao',
  'Pijino del Carmen' => 'Pijiño del Carmen',
  'Pinchote' => 'Pinchote',
  'Pinillos' => 'Pinillos',
  'Piojo' => 'Piojó',
  'Pisba' => 'Pisba',
  'Pital' => 'Pital',
  'Pitalito' => 'Pitalito',
  'Pivijay' => 'Pivijay',
  'Planadas' => 'Planadas',
  'Planeta Rica' => 'Planeta Rica',
  'Plato' => 'Plato',
  'Policarpa' => 'Policarpa',
  'Polonuevo' => 'Polonuevo',
  'Ponedera' => 'Ponedera',
  'Popayan' => 'Popayán',
  'Pore' => 'Pore',
  'Potosi' => 'Potosí',
  'Pradera' => 'Pradera',
  'Prado' => 'Prado',
  'Providencia' => 'Providencia',
  'Pueblo Bello' => 'Pueblo Bello',
  'Pueblo Nuevo' => 'Pueblo Nuevo',
  'Pueblo Rico' => 'Pueblo Rico',
  'Pueblo Viejo' => 'Pueblo Viejo',
  'Pueblorrico' => 'Pueblorrico',
  'Puente Nacional' => 'Puente Nacional',
  'Puerres' => 'Puerres',
  'Puerto Alegria' => 'Puerto Alegría',
  'Puerto Arica' => 'Puerto Arica',
  'Puerto Asis' => 'Puerto Asís',
  'Puerto Berrio' => 'Puerto Berrío',
  'Puerto Boyaca' => 'Puerto Boyacá',
  'Puerto Caicedo' => 'Puerto Caicedo',
  'Puerto Carreno' => 'Puerto Carreño',
  'Puerto Colombia' => 'Puerto Colombia',
  'Puerto Concordia' => 'Puerto Concordia',
  'Puerto Escondido' => 'Puerto Escondido',
  'Puerto Gaitan' => 'Puerto Gaitán',
  'Puerto Guzman' => 'Puerto Guzmán',
  'Puerto Libertador' => 'Puerto Libertador',
  'Puerto Lleras' => 'Puerto Lleras',
  'Puerto Lopez' => 'Puerto López',
  'Puerto Nare' => 'Puerto Nare',
  'Puerto Narino' => 'Puerto Nariño',
  'Puerto Parra' => 'Puerto Parra',
  'Puerto Rico' => 'Puerto Rico',
  'Puerto Rondon' => 'Puerto Rondón',
  'Puerto Salgar' => 'Puerto Salgar',
  'Puerto Santander' => 'Puerto Santander',
  'Puerto Tejada' => 'Puerto Tejada',
  'Puerto Triunfo' => 'Puerto Triunfo',
  'Puerto Wilches' => 'Puerto Wilches',
  'Puli' => 'Pulí',
  'Pupiales' => 'Pupiales',
  'Purace' => 'Puracé',
  'Purificacion' => 'Purificación',
  'Purisima' => 'Purísima',
  'Quebradanegra' => 'Quebradanegra',
  'Quetame' => 'Quetame',
  'Quibdo' => 'Quibdó',
  'Quimbaya' => 'Quimbaya',
  'Quinchia' => 'Quinchía',
  'Quipama' => 'Quípama',
  'Quipile' => 'Quipile',
  'Ragonvalia' => 'Ragonvalia',
  'Ramiriqui' => 'Ramiriquí',
  'Raquira' => 'Ráquira',
  'Recetor' => 'Recetor',
  'Regidor' => 'Regidor',
  'Remedios' => 'Remedios',
  'Remolino' => 'Remolino',
  'Repelon' => 'Repelón',
  'Restrepo' => 'Restrepo',
  'Retiro' => 'Retiro',
  'Ricaurte' => 'Ricaurte',
  'Rio Blanco' => 'Rio Blanco',
  'Rio de Oro' => 'Río de Oro',
  'Rio Iro' => 'Río Iro',
  'Rio Quito' => 'Río Quito',
  'Rio Viejo' => 'Río Viejo',
  'Riofrio' => 'Riofrío',
  'Riohacha' => 'Riohacha',
  'Rionegro' => 'Rionegro',
  'Riosucio' => 'Riosucio',
  'Risaralda' => 'Risaralda',
  'Rivera' => 'Rivera',
  'Roberto Payan' => 'Roberto Payán',
  'Roldanillo' => 'Roldanillo',
  'Roncesvalles' => 'Roncesvalles',
  'Rondon' => 'Rondón',
  'Rosas' => 'Rosas',
  'Rovira' => 'Rovira',
  'Sabana de Torres' => 'Sabana de Torres',
  'Sabanagrande' => 'Sabanagrande',
  'Sabanalarga' => 'Sabanalarga',
  'Sabanas de San Angel' => 'Sabanas de San Angel',
  'Sabaneta' => 'Sabaneta',
  'Saboya' => 'Saboyá',
  'Sacama' => 'Sácama',
  'Sachica' => 'Sáchica',
  'Sahagun' => 'Sahagún',
  'Saladoblanco' => 'Saladoblanco',
  'Salamina' => 'Salamina',
  'Salazar' => 'Salazar',
  'Saldana' => 'Saldaña',
  'Salento' => 'Salento',
  'Salgar' => 'Salgar',
  'Samaca' => 'Samacá',
  'Samana' => 'Samaná',
  'Samaniego' => 'Samaniego',
  'Sampues' => 'Sampués',
  'San Agustin' => 'San Agustín',
  'San Alberto' => 'San Alberto',
  'San Andres' => 'San Andrés',
  'San Andres de Cuerquia' => 'San Andrés de Cuerquía',
  'San Andres de Tumaco' => 'San Andrés de Tumaco',
  'San Andres Sotavento' => 'San Andrés Sotavento',
  'San Antero' => 'San Antero',
  'San Antonio' => 'San Antonio',
  'San Antonio del Tequendama' => 'San Antonio del Tequendama',
  'San Benito' => 'San Benito',
  'San Benito Abad' => 'San Benito Abad',
  'San Bernardo' => 'San Bernardo',
  'San Bernardo del Viento' => 'San Bernardo del Viento',
  'San Calixto' => 'San Calixto',
  'San Carlos' => 'San Carlos',
  'San Carlos de Guaroa' => 'San Carlos de Guaroa',
  'San Cayetano' => 'San Cayetano',
  'San Cristobal' => 'San Cristóbal',
  'San Diego' => 'San Diego',
  'San Eduardo' => 'San Eduardo',
  'San Estanislao' => 'San Estanislao',
  'San Felipe' => 'San Felipe',
  'San Fernando' => 'San Fernando',
  'San Francisco' => 'San Francisco',
  'San Gil' => 'San Gil',
  'San Jacinto' => 'San Jacinto',
  'San Jacinto del Cauca' => 'San Jacinto del Cauca',
  'San Jeronimo' => 'San Jerónimo',
  'San Joaquin' => 'San Joaquín',
  'San Jose' => 'San José',
  'San Jose de La Montana' => 'San José de La Montaña',
  'San Jose de Miranda' => 'San José de Miranda',
  'San Jose de Pare' => 'San José de Pare',
  'San Jose de Ure' => 'San José de Uré',
  'San Jose del Fragua' => 'San José del Fragua',
  'San Jose del Guaviare' => 'San José del Guaviare',
  'San Jose del Palmar' => 'San José del Palmar',
  'San Juan de Arama' => 'San Juan de Arama',
  'San Juan de Betulia' => 'San Juan de Betulia',
  'San Juan de Rio Seco' => 'San Juan de Río Seco',
  'San Juan de Uraba' => 'San Juan de Urabá',
  'San Juan del Cesar' => 'San Juan del Cesar',
  'San Juan Nepomuceno' => 'San Juan Nepomuceno',
  'San Juanito' => 'San Juanito',
  'San Lorenzo' => 'San Lorenzo',
  'San Luis' => 'San Luis',
  'San Luis de Gaceno' => 'San Luis de Gaceno',
  'San Luis de Since' => 'San Luis de Sincé',
  'San Marcos' => 'San Marcos',
  'San Martin' => 'San Martín',
  'San Martin de Loba' => 'San Martín de Loba',
  'San Mateo' => 'San Mateo',
  'San Miguel' => 'San Miguel',
  'San Miguel de Sema' => 'San Miguel de Sema',
  'San Onofre' => 'San Onofre',
  'San Pablo' => 'San Pablo',
  'San Pablo de Borbur' => 'San Pablo de Borbur',
  'San Pedro' => 'San Pedro',
  'San Pedro de Cartago' => 'San Pedro de Cartago',
  'San Pedro de Uraba' => 'San Pedro de Uraba',
  'San Pelayo' => 'San Pelayo',
  'San Rafael' => 'San Rafael',
  'San Roque' => 'San Roque',
  'San Sebastian' => 'San Sebastián',
  'San Sebastian de Buenavista' => 'San Sebastián de Buenavista',
  'San Vicente' => 'San Vicente',
  'San Vicente de Chucuri' => 'San Vicente de Chucurí',
  'San Vicente del Caguan' => 'San Vicente del Caguán',
  'San Zenon' => 'San Zenón',
  'Sandona' => 'Sandoná',
  'Santa Ana' => 'Santa Ana',
  'Santa Barbara' => 'Santa Bárbara',
  'Santa Barbara de Pinto' => 'Santa Bárbara de Pinto',
  'Santa Catalina' => 'Santa Catalina',
  'Socha' => 'Socha',
  'Santa Helena del Opon' => 'Santa Helena del Opón',
  'Santa Isabel' => 'Santa Isabel',
  'Santa Lucia' => 'Santa Lucía',
  'Santa Maria' => 'Santa María',
  'Santa Marta' => 'Santa Marta',
  'Santa Rosa' => 'Santa Rosa',
  'Santa Rosa de Cabal' => 'Santa Rosa de Cabal',
  'Santa Rosa de Osos' => 'Santa Rosa de Osos',
  'Santa Rosa de Viterbo' => 'Santa Rosa de Viterbo',
  'Santa Rosa del Sur' => 'Santa Rosa del Sur',
  'Santa Rosalia' => 'Santa Rosalía',
  'Santa Sofia' => 'Santa Sofía',
  'Santacruz' => 'Santacruz',
  'Santafe de Antioquia' => 'Santafé de Antioquia',
  'Santana' => 'Santana',
  'Santander de Quilichao' => 'Santander de Quilichao',
  'Santiago' => 'Santiago',
  'Santiago de Tolu' => 'Santiago de Tolú',
  'Santo Domingo' => 'Santo Domingo',
  'Santo Tomas' => 'Santo Tomás',
  'Santuario' => 'Santuario',
  'Sapuyes' => 'Sapuyes',
  'Saravena' => 'Saravena',
  'Sardinata' => 'Sardinata',
  'Sasaima' => 'Sasaima',
  'Sativanorte' => 'Sativanorte',
  'Sativasur' => 'Sativasur',
  'Segovia' => 'Segovia',
  'Sesquile' => 'Sesquilé',
  'Sevilla' => 'Sevilla',
  'Siachoque' => 'Siachoque',
  'Sibate' => 'Sibaté',
  'Sibundoy' => 'Sibundoy',
  'Silos' => 'Silos',
  'Silvania' => 'Silvania',
  'Silvia' => 'Silvia',
  'Simacota' => 'Simacota',
  'Simijaca' => 'Simijaca',
  'Simiti' => 'Simití',
  'Sincelejo' => 'Sincelejo',
  'Sipi' => 'Sipí',
  'Sitionuevo' => 'Sitionuevo',
  'Soacha' => 'Soacha',
  'Soata' => 'Soatá',
  'Socorro' => 'Socorro',
  'Socota' => 'Socotá',
  'Sogamoso' => 'Sogamoso',
  'Solano' => 'Solano',
  'Soledad' => 'Soledad',
  'Solita' => 'Solita',
  'Somondoco' => 'Somondoco',
  'Sonson' => 'Sonsón',
  'Sopetran' => 'Sopetrán',
  'Soplaviento' => 'Soplaviento',
  'Sopo' => 'Sopó',
  'Sora' => 'Sora',
  'Soraca' => 'Soracá',
  'Sotaquira' => 'Sotaquirá',
  'Sotara' => 'Sotara',
  'Suaita' => 'Suaita',
  'Suan' => 'Suan',
  'Suarez' => 'Suárez',
  'Suaza' => 'Suaza',
  'Subachoque' => 'Subachoque',
  'Sucre' => 'Sucre',
  'Suesca' => 'Suesca',
  'Supata' => 'Supatá',
  'Supia' => 'Supía',
  'Surata' => 'Suratá',
  'Susa' => 'Susa',
  'Susacon' => 'Susacón',
  'Sutamarchan' => 'Sutamarchán',
  'Sutatausa' => 'Sutatausa',
  'Sutatenza' => 'Sutatenza',
  'Tabio' => 'Tabio',
  'Tado' => 'Tadó',
  'Talaigua Nuevo' => 'Talaigua Nuevo',
  'Tamalameque' => 'Tamalameque',
  'Tamara' => 'Támara',
  'Tame' => 'Tame',
  'Tamesis' => 'Támesis',
  'Taminango' => 'Taminango',
  'Tangua' => 'Tangua',
  'Taraira' => 'Taraira',
  'Tarapaca' => 'Tarapacá',
  'Taraza' => 'Tarazá',
  'Tarqui' => 'Tarqui',
  'Tarso' => 'Tarso',
  'Tasco' => 'Tasco',
  'Tauramena' => 'Tauramena',
  'Tausa' => 'Tausa',
  'Tello' => 'Tello',
  'Tena' => 'Tena',
  'Tenerife' => 'Tenerife',
  'Tenjo' => 'Tenjo',
  'Tenza' => 'Tenza',
  'Teorama' => 'Teorama',
  'Teruel' => 'Teruel',
  'Tesalia' => 'Tesalia',
  'Tibacuy' => 'Tibacuy',
  'Tibana' => 'Tibaná',
  'Tibasosa' => 'Tibasosa',
  'Tibirita' => 'Tibirita',
  'Tibu' => 'Tibú',
  'Tierralta' => 'Tierralta',
  'Timana' => 'Timaná',
  'Timbio' => 'Timbío',
  'Timbiqui' => 'Timbiquí',
  'Tinjaca' => 'Tinjacá',
  'Tipacoque' => 'Tipacoque',
  'Tiquisio' => 'Tiquisio',
  'Titiribi' => 'Titiribí',
  'Toca' => 'Toca',
  'Tocaima' => 'Tocaima',
  'Tocancipa' => 'Tocancipá',
  'Togüi' => 'Togüí',
  'Toledo' => 'Toledo',
  'Tolu Viejo' => 'Tolú Viejo',
  'Tona' => 'Tona',
  'Topaga' => 'Tópaga',
  'Topaipi' => 'Topaipí',
  'Toribio' => 'Toribio',
  'Toro' => 'Toro',
  'Tota' => 'Tota',
  'Totoro' => 'Totoró',
  'Trinidad' => 'Trinidad',
  'Trujillo' => 'Trujillo',
  'Tubara' => 'Tubará',
  'Tuchin' => 'Tuchín',
  'Tulua' => 'Tuluá',
  'Tunja' => 'Tunja',
  'Tunungua' => 'Tununguá',
  'Tuquerres' => 'Túquerres',
  'Turbaco' => 'Turbaco',
  'Turbana' => 'Turbaná',
  'Turbo' => 'Turbo',
  'Turmeque' => 'Turmequé',
  'Tuta' => 'Tuta',
  'Tutaza' => 'Tutazá',
  'Ubala' => 'Ubalá',
  'Ubaque' => 'Ubaque',
  'Ulloa' => 'Ulloa',
  'Umbita' => 'Umbita',
  'Une' => 'Une',
  'Unguia' => 'Unguía',
  'Union Panamericana' => 'Unión Panamericana',
  'Uramita' => 'Uramita',
  'Uribe' => 'Uribe',
  'Uribia' => 'Uribia',
  'Urrao' => 'Urrao',
  'Urumita' => 'Urumita',
  'Usiacuri' => 'Usiacurí',
  'utica' => 'Útica',
  'Valdivia' => 'Valdivia',
  'Valencia' => 'Valencia',
  'Valle de Guamez' => 'Valle de Guamez',
  'Valle de San Jose' => 'Valle de San José',
  'Valle de San Juan' => 'Valle de San Juan',
  'Valledupar' => 'Valledupar',
  'Valparaiso' => 'Valparaíso',
  'Vegachi' => 'Vegachí',
  'Velez' => 'Vélez',
  'Venadillo' => 'Venadillo',
  'Venecia' => 'Venecia',
  'Ventaquemada' => 'Ventaquemada',
  'Vergara' => 'Vergara',
  'Versalles' => 'Versalles',
  'Vetas' => 'Vetas',
  'Viani' => 'Vianí',
  'Victoria' => 'Victoria',
  'Vigia del Fuerte' => 'Vigía del Fuerte',
  'Vijes' => 'Vijes',
  'Villa Caro' => 'Villa Caro',
  'Villa de Leyva' => 'Villa de Leyva',
  'Villa de San Diego de Ubate' => 'Villa de San Diego de Ubate',
  'Villa del Rosario' => 'Villa del Rosario',
  'Villa Rica' => 'Villa Rica',
  'Villagarzon' => 'Villagarzón',
  'Villagomez' => 'Villagómez',
  'Villahermosa' => 'Villahermosa',
  'Villamaria' => 'Villamaría',
  'Villanueva' => 'Villanueva',
  'Villapinzon' => 'Villapinzón',
  'Villarrica' => 'Villarrica',
  'Villavicencio' => 'Villavicencio',
  'Villavieja' => 'Villavieja',
  'Villeta' => 'Villeta',
  'Viota' => 'Viotá',
  'Viracacha' => 'Viracachá',
  'Vista Hermosa' => 'Vista Hermosa',
  'Viterbo' => 'Viterbo',
  'Yacopi' => 'Yacopí',
  'Yacuanquer' => 'Yacuanquer',
  'Yaguara' => 'Yaguará',
  'Yali' => 'Yalí',
  'Yarumal' => 'Yarumal',
  'Yavarate' => 'Yavaraté',
  'Yolombo' => 'Yolombó',
  'Yondo' => 'Yondó',
  'Yopal' => 'Yopal',
  'Yotoco' => 'Yotoco',
  'Yumbo' => 'Yumbo',
  'Zambrano' => 'Zambrano',
  'Zapatoca' => 'Zapatoca',
  'Zapayan' => 'Zapayán',
  'Zaragoza' => 'Zaragoza',
  'Zarzal' => 'Zarzal',
  'Zetaquira' => 'Zetaquira',
  'Zipacon' => 'Zipacón',
  'Zipaquira' => 'Zipaquirá',
  'Zona Bananera' => 'Zona Bananera',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_proceso_c_list.php

 // created: 2022-11-08 19:35:59

$app_list_strings['sasa_proceso_c_list']=array (
  1 => 'Certificaciones',
  2 => 'Certificados',
  3 => 'Extractos',
  4 => 'Firma electrónica',
  5 => 'A1CLICK',
  6 => 'Otros transmite en linea',
  7 => 'Portal transaccional',
  8 => 'Portal de pagos masivos',
  9 => 'Mi fiducia',
  10 => 'Información',
  14 => 'Portal adquiriente',
  15 => 'Mesa de transmite',
  16 => 'Procesos sac (Operaciones call center)',
  17 => 'E-trading',
  18 => 'Certificados no tributario',
  20 => 'Derecho de petición',
  21 => 'Tutela',
  22 => 'Reclamación',
  23 => 'Solicitud',
  24 => 'Certificados tributarios',
  25 => 'PSE',
  26 => 'operaciones call center(operaciones sac)',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_referencia_c_list.php

 // created: 2022-11-17 20:29:51

$app_list_strings['sasa_referencia_c_list']=array (
  '' => '',
  1 => 'Queja',
  2 => 'Apoderado',
  3 => 'Replica',
  4 => 'Escalonamiento',
  5 => 'Anexo nuevo',
  6 => 'Gestión',
  7 => 'Respuesta',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivotipi_c_list.php

 // created: 2022-11-18 15:59:36

$app_list_strings['sasa_motivotipi_c_list']=array (
  '' => '',
  'Queja' => 'Queja',
  'peticion' => 'Petición',
  'sugerencia' => 'Sugerencia',
  'felicitaciones' => 'Felicitaciones',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.PQRsHistorico.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa_pqrs_historico'] = 'PQRs Histórico';
$app_list_strings['moduleListSingular']['sasa_pqrs_historico'] = 'PQRs Histórico';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_tipoproceso_c_list.php

 // created: 2022-12-02 16:48:44

$app_list_strings['sasa_tipoproceso_c_list']=array (
  '' => '',
  'Cliente' => 'Cliente',
  'Escalamiento' => 'Escalamiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_programarllamadavisita_list.php

 // created: 2022-12-07 15:53:20

$app_list_strings['programarllamadavisita_list']=array (
  '' => '',
  'Llamada' => 'Llamada',
  'Visita' => 'Visita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_programarllamadavisita_list.php

 // created: 2023-01-26 21:28:31

$app_list_strings['sasa_programarllamadavisita_list']=array (
  '' => '',
  'Llamada' => 'Llamada',
  'Visita' => 'Visita',
  'No agendar' => 'No agendar',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_envionotificacion_c_list.php

 // created: 2023-01-27 15:31:58

$app_list_strings['sasa_envionotificacion_c_list']=array (
  'Si' => 'Si',
  'No' => 'No',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Unidades_de_negocio_por_cliente.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa1_Unidades_de_negocio_por_'] = 'Unidades de negocio por cliente';
$app_list_strings['moduleListSingular']['sasa1_Unidades_de_negocio_por_'] = 'Unidad de negocio por cliente';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Unidades_de_negocio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasa2_Unidades_de_negocio'] = 'Unidades de negocio';
$app_list_strings['moduleListSingular']['sasa2_Unidades_de_negocio'] = 'Unidades de negocio';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_detalle_c_list.php

 // created: 2023-02-02 19:25:49

$app_list_strings['sasa_detalle_c_list']=array (
  '' => '',
  1 => 'Contacto con un funcionario',
  2 => 'Duplicado de tarjeta recaudo',
  3 => 'Requiere valor a consignar',
  4 => 'Dirección y teléfono sucursales',
  5 => 'Estado del encargo',
  6 => 'Correo Electrónico Funcionario',
  7 => 'Movimientos y Saldos',
  8 => 'Inconsistencia pago fondo de inversión',
  9 => 'Inconsistencia- pago para encargo',
  10 => 'Proyecto Vinculado a Alianza',
  11 => 'Reporte ante Centrales de Riesgo',
  12 => 'Clave documentos encriptados',
  13 => 'Medios de Pago',
  14 => 'Número de producto',
  15 => 'Documentos Alianza (Rut, camara de comercio, super financiera)',
  16 => 'Facturación Electrónica',
  17 => 'Capacitación virtual',
  18 => 'Capacitación presencial',
  19 => 'Aclaración o dudas - Navegación en el portal',
  20 => 'Aclaración dudas - Procesos Internos',
  21 => 'Fallas tecnológicos – Error en Portal',
  22 => 'Fallas de sistemas internos',
  23 => 'Error en información PSE',
  24 => 'Error en login unificado',
  25 => 'Error estado SIFI',
  26 => 'Sin información de proyectos',
  27 => 'Aclaración de dudas WEB INMOBILIARIA',
  28 => 'Falla login unificado',
  29 => 'Error WEB INMOBILIARIA',
  35 => 'Inconsistencistencia- pago para encargo',
  37 => 'No permite crear contrato',
  38 => 'Casos de pagos no acreditados',
  39 => 'Creación de unidades o carga no existe en contrato',
  40 => 'Generación de liquidación de bolsa',
  41 => 'Registro de cuenta natural',
  42 => 'Registro de cuenta jurídica',
  43 => 'Aclaración dudas – Registro de usuario',
  47 => 'Estado inactivo',
  49 => 'Código activación extranjero',
  56 => 'Estado pendiente por confirmar',
  58 => 'Estado bloqueado',
  59 => 'Registro de cuenta',
  60 => 'Autenticación OTP',
  61 => 'Anulación autenticación OTP',
  62 => 'Activación de dispositivo',
  63 => 'Activación Dispositivo - No Exitosa',
  64 => 'Final creacion anexo 2',
  66 => 'Reinicio de contraseña',
  67 => 'Error Configuración Permisos',
  70 => 'Información proceso en tramite',
  71 => 'Soporte descarga documentos',
  75 => 'Consulta y/o soporte - Otro si',
  77 => 'Falla Aplicación – Otro si masivo',
  78 => 'Falla Aplicación – Otro si individual',
  79 => 'Conciliación de cartera',
  80 => 'GMF (4*1000)',
  81 => 'Retención en la fuente clientes',
  82 => 'GMF y Retención',
  83 => 'Inconsistencias en los datos 4*100 y retención',
  84 => 'Inconsistencias por generación',
  85 => 'Certificado de pensiones voluntarias con inconsistencia',
  86 => 'no genera certificado para el periodo solicitado',
  87 => 'Declaración de Renta adquirientes',
  88 => 'No aplica generación de certificado',
  89 => 'Error en la generación de certificado',
  90 => 'Retención de Iva Proveedor',
  91 => 'Retención de Ica Proveedor',
  92 => 'Retención en la fuente Proveedor',
  93 => 'Certificados proveedor',
  94 => 'Error de generación certificados proveedores',
  95 => 'Autogestión',
  96 => 'Error de generación',
  97 => 'Certificado Patrimonial',
  98 => 'No genera certificado en el periodo solicitado',
  99 => 'Certificado de Participación',
  100 => 'Error en certificado',
  101 => 'Falta de información',
  102 => 'Generación de certificado Aportes',
  103 => 'Generación estados de cuenta',
  104 => 'Eliminar Correo electrónico a SIFI - SIRE',
  105 => 'No Genera extracto - apertura en SIFI',
  106 => 'Atar Correo electrónico a SIFI - SIRE',
  107 => 'Fondo inmobiliario',
  108 => 'Fondos de capital privado',
  109 => 'Generación Orion',
  110 => 'Generación de extracto',
  111 => 'No aplica la generación de extracto',
  112 => 'Generación de extracto -Atar Correo electrónico a SIFI - SIRE',
  113 => 'Generación de extracto - Sin envío a Radar',
  114 => 'Generación de extracto -Atar Correo electrónico a SIFI - SIRE - Sin envío a Radar',
  115 => 'Extracto VIP por actualizar',
  116 => 'No aplica generación de extractos',
  117 => 'Generación de extracto - Eliminar Correo electrónico a SIFI - SIRE',
  118 => 'Generación de extracto - Envío con radar',
  119 => 'Generación de extracto -Atar Correo electrónico a SIFI - SIRE -Sin envío a radar',
  120 => 'Generación de extracto -Atar Correo electrónico a SIFI - SIRE -Envío con Radar',
  122 => 'Generación de extracto - Eliminar Correo electrónico SIFI-SIRE -Envío con Radar',
  123 => 'Reenvio de extracto',
  124 => 'Generación de extractos Tapa unificada',
  125 => 'Consulta',
  126 => 'Falla convenio',
  127 => 'Falla NVI',
  128 => 'Actualización de datos',
  129 => 'Actualización de datos No Exitosa',
  130 => 'Firmas y Condiciones de manejo',
  131 => 'Embajadas',
  132 => 'Exoneración 4*1000',
  133 => 'Referencia comercial',
  134 => 'Saldos',
  135 => 'Titularidad',
  136 => 'Movimientos',
  137 => 'Movimientos no registrado',
  138 => 'Otros - Casos especiales',
  139 => 'Revisoria Fiscal',
  140 => 'Error en la generación',
  141 => 'Desbloqueo de usuarios',
  142 => 'Verificación de documento',
  143 => 'Verificación de documento - no exitosa',
  144 => 'Cesiones',
  145 => 'Desistimiento sin procesar',
  146 => 'Desistimientos',
  147 => 'Escrituración',
  148 => 'Informacion Otros Si',
  149 => 'Anexo sin firmar',
  150 => 'Autogestión persona natural',
  151 => 'solicitud telefónica',
  152 => 'Confronta no exitoso',
  153 => 'No procede retiro',
  154 => 'Retiros',
  155 => 'Amortización de plan de pagos',
  156 => 'Sin soporte de pago',
  157 => 'Encuesta',
  158 => 'Falta de saldo',
  159 => 'Simulador',
  160 => 'Capacitación',
  161 => 'Sin detalle',
  162 => 'Modificaciones datos SIFI',
  163 => 'Sarlaft',
  164 => 'Pendiente conciliación',
  165 => 'Falla Aplicación – Desistimientos',
  166 => 'Fallas tecnológicos – Error en Portal',
  167 => 'Aclaración dudas - Navegación en el portal',
  168 => 'Confronta no exitoso',
  169 => 'Parametrización correo electrónico',
  170 => 'Generación de extracto - Certificaciones',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_subproceso_c_list.php

 // created: 2023-02-02 19:44:10

$app_list_strings['sasa_subproceso_c_list']=array (
  '' => '',
  1 => 'Contactar con un funcionario',
  2 => 'Duplicado de la tarjeta recaudo',
  3 => 'Preguntas frecuentes',
  4 => 'Movimientos y Saldos',
  5 => 'Reporte ante Centrales de Riesgo',
  6 => 'Facturación Electrónica',
  7 => 'Capacitación',
  8 => 'Consultas y fallas tecnológicas',
  9 => 'Administrador WIA consultas y fallas',
  10 => 'Tramites de gestión',
  11 => 'Generación de liquidaciones de bolsa',
  12 => 'Registro de usuario y Activación de token',
  13 => 'Creación de usuarios',
  14 => 'Activación de token',
  15 => 'Portal adquirientes fallas y consultas',
  16 => 'Otro si',
  17 => 'GMF (4* mil) y Retención en la fuente clientes',
  18 => 'Declaración de renta Adquirientes',
  19 => 'Certificados Contables',
  20 => 'Certificado Patrimonial',
  21 => 'Certificado de Participación',
  22 => 'Certificado de Aportes y estado de cuenta',
  23 => 'Atar correo electrónico a SIFI-SIRE',
  24 => 'Fondo capital privado y fondo inmobiliario',
  25 => 'Generación Orion',
  26 => 'Fondo de pensión voluntaria',
  27 => 'Fondo de Inversión',
  28 => 'Extractos Alianza Valores y Tapa unificada',
  29 => 'Información proceso en trámite',
  30 => 'Medios de pago',
  31 => 'Reinicio de contraseña',
  32 => 'Actualización de datos',
  33 => 'Firmas y condiciones de manejo',
  34 => 'General',
  35 => 'movimientos',
  36 => 'Otros casos',
  37 => 'Revisoría fiscal',
  38 => 'Reinicio de contraseña y desbloqueo de usuario',
  39 => 'Verificación de documento',
  40 => 'Trámites inmobiliarios',
  41 => 'Registro de cuenta',
  42 => 'Retiros',
  43 => 'Amortización de plan de pagos',
  44 => 'Simulador y encuesta',
  45 => 'Consultas y fallas tecnológicas PSE',
  46 => 'Desistimiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleIconList.php

// created: 2023-02-03 09:39:49
$app_list_strings['moduleIconList']['Home'] = 'Ho';
$app_list_strings['moduleIconList']['Contacts'] = 'Co';
$app_list_strings['moduleIconList']['Accounts'] = 'Ac';
$app_list_strings['moduleIconList']['Opportunities'] = 'Op';
$app_list_strings['moduleIconList']['Cases'] = 'Ca';
$app_list_strings['moduleIconList']['Notes'] = 'No';
$app_list_strings['moduleIconList']['Calls'] = 'Ca';
$app_list_strings['moduleIconList']['Emails'] = 'Em';
$app_list_strings['moduleIconList']['Meetings'] = 'Me';
$app_list_strings['moduleIconList']['Tasks'] = 'Ta';
$app_list_strings['moduleIconList']['Calendar'] = 'Ca';
$app_list_strings['moduleIconList']['Leads'] = 'Le';
$app_list_strings['moduleIconList']['Currencies'] = 'Cu';
$app_list_strings['moduleIconList']['Contracts'] = 'Co';
$app_list_strings['moduleIconList']['Quotes'] = 'Qu';
$app_list_strings['moduleIconList']['Products'] = 'QL';
$app_list_strings['moduleIconList']['WebLogicHooks'] = 'WL';
$app_list_strings['moduleIconList']['ProductCategories'] = 'PC';
$app_list_strings['moduleIconList']['ProductTypes'] = 'PT';
$app_list_strings['moduleIconList']['ProductTemplates'] = 'PC';
$app_list_strings['moduleIconList']['ProductBundles'] = 'PB';
$app_list_strings['moduleIconList']['ProductBundleNotes'] = 'PB';
$app_list_strings['moduleIconList']['Reports'] = 'Re';
$app_list_strings['moduleIconList']['Forecasts'] = 'Fo';
$app_list_strings['moduleIconList']['ForecastWorksheets'] = 'FW';
$app_list_strings['moduleIconList']['ForecastManagerWorksheets'] = 'FM';
$app_list_strings['moduleIconList']['Quotas'] = 'Qu';
$app_list_strings['moduleIconList']['VisualPipeline'] = 'VP';
$app_list_strings['moduleIconList']['ConsoleConfiguration'] = 'CC';
$app_list_strings['moduleIconList']['SugarLive'] = 'Su';
$app_list_strings['moduleIconList']['Teams'] = 'Te';
$app_list_strings['moduleIconList']['TeamNotices'] = 'TN';
$app_list_strings['moduleIconList']['Manufacturers'] = 'Ma';
$app_list_strings['moduleIconList']['Activities'] = 'Ac';
$app_list_strings['moduleIconList']['Comments'] = 'Co';
$app_list_strings['moduleIconList']['Subscriptions'] = 'Su';
$app_list_strings['moduleIconList']['Bugs'] = 'Bu';
$app_list_strings['moduleIconList']['Feeds'] = 'RS';
$app_list_strings['moduleIconList']['iFrames'] = 'MS';
$app_list_strings['moduleIconList']['TimePeriods'] = 'TP';
$app_list_strings['moduleIconList']['TaxRates'] = 'TR';
$app_list_strings['moduleIconList']['ContractTypes'] = 'CT';
$app_list_strings['moduleIconList']['Schedulers'] = 'Sc';
$app_list_strings['moduleIconList']['Project'] = 'Pr';
$app_list_strings['moduleIconList']['ProjectTask'] = 'PT';
$app_list_strings['moduleIconList']['Campaigns'] = 'Ca';
$app_list_strings['moduleIconList']['CampaignLog'] = 'CL';
$app_list_strings['moduleIconList']['CampaignTrackers'] = 'CT';
$app_list_strings['moduleIconList']['Documents'] = 'Do';
$app_list_strings['moduleIconList']['DocumentRevisions'] = 'DR';
$app_list_strings['moduleIconList']['Connectors'] = 'Co';
$app_list_strings['moduleIconList']['Notifications'] = 'No';
$app_list_strings['moduleIconList']['Sync'] = 'Sy';
$app_list_strings['moduleIconList']['HintAccountsets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNotificationTargets'] = 'Hi';
$app_list_strings['moduleIconList']['HintNewsNotifications'] = 'Hi';
$app_list_strings['moduleIconList']['HintEnrichFieldConfigs'] = 'Hi';
$app_list_strings['moduleIconList']['ExternalUsers'] = 'EU';
$app_list_strings['moduleIconList']['ReportMaker'] = 'AR';
$app_list_strings['moduleIconList']['DataSets'] = 'DF';
$app_list_strings['moduleIconList']['CustomQueries'] = 'CQ';
$app_list_strings['moduleIconList']['pmse_Inbox'] = 'Pr';
$app_list_strings['moduleIconList']['pmse_Project'] = 'PD';
$app_list_strings['moduleIconList']['pmse_Business_Rules'] = 'PB';
$app_list_strings['moduleIconList']['pmse_Emails_Templates'] = 'PE';
$app_list_strings['moduleIconList']['BusinessCenters'] = 'BC';
$app_list_strings['moduleIconList']['Shifts'] = 'Sh';
$app_list_strings['moduleIconList']['ShiftExceptions'] = 'SE';
$app_list_strings['moduleIconList']['Purchases'] = 'Pu';
$app_list_strings['moduleIconList']['PurchasedLineItems'] = 'PL';
$app_list_strings['moduleIconList']['MobileDevices'] = 'Mo';
$app_list_strings['moduleIconList']['PushNotifications'] = 'Pu';
$app_list_strings['moduleIconList']['Escalations'] = 'Es';
$app_list_strings['moduleIconList']['DocumentTemplates'] = 'DT';
$app_list_strings['moduleIconList']['DocumentMerges'] = 'DM';
$app_list_strings['moduleIconList']['CloudDrivePaths'] = 'CD';
$app_list_strings['moduleIconList']['WorkFlow'] = 'WD';
$app_list_strings['moduleIconList']['EAPM'] = 'EA';
$app_list_strings['moduleIconList']['Worksheet'] = 'Wo';
$app_list_strings['moduleIconList']['Users'] = 'Us';
$app_list_strings['moduleIconList']['Employees'] = 'Em';
$app_list_strings['moduleIconList']['Administration'] = 'Ad';
$app_list_strings['moduleIconList']['ACLRoles'] = 'Ro';
$app_list_strings['moduleIconList']['InboundEmail'] = 'IE';
$app_list_strings['moduleIconList']['Releases'] = 'Re';
$app_list_strings['moduleIconList']['Prospects'] = 'Ta';
$app_list_strings['moduleIconList']['Queues'] = 'Qu';
$app_list_strings['moduleIconList']['EmailMarketing'] = 'EM';
$app_list_strings['moduleIconList']['EmailTemplates'] = 'ET';
$app_list_strings['moduleIconList']['SNIP'] = 'EA';
$app_list_strings['moduleIconList']['ProspectLists'] = 'TL';
$app_list_strings['moduleIconList']['SavedSearch'] = 'SS';
$app_list_strings['moduleIconList']['UpgradeWizard'] = 'UW';
$app_list_strings['moduleIconList']['Trackers'] = 'Tr';
$app_list_strings['moduleIconList']['TrackerPerfs'] = 'TP';
$app_list_strings['moduleIconList']['TrackerSessions'] = 'TS';
$app_list_strings['moduleIconList']['TrackerQueries'] = 'TQ';
$app_list_strings['moduleIconList']['FAQ'] = 'FA';
$app_list_strings['moduleIconList']['Newsletters'] = 'Ne';
$app_list_strings['moduleIconList']['SugarFavorites'] = 'Fa';
$app_list_strings['moduleIconList']['PdfManager'] = 'PM';
$app_list_strings['moduleIconList']['DataArchiver'] = 'DA';
$app_list_strings['moduleIconList']['ArchiveRuns'] = 'AR';
$app_list_strings['moduleIconList']['OAuthKeys'] = 'OC';
$app_list_strings['moduleIconList']['OAuthTokens'] = 'OT';
$app_list_strings['moduleIconList']['Filters'] = 'Fi';
$app_list_strings['moduleIconList']['UserSignatures'] = 'ES';
$app_list_strings['moduleIconList']['Shippers'] = 'SP';
$app_list_strings['moduleIconList']['Styleguide'] = 'St';
$app_list_strings['moduleIconList']['Feedbacks'] = 'Fe';
$app_list_strings['moduleIconList']['Tags'] = 'Ta';
$app_list_strings['moduleIconList']['Categories'] = 'Ca';
$app_list_strings['moduleIconList']['Dashboards'] = 'Da';
$app_list_strings['moduleIconList']['OutboundEmail'] = 'ES';
$app_list_strings['moduleIconList']['EmailParticipants'] = 'EP';
$app_list_strings['moduleIconList']['DataPrivacy'] = 'DP';
$app_list_strings['moduleIconList']['ReportSchedules'] = 'RS';
$app_list_strings['moduleIconList']['CommentLog'] = 'CL';
$app_list_strings['moduleIconList']['Holidays'] = 'Ho';
$app_list_strings['moduleIconList']['ChangeTimers'] = 'CT';
$app_list_strings['moduleIconList']['Metrics'] = 'Me';
$app_list_strings['moduleIconList']['Messages'] = 'Me';
$app_list_strings['moduleIconList']['Audit'] = 'Au';
$app_list_strings['moduleIconList']['RevenueLineItems'] = 'RL';
$app_list_strings['moduleIconList']['DocuSignEnvelopes'] = 'DE';
$app_list_strings['moduleIconList']['Geocode'] = 'Ge';
$app_list_strings['moduleIconList']['DRI_Workflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Task_Templates'] = 'SG';
$app_list_strings['moduleIconList']['DRI_SubWorkflows'] = 'SG';
$app_list_strings['moduleIconList']['DRI_Workflow_Templates'] = 'SG';
$app_list_strings['moduleIconList']['CJ_WebHooks'] = 'SA';
$app_list_strings['moduleIconList']['CJ_Forms'] = 'SG';
$app_list_strings['moduleIconList']['DRI_SubWorkflow_Templates'] = 'SG';
$app_list_strings['moduleIconList']['Library'] = 'Li';
$app_list_strings['moduleIconList']['EmailAddresses'] = 'EA';
$app_list_strings['moduleIconList']['Words'] = 'Wo';
$app_list_strings['moduleIconList']['Sugar_Favorites'] = 'Fa';
$app_list_strings['moduleIconList']['KBDocuments'] = 'KB';
$app_list_strings['moduleIconList']['KBContents'] = 'KB';
$app_list_strings['moduleIconList']['KBArticles'] = 'KB';
$app_list_strings['moduleIconList']['KBContentTemplates'] = 'KB';
$app_list_strings['moduleIconList']['KBLocalizations'] = 'Lo';
$app_list_strings['moduleIconList']['KBRevisions'] = 'Re';
$app_list_strings['moduleIconList']['EmbeddedFiles'] = 'EF';
$app_list_strings['moduleIconList']['fbsg_ConstantContactIntegration'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_TableTest'] = 'Ta';
$app_list_strings['moduleIconList']['fbsg_test'] = 'CE';
$app_list_strings['moduleIconList']['fbsg_CCErrors'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIErrors'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIContactLog'] = 'CC';
$app_list_strings['moduleIconList']['fbsg_CCIntegrationLog'] = 'CC';
$app_list_strings['moduleIconList']['sasa_SaldosAF'] = 'SA';
$app_list_strings['moduleIconList']['sasa_SaldosAV'] = 'SA';
$app_list_strings['moduleIconList']['sasa_MovimientosAF'] = 'MA';
$app_list_strings['moduleIconList']['sasa_MovimientosAV'] = 'MA';
$app_list_strings['moduleIconList']['sasa_MovimientosAVDivisas'] = 'MA';
$app_list_strings['moduleIconList']['sasa_ProductosAFAV'] = 'PA';
$app_list_strings['moduleIconList']['sasa_Categorias'] = 'Ca';
$app_list_strings['moduleIconList']['sasaS_SaldosConsolidados'] = 'SC';
$app_list_strings['moduleIconList']['sasaP_PresupuestosxAsesor'] = 'Px';
$app_list_strings['moduleIconList']['sasaP_PresupuestoxProducto'] = 'Px';
$app_list_strings['moduleIconList']['sasa_sasa_tipificaciones'] = 'Ti';
$app_list_strings['moduleIconList']['sasa_pqrs_historico'] = 'PH';
$app_list_strings['moduleIconList']['sasa1_Unidades_de_negocio_por_'] = 'Ud';
$app_list_strings['moduleIconList']['sasa2_Unidades_de_negocio'] = 'Ud';
$app_list_strings['moduleIconList']['ops_Backups'] = 'Ba';
$app_list_strings['moduleIconList']['Roles'] = 'Ro';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_source_dom.php

 // created: 2023-02-09 16:43:46

$app_list_strings['source_dom']=array (
  '' => '',
  'Internal' => 'Internal',
  'Forum' => 'Forum',
  'Web' => 'Web',
  'InboundEmail' => 'Email',
  'Twitter' => 'Twitter',
  'Portal' => 'Portal',
  'SFC' => 'SFC',
  'Defensor del Consumidor' => 'Defensor del Consumidor',
  'Carta' => 'Carta',
  'Telefono' => 'Teléfono',
  'Portal publico' => 'Portal público',
  'Portal transaccional' => 'Portal transaccional',
  'Bizagi' => 'Bizagi',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_estadorespuesta_c_list.php

 // created: 2023-03-10 13:23:53

$app_list_strings['sasa_estadorespuesta_c_list']=array (
  '' => '',
  1 => 'A favor de Alianza',
  2 => 'A favor del cliente',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_estado2_c_list.php

 // created: 2023-03-10 13:42:46

$app_list_strings['sasa_estado2_c_list']=array (
  '' => '',
  'Abierta' => 'Abierta',
  'Escalada' => 'Escalada',
  'Vencida' => 'Vencida',
  'Cerrada' => 'Cerrada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_status_list.php

 // created: 2023-04-10 20:40:22

$app_list_strings['status_list']=array (
  '' => '',
  1 => 'Recibida',
  2 => 'Abierta',
  3 => 'Abierta en conciliación',
  4 => 'Cerrada',
  10 => 'Crear Queja SFC',
  5 => 'Reabierto',
  6 => 'Escalada',
  7 => 'Vencida',
  8 => 'Nuevo',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_categoria_c_list.php

 // created: 2023-05-17 15:40:43

$app_list_strings['sasa_categoria_c_list']=array (
  '' => '',
  'trato inadecuado' => 'Trato Inadecuado',
  'inconsistencias' => 'Inconsistencias',
  'incumplimiento' => 'Incumplimiento',
  'informacion' => 'Información',
  'Aplazamiento' => 'Aplazamiento',
  'fallas y_o errores' => 'Fallas y/o Errores',
  'datos_personales' => 'Datos Personales',
  'reporte ante centrales' => 'Reportes antes Centrales',
  'otros_motivos' => 'Otros Motivos',
  'desvalorizacion_perdidas_diferencias' => 'Desvalorización Perdidas y Diferencias',
  'negativao_demora_en_el_pago' => 'Negativa o demora en el pago',
  'suplantacion_y_o_fraude' => 'Suplantación y/o Fraude',
  1 => 'Certificaciones',
  2 => 'Certificados',
  3 => 'Extractos',
  4 => 'Firma electrónica',
  5 => 'A1CLICK',
  6 => 'Otros tramites en linea',
  7 => 'Portal transaccional',
  8 => 'Portal de pagos masivos',
  9 => 'Mi fiducia',
  10 => 'Información',
  14 => 'Portal adquirientes',
  15 => 'Información otros tramites inmobiliarios',
  16 => 'Operaciones transaccionales',
  17 => 'E-trading',
  18 => 'Certificados no tributario',
  20 => 'Derecho de petición',
  21 => 'Tutela',
  22 => 'Reclamación',
  23 => 'Solicitud',
  24 => 'Certificados tributarios',
  25 => 'PSE',
  26 => 'operaciones call center(operaciones sac)',
  27 => 'Falsificación',
  28 => 'Cobros',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sasa_motivo_c_list.php

 // created: 2023-05-17 15:44:03

$app_list_strings['sasa_motivo_c_list']=array (
  '' => '',
  401 => 'Cobro de penalidad por desistimiento',
  402 => 'Inconsistencia o falta de pago de la cláusula penal por incumplimiento o demora del proyecto inmobiliario',
  403 => 'Incumplimiento de inversiones con políticas definidas',
  404 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  405 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  406 => 'Inconsistencia en el valor de rendimientos',
  407 => 'Redención de derechos o participaciones - cancelacion del producto o servicio',
  408 => 'Incumplimiento del deber legal de rendición de cuentas (información periódica)',
  499 => 'Otros motivos',
  906 => 'Mal trato por parte de un funcionario',
  907 => 'Mal trato por parte del asesor comercial o proveedor',
  965 => 'Indebido deber de asesoría',
  938 => 'Inconsistencias en los pagos a terceros',
  961 => 'Inconsistencias en el movimiento y saldo total del producto',
  909 => 'Incumplimiento de los términos del contrato',
  902 => 'Dificultad en el acceso a la información',
  903 => 'Información o asesoría incompleta y/o errada',
  904 => 'Información inoportuna',
  945 => 'Dificultad o imposibilidad para realizar transacciones o consulta de información por el canal',
  949 => 'Errores en el contenido de la información en informes, extractos o reportes.',
  964 => 'Información sujeta a reserva',
  905 => 'Dificultad en la comunicación con la entidad',
  946 => 'Demora en la atención o en el servicio requerido',
  928 => 'Demora en la respuesta a quejas, reclamos o peticiones',
  942 => 'Demora o no aplicación del pago',
  921 => 'Demora o no devolución de saldos, aportes o primas',
  926 => 'No disponibilidad o fallas de los canales de atención',
  963 => 'Fallas o inoportunidad en el proceso de vinculación',
  943 => 'Error en la aplicación del pago',
  955 => 'Error en la facturación o cobro no pactado',
  929 => 'Errores en la resolución de quejas, reclamos o peticiones.',
  934 => 'Actualización equivocada de datos personales',
  933 => 'Demora o no modificación de datos personales',
  935 => 'Inadecuado tratamiento de datos personales',
  910 => 'Presunta suplantación de personas',
  908 => 'Presunta actuación fraudulenta o no ética del personal',
  931 => 'Reporte injustificado a centrales de riesgo',
  932 => 'No levantamiento de reporte negativo a centrales de riesgo',
  916 => 'Vinculación no autorizada',
  901 => 'Publicidad engañosa',
  917 => 'Condicionamiento a la adquisición de productos o servicios',
  962 => 'Inconformidad con procesos internos de conocimiento del cliente y SARLAFT',
  954 => 'Incrementos de tarifas no pactadas o informadas',
  950 => 'Limitación en la expedición de certificaciones',
  956 => 'Modificación de condiciones en contratos',
  918 => 'No cancelación o terminación de los productos',
  920 => 'No entrega de paz y salvo',
  930 => 'No resolución a quejas, peticiones y reclamos',
  927 => 'Obstáculo para la interposición de quejas, reclamos o peticiones',
  948 => 'Omisión o envío tardío o inoportuno de informes, extractos o reportes a los que esté obligada la entidad.',
  952 => 'Producto terminado o cancelado sin justificación',
  947 => 'Seguridad en canales',
  940 => 'Transacción no reconocida',
  513 => 'Inconsistencia en el cálculo y /o cobro de penalizaciones',
  514 => 'Inconsistencias en la aplicación de aportes, retiros y cancelaciones',
  515 => 'Inconsistencia en el valor de rendimientos',
  501 => 'Inconsistencia en la compra, intercambio, transferencia, traspaso y/o redención',
  505 => 'Incumplimiento en instrucción del cliente (ejecución operación)',
  512 => 'Incumplimiento de inversiones con políticas definidas',
  502 => 'Desvalorización por riesgos del mercado',
  511 => 'Perdida o desvalorización unidad',
  966 => 'Fallas en operaciones en moneda extranjera',
  967 => 'Diferencias en monetización',
  504 => 'Errores en la colocación y adjudicación de valores',
  510 => 'Negativa o demora en el pago',
  923 => 'Negación injustificada a la apertura del producto',
  1 => 'Contactar con un funcionario',
  2 => 'Duplicado de la tarjeta recaudo',
  3 => 'Preguntas frecuentes',
  4 => 'Movimientos y Saldos',
  5 => 'Reporte ante Centrales de Riesgo',
  6 => 'Facturación Electrónica',
  7 => 'Capacitación',
  8 => 'Consultas y fallas tecnológicas',
  9 => 'Administrador WIA consultas y fallas',
  10 => 'Tramites de gestión',
  11 => 'Generación de liquidaciones de bolsa',
  12 => 'Registro de usuario y Activación de token',
  13 => 'Creación de usuarios',
  14 => 'Activación de token',
  15 => 'Portal adquirientes fallas y consultas',
  16 => 'Otro si',
  17 => 'GMF (4* mil) y Retención en la fuente clientes',
  18 => 'Declaración de renta Adquirientes',
  19 => 'Certificados Contables',
  20 => 'Certificado Patrimonial',
  21 => 'Certificado de Participación',
  22 => 'Certificado de Aportes y estado de cuenta',
  23 => 'Atar correo electrónico a SIFI-SIRE',
  24 => 'Fondo capital privado y fondo inmobiliario',
  25 => 'Generación Orion',
  26 => 'Fondo de pensión voluntaria',
  27 => 'Fondo de Inversión',
  28 => 'Extractos Alianza Valores y Tapa unificada',
  29 => 'Información proceso en trámite',
  30 => 'Medios de pago',
  31 => 'Reinicio de contraseña',
  32 => 'Actualización de datos',
  33 => 'Firmas y condiciones de manejo',
  34 => 'General',
  35 => 'movimientos',
  36 => 'Otros casos',
  37 => 'Revisoría fiscal',
  38 => 'Reinicio de contraseña y desbloqueo de usuario',
  39 => 'Verificación de documento',
  40 => 'Trámites inmobiliarios',
  41 => 'Registro de cuenta',
  42 => 'Retiros',
  43 => 'Amortización de plan de pagos',
  44 => 'Simulador y encuesta',
  45 => 'Consultas y fallas tecnológicas PSE',
  203 => 'Comprobantes y/o titulos valores (CDT, cheques, pagarés, comprobantes)',
  215 => 'Pagos o negociación de cheques',
  219 => 'Operaciones fallidas en cajeros electronicos',
);
?>
