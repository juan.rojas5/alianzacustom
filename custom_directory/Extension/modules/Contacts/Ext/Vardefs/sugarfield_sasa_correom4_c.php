<?php
 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_correom4_c']['labelValue']='Correo Electrónico - M4';
$dictionary['Contact']['fields']['sasa_correom4_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_correom4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_correom4_c']['readonly_formula']='';

 ?>