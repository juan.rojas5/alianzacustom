<?php
 // created: 2022-11-08 19:35:59

$app_list_strings['sasa_proceso_c_list']=array (
  '' => '',
  1 => 'Certificaciones',
  2 => 'Certificados',
  3 => 'Extractos',
  4 => 'Firma electrónica',
  5 => 'A1CLICK',
  6 => 'Otros tramites en linea',
  7 => 'Portal transaccional',
  8 => 'Portal de pagos masivos',
  9 => 'Mi fiducia',
  10 => 'Información',
  14 => 'Portal adquirientes',
  15 => 'Información otros tramites inmobiliarios',
  16 => 'Operaciones transaccionales',
  17 => 'E-trading',
  18 => 'Certificados no tributario',
  20 => 'Derecho de petición',
  21 => 'Tutela',
  22 => 'Reclamación',
  23 => 'Solicitud',
  24 => 'Certificados tributarios',
  25 => 'PSE',
  26 => 'operaciones call center(operaciones sac)',
);