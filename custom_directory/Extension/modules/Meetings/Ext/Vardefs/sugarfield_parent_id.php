<?php
 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_id']['len']=255;
$dictionary['Meeting']['fields']['parent_id']['required']=true;
$dictionary['Meeting']['fields']['parent_id']['audited']=false;
$dictionary['Meeting']['fields']['parent_id']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_id']['calculated']=false;
$dictionary['Meeting']['fields']['parent_id']['reportable']=true;
$dictionary['Meeting']['fields']['parent_id']['unified_search']=false;
$dictionary['Meeting']['fields']['parent_id']['group']='';

 ?>