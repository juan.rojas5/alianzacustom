<?php
// created: 2019-01-04 21:22:57
$dictionary["sasa_categorias_sasa_productosafav_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'sasa_categorias_sasa_productosafav_1' => 
    array (
      'lhs_module' => 'sasa_Categorias',
      'lhs_table' => 'sasa_categorias',
      'lhs_key' => 'id',
      'rhs_module' => 'sasa_ProductosAFAV',
      'rhs_table' => 'sasa_productosafav',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sasa_categorias_sasa_productosafav_1_c',
      'join_key_lhs' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
      'join_key_rhs' => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
    ),
  ),
  'table' => 'sasa_categorias_sasa_productosafav_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'sasa_categorias_sasa_productosafav_1sasa_categorias_ida' => 
    array (
      'name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
      'type' => 'id',
    ),
    'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb' => 
    array (
      'name' => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_sasa_categorias_sasa_productosafav_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_sasa_categorias_sasa_productosafav_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_sasa_categorias_sasa_productosafav_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'sasa_categorias_sasa_productosafav_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
      ),
    ),
  ),
);