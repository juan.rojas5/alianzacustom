<?php

use Sugarcrm\Sugarcrm\Util\Uuid;

$job_strings[] = 'SfcCreateComplaints';
function SfcCreateComplaints() {
	$GLOBALS['log']->security("\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("START-SfcCreateComplaints-Momento1. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");

	$return = true;
	try {
		require_once("custom/sasa_projects/sfc/SfcRestApi.php");
		require_once('custom/sasa_projects/sfc/config/config.php');

//++++++++++++++++++++++++MOMENTO 1+++++++++++++++++++++++++++++++++

		$GLOBALS['log']->security("++++ALIANZA VALORES MOMENTO 1:++++");
		getComplaintsFromSfcApi(new SfcRestApi('valores', $config));
		
		$GLOBALS['log']->security("++++ALIANZA FIDUCIARIA MOMENTO 1:++++");
		getComplaintsFromSfcApi(new SfcRestApi('fiduciaria', $config));
//++++++++++++++++++++++++MOMENTO 4+++++++++++++++++++++++++++++++++

		$GLOBALS['log']->security("#####ALIANZA VALORES MOMENTO 4:######");
		getUserFromSfcApi(new SfcRestApi('valores', $config));
		
		$GLOBALS['log']->security("#####ALIANZA FIDUCIARIA MOMENTO 4:######");
		getUserFromSfcApi(new SfcRestApi('fiduciaria', $config));
		
	} catch (Exception $e) {
		$return = false;
		$GLOBALS['log']->security("SFC-Exception: ".$e->getMessage());
	}

	$GLOBALS['log']->security("******************************************************");
    $GLOBALS['log']->security("Fin SfcCreateComplaints Momento1. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("******************************************************");

    return $return;
}
//TRAER USUARIOS DE LA SUPER.
function getUserFromSfcApi($sfcRestApi){

	$cuenta_id;

	$processedUserIds = array();

	$sfcUserResponse = $sfcRestApi->getUserInfo();
	//array de prueba.
	/*$sfcUserResponse = array("response" => array(
		"count"=>1,
		"pages"=>1,
		"results"=>array(
			"numero_id_CF"=>1096187206,
			"tipo_id_CF"=>"Cedula de Ciudadania",
			"nombres"=>"Jhon",
			"apellido"=>"Mantilla",
			"fecha_nacimiento"=>"1987-01-27",
			"correo"=>"jhon.mantilla@sasaconsultoria.com",
			"telefono"=>3043440883,
			"razon_social"=>"jhon devs",
			"direccion"=>"Calle 52 a # 34c 192",
			"departamento_cod"=>"",
			"municipio_cod"=>"",
			)
		)
	);*/


	$GLOBALS['log']->security("Response User:" . print_r($sfcUserResponse, true));
	
	$accountFromSfc2 = json_decode($sfcUserResponse['response']);
	
	/*$accountFromSfc = json_encode($sfcUserResponse['response']);

	$accountFromSfc2 = json_decode($accountFromSfc);*/

	$GLOBALS['log']->security("Response account:" . print_r($accountFromSfc2, true));

	if ($accountFromSfc2->count > 0) {
	
	$GLOBALS['log']->security("Entro count > 1:#: ".$accountFromSfc2->count);
		
		foreach ($accountFromSfc2->results as $index => $accountUser) {
			//$accoutId = getAccountByIdCF($accountUser);
			$GLOBALS['log']->security("###id Cuenta::::".$accountUser->numero_id_CF);
			$cuenta_id = getAccountByUser($accountUser);
			//$GLOBALS['log']->security("###index: " . " datos: " . $accountUser);

			$processedUserIds[] = $accountUser->numero_id_CF;
		}

		
		$GLOBALS['log']->security("Array id User:" . print_r($processedUserIds, true));
		$sfcAckResponse = $sfcRestApi->userAck($processedUserIds);
		$GLOBALS['log']->security("ackUserResponse: " .$sfcAckResponse['response']);
	}	

}
//############Quejas para casos##########
function getComplaintsFromSfcApi($sfcRestApi) {
	$sfcComplaintsResponse = $sfcRestApi->getComplaints();
	$GLOBALS['log']->security("getComplaints: {$sfcComplaintsResponse['response']}");
	$complaintsFromSfc = json_decode($sfcComplaintsResponse['response']);
	$processedComplaintIds = array();
	if ($complaintsFromSfc->count > 0) {

		foreach ($complaintsFromSfc->results as $index => $complaint) {
			$caseId = checkCaseBySfcId($complaint->codigo_queja);
			
			if (empty($caseId)) {
				$caseId = createNewCase($complaint);
			} 

			if ($complaint->anexo_queja) {
				$GLOBALS['log']->security("Complaint has file complaintId: {$complaint->codigo_queja}, creating note");
				$sfcComplaintFileResponse = $sfcRestApi->getFilesComplaint($complaint->codigo_queja);
				$newNote = createNoteWithAttchment($caseId, $complaint->codigo_queja, $sfcComplaintFileResponse['response']);
			}

			$processedComplaintIds[] = $complaint->codigo_queja;
		} 
		$sfcAckResponse = $sfcRestApi->postAck($processedComplaintIds);
		$GLOBALS['log']->security("acKResponse: " .$sfcAckResponse['response']);

		
	} else {
		$GLOBALS['log']->security("SFC: There aren't complaints to report");
	}
}

function createNewCase($complaint) {
	$module = 'Cases';

	$newCase = BeanFactory::newBean($module);
	$newCase->sasa_tipoentidad_c = $complaint->tipo_entidad; 
	$newCase->sasa_codigoentidad_c = $complaint->entidad_cod;
	$newCase->sasa_sfcfechacreacion_c = date('Y-m-d h:i:s', strtotime($complaint->fecha_creacion . ' + 5 hours'));
	$newCase->sasa_codigoqueja_c = $complaint->codigo_queja;
	$newCase->sasa_canal_c = $complaint->canal_cod;
	$newCase->sasa_producto_cod_c = $complaint->producto_cod;
	$newCase->work_log = $complaint->producto_nombre;
	$newCase->description = $complaint->texto_queja;
	$newCase->sasa_anexoqueja_c = $complaint->anexo_queja;
	$newCase->sasa_tutela_c = $complaint->tutela;
	$newCase->sasa_ente_control_c = $complaint->ente_control;
	$newCase->sasa_escalamiento_dcf_c = $complaint->escalamiento_DCF;
	$newCase->sasa_replica_c = $complaint->replica;
	$newCase->resolution = $complaint->argumento_replica;
	$newCase->sasa_desistimiento_c = $complaint->desistimiento_queja;
	$newCase->sasa_quejaexpres_c = $complaint->queja_expres;
	$newCase->sasa_condicionespecial_c = $complaint->condicion_especial;
	$newCase->sasa_direccionsfc_c = $complaint->direccion;
	$newCase->source = 'SFC';

	$newCase->save();
	$accountId = getAccountByIdCF($complaint);
	$contactdId = getContactByIdCFAndAccountId($complaint, $accountId);
	$newCase->primary_contact_id = $contactdId;
	$newCase->load_relationship("accounts"); 
	$newCase->accounts->add($accountId);
	$newCase->load_relationship("sasa_sasa_tipificaciones_cases_1");
	$newCase->sasa_sasa_tipificaciones_cases_1->add(
		syncDataConf('typifications', $complaint->macro_motivo_cod));

	$newCase->save();

	$GLOBALS['log']->security("Case created id: " . $newCase->id);

	return $newCase->id;
}

function urlGetContent ($Url) {
    if (!function_exists('curl_init')){ 
        $errorMessage = "SFC: there isn't installed cURL";
		throw new Exception($errorMessage);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function createFileRelatedToNote($noteId, $urlContent) {
	$content = urlGetContent($urlContent);
	$imageFile = 'upload://' . $noteId;
	$fh = new UploadStream();
	$fh->stream_open($imageFile, 'w');
	$fh->stream_write($content);
	$fh->stream_close();
}

function createNoteWithAttchment($caseId, $complaintId, $sfcComplaintFileResponse) {
	if (empty($caseId) OR empty($sfcComplaintFileResponse) OR empty($complaintId)) {
		$errorMessage = "SFC: caseId: {$caseId} OR sfcComplaintFileResponse: {$sfcComplaintFileResponse} OR complaintId: {$complaintId} can't be empty createNoteWithAttchment";
		throw new Exception($errorMessage);
	}
	$GLOBALS['log']->security("Responsefile is " . $sfcComplaintFileResponse);
	$sfcFileResponseObject = json_decode($sfcComplaintFileResponse);
	if ($sfcFileResponseObject->count > 0) {
		$module = 'Notes';	
		$parentNote = BeanFactory::newBean($module);
		$parentNoteId = Uuid::uuid4();
		$parentNote->name = 'Attachments from SFC - '.$complaintId;
		$parentNote->new_with_id = true;
		$parentNote->parent_type = 'Cases';
		$parentNote->parent_id = $caseId;
		$parentNote->save();
		
		$childArrayIds = array();
		foreach ($sfcFileResponseObject->results as $key => $sfcImgObject) {

			$newNote = BeanFactory::newBean($module);
			$noteId = Uuid::uuid4();
			$newNote->id = $noteId;
			$newNote->new_with_id = true;

			createFileRelatedToNote($noteId, $sfcImgObject->file);

			$newNote->file_mime_type = get_file_mime_type("upload://{$noteId}");

			$pathVariables = parse_url($sfcImgObject->file)['path'];
			$fileName = substr($pathVariables, strrpos($pathVariables, '/') + 1);
			$newNote->name = $complaintId ." - " .$fileName;
			$newNote->sasa_referencia_c = $sfcImgObject->reference;
			$newNote->sasa_id_sfc_c = $sfcImgObject->id;
			$newNote->file_ext = substr($fileName, strpos($fileName, '.') + 1);
			$newNote->filename = $fileName;
			$newNote->attachment_flag = 1;
			$newNote->note_parent_id = $parentNote->id;
			$note->entry_source = 'internal';

			$newNote->save();
			$childArrayIds[] = $newNote->id;
			$GLOBALS['log']->security("Note created id: " . $newNote->id);
		}
		$description = "Se han relacionado {$sfcFileResponseObject->count} adjuntos de la SFC:\n";
		$description .= implode("\n", $childArrayIds);
		$parentNote->description = $description;
		$parentNote->save();
	}
}


function syncDataConf($dataType, $indexCode) {
	require('custom/sasa_projects/sfc/config/config.php');

	if ($dataType == 'typifications') {
		if ($indexCode == 1 ) $indexCode = 907;
		else if ($indexCode == 2 ) $indexCode = 926;
		else if ($indexCode == 5 ) $indexCode = 901;
		else if ($indexCode == 6 ) $indexCode = 903;
		else if ($indexCode == 7 ) $indexCode = 905;
		else if ($indexCode == 8 ) $indexCode = 914;
		else if ($indexCode == 11 ) $indexCode = 916;
		else if ($indexCode == 12 ) $indexCode = 917;
		else if ($indexCode == 13 ) $indexCode = 918;
		else if ($indexCode == 15 ) $indexCode = 948;
		else if ($indexCode == 16 ) $indexCode = 921;
		else if ($indexCode == 17 ) $indexCode = 923;
		else if ($indexCode == 18 ) $indexCode = 928;
		else if ($indexCode == 21 ) $indexCode = 951;
		else if ($indexCode == 22 ) $indexCode = 959;
		else if ($indexCode == 26 ) $indexCode = 942;
		else if ($indexCode == 29 ) $indexCode = 404;
		else if ($indexCode == 35 ) $indexCode = 942;
		else if ($indexCode == 47 ) $indexCode = 515;
		else if ($indexCode == 48 ) $indexCode = 515;
		else if ($indexCode == 49 ) $indexCode = 515;
		else if ($indexCode == 50 ) $indexCode = 402;
		else if ($indexCode == 51 ) $indexCode = 408;
		else if ($indexCode == 99 ) $indexCode = 499;
		else if ($indexCode == 20 ) $indexCode = 939;
		else if ($indexCode == 67 ) $indexCode = 946;
		else if ($indexCode == 52 ) $indexCode = 501;
		else if ($indexCode == 53 ) $indexCode = 511;
		else if ($indexCode == 55 ) $indexCode = 504;
		else if ($indexCode == 56 ) $indexCode = 505;
		else if ($indexCode == 60 ) $indexCode = 511;
		else if($indexCode == 61 ) $indexCode = 512;
	}

	$sugarValue = $config['sync'][$dataType][$indexCode];
	if (empty($sugarValue)) {
		switch ($dataType) {
			case 'typifications':
				$sugarValue = 'f0c450c6-b9db-11ec-9148-901b0efe306a';
				break;														
			default:
				$sugarValue = $indexCode;
				break;
		}
	}

	return $sugarValue;
}

function checkCaseBySfcId($complaintId){
	global $db;

	$query="SELECT cases_cstm.id_c
	FROM cases 
	INNER JOIN cases_cstm ON cases.id = cases_cstm.id_c
	WHERE cases_cstm.sasa_codigoqueja_c = '{$complaintId}' AND cases.deleted <> 1 
	LIMIT 1";
    $result = $db->query($query);
    $caseId = NULL;
    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id_c'])){
    		$GLOBALS['log']->security("Complaint exists {$row['id_c']}");
    		$caseId = $row['id_c'];
    	} 
    }

    return $caseId;
}
//###############MOMENTO 4.####################
function getAccountByUser($account) {

	$doc = $account->numero_id_CF;
	$module = 'Contacts';
	$contactId = '';
    $accountId = '';
	
	$GLOBALS['log']->security("#####get Account document :######".$doc);

	global $db;

	//Consulta para revisar si la cuenta tiene relacionado el contacto
	$query="SELECT c.id
		FROM accounts a
		INNER JOIN accounts_cstm ON accounts_cstm.id_c = a.id 
		INNER JOIN accounts_contacts ac ON ac.account_id = a.id
		INNER JOIN contacts c ON c.id = ac.contact_id
		INNER JOIN contacts_cstm cc ON cc.id_c = c.id
		WHERE accounts_cstm.sasa_nroidentificacion_c= '{$doc}' AND a.deleted <> 1 AND c.deleted = 0 AND cc.sasa_nroidentificacion_c = '{$doc}' AND ac.deleted = 0
		LIMIT 1";

    $result = $db->query($query);


    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id'])){
    		$GLOBALS['log']->security("Contacts exists user: {$row['id']}");
    		$contactId = $row['id'];
    	} 
    }



	if(empty($contactId)){
		//####Consulto si existe un contacto.######
		$contactIdExist = '';

		$queryContact = "SELECT c.id 
		FROM contacts c 
		INNER JOIN contacts_cstm cc ON cc.id_c = c.id
		WHERE cc.sasa_nroidentificacion_c = '{$doc}' AND c.deleted = 0";
		
		$resultContact = $db->query($queryContact);

		while($row = $db->fetchByAssoc($resultContact)){
	    
	    if(!empty($row['id'])){
	    		$GLOBALS['log']->security("Contact exists {$row['id']}");
	    		$contactIdExist = $row['id'];
	    	} 
	    }

		if(empty($contactIdExist)){
			//Empiezo crear contacto
			$GLOBALS['log']->security("#####Create Contacts empty: contacts id:######");
		    $bean = BeanFactory::newBean($module);
			
			$bean->first_name = $account->nombre;
			$bean->last_name = $account->apellido;
			$bean->sasa_tipoidentificacion_c = syncDataConf('documentType', $account->tipo_id_CF);
			$bean->sasa_nroidentificacion_c = $account->numero_id_CF;

		}else{
			$GLOBALS['log']->security("#####UPDATE Contacts empty: sin relación:######");
			$bean = BeanFactory::retrieveBean($module, $contactIdExist);
		}

		//Consulto la cuenta para la relación
		$query="SELECT accounts.id 
		FROM accounts 
		INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id 
		WHERE accounts_cstm.sasa_nroidentificacion_c= '{$doc}' AND accounts.deleted <> 1 
		LIMIT 1";

	    $result = $db->query($query);
	   

	    while($row = $db->fetchByAssoc($result)){
	    	if(!empty($row['id'])){
	    		$GLOBALS['log']->security("Account exists {$row['id']}");
	    		$accountId = $row['id'];
	    	} 
	    }


	}else{

		$GLOBALS['log']->security("#####Update Contacts :######".$contactId);

		$bean = BeanFactory::retrieveBean($module, $contactId);
	}

		//Populate bean fields
		//$bean->name = $account->nombre ." ". $account->apellido;
		$bean->sasa_nombrem4_c = $account->nombre;
		$bean->sasa_apellidom4_c = $account->apellido;
		$bean->sasa_nroidentificacionm4_c = $account->numero_id_CF;
		$bean->sasa_tipoidentificacionm4_c = syncDataConf('documentType', $account->tipo_id_CF);
		$bean->sasa_correom4_c = $account->correo;
		$bean->sasa_celularm4_c = $account->telefono;
		$bean->sasa_direccionprincm4_c  = $account->direccion;
		$bean->sasa_municipiom4_c = syncDataConf('cities', $account->municipio_cod);
		$bean->sasa_departamentom4_c = syncDataConf('states', $account->departamento_cod);
		$bean->sasa_fechanacm4_c = $account->fecha_nacimiento;
		$bean->sasa_razon_socialm4_c = $account->razon_social;
		/*if(!empty($accountId)){
			$bean->load_relationship('accounts');
            $bean->account_id->add($accountId);
		}*/
		//Save
		$bean->save();
		
		$contactId = $bean->id;
	
		if(!empty($accountId)){

			$GLOBALS['log']->security("Contact Relacionado o Creado: ".$contactId);

			$bean = BeanFactory::getBean('Accounts', $accountId);
			$bean->account_id = $accountId;
			$bean->load_relationship('contacts');
            $bean->contacts->add($contactId);
		}
		
		return $contactId;

}
function getAccountByIdCF($complaint) {
	global $db;

	$query="SELECT accounts.id 
	FROM accounts 
	INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts.id 
	WHERE accounts_cstm.sasa_nroidentificacion_c= '{$complaint->numero_id_CF}' AND accounts.deleted <> 1 
	LIMIT 1";
    $result = $db->query($query);
    $accountId = '';
    while($row = $db->fetchByAssoc($result)){
    	if(!empty($row['id'])){
    		$GLOBALS['log']->security("Account exists {$row['id']}");
    		$accountId = $row['id'];
    	} 
    }
    if(empty($accountId)){
		$module = 'Accounts';
		$newAccount = BeanFactory::newBean($module);

		$newAccount->sasa_pais_c = syncDataConf('countries', $complaint->codigo_pais); //sync //account
		$newAccount->sasa_departamento_c = syncDataConf('states', $complaint->departamento_cod); //sync //account
		$newAccount->sasa_municipio_c = syncDataConf('cities', $complaint->municipio_cod); 
		$newAccount->sasa_tipoidentificacion_c = syncDataConf('documentType', $complaint->tipo_id_CF); //account
		$newAccount->sasa_nroidentificacion_c = $complaint->numero_id_CF; //account
		$newAccount->sasa_telefono_c = $complaint->telefono;	//account
		$newAccount->email1 = $complaint->correo;	//account
		$newAccount->sasa_tipopersona_c = syncDataConf('personType', $complaint->tipo_persona); //account	

		$newAccount->name = $complaint->nombres;
		$newAccount->description = 'Cliente no registra en el CRM.';
		$newAccount->account_type = 'No Registra';

		$newAccount->save();

		$accountId = $newAccount->id;
		$GLOBALS['log']->security("There isn't account, new account: {$accountId}");
    }

    return $accountId;
}

function getContactByIdCFAndAccountId($complaint, $accountId) {
	global $db;
	$contactId = '';
	$query="SELECT contacts.id 
			FROM contacts 
			INNER JOIN contacts_cstm ON contacts_cstm.id_c = contacts.id 
			WHERE contacts_cstm.sasa_nroidentificacion_c = '{$complaint->numero_id_CF}' 
			LIMIT 1";
    $result = $db->query($query);
    $lastDbError = $db->lastDbError();
    if(!empty($lastDbError)){
        $GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}");
    } else {
    	while($row = $db->fetchByAssoc($result)){
    		$contactId = $row['id'];
	    }

	    $module = 'Contacts';
	    if(!empty($contactId)) {
    		$GLOBALS['log']->security("Contact exists {$contactId}");

    		$contact = BeanFactory::getBean($module, $contactId);
    		$contact->sasa_lgbtiq_c = $complaint->lgbtiq;
			$contact->sasa_genero_c = $complaint->sexo;
			$contact->sasa_condicionespecial_c = $complaint->condicion_especial;
			$contactId = $contact->id;
			$contact->save();
    	} else {
			$newContact = BeanFactory::newBean($module);
			$newContact->primary_address_country = syncDataConf('countries', $complaint->codigo_pais);
			$newContact->primary_address_state = syncDataConf('states', $complaint->departamento_cod);
			$newContact->primary_address_city = syncDataConf('cities', $complaint->municipio_cod);
			$newContact->sasa_tipoidentificacion_c = syncDataConf('documentType', $complaint->tipo_id_CF); 
			$newContact->sasa_nroidentificacion_c = $complaint->numero_id_CF;
			$newContact->sasa_telefono_c = $complaint->telefono;
			$newContact->email1 = $complaint->correo;	

			$newContact->sasa_lgbtiq_c = $complaint->lgbtiq;
			$newContact->sasa_genero_c = syncDataConf('gender', $complaint->lgbtiq);;
			$newContact->sasa_condicionespecial_c = $complaint->condicion_especial;

			$newContact->first_name = $complaint->nombres;

			$newContact->description = 'Contacto no registra en el CRM.';

			$newContact->save();

			$contactId = $newContact->id;
			$GLOBALS['log']->security("Not exists contact {$newContact->id}");
    	}
    }
    
    return $contactId;
}

?>
