<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_meetings_compromiso';
function tarea_meetings_compromiso(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_meetings_compromiso. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n");
  $return = true;
  try{
    global $mod_strings;
         global $current_user;
         global $sugar_config;
         global $locale;
    global $db;
    global $timedate;
    global $datetime;

    // $fecha = new DateTime(date("Y-m-d H:i:s"));
    // $fecha->modify('+5 hours');
    // $actual = $fecha->format('Y-m-d H:i:s');
    // $fecha->modify('+40 minute');
    // $menos30 = $fecha->format('Y-m-d H:i:s');


    // $timefecha = $timedate->getInstance()->nowDb();

    $fechasugar = new SugarDateTime();
    $actual = $fechasugar->formatDateTime("datetime", "db");
    
    $fechasugar->modify("+40 minute");
    $menos30 = $fechasugar->format("Y-m-d H:i:s");

    $GLOBALS['log']->security("*************FECHA OBJETO GLOBAL: {$actual} Y fecha DateTime {$menos30} ***********************\n");

    //$query = $db->query("SELECT meetings.assigned_user_id FROM meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c=meetings.id WHERE (meetings_cstm.sasa_fechaproximcompromiso_c BETWEEN '{$menos30}' AND '{$actual}') AND meetings.deleted=0 GROUP BY meetings.assigned_user_id;");
    //SELECT email_addresses.email_address FROM email_addr_bean_rel inner JOIN email_addresses ON email_addresses.id=email_addr_bean_rel.email_address_id WHERE email_addr_bean_rel.bean_id="a67e6c88-bedc-11e9-bc7b-000000000000";

    $query = $db->query("SELECT GROUP_CONCAT(users.first_name, ' ', users.last_name) AS nomuser, meetings_cstm.sasa_fechaproximcompromiso_c AS compromiso, meetings.parent_type AS parenttipo, meetings.parent_id AS parent, email_addresses.email_address, meetings.id AS emailid, meetings.assigned_user_id AS idassigned FROM meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c=meetings.id INNER JOIN users ON users.id=meetings.assigned_user_id INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=meetings.assigned_user_id INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id=email_addresses.id WHERE (meetings_cstm.sasa_fechaproximcompromiso_c BETWEEN '{$actual}' AND '{$menos30}') AND email_addr_bean_rel.deleted=0 AND meetings.deleted=0 AND meetings_cstm.sasa_envio_compro_c IS NULL GROUP BY meetings.assigned_user_id");
    $querycalls = $db->query("SELECT GROUP_CONCAT(users.first_name, ' ', users.last_name) AS nomuser, calls_cstm.sasa_fechaproximallamada_c AS compromiso, calls.parent_type AS parenttipo, calls.parent_id AS parent, email_addresses.email_address, calls.id AS emailid, calls.assigned_user_id AS idassigned FROM calls_cstm INNER JOIN calls ON calls_cstm.id_c=calls.id INNER JOIN users ON users.id=calls.assigned_user_id INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id=calls.assigned_user_id INNER JOIN email_addresses ON email_addr_bean_rel.email_address_id=email_addresses.id WHERE (calls_cstm.sasa_fechaproximallamada_c BETWEEN '{$actual}' AND '{$menos30}') AND email_addr_bean_rel.deleted=0 AND calls.deleted=0 AND calls_cstm.sasa_envio_compro_c IS NULL GROUP BY calls.assigned_user_id");

    //$queryemail = $db->query("SELECT email_addresses.email_address FROM email_addr_bean_rel inner JOIN email_addresses ON email_addresses.id=email_addr_bean_rel.email_address_id WHERE email_addr_bean_rel.bean_id='{$idsasiigned[]}'");

    //$email = mysqli_fetch_array($queryemail);
    //GROUP_CONCAT("'",meetings.assigned_user_id,"'")


    try {
        $usersemil=array();
      
        require_once 'modules/Emails/Email.php';
        require_once 'include/SugarPHPMailer.php';
        if (mysqli_num_rows($query)>=1) {
          $email_template = new EmailTemplate();
          $email_template->retrieve("f336fa98-f5a2-11e9-84b8-023ef1e03e82");

          while ($row = $db->fetchByAssoc($query)) {
            if ($row['parenttipo']=='Accounts') {
              $parent = BeanFactory::retrieveBean('Accounts',$row['parent'],array('disable_row_level_security' => true));
            }else{
              $parent = BeanFactory::retrieveBean('Leads',$row['parent'],array('disable_row_level_security' => true));
            }

            $fechasignado = new SugarDateTime($row['compromiso']);
            $fechasignado->modify('-5 hours');
            $asignadofecha = $fechasignado->format("H:i:s");

            // $fechacompromiso = new DateTime($row['compromiso']);
            // $fechacompromiso->modify('-5 hours');
            // $fecharealcom = $fechacompromiso->format('H:i:s');
            
            $url="https://alianza.sugarondemand.com/#Meetings/".$row['emailid'];
            $bodyhtml = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);
            $body = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);
            $email_obj = new Email();
            $defaults = $email_obj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults["email"];
            $mail->FromName = $defaults["name"];
            $mail->Subject = $email_template->subject;
            $mail->Body = from_html($bodyhtml);
            $mail->AltBody =from_html($body);
            $mail->prepForOutbound();
            $param = array();
            $param['asingenemail']=$row['email_address'];
            $mail->AddAddress($row['email_address']);
            $GLOBALS['log']->security("****REUNION {$row['email_address']} **********************\n");
            $actualizacompromisome=$db->query("UPDATE meetings_cstm SET meetings_cstm.sasa_envio_compro_c='si' WHERE meetings_cstm.id_c='{$row['emailid']}'");
             $GLOBALS['log']->security("****REUNION {$actualizacompromisome} **********************\n");
             if (@$mail->Send()) {
              }
          }
        }
        if (mysqli_num_rows($querycalls)>=1) {
          while ($row = $db->fetchByAssoc($querycalls)) {
            if ($row['parenttipo']=='Accounts') {
              $parent2 = BeanFactory::retrieveBean('Accounts',$row['parent'],array('disable_row_level_security' => true));
            }else{
              $parent2 = BeanFactory::retrieveBean('Leads',$row['parent'],array('disable_row_level_security' => true));
            }

            $fechasignado = new SugarDateTime($row['compromiso']);
            $fechasignado->modify('-5 hours');
            $asignadofecha = $fechasignado->format("H:i:s");
            
            $email_template = new EmailTemplate();
            $email_template->retrieve("babf2962-f5a5-11e9-9cf4-06f2b4fb7f46");

            $url="https://alianza.sugarondemand.com/#Calls/".$row['emailid'];
            $bodyhtml = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent2->name,$url),$email_template->body_html);
            $body = str_replace(array('$contact_user_user_name','$contact_sasa_fechadeactualizacionsc__c','$contact_name','$contact_title'),array($row['nomuser'],$asignadofecha,$parent->name,$url),$email_template->body_html);

            $email_obj = new Email();
            $defaults = $email_obj->getSystemDefaultEmail();
            $mail = new SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->IsHTML(true);
            $mail->From = $defaults["email"];
            $mail->FromName = $defaults["name"];
            $mail->Subject = $email_template->subject;
            $mail->Body = from_html($bodyhtml);
            $mail->AltBody =from_html($body);
            $mail->prepForOutbound();
          
            $param = array();
            $param['asingenemail']=$row['email_address'];
            $mail->AddAddress($row['email_address']);
            $GLOBALS['log']->security("****LLAMADAS {$row['email_address']} **********************\n");
            $actualizacompromisoca=$db->query("UPDATE calls_cstm SET calls_cstm.sasa_envio_compro_c='si' WHERE calls_cstm.id_c='{$row['emailid']}'");
            if (@$mail->Send()) {
            }
          }
          
        }

    } catch (Exception $e) {
      
    }


    $GLOBALS['log']->security("****FECHA ACTUAL8 {$actual} Menos 30 {$menos30}**********************\n");
    $GLOBALS['log']->security("****QUERYRESULASSIGNED : {$idsasiigned[0]}**********************\n");
    $GLOBALS['log']->security("****QUERYEMAIL : {$email[0]}**********************\n");
    $GLOBALS['log']->security("****UserenvioEmail : {$usersemil['usersemail']}**********************\n");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("\n******************************************************");
  $GLOBALS['log']->security("Fin-tarea_meetings_compromiso. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
