<?php
 // created: 2019-03-08 18:57:52
$layout_defs["sasa_ProductosAFAV"]["subpanel_setup"]['sasa_productosafav_sasap_presupuestoxproducto_1'] = array (
  'order' => 100,
  'module' => 'sasaP_PresupuestoxProducto',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASAP_PRESUPUESTOXPRODUCTO_TITLE',
  'get_subpanel_data' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
