<?php
$module_name = 'sasa_pqrs_historico';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'preview' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
                'size' => 'large',
              ),
              1 => 'name',
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_case_number_c',
                'label' => 'LBL_SASA_CASE_NUMBER_C',
              ),
              1 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_fecha_de_vencimiento_c',
                'label' => 'LBL_SASA_FECHA_DE_VENCIMIENTO_C',
              ),
              2 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_nombres_cliente_c',
                'label' => 'LBL_SASA_NOMBRES_CLIENTE_C',
              ),
              3 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_apellidos_cliente_c',
                'label' => 'LBL_SASA_APELLIDOS_CLIENTE_C',
              ),
              4 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_numero_documento_client_c',
                'label' => 'LBL_SASA_NUMERO_DOCUMENTO_CLIENT',
              ),
              5 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_medio_de_recepcion_c',
                'label' => 'LBL_SASA_MEDIO_DE_RECEPCION_C',
              ),
              6 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_estado_de_caso_c',
                'label' => 'LBL_SASA_ESTADO_DE_CASO_C',
              ),
              7 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_estado_de_caso_codigo_c',
                'label' => 'LBL_SASA_ESTADO_DE_CASO_CODIGO_C',
              ),
              8 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipo_de_solicitud_c',
                'label' => 'LBL_SASA_TIPO_DE_SOLICITUD_C',
              ),
              9 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipo_de_solicitud_codig_c',
                'label' => 'LBL_SASA_TIPO_DE_SOLICITUD_CODIG',
              ),
              10 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_1_c',
                'label' => 'LBL_SASA_TIPIFICACION_1_C',
              ),
              11 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_1_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_1_CODIGO_C',
              ),
              12 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_2_c',
                'label' => 'LBL_SASA_TIPIFICACION_2_C',
              ),
              13 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_2_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_2_CODIGO_C',
              ),
              14 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_3_c',
                'label' => 'LBL_SASA_TIPIFICACION_3_C',
              ),
              15 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_tipificacion_3_codigo_c',
                'label' => 'LBL_SASA_TIPIFICACION_3_CODIGO_C',
              ),
              16 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              17 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_categoria_codigo_c',
                'label' => 'LBL_SASA_CATEGORIA_CODIGO_C',
              ),
              18 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_fecha_creacion_c',
                'label' => 'LBL_SASA_FECHA_CREACION_C',
              ),
              19 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_fecha_solucion_c',
                'label' => 'LBL_SASA_FECHA_SOLUCION_C',
              ),
              20 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_documentado_c',
                'label' => 'LBL_SASA_DOCUMENTADO_C',
                'span' => 12,
              ),
              21 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_descripcion_de_la_solic_c',
                'label' => 'LBL_SASA_DESCRIPCION_DE_LA_SOLIC',
              ),
              22 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_solucionador_c',
                'label' => 'LBL_SASA_SOLUCIONADOR_C',
              ),
              23 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_escalado_c',
                'label' => 'LBL_SASA_ESCALADO_C',
              ),
              24 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
              ),
              25 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_creado_por_nombre_compl_c',
                'label' => 'LBL_SASA_CREADO_POR_NOMBRE_COMPL',
              ),
              26 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_usuario_asistente_encar_c',
                'label' => 'LBL_SASA_USUARIO_ASISTENTE_ENCAR',
              ),
              27 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_solucionador_inmediato__c',
                'label' => 'LBL_SASA_SOLUCIONADOR_INMEDIATO_',
              ),
              28 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_vencido_c',
                'label' => 'LBL_SASA_VENCIDO_C',
              ),
              29 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_hora_c',
                'label' => 'LBL_SASA_HORA_C',
              ),
              30 => 
              array (
                'readonly' => '1',
                'name' => 'sasa_segmento_datawarehouse__c',
                'label' => 'LBL_SASA_SEGMENTO_DATAWAREHOUSE_',
              ),
              31 => 
              array (
                'name' => 'accounts_sasa_pqrs_historico_1_name',
              ),
              32 => 
              array (
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              3 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'maxColumns' => 1,
        ),
      ),
    ),
  ),
);
