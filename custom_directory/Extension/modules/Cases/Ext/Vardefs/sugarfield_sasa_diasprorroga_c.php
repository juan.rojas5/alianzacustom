<?php
 // created: 2023-03-31 19:46:06
$dictionary['Case']['fields']['sasa_diasprorroga_c']['labelValue']='Días prorroga';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_diasprorroga_c']['enforced']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['dependency']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_diasprorroga_c']['readonly_formula']='';

 ?>