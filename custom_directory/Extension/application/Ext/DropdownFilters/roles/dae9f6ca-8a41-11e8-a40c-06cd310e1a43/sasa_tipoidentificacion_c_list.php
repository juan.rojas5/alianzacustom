<?php
// created: 2018-08-21 04:59:52
$role_dropdown_filters['sasa_tipoidentificacion_c_list'] = array (
  '' => true,
  'Cedula de Ciudadania' => true,
  'Cedula de Extranjeria' => true,
  'Pasaporte' => true,
  'Nit' => true,
  'NUIP' => true,
  'Registro Civil' => true,
  'Tarjeta de identidad' => true,
  'Carne Diplomatico' => true,
  'Fideicomiso' => true,
  'Sociedad Extranjera' => true,
  'Otro' => true,
  'No Registra' => false,
);