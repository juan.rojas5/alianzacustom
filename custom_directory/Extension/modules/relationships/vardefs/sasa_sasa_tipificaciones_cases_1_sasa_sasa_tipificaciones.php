<?php
// created: 2022-04-29 13:46:19
$dictionary["sasa_sasa_tipificaciones"]["fields"]["sasa_sasa_tipificaciones_cases_1"] = array (
  'name' => 'sasa_sasa_tipificaciones_cases_1',
  'type' => 'link',
  'relationship' => 'sasa_sasa_tipificaciones_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_SASA_SASA_TIPIFICACIONES_CASES_1_FROM_SASA_SASA_TIPIFICACIONES_TITLE',
  'id_name' => 'sasa_sasa_tipificaciones_cases_1sasa_sasa_tipificaciones_ida',
  'link-type' => 'many',
  'side' => 'left',
);
