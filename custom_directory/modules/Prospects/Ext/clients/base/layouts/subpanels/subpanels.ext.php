<?php
// WARNING: The contents of this file are auto-generated.


 // created: 2012-06-13 21:08:52
$layout_defs["Prospects"]["subpanel_setup"]['fbsg_ccintegrationlog_prospects'] = array (
  'order' => 100,
  'module' => 'fbsg_CCIntegrationLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'CC Integration Log',
  'get_subpanel_data' => 'fbsg_ccintegrationlog_prospects',
  'top_buttons' => 
  array (
  ),
);


$viewdefs["Prospects"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_CCILOG_SUBPANEL',
  'context' => array(
    'link' => 'fbsg_ccintegrationlog_prospects',
  ),
);


$viewdefs["Prospects"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
  'context' => array(
    'link' => 'prospect_list_prospects',
  ),
);


//auto-generated file DO NOT EDIT
$viewdefs['Prospects']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'fbsg_ccintegrationlog_prospects',
  'view' => 'subpanel-for-prospects-fbsg_ccintegrationlog_prospects',
);


//auto-generated file DO NOT EDIT
$viewdefs['Prospects']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prospect_list_prospects',
  'view' => 'subpanel-for-prospects-prospect_list_prospects',
);
