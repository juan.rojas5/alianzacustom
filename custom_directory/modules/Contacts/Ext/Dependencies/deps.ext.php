<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/readonly_primary_address_city.php


$dependencies['Contacts']['readonly_primary_address_city'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_city'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'equal($sasa_parentescorelacion_c,"Otro")'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'true'
      )
    ),
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);


?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/readonly_primary_address_country.php


$dependencies['Contacts']['readonly_primary_address_country'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_country'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'equal($sasa_parentescorelacion_c,"Otro")'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'true'
      )
    ),
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/readonly_primary_address_state.php


$dependencies['Contacts']['readonly_primary_address_state'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_state'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'primary_address_state',
        'value' => 'equal($sasa_parentescorelacion_c,"Otro")'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_state',
        'value' => 'true'
      )
    ),
    
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);



?>
