<?php
 // created: 2022-05-17 13:35:47
$dictionary['Case']['fields']['sasa_codigoentidad_c']['labelValue']='Código de Entidad';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['readonly_formula']='';
$dictionary['Case']['fields']['sasa_codigoentidad_c']['visibility_grid']=array (
  'trigger' => 'sasa_tipoentidad_c',
  'values' => 
  array (
    5 => 
    array (
      0 => '16',
    ),
    85 => 
    array (
      0 => '51',
    ),
    '' => 
    array (
    ),
  ),
);

 ?>