<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAV/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2018-11-21 22:08:07
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_movimientosav_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_MovimientosAV']['fields']['sasa_categoria_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAV/Ext/Vardefs/sugarfield_name.php

 // created: 2018-11-21 22:04:59
$dictionary['sasa_MovimientosAV']['fields']['name']['len']='255';
$dictionary['sasa_MovimientosAV']['fields']['name']['audited']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['massupdate']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['importable']='false';
$dictionary['sasa_MovimientosAV']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_MovimientosAV']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_MovimientosAV']['fields']['name']['unified_search']=false;
$dictionary['sasa_MovimientosAV']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_MovimientosAV']['fields']['name']['calculated']='true';
$dictionary['sasa_MovimientosAV']['fields']['name']['formula']='$sasa_nemotecnico_c';
$dictionary['sasa_MovimientosAV']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAV/Ext/Vardefs/sasa_productosafav_sasa_movimientosav_1_sasa_MovimientosAV.php

// created: 2019-01-04 21:28:46
$dictionary["sasa_MovimientosAV"]["fields"]["sasa_productosafav_sasa_movimientosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAV"]["fields"]["sasa_productosafav_sasa_movimientosav_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosav_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAV"]["fields"]["sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE_ID',
  'id_name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_movimientosav_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAV/Ext/Vardefs/accounts_sasa_movimientosav_1_sasa_MovimientosAV.php

// created: 2019-01-04 21:40:08
$dictionary["sasa_MovimientosAV"]["fields"]["accounts_sasa_movimientosav_1"] = array (
  'name' => 'accounts_sasa_movimientosav_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosav_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE',
  'id_name' => 'accounts_sasa_movimientosav_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_MovimientosAV"]["fields"]["accounts_sasa_movimientosav_1_name"] = array (
  'name' => 'accounts_sasa_movimientosav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_movimientosav_1accounts_ida',
  'link' => 'accounts_sasa_movimientosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_MovimientosAV"]["fields"]["accounts_sasa_movimientosav_1accounts_ida"] = array (
  'name' => 'accounts_sasa_movimientosav_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_SASA_MOVIMIENTOSAV_TITLE_ID',
  'id_name' => 'accounts_sasa_movimientosav_1accounts_ida',
  'link' => 'accounts_sasa_movimientosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_MovimientosAV/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_MovimientosAV']['full_text_search']=true;

?>
