<?php
$module_name = 'sasa_SaldosAV';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'detail' => 
      array (
        'templateMeta' => 
        array (
          'form' => 
          array (
            'buttons' => 
            array (
              0 => 'EDIT',
              1 => 'DUPLICATE',
              2 => 'DELETE',
            ),
          ),
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'accounts_sasa_saldosav_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_ACCOUNTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_productosafav_sasa_saldosav_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_nombreproducto_c',
                'label' => 'LBL_SASA_NOMBREPRODUCTO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_nemotecnico_c',
                'label' => 'LBL_SASA_NEMOTECNICO_C',
              ),
              6 => 
              array (
                'name' => 'sasa_emisor_c',
                'label' => 'LBL_SASA_EMISOR_C',
              ),
              7 => 
              array (
                'name' => 'sasa_isinespecie_c',
                'label' => 'LBL_SASA_ISINESPECIE_C',
              ),
              8 => 
              array (
                'name' => 'sasa_numeroacciones_c',
                'label' => 'LBL_SASA_NUMEROACCIONES_C',
              ),
              9 => 
              array (
                'name' => 'sasa_tipotitulo_c',
                'label' => 'LBL_SASA_TIPOTITULO_C',
              ),
              10 => 
              array (
                'name' => 'sasa_fechaemision_c',
                'label' => 'LBL_SASA_FECHAEMISION_C',
              ),
              11 => 
              array (
                'name' => 'sasa_fechavencimiento_c',
                'label' => 'LBL_SASA_FECHAVENCIMIENTO_C',
              ),
              12 => 
              array (
                'name' => 'sasa_tasafacial_c',
                'label' => 'LBL_SASA_TASAFACIAL_C',
              ),
              13 => 
              array (
                'name' => 'sasa_tipooperacion_c',
                'label' => 'LBL_SASA_TIPOOPERACION_C',
              ),
              14 => 
              array (
                'name' => 'sasa_nominal_c',
                'label' => 'LBL_SASA_NOMINAL_C',
              ),
              15 => 
              array (
                'name' => 'sasa_valorcompra_c',
                'label' => 'LBL_SASA_VALORCOMPRA_C',
              ),
              16 => 
              array (
                'name' => 'sasa_tirprecio_c',
                'label' => 'LBL_SASA_TIRPRECIO_C',
              ),
              17 => 
              array (
                'name' => 'sasa_valormercado_c',
                'label' => 'LBL_SASA_VALORMERCADO_C',
              ),
              18 => 
              array (
                'name' => 'sasa_tirvpn_c',
                'label' => 'LBL_SASA_TIRVPN_C',
              ),
              19 => 
              array (
                'name' => 'sasa_preciovpn_c',
                'label' => 'LBL_SASA_PRECIOVPN_C',
              ),
              20 => 
              array (
                'name' => 'sasa_disponiblenominal_c',
                'label' => 'LBL_SASA_DISPONIBLENOMINAL_C',
              ),
              21 => 
              array (
                'name' => 'sasa_disponiblevlrmercado_c',
                'label' => 'LBL_SASA_DISPONIBLEVLRMERCADO_C',
              ),
              22 => 
              array (
                'name' => 'sasa_garantianominal_c',
                'label' => 'LBL_SASA_GARANTIANOMINAL_C',
              ),
              23 => 
              array (
                'name' => 'sasa_garantiavlrmercado_c',
                'label' => 'LBL_SASA_GARANTIAVLRMERCADO_C',
              ),
              24 => 
              array (
                'name' => 'sasa_valorinicial_c',
                'label' => 'LBL_SASA_VALORINICIAL_C',
              ),
              25 => 
              array (
                'name' => 'sasa_valorfinal_c',
                'label' => 'LBL_SASA_VALORFINAL_C',
              ),
              26 => 
              array (
                'name' => 'sasa_fecharegistro_c',
                'label' => 'LBL_SASA_FECHAREGISTRO_C',
              ),
              27 => 
              array (
                'name' => 'sasa_fechacumplimiento_c',
                'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
              ),
              28 => 
              array (
                'name' => 'sasa_saldototal_c',
                'label' => 'LBL_SASA_SALDOTOTAL_C',
              ),
              29 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'label' => 'LBL_DESCRIPTION',
              ),
              30 => 
              array (
                'name' => 'tag',
                'studio' => 
                array (
                  'portal' => false,
                  'base' => 
                  array (
                    'popuplist' => false,
                  ),
                  'mobile' => 
                  array (
                    'wirelesseditview' => true,
                    'wirelessdetailview' => true,
                  ),
                ),
                'label' => 'LBL_TAGS',
              ),
              31 => 'assigned_user_name',
              32 => 'team_name',
              33 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              34 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              35 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              36 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
