<?php
 // created: 2022-10-19 15:24:17
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['audited']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['massupdate']=true;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['hidemassupdate']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['merge_filter']='disabled';
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['reportable']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['calculated']=false;
$dictionary['Case']['fields']['sasa_sasa_tipificaciones_cases_1_name']['vname']='LBL_SASA_SASA_TIPIFICACIONES_CASES_1_NAME_FIELD_TITLE';

 ?>