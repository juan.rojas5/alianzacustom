<?php
// created: 2022-11-21 20:51:44
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_pqrs_historico_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1_name"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link' => 'accounts_sasa_pqrs_historico_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_pqrs_historico"]["fields"]["accounts_sasa_pqrs_historico_1accounts_ida"] = array (
  'name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE_ID',
  'id_name' => 'accounts_sasa_pqrs_historico_1accounts_ida',
  'link' => 'accounts_sasa_pqrs_historico_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
