({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	casos: [],
	Username:"**PAI**",
	Password:"**PAI**",
	url:'**PAI**',
	ws:'**PAI**',
	initialize: function(options) {
		var self = this;
		dashlet2 = self;		
		if (typeof app === 'undefined') app = App;		
			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet2_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet2_No_Identificacion").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet2_No_Identificacion").css("border", "none"); 
						$("#dashlet2_No_Identificacion").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet2_No_Identificacion").attr('readonly', true);
					}
					
					$("#dashlet2_salida_no_de_caso").change( function(){
						self.dashlet2_salida_no_de_caso_change();	
					});
					
					$("#dashlet2_consultar").click( function(){
						self.dashlet2_bizagi();	
					});
					
					$("#dashlet2_consultar").show();
					self.dashlet2_fechas();//poblando fechas
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet2_bizagi: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		app.alert.show('dashlet2_bizagi_procesando', {
			level: 'process',
			title: 'Procesando...' ,
			autoClose: false,
		});
		
		if (!_.isEmpty( $("#dashlet2_No_Identificacion").val() )){
			self.dashlet2_nombre_cuenta();//poblando nombre cuenta	
			self.dashlet2_consultar();//consultando caso
		}
		else{
			app.alert.dismiss('dashlet2_bizagi_procesando');
			app.alert.show('error_procesando_id_cliente', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}		
	},	
	dashlet2_nombre_cuenta: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
	
		/*
		Obteniendo cuenta
		*/
		$("#dashlet2_nombre").html('');
		app.api.call(
			'GET', 
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet2_No_Identificacion").val()), 
				{
					"sasa_nroidentificacion_c": $("#dashlet2_No_Identificacion").val(),
				}, 
				{
				success: function (data) {
					if(!_.isEmpty(data.records)){
						$("#dashlet2_nombre").html(data.records[0].name);
					}
					else{
						$("#dashlet2_nombre").html('--<b>No Existe en sugar</b>--');
					}
				},
				error: function (e) {		
					app.alert.dismiss('dashlet1_bizagi_procesando');				
					app.alert.show('error_procesando_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
						autoClose: false,
					});
				}
			}, {async: false}
		);
	},
	dashlet2_salida_no_de_caso_change: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		if ( _.isEmpty( $("#dashlet2_salida_no_de_caso").html() ) ){ 
			$("#dashlet2_salida_no_de_caso").append('<option value="">--</option>');
		}
		
		$("#dashlet2_salida_Descripción").val("")//limpiando
		$("#dashlet2_salida_Fecha_Apertura").val("")//limpiando
		$("#dashlet2_salida_Estado").val("")//limpiando
		$("#dashlet2_salida_Asunto").val("")//limpiando
		$("#dashlet2_salida_Tipo_Solicitud").val("")//limpiando
		$("#dashlet2_salida_Asignado_A").val("");//limpiando
		$("#dashlet2_salida_Respuesta").val("")//limpiando
		console.log("dashlet2_salida_no_de_caso_change",self.casos);
		if (!_.isEmpty( self.casos )){
			if (!_.isEmpty( $("#dashlet2_salida_no_de_caso").val() )){	
				var i = $("#dashlet2_salida_no_de_caso").val();	
				$("#dashlet2_salida_Tipo_Solicitud").val(self.casos[i].tipoSolicitud);
				$("#dashlet2_salida_Descripción").val(self.casos[i].descripcion);
				$("#dashlet2_salida_Estado").val(self.casos[i].estado);
				$("#dashlet2_salida_Asunto").val(self.casos[i].categoria);
				$("#dashlet2_salida_Fecha_Apertura").val(self.casos[i].fechaApertura);
				$("#dashlet2_salida_Respuesta").val(self.casos[i].campoRespuesta);
				
				$("#dashlet2_salida_Asignado_A").val("--??????????--");
				//canalRecepcion
				//categoria
				//compania
			}
		}
	},
	dashlet2_fechas: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		$("#dashlet2_Fecha_Apertura_Inicio").empty();//limpiando
		$("#dashlet2_Fecha_Apertura_Fin").empty();//limpiando
		
		
		a = new Date; 
		fecha = a.getFullYear()+"-"+(a.getMonth()+1)+"-"+a.getDate();  
		$("#dashlet2_Fecha_Apertura_Inicio").val(fecha);
		$("#dashlet2_Fecha_Apertura_Fin").val(fecha);
		
		$( function() {
			$( "#dashlet2_Fecha_Apertura_Inicio" ).datepicker({
				format: 'yyyy-m-dd'
			});
		} );
		$( function() {
			$( "#dashlet2_Fecha_Apertura_Fin" ).datepicker({
				format: 'yyyy-m-dd'
			});
		} );
	},
	dashlet2_consultar: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.casos = [];
		$("#dashlet2_salida_no_de_caso").empty();//limpiando
		if (!_.isEmpty( $("#dashlet2_No_Identificacion").val() )){	
			self.dashlet_bizagi_call();
			if (!_.isEmpty( self.respuesta )){ 		
				console.log('dashlet2_consultar ',self.respuesta);	
				if (typeof self.respuesta.consultaSolicitudResponseWS !== 'undefined'){				
					if(!$.isArray( self.respuesta.consultaSolicitudResponseWS )){			
						var temp = self.respuesta.consultaSolicitudResponseWS;
						self.respuesta.consultaSolicitudResponseWS = [];
						self.respuesta.consultaSolicitudResponseWS.push(temp);
					}
					self.casos = self.respuesta.consultaSolicitudResponseWS;
					if (Array.isArray(self.respuesta.consultaSolicitudResponseWS)){
						app.alert.dismiss('dashlet2_bizagi_procesando');
						app.alert.dismiss('info_crear');
						app.alert.show('info_crear', {
							level: 'info',
							title: 'Consultado',
							messages: self.respuesta.descripcion,
							autoClose: true,
						});
						$.each(self.respuesta.consultaSolicitudResponseWS, function( i, caso ) {							
							$("#dashlet2_salida_no_de_caso").append("<option value='"+i+"'>"+caso.numeroDeCaso+"</option>");
						});
						self.dashlet2_salida_no_de_caso_change();
					}
					else{				
						app.alert.dismiss('dashlet2_bizagi_procesando');
						app.alert.dismiss('error_crear');
						app.alert.show('error_crear', {
							level: 'error',
							title: 'Error al crear por WS2:<p>'+JSON.stringify(self.respuesta,null,'\t'),
							autoClose: false,
						});
					}
				}
				else{				
					app.alert.dismiss('dashlet2_bizagi_procesando');
					app.alert.dismiss('error_crear');
					app.alert.show('error_crear', {
						level: 'error',
						title: 'Error al crear por WS1:<p>'+self.respuesta.descripcion,
						autoClose: false,
					});
				}
			}
			else{				
				app.alert.dismiss('dashlet2_bizagi_procesando');
				app.alert.dismiss('error_crear');
				app.alert.show('error_crear', {
					level: 'error',
					title: 'Error al crear por WS0:<p>'+JSON.stringify(self.respuesta,null,'\t'),
					autoClose: false,
				});
			}
		}
		else{
			app.alert.dismiss('dashlet2_bizagi_procesando');
			app.alert.dismiss('error_crear');
			app.alert.show('error_crear', {
				level: 'error',
				title: 'No. Identificación vacio!',
				autoClose: false,
			});
		}
		self.dashlet2_salida_no_de_caso_change();		
	},
	dashlet_bizagi_call: function(){
		var self = this;
		if (typeof app === 'undefined') app = App;
		
		var numeroDocumento = $("#dashlet2_No_Identificacion").val();	
		var method  = "dashlet_bizagi";		
		var data = [
			{
				'url':self.url,
				'ws':self.ws,
				'tipo':'POST',
				'Username':self.Username,
				'Password':self.Password,
			},
			{		
				'canalRecepcion':'',//108 CRM
				'estado':$("#dashlet2_Estado").val(),
				'fechaAperturaFin':$("#dashlet2_Fecha_Apertura_Fin").val(),
				'fechaAperturaInicio':$("#dashlet2_Fecha_Apertura_Inicio").val(),
				'identificacionCliente':$("#dashlet2_No_Identificacion").val(),
				'numeroCaso':$("#dashlet2_No_Caso").val(),
				
				'xmlns:soapenv':'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"',
				'xmlns:web':'xmlns:web="http://web.ws.sqdm.com/"',
				'web':'consultaSolicitud',
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) {  
				self.respuesta=response;
				app.alert.dismiss('dashlet_bizagi_procesando');
				
				if (typeof response !== 'undefined')
				if (typeof response.descripcion !== 'undefined')
				if (response.descripcion == 'Error de autenticación. Por favor valide sus credenciales')
				app.alert.show('error_dashlet2_autenticacion', {
					level: 'error',
					title: 'Bizagi2: '+response.descripcion,
					autoClose: false,
				});
			}, this),  
			error: _.bind(function(error) {  
				self.respuesta==null;
				console.log("Error "+method+": ", error);
				app.alert.dismiss('dashlet_bizagi_procesando');
				app.alert.dismiss('error_procesando_pedido');
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
})
