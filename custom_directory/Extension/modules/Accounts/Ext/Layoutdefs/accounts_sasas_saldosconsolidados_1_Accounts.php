<?php
 // created: 2019-01-04 22:23:30
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasas_saldosconsolidados_1'] = array (
  'order' => 100,
  'module' => 'sasaS_SaldosConsolidados',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASAS_SALDOSCONSOLIDADOS_1_FROM_SASAS_SALDOSCONSOLIDADOS_TITLE',
  'get_subpanel_data' => 'accounts_sasas_saldosconsolidados_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
