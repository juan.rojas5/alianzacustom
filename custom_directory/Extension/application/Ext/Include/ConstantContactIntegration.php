<?php 
 //WARNING: The contents of this file are auto-generated
$beanList['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$beanFiles['fbsg_ConstantContactIntegration'] = 'modules/fbsg_ConstantContactIntegration/fbsg_ConstantContactIntegration.php';
$modules_exempt_from_availability_check['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$report_include_modules['fbsg_ConstantContactIntegration'] = 'fbsg_ConstantContactIntegration';
$modInvisList[] = 'fbsg_ConstantContactIntegration';
$beanList['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$beanFiles['fbsg_CCIErrors'] = 'modules/fbsg_CCIErrors/fbsg_CCIErrors.php';
$modules_exempt_from_availability_check['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$report_include_modules['fbsg_CCIErrors'] = 'fbsg_CCIErrors';
$modInvisList[] = 'fbsg_CCIErrors';
$beanList['fbsg_CCIntegrationLog'] = 'fbsg_CCIntegrationLog';
$beanFiles['fbsg_CCIntegrationLog'] = 'modules/fbsg_CCIntegrationLog/fbsg_CCIntegrationLog.php';
$moduleList[] = 'fbsg_CCIntegrationLog';

?>