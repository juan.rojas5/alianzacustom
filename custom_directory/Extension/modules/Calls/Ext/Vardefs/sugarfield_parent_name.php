<?php
 // created: 2018-09-11 21:52:57
$dictionary['Call']['fields']['parent_name']['len']=36;
$dictionary['Call']['fields']['parent_name']['required']=true;
$dictionary['Call']['fields']['parent_name']['audited']=false;
$dictionary['Call']['fields']['parent_name']['massupdate']=false;
$dictionary['Call']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Call']['fields']['parent_name']['calculated']=false;

 ?>