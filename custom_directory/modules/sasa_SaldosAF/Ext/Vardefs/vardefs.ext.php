<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_nombrefondo_c.php

 // created: 2018-11-21 20:58:25
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['importable']='false';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['calculated']='true';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['formula']='related($sasa_productosafav_sasa_saldosaf_1,"sasa_producto_c")';
$dictionary['sasa_SaldosAF']['fields']['sasa_nombrefondo_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2018-11-21 20:57:48
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_saldosaf_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_SaldosAF']['fields']['sasa_categoria_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_name.php

 // created: 2018-11-21 20:37:11
$dictionary['sasa_SaldosAF']['fields']['name']['unified_search']=false;
$dictionary['sasa_SaldosAF']['fields']['name']['calculated']='true';
$dictionary['sasa_SaldosAF']['fields']['name']['formula']='$sasa_encargo_c';
$dictionary['sasa_SaldosAF']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sasa_productosafav_sasa_saldosaf_1_sasa_SaldosAF.php

// created: 2019-01-04 21:25:02
$dictionary["sasa_SaldosAF"]["fields"]["sasa_productosafav_sasa_saldosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_SaldosAF"]["fields"]["sasa_productosafav_sasa_saldosaf_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_saldosaf_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_saldosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_SaldosAF"]["fields"]["sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE_ID',
  'id_name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_saldosaf_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/accounts_sasa_saldosaf_1_sasa_SaldosAF.php

// created: 2019-01-04 21:36:33
$dictionary["sasa_SaldosAF"]["fields"]["accounts_sasa_saldosaf_1"] = array (
  'name' => 'accounts_sasa_saldosaf_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_saldosaf_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE',
  'id_name' => 'accounts_sasa_saldosaf_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_SaldosAF"]["fields"]["accounts_sasa_saldosaf_1_name"] = array (
  'name' => 'accounts_sasa_saldosaf_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_saldosaf_1accounts_ida',
  'link' => 'accounts_sasa_saldosaf_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_SaldosAF"]["fields"]["accounts_sasa_saldosaf_1accounts_ida"] = array (
  'name' => 'accounts_sasa_saldosaf_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_SASA_SALDOSAF_TITLE_ID',
  'id_name' => 'accounts_sasa_saldosaf_1accounts_ida',
  'link' => 'accounts_sasa_saldosaf_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_valorunid_c.php

 // created: 2018-12-20 20:30:44
$dictionary['sasa_SaldosAF']['fields']['sasa_valorunid_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_valorunid_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_retcont_c.php

 // created: 2019-01-08 22:08:58
$dictionary['sasa_SaldosAF']['fields']['sasa_retcont_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_retcont_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_4x1000_c.php

 // created: 2019-01-08 22:10:05
$dictionary['sasa_SaldosAF']['fields']['sasa_4x1000_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_4x1000_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_canje_c.php

 // created: 2019-01-08 22:08:33
$dictionary['sasa_SaldosAF']['fields']['sasa_canje_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_maximoretiro_c.php

 // created: 2019-01-08 22:10:28
$dictionary['sasa_SaldosAF']['fields']['sasa_maximoretiro_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_maximoretiro_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_saldoactual_c.php

 // created: 2019-01-08 22:01:43
$dictionary['sasa_SaldosAF']['fields']['sasa_saldoactual_c']['precision']=2;
$dictionary['sasa_SaldosAF']['fields']['sasa_saldoactual_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_inversiones_c.php

 // created: 2019-01-08 22:10:56
$dictionary['sasa_SaldosAF']['fields']['sasa_inversiones_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_inversiones_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_ingresosdia_c.php

 // created: 2019-01-08 22:06:51
$dictionary['sasa_SaldosAF']['fields']['sasa_ingresosdia_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_egresosdia_c.php

 // created: 2019-01-08 22:08:01
$dictionary['sasa_SaldosAF']['fields']['sasa_egresosdia_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/sugarfield_sasa_retrend_c.php

 // created: 2019-01-08 22:09:17
$dictionary['sasa_SaldosAF']['fields']['sasa_retrend_c']['len']='26';
$dictionary['sasa_SaldosAF']['fields']['sasa_retrend_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAF/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_SaldosAF']['full_text_search']=true;

?>
