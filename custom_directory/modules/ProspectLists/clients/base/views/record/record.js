({
    extendsFrom: 'ProspectListsRecordView',

    initialize: function(options) {

	this._super('initialize', [options]);

        this.context.on('button:send_to_cc:click', this.confirmCcSend, this);

    },

    delegateButtonEvents: function() {
        this.context.on('button:send_to_cc:click', this.confirmCcSend, this);
        this._super('delegateButtonEvents');
    },

    confirmCcSend: function(ev) {
        app.alert.show('sendToCc', {
            level: 'confirmation',
            messages: ['Are you sure you want to send this list to CC?'],
            onConfirm: _.bind(this.sendToCc, this)
        });
    },

    sendToCc: function() {
        var record = this.model.get('id');
        var record_type = this.model.module || this.model.get('module') || this.model.get('_module');

        var url = app.api.buildURL(record_type + '/sendToCc/' + record);
        var xhr = app.api.call('create', url, {}).xhr;
        var that = this;

        return xhr.fail(function() {
            app.alert.show('sendToCcError', {
                level: 'error',
                messages: ["There was an error communicating with Sugar. Please try again in a few minutes, or contact FBSG Support if the problem continues."]
            });
        }).done(function(response) {
            if(response.error) {
                app.alert.show('sendToCcComplete', {
                    level: 'error',
                    messages: [response.message],
                    title: 'Error while sending',
                    autoClose: true
                });
            } else {
                app.alert.show('sendToCcComplete', {
                    level: 'info',
                    messages: [response.message],
                    title: 'Complete',
                    autoClose: true
                });
            }
        });
    }
})
