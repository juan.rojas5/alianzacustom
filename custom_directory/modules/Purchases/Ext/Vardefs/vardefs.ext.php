<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Purchase']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_tipotitular_c.php

 // created: 2023-02-01 15:52:21
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['labelValue']='tipo titular';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_encargo_c.php

 // created: 2023-02-01 15:52:59
$dictionary['Purchase']['fields']['sasa_encargo_c']['labelValue']='Número Encargo';
$dictionary['Purchase']['fields']['sasa_encargo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_encargo_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_encargo_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_encargo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_codigonegocio_c.php

 // created: 2023-02-01 15:55:55
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['labelValue']='Código negocio';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codigonegocio_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_estado_c.php

 // created: 2023-02-01 19:05:31
$dictionary['Purchase']['fields']['sasa_estado_c']['labelValue']='Estado producto';
$dictionary['Purchase']['fields']['sasa_estado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_estado_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_estado_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_estado_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_ejecutivocomercial_c.php

 // created: 2023-02-01 19:06:19
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['labelValue']='ejecutivo comercial';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_ejecutivocomercial_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_codregional_c.php

 // created: 2023-02-01 19:09:49
$dictionary['Purchase']['fields']['sasa_codregional_c']['labelValue']='codigo regional';
$dictionary['Purchase']['fields']['sasa_codregional_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codregional_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codregional_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codregional_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_codsucursal_c.php

 // created: 2023-02-01 19:11:35
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['labelValue']='codigo sucursal';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_directordegestion_c.php

 // created: 2023-02-01 19:13:10
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['labelValue']='director de gestión';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_directordegestion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_asistentedegestion_c.php

 // created: 2023-02-01 19:15:18
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['labelValue']='asistente de gestión';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_nombredenegocio_c.php

 // created: 2023-02-01 19:16:06
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['labelValue']='Nombre de negocio';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombredenegocio_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_dvencargo_c.php

 // created: 2023-02-01 19:20:31
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['labelValue']='DV encargo';
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_dvencargo_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sasa_productosafav_purchases_1_Purchases.php

// created: 2023-02-06 20:21:32
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1"] = array (
  'name' => 'sasa_productosafav_purchases_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_purchases_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1_name"] = array (
  'name' => 'sasa_productosafav_purchases_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_purchases_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["Purchase"]["fields"]["sasa_productosafav_purchases_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_PURCHASES_TITLE_ID',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_purchases_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_fechacreacion_c.php

 // created: 2023-02-13 20:26:18
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['labelValue']='Fecha de creación Producto';
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_fechacreacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_codigocomercial_c.php

 // created: 2023-02-21 13:29:48
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['labelValue']='Código comercial';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codigocomercial_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_estadonegocio_c.php

 // created: 2023-02-21 13:32:00
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['labelValue']='Estado de negocio';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_estadonegocio_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_fechacancelacion_c.php

 // created: 2023-02-21 13:40:15
$dictionary['Purchase']['fields']['sasa_fechacancelacion_c']['labelValue']='Fecha de cancelación';
$dictionary['Purchase']['fields']['sasa_fechacancelacion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_fechacancelacion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_fechacancelacion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_fechacancelacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_nombredirectorgestion_c.php

 // created: 2023-04-11 19:08:52
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['labelValue']='Nombre director de gestión';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombredirectorgestion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Purchases/Ext/Vardefs/sugarfield_sasa_nombreasistentegestion__c.php

 // created: 2023-04-11 19:11:20
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['labelValue']='Nombre asistente de gestión';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_nombreasistentegestion__c']['readonly_formula']='';

 
?>
