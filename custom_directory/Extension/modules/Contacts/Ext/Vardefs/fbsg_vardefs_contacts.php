<?php
$dictionary['Contact']['fields']['fbsg_ccintegrationlog_contacts'] = array(
    'name' => 'fbsg_ccintegrationlog_contacts',
    'type' => 'link',
    'relationship' => 'fbsg_ccintegrationlog_contacts',
    'source' => 'non-db',
    'vname' => 'CC Integration Log',
);

$dictionary["Contact"]["fields"]["prospect_list_contacts"] = array(
    'name' => 'prospect_list_contacts',
    'type' => 'link',
    'relationship' => 'prospect_list_contacts',
    'source' => 'non-db',
    'vname' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
);

$cc_module = 'Contact';
include('custom/include/fbsg_cc_newvars.php');
