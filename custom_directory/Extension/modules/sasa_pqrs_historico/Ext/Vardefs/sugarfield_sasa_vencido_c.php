<?php
 // created: 2022-11-18 20:49:11
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['labelValue']='Vencido';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_vencido_c']['readonly_formula']='';

 ?>