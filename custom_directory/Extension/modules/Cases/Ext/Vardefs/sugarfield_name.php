<?php
 // created: 2023-01-10 19:20:48
$dictionary['Case']['fields']['name']['len']='255';
$dictionary['Case']['fields']['name']['massupdate']=false;
$dictionary['Case']['fields']['name']['hidemassupdate']=false;
$dictionary['Case']['fields']['name']['comments']='The short description of the bug';
$dictionary['Case']['fields']['name']['importable']='false';
$dictionary['Case']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Case']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['name']['merge_filter']='disabled';
$dictionary['Case']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.53',
  'searchable' => true,
);
$dictionary['Case']['fields']['name']['calculated']='1';
$dictionary['Case']['fields']['name']['formula']='ifElse(
equal($sasa_tipodesolicitud_c,"quejas"),concat(toString($case_number)," - ",toString($sasa_radalianza_c)," - ",toString($sasa_radalianzam2_c)," - ",related($sasa_sasa_tipificaciones_cases_1,"name")),

ifElse(
equal($sasa_tipodesolicitud_c,"peticion"),concat(toString($case_number)," - ",related($sasa_sasa_tipificaciones_cases_1,"name")),

concat(toString($case_number)," - ",toString($sasa_tipodesolicitud_c))
)
)';
$dictionary['Case']['fields']['name']['enforced']=true;

 ?>