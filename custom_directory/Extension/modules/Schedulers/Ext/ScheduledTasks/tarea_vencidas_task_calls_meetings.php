<?php
/*
* Funcion para planificador de tareas
* Caso sasa 5407 Cuentas - Funcionalidad campo "Fecha de Actualización"
* 
* Se requiere, para el módulo de cuentas, monitorear el campo "Fecha de Actualización" y 60 días antes del valor de este campo se debe marcar el cliente potencial.
*
* La idea es validar el campo "Fecha de Actualización" y poblar el campo "Ctrl WF" en el módulo de Cuentas con el valor "Proceso Fecha de Actualización", la funcionalidad se debe ejecutar, solo si el campo "Ctrl WF" está "Vacío"
*
* Un WF monitoreará la actualización de registros del CP con este valor en el campo "Ctrl WF" , para hacer un proceso de generación de tarea y notificación en caso de no gestionarse en 60 días.
*/
$job_strings[] = 'tarea_vencidas_task_calls_meetings';
function tarea_vencidas_task_calls_meetings(){
	$GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Inicio-tarea_vencidas_task_calls_meetings. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	$return=true;
	try{
		$queries= array(
		//"select sasa_vencida_c,id, date_end,CURRENT_TIMESTAMP from meetings left join meetings_cstm on id = id_c where date_end < CURRENT_TIMESTAMP and status not in ('Held','Not Held')",
		//"select sasa_vencida_c,id, date_end,CURRENT_TIMESTAMP from calls left join calls_cstm on id = id_c where date_end < CURRENT_TIMESTAMP and status not in ('Held','Not Held')",
		//"select sasa_vencida_c,status, id, date_due,CURRENT_TIMESTAMP from tasks left join tasks_cstm on id = id_c where date_due < CURRENT_TIMESTAMP and status not in ('Completed')",
		"UPDATE meetings_cstm INNER JOIN meetings ON meetings_cstm.id_c = meetings.id SET sasa_vencida_c='Si' WHERE date_end < utc_timestamp and status not in ('Held','Not Held')",
		"UPDATE calls_cstm INNER JOIN calls ON calls_cstm.id_c = calls.id SET sasa_vencida_c='Si' WHERE date_end < utc_timestamp and status not in ('Held','Not Held')",
		"UPDATE tasks_cstm INNER JOIN tasks ON tasks_cstm.id_c = tasks.id SET sasa_vencida_c='Si' WHERE date_due < utc_timestamp and status not in ('Completed')",
		);
		
		foreach( $queries as $query ){
			global $db;	
			$query = str_replace(array("\n","\t"),array(" "," "), trim($query));
			$result = $db->query($query);
			$lastDbError = $db->lastDbError();				
			if(!empty($lastDbError)){
				$GLOBALS['log']->security("Error de base de datos: {$lastDbError} -> Query {$query}"); 
			}
			else{
				while($row = $db->fetchRow($result)){					
					$GLOBALS['log']->security("row: ".print_r( $row  ,true)); 
				}
			}
		}
	}
	catch (Exception $e) {     
		$GLOBALS['log']->security("ERROR: ".$e->getMessage()); 
	}
	$GLOBALS['log']->security("******************************************************");
	$GLOBALS['log']->security("Fin-tarea_vencidas_task_calls_meetings. ".date("Y-m-d h:i:s"));
	$GLOBALS['log']->security("******************************************************");
	return $return;
}
?>
