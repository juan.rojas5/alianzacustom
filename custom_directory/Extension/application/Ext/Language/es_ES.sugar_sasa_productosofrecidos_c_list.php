<?php
 // created: 2021-06-01 16:41:25

$app_list_strings['sasa_productosofrecidos_c_list']=array (
  '' => '',
  'APT' => 'APT',
  'Contrato de administracion de valores' => 'Contrato de Administraciòn de Valores',
  'EFG' => 'EFG',
  'Fideicomisos de Inversion con Destinacion Especifica' => 'Fideicomisos de Inversión con Destinación Específica',
  'Administracion de Inversiones de Fondos Mutuos de Inversion' => 'Administración de Inversiones de Fondos Mutuos de Inversión',
  'FCP OXO Propiedades Sostenibles' => 'FCP OXO Propiedades Sostenibles',
  'FCP OXO Propiedades Sostenibles COMP I Proyecto General' => 'FCP OXO Propiedades Sostenibles COMP I Proyecto General',
  'FCP OXO Propiedades Sostenibles COMP I Cartagena' => 'FCP OXO Propiedades Sostenibles COMP I Cartagena',
  'FCP OXO Propiedades Sostenibles COMP II OXO69' => 'FCP OXO Propiedades Sostenibles COMP II OXO69',
  'FCP OXO Propiedades Sostenibles COMP II Barranquilla' => 'FCP OXO Propiedades Sostenibles COMP II Barranquilla',
  'Fondo Abierto Libranza' => 'Fondo Abierto Libranza',
  'Fondo de Pensiones Voluntarias Vision' => 'Fondo de Pensiones Voluntarias Visión',
  'CXC Parte Activa' => 'CXC Parte Activa',
  'Recaudo' => 'Recaudo',
  'Pagos  Giros' => 'Pagos - Giros',
  'Fondo Cerrado mas Colombia Opportunity' => 'Fondo Cerrado mas Colombia Opportunity',
  'Fondo Alianza Acciones' => 'Fondo Alianza Acciones',
  'Fondo Alianza Liquidez Dolar' => 'Fondo Alianza Liquidez Dólar',
  'Fondo Alianza Renta Fija 90' => 'Fondo Alianza Renta Fija 90',
  'Fondo Abierto Alianza' => 'Fondo Abierto Alianza',
  'Fondo Abierto Alianza Gobierno' => 'Fondo Abierto Alianza Gobierno',
  'Fondo Abierto con Pacto de Permanencia CXC' => 'Fondo Abierto con Pacto de Permanencia CXC',
  'Fondo Cerrado Inmobiliario Alianza' => 'Fondo Cerrado Inmobiliario Alianza',
  'Fondo de Inversion Colectiva Inmuebles Fiducor Compartimento Aptos cll 92' => 'Fondo de Inversión Colectiva Inmuebles Fiducor Compartimento Aptos cll 92',
  'Fondo Alianza Renta Fija Mercados Emergentes' => 'Fondo Alianza Renta Fija Mercados Emergentes',
  'Custodia' => 'Custodia',
  'USD' => 'USD',
  'Derivados' => 'Derivados',
  'FOREX' => 'FOREX',
  'Patrimonio Autonomo de Terceros' => 'Patrimonio Autónomo de Terceros',
  'Bonos' => 'Bonos',
  'CDT' => 'CDT',
  'Repos' => 'Repos',
  'Simultaneas' => 'Simultaneas',
  'Renta Fija  Internacional' => 'Renta Fija - Internacional',
  'Colocacion Bonos' => 'Colocación Bonos',
  'TTV' => 'TTV',
  'Renta fija' => 'Renta fija',
  'PEI renta fija' => 'PEI (Renta Fija)',
  'Acciones' => 'Acciones',
  'OPA' => 'OPA',
  'Renta Variable' => 'Renta Variable',
  'Otros' => 'Otros',
  'DCEs' => 'DCE´s',
  'Factoring' => 'Factoring',
  'Confirming' => 'Confirming',
  'Flujos' => 'Flujos',
  'Pagares' => 'Pagares',
  'Sentencias Nacion Alianza' => 'Sentencias Nación Alianza',
  'Fic Alternativos Alianza' => 'Fic Alternativos Alianza',
  'Fideicomisos' => 'Fideicomisos',
);