<?php
 // created: 2022-10-25 23:16:57
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['labelValue']='Ingresos/Ventas Anuales';
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['calculated']='false';
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['dependency']='';
$dictionary['Account']['fields']['sasa_ingresosventasanuales_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>