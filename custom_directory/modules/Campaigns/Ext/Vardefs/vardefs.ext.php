<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Campaigns/Ext/Vardefs/fbsg_cc_newvars.php

$dictionary['Campaign']['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'len' => 255,
);
$dictionary['Campaign']['fields']['clicks'] = array(
    'required' => false,
    'name' => 'clicks',
    'vname' => 'LBL_CC_CLICKS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['opens'] = array(
    'required' => false,
    'name' => 'opens',
    'vname' => 'LBL_CC_OPENS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['forwards'] = array(
    'required' => false,
    'name' => 'forwards',
    'vname' => 'LBL_CC_FORWARDS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['bounces'] = array(
    'required' => false,
    'name' => 'bounces',
    'vname' => 'LBL_CC_BOUNCES',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['optouts'] = array(
    'required' => false,
    'name' => 'optouts',
    'vname' => 'LBL_CC_OPTOUTS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['spamreports'] = array(
    'required' => false,
    'name' => 'spamreports',
    'vname' => 'LBL_CC_SPAMREPORTS',
    'type' => 'int',
    'len' => 11,
);
$dictionary['Campaign']['fields']['last_run_completed'] = array(
    'name' => 'last_run_completed',
    'type' => 'bool',
    'default' => 0,
);

?>
<?php
// Merged from custom/Extension/modules/Campaigns/Ext/Vardefs/sugarfield_name.php

 // created: 2018-06-25 14:02:27
$dictionary['Campaign']['fields']['name']['audited']=false;
$dictionary['Campaign']['fields']['name']['massupdate']=false;
$dictionary['Campaign']['fields']['name']['comments']='The name of the campaign';
$dictionary['Campaign']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Campaign']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Campaign']['fields']['name']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['name']['unified_search']=false;
$dictionary['Campaign']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.39',
  'searchable' => true,
);
$dictionary['Campaign']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Campaigns/Ext/Vardefs/sugarfield_campaign_type.php

 // created: 2018-06-25 14:02:52
$dictionary['Campaign']['fields']['campaign_type']['massupdate']=true;
$dictionary['Campaign']['fields']['campaign_type']['comments']='The type of campaign';
$dictionary['Campaign']['fields']['campaign_type']['duplicate_merge']='enabled';
$dictionary['Campaign']['fields']['campaign_type']['duplicate_merge_dom_value']='1';
$dictionary['Campaign']['fields']['campaign_type']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['campaign_type']['unified_search']=false;
$dictionary['Campaign']['fields']['campaign_type']['full_text_search']=array (
);
$dictionary['Campaign']['fields']['campaign_type']['calculated']=false;
$dictionary['Campaign']['fields']['campaign_type']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Campaigns/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Campaign']['full_text_search']=false;

?>
