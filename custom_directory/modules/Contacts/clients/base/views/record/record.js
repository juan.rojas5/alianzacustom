({
    extendsFrom: 'RecordView',
	/*
	* Archivos necesarios
	* -> custom/modules/Leads/clients/base/views/create/create.js
	* -> custom/modules/Leads/clients/base/views/record/record.js
	*/

	/*
	* @inheritdoc, inicializa la UI de sugar
	*/
	initialize: function (options) {//@TODO cambiar this por self
		var self=this;//por contexto
		este = self;//para depurar en consola

		if(self.extendsFrom == 'RecordView') this.plugins = _.union(this.plugins || [], ['HistoricalSummary']);
		self._super('initialize', [options]);
	
		/*
		* Detectando cuando cambie sasa_pais_c
		*/
		self.model.on("change:sasa_pais_c", self._onChange_sasa_pais_c, self);

		/*
		* Detectando cuando cambie sasa_departamento_c
		*/
		self.model.on("change:sasa_departamento_c", self._onChange_sasa_departamento_c, self);

		/*
		* Detectando cuando cambie sasa_municipio_c
		*/
		self.model.on("change:sasa_municipio_c", self._onChange_sasa_municipio_c, self);

		/*
		* Detectando cuando cambie sasa_departamentointer_c
		*/
		self.model.on("change:sasa_departamentointer_c", self._onChange_sasa_departamentointer_c, self);

		/*
		* Detectando cuando cambie sasa_municipiointer_c
		*/
		self.model.on("change:sasa_municipiointer_c", self._onChange_sasa_municipiointer_c, self);

		/*
		* Solo cuando termine de graficar los campos
		*/
		self.model.once( "sync",
			function() {
				self._onChange_sasa_pais_c();
				self._onChange_sasa_departamento_c();
				self._onChange_sasa_municipio_c();
				self._onChange_sasa_departamentointer_c();
				self._onChange_sasa_municipiointer_c();
			},
			self
		);
	},
	
	_onChange_sasa_pais_c: function(fields, errors, callback) {
		var self=this;//por contexto
		//var app = App;
		var sasa_pais_c = self.model.get('sasa_pais_c');
		if(!_.isEmpty(sasa_pais_c)){
			var country = app.lang.getAppListStrings('sasa_pais_c_list');
			self.model.set('primary_address_country', country[sasa_pais_c]);
			self.model.set('primary_address_state', '');
			self.model.set('primary_address_city', '');	
		}
	},
	_onChange_sasa_departamento_c: function(fields, errors, callback) {
		var self=this;//por contexto
		//var app = App;
		var sasa_departamento_c = self.model.get('sasa_departamento_c');
		var sasa_pais_c = self.model.get('sasa_pais_c');
		if(!_.isEmpty(sasa_departamento_c) && !_.isEmpty(sasa_pais_c) && (sasa_pais_c == 'Colombia' || sasa_pais_c == 'No Registra')){
			var state  = app.lang.getAppListStrings('sasa_departamento_c_list');
			self.model.set('primary_address_state', state[sasa_departamento_c]);
			self.model.set('primary_address_city', '');	
		}
	},
	_onChange_sasa_municipio_c: function(fields, errors, callback) {
		var self=this;//por contexto
		//var app = App;
		var sasa_municipio_c = self.model.get('sasa_municipio_c');
		var sasa_pais_c = self.model.get('sasa_pais_c');
		if(!_.isEmpty(sasa_municipio_c) && !_.isEmpty(sasa_pais_c) && (sasa_pais_c == 'Colombia' || sasa_pais_c == 'No Registra')){
			var city = app.lang.getAppListStrings('sasa_municipio_c_list');
			self.model.set('primary_address_city', city[sasa_municipio_c]);	
		}
	},
	_onChange_sasa_departamentointer_c: function(fields, errors, callback) {
		var self=this;//por contexto
		//var app = App;
		var sasa_departamentointer_c = self.model.get('sasa_departamentointer_c');
		var sasa_pais_c = self.model.get('sasa_pais_c');
		if(!_.isEmpty(sasa_departamentointer_c) && !_.isEmpty(sasa_pais_c) && sasa_pais_c != 'Colombia'){
			self.model.set('primary_address_state', sasa_departamentointer_c);
		}
	},
	_onChange_sasa_municipiointer_c: function(fields, errors, callback) {
		var self=this;//por contexto
		//var app = App;
		var sasa_municipiointer_c = self.model.get('sasa_municipiointer_c');
		var sasa_pais_c = self.model.get('sasa_pais_c');
		if(!_.isEmpty(sasa_municipiointer_c) && !_.isEmpty(sasa_pais_c) && sasa_pais_c != 'Colombia'){
			self.model.set('primary_address_city', sasa_municipiointer_c);	
		}
	},
})
