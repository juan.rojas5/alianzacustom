({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	token: null,
	autenticarse: null,
	ws: null,
	data: null,
	tipo: null,
	cuenta2:[],
	certificado:[],
	url:'',
	tipoIdentificacion:'',
	initialize: function(options) {
		var self = this;	
		dashlet8 = self;	
		if (typeof app === 'undefined') app = App;			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet8_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet8_id_cliente").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet8_id_cliente").css("border", "none"); 
						$("#dashlet8_id_cliente").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet8_id_cliente").attr('readonly', true);
					}
					
					$("#dashlet8_Tipo_Certificado").change( function(){
						self.dashlet8_portal_dashlet8_Tipo_Certificado_change();	
					});
					
					$("#dashlet8_consultar").click( function(){
						self.dashlet8_portal();	
					});
					
					$("#dashlet8_consultar").show();
					
					$("#dashlet8_descargar1").click( function(){
						self.dashlet_portal_obtener_descargar20();	
					});
					
					$("#dashlet8_enviar1").click( function(){
						self.dashlet_portal_obtener_enviar20();	
					});
					
					$("#dashlet8_descargar2").click( function(){
						self.dashlet8_portal_obtener_descargar2(true,false);	
					});
					
					$("#dashlet8_enviar2").click( function(){
						self.dashlet8_portal_obtener_descargar2(true,true);	
					});
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet8_portal: function(){
		var self = dashlet8;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet8_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet8_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){
			if (!_.isEmpty( $("#dashlet8_id_cliente").val() )){		
				//cuenta/correo $("#dashlet10_correo").html("<option value='daniel.montoya@sasaconsultoria.com'>me</option>");
				self.dashlet8_portal_dashlet8_get_cuenta();	
				
				//extracto
				self.dashlet8_portal_obtener_cuenta1(true);//poblando lista Cuenta	
				self.dashlet8_portal_obtener_periodo(false);//poblando lista periodo	
				
				//certificado
				self.dashlet8_portal_obtener_tipo_certificado(false);//poblando lista tipo certificado	
				self.dashlet8_portal_dashlet8_Tipo_Certificado_change();//poblando cuenta y vigencia		
			}
			else{
				
				app.alert.show('error_procesando_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet8_portal_procesando');		
			document.getElementById('dashlet8_skm_LockPane').className = "LockOff"; 	
		}, 200 );		
	},
	dashlet8_portal_dashlet8_get_cuenta: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
	
		/*
		Obteniendo cuenta
		*/
		$("#dashlet8_nombre").html('');
		$("#dashlet8_correo").html('');
		self.tipoIdentificacion="";
		app.api.call(
			'GET', 
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet8_id_cliente").val()), 
				{
					"sasa_nroidentificacion_c": $("#dashlet8_id_cliente").val(),
				}, 
				{
				success: function (data) {
					if(!_.isEmpty(data.records)){
						$("#dashlet8_nombre").html(data.records[0].name);
						self.tipoIdentificacion = data.records[0].sasa_tipoidentificacion_c;
						if(self.tipoIdentificacion=="Cedula de Ciudadania") self.tipoIdentificacion ="CC";
						if(self.tipoIdentificacion=="Cedula de Extranjeria") self.tipoIdentificacion ="CE";
						if(self.tipoIdentificacion=="Nit") self.tipoIdentificacion ="NIT";
						if(!_.isEmpty(data.records[0].email)){						
							$.each(data.records[0].email, function( k, mail ) {
								var valor = mail.email_address;
								var texto = mail.email_address;
								var selected = "";
								if(mail.primary_address) selected ='selected="true"';
								if(mail.invalid_email){
									texto += " (Invalido)";
								}
								else if(mail.primary_address){
									texto += " (Principal)";
								} 
								else if(mail.opt_out){
									texto += " (Rehusado)";
								} 
								else if(mail.invalid_email){
									texto += " (No Valido)";
								}
								else{
									texto += " (Opcional)";
								}
								if (!_.isEmpty( valor )){ 
									$("#dashlet8_correo").append('<option '+selected+' value="'+valor+'">'+texto+'</option>');	
								}
							});
						}
					}
					else{
						$("#dashlet8_nombre").html('--<b>No Existe en sugar</b>--');
						$("#dashlet8_correo").html('<option>--</option>');
					}
				},
				error: function (e) {		
					app.alert.dismiss('dashlet8_portal_procesando');				
					app.alert.show('error_procesando_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
						autoClose: false,
					});
				}
			}, {async: false}
		);
		if(self.tipoIdentificacion=="" | self.tipoIdentificacion==null) self.tipoIdentificacion ="CC";
	},
	dashlet8_portal_obtener_cuenta2: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;		

		self.respuesta = [];
		if (!_.isEmpty( self.certificado )){	
			var i = $("#dashlet8_Tipo_Certificado").val();
			var fondo = self.certificado[i].codigoFondo;
			self.autenticarse = autenticarse;dashlet8_Tipo_Certificado
			self.ws = "/api_fiducia/consultas/pensiones/"+fondo+"/cuentas?idUsuarioRecurso="+$("#dashlet8_id_cliente").val();
			self.tipo = "GET";
			self.data = [];
			self.dashlet8_portal_call({});//autenticandose y consumo
			if (self.token != null){
				if (!_.isEmpty( self.respuesta )){ 
					if(!$.isArray( self.respuesta )){			
						var temp = self.respuesta;
						self.respuesta = [];
						self.respuesta.push(temp);
					}	
					console.log("respuesta_cuenta2: ",self.respuesta);
					self.cuenta = [];
					$.each(self.respuesta, function( i, respuesta ) {				
						if (typeof respuesta.wpft_fond !== 'undefined') {					
							var valor = respuesta.wpft_plan;
							var texto = respuesta.wpft_plan+" "+respuesta.wpft_plan_descri;
							self.cuenta[i]=[];
							self.cuenta[i][respuesta.wpft_fond+'_valor'] = valor;	
							self.cuenta[i][respuesta.wpft_fond+'_texto'] = texto;	
						}		
					});
				}
				else{				
					
					app.alert.show('error_procesando_obtener_cuenta2', {
						level: 'error',
						title: 'Error al obtener_cuenta2',
						autoClose: false,
					});
				}
			}
		}
	},
	dashlet8_portal_dashlet8_Tipo_Certificado_change: function(){	
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		
		self.dashlet8_portal_obtener_cuenta2(true);//poblando lista Cuenta
			
		console.log("dashlet8_portal_dashlet8_Tipo_Certificado_change",self.cuenta);
		$("#dashlet8_cuenta2").empty();//limpiando
		$("#dashlet8_Tipo_vigencia").empty();//limpiando
		if (!_.isEmpty( self.certificado )){		
			var i = $("#dashlet8_Tipo_Certificado").val();
			var fondo = self.certificado[i].codigoFondo;
			var anioDesde = self.certificado[i].anioDesde;
			var anioHasta = self.certificado[i].anioHasta;
			if (!_.isEmpty( self.cuenta )){ 	
				$.each(self.cuenta, function( k, cuenta ) {
					var cuenta_valor = cuenta[fondo+'_valor'];
					var cuenta_texto = cuenta[fondo+'_texto'];
					if (!_.isEmpty( cuenta_valor )){ 
						$("#dashlet8_cuenta2").append('<option value="'+cuenta_valor+'">'+cuenta_texto+'</option>');	
					}
				});
			}
			else{			
				$("#dashlet8_cuenta2").append('<option value="">--</option>');
			}			
			for (j = anioDesde; j <= anioHasta; j++) { 
			  $("#dashlet8_Tipo_vigencia").append('<option value="'+j+'">'+j+'</option>');
			}
		}
		
		if ( _.isEmpty( $("#dashlet8_Tipo_vigencia").html().trim() ) ){ 
			$("#dashlet8_Tipo_vigencia").append('<option value="">--</option>');
		}
		
		if ( _.isEmpty( $("#dashlet8_cuenta2").html().trim() ) ){ 
			$("#dashlet8_cuenta2").append('<option value="">--</option>');
		}
	},
	dashlet8_portal_obtener_descargar1: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;				
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet8_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet8_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){			
			if (!_.isEmpty( $("#dashlet8_id_cliente").val() )    ){
				self.autenticarse = autenticarse;
				self.ws = "/api_reportes/consultas/extractos/pensiones?encargo="+$("#dashlet8_cuenta1").val()+"&fechaDesde="+$("#dashlet8_Periodo").val()+"&idUsuarioRecurso="+$("#dashlet8_id_cliente").val();
				self.tipo = "file";
				self.data = [];
				var mail = {};
				var tipo = "Extracto Pensiones";
				if(enviar){	
					var Address = "";	
					if ( !_.isEmpty( $("#dashlet8_correo").val() )){
						if (typeof $("#dashlet8_correo").val()[0] !== 'undefined'){	
							if ( !_.isEmpty( $("#dashlet8_correo").val()[0] )){ 
								firma="Atentamente,\n___________\nAlianza";
								//Address = $("#dashlet8_correo").val().join("; ");
								Address = $("#dashlet8_correo").val();	
								Subject = tipo+" - "+$("#dashlet8_id_cliente").val()+" - "+Date();								
								nombre = $("#dashlet8_nombre").html();
								
								Body = "";
								Body += "Buen día,";
								Body += "\n\n";
								Body += "Estimado "+nombre+",";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Reciba un cordial saludo de parte de Alianza.";
								Body += "\n\n";
								Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
								Body += "\n\n";
								Body += "\n\n";
								Body += "Cordialmente,";
								Body += "\n\n";
								Body += "Alianza.";
								Body += "\n\n";
								Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
								
								mail = {
									"Subject": Subject,
									"Body": Body,
									"Address": Address,
									"AddressCC": "",
								};
							}
						}
					}
					if ( _.isEmpty( Address )){
						app.alert.show('error_procesando_obtener_descargar2', {
							level: 'error',
							title: "Por favor, seleccione una direccion de correo!!",
							autoClose: false,
						});
						return;	
					}
				}
				self.dashlet8_portal_call(mail);//autenticandose y consumo
				if (self.token != null){
					if (!_.isEmpty( self.respuesta )){ 
						if(enviar){									
							if(self.respuesta == "ok"){
								//var Address = $("#dashlet8_correo").val().join("; ");
								var Address = $("#dashlet8_correo").val();
								app.alert.show('success_procesando_obtener_descargar1', {
									level: 'success',
									messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
									autoClose: false,
								});	
							}
							else{
								app.alert.show('error_procesando_obtener_descargar1', {
									level: 'error',
									title: self.respuesta,
									autoClose: false,
								});	
							}									
						}
						else{
							let pdfWindow = window.open("","Extracto");
							if(pdfWindow != null){
								pdfWindow.document.write("<iframe  height='98.5%' width='99%' style='position:absolute;' src='data:application/pdf;base64, " + encodeURI(self.respuesta)+"'></iframe>");
							}
							else{
								app.alert.show('error_procesando_obtener_descargar1', {
									level: 'error',
									title: 'Error al descargar PDF',
									messages: "El navegador esta bloqueando la ventana!!!",
									autoClose: false,
								});
							}	
						}
					}
					else{		
						app.alert.show('error_procesando_obtener_descargar1', {
							level: 'error',
							title: 'Error al descargar1 PDF',
							autoClose: false,
						});
					}
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet8_portal_procesando');		
			document.getElementById('dashlet8_skm_LockPane').className = "LockOff"; 	
		}, 200 );	
	},		
	dashlet8_portal_obtener_cuenta: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;	
		
		self.autenticarse = autenticarse;
		self.ws = "/api_fiducia/comercial/consultas/obtenerClientes";
		self.tipo = "POST";
		self.data = '{\"nombre": \"\", \"id\": \"'+$("#dashlet8_id_cliente").val()+'\", \"tipoId\": \"\", \"producto\": \"\", \"desde\": 1, \"hasta\": 1}';
		self.dashlet8_portal_call({});//autenticandose y consumo
	},		
	dashlet8_portal_obtener_descargar2: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet8_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet8_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){	
			if (!_.isEmpty( $("#dashlet8_id_cliente").val() )    ){					
				if ( !_.isEmpty($("#dashlet8_Tipo_Certificado").val())  &&  !_.isEmpty($("#dashlet8_cuenta2").val())    &&  !_.isEmpty($("#dashlet8_Tipo_vigencia").val())      ){					
					self.dashlet8_portal_obtener_cuenta(true);//poblando cuenta
					if (self.token != null){			
						if (!_.isEmpty( self.respuesta )){					
							console.log("cuenta_tipoIdentificacion:",  self.respuesta);										
							if (typeof self.respuesta.datos !== 'undefined'){
								if (typeof self.respuesta.datos[0] !== 'undefined'){
									if (typeof self.respuesta.datos[0].codTipoIdentificacion !== 'undefined'){
										var tipoIdentificacion = self.respuesta.datos[0].codTipoIdentificacion;	
										if(tipoIdentificacion != null){
											self.autenticarse = false;
											self.tipo = "file_post";
											var ruta = self.certificado[$("#dashlet8_Tipo_Certificado").val()].rutaCert;
											var encargo = $("#dashlet8_cuenta2").val();
											var vigencia = $("#dashlet8_Tipo_vigencia").val();	
											self.ws = "/api_reportes/consultas/certificaciones/generarCertificadoTributarioPensiones?&idUsuarioRecurso="+$("#dashlet8_id_cliente").val()+"&tipoIdentificacion="+tipoIdentificacion;		
											self.data = '{\"ruta\":\"'+ruta+'\",\"encargo\":\"'+encargo+'\",\"vigencia\":'+vigencia+'}';var mail = {};
											var tipo = "Certificados Tributarios Pensiones";
											if(enviar){	
												var Address = "";	
												if ( !_.isEmpty( $("#dashlet8_correo").val() )){
													if (typeof $("#dashlet8_correo").val()[0] !== 'undefined'){	
														if ( !_.isEmpty( $("#dashlet8_correo").val()[0] )){ 
															firma="Atentamente,\n___________\nAlianza";
															//Address = $("#dashlet8_correo").val().join("; ");
															Address = $("#dashlet8_correo").val();	
															Subject = tipo+" - "+$("#dashlet8_id_cliente").val()+" - "+Date();				
															nombre = $("#dashlet8_nombre").html();
															
															Body = "";
															Body += "Buen día,";
															Body += "\n\n";
															Body += "Estimado "+nombre+",";
															Body += "\n\n";
															Body += "\n\n";
															Body += "Reciba un cordial saludo de parte de Alianza.";
															Body += "\n\n";
															Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
															Body += "\n\n";
															Body += "\n\n";
															Body += "Cordialmente,";
															Body += "\n\n";
															Body += "Alianza.";
															Body += "\n\n";
															Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
											
															mail = {
																"Subject": Subject,
																"Body": Body,
																"Address": Address,
																"AddressCC": "",
															};
														}
													}
												}
												if ( _.isEmpty( Address )){
													app.alert.show('error_procesando_obtener_descargar2', {
														level: 'error',
														title: "Por favor, seleccione una direccion de correo!!",
														autoClose: false,
													});
													return;	
												}
											}
											self.dashlet8_portal_call(mail);//autenticandose y consumo
											if (!_.isEmpty( self.respuesta )){ 
												if(enviar){						
													if(self.respuesta == "ok"){
														//var Address = $("#dashlet8_correo").val().join("; ");
														var Address = $("#dashlet8_correo").val();
														app.alert.show('success_procesando_obtener_descargar2', {
															level: 'success',
															messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
															autoClose: false,
														});	
													}
													else{
														app.alert.show('error_procesando_obtener_descargar2', {
															level: 'error',
															title: self.respuesta,
															autoClose: false,
														});	
													}									
												}
												else{
													//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
							                        var mapForm = document.createElement("form");
							                            mapForm.target = "Map";
							                            mapForm.method = "POST"; // or "post" if appropriate
							                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

							                            var mapInput = document.createElement("input");
							                            mapInput.type = "text";
							                            mapInput.name = "file";
							                            mapInput.value = self.respuesta;
							                            mapForm.appendChild(mapInput);

							                            document.body.appendChild(mapForm);

							                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

							                        if (map) {
							                            mapForm.submit();
							                        }
													else{
														app.alert.show('error_procesando_obtener_descargar1', {
															level: 'error',
															title: 'Error al descargar PDF',
															messages: "El navegador esta bloqueando la ventana!!!",
															autoClose: false,
														});
													}							
												}
											}
											else{				
												
												app.alert.show('error_procesando_obtener_descargar2', {
													level: 'error',
													title: 'Error al descargar PDF',
													autoClose: false,
												});
											}
										}
										else{				
											
											app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
												level: 'error',
												title: 'Error tipoIdentificacion vacio',
												autoClose: false,
											});
										}
									}
									else{				
										
										app.alert.show('error_obtener_cuenta', {
											level: 'error',
											title: 'Error obtener_cuenta vacio',
											autoClose: false,
										});
									}
								}
								else{				
									
									app.alert.show('error_obtener_cuenta', {
										level: 'error',
										title: 'Error obtener_cuenta vacio',
										autoClose: false,
									});
								}
							}
							else{				
								
								app.alert.show('error_obtener_cuenta', {
									level: 'error',
									title: 'Error obtener_cuenta vacio',
									autoClose: false,
								});
							}
						}
						else{				
							
							app.alert.show('error_obtener_cuenta', {
								level: 'error',
								title: 'Error obtener_cuenta vacio',
								autoClose: false,
							});
						}
					}
				
				}
				else{
					
					app.alert.show('error_procesando_obtener_descargar2', {
						level: 'error',
						title: "Hay un campo sin seleccionar",
						autoClose: false,
					});
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet8_portal_procesando');		
			document.getElementById('dashlet8_skm_LockPane').className = "LockOff"; 	
		}, 200 );
	},
	dashlet8_portal_obtener_tipo_certificado: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet8_Tipo_Certificado").empty();//limpiando
		self.ws = "/api_reportes/consultas/certificaciones/certificadosTributariosPensionesList";
		self.tipo = "GET";
		self.data = [];
		self.dashlet8_portal_call({});//autenticandose y consumo
		self.certificado = [];		
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("respuesta_obtener_tipo_certificado: ",self.respuesta);
				self.certificado = self.respuesta;
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.nombreCert !== 'undefined') {
						$("#dashlet8_Tipo_Certificado").append('<option value="'+i+'">'+respuesta.nombreCert+'</option>');			
					}
				});
				if ( _.isEmpty( $("#dashlet8_Tipo_Certificado").html().trim() ) ){ 
					$("#dashlet8_Tipo_Certificado").append('<option value="">--</option>');
				}	
			}
			else{				
				self.certificado = [];		
				$("#dashlet8_Tipo_Certificado").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener_certificado vacio',
					autoClose: false,
				});
			}
		}
		
		if ( _.isEmpty( $("#dashlet8_Tipo_Certificado").html().trim() ) ){ 
			$("#dashlet8_Tipo_Certificado").append('<option value="">--</option>');
		}
	},	
	dashlet8_portal_obtener_cuenta1: function(autenticarse){
		var self = dashlet8;
		var fondo = "";
		if(typeof app === 'undefined') app = App;

		self.ws = "/extractos-v1/api_fiducia/consultas/pensiones";
		self.tipo = "POST";
		self.data = {
			"idUsuarioRecurso": $("#dashlet8_id_cliente").val(),
		};
		self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
		$("#dashlet8_cuenta1").empty(); //limpiando
		if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
			if(self.respuesta.operacionExitosa) {
				console.log("respuesta_fondo_extracto: ", self.respuesta);				
				if(typeof self.respuesta.listaProductos !== 'undefined') {
					if(typeof self.respuesta.listaProductos[0] !== 'undefined') {
						if(typeof self.respuesta.listaProductos[0].identificacion !== 'undefined') {
							fondo = self.respuesta.listaProductos[0].identificacion;
						
							self.ws = "/extractos-v1/api_fiducia/consultas/encargo";
							self.tipo = "POST";
							self.data = {
								"idUsuarioRecurso": $("#dashlet8_id_cliente").val(),
								"numFondo": fondo,
							};
						
							self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
							
							if(!_.isEmpty(self.respuesta) & self.respuesta != null) {
								if(self.respuesta.operacionExitosa) {
									console.log("Encargo extracto: ", self.respuesta);
									if(typeof self.respuesta.listaProductos !== 'undefined') {
										plan = self.respuesta.listaProductos[0].plan + "" + self.respuesta.listaProductos[0].digitoVerificacion;
										$.each(self.respuesta.listaProductos, function(i, respuesta) {
											if(typeof respuesta.plan !== 'undefined') {
												plan = respuesta.plan  + '' + respuesta.digitoVerificacion;
												if(plan != "" && plan != null) {	
													$("#dashlet8_cuenta1").append('<option value="' + plan + '">' + respuesta.fondo + ' - ' + plan + ' - ' + respuesta.descripcion + '</option>');
												}
											}
										});
									}
								} 
								
								if(typeof self.respuesta.error !== 'undefined') {					
									app.alert.show('error_dashlet8_portal_obtener_fondo', {
										level: 'error',
										title: '<p><br>Error al consultar encargo Extractos',
										messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
										autoClose: false,
									});
								}
							}
						}
					}
				}
				
			} 
			else {
				if(typeof self.respuesta.error !== 'undefined') {					
					app.alert.show('error_dashlet8_portal_obtener_fondo', {
						level: 'error',
						title: '<p><br>Error al enviar PDF Extractos',
						messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
						autoClose: false,
					});
				}
				else{
					app.alert.show('error_dashlet8_portal_obtener_fondo', {
						level: 'error',
						title: 'Error al obtener_fondo extracto: ' + self.respuesta.descripcionError,
						autoClose: false,
					});
				}
			}	
		}		
				
		if(_.isEmpty($("#dashlet8_cuenta1").html().trim())) {
			$("#dashlet8_cuenta1").append('<option value="">--</option>');
		}
	},
	dashlet8_portal_obtener_periodo: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		$("#dashlet8_Periodo").empty();//limpiando	
		var date = new Date();		
		for (i = 0; i < 6; i++) { 
			var option =  date.toLocaleString("es", {year: "numeric"}) + '/'+ date.toLocaleString("es", {month:"2-digit"});//formato requerido	
			var value = date.toLocaleString("es", { month: "long" }).replace(/(\b\w)/gi,function(m){return m.toUpperCase();})+" "+date.getFullYear();//formato requerido			
			date.setMonth(date.getMonth()-1);//un mes menos
			$("#dashlet8_Periodo").append('<option value="'+option+'">'+value+'</option>');	
		}		
				
		if(_.isEmpty($("#dashlet8_Periodo").html().trim())) {
			$("#dashlet8_Periodo").append('<option value="">--</option>');
		}
	},	
	dashlet_portal_call20: function(PAI, mail) {
		var self = this;
		if(typeof app === 'undefined') app = App;
		var method = "dashlet_portal";
		var data = [{
			'url': self.url,
			'tipo': self.tipo,
			'ws': self.ws,
			'data': self.data,
			'PAI': PAI,
		}];

		var url = app.api.buildURL(method, 'read', '', {
			data
		});

		app.api.call('read', url, null, {
			success: _.bind(function(response) {
				self.respuesta = response['respuesta'];
			}, this),
			error: _.bind(function(error) {
				self.token = null;
				self.respuesta = null;

				app.alert.show('error_dashlet_portal_call20', {
					level: 'error',
					title: 'Fallo al procesar ' + error,
					autoClose: false,
				});
			}, this),
		}, {
			async: false
		});
	},
	dashlet_portal_obtener_descargar20: function(autenticarse, enviar) {
		var self = dashlet8;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet8_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet8_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if( !_.isEmpty($("#dashlet8_id_cliente").val()) && !_.isEmpty($("#dashlet8_cuenta1").val()) && !_.isEmpty($("#dashlet8_Periodo").val()) ) {				
				var plan = $("#dashlet8_cuenta1").val();
				if(plan != "" && plan != null) {	
					self.ws = "/extractos-v1/api_alianza/docs/extractos";
					self.tipo = "POST";
					self.data = {
						"tipoProducto": "FPV",
						"producto": plan,
						"periodo":$("#dashlet8_Periodo").val(),
						"tipoIdentificacion": self.tipoIdentificacion,
						"numeroIdentificacion": $("#dashlet8_id_cliente").val(),
					};
					
					self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
					
					if(!_.isEmpty(self.respuesta)) {
						if(typeof self.respuesta.error !== 'undefined') {
							app.alert.show('error_dashlet_portal_obtener_descargar20', {
								level: 'error',
								title: '<p><br>Error al descargar PDF Extractos',
								messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
								autoClose: false,
							});
						}
						else{
							//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
	                        var mapForm = document.createElement("form");
	                            mapForm.target = "Map";
	                            mapForm.method = "POST"; // or "post" if appropriate
	                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

	                            var mapInput = document.createElement("input");
	                            mapInput.type = "text";
	                            mapInput.name = "file";
	                            mapInput.value = self.respuesta;
	                            mapForm.appendChild(mapInput);

	                            document.body.appendChild(mapForm);

	                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

	                        if (map) {
	                            mapForm.submit();
	                        }
							else{
								app.alert.show('error_procesando_obtener_descargar1', {
									level: 'error',
									title: 'Error al descargar PDF',
									messages: "El navegador esta bloqueando la ventana!!!",
									autoClose: false,
								});
							}										
						}
					} 
					else {
						app.alert.show('error_dashlet_portal_obtener_descargar20', {
							level: 'error',
							title: 'Error al descargar PDF Extractos',
							autoClose: false,
						});
					}							
				}
				else{
					app.alert.show('error_dashlet8_portal_obtener_fondo', {
						level: 'error',
						title: 'Error al consultar encargo extracto: ' + self.respuesta.descripcionError,
						autoClose: false,
					});
				}				
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_descargar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet8_portal_procesando');
			document.getElementById('dashlet8_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet_portal_obtener_enviar20: function(autenticarse, enviar) {	
		var self = dashlet8;
		if(typeof app === 'undefined') app = App;

		/*
		Bloqueando interfaz
		*/
		window.setTimeout(function() {
			document.getElementById('dashlet8_skm_LockPane').className = "LockOn";
			app.alert.dismissAll();
			app.alert.show('dashlet8_portal_procesando', {
				level: 'process',
				title: 'Procesando...',
				autoClose: false,
			});
		}, 100);

		window.setTimeout(function() {
			if( !_.isEmpty($("#dashlet8_id_cliente").val()) && !_.isEmpty($("#dashlet8_cuenta1").val()) && !_.isEmpty($("#dashlet8_Periodo").val()) ) {
				var plan = $("#dashlet8_cuenta1").val();
				if(plan != "" && plan != null) {
					var Address = "";
					if(!_.isEmpty($("#dashlet8_correo").val()) && $("#dashlet8_correo").val() != "--") {
						if(typeof $("#dashlet8_correo").val()[0] !== 'undefined') {
							if(!_.isEmpty($("#dashlet8_correo").val()[0])) {
								//Address = $("#dashlet8_correo").val().join("; ");
								Address = $("#dashlet8_correo").val();
							}
						}
					}
					
					if(_.isEmpty(Address)) {
						app.alert.show('error_dashlet_portal_obtener_enviar20', {
							level: 'error',
							title: "Por favor, seleccione una direccion de correo!!",
							autoClose: false,
						});
					}
					else{	
						self.ws = "/extractos-v1/api_alianza/docs/extractosPorCorreo";
						self.tipo = "POST";
						self.data = {
							"tipoProducto": "FPV",
							"producto": plan,
							"periodo":$("#dashlet8_Periodo").val(),
							"tipoIdentificacion": self.tipoIdentificacion,
							"numeroIdentificacion": $("#dashlet8_id_cliente").val(),
							"correo": Address,
						};
						
						self.dashlet_portal_call20("dashlet_portal20", {}); //autenticandose y consumo
						
						if(self.respuesta == '' | self.respuesta == null){
							self.respuesta = "En proceso...";
							
							app.alert.show('dashlet8_dashlet_portal_obtener_enviar20', {
								level: 'success',
								title: 'Se ha enviado la solicitud al servidor!!',							
								messages:'<hr><p><b>Respuesta:</b><p><br><p>'+self.respuesta+"<p>",
								autoClose: false,
							});	
						}
						else{						
							if(typeof self.respuesta.error !== 'undefined') {
								app.alert.show('error_dashlet_portal_obtener_enviar20', {
									level: 'error',
									title: '<p><br>Error al enviar PDF Extractos',
									messages:' <p>'+self.respuesta.status+ "<br> " +self.respuesta.error+" <p><br><p> "+self.respuesta.message,
									autoClose: false,
								});
							}
							else{	
								app.alert.show('dashlet8_dashlet_portal_obtener_enviar20', {
									level: 'error',
									title: '<p><br>Error al enviar PDF Extractos',						
									messages:"Fallo en el  WS de enviar PDF pro correo para Extractos",
									autoClose: false,
								});	
							}
						}					
					}
				}
				else{
					app.alert.show('error_dashlet8_portal_obtener_fondo', {
						level: 'error',
						title: 'Error al consultar encargo extracto: ' + self.respuesta.descripcionError,
						autoClose: false,
					});
				}			
			} 
			else {

				app.alert.show('error_dashlet_portal_obtener_descargar20', {
					level: 'error',
					title: 'Error campos necesarios vacios',
					autoClose: false,
				});
			}

			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet8_portal_procesando');
			document.getElementById('dashlet8_skm_LockPane').className = "LockOff";
		}, 200);
	},
	dashlet8_portal_call: function(mail){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		
		/*
		Para el periodo
		Al parecer no hay ningún servicio que realice esta lógica, el portal con esa función lo que hace es calcular los últimos 6 meses a partir de la fecha para mostrar en el listado.

		*/
		if (typeof app === 'undefined') app = App;
		var method  = "dashlet_portal";		
		var data = [
			{
				'url':self.url,			
				'tipo':'POST',	
				'ws':'/auth_fiducia/comercial/autenticar',
				'data': "**PAI**",
				'autenticarse': self.autenticarse,
				'token': self.token,
			},
			{				
				//call
				'url':self.url,
				'tipo':self.tipo,
				'ws':self.ws,
				'data':self.data,
				'mail':mail,
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) { 
				self.token=response['token'];
				self.respuesta=response['respuesta'];	
				
				
				if(self.token.includes("Credenciales invalidas")){
					error = JSON.parse(self.token);				
					if(error["data"] == "Credenciales invalidas" ){						
						app.alert.show('error_procesando_obtener_fondo', {
							level: 'error',
							title: 'Error '+error["code"]+' '+error["data"],
							autoClose: false,
						});
						self.token=null;
					}
				}	
				
			}, this),  
			error: _.bind(function(error) {  
				self.token=null;
				self.respuesta=null;
				
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	}
})
