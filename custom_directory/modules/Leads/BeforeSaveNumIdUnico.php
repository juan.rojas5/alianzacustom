<?php
//if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once "custom/include/api/cstmSugarApiExceptionError.php";
//Caso 5889 Se requiere extender a versión "móvil" la funcionalidad que actualmente se tiene (modo "escritorio") en clientes potenciales para identificar algún registro con el mismo Número de ID en el módulo de CP o CU.
class BeforeSaveNumIdUnico {
	function before_save($bean, $event, $arguments) {
		global $db;
		if (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false) { //Previene el Evenp Loop
			$bean->ignore_update_c = true; //update
			if (!empty(trim($bean->sasa_nroidentificacion_c))) { //tarea CRM sasa, no tener en cuenta documento vacio... https://sasaconsultoria.sugarondemand.com/#Tasks/7364d2da-476f-11ea-b95b-02fb8f607ac4
				if (!isset($bean->fetched_row['id'])) { //Si el registro es nuevo
					/*
					* Validar si existe un account diferente con ese numero de documento
					*/
				 	$account_and = ($bean->module_name == "Accounts")? " and accounts.id != '{$bean->id}'":" ";
					$accounts_query="SELECT * FROM accounts_cstm left JOIN accounts ON accounts_cstm.id_c=accounts.id WHERE accounts_cstm.sasa_nroidentificacion_c = '{$bean->sasa_nroidentificacion_c}' AND accounts.deleted = '0' {$account_and}";
					$queryaccount = $db->query($accounts_query);
					$accounts = mysqli_num_rows($queryaccount);
					
					if ($accounts >= 1) {
						//API exception para impedir el duardado del registro
						throw new cstmSugarApiExceptionError("fallo, El numero de identificacion '{$bean->sasa_nroidentificacion_c}' ya existe en accounts."); //Mensaje que llegara a la API exception						
					}
					
					/*
					* Solo al crear leads, validar primero account y luego validar si existe otro lead con ese numero de documento
					*/
					if($bean->module_name == "Leads"){
					 	$lead_and = ($bean->module_name == "Leads")? " and leads.id != '{$bean->id}'":" ";
						$leads_query="SELECT * FROM leads_cstm left JOIN leads ON leads_cstm.id_c=leads.id WHERE leads_cstm.sasa_nroidentificacion_c = '{$bean->sasa_nroidentificacion_c}' AND leads.deleted = '0' {$lead_and}";
						$queryleads = $db->query($leads_query);
						$leads = mysqli_num_rows($queryleads);
						
						$GLOBALS['log']->security("bean->module_name: {$bean->module_name}");
						$GLOBALS['log']->security("accounts_query: {$accounts_query}");
						$GLOBALS['log']->security("leads_query: {$leads_query}");
						
						if ($leads >= 1) {
							//API exception para impedir el duardado del registro
							throw new cstmSugarApiExceptionError("fallo, El numero de identificacion '{$bean->sasa_nroidentificacion_c}' ya existe en leads."); //Mensaje que llegara a la API exception						
						}
					}
				}
			}
		}
	}
}
?>