<?php
 // created: 2023-04-11 19:20:19
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['labelValue']='Nombre perfil de riesgo';
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['enforced']='';
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['dependency']='';
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_nombreperfilriesgo_c']['readonly_formula']='';

 ?>