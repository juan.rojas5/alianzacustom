<?php
$module_name = 'sasa_MovimientosAV';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosav_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
                'enabled' => true,
                'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1SASA_PRODUCTOSAFAV_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'accounts_sasa_movimientosav_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_SASA_MOVIMIENTOSAV_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_nemotecnico_c',
                'label' => 'LBL_SASA_NEMOTECNICO_C',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_emisor_c',
                'label' => 'LBL_SASA_EMISOR_C',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_fechacumplimiento_c',
                'label' => 'LBL_SASA_FECHACUMPLIMIENTO_C',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_fechaliquid_c',
                'label' => 'LBL_SASA_FECHALIQUID_C',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_documento_c',
                'label' => 'LBL_SASA_DOCUMENTO_C',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_tipooperacion_c',
                'label' => 'LBL_SASA_TIPOOPERACION_C',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_detalle_c',
                'label' => 'LBL_SASA_DETALLE_C',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_nominal_c',
                'label' => 'LBL_SASA_NOMINAL_C',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_tirprecio_c',
                'label' => 'LBL_SASA_TIRPRECIO_C',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'sasa_valor_c',
                'label' => 'LBL_SASA_VALOR_C',
                'enabled' => true,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'sasa_comision_c',
                'label' => 'LBL_SASA_COMISION_C',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'sasa_iva_c',
                'label' => 'LBL_SASA_IVA_C',
                'enabled' => true,
                'default' => true,
              ),
              16 => 
              array (
                'name' => 'sasa_retencion_c',
                'label' => 'LBL_SASA_RETENCION_C',
                'enabled' => true,
                'default' => true,
              ),
              17 => 
              array (
                'name' => 'sasa_afavor_c',
                'label' => 'LBL_SASA_AFAVOR_C',
                'enabled' => true,
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'sasa_encontra_c',
                'label' => 'LBL_SASA_ENCONTRA_C',
                'enabled' => true,
                'default' => true,
              ),
              19 => 
              array (
                'name' => 'sasa_saldo_c',
                'label' => 'LBL_SASA_SALDO_C',
                'enabled' => true,
                'default' => true,
              ),
              20 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              21 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              22 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              23 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => true,
              ),
              24 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              25 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
              26 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              27 => 
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
