<?php
 // created: 2018-07-10 21:27:22
$dictionary['Contact']['fields']['department']['audited']=true;
$dictionary['Contact']['fields']['department']['massupdate']=false;
$dictionary['Contact']['fields']['department']['comments']='The department of the contact';
$dictionary['Contact']['fields']['department']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['department']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['department']['merge_filter']='disabled';
$dictionary['Contact']['fields']['department']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['department']['calculated']=false;
$dictionary['Contact']['fields']['department']['importable']='false';
$dictionary['Contact']['fields']['department']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>