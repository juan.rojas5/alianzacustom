<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_TITLE'] = 'Líneas de Ingreso: Inicio';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Líneas de Ingreso';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COST_PRICE'] = 'Coste:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_DISCOUNT_PRICE'] = 'Valor Mínimo de Inversión:';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Información del Registro';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_LIKELY'] = 'Valor Proyectado';
$mod_strings['LBL_SASA_MOTIVOPERDIDA_C'] = 'Motivo de Pérdida';
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Crear una Lnea de Ingreso';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Lnea de Ingreso';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Crear una Lnea de Ingreso';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Lnea de Ingreso';
$mod_strings['LBL_RELATED_PRODUCTS'] = 'Lnea de Ingreso Relacionado';
$mod_strings['LBL_CONVERT_TO_QUOTE_INFO_MESSAGE'] = 'Generar Presupuesto desde la Lnea de Ingreso';
$mod_strings['LBL_CONVERT_INVALID_RLI_PRODUCT'] = 'La Lnea de Ingreso necesita un Producto del Catálogo de Productos antes de que el Presupuesto pueda ser creado.';
$mod_strings['LBL_CONVERT_TO_QUOTE_ERROR_MESSAGE'] = 'Hubo un error convirtiendo esta Lnea de Ingreso en un Presupuesto';
$mod_strings['LBL_CONVERT_RLI_TO_QUOTE'] = 'Generar Presupuesto desde la Lnea de Ingreso';

?>
