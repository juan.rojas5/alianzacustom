<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/fbsg_cc_usernewvars.php

/**
 * ATTENTION!!!!!!!!!!
 * ANY CHANGES MADE HERE MUST BE REFLECTED IN THE pre_execute.php FILE
 *
 */
$cc_module = 'User';

global $sugar_version;
$is7 = !!(preg_match('/^7/', $sugar_version));

$dictionary[$cc_module]['fields']['cc_id'] = array(
    'required' => false,
    'name' => 'cc_id',
    'vname' => 'LBL_CCID',
    'type' => 'varchar',
    'reportable' => true,
    'importable' => false,
    'len' => 255,
);

$dictionary[$cc_module]['fields']['cc_synced'] = array(
    'required' => false,
    'name' => 'cc_synced',
    'vname' => 'LBL_CC_SYNCED',
    'type' => 'bool',
    'default' => false,
    'importable' => false,
    'reportable' => true,
    'duplicate_merge' => 'disabled',
);

?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_address_postalcode.php

 // created: 2018-07-11 20:56:55
$dictionary['User']['fields']['address_postalcode']['audited']=false;
$dictionary['User']['fields']['address_postalcode']['massupdate']=false;
$dictionary['User']['fields']['address_postalcode']['duplicate_merge']='enabled';
$dictionary['User']['fields']['address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['address_postalcode']['merge_filter']='disabled';
$dictionary['User']['fields']['address_postalcode']['unified_search']=false;
$dictionary['User']['fields']['address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['address_postalcode']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_phone_home.php

 // created: 2018-07-13 21:31:52
$dictionary['User']['fields']['phone_home']['audited']=false;
$dictionary['User']['fields']['phone_home']['massupdate']=false;
$dictionary['User']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';
$dictionary['User']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['User']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['User']['fields']['phone_home']['unified_search']=false;
$dictionary['User']['fields']['phone_home']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['phone_home']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_nombrecompleto_c.php

 // created: 2018-08-18 14:11:48
$dictionary['User']['fields']['sasa_nombrecompleto_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_nombrecompleto_c']['labelValue']='Nombre completo';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['sasa_nombrecompleto_c']['calculated']='true';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['formula']='concat($first_name," ",$last_name)';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['enforced']='true';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_external_auth_only.php

 // created: 2018-08-23 11:22:55
$dictionary['User']['fields']['external_auth_only']['default']=false;
$dictionary['User']['fields']['external_auth_only']['audited']=false;
$dictionary['User']['fields']['external_auth_only']['duplicate_merge']='enabled';
$dictionary['User']['fields']['external_auth_only']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['external_auth_only']['merge_filter']='disabled';
$dictionary['User']['fields']['external_auth_only']['reportable']=true;
$dictionary['User']['fields']['external_auth_only']['unified_search']=false;
$dictionary['User']['fields']['external_auth_only']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_segmento_c.php

 // created: 2021-08-06 06:07:35
$dictionary['User']['fields']['sasa_segmento_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_segmento_c']['labelValue']='Segmento';
$dictionary['User']['fields']['sasa_segmento_c']['calculated']=false;
$dictionary['User']['fields']['sasa_segmento_c']['dependency']='';
$dictionary['User']['fields']['sasa_segmento_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_compania_c.php

 // created: 2021-08-06 06:07:36
$dictionary['User']['fields']['sasa_compania_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_compania_c']['labelValue']='Compañía';
$dictionary['User']['fields']['sasa_compania_c']['calculated']=false;
$dictionary['User']['fields']['sasa_compania_c']['dependency']='';
$dictionary['User']['fields']['sasa_compania_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_regional_c.php

 // created: 2021-08-06 06:07:36
$dictionary['User']['fields']['sasa_regional_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_regional_c']['labelValue']='Regional';
$dictionary['User']['fields']['sasa_regional_c']['calculated']=false;
$dictionary['User']['fields']['sasa_regional_c']['dependency']='';
$dictionary['User']['fields']['sasa_regional_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2021-08-06 06:07:36
$dictionary['User']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['User']['fields']['sasa_superior2_c']['calculated']=false;
$dictionary['User']['fields']['sasa_superior2_c']['dependency']='';
$dictionary['User']['fields']['sasa_superior2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2021-08-06 06:07:36
$dictionary['User']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['User']['fields']['sasa_superior1_c']['calculated']=false;
$dictionary['User']['fields']['sasa_superior1_c']['dependency']='';
$dictionary['User']['fields']['sasa_superior1_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_controlvacaciones_c.php

 // created: 2021-06-09 15:51:36
$dictionary['User']['fields']['sasa_controlvacaciones_c']['labelValue']='Control vacaciones';
$dictionary['User']['fields']['sasa_controlvacaciones_c']['enforced']='';
$dictionary['User']['fields']['sasa_controlvacaciones_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_envacaciones_c.php

 // created: 2021-06-09 15:31:37
$dictionary['User']['fields']['sasa_envacaciones_c']['labelValue']='En vacaciones';
$dictionary['User']['fields']['sasa_envacaciones_c']['enforced']='';
$dictionary['User']['fields']['sasa_envacaciones_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_fechafin_c.php

 // created: 2021-06-09 15:26:27
$dictionary['User']['fields']['sasa_fechafin_c']['labelValue']='Fecha fin';
$dictionary['User']['fields']['sasa_fechafin_c']['enforced']='';
$dictionary['User']['fields']['sasa_fechafin_c']['dependency']='equal($sasa_envacaciones_c,"true")';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_fechainicio_c.php

 // created: 2021-06-09 15:25:45
$dictionary['User']['fields']['sasa_fechainicio_c']['labelValue']='Fecha inicio';
$dictionary['User']['fields']['sasa_fechainicio_c']['enforced']='';
$dictionary['User']['fields']['sasa_fechainicio_c']['dependency']='equal($sasa_envacaciones_c,"true")';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sasa_usuarioreemplazante_c.php

 // created: 2021-06-09 15:32:38
$dictionary['User']['fields']['sasa_usuarioreemplazante_c']['labelValue']='Usuario reemplazante';
$dictionary['User']['fields']['sasa_usuarioreemplazante_c']['dependency']='equal($sasa_envacaciones_c,"true")';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_user_id_c.php

 // created: 2021-06-09 15:32:38

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/users_sasa1_unidades_de_negocio_por__1_Users.php

// created: 2023-02-06 20:16:31
$dictionary["User"]["fields"]["users_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'users_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa1_Unidades_de_negocio_por_',
  'bean_name' => 'sasa1_Unidades_de_negocio_por_',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_USERS_TITLE',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
