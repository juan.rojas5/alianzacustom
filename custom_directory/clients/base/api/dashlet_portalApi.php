<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

class dashlet_portalApi extends SugarApi
{
	var $data;
	var $token;
	var $respuesta;
	var $file_post;
	
	var $contrasena;
	var $tipoUsuario;
	var $url;
	var $usuario;
	
	public function registerApiRest()
	{
		return array(
		    //GET & POST
		    'dashlet_portal' => array(
			//request type
			'reqType' => array('GET','POST'),

			//set authentication
			'noLoginRequired' => false,

			//endpoint path
			'path' => array('dashlet_portal'),

			//endpoint variables
			'pathVars' => array(''),

			//method to call
			'method' => 'dashlet_portal',

			//short help string to be displayed in the help documentation
			'shortHelp' => 'dashlet_portal',

			//long help to be displayed in the help documentation
			//'longHelp' => 'custom/clients/base/api/help/dashlet_portalApi_help.html',
		    ),
		);
	}

    /**
     * Method to be used for my MyEndpoint dashlet_portal
     */
	public function dashlet_portal($api, $args)
	{ 			
		$retorno="";
		try{
			require_once 'modules/Administration/Administration.php';
			$Administration = new Administration();
			$Administration->retrieveSettings("PAI");
			
			$name = $args['data'][0]['PAI'];
			if(empty($name)) $name = "dashlet_portal";
			$settings = $Administration->settings["PAI_{$name}"];
			$this->contrasena = $settings["contrasena"];
			$this->tipoUsuario = $settings["tipoUsuario"];
			$this->url = $settings["url"];
			$this->usuario = $settings["usuario"];
			
			if($args['data'][0]['data'] == "**PAI**") $args['data'][0]['data']  = "{\"tipoUsuario\":{$this->tipoUsuario},\"usuario\":\"{$this->usuario}\",\"contrasena\":\"{$this->contrasena}\"}";
			$this->data = $args['data'];
			
			if($name == "dashlet_portal20"){				
				$retorno=$this->dahslet_portal_data20();
			}
			else{
				$retorno=$this->dahslet_portal_data();
			}
			if(!isset($retorno["respuesta"]->error)){
				if(($retorno["data"]["tipoProducto"] == "FIC" or $retorno["data"]["tipoProducto"] == "FPV" or $retorno["data"]["tipoProducto"] == "CI") and empty($retorno["data"]["correo"]) and !strpos($retorno["respuesta"], 'ERROR:') and !strpos($retorno["respuesta"], 'Recurso no encontrado')){
					$this->file_post=$retorno["respuesta"];
					$retorno["respuesta"] = "PDF! en base 64...";	
					$GLOBALS['log']->security($retorno);		
					$retorno["respuesta"]=$this->file_post;		
				}
				else{					
					$GLOBALS['log']->security($retorno);
				}
			}
			else{			
				$GLOBALS['log']->security($retorno);
			}
		}
		catch (Exception $e) {      
			$retorno="dashlet_portal->. ERROR1:".$e->getMessage(); 
		}
		return $retorno;
	}	

    /**
     * Custom logic of dahslet_portal_data
     */
	public function dahslet_portal_data20(){		
		$retorno="";
		try{
			if($this->dahslet_portal_conectar($this->url)){
				if(isset($this->data) == false)  $this->data = array();
				if(isset($this->data[0]) == false)  $this->data[0] = array();
				if(isset($this->data[0]["data"]) == false)  $this->data[0]["data"] ="";
				if(isset($this->data[0]["mail"]) == false)  $this->data[0]["mail"] ="";
				$this->token = "Basic ".base64_encode("{$this->usuario}:{$this->contrasena}");		
				$data_raw = $this->data[0]["data"];				
				$this->respuesta=$this->dahslet_portal_call20($this->url,$this->data[0]["ws"],$data_raw,$this->data[0]["tipo"],$this->data[0]["mail"]);
												
				$retorno=array(
					"ws"=>$this->data[0]["ws"],
					"tipo"=>$this->data[0]["tipo"],
					"data"=>$this->data[0]["data"],
					"respuesta"=>$this->respuesta,
				);
			}
			else{
				$retorno="No se pudo conectar a {$thisurl}";
			}
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_portal->. ERROR2:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	public function dahslet_portal_call20($url,$ws_auth,$data_raw,$tipo,$mail_raw){
		$retorno="";
		try{	
			$type = $tipo;	
							
			$curl = curl_init();
			$data = json_encode($data_raw,JSON_UNESCAPED_SLASHES);
			
			curl_setopt($curl, CURLOPT_URL, "{$url}{$ws_auth}");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($curl, CURLOPT_HTTPHEADER,array("cache-control: no-cache","content-type: application/json","Authorization: {$this->token}"));
			curl_setopt($curl, CURLOPT_ENCODING, "");
			curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
			$response = trim(curl_exec($curl));	
			
			$err = curl_error($curl);
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			
			if ($err) {
				$retorno="dashlet_portal->. ERROR3: cURL Error #{$err}";
			}
			else{				
				if(empty($data_raw["correo"]) and ($data_raw["tipoProducto"] == "FIC" or $data_raw["tipoProducto"] == "FPV" or $data_raw["tipoProducto"] == "CI") and  !strpos($response, 'Recurso no encontrado') ){
					$retorno=  base64_encode($response);
				}
				else{
					if(!empty($data_raw["correo"]) and ($data_raw["tipoProducto"] == "FIC" or $data_raw["tipoProducto"] == "FPV" or $data_raw["tipoProducto"] == "CI")) $this->dashlet_portal_enviar20($data_raw["correo"],$data_raw["numeroIdentificacion"],$data_raw["tipoIdentificacion"]);
					$retorno=  json_decode($response);
				}
			}
			
			
			/*
			$GLOBALS['log']->security( " \n **************** \n" );
			$GLOBALS['log']->security( "retorno:\n". print_r($retorno,true));
			$GLOBALS['log']->security( " \n **************** \n" );	
			*/
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_portal->. ERROR4:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	public function dashlet_portal_enviar20($correo,$numeroIdentificacion,$tipoIdentificacion){	
		$firma = "Atentamente,\n___________\nAlianza";
		$date = date("Y-m-d h:i:sa");
		$Subject = "{$tipoIdentificacion} - {$numeroIdentificacion} - {$date}";

		$Body = "Se ha enviado una solicitud para enviar el PDF de Extracto para <b>{$tipoIdentificacion} {$numeroIdentificacion}</b> al correo <b>{$correo}</b>, {$date} \n\n<p>";
		$Body .= "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes.  \n\n<p>";
								
		/*
		Enviar PDF por correo
		*/
		if(!empty($correo)){		
			$email = BeanFactory::newBean('Emails');
			$defaults = $email->getSystemDefaultEmail();	
						
			$email->from_addr = $defaults['email'];
			$email->from_name = $defaults['name'];
			$email->name = from_html($Subject);
			$email->description_html = from_html($Body );
			$email->to_addrs_arr = array($correo);
			$email->to = $correo;
			$email->to_addrs = $correo;
			//$email->parent_type = 'Contacts';
			//$email->parent_id = $single_mail['contact_id'];
			$email->save();
		}
	}

	/**
	* Custom logic of dahslet_portal_data
	*/
	public function dahslet_portal_data(){		
		$retorno="";
		try{
			if($this->dahslet_portal_conectar($this->url)){
				if($this->data[0]["autenticarse"] ){
					$this->token=$this->dahslet_portal_call($this->url,$this->data[0]["ws"],$this->data[0]["data"],$this->data[0]["tipo"],array());//autenticarse
				}
				else{
					$this->token=$this->data[0]["token"];
				}
				if(isset($this->data) == false)  $this->data = array();
				if(isset($this->data[1]) == false)  $this->data[1] = array();
				if(isset($this->data[1]["data"]) == false)  $this->data[1]["data"] ="";
				if(isset($this->data[1]["mail"]) == false)  $this->data[1]["mail"] ="";
				$this->respuesta=$this->dahslet_portal_call($this->url,$this->data[1]["ws"],$this->data[1]["data"],$this->data[1]["tipo"],$this->data[1]["mail"]);//consumo original
				if($this->data[1]["tipo"] != "file") $this->respuesta= json_decode($this->respuesta);//json_decode....
				if($this->data[1]["tipo"] == "file_post" or $this->data[1]["tipo"] == "file_body" or $this->data[1]["ws"] =="/api_fiducia/certificaciones/valores/consultas/certificaciones/ultimoDiaHabil") $this->respuesta = $this->file_post;//por alguna razon queda en blanco en el return, use una propiedad en su lugar en la funcion....
				$retorno=array(
					"ws"=>$this->data[1]["ws"],
					"tipo"=>$this->data[1]["tipo"],
					"data"=>$this->data[1]["data"],
					"token"=>$this->token,
					"respuesta"=>$this->respuesta,
				);
			}
			else{
				$retorno="No se pudo conectar a {$thisurl}";
			}
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_portal->. ERROR2:".$e->getMessage(); 
		}	
		return $retorno;
	}
	
	public function dahslet_portal_conectar($url){
		$retorno=false;
		try{		
			//$GLOBALS['log']->security("dahslet_portal_conectar1");
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, "");
			curl_setopt($curl
			, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
				//$retorno="dashlet_portal->. ERROR5: cURL Error #{$err}";
				$retorno=false;
			} 
			else {
				$retorno=true;
			}
			$retorno=true;
		} 
		catch (Exception $e) {      
			//$retorno = "dashlet_portal->. ERROR4:".$e->getMessage(); 
			$retorno = false; 
		}	
		return $retorno;
	}
	public function dahslet_portal_call($url,$ws_auth,$data,$tipo,$mail_raw){
		$retorno="";
		try{	
			//$GLOBALS['log']->security("dahslet_portal_conectar2");
			$type = $tipo;	
			
			if($tipo == 'file')$type = "GET";
			if($tipo == 'file_post')$type = "POST";
			if($tipo == 'file_body')$type = "GET";	
							
			$curl = curl_init();	
			
			/*
			DEBUG
			*/
			//$fp = fopen('/var/www/html/alianzatest/errorlog.txt', 'w');curl_setopt($curl, CURLOPT_STDERR, $fp);curl_setopt($curl, CURLOPT_VERBOSE, true);
				
			curl_setopt($curl, CURLOPT_URL, "{$url}{$ws_auth}");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($curl, CURLOPT_HTTPHEADER,array("cache-control: no-cache","content-type: application/json","token: {$this->token}"));
			curl_setopt($curl, CURLOPT_ENCODING, "");
			curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
		
			if($tipo == 'file' or $tipo == 'file_post'){
				curl_setopt($curl, CURLOPT_HEADER, 1);
				curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
			}
			
			$response = curl_exec($curl);
						
			$err = curl_error($curl);
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			
			if ($err) {
				$retorno="dashlet_portal->. ERROR3: cURL Error #{$err}";
			}
			else {				
				if($tipo == 'file'  or $tipo == 'file_post'){
					$separador = "%PDF-";
					/*
					fuente https://ryansechrest.com/2012/07/send-and-receive-binary-files-using-php-and-curl/
					*/
					$response_array = explode("\n\r", $response, 2);
					$header_array = explode("\n", $response_array[0]);
					foreach($header_array as $header_value) {
						$header_pieces = explode(':', $header_value);
						if(count($header_pieces) == 2) {
							$headers[$header_pieces[0]] = trim($header_pieces[1]);
						}
					}
					$response=  substr($response_array[1], 1);					
					$response = explode($separador, $response);
					$response = (!empty(trim($response[1])))? $separador.$response[1]:"";
				}				
				if($tipo == 'file_body' or $tipo == 'file'  or $tipo == 'file_post'){
					$response=  base64_encode(trim($response));
				
					/*
					Enviar PDF por correo
					*/
					if( !is_null($mail_raw) and !empty($mail_raw) ){
						if(!empty($response)){
							if(!empty($mail_raw["Address"])){					
								$emailObj = new Email();
								$defaults = $emailObj->getSystemDefaultEmail();
								$mail_data=array(
									"From"=>$defaults['email'],
									"FromName"=>$defaults['name'],
									"Subject"=>from_html($mail_raw["Subject"] ),
									"Body"=>from_html($mail_raw["Body"] ),
									"Address"=>from_html($mail_raw["Address"] ),
									"AddressCC"=>from_html($mail_raw["AddressCC"]),
									"Attachment"=>base64_decode(substr($response, strpos($response, ","))),
									"filename"=>"adjunto.pdf",
									"filename"=>"adjunto.pdf",
									"collation"=>"base64",
									"type"=>"application/pdf",
								);
								try{
									if( empty($emailTo)) $emailTo = $defaults['email'];
									try{
										$mailer = MailerFactory::getSystemDefaultMailer();
										$mailTransmissionProtocol = $mailer->getMailTransmissionProtocol();
										$SugarPHPMailer = new SugarPHPMailer();
										$SugarPHPMailer->setMailerForSystem();
										$SugarPHPMailer->From = $mail_data['From'];
										$SugarPHPMailer->FromName = $mail_data['FromName'];
										$SugarPHPMailer->ClearAllRecipients();
										$SugarPHPMailer->ClearReplyTos();
										$SugarPHPMailer->Subject=from_html($mail_data["Subject"]);
										$SugarPHPMailer->Body=from_html($mail_data["Body"]);
										$SugarPHPMailer->prepForOutbound();	
										$Addresses = explode(";",$mail_data["Address"]);		
										foreach($Addresses as $i => $Address){
											$SugarPHPMailer->AddAddress($Address);    
										}
										if(!empty($mail_data["AddressCC"])){
											$SugarPHPMailer->addCC($mail_data["AddressCC"]);   		
										}	
										$SugarPHPMailer->addStringAttachment($mail_data["Attachment"], $mail_data["filename"], $mail_data["collation"], $mail_data["type"]);
										try{
											if (@$SugarPHPMailer->Send()) {
												$response = "ok";
											}
											else{
												$response = "Error al enviar {$mail_data["Address"]}:<p><br><p>".$SugarPHPMailer->ErrorInfo."<hr>".__METHOD__."::".__LINE__;
											}
										}
										catch (Exception $e) {     
											$response= "ERROR: ".$e->getMessage(); 
										}
									}
									catch (MailerException $me) {     
										$message = $me->getMessage();
										switch ($me->getCode()) {
											case \MailerException::FailedToConnectToRemoteServer:
												$response= "BeanUpdatesMailer :: error sending email, system smtp server is not set";
												break;
											default:
												$response= "BeanUpdatesMailer :: error sending e-mail (method: {$mailTransmissionProtocol}), (error: {$message})";
												break;
										}
									}
								}
								catch (Exception $e) {     
									$response= "ERROR: ".$e->getMessage(); 
								}
							}
							else{
								$response = "direccion para vacia!!";
							}	
						}
						else{
							$response = "PDF vacio!!";
						}
					}	
				}	
				$retorno = $this->file_post = $response;	//por alguna razon queda en blanco en el return, use una propiedad en su lugar en la funcion....
			}		
		} 
		catch (Exception $e) {      
			$retorno = "dashlet_portal->. ERROR4:".$e->getMessage(); 
		}	
		return $retorno;
	}
}
?>
