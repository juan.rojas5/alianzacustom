<?php
 // created: 2018-07-10 15:41:06
$dictionary['Opportunity']['fields']['name']['audited']=true;
$dictionary['Opportunity']['fields']['name']['massupdate']=false;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['duplicate_merge']='disabled';
$dictionary['Opportunity']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.65',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['name']['calculated']=false;
$dictionary['Opportunity']['fields']['name']['importable']='true';

 ?>