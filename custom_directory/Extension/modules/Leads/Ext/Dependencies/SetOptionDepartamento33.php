<?php
$dependencies['Leads']['SetOptionDepartamento33'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Vichada")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Cumaribo","La Primavera","Puerto Carreno","Santa Rosalia")',
           'labels' => 'createList("","Cumaribo","La Primavera","Puerto Carreño","Santa Rosalía")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
