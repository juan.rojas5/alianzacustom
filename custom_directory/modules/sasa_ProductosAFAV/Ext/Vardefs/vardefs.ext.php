<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_categorias_sasa_productosafav_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:22:57
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa_categorias_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa_Categorias',
  'bean_name' => 'sasa_Categorias',
  'side' => 'right',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link-type' => 'one',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1_name"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_CATEGORIAS_TITLE',
  'save' => true,
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasa_productosafav_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'name',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_categorias_sasa_productosafav_1sasa_categorias_ida"] = array (
  'name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_CATEGORIAS_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE_ID',
  'id_name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
  'link' => 'sasa_categorias_sasa_productosafav_1',
  'table' => 'sasa_categorias',
  'module' => 'sasa_Categorias',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasa_saldosaf_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:25:02
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_saldosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAF',
  'bean_name' => 'sasa_SaldosAF',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasa_saldosav_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:26:58
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_saldosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'sasa_SaldosAV',
  'bean_name' => 'sasa_SaldosAV',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasa_movimientosaf_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:27:42
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosaf_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosaf_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosaf_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAF',
  'bean_name' => 'sasa_MovimientosAF',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasa_movimientosav_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:28:46
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosav_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAV',
  'bean_name' => 'sasa_MovimientosAV',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasa_movimientosavdivisas_1_sasa_ProductosAFAV.php

// created: 2019-01-04 21:30:05
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasa_movimientosavdivisas_1"] = array (
  'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAVDivisas',
  'bean_name' => 'sasa_MovimientosAVDivisas',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAVDIVISAS_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_produ845etosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_sasap_presupuestoxproducto_1_sasa_ProductosAFAV.php

// created: 2019-03-08 18:57:52
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_sasap_presupuestoxproducto_1"] = array (
  'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
  'source' => 'non-db',
  'module' => 'sasaP_PresupuestoxProducto',
  'bean_name' => 'sasaP_PresupuestoxProducto',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASAP_PRESUPUESTOXPRODUCTO_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_produff64tosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_ProductosAFAV']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa2_unidades_de_negocio_sasa_productosafav_1_sasa_ProductosAFAV.php

// created: 2023-02-06 20:17:53
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'source' => 'non-db',
  'module' => 'sasa2_Unidades_de_negocio',
  'bean_name' => 'sasa2_Unidades_de_negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link-type' => 'one',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unidades_de_negocio_sasa_productosafav_1_name"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'name',
);
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa2_unid99b0negocio_ida"] = array (
  'name' => 'sasa2_unid99b0negocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA_PRODUCTOSAFAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE_ID',
  'id_name' => 'sasa2_unid99b0negocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_ProductosAFAV/Ext/Vardefs/sasa_productosafav_purchases_1_sasa_ProductosAFAV.php

// created: 2023-02-06 20:21:32
$dictionary["sasa_ProductosAFAV"]["fields"]["sasa_productosafav_purchases_1"] = array (
  'name' => 'sasa_productosafav_purchases_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_purchases_1',
  'source' => 'non-db',
  'module' => 'Purchases',
  'bean_name' => 'Purchase',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_PURCHASES_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'id_name' => 'sasa_productosafav_purchases_1sasa_productosafav_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
