<?php
 // created: 2022-10-25 23:16:59
$dictionary['Account']['fields']['sasa_ocupacion_c']['labelValue']='Ocupación';
$dictionary['Account']['fields']['sasa_ocupacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_ocupacion_c']['enforced']='';
$dictionary['Account']['fields']['sasa_ocupacion_c']['dependency']='or(
equal($sasa_tipopersona_c,"Natural"),
equal($sasa_tipopersona_c,"Juridica")
)';

 ?>