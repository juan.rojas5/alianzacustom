<?php
 // created: 2018-07-14 14:58:08
$dictionary['Account']['fields']['phone_office']['len']='100';
$dictionary['Account']['fields']['phone_office']['required']=false;
$dictionary['Account']['fields']['phone_office']['massupdate']=false;
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['phone_office']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_office']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1.05',
  'searchable' => false,
);
$dictionary['Account']['fields']['phone_office']['calculated']=false;
$dictionary['Account']['fields']['phone_office']['importable']='false';

 ?>