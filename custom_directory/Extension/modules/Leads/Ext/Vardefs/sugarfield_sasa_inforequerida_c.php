<?php
 // created: 2018-08-20 23:46:18
$dictionary['Lead']['fields']['sasa_inforequerida_c']['labelValue']='Información Requerida*';
$dictionary['Lead']['fields']['sasa_inforequerida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['sasa_inforequerida_c']['enforced']='';
$dictionary['Lead']['fields']['sasa_inforequerida_c']['dependency']='equal($sasa_ctrlconversion_c,"Conversion II")';

 ?>