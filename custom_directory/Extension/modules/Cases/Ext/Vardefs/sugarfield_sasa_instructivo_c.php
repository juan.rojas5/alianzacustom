<?php
 // created: 2022-12-16 16:00:06
$dictionary['Case']['fields']['sasa_instructivo_c']['duplicate_merge_dom_value']=0;
$dictionary['Case']['fields']['sasa_instructivo_c']['labelValue']='Instructivo';
$dictionary['Case']['fields']['sasa_instructivo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_instructivo_c']['calculated']='true';
$dictionary['Case']['fields']['sasa_instructivo_c']['formula']='related($sasa_sasa_tipificaciones_cases_1,"sasa_instructivo_c")';
$dictionary['Case']['fields']['sasa_instructivo_c']['enforced']='true';
$dictionary['Case']['fields']['sasa_instructivo_c']['dependency']='';
$dictionary['Case']['fields']['sasa_instructivo_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_instructivo_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_instructivo_c']['readonly_formula']='';

 ?>