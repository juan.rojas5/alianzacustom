<?php
 // created: 2022-11-21 20:51:44
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sasa_pqrs_historico_1'] = array (
  'order' => 100,
  'module' => 'sasa_pqrs_historico',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SASA_PQRS_HISTORICO_1_FROM_SASA_PQRS_HISTORICO_TITLE',
  'get_subpanel_data' => 'accounts_sasa_pqrs_historico_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
