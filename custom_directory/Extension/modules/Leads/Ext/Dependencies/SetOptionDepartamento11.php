<?php
$dependencies['Leads']['SetOptionDepartamento11'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Casanare")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","San Luis de Gaceno","Villanueva","Sabanalarga","Aguazul","Chameza","Hato Corozal","La Salina","Mani","Monterrey","Nunchia","Orocue","Paz de Ariporo","Pore","Recetor","Sacama","Tamara","Tauramena","Trinidad","Yopal")',
           'labels' => 'createList("","San Luis de Gaceno","Villanueva","Sabanalarga","Aguazul","Chámeza","Hato Corozal","La Salina","Maní","Monterrey","Nunchía","Orocué","Paz de Ariporo","Pore","Recetor","Sácama","Támara","Tauramena","Trinidad","Yopal")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
