<?php
 // created: 2018-07-09 14:49:04
$dictionary['RevenueLineItem']['fields']['name']['required']=false;
$dictionary['RevenueLineItem']['fields']['name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['name']['comments']='Name of the product';
$dictionary['RevenueLineItem']['fields']['name']['importable']='false';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge']='disabled';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['RevenueLineItem']['fields']['name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.57',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['name']['calculated']='true';
$dictionary['RevenueLineItem']['fields']['name']['formula']='related($rli_templates_link,"name")';
$dictionary['RevenueLineItem']['fields']['name']['enforced']=true;

 ?>