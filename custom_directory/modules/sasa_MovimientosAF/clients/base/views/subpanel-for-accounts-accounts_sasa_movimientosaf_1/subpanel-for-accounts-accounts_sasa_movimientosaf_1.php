<?php
// created: 2019-01-04 21:54:39
$viewdefs['sasa_MovimientosAF']['base']['view']['subpanel-for-accounts-accounts_sasa_movimientosaf_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'sasa_productosafav_sasa_movimientosaf_1_name',
          'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
          'enabled' => true,
          'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1SASA_PRODUCTOSAFAV_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sasa_categoria_c',
          'label' => 'LBL_SASA_CATEGORIA_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sasa_nombrefondo_c',
          'label' => 'LBL_SASA_NOMBREFONDO_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sasa_nombresubcuenta_c',
          'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'sasa_encargo_c',
          'label' => 'LBL_SASA_ENCARGO_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'sasa_fecha_c',
          'label' => 'LBL_SASA_FECHA_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'sasa_motivo_c',
          'label' => 'LBL_SASA_MOTIVO_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'sasa_valor_c',
          'label' => 'LBL_SASA_VALOR_C',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sasa_formapago_c',
          'label' => 'LBL_SASA_FORMAPAGO_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'sasa_concepto_c',
          'label' => 'LBL_SASA_CONCEPTO_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'sasa_cuentabancaria_c',
          'label' => 'LBL_SASA_CUENTABANCARIA_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'sasa_compartimiento_c',
          'label' => 'LBL_SASA_COMPARTIMIENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        13 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);