<?php
 // created: 2018-07-06 21:58:47
$dictionary['Call']['fields']['date_entered']['audited']=true;
$dictionary['Call']['fields']['date_entered']['comments']='Date record created';
$dictionary['Call']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Call']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Call']['fields']['date_entered']['calculated']=false;
$dictionary['Call']['fields']['date_entered']['enable_range_search']='1';

 ?>