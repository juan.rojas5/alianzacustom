<?php
$dependencies['Leads']['SetOptionDepartamento22'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Meta")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Guamal","Granada","San Martin","Puerto Rico","Acacias","Barranca de Upia","Cabuyaro","Castilla la Nueva","Cubarral","Cumaral","El Calvario","El Castillo","El Dorado","Fuente de Oro","La Macarena","Lejanias","Mapiripan","Mesetas","Puerto Concordia","Puerto Gaitan","Puerto Lleras","Puerto Lopez","Restrepo","San Carlos de Guaroa","San Juan de Arama","San Juanito","Uribe","Villavicencio","Vista Hermosa")',
           'labels' => 'createList("","Guamal","Granada","San Martín","Puerto Rico","Acacias","Barranca de Upía","Cabuyaro","Castilla la Nueva","Cubarral","Cumaral","El Calvario","El Castillo","El Dorado","Fuente de Oro","La Macarena","Lejanías","Mapiripán","Mesetas","Puerto Concordia","Puerto Gaitán","Puerto Lleras","Puerto López","Restrepo","San Carlos de Guaroa","San Juan de Arama","San Juanito","Uribe","Villavicencio","Vista Hermosa")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
