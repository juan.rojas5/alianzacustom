<?php
 // created: 2022-10-19 15:53:51
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['len']='255';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['audited']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['massupdate']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['hidemassupdate']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['importable']='false';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['unified_search']=false;
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['calculated']='1';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['formula']='concat($sasa_motivo_c," - ",getDropdownValue("sasa_categoria_c_list",$sasa_categoria_c)," - ",getDropdownValue("sasa_motivo_c_list",$sasa_proceso_c)," - ",getDropdownValue("sasa_detalle_c_list",$sasa_detalle_c))';
$dictionary['sasa_sasa_tipificaciones']['fields']['name']['enforced']=true;

 ?>