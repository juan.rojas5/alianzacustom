<?php
$viewdefs['Purchases'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'link' => true,
                'label' => 'LBL_NAME',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              1 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_dvencargo_c',
                'label' => 'LBL_SASA_DVENCARGO_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'account_name',
                'label' => 'LBL_ACCOUNT_NAME',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'sasa_ejecutivocomercial_c',
                'label' => 'LBL_SASA_EJECUTIVOCOMERCIAL_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_estado_c',
                'label' => 'LBL_SASA_ESTADO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_codigocomercial_c',
                'label' => 'LBL_SASA_CODIGOCOMERCIAL_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_estadonegocio_c',
                'label' => 'LBL_SASA_ESTADONEGOCIO_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_codigonegocio_c',
                'label' => 'LBL_SASA_CODIGONEGOCIO_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_nombredenegocio_c',
                'label' => 'LBL_SASA_NOMBREDENEGOCIO_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_asistentedegestion_c',
                'label' => 'LBL_SASA_ASISTENTEDEGESTION_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_fechacreacion_c',
                'label' => 'LBL_SASA_FECHACREACION_C',
                'enabled' => true,
                'readonly' => '1',
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_fechacancelacion_c',
                'label' => 'LBL_SASA_FECHACANCELACION_C',
                'enabled' => true,
                'readonly' => false,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'date_entered',
                'type' => 'datetime',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              14 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_LIST_ASSIGNED_USER',
                'id' => 'ASSIGNED_USER_ID',
                'enabled' => true,
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAMS',
                'enabled' => true,
                'id' => 'TEAM_ID',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              18 => 
              array (
                'name' => 'end_date',
                'label' => 'LBL_END_DATE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
