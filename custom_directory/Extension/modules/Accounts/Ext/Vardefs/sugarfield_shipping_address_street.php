<?php
 // created: 2018-06-26 00:39:53
$dictionary['Account']['fields']['shipping_address_street']['required']=false;
$dictionary['Account']['fields']['shipping_address_street']['audited']=true;
$dictionary['Account']['fields']['shipping_address_street']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.34',
  'searchable' => true,
);
$dictionary['Account']['fields']['shipping_address_street']['calculated']=false;
$dictionary['Account']['fields']['shipping_address_street']['rows']='4';
$dictionary['Account']['fields']['shipping_address_street']['cols']='20';
$dictionary['Account']['fields']['shipping_address_street']['reportable']=false;

 ?>