<?php 
$app_list_strings['sasa_categoria_c_list'] = array (
  '' => '',
  'trato inadecuado' => 'Trato Inadecuado',
  'inconsistencias' => 'Inconsistencias',
  'incumplimiento' => 'Incumplimiento',
  'informacion' => 'Información',
  'Aplazamiento' => 'Aplazamiento',
  'fallas y_o errores' => 'Fallas y/o Errores',
  'datos_personales' => 'Datos Personales',
  'reporte ante centrales' => 'Reportes ante Centrales',
  'otros_motivos' => 'Otros Motivos',
  'desvalorizacion_perdidas_diferencias' => 'Desvalorización Perdidas y Diferencias',
  'negativao_demora_en_el_pago' => 'Negativa o demora en el pago',
  'suplantacion_y_o_fraude' => 'Suplantación y/o Fraude',
);