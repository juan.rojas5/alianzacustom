<?php

$job_strings[] = 'DeletePmseBpmFlowRecords';

function DeletePmseBpmFlowRecords()
{
    $GLOBALS['log']->security("\n\n");
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Inicio TP Delete pmse_bpm_flow records. ".date("Y-m-d h:i:s"));
    $GLOBALS['log']->security("**********************************************************************************");

    $GLOBALS['log']->security("Ejecutando proceso de eliminación para registros de pmse_bpm_flow...");
    delete_rows_pmse_bpm_flow();
    
    $GLOBALS['log']->security("**********************************************************************************");
    $GLOBALS['log']->security("Fin TP Delete pmse_bpm_flow records. ".date('Y-m-d H:i:s'));
    $GLOBALS['log']->security("**********************************************************************************");

    return true;
}


function delete_rows_pmse_bpm_flow()
{
    $antes = get_pmse_bpm_flow();
    
    $query1 = "DELETE FROM pmse_bpm_flow 
    WHERE cas_flow_status = 'TERMINATED'
    AND date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $query2 = "DELETE FROM pmse_bpm_flow
    WHERE cas_flow_status = 'CLOSED'
    AND date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $resultQuery1 = $GLOBALS['db']->query($query1);
    $resultQuery2 = $GLOBALS['db']->query($query2);

    $despues = get_pmse_bpm_flow();
}

// Consultas
// pmse_bpm_flow

function get_pmse_bpm_flow()
{
    $query3 = "SELECT COUNT(p.id) AS total 
    FROM pmse_bpm_flow AS p 
    WHERE p.cas_flow_status = 'TERMINATED'
    AND p.date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";
    
    $query4 = "SELECT COUNT(p.id) AS total 
    FROM pmse_bpm_flow AS p 
    WHERE p.cas_flow_status = 'CLOSED'
    AND p.date_entered < date_sub( curdate(), INTERVAL 3 MONTH )";

    $resultQuery3 = $GLOBALS['db']->query($query3);
    $resultQuery4 = $GLOBALS['db']->query($query4);

    while( $row = $GLOBALS['db']->fetchByAssoc($resultQuery3) )
    {
        $re = !empty($row['total']) ? $row['total'] : 0; // result
        $GLOBALS['log']->security("Total pmse_bpm_flow con estado terminated: ".$re);
    }
    
    while( $row = $GLOBALS['db']->fetchByAssoc($resultQuery4) )
    {
        $re = !empty($row['total']) ? $row['total'] : 0; // result
        $GLOBALS['log']->security("Total pmse_bpm_flow con estado closed: ".$re);
    }
}