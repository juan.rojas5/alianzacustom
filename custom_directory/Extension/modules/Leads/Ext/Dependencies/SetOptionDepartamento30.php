<?php
$dependencies['Leads']['SetOptionDepartamento30'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Tolima")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Suarez","Alpujarra","Alvarado","Ambalema","Anzoategui","Armero","Ataco","Cajamarca","Carmen de Apicala","Casabianca","Chaparral","Coello","Coyaima","Cunday","Dolores","Espinal","Falan","Flandes","Fresno","Guamo","Herveo","Honda","Ibague","Icononzo","Lerida","Libano","Mariquita","Melgar","Murillo","Natagaima","Ortega","Palocabildo","Piedras","Planadas","Prado","Purificacion","Rio Blanco","Roncesvalles","Rovira","Saldana","San Antonio","San Luis","Santa Isabel","Valle de San Juan","Venadillo","Villahermosa","Villarrica")',
           'labels' => 'createList("","Suárez","Alpujarra","Alvarado","Ambalema","Anzoátegui","Armero","Ataco","Cajamarca","Carmen de Apicala","Casabianca","Chaparral","Coello","Coyaima","Cunday","Dolores","Espinal","Falan","Flandes","Fresno","Guamo","Herveo","Honda","Ibagué","Icononzo","Lérida","Líbano","Mariquita","Melgar","Murillo","Natagaima","Ortega","Palocabildo","Piedras","Planadas","Prado","Purificación","Rio Blanco","Roncesvalles","Rovira","Saldaña","San Antonio","San Luis","Santa Isabel","Valle de San Juan","Venadillo","Villahermosa","Villarrica")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
