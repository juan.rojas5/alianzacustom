<?php
 // created: 2023-02-01 19:15:18
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['labelValue']='asistente de gestión';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_asistentedegestion_c']['readonly_formula']='';

 ?>