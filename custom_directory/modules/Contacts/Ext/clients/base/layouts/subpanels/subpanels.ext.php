<?php
// WARNING: The contents of this file are auto-generated.


 // created: 2012-06-13 21:08:52
$layout_defs["Contacts"]["subpanel_setup"]['fbsg_ccintegrationlog_contacts'] = array (
  'order' => 100,
  'module' => 'fbsg_CCIntegrationLog',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'CC Integration Log',
  'get_subpanel_data' => 'fbsg_ccintegrationlog_contacts',
  'top_buttons' =>
  array (
  ),
);


$viewdefs["Contacts"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_CCILOG_SUBPANEL',
  'context' => array(
    'link' => 'fbsg_ccintegrationlog_contacts',
  ),
);


$viewdefs["Contacts"]["base"]["layout"]["subpanels"]["components"][] = array(
  'layout' => 'subpanel',
  'label' => 'LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE',
  'context' => array(
    'link' => 'prospect_list_contacts',
  ),
);


//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'fbsg_ccintegrationlog_contacts',
  'view' => 'subpanel-for-contacts-fbsg_ccintegrationlog_contacts',
);


//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'leads',
  'view' => 'subpanel-for-contacts-leads',
);


//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'opportunities',
  'view' => 'subpanel-for-contacts-opportunities',
);


//auto-generated file DO NOT EDIT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'prospect_list_contacts',
  'view' => 'subpanel-for-contacts-prospect_list_contacts',
);
