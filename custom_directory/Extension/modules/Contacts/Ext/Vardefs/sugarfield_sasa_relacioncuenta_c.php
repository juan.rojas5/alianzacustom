<?php
 // created: 2022-11-16 02:16:42
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['labelValue']='Relación con la Cuenta';
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['dependency']='and(equal(related($accounts,"sasa_tipopersona_c"),"Juridica"),
equal($sasa_parentescorelacion_c,"Otro")
)';
$dictionary['Contact']['fields']['sasa_relacioncuenta_c']['visibility_grid']='';

 ?>