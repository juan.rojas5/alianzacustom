<?php
 // created: 2018-07-11 20:52:46
$dictionary['Contact']['fields']['alt_address_postalcode']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['alt_address_postalcode']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_postalcode']['dependency']='equal($sasa_parentescorelacion_c,"Otro")';

 ?>