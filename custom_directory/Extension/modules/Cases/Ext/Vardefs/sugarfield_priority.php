<?php
 // created: 2022-08-26 16:54:58
$dictionary['Case']['fields']['priority']['massupdate']=true;
$dictionary['Case']['fields']['priority']['hidemassupdate']=false;
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['calculated']=false;
$dictionary['Case']['fields']['priority']['dependency']='equal($sasa_tipodesolicitud_c,"quejas")';

 ?>