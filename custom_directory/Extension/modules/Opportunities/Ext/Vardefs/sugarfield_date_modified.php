<?php
 // created: 2018-06-20 13:52:16
$dictionary['Opportunity']['fields']['date_modified']['audited']=true;
$dictionary['Opportunity']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Opportunity']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_modified']['calculated']=false;
$dictionary['Opportunity']['fields']['date_modified']['enable_range_search']='1';

 ?>