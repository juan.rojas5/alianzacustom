<?php
// created: 2018-08-11 23:23:05
$role_dropdown_filters['sasa_segmentocuentas_c_list'] = array (
  '' => true,
  'Preferenciales' => true,
  'Renta Alta' => true,
  'Banca Privada' => true,
  'PYME' => true,
  'Empresarial' => true,
  'Banca Corporativa' => true,
  'Institucional' => true,
  'No Registra' => false,
);