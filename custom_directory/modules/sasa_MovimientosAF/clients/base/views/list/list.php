<?php
$module_name = 'sasa_MovimientosAF';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosaf_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
                'enabled' => true,
                'id' => 'SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1SASA_PRODUCTOSAFAV_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'sasa_nombrefondo_c',
                'label' => 'LBL_SASA_NOMBREFONDO_C',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'accounts_sasa_movimientosaf_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_SASA_MOVIMIENTOSAF_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sasa_nombresubcuenta_c',
                'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sasa_fecha_c',
                'label' => 'LBL_SASA_FECHA_C',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'sasa_motivo_c',
                'label' => 'LBL_SASA_MOTIVO_C',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'sasa_valor_c',
                'label' => 'LBL_SASA_VALOR_C',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'sasa_formapago_c',
                'label' => 'LBL_SASA_FORMAPAGO_C',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'sasa_concepto_c',
                'label' => 'LBL_SASA_CONCEPTO_C',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'sasa_cuentabancaria_c',
                'label' => 'LBL_SASA_CUENTABANCARIA_C',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              14 => 
              array (
                'name' => 'sasa_compartimiento_c',
                'label' => 'LBL_SASA_COMPARTIMIENTO_C',
                'enabled' => true,
                'default' => true,
              ),
              15 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              16 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              17 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              18 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => true,
              ),
              19 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              20 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
