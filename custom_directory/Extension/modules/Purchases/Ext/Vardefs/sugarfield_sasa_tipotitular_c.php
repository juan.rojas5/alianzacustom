<?php
 // created: 2023-02-01 15:52:21
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['labelValue']='tipo titular';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_tipotitular_c']['readonly_formula']='';

 ?>