<?php
// created: 2023-04-25 03:53:11
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Név által létrehozva',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio por cliente Lista',
  'LBL_MODULE_NAME' => 'Unidades de negocio por cliente',
  'LBL_MODULE_TITLE' => 'Unidades de negocio por cliente',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidad de negocio por cliente',
  'LBL_HOMEPAGE_TITLE' => 'Saját Unidades de negocio por cliente',
  'LNK_NEW_RECORD' => 'Új létrehozása Unidad de negocio por cliente',
  'LNK_LIST' => 'Megtekintés Unidades de negocio por cliente',
  'LNK_IMPORT_SASA1_UNIDADES_DE_NEGOCIO_POR_' => 'Importar Unidades de negocio por cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Unidad de negocio por cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__SUBPANEL_TITLE' => 'Unidades de negocio por cliente',
  'LBL_NEW_FORM_TITLE' => 'Új Unidad de negocio por cliente',
  'LNK_IMPORT_VCARD' => 'Importar Unidad de negocio por cliente vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio por cliente',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidad de negocio por cliente record by importing a vCard from your file system.',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio por cliente Panel de enfoque',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__RECORD_DASHBOARD' => 'Unidades de negocio por cliente Cuadro de mando del registro',
);