<?php

$hook_array['after_save'][] = Array(
  //Processing index. For sorting the array.
  1,

  //Label. A string value to identify the hook.
  'meetings_deleted_no_asigned',

  //The PHP file where your class is located.
  'custom/modules/Meetings/meetings_deleted_no_asigned.php',

  //The class the method is in.
  'meetings_deleted_no_asigned_class',

  //The method to call.
  'after_save'
);

?>