<?php
 // created: 2022-09-09 21:58:07
$dictionary['Contact']['fields']['sasa_razonsocial_c']['labelValue']='Razon Social';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['sasa_razonsocial_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['dependency']='equal($sasa_tipopersona_c,"Juridica")';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_razonsocial_c']['readonly_formula']='';

 ?>