<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['DEFAULT_REPORT_TITLE_15'] = 'Visitas por Equipo por Usuario';
$mod_strings['DEFAULT_REPORT_TITLE_48'] = 'Visitas Abiertas';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['DEFAULT_REPORT_TITLE_7'] = 'PQRs Abiertos por Usuario por Estado';
$mod_strings['DEFAULT_REPORT_TITLE_8'] = 'PQRs Abiertos por Mes por Usuario';
$mod_strings['DEFAULT_REPORT_TITLE_9'] = 'PQRs Abiertos por Prioridad por Usuario';
$mod_strings['DEFAULT_REPORT_TITLE_10'] = 'Nuevos PQRs por Mes';
$mod_strings['LBL_REVENUELINEITEMS'] = 'Líneas de Ingreso';
