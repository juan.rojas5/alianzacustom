<?php
// created: 2023-04-25 03:53:11
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'ID de l&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'ID d&#39;usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat per',
  'LBL_MODIFIED_ID' => 'Modificat per ID',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat per ID',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Suprimit',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Edita',
  'LBL_REMOVE' => 'Suprimir',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Creat pel nom',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'Unidades de negocio por cliente Llista',
  'LBL_MODULE_NAME' => 'Unidades de negocio por cliente',
  'LBL_MODULE_TITLE' => 'Unidades de negocio por cliente',
  'LBL_MODULE_NAME_SINGULAR' => 'Unidad de negocio por cliente',
  'LBL_HOMEPAGE_TITLE' => 'Meu Unidades de negocio por cliente',
  'LNK_NEW_RECORD' => 'Crea Unidad de negocio por cliente',
  'LNK_LIST' => 'Vista Unidades de negocio por cliente',
  'LNK_IMPORT_SASA1_UNIDADES_DE_NEGOCIO_POR_' => 'Importar Unidades de negocio por cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Unidad de negocio por cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__SUBPANEL_TITLE' => 'Unidades de negocio por cliente',
  'LBL_NEW_FORM_TITLE' => 'Nou Unidad de negocio por cliente',
  'LNK_IMPORT_VCARD' => 'Importar Unidad de negocio por cliente vCard',
  'LBL_IMPORT' => 'Importar Unidades de negocio por cliente',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Unidad de negocio por cliente record by importing a vCard from your file system.',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__FOCUS_DRAWER_DASHBOARD' => 'Unidades de negocio por cliente Panel de enfoque',
  'LBL_SASA1_UNIDADES_DE_NEGOCIO_POR__RECORD_DASHBOARD' => 'Unidades de negocio por cliente Cuadro de mando del registro',
);