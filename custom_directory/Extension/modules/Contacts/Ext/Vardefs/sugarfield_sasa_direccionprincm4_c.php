<?php
 // created: 2022-11-16 02:16:43
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['labelValue']='Dirección Principal - M4';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0,35',
  'searchable' => true,
);
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['enforced']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['dependency']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['required_formula']='';
$dictionary['Contact']['fields']['sasa_direccionprincm4_c']['readonly_formula']='';

 ?>