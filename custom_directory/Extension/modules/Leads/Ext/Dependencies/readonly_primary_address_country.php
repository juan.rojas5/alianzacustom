<?php

$dependencies['Leads']['readonly_primary_address_country'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_country'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_country',
        'value' => 'true'
      )
    )
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);

