<?php
 // created: 2018-08-24 14:04:44

$app_list_strings['sasa_actividadeconomica_c_list']=array (
  '' => '',
  'Agroindustria' => 'Agroindustria',
  'Comercial' => 'Comercial',
  'Comunicacion' => 'Comunicación',
  'Construccion' => 'Construcción',
  'Educacion' => 'Educación',
  'Financiero' => 'Financiero',
  'Industrial' => 'Industrial',
  'Salud' => 'Salud',
  'Servicios' => 'Servicios',
  'Transporte' => 'Transporte',
  'Otros' => 'Otros',
  'No Registra' => 'No Registra',
);