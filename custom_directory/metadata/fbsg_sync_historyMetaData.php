<?php
// created: 2018-07-19 23:16:12
$dictionary['fbsg_sync_history'] = array (
  'relationships' => 
  array (
  ),
  'table' => 'fbsg_sync_history',
  'fields' => 
  array (
    'cc_list_id' => 
    array (
      'name' => 'cc_list_id',
      'type' => 'id',
      'isnull' => 'false',
      'required' => true,
    ),
    'last_updated_link' => 
    array (
      'name' => 'last_updated_link',
      'type' => 'varchar',
      'len' => 255,
    ),
    'updated_since' => 
    array (
      'name' => 'updated_since',
      'type' => 'datetime',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_fbsg_sync_history_id',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'cc_list_id',
      ),
    ),
  ),
);