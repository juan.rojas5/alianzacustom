<?php
$dependencies['Leads']['SetOptionDepartamento25'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Putumayo")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Santiago","Colon","San Francisco","Leguizamo","Mocoa","Orito","Puerto Asis","Puerto Caicedo","Puerto Guzman","San Miguel","Sibundoy","Valle de Guamez","Villagarzon")',
           'labels' => 'createList("","Santiago","Colón","San Francisco","Leguízamo","Mocoa","Orito","Puerto Asís","Puerto Caicedo","Puerto Guzmán","San Miguel","Sibundoy","Valle de Guamez","Villagarzón")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
