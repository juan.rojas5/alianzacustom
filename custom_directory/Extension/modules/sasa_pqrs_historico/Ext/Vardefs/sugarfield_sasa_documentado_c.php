<?php
 // created: 2022-11-18 20:47:52
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['labelValue']='¿Documentado?';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['enforced']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['dependency']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['required_formula']='';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['readonly']='1';
$dictionary['sasa_pqrs_historico']['fields']['sasa_documentado_c']['readonly_formula']='';

 ?>