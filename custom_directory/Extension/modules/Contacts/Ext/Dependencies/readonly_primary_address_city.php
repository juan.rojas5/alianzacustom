<?php

$dependencies['Contacts']['readonly_primary_address_city'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('primary_address_city'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'equal($sasa_parentescorelacion_c,"Otro")'
      )
    ),
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'primary_address_city',
        'value' => 'true'
      )
    ),
  ),
  //notActions is a list of actions to fire when the trigger is false
  'notActions' => array(
    
  )
);

