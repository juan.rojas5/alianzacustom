<?php
// created: 2023-02-17 20:29:36
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa2_Unidades_de_negocio',
  'bean_name' => 'sasa2_Unidades_de_negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'name',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unid7bfcnegocio_ida"] = array (
  'name' => 'sasa2_unid7bfcnegocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
