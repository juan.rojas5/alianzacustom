<?php
 // created: 2022-06-01 15:52:36
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['labelValue']='Respuesta SFC API';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['enforced']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['dependency']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['required_formula']='';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['readonly']='1';
$dictionary['Case']['fields']['sasa_sfc_response_api_c']['readonly_formula']='';

 ?>