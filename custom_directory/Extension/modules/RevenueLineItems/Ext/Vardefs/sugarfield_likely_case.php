<?php
 // created: 2018-07-14 20:15:42
$dictionary['RevenueLineItem']['fields']['likely_case']['default']='';
$dictionary['RevenueLineItem']['fields']['likely_case']['len']=26;
$dictionary['RevenueLineItem']['fields']['likely_case']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['likely_case']['calculated']='1';
$dictionary['RevenueLineItem']['fields']['likely_case']['enforced']=false;
$dictionary['RevenueLineItem']['fields']['likely_case']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);
$dictionary['RevenueLineItem']['fields']['likely_case']['enable_range_search']=false;

 ?>