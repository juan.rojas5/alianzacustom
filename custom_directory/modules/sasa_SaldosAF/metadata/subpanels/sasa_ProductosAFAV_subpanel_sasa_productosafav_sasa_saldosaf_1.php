<?php
// created: 2019-01-04 21:57:41
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'sasa_encargo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_ENCARGO_C',
    'width' => 10,
  ),
  'sasa_productosafav_sasa_saldosaf_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
    'id' => 'SASA_PRODUCTOSAFAV_SASA_SALDOSAF_1SASA_PRODUCTOSAFAV_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'sasa_ProductosAFAV',
    'target_record_key' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
  ),
  'sasa_categoria_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SASA_CATEGORIA_C',
    'width' => 10,
  ),
  'accounts_sasa_saldosaf_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAF_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SASA_SALDOSAF_1ACCOUNTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_sasa_saldosaf_1accounts_ida',
  ),
  'sasa_saldoactual_c' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_SASA_SALDOACTUAL_C',
    'width' => 10,
  ),
  'sasa_fechasaldo_c' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_SASA_FECHASALDO_C',
    'width' => 10,
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);