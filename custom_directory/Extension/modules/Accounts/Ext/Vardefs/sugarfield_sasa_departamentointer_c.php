<?php
 // created: 2022-10-25 23:16:55
$dictionary['Account']['fields']['sasa_departamentointer_c']['labelValue']='Departamento internacional';
$dictionary['Account']['fields']['sasa_departamentointer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_departamentointer_c']['enforced']='';
$dictionary['Account']['fields']['sasa_departamentointer_c']['dependency']='and(not(equal($sasa_pais_c,"Colombia")),not(equal($sasa_pais_c,"")),not(equal($sasa_pais_c,"No Registra")))';

 ?>