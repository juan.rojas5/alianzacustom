<?php
// created: 2023-04-25 03:53:07
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_EXPORT_CREATED_BY_NAME' => 'Loodud nimi',
  'LBL_COMMENTLOG' => 'Comment Log',
  'LBL_LIST_FORM_TITLE' => 'PQRs Histórico Loend',
  'LBL_MODULE_NAME' => 'PQRs Histórico',
  'LBL_MODULE_TITLE' => 'PQRs Histórico',
  'LBL_MODULE_NAME_SINGULAR' => 'PQRs Histórico',
  'LBL_HOMEPAGE_TITLE' => 'Minu PQRs Histórico',
  'LNK_NEW_RECORD' => 'Loo PQRs Histórico',
  'LNK_LIST' => 'Vaade PQRs Histórico',
  'LNK_IMPORT_SASA_PQRS_HISTORICO' => 'Importar PQRs Histórico',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi PQRs Histórico',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_SASA_PQRS_HISTORICO_SUBPANEL_TITLE' => 'PQRs Histórico',
  'LBL_NEW_FORM_TITLE' => 'Uus PQRs Histórico',
  'LNK_IMPORT_VCARD' => 'Importar PQRs Histórico vCard',
  'LBL_IMPORT' => 'Importar PQRs Histórico',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new PQRs Histórico record by importing a vCard from your file system.',
  'LBL_SASA_PQRS_HISTORICO_FOCUS_DRAWER_DASHBOARD' => 'PQRs Histórico Panel de enfoque',
  'LBL_SASA_PQRS_HISTORICO_RECORD_DASHBOARD' => 'PQRs Histórico Cuadro de mando del registro',
);