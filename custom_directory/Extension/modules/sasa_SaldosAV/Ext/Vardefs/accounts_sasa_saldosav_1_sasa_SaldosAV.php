<?php
// created: 2019-01-04 21:37:35
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1"] = array (
  'name' => 'accounts_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1_name"] = array (
  'name' => 'accounts_sasa_saldosav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link' => 'accounts_sasa_saldosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1accounts_ida"] = array (
  'name' => 'accounts_sasa_saldosav_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE_ID',
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link' => 'accounts_sasa_saldosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
