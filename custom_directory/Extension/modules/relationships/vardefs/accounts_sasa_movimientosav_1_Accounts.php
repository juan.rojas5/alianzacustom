<?php
// created: 2019-01-04 21:40:08
$dictionary["Account"]["fields"]["accounts_sasa_movimientosav_1"] = array (
  'name' => 'accounts_sasa_movimientosav_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_movimientosav_1',
  'source' => 'non-db',
  'module' => 'sasa_MovimientosAV',
  'bean_name' => 'sasa_MovimientosAV',
  'vname' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAV_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_sasa_movimientosav_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
