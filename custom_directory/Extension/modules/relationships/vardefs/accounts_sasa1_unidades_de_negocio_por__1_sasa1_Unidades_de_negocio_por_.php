<?php
// created: 2023-02-20 21:38:36
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1accounts_ida"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
