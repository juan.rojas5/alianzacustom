<?php
 // created: 2018-08-18 14:11:48
$dictionary['User']['fields']['sasa_nombrecompleto_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sasa_nombrecompleto_c']['labelValue']='Nombre completo';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['sasa_nombrecompleto_c']['calculated']='true';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['formula']='concat($first_name," ",$last_name)';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['enforced']='true';
$dictionary['User']['fields']['sasa_nombrecompleto_c']['dependency']='';

 ?>