<?php
 // created: 2018-06-20 17:34:50
$dictionary['RevenueLineItem']['fields']['date_entered']['audited']=true;
$dictionary['RevenueLineItem']['fields']['date_entered']['comments']='Date record created';
$dictionary['RevenueLineItem']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['RevenueLineItem']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['date_entered']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['date_entered']['enable_range_search']='1';

 ?>