<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'sasa_categorias_sasa_productosafav_1' => 
  array (
    'name' => 'sasa_categorias_sasa_productosafav_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_categorias_sasa_productosafav_1' => 
      array (
        'lhs_module' => 'sasa_Categorias',
        'lhs_table' => 'sasa_categorias',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_ProductosAFAV',
        'rhs_table' => 'sasa_productosafav',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_categorias_sasa_productosafav_1_c',
        'join_key_lhs' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
        'join_key_rhs' => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
      ),
    ),
    'table' => 'sasa_categorias_sasa_productosafav_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_categorias_sasa_productosafav_1sasa_categorias_ida' => 
      array (
        'name' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
        'type' => 'id',
      ),
      'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb' => 
      array (
        'name' => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_categorias_sasa_productosafav_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_categorias_sasa_productosafav_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_categorias_sasa_productosafav_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_categorias_sasa_productosafav_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_Categorias',
    'lhs_table' => 'sasa_categorias',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_categorias_sasa_productosafav_1_c',
    'join_key_lhs' => 'sasa_categorias_sasa_productosafav_1sasa_categorias_ida',
    'join_key_rhs' => 'sasa_categorias_sasa_productosafav_1sasa_productosafav_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_categorias_sasa_productosafav_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasa_saldosaf_1' => 
  array (
    'name' => 'sasa_productosafav_sasa_saldosaf_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasa_saldosaf_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_SaldosAF',
        'rhs_table' => 'sasa_saldosaf',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasa_saldosaf_1_c',
        'join_key_lhs' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
        'join_key_rhs' => 'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasa_saldosaf_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida' => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
        'type' => 'id',
      ),
      'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb' => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosaf_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosaf_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosaf_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosaf_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_SaldosAF',
    'rhs_table' => 'sasa_saldosaf',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasa_saldosaf_1_c',
    'join_key_lhs' => 'sasa_productosafav_sasa_saldosaf_1sasa_productosafav_ida',
    'join_key_rhs' => 'sasa_productosafav_sasa_saldosaf_1sasa_saldosaf_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasa_saldosaf_1',
    'rhs_subpanel' => 'ForSasa_productosafavSasa_productosafav_sasa_saldosaf_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasa_saldosav_1' => 
  array (
    'name' => 'sasa_productosafav_sasa_saldosav_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasa_saldosav_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_SaldosAV',
        'rhs_table' => 'sasa_saldosav',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasa_saldosav_1_c',
        'join_key_lhs' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
        'join_key_rhs' => 'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasa_saldosav_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida' => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
        'type' => 'id',
      ),
      'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb' => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosav_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosav_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_saldosav_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasa_saldosav_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_SaldosAV',
    'rhs_table' => 'sasa_saldosav',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasa_saldosav_1_c',
    'join_key_lhs' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
    'join_key_rhs' => 'sasa_productosafav_sasa_saldosav_1sasa_saldosav_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasa_saldosav_1',
    'rhs_subpanel' => 'ForSasa_productosafavSasa_productosafav_sasa_saldosav_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasa_movimientosaf_1' => 
  array (
    'name' => 'sasa_productosafav_sasa_movimientosaf_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasa_movimientosaf_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_MovimientosAF',
        'rhs_table' => 'sasa_movimientosaf',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasa_movimientosaf_1_c',
        'join_key_lhs' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
        'join_key_rhs' => 'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasa_movimientosaf_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida' => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
        'type' => 'id',
      ),
      'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb' => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosaf_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosaf_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosaf_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosaf_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_MovimientosAF',
    'rhs_table' => 'sasa_movimientosaf',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasa_movimientosaf_1_c',
    'join_key_lhs' => 'sasa_productosafav_sasa_movimientosaf_1sasa_productosafav_ida',
    'join_key_rhs' => 'sasa_productosafav_sasa_movimientosaf_1sasa_movimientosaf_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasa_movimientosaf_1',
    'rhs_subpanel' => 'ForSasa_productosafavSasa_productosafav_sasa_movimientosaf_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasa_movimientosav_1' => 
  array (
    'name' => 'sasa_productosafav_sasa_movimientosav_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasa_movimientosav_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_MovimientosAV',
        'rhs_table' => 'sasa_movimientosav',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasa_movimientosav_1_c',
        'join_key_lhs' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
        'join_key_rhs' => 'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasa_movimientosav_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida' => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
        'type' => 'id',
      ),
      'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb' => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosav_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosav_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosav_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosav_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_MovimientosAV',
    'rhs_table' => 'sasa_movimientosav',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasa_movimientosav_1_c',
    'join_key_lhs' => 'sasa_productosafav_sasa_movimientosav_1sasa_productosafav_ida',
    'join_key_rhs' => 'sasa_productosafav_sasa_movimientosav_1sasa_movimientosav_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasa_movimientosav_1',
    'rhs_subpanel' => 'ForSasa_productosafavSasa_productosafav_sasa_movimientosav_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasa_movimientosavdivisas_1' => 
  array (
    'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasa_movimientosavdivisas_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_MovimientosAVDivisas',
        'rhs_table' => 'sasa_movimientosavdivisas',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasa_movimientosavdivisas_1_c',
        'join_key_lhs' => 'sasa_produ845etosafav_ida',
        'join_key_rhs' => 'sasa_produd707divisas_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasa_movimientosavdivisas_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_produ845etosafav_ida' => 
      array (
        'name' => 'sasa_produ845etosafav_ida',
        'type' => 'id',
      ),
      'sasa_produd707divisas_idb' => 
      array (
        'name' => 'sasa_produd707divisas_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_produ845etosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasa_movimientosavdivisas_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_produd707divisas_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasa_movimientosavdivisas_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_produd707divisas_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_MovimientosAVDivisas',
    'rhs_table' => 'sasa_movimientosavdivisas',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasa_movimientosavdivisas_1_c',
    'join_key_lhs' => 'sasa_produ845etosafav_ida',
    'join_key_rhs' => 'sasa_produd707divisas_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasa_movimientosavdivisas_1',
    'rhs_subpanel' => 'ForSasa_productosafavSasa_productosafav_sasa_movimientosavdivisas_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_sasap_presupuestoxproducto_1' => 
  array (
    'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa_productosafav_sasap_presupuestoxproducto_1' => 
      array (
        'lhs_module' => 'sasa_ProductosAFAV',
        'lhs_table' => 'sasa_productosafav',
        'lhs_key' => 'id',
        'rhs_module' => 'sasaP_PresupuestoxProducto',
        'rhs_table' => 'sasap_presupuestoxproducto',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa_productosafav_sasap_presupuestoxproducto_1_c',
        'join_key_lhs' => 'sasa_produff64tosafav_ida',
        'join_key_rhs' => 'sasa_produ6f56roducto_idb',
      ),
    ),
    'table' => 'sasa_productosafav_sasap_presupuestoxproducto_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa_produff64tosafav_ida' => 
      array (
        'name' => 'sasa_produff64tosafav_ida',
        'type' => 'id',
      ),
      'sasa_produ6f56roducto_idb' => 
      array (
        'name' => 'sasa_produ6f56roducto_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_produff64tosafav_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa_productosafav_sasap_presupuestoxproducto_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa_produ6f56roducto_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa_productosafav_sasap_presupuestoxproducto_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa_produ6f56roducto_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'sasaP_PresupuestoxProducto',
    'rhs_table' => 'sasap_presupuestoxproducto',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa_productosafav_sasap_presupuestoxproducto_1_c',
    'join_key_lhs' => 'sasa_produff64tosafav_ida',
    'join_key_rhs' => 'sasa_produ6f56roducto_idb',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_sasap_presupuestoxproducto_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_modified_user' => 
  array (
    'name' => 'sasa_productosafav_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_productosafav_created_by' => 
  array (
    'name' => 'sasa_productosafav_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_productosafav_activities' => 
  array (
    'name' => 'sasa_productosafav_activities',
    'lhs_module' => 'sasa_ProductosAFAV',
    'lhs_table' => 'sasa_productosafav',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_ProductosAFAV',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_productosafav_following' => 
  array (
    'name' => 'sasa_productosafav_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'sasa_ProductosAFAV',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_productosafav_favorite' => 
  array (
    'name' => 'sasa_productosafav_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'sasa_ProductosAFAV',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa_productosafav_assigned_user' => 
  array (
    'name' => 'sasa_productosafav_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sasa_productosafav_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sasa2_unidades_de_negocio_sasa_productosafav_1' => 
  array (
    'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'sasa2_unidades_de_negocio_sasa_productosafav_1' => 
      array (
        'lhs_module' => 'sasa2_Unidades_de_negocio',
        'lhs_table' => 'sasa2_unidades_de_negocio',
        'lhs_key' => 'id',
        'rhs_module' => 'sasa_ProductosAFAV',
        'rhs_table' => 'sasa_productosafav',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_c',
        'join_key_lhs' => 'sasa2_unid99b0negocio_ida',
        'join_key_rhs' => 'sasa2_unid7f8btosafav_idb',
      ),
    ),
    'table' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'sasa2_unid99b0negocio_ida' => 
      array (
        'name' => 'sasa2_unid99b0negocio_ida',
        'type' => 'id',
      ),
      'sasa2_unid7f8btosafav_idb' => 
      array (
        'name' => 'sasa2_unid7f8btosafav_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa2_unid99b0negocio_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_sasa2_unidades_de_negocio_sasa_productosafav_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'sasa2_unid7f8btosafav_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'sasa2_unid7f8btosafav_idb',
        ),
      ),
    ),
    'lhs_module' => 'sasa2_Unidades_de_negocio',
    'lhs_table' => 'sasa2_unidades_de_negocio',
    'lhs_key' => 'id',
    'rhs_module' => 'sasa_ProductosAFAV',
    'rhs_table' => 'sasa_productosafav',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'sasa2_unidades_de_negocio_sasa_productosafav_1_c',
    'join_key_lhs' => 'sasa2_unid99b0negocio_ida',
    'join_key_rhs' => 'sasa2_unid7f8btosafav_idb',
    'readonly' => true,
    'relationship_name' => 'sasa2_unidades_de_negocio_sasa_productosafav_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'sasa_productosafav_purchases_1' => 
  array (
    'rhs_label' => 'Compras',
    'lhs_label' => 'Productos AF/AV',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'sasa_ProductosAFAV',
    'rhs_module' => 'Purchases',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'sasa_productosafav_purchases_1',
  ),
);