<?php
 // created: 2018-06-03 16:58:03
$dictionary['Lead']['fields']['date_entered']['audited']=false;
$dictionary['Lead']['fields']['date_entered']['comments']='Date record created';
$dictionary['Lead']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Lead']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Lead']['fields']['date_entered']['reportable']=true;
$dictionary['Lead']['fields']['date_entered']['calculated']=false;
$dictionary['Lead']['fields']['date_entered']['enable_range_search']='1';

 ?>