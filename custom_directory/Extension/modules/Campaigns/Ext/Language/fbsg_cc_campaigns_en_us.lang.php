<?php
$mod_strings['LBL_CCSYNCED'] = 'Sync with Constant Contact';
$mod_strings['LBL_CCID'] = 'Constant Contact ID';
$mod_strings['LBL_DETAILVIEW_CCIPANEL1'] = 'Constant Contact Campaign Details';

$mod_strings['LBL_CC_CLICKS'] = 'Clicks';
$mod_strings['LBL_CC_OPENS'] = 'Opens';
$mod_strings['LBL_CC_FORWARDS'] = 'Forwards';
$mod_strings['LBL_CC_BOUNCES'] = 'Bounces';
$mod_strings['LBL_CC_OPTOUTS'] = 'Optouts';
$mod_strings['LBL_CC_SPAMREPORTS'] = 'Spam Reports';
?>
