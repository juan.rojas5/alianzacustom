<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_categoria_c.php

 // created: 2018-11-21 21:46:09
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['importable']='false';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['calculated']='true';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['formula']='related($sasa_productosafav_sasa_saldosav_1,"sasa_categorias_sasa_productosafav_1_name")';
$dictionary['sasa_SaldosAV']['fields']['sasa_categoria_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_name.php

 // created: 2018-11-21 21:42:12
$dictionary['sasa_SaldosAV']['fields']['name']['len']='255';
$dictionary['sasa_SaldosAV']['fields']['name']['audited']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['massupdate']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['unified_search']=false;
$dictionary['sasa_SaldosAV']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['sasa_SaldosAV']['fields']['name']['calculated']='true';
$dictionary['sasa_SaldosAV']['fields']['name']['importable']='false';
$dictionary['sasa_SaldosAV']['fields']['name']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAV']['fields']['name']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAV']['fields']['name']['merge_filter']='disabled';
$dictionary['sasa_SaldosAV']['fields']['name']['formula']='$sasa_nemotecnico_c';
$dictionary['sasa_SaldosAV']['fields']['name']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_nombreproducto_c.php

 // created: 2018-11-21 21:47:06
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['importable']='false';
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['duplicate_merge']='disabled';
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['duplicate_merge_dom_value']=0;
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['calculated']='true';
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['formula']='related($sasa_productosafav_sasa_saldosav_1,"sasa_producto_c")';
$dictionary['sasa_SaldosAV']['fields']['sasa_nombreproducto_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sasa_productosafav_sasa_saldosav_1_sasa_SaldosAV.php

// created: 2019-01-04 21:26:58
$dictionary["sasa_SaldosAV"]["fields"]["sasa_productosafav_sasa_saldosav_1"] = array (
  'name' => 'sasa_productosafav_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'sasa_productosafav_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'sasa_ProductosAFAV',
  'bean_name' => 'sasa_ProductosAFAV',
  'side' => 'right',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'id_name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'link-type' => 'one',
);
$dictionary["sasa_SaldosAV"]["fields"]["sasa_productosafav_sasa_saldosav_1_name"] = array (
  'name' => 'sasa_productosafav_sasa_saldosav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
  'save' => true,
  'id_name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_saldosav_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'name',
);
$dictionary["sasa_SaldosAV"]["fields"]["sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida"] = array (
  'name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA_PRODUCTOSAFAV_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE_ID',
  'id_name' => 'sasa_productosafav_sasa_saldosav_1sasa_productosafav_ida',
  'link' => 'sasa_productosafav_sasa_saldosav_1',
  'table' => 'sasa_productosafav',
  'module' => 'sasa_ProductosAFAV',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/accounts_sasa_saldosav_1_sasa_SaldosAV.php

// created: 2019-01-04 21:37:35
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1"] = array (
  'name' => 'accounts_sasa_saldosav_1',
  'type' => 'link',
  'relationship' => 'accounts_sasa_saldosav_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE',
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1_name"] = array (
  'name' => 'accounts_sasa_saldosav_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link' => 'accounts_sasa_saldosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa_SaldosAV"]["fields"]["accounts_sasa_saldosav_1accounts_ida"] = array (
  'name' => 'accounts_sasa_saldosav_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA_SALDOSAV_1_FROM_SASA_SALDOSAV_TITLE_ID',
  'id_name' => 'accounts_sasa_saldosav_1accounts_ida',
  'link' => 'accounts_sasa_saldosav_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_disponiblenominal_c.php

 // created: 2019-01-08 22:14:20
$dictionary['sasa_SaldosAV']['fields']['sasa_disponiblenominal_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_disponiblenominal_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_saldototal_c.php

 // created: 2019-01-08 22:17:26
$dictionary['sasa_SaldosAV']['fields']['sasa_saldototal_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_garantiavlrmercado_c.php

 // created: 2019-01-08 22:15:33
$dictionary['sasa_SaldosAV']['fields']['sasa_garantiavlrmercado_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_garantiavlrmercado_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_nominal_c.php

 // created: 2019-01-08 22:12:57
$dictionary['sasa_SaldosAV']['fields']['sasa_nominal_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_nominal_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_valorfinal_c.php

 // created: 2019-01-08 22:16:14
$dictionary['sasa_SaldosAV']['fields']['sasa_valorfinal_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_valorfinal_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_valormercado_c.php

 // created: 2019-01-08 22:19:14
$dictionary['sasa_SaldosAV']['fields']['sasa_valormercado_c']['precision']=2;
$dictionary['sasa_SaldosAV']['fields']['sasa_valormercado_c']['len']='26';

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_numeroacciones_c.php

 // created: 2019-01-08 22:17:01
$dictionary['sasa_SaldosAV']['fields']['sasa_numeroacciones_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_numeroacciones_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_preciovpn_c.php

 // created: 2019-01-08 22:16:36
$dictionary['sasa_SaldosAV']['fields']['sasa_preciovpn_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_preciovpn_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_valorcompra_c.php

 // created: 2019-01-08 22:13:23
$dictionary['sasa_SaldosAV']['fields']['sasa_valorcompra_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_valorcompra_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_tasafacial_c.php

 // created: 2019-01-08 22:12:35
$dictionary['sasa_SaldosAV']['fields']['sasa_tasafacial_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_tasafacial_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_valorinicial_c.php

 // created: 2019-01-08 22:15:55
$dictionary['sasa_SaldosAV']['fields']['sasa_valorinicial_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_valorinicial_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_garantianominal_c.php

 // created: 2019-01-08 22:15:02
$dictionary['sasa_SaldosAV']['fields']['sasa_garantianominal_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_garantianominal_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_tirvpn_c.php

 // created: 2019-01-08 22:13:59
$dictionary['sasa_SaldosAV']['fields']['sasa_tirvpn_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_tirvpn_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_tirprecio_c.php

 // created: 2019-01-08 22:18:21
$dictionary['sasa_SaldosAV']['fields']['sasa_tirprecio_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_tirprecio_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/sugarfield_sasa_disponiblevlrmercado_c.php

 // created: 2019-01-08 22:14:44
$dictionary['sasa_SaldosAV']['fields']['sasa_disponiblevlrmercado_c']['len']='26';
$dictionary['sasa_SaldosAV']['fields']['sasa_disponiblevlrmercado_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/sasa_SaldosAV/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['sasa_SaldosAV']['full_text_search']=true;

?>
