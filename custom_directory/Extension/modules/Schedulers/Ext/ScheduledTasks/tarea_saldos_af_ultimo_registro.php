<?php
/*
Se requiere para Nutresa que a las 5:30am 
el sistema envíe un correo a todos los usuarios que tengan una tarea
en estado "No iniciada" o una llamada "Planificada", cuya fecha de vencimiento sea el día presente. 

Igualmente se requiere que envíe un correo al usuario registrado en el "Informa a" 
de los usuarios asignados a tareas en estado diferente a "Completada" o llamadas en 
estado diferente a "Realizada", cuya fecha de vencimiento sea el día anterior.
*/
$job_strings[] = 'tarea_saldos_af_ultimo_registro';
function tarea_saldos_af_ultimo_registro(){
  $GLOBALS['log']->security("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Inicio-tarea_saldos_af_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************");
  $return = true;
  try{
    global $db;
    
    /******Saldos AF******/    
    $querySAF = "		
	UPDATE sasa_saldosaf LEFT JOIN (select sasa_saldosaf.name, sasa_saldosaf.id Idsaldo, accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida from sasa_saldosaf inner join accounts_sasa_saldosaf_1_c ON sasa_saldosaf.id=accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1sasa_saldosaf_idb GROUP BY sasa_saldosaf.name, sasa_saldosaf.assigned_user_id, accounts_sasa_saldosaf_1_c.accounts_sasa_saldosaf_1accounts_ida ORDER BY `sasa_saldosaf`.`sasa_fechasaldo_c` DESC)As tabla ON sasa_saldosaf.id=tabla.Idsaldo WHERE sasa_saldosaf.deleted=0 AND sasa_saldosaf.team_id != '' AND sasa_saldosaf.team_set_id !='' AND tabla.Idsaldo IS NULL
    ";
    
    $resultSAF = $db->query($querySAF);
    $getAffectedRowCount = $GLOBALS['db']->getAffectedRowCount($resultSAF)." ".$GLOBALS['db']->affected_rows;
    $GLOBALS['log']->security("- {$getAffectedRowCount}= SaldosAF Actualizados");
  }
  catch (Exception $e) {
    $GLOBALS['log']->security("ERROR: ".$e->getMessage());
    $return=false;
  }
  $GLOBALS['log']->security("******************************************************");
  $GLOBALS['log']->security("Fin-tarea_saldos_af_ultimo_registro. ".date("Y-m-d h:i:s"));
  $GLOBALS['log']->security("******************************************************\n\n\n\n");
  return $return;
}
