<?php
 // created: 2018-06-20 17:26:31
$dictionary['RevenueLineItem']['fields']['quantity']['default']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['len']='12';
$dictionary['RevenueLineItem']['fields']['quantity']['audited']=true;
$dictionary['RevenueLineItem']['fields']['quantity']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['comments']='Quantity in use';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['quantity']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['quantity']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['quantity']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['quantity']['enable_range_search']=false;

 ?>