({
	plugins: ['Dashlet'],
	bean: null,
	boton: true,
	respuesta: null,
	token: null,
	autenticarse: null,
	ws: null,
	data: null,
	tipo: null,
	cuenta2:[],
	certificado:[],
	url:'**PAI**',
	initialize: function(options) {
		var self = this;	
		dashlet9 = self;	
		if (typeof app === 'undefined') app = App;			
		self._super('initialize', [options]);
		if (self.context.attributes.model.attributes) self.bean = self.context.attributes.model.attributes;
		
		var checkExist = setInterval(function() {
			if ($('#dashlet9_consultar').length) {	  
				if(self.boton){
					self.boton=false;//solo añadir la funciona al boton una vez...
					if (typeof self.bean.sasa_nroidentificacion_c !== 'undefined'){//para la vista registro de cuenta
						$("#dashlet9_id_cliente").val(self.bean.sasa_nroidentificacion_c);
						$("#dashlet9_id_cliente").css("border", "none"); 
						$("#dashlet9_id_cliente").css("background", "rgba(0,0,0,0)"); 
						$("#dashlet9_id_cliente").attr('readonly', true);
					}
					
					$("#dashlet9_consultar").click( function(){
						self.dashlet9_portal();	
					});
					
					$("#dashlet9_consultar").show();
					
					$("#dashlet9_descargar1").click( function(){
						self.dashlet9_portal_obtener_descargar1(true,false);	
					});
					
					$("#dashlet9_descargar2").click( function(){
						self.dashlet9_portal_obtener_descargar2(true,false);	
					});
					
					$("#dashlet9_enviar1").click( function(){
						self.dashlet9_portal_obtener_descargar1(true,true);	
					});
					
					$("#dashlet9_enviar2").click( function(){
						self.dashlet9_portal_obtener_descargar2(true,true);	
					});
				}
				clearInterval(checkExist);
			}
		}, 100);
	},
	dashlet9_portal: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet9_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet9_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){
			if (!_.isEmpty( $("#dashlet9_id_cliente").val() )){		
				self.dashlet9_portal_tipo_certificado_manual(false);
				self.dashlet9_portal_obtener_Embajada(false);//poblando lista tipo Embajada
				//self.dashlet9_portal_obtener_Tipo_Certificado1(true);//poblando lista Tipo_Certificado1
				self.dashlet9_portal_obtener_Fondo1(false);//poblando lista Fondo1
				//self.dashlet9_portal_obtener_Tipo_Certificado2(false);//poblando lista Tipo_Certificado2
				self.dashlet9_portal_obtener_Fondo2(false);//poblando lista Fondo2
				self.dashlet9_portal_dashlet9_get_cuenta();	
			}
			else{
				
				app.alert.show('error_procesando_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}	
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet9_portal_procesando');		
			document.getElementById('dashlet9_skm_LockPane').className = "LockOff"; 	
		}, 200 );	
	},
	dashlet9_portal_dashlet9_get_cuenta: function(){
		var self = this;	
		if (typeof app === 'undefined') app = App;
	
		/*
		Obteniendo cuenta
		*/
		$("#dashlet9_nombre").html('');
		$("#dashlet9_correo").html('');
		app.api.call(
			'GET', 
			app.api.buildURL('Accounts?filter[0][sasa_nroidentificacion_c][$equals]='+$("#dashlet9_id_cliente").val()), 
				{
					"sasa_nroidentificacion_c": $("#dashlet9_id_cliente").val(),
				}, 
				{
				success: function (data) {
					if(!_.isEmpty(data.records)){
						$("#dashlet9_nombre").html(data.records[0].name);
						if(!_.isEmpty(data.records[0].email)){						
							$.each(data.records[0].email, function( k, mail ) {
								var valor = mail.email_address;
								var texto = mail.email_address;
								var selected = "";
								if(mail.primary_address) selected ='selected="true"';
								if(mail.invalid_email){
									texto += " (Invalido)";
								}
								else if(mail.primary_address){
									texto += " (Principal)";
								} 
								else if(mail.opt_out){
									texto += " (Rehusado)";
								} 
								else if(mail.invalid_email){
									texto += " (No Valido)";
								}
								else{
									texto += " (Opcional)";
								}
								if (!_.isEmpty( valor )){ 
									$("#dashlet9_correo").append('<option '+selected+' value="'+valor+'">'+texto+'</option>');	
								}
							});
						}
					}
					else{
						$("#dashlet9_nombre").html('--<b>No Existe en sugar</b>--');
						$("#dashlet9_correo").html('<option>--</option>');
					}
				},
				error: function (e) {		
					app.alert.dismiss('dashlet9_portal_procesando');				
					app.alert.show('error_procesando_cuenta', {
						level: 'error',
						title: 'Fallo al cargar cuenta en SugarCRM ' + e ,
						autoClose: false,
					});
				}
			}, {async: false}
		);
	},
	dashlet9_portal_tipo_certificado_manual: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		var valor = "Embajadas";
		var texto = valor;
		$("#dashlet9_Tipo_Certificado1").html('<option value="'+valor+'">'+texto+'</option>');
		
		var valor = "Certificado comercial por fondo";
		var texto = valor;
		$("#dashlet9_Tipo_Certificado2").html('<option value="'+valor+'">'+texto+'</option>');
	},
	dashlet9_portal_obtener_Fondo2: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet9_Fondo2").empty();//limpiando
		self.ws = "/api_fiducia/consultas/fondos?idUsuarioRecurso="+$("#dashlet9_id_cliente").val();
		self.tipo = "GET";
		self.data = [];
		self.dashlet9_portal_call({});//autenticandose y consumo
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("dashlet9_portal_obtener_Fondo2: ",self.respuesta);
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.wsft_fond !== 'undefined') {
						if (respuesta.wsft_saldo != '0') {
							var valor = respuesta.wsft_fond;
							var texto = respuesta.wsft_fond_descri1 +" " + respuesta.wsft_fond_descri2;
							$("#dashlet9_Fondo2").append('<option value="'+valor+'">'+texto+'</option>');	
						}		
					}
				});	
			}
			else{				
				self.certificado = [];		
				$("#dashlet9_Fondo2").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener fondo2 vacio',
					autoClose: false,
				});
			}
		}
		
		if ( _.isEmpty( $("#dashlet9_Fondo2").html().trim() ) ){ 
			$("#dashlet9_Fondo2").append('<option value="">--</option>');
		}
	},
	dashlet9_portal_obtener_Tipo_Certificado2: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet9_Tipo_Certificado2").empty();//limpiando
		self.ws = "/auth_fiducia/recursoComercial/menu-certificados";
		self.tipo = "GET";
		self.data = [];
		self.dashlet9_portal_call({});//autenticandose y consumo
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("dashlet9_portal_obtener_Tipo_Certificado2: ",self.respuesta);
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.nombre !== 'undefined') {
						var valor = respuesta.id;
						var texto = respuesta.nombre;
						$("#dashlet9_Tipo_Certificado2").append('<option value="'+valor+'">'+texto+'</option>');			
					}
				});	
			}
			else{				
				self.certificado = [];		
				$("#dashlet9_Tipo_Certificado2").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener_certificado2 vacio',
					autoClose: false,
				});
			}
		}
		
		if ( _.isEmpty( $("#dashlet9_Tipo_Certificado2").html().trim() ) ){ 
			$("#dashlet9_Tipo_Certificado2").append('<option value="">--</option>');
		}
	},
	dashlet9_portal_obtener_descargar2: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet9_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet9_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){	
			if (!_.isEmpty( $("#dashlet9_id_cliente").val() )    ){
				if ( !_.isEmpty($("#dashlet9_Tipo_Certificado2").val())  &&  !_.isEmpty($("#dashlet9_Fondo2").val())       ){
					self.dashlet9_portal_obtener_cuenta(true);//poblando cuenta
					if (self.token != null){
						if (!_.isEmpty( self.respuesta )){	
							console.log("cuenta_tipoIdentificacion:",  self.respuesta);	
							if (typeof self.respuesta.datos[0].codTipoIdentificacion !== 'undefined'){
								var tipoIdentificacion = self.respuesta.datos[0].codTipoIdentificacion;	
								if(tipoIdentificacion != null){
									self.autenticarse = false;
									self.ws = "/api_reportes/consultas/certificaciones/comercial?fondo="+$("#dashlet9_Fondo2").val()+"&idUsuarioRecurso="+$("#dashlet9_id_cliente").val()+"&tipoIdentificacion="+tipoIdentificacion;
									self.tipo = "file";
									self.data = [];
									var mail = {};
									var tipo = "Carta comercial";
									if(enviar){	
										var Address = "";	
										if ( !_.isEmpty( $("#dashlet9_correo").val() )){
											if (typeof $("#dashlet9_correo").val()[0] !== 'undefined'){	
												if ( !_.isEmpty( $("#dashlet9_correo").val()[0] )){ 
													firma="Atentamente,\n___________\nAlianza";
													//Address = $("#dashlet9_correo").val().join("; ");
													Address = $("#dashlet9_correo").val();	
													Subject = tipo+" - "+$("#dashlet9_id_cliente").val()+" - "+Date();			
													nombre = $("#dashlet9_nombre").html();
													
													Body = "";
													Body += "Buen día,";
													Body += "\n\n";
													Body += "Estimado "+nombre+",";
													Body += "\n\n";
													Body += "\n\n";
													Body += "Reciba un cordial saludo de parte de Alianza.";
													Body += "\n\n";
													Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
													Body += "\n\n";
													Body += "\n\n";
													Body += "Cordialmente,";
													Body += "\n\n";
													Body += "Alianza.";
													Body += "\n\n";
													Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
													
													mail = {
														"Subject": Subject,
														"Body": Body,
														"Address": Address,
														"AddressCC": "",
													};
												}
											}
										}
										if ( _.isEmpty( Address )){
											app.alert.show('error_procesando_obtener_descargar2', {
												level: 'error',
												title: "Por favor, seleccione una direccion de correo!!",
												autoClose: false,
											});
											return;	
										}
									}
									self.dashlet9_portal_call(mail);//autenticandose y consumo
									if (!_.isEmpty( self.respuesta )){ 
										if(enviar){						
											if(self.respuesta == "ok"){
												//var Address = $("#dashlet9_correo").val().join("; ");
												var Address = $("#dashlet9_correo").val();
												app.alert.show('success_procesando_obtener_descargar2', {
													level: 'success',
													messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
													autoClose: false,
												});	
											}
											else{
												app.alert.show('error_procesando_obtener_descargar2', {
													level: 'error',
													title: self.respuesta,
													autoClose: false,
												});	
											}									
										}
										else{
											//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
					                        var mapForm = document.createElement("form");
					                            mapForm.target = "Map";
					                            mapForm.method = "POST"; // or "post" if appropriate
					                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

					                            var mapInput = document.createElement("input");
					                            mapInput.type = "text";
					                            mapInput.name = "file";
					                            mapInput.value = self.respuesta;
					                            mapForm.appendChild(mapInput);

					                            document.body.appendChild(mapForm);

					                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

					                        if (map) {
					                            mapForm.submit();
					                        }
											else{
												app.alert.show('error_procesando_obtener_descargar1', {
													level: 'error',
													title: 'Error al descargar PDF',
													messages: "El navegador esta bloqueando la ventana!!!",
													autoClose: false,
												});
											}	
										}
									}
									else{				
										
										app.alert.show('error_procesando_obtener_descargar2', {
											level: 'error',
											title: 'Error al descargar2 PDF',
											autoClose: false,
										});
									}
								}
								else{				
									
									app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
										level: 'error',
										title: 'Error tipoIdentificacion vacio',
										autoClose: false,
									});
								}						
							}
						}
						else{				
							
							app.alert.show('error_obtener_cuenta', {
								level: 'error',
								title: 'Error obtener_cuenta vacio',
								autoClose: false,
							});
						}				
					}
				}
				else{
						
					app.alert.show('error_procesando_obtener_descargar2', {
						level: 'error',
						title: "Hay un campo sin seleccionar",
						autoClose: false,
					});
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet9_portal_procesando');		
			document.getElementById('dashlet9_skm_LockPane').className = "LockOff"; 	
		}, 200 );
	},
	dashlet9_portal_obtener_descargar1: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;				
		
		/*
		Bloqueando interfaz
		*/
		window.setTimeout( function(){
			document.getElementById('dashlet9_skm_LockPane').className = "LockOn"; 
			app.alert.dismissAll();
			app.alert.show('dashlet9_portal_procesando', {
				level: 'process',
				title: 'Procesando...' ,
				autoClose: false,
			}); 			
		}, 100 );
		
		window.setTimeout( function(){			
		
			if (!_.isEmpty( $("#dashlet9_id_cliente").val() )    ){
				if ( !_.isEmpty($("#dashlet9_Tipo_Certificado1").val())  &&  !_.isEmpty($("#dashlet9_Fondo1").val())    &&  !_.isEmpty($("#dashlet9_Embajada").val())      ){
					self.dashlet9_portal_obtener_cuenta(true);//poblando cuenta
					if (self.token != null){
						if (!_.isEmpty( self.respuesta )){	
							console.log("cuenta_tipoIdentificacion:",  self.respuesta);	
							if (typeof self.respuesta.datos[0].codTipoIdentificacion !== 'undefined'){
								var tipoIdentificacion = self.respuesta.datos[0].codTipoIdentificacion;	
								if(tipoIdentificacion != null){
									self.autenticarse = false;
									self.ws = "/api_reportes/consultas/certificaciones/embajadasFiduciaria?pais="+$("#dashlet9_Embajada").val()+"&fondo="+$("#dashlet9_Fondo1").val()+"&idUsuarioRecurso="+$("#dashlet9_id_cliente").val()+"&tipoIdentificacion="+tipoIdentificacion;
									self.tipo = "file";
									self.data = [];
									var mail = {};
									var tipo = "Cartas Embajadas";
									if(enviar){	
										var Address = "";	
										if ( !_.isEmpty( $("#dashlet9_correo").val() )){
											if (typeof $("#dashlet9_correo").val()[0] !== 'undefined'){	
												if ( !_.isEmpty( $("#dashlet9_correo").val()[0] )){ 
													firma="Atentamente,\n___________\nAlianza";
													//Address = $("#dashlet9_correo").val().join("; ");
													Address = $("#dashlet9_correo").val();	
													Subject = tipo+" - "+$("#dashlet9_id_cliente").val()+" - "+Date();					
													nombre = $("#dashlet9_nombre").html();
													
													Body = "";
													Body += "Buen día,";
													Body += "\n\n";
													Body += "Estimado "+nombre+",";
													Body += "\n\n";
													Body += "\n\n";
													Body += "Reciba un cordial saludo de parte de Alianza.";
													Body += "\n\n";
													Body += "Adjunto a este correo encontrara el "+tipo+" en formato PDF.";
													Body += "\n\n";
													Body += "\n\n";
													Body += "Cordialmente,";
													Body += "\n\n";
													Body += "Alianza.";
													Body += "\n\n";
													Body += "Este email es informativo, favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes. ";
									
													mail = {
														"Subject": Subject,
														"Body": Body,
														"Address": Address,
														"AddressCC": "",
													};
												}
											}
										}
										if ( _.isEmpty( Address )){
											app.alert.show('error_procesando_obtener_descargar2', {
												level: 'error',
												title: "Por favor, seleccione una direccion de correo!!",
												autoClose: false,
											});
											return;	
										}
									}
									self.dashlet9_portal_call(mail);//autenticandose y consumo
									if (!_.isEmpty( self.respuesta )){ 
										if(enviar){									
											if(self.respuesta == "ok"){
												//var Address = $("#dashlet9_correo").val().join("; ");
												var Address = $("#dashlet9_correo").val();
												app.alert.show('success_procesando_obtener_descargar1', {
													level: 'success',
													messages: "Correo "+tipo+" enviado!!<p> Correos: "+Address+"!",
													autoClose: false,
												});	
											}
											else{
												app.alert.show('error_procesando_obtener_descargar1', {
													level: 'error',
													title: self.respuesta,
													autoClose: false,
												});	
											}									
										}
										else{
											//Funcion para ver el PDF en verison 11 de sugar, se debe pasar por una URL intermedia.
					                        var mapForm = document.createElement("form");
					                            mapForm.target = "Map";
					                            mapForm.method = "POST"; // or "post" if appropriate
					                            mapForm.action = "https://www.sasaconsultoria.com/VisualizadorAlianza.php";

					                            var mapInput = document.createElement("input");
					                            mapInput.type = "text";
					                            mapInput.name = "file";
					                            mapInput.value = self.respuesta;
					                            mapForm.appendChild(mapInput);

					                            document.body.appendChild(mapForm);

					                            map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

					                        if (map) {
					                            mapForm.submit();
					                        }
											else{
												app.alert.show('error_procesando_obtener_descargar1', {
													level: 'error',
													title: 'Error al descargar PDF',
													messages: "El navegador esta bloqueando la ventana!!!",
													autoClose: false,
												});
											}
										}
									}
									else{				
										
										app.alert.show('error_procesando_obtener_descargar1', {
											level: 'error',
											title: 'Error al descargar1 PDF',
											autoClose: false,
										});
									}
								}
								else{				
									
									app.alert.show('error_procesando_obtener_tipo_tipoIdentificacion', {
										level: 'error',
										title: 'Error tipoIdentificacion vacio',
										autoClose: false,
									});
								}						
							}
						}
						else{				
							
							app.alert.show('error_obtener_cuenta', {
								level: 'error',
								title: 'Error obtener_cuenta vacio',
								autoClose: false,
							});
						}				
					}
				}
				else{				
					
					app.alert.show('error_procesando_obtener_descargar1', {
						level: 'error',
						title: 'Error campos necesarios vacios',
						autoClose: false,
					});
				}
			}
			else{				
				
				app.alert.show('error_procesando_obtener_id_cliente', {
					level: 'error',
					title: 'No. Identificación vacio!',
					autoClose: false,
				});
			}
		
			/*
			Desbloqueando interfaz
			*/
			app.alert.dismiss('dashlet9_portal_procesando');		
			document.getElementById('dashlet9_skm_LockPane').className = "LockOff"; 	
		}, 200 );
	},		
	dashlet9_portal_obtener_cuenta: function(autenticarse,enviar){
		var self = this;	
		if (typeof app === 'undefined') app = App;	
		
		self.autenticarse = autenticarse;
		self.ws = "/api_fiducia/comercial/consultas/obtenerClientes";
		self.tipo = "POST";
		self.data = '{\"nombre": \"\", \"id\": \"'+$("#dashlet9_id_cliente").val()+'\", \"tipoId\": \"\", \"producto\": \"\", \"desde\": 1, \"hasta\": 1}';
		self.dashlet9_portal_call({});//autenticandose y consumo
	},
	dashlet9_portal_obtener_Embajada: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;

		Embajadas = [
			'Alemania', 
			'Argelia', 
			'Argentina', 
			'Australia', 
			'Austria', 
			'Barbados', 
			'Bélgica', 
			'Bolivia', 
			'Brasil', 
			'Canadá', 
			'Chile', 
			'China',
			'Corea', 
			'Costa Rica', 
			'Cuba', 
			'Dinamarca', 
			'Ecuador', 
			'El Salvador', 
			'Emiratos Árabes Unidos', 
			'España', 
			'Estados Unidos',
			'Filipinas', 
			'Finlandia', 
			'Francia', 
			'Grecia', 
			'Guatemala', 
			'Haití', 
			'Honduras', 
			'Hungría', 
			'India', 
			'Indonesia', 
			'Irán', 
			'Irlanda',
			'Israel', 
			'Italia', 
			'Jamaica', 
			'Japón', 
			'La República Árabe de Egipto', 
			'La República Checa', 
			'La República de Azerbaiyán',
			'La República de Chipre', 
			'La República Dominicana', 
			'La República Eslovaca', 
			'Líbano', 
			'Los Países Bajos', 
			'Malta', 
			'Marruecos',
			'México', 
			'Nicaragua', 
			'Noruega', 
			'Palestina', 
			'Panamá', 
			'Paraguay', 
			'Perú', 
			'Polonia', 
			'Portugal', 
			'Reino Unido de la Gran Bretaña',
			'Rumania', 
			'Rusia', 
			'Serbia', 
			'Siria', 
			'Suecia', 
			'Suiza', 
			'Tailandia', 
			'Trinidad y Tobago', 
			'Turquía', 
			'Ucrania', 
			'Uruguay', 
			'Venezuela',
			'Vietnam',
		];
		$("#dashlet9_Embajada").empty();//limpiando		
		$.each(Embajadas, function( i, Embajada ) {
			if (typeof Embajadas !== 'undefined') {
				var valor = Embajada;
				var texto = Embajada;
				$("#dashlet9_Embajada").append('<option value="'+encodeURI(valor)+'">'+texto+'</option>');			
			}
		});
		if ( _.isEmpty( $("#dashlet9_Embajada").html().trim() ) ){ 
			$("#dashlet9_Embajada").append('<option value="">--</option>');
		}
	},
	dashlet9_portal_obtener_Fondo1: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet9_Fondo1").empty();//limpiando
		self.ws = "/api_fiducia/consultas/fondos?idUsuarioRecurso="+$("#dashlet9_id_cliente").val();
		self.tipo = "GET";
		self.data = [];
		self.dashlet9_portal_call({});//autenticandose y consumo
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("dashlet9_portal_obtener_Fondo1: ",self.respuesta);
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.wsft_fond !== 'undefined') {
						var valor = respuesta.wsft_fond;
						var texto = respuesta.wsft_fond_descri1 +" " + respuesta.wsft_fond_descri2;
						$("#dashlet9_Fondo1").append('<option value="'+valor+'">'+texto+'</option>');			
					}
				});	
			}
			else{				
				self.certificado = [];		
				$("#dashlet9_Fondo1").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener fondo1 vacio',
					autoClose: false,
				});
			}
		}
		
		if ( _.isEmpty( $("#dashlet9_Fondo1").html().trim() ) ){ 
			$("#dashlet9_Fondo1").append('<option value="">--</option>');
		}
	},
	dashlet9_portal_obtener_Tipo_Certificado1: function(autenticarse){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		self.autenticarse = autenticarse;
		$("#dashlet9_Tipo_Certificado1").empty();//limpiando
		self.ws = "/auth_fiducia/recursoComercial/menu-certificados";
		self.tipo = "GET";
		self.data = [];
		self.dashlet9_portal_call({});//autenticandose y consumo
		if (self.token != null){
			if (!_.isEmpty( self.respuesta )){ 		
				if(!$.isArray( self.respuesta )){			
					var temp = self.respuesta;
					self.respuesta = [];
					self.respuesta.push(temp);
				}
				console.log("dashlet9_portal_obtener_Tipo_Certificado1: ",self.respuesta);
				$.each(self.respuesta, function( i, respuesta ) {
					if (typeof respuesta.nombre !== 'undefined') {
						var valor = respuesta.id;
						var texto = respuesta.nombre;
						$("#dashlet9_Tipo_Certificado1").append('<option value="'+valor+'">'+texto+'</option>');			
					}
				});	
			}
			else{				
				self.certificado = [];		
				$("#dashlet9_Tipo_Certificado1").append('<option value="">--</option>');		
				
				app.alert.show('error_procesando_obtener_tipo_certificado', {
					level: 'error',
					title: 'Error al obtener_certificado1 vacio',
					autoClose: false,
				});
			}
		}
		if ( _.isEmpty( $("#dashlet9_Tipo_Certificado1").html().trim() ) ){ 
			$("#dashlet9_Tipo_Certificado1").append('<option value="">--</option>');
		}
	},
	dashlet9_portal_call: function(mail){
		var self = this;	
		if (typeof app === 'undefined') app = App;
		
		/*
		Para el periodo
		Al parecer no hay ningún servicio que realice esta lógica, el portal con esa función lo que hace es calcular los últimos 6 meses a partir de la fecha para mostrar en el listado.

		*/
		if (typeof app === 'undefined') app = App;
		var method  = "dashlet_portal";		
		var data = [
			{
				'url':self.url,			
				'tipo':'POST',	
				'ws':'/auth_fiducia/comercial/autenticar',
				'data': "**PAI**",
				'autenticarse': self.autenticarse,
				'token': self.token,
			},
			{				
				//call
				'url':self.url,
				'tipo':self.tipo,
				'ws':self.ws,
				'data':self.data,
				'mail':mail,
			},
		];
			
		var url = app.api.buildURL(method, 'read','', { data }   );  
		
		app.api.call('read', url, null, {  
			success: _.bind(function(response) { 
				self.token=response['token'];
				self.respuesta=response['respuesta'];	
				
				
				if(self.token.includes("Credenciales invalidas")){
					error = JSON.parse(self.token);				
					if(error["data"] == "Credenciales invalidas" ){						
						app.alert.show('error_procesando_obtener_fondo', {
							level: 'error',
							title: 'Error '+error["code"]+' '+error["data"],
							autoClose: false,
						});
						self.token=null;
					}
				}	
				
			}, this),  
			error: _.bind(function(error) {  
				self.token=null;
				self.respuesta=null;
				
				app.alert.show('error_procesando_pedido', {
					level: 'error',
					title: 'Fallo al procesar ' + error ,
					autoClose: false,
				});
			}, this),  
		}, {async: false});
	},
})
