<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/sasa1_Unidades_de_negocio_por_/Ext/Vardefs/sugarfield_sasa_regional_c.php

 // created: 2023-02-01 21:12:25
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['labelValue']='Regional';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['enforced']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['dependency']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['required_formula']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_regional_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa1_Unidades_de_negocio_por_/Ext/Vardefs/sugarfield_sasa_ejecutivocomercial_c.php

 // created: 2023-02-01 21:14:17
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['labelValue']='Ejecutivo comercial';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['enforced']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['dependency']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['required_formula']='';
$dictionary['sasa1_Unidades_de_negocio_por_']['fields']['sasa_ejecutivocomercial_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/sasa1_Unidades_de_negocio_por_/Ext/Vardefs/users_sasa1_unidades_de_negocio_por__1_sasa1_Unidades_de_negocio_por_.php

// created: 2023-02-06 20:16:31
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'users_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'side' => 'right',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link' => 'users_sasa1_unidades_de_negocio_por__1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["users_sasa1_unidades_de_negocio_por__1users_ida"] = array (
  'name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'users_sasa1_unidades_de_negocio_por__1users_ida',
  'link' => 'users_sasa1_unidades_de_negocio_por__1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa1_Unidades_de_negocio_por_/Ext/Vardefs/sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_sasa1_Unidades_de_negocio_por_.php

// created: 2023-02-17 20:29:36
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'sasa2_Unidades_de_negocio',
  'bean_name' => 'sasa2_Unidades_de_negocio',
  'side' => 'right',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA2_UNIDADES_DE_NEGOCIO_TITLE',
  'save' => true,
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'name',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["sasa2_unid7bfcnegocio_ida"] = array (
  'name' => 'sasa2_unid7bfcnegocio_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SASA2_UNIDADES_DE_NEGOCIO_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'sasa2_unid7bfcnegocio_ida',
  'link' => 'sasa2_unidades_de_negocio_sasa1_unidades_de_negocio_por__1',
  'table' => 'sasa2_unidades_de_negocio',
  'module' => 'sasa2_Unidades_de_negocio',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/sasa1_Unidades_de_negocio_por_/Ext/Vardefs/accounts_sasa1_unidades_de_negocio_por__1_sasa1_Unidades_de_negocio_por_.php

// created: 2023-02-20 21:38:36
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'type' => 'link',
  'relationship' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE',
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link-type' => 'one',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1_name"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["sasa1_Unidades_de_negocio_por_"]["fields"]["accounts_sasa1_unidades_de_negocio_por__1accounts_ida"] = array (
  'name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SASA1_UNIDADES_DE_NEGOCIO_POR__1_FROM_SASA1_UNIDADES_DE_NEGOCIO_POR__TITLE_ID',
  'id_name' => 'accounts_sasa1_unidades_de_negocio_por__1accounts_ida',
  'link' => 'accounts_sasa1_unidades_de_negocio_por__1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
