<?php
 // created: 2022-10-25 23:17:04
$dictionary['Account']['fields']['sasa_razon_social_c']['labelValue']='Razón social';
$dictionary['Account']['fields']['sasa_razon_social_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_razon_social_c']['enforced']='';
$dictionary['Account']['fields']['sasa_razon_social_c']['dependency']='';
$dictionary['Account']['fields']['sasa_razon_social_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_razon_social_c']['readonly_formula']='';

 ?>