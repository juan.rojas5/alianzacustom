<?php
 // created: 2018-06-12 20:23:10
$dictionary['Account']['fields']['googleplus']['audited']=false;
$dictionary['Account']['fields']['googleplus']['massupdate']=false;
$dictionary['Account']['fields']['googleplus']['comments']='The Google Plus name of the company';
$dictionary['Account']['fields']['googleplus']['duplicate_merge']='disabled';
$dictionary['Account']['fields']['googleplus']['duplicate_merge_dom_value']='0';
$dictionary['Account']['fields']['googleplus']['merge_filter']='disabled';
$dictionary['Account']['fields']['googleplus']['reportable']=false;
$dictionary['Account']['fields']['googleplus']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['googleplus']['calculated']=false;

 ?>