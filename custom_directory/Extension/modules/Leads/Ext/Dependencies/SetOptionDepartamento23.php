<?php
$dependencies['Leads']['SetOptionDepartamento23'] = array(
   'hooks' => array("all"),
   'trigger' => 'equal($sasa_departamento_c,"Narino")',
   'triggerFields' => array('sasa_municipio_c','sasa_departamento_c'),
   'onload' => true,
   'actions' => array(
      array(
        'name' => 'SetOptions',
        'params' => array(
           'target' => 'sasa_municipio_c',
           'keys' => 'createList("","Alban","Mosquera","Narino","Ricaurte","San Bernardo","El Tambo","Belen","Cordoba","Aldana","Ancuya","Arboleda","Barbacoas","Buesaco","Chachagüi","Colon","Consaca","Contadero","Cuaspud","Cumbal","Cumbitara","El Charco","El Penol","El Rosario","El Tablon de Gomez","Francisco Pizarro","Funes","Guachucal","Guaitarilla","Gualmatan","Iles","Imues","Ipiales","La Cruz","La Florida","La Llanada","La Tola","La Union","Leiva","Linares","Los Andes","Magüi","Mallama","Olaya Herrera","Ospina","Pasto","Policarpa","Potosi","Providencia","Puerres","Pupiales","Roberto Payan","Samaniego","San Andres de Tumaco","San Lorenzo","San Pablo","San Pedro de Cartago","Sandona","Santa Barbara","Santacruz","Sapuyes","Taminango","Tangua","Tuquerres","Yacuanquer")',
           'labels' => 'createList("","Albán","Mosquera","Nariño","Ricaurte","San Bernardo","El Tambo","Belén","Córdoba","Aldana","Ancuyá","Arboleda","Barbacoas","Buesaco","Chachagüí","Colón","Consaca","Contadero","Cuaspud","Cumbal","Cumbitara","El Charco","El Peñol","El Rosario","El Tablón de Gómez","Francisco Pizarro","Funes","Guachucal","Guaitarilla","Gualmatán","Iles","Imués","Ipiales","La Cruz","La Florida","La Llanada","La Tola","La Unión","Leiva","Linares","Los Andes","Magüí","Mallama","Olaya Herrera","Ospina","Pasto","Policarpa","Potosí","Providencia","Puerres","Pupiales","Roberto Payán","Samaniego","San Andrés de Tumaco","San Lorenzo","San Pablo","San Pedro de Cartago","Sandoná","Santa Bárbara","Santacruz","Sapuyes","Taminango","Tangua","Túquerres","Yacuanquer")'
        ),
      ),
      array(
          'name' => 'ReadOnly',
          //The parameters passed in will depend on the action type set in 'name'
          'params' => array(
              'target' => 'sasa_municipio_c',
              'value' => 'and(equal($converted,true),not(isUserAdmin("")))',
          ),
      ),
    ),
);
