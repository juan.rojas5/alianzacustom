<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['sasaP_PresupuestosxAsesor'] = 'Presupuestos x Asesor';
$app_list_strings['moduleList']['sasaP_PresupuestoxProducto'] = 'Presupuestos x Producto';
$app_list_strings['moduleListSingular']['sasaP_PresupuestosxAsesor'] = 'Presupuesto x Asesor';
$app_list_strings['moduleListSingular']['sasaP_PresupuestoxProducto'] = 'Presupuesto x Producto';
$app_list_strings['sasa_year_c_list'][2018] = '2018';
$app_list_strings['sasa_year_c_list'][2019] = '2019';
$app_list_strings['sasa_year_c_list'][2020] = '2020';
$app_list_strings['sasa_year_c_list'][''] = '';
$app_list_strings['sasa_mes_c_list']['01'] = 'Enero';
$app_list_strings['sasa_mes_c_list']['02'] = 'Febrero';
$app_list_strings['sasa_mes_c_list'][''] = '';
$app_list_strings['account_type_dom']['Customer'] = 'Cliente';
$app_list_strings['account_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['account_type_dom']['No Interesado'] = 'No Interesado';
$app_list_strings['account_type_dom']['No Registra'] = 'No Registra';
$app_list_strings['account_type_dom'][''] = '';
