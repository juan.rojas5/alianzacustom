<?php
 // created: 2018-07-06 21:54:20
$dictionary['Meeting']['fields']['date_modified']['audited']=true;
$dictionary['Meeting']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Meeting']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_modified']['calculated']=false;
$dictionary['Meeting']['fields']['date_modified']['enable_range_search']='1';

 ?>