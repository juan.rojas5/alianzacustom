<?php
$job_strings[] = 'update_retraso_ctrl_wf_canastas';
require_once ("data/BeanFactory.php");
require_once('include/SugarQuery/SugarQuery.php');

function update_retraso_ctrl_wf_canastas() {
    global $db;
    $GLOBALS['log']->security("Inicio-Actualiza retrasoctrlworkflow en Canastas-> ".date("Y-m-d h:i:s"));

    $sql_accounts="SELECT accounts.id AS id";
    $sql_accounts.=" FROM accounts INNER JOIN users ON accounts.assigned_user_id=users.id";
    $sql_accounts.=" WHERE users.description='Canastas.' AND accounts.date_entered > '2019-05-03' AND accounts.deleted=0;";
    $query_result = $db->query($sql_accounts);
    
    while($rows = $db->fetchByAssoc($query_result)){
        $id=$rows["id"];
        //Obtener fecha de primera compra(Sera la fecha de creacion del cliente)
        $account_bean = BeanFactory::getBean('Accounts',$id);
        $account_bean->sasa_retrasoctrlworkflow_c ='3';
        $account_bean->save();
	}
    $GLOBALS['log']->security("Fin-Actualiza retrasoctrlworkflow en Canastas--> ".date("Y-m-d h:i:s"));

	return true;
}
