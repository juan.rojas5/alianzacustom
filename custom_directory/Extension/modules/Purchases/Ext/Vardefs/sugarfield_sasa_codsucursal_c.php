<?php
 // created: 2023-02-01 19:11:35
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['labelValue']='codigo sucursal';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['enforced']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['dependency']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['required_formula']='';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['readonly']='1';
$dictionary['Purchase']['fields']['sasa_codsucursal_c']['readonly_formula']='';

 ?>