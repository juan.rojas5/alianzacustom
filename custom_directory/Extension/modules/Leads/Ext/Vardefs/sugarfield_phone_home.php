<?php
 // created: 2018-07-18 04:32:41
$dictionary['Lead']['fields']['phone_home']['len']='20';
$dictionary['Lead']['fields']['phone_home']['audited']=false;
$dictionary['Lead']['fields']['phone_home']['massupdate']=false;
$dictionary['Lead']['fields']['phone_home']['comments']='';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_home']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_home']['reportable']=false;
$dictionary['Lead']['fields']['phone_home']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.02',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_home']['calculated']=false;
$dictionary['Lead']['fields']['phone_home']['pii']=false;
$dictionary['Lead']['fields']['phone_home']['help']='Formato: (03 + indicativo de ciudad + número fijo de siete dígitos)';

 ?>