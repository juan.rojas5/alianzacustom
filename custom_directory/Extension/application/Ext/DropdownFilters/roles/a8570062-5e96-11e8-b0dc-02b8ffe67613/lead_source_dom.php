<?php
// created: 2018-05-23 21:15:17
$role_dropdown_filters['lead_source_dom'] = array (
  '' => true,
  'Cold Call' => true,
  'Existing Customer' => true,
  'Self Generated' => true,
  'Employee' => true,
  'Partner' => false,
  'Public Relations' => true,
  'Direct Mail' => true,
  'Conference' => true,
  'Trade Show' => true,
  'Web Site' => true,
  'Word of mouth' => true,
  'Email' => true,
  'Campaign' => true,
  'Support Portal User Registration' => false,
  'Other' => true,
  'Socio' => true,
  'Correo Electronico' => true,
  'Regristro de Usuario del Portal de Soporte' => true,
);