<?php
$module_name = 'sasa_MovimientosAF';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'detail' => 
      array (
        'templateMeta' => 
        array (
          'form' => 
          array (
            'buttons' => 
            array (
              0 => 'EDIT',
              1 => 'DUPLICATE',
              2 => 'DELETE',
            ),
          ),
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
          'useTabs' => false,
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'name',
              1 => 
              array (
                'name' => 'accounts_sasa_movimientosaf_1_name',
                'label' => 'LBL_ACCOUNTS_SASA_MOVIMIENTOSAF_1_FROM_ACCOUNTS_TITLE',
              ),
              2 => 
              array (
                'name' => 'sasa_productosafav_sasa_movimientosaf_1_name',
                'label' => 'LBL_SASA_PRODUCTOSAFAV_SASA_MOVIMIENTOSAF_1_FROM_SASA_PRODUCTOSAFAV_TITLE',
              ),
              3 => 
              array (
                'name' => 'sasa_categoria_c',
                'label' => 'LBL_SASA_CATEGORIA_C',
              ),
              4 => 
              array (
                'name' => 'sasa_nombrefondo_c',
                'label' => 'LBL_SASA_NOMBREFONDO_C',
              ),
              5 => 
              array (
                'name' => 'sasa_nombresubcuenta_c',
                'label' => 'LBL_SASA_NOMBRESUBCUENTA_C',
              ),
              6 => 
              array (
                'name' => 'sasa_fecha_c',
                'label' => 'LBL_SASA_FECHA_C',
              ),
              7 => 
              array (
                'name' => 'sasa_valor_c',
                'label' => 'LBL_SASA_VALOR_C',
              ),
              8 => 
              array (
                'name' => 'sasa_compartimiento_c',
                'label' => 'LBL_SASA_COMPARTIMIENTO_C',
              ),
              9 => 
              array (
                'name' => 'sasa_concepto_c',
                'label' => 'LBL_SASA_CONCEPTO_C',
              ),
              10 => 
              array (
                'name' => 'sasa_cuentabancaria_c',
                'label' => 'LBL_SASA_CUENTABANCARIA_C',
              ),
              11 => 
              array (
                'name' => 'sasa_encargo_c',
                'label' => 'LBL_SASA_ENCARGO_C',
              ),
              12 => 
              array (
                'name' => 'sasa_formapago_c',
                'label' => 'LBL_SASA_FORMAPAGO_C',
              ),
              13 => 
              array (
                'name' => 'sasa_motivo_c',
                'label' => 'LBL_SASA_MOTIVO_C',
              ),
              14 => 
              array (
                'name' => 'description',
                'comment' => 'Full text of the note',
                'studio' => 'visible',
                'label' => 'LBL_DESCRIPTION',
              ),
              15 => 
              array (
                'name' => 'tag',
                'studio' => 
                array (
                  'portal' => false,
                  'base' => 
                  array (
                    'popuplist' => false,
                  ),
                  'mobile' => 
                  array (
                    'wirelesseditview' => true,
                    'wirelessdetailview' => true,
                  ),
                ),
                'label' => 'LBL_TAGS',
              ),
              16 => 'assigned_user_name',
              17 => 'team_name',
              18 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              19 => 
              array (
                'name' => 'created_by_name',
                'readonly' => true,
                'label' => 'LBL_CREATED',
              ),
              20 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              21 => 
              array (
                'name' => 'modified_by_name',
                'readonly' => true,
                'label' => 'LBL_MODIFIED',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
