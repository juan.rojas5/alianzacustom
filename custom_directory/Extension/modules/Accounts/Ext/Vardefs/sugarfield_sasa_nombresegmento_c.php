<?php
 // created: 2023-04-11 19:19:08
$dictionary['Account']['fields']['sasa_nombresegmento_c']['labelValue']='Nombre segmento';
$dictionary['Account']['fields']['sasa_nombresegmento_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['sasa_nombresegmento_c']['enforced']='';
$dictionary['Account']['fields']['sasa_nombresegmento_c']['dependency']='';
$dictionary['Account']['fields']['sasa_nombresegmento_c']['required_formula']='';
$dictionary['Account']['fields']['sasa_nombresegmento_c']['readonly_formula']='';

 ?>