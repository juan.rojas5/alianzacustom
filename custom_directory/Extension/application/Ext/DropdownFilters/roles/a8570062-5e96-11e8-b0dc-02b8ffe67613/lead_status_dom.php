<?php
// created: 2018-05-23 21:18:50
$role_dropdown_filters['lead_status_dom'] = array (
  '' => true,
  'New' => true,
  'Assigned' => true,
  'In Process' => false,
  'Converted' => true,
  'Recycled' => true,
  'Dead' => true,
);