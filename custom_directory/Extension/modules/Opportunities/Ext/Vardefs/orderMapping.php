<?php
// created: 2023-02-03 09:42:46
$extensionOrderMap = array (
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_entered.php' => 
  array (
    'md5' => '2c623774650eb0c09209958ea2e878b5',
    'mtime' => 1529501489,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_opportunity_type.php' => 
  array (
    'md5' => '7e54ac3723493005024bc5986fd85e84',
    'mtime' => 1529501755,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_lead_source.php' => 
  array (
    'md5' => 'e158507f27f26088feaec3614c88b4ca',
    'mtime' => 1529502073,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_modified.php' => 
  array (
    'md5' => 'f82830db374ddf94dbeaa23549caf24c',
    'mtime' => 1529502736,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_nombrecompetidor_c.php' => 
  array (
    'md5' => '19f1d9e81f3ce9075513ce319133f1bd',
    'mtime' => 1530985491,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_superior1_c.php' => 
  array (
    'md5' => 'c58a072b39e63dde4f4ddf6de993c3de',
    'mtime' => 1531141508,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_superior2_c.php' => 
  array (
    'md5' => 'fb0261f4da3e2bab9e7ba78feb992986',
    'mtime' => 1531142553,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_name.php' => 
  array (
    'md5' => 'bb75622cb9c0f60594e547012bce16f4',
    'mtime' => 1531237266,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_test_c.php' => 
  array (
    'md5' => '0718ccd3c7e587545bbc81084090208a',
    'mtime' => 1531241413,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sasa_motivoperdida_c.php' => 
  array (
    'md5' => '8af85dc358c4d151b32603a0f675348c',
    'mtime' => 1533404539,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed_timestamp.php' => 
  array (
    'md5' => '0f11ffed80418ee1f9c8e51b9ad6ab51',
    'mtime' => 1533751904,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_probability.php' => 
  array (
    'md5' => 'e0dc036673a5efbfb213551fb4e3f264',
    'mtime' => 1533751904,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/rli_link_workflow.php' => 
  array (
    'md5' => 'fe0d87b5f2d3de58dc76887e453bd630',
    'mtime' => 1533751914,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed.php' => 
  array (
    'md5' => '9d7611a4d11f0f517664f50c53a6ce4f',
    'mtime' => 1600139601,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_service_start_date.php' => 
  array (
    'md5' => 'f76dcb77bbbf94d968d2df69fbcf1501',
    'mtime' => 1600139601,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/dupe_check.ext.php' => 
  array (
    'md5' => '19fa68610ae8f8f16369b67cfaaab550',
    'mtime' => 1615764197,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/full_text_search_admin.php' => 
  array (
    'md5' => '06735aedc91d1605b23f9da258805c77',
    'mtime' => 1636002233,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/denorm_account_name.php' => 
  array (
    'md5' => '29894f218f4f7b1d55f132435807c23d',
    'mtime' => 1643412270,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_status.php' => 
  array (
    'md5' => '0ffcdfa9fb83d17a3224a9eacbffd244',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_stage.php' => 
  array (
    'md5' => 'a0051d9636c7b9140a4f0f9716ffe68d',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_total_revenue_line_items.php' => 
  array (
    'md5' => '4e365b40661aacb757b10af1a7bba552',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_amount.php' => 
  array (
    'md5' => '294c09215edf48e3f46a33a779581d1b',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_commit_stage.php' => 
  array (
    'md5' => '6a4b31bcf1bc1a3ce1ff62f50bfa0463',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_best_case.php' => 
  array (
    'md5' => 'fe06cc79f7016e434c73913c831df976',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_worst_case.php' => 
  array (
    'md5' => '0f51c8f262b84eaba2a8fcda8c1d6975',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_closed_revenue_line_items.php' => 
  array (
    'md5' => '9dbec117cde1ebaad3d7ee8fb6545b08',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_closed_won_revenue_line_items.php' => 
  array (
    'md5' => 'c7ae245d17795c15f6eb71255b8cfd15',
    'mtime' => 1658980381,
    'is_override' => false,
  ),
  'custom/Extension/modules/Opportunities/Ext/Vardefs/customer_journey_parent.php' => 
  array (
    'md5' => '6daf3e2bb482b2932f9aa36de07c4969',
    'mtime' => 1675417351,
    'is_override' => false,
  ),
);