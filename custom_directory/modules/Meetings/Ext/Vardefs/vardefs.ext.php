<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/audit.php

$dictionary['Meeting']['audited'] = true;
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_name.php

 // created: 2018-07-06 21:49:11
$dictionary['Meeting']['fields']['name']['audited']=true;
$dictionary['Meeting']['fields']['name']['massupdate']=false;
$dictionary['Meeting']['fields']['name']['comments']='Meeting name';
$dictionary['Meeting']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.43',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_status.php

 // created: 2018-07-06 21:50:21
$dictionary['Meeting']['fields']['status']['audited']=true;
$dictionary['Meeting']['fields']['status']['massupdate']=true;
$dictionary['Meeting']['fields']['status']['comments']='Meeting status (ex: Planned, Held, Not held)';
$dictionary['Meeting']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['status']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['status']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['status']['calculated']=false;
$dictionary['Meeting']['fields']['status']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_start.php

 // created: 2018-07-06 21:50:43
$dictionary['Meeting']['fields']['date_start']['audited']=true;
$dictionary['Meeting']['fields']['date_start']['comments']='Date of start of meeting';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_start']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_start']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_start']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_start']['calculated']=false;
$dictionary['Meeting']['fields']['date_start']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_location.php

 // created: 2018-07-06 21:52:52
$dictionary['Meeting']['fields']['location']['audited']=true;
$dictionary['Meeting']['fields']['location']['massupdate']=false;
$dictionary['Meeting']['fields']['location']['comments']='Meeting location';
$dictionary['Meeting']['fields']['location']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['location']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['location']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['location']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.36',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['location']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_email_reminder_time.php

 // created: 2018-07-06 21:53:28
$dictionary['Meeting']['fields']['email_reminder_time']['default']='-1';
$dictionary['Meeting']['fields']['email_reminder_time']['audited']=true;
$dictionary['Meeting']['fields']['email_reminder_time']['comments']='Specifies when a email reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['email_reminder_time']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['email_reminder_time']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['email_reminder_time']['calculated']=false;
$dictionary['Meeting']['fields']['email_reminder_time']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2018-07-06 21:53:58
$dictionary['Meeting']['fields']['date_entered']['audited']=true;
$dictionary['Meeting']['fields']['date_entered']['comments']='Date record created';
$dictionary['Meeting']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_entered']['calculated']=false;
$dictionary['Meeting']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2018-07-06 21:54:20
$dictionary['Meeting']['fields']['date_modified']['audited']=true;
$dictionary['Meeting']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Meeting']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_modified']['calculated']=false;
$dictionary['Meeting']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_referidos_c.php

 // created: 2018-07-06 21:55:40
$dictionary['Meeting']['fields']['sasa_referidos_c']['labelValue']='Referidos';
$dictionary['Meeting']['fields']['sasa_referidos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_referidos_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_referidos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_responsableejecucion_c.php

 // created: 2018-07-10 22:21:38
$dictionary['Meeting']['fields']['sasa_responsableejecucion_c']['labelValue']='Responsable de la Ejecución';
$dictionary['Meeting']['fields']['sasa_responsableejecucion_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_responsableejecucion_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_vencida_c.php

 // created: 2018-07-21 21:02:47
$dictionary['Meeting']['fields']['sasa_vencida_c']['labelValue']='Vencida';
$dictionary['Meeting']['fields']['sasa_vencida_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_vencida_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_superior1_c.php

 // created: 2018-07-26 02:47:02
$dictionary['Meeting']['fields']['sasa_superior1_c']['duplicate_merge_dom_value']=0;
$dictionary['Meeting']['fields']['sasa_superior1_c']['labelValue']='Superior 1';
$dictionary['Meeting']['fields']['sasa_superior1_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_superior1_c']['calculated']='true';
$dictionary['Meeting']['fields']['sasa_superior1_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Meeting']['fields']['sasa_superior1_c']['enforced']='true';
$dictionary['Meeting']['fields']['sasa_superior1_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_superior2_c.php

 // created: 2018-07-26 02:47:48
$dictionary['Meeting']['fields']['sasa_superior2_c']['duplicate_merge_dom_value']=0;
$dictionary['Meeting']['fields']['sasa_superior2_c']['labelValue']='Superior 2';
$dictionary['Meeting']['fields']['sasa_superior2_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_superior2_c']['calculated']='true';
$dictionary['Meeting']['fields']['sasa_superior2_c']['formula']='related($assigned_user_link,"sasa_superior1_c")';
$dictionary['Meeting']['fields']['sasa_superior2_c']['enforced']='true';
$dictionary['Meeting']['fields']['sasa_superior2_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/rli_link_workflow.php

$dictionary['Meeting']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_date_end.php

 // created: 2018-09-10 22:11:11
$dictionary['Meeting']['fields']['date_end']['audited']=true;
$dictionary['Meeting']['fields']['date_end']['comments']='Date meeting ends';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['date_end']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['date_end']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['date_end']['calculated']=false;
$dictionary['Meeting']['fields']['date_end']['enable_range_search']='1';
$dictionary['Meeting']['fields']['date_end']['required']=true;
$dictionary['Meeting']['fields']['date_end']['full_text_search']=array (
);
$dictionary['Meeting']['fields']['date_end']['group_label']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_name.php

 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_name']['len']=36;
$dictionary['Meeting']['fields']['parent_name']['required']=true;
$dictionary['Meeting']['fields']['parent_name']['audited']=false;
$dictionary['Meeting']['fields']['parent_name']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_name']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_name']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_type.php

 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_type']['len']=255;
$dictionary['Meeting']['fields']['parent_type']['required']=true;
$dictionary['Meeting']['fields']['parent_type']['audited']=false;
$dictionary['Meeting']['fields']['parent_type']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_type']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_type']['calculated']=false;
$dictionary['Meeting']['fields']['parent_type']['options']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_parent_id.php

 // created: 2018-09-11 21:51:12
$dictionary['Meeting']['fields']['parent_id']['len']=255;
$dictionary['Meeting']['fields']['parent_id']['required']=true;
$dictionary['Meeting']['fields']['parent_id']['audited']=false;
$dictionary['Meeting']['fields']['parent_id']['massupdate']=false;
$dictionary['Meeting']['fields']['parent_id']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['parent_id']['duplicate_merge_dom_value']=1;
$dictionary['Meeting']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['parent_id']['calculated']=false;
$dictionary['Meeting']['fields']['parent_id']['reportable']=true;
$dictionary['Meeting']['fields']['parent_id']['unified_search']=false;
$dictionary['Meeting']['fields']['parent_id']['group']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_envio_compro_c.php

 // created: 2019-10-26 00:12:58
$dictionary['Meeting']['fields']['sasa_envio_compro_c']['duplicate_merge_dom_value']=0;
$dictionary['Meeting']['fields']['sasa_envio_compro_c']['calculated']='true';
$dictionary['Meeting']['fields']['sasa_envio_compro_c']['formula']='ifElse(not(equal($sasa_fechaproximcompromiso_c,$sasa_fechaproximcompromiso_c)),concat("SI"),concat(""))';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_visitacompaniade_c.php

 // created: 2020-05-06 17:23:30
$dictionary['Meeting']['fields']['sasa_visitacompaniade_c']['labelValue']='Visita en Compañía de:';
$dictionary['Meeting']['fields']['sasa_visitacompaniade_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_visitacompaniade_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_visitacompaniade_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_fechaproximcompromiso_c.php

 // created: 2019-10-10 14:22:21
$dictionary['Meeting']['fields']['sasa_fechaproximcompromiso_c']['labelValue']='Fecha de Próximo Compromiso';
$dictionary['Meeting']['fields']['sasa_fechaproximcompromiso_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_fechaproximcompromiso_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadovisita_c,"Descartado")))';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/full_text_search_admin.php

 // created: 2021-11-04 05:03:53
$dictionary['Meeting']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_resultadovisita_c.php

 // created: 2022-09-15 22:13:57
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['labelValue']='Resultado de la Visita';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_resultadovisita_c']['visibility_grid']=array (
  'trigger' => 'status',
  'values' => 
  array (
    'Planned' => 
    array (
    ),
    'Held' => 
    array (
      0 => '',
      1 => 'Interesado',
      2 => 'Efectivo',
      3 => 'Negocio en Tramite',
      4 => 'Descartado',
      5 => 'Postergado',
    ),
    'Not Held' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_productosofrecidos_c.php

 // created: 2022-09-15 22:15:28
$dictionary['Meeting']['fields']['sasa_productosofrecidos_c']['labelValue']='Productos Ofrecidos';
$dictionary['Meeting']['fields']['sasa_productosofrecidos_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_productosofrecidos_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_productosofrecidos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_objetivovisita_c.php

 // created: 2022-09-15 22:17:12
$dictionary['Meeting']['fields']['sasa_objetivovisita_c']['labelValue']='Objetivo de la Visita';
$dictionary['Meeting']['fields']['sasa_objetivovisita_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_objetivovisita_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_objetivovisita_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_presencial_c.php

 // created: 2022-09-15 22:19:20
$dictionary['Meeting']['fields']['sasa_presencial_c']['labelValue']='La capacitación es presencial';
$dictionary['Meeting']['fields']['sasa_presencial_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_presencial_c']['required_formula']='';
$dictionary['Meeting']['fields']['sasa_presencial_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_presencial_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_compromiso_c.php

 // created: 2022-09-15 22:31:01
$dictionary['Meeting']['fields']['sasa_compromiso_c']['labelValue']='Compromiso';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Meeting']['fields']['sasa_compromiso_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['dependency']='and(equal($status,"Held"),not(equal($sasa_resultadovisita_c,"Descartado")))';
$dictionary['Meeting']['fields']['sasa_compromiso_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_description.php

 // created: 2022-09-15 22:31:27
$dictionary['Meeting']['fields']['description']['audited']=true;
$dictionary['Meeting']['fields']['description']['massupdate']=false;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.55',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['description']['calculated']=false;
$dictionary['Meeting']['fields']['description']['rows']='6';
$dictionary['Meeting']['fields']['description']['cols']='80';
$dictionary['Meeting']['fields']['description']['required']=false;
$dictionary['Meeting']['fields']['description']['hidemassupdate']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_enviarcapacitacion_c.php

 // created: 2022-09-26 19:24:36
$dictionary['Meeting']['fields']['sasa_enviarcapacitacion_c']['labelValue']='Enviar Capacitación';
$dictionary['Meeting']['fields']['sasa_enviarcapacitacion_c']['enforced']='';
$dictionary['Meeting']['fields']['sasa_enviarcapacitacion_c']['dependency']='';
$dictionary['Meeting']['fields']['sasa_enviarcapacitacion_c']['readonly_formula']='';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_sasa_programarvisita_c.php

 // created: 2023-01-26 21:28:45
$dictionary['Meeting']['fields']['sasa_programarvisita_c']['labelValue']='¿Programar visita o llamada?';
$dictionary['Meeting']['fields']['sasa_programarvisita_c']['dependency']='and(equal($status,"Held"))';
$dictionary['Meeting']['fields']['sasa_programarvisita_c']['required_formula']='';
$dictionary['Meeting']['fields']['sasa_programarvisita_c']['readonly_formula']='';
$dictionary['Meeting']['fields']['sasa_programarvisita_c']['visibility_grid']=array (
);

 
?>
