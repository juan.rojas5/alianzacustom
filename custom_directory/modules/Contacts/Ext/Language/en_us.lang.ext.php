<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.cci.lang.php

include('custom/include/en_us.cci.lang.php');
 
// $mod_strings['LBL_CCSYNCED'] = 'Sync with Constant Contact';
// $mod_strings['LBL_CCID'] = 'Constant Contact ID';
// $mod_strings['LBL_CCILOG_SUBPANEL'] = 'Constant Contact Audit Log';
// $mod_strings['LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE'] = 'Constant Contact Target Lists';
// $mod_strings['LBL_PROSPECTLISTS_TARGET_LIST_TITLE'] = 'Target Lists';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.ConstantContactIntegration.php

$mod_strings['LBL_CC_SYNCED'] = 'Linked with Constant Contact';
$mod_strings['LBL_CCID'] = 'Constant Contact ID';
$mod_strings['LBL_CCILOG_SUBPANEL'] = 'Constant Contact Audit Log';
$mod_strings['LBL_PROSPECTLISTS_FROM_PROSPECTLISTS_TITLE'] = 'Constant Contact Target Lists';
$mod_strings['LBL_PROSPECTLISTS_TARGET_LIST_TITLE'] = 'Target Lists';
$mod_strings["LBL_CCLISTS"] = 'Constant Contact Target Lists';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.AFContactsCustomFieldsv0012022040709.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other:';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3:';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento:';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
$mod_strings['LBL_SASA_HOBBIESAFICIONESDEPOR_C'] = 'Hobbies/Aficiones/Deportes';
$mod_strings['LBL_SASA_ESTADOCIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_NROHIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_RELACIONCUENTA_C'] = 'Relación con la Cuenta';
$mod_strings['LBL_SASA_QUIENREFIRIO_C'] = 'Quien Refirió?';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_NOENVIARINFOCLIENTE_C'] = 'No Enviar Información al Cliente';
$mod_strings['LBL_SASA_FONDOPENSIONES_C'] = 'Fondo de Pensiones';
$mod_strings['LBL_SASA_FONDOCESANTIAS_C'] = 'Fondo de Cesantías';
$mod_strings['LBL_SASA_PARENTESCORELACION_C'] = 'Parentesco/Relación';
$mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona (eliminar)';
$mod_strings['LBL_SASA_PERIODICIDAD_C'] = 'Periodicidad';
$mod_strings['LBL_SASA_INFOINTERES_C'] = 'Información de interés que quisiera recibir';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DESCRIPCIONCONTACTO_C'] = 'Descripción (eliminar)';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work:';
$mod_strings['LBL_SASA_TIPOCONTACTO_C'] = 'Tipo de Contacto (eliminar)';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información del Registro';
$mod_strings['LBL_SASA_INFOQUIERERECIBIR_C'] = 'Información de interés que quiere recibir';
$mod_strings['LBL_SASA_TIPOINFOPERIODICIDAD_C'] = 'Tipo de Información y Periodicidad';
$mod_strings['LBL_PREFERRED_LANGUAGE'] = 'Idioma preferido:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Codigo Postal';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa:';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
$mod_strings['LBL_SASA_FECHADEACTUALIZACIONSC'] = 'Fecha de Actualización SC';
$mod_strings['LBL_SASA_FECHADEMODIFICACIONSC_C'] = 'Fecha de Modificación SC';
$mod_strings['LBL_SASA_FECHADEVINCULACIONSC_C'] = 'Fecha de Vinculación SC';
$mod_strings['LBL_SASA_CONDICIONESPECIAL_C'] = 'Condición especial';
$mod_strings['LBL_SASA_LGBTIQ_C'] = 'LGBTIQ+';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.afcontactscustomfieldsv0032022092012.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other:';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3:';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento:';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
$mod_strings['LBL_SASA_HOBBIESAFICIONESDEPOR_C'] = 'Hobbies/Aficiones/Deportes';
$mod_strings['LBL_SASA_ESTADOCIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_NROHIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_RELACIONCUENTA_C'] = 'Relación con la Cuenta';
$mod_strings['LBL_SASA_QUIENREFIRIO_C'] = 'Quien Refirió?';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_NOENVIARINFOCLIENTE_C'] = 'No Enviar Información al Cliente';
$mod_strings['LBL_SASA_FONDOPENSIONES_C'] = 'Fondo de Pensiones';
$mod_strings['LBL_SASA_FONDOCESANTIAS_C'] = 'Fondo de Cesantías';
$mod_strings['LBL_SASA_PARENTESCORELACION_C'] = 'Parentesco/Relación';
$mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo Persona';
$mod_strings['LBL_SASA_PERIODICIDAD_C'] = 'Periodicidad';
$mod_strings['LBL_SASA_INFOINTERES_C'] = 'Información de interés que quisiera recibir';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DESCRIPCIONCONTACTO_C'] = 'Descripción (eliminar)';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work:';
$mod_strings['LBL_SASA_TIPOCONTACTO_C'] = 'Tipo de Contacto (eliminar)';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información del Registro';
$mod_strings['LBL_SASA_INFOQUIERERECIBIR_C'] = 'Información de interés que quiere recibir';
$mod_strings['LBL_SASA_TIPOINFOPERIODICIDAD_C'] = 'Tipo de Información y Periodicidad';
$mod_strings['LBL_PREFERRED_LANGUAGE'] = 'Idioma preferido:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Codigo Postal';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa:';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
$mod_strings['LBL_SASA_FECHADEACTUALIZACIONSC'] = 'Fecha de Actualización SC';
$mod_strings['LBL_SASA_FECHADEMODIFICACIONSC_C'] = 'Fecha de Modificación SC';
$mod_strings['LBL_SASA_FECHADEVINCULACIONSC_C'] = 'Fecha de Vinculación SC';
$mod_strings['LBL_SASA_NUEVO_C'] = 'nuevo';
$mod_strings['LBL_CASES_SUBPANEL_TITLE'] = 'PQRs';
$mod_strings['LNK_NEW_CASE'] = 'Nuevo PQRs';
$mod_strings['LBL_SASA_PQRS_C'] = 'PQRS';
$mod_strings['LBL_SASA_RAZONSOCIAL_C'] = 'Razon Social';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.AFContactsCustomFieldsv0012022061803.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other:';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3:';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento:';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
$mod_strings['LBL_SASA_HOBBIESAFICIONESDEPOR_C'] = 'Hobbies/Aficiones/Deportes';
$mod_strings['LBL_SASA_ESTADOCIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_NROHIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_RELACIONCUENTA_C'] = 'Relación con la Cuenta';
$mod_strings['LBL_SASA_QUIENREFIRIO_C'] = 'Quien Refirió?';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_NOENVIARINFOCLIENTE_C'] = 'No Enviar Información al Cliente';
$mod_strings['LBL_SASA_FONDOPENSIONES_C'] = 'Fondo de Pensiones';
$mod_strings['LBL_SASA_FONDOCESANTIAS_C'] = 'Fondo de Cesantías';
$mod_strings['LBL_SASA_PARENTESCORELACION_C'] = 'Parentesco/Relación';
$mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona (eliminar)';
$mod_strings['LBL_SASA_PERIODICIDAD_C'] = 'Periodicidad';
$mod_strings['LBL_SASA_INFOINTERES_C'] = 'Información de interés que quisiera recibir';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DESCRIPCIONCONTACTO_C'] = 'Descripción (eliminar)';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work:';
$mod_strings['LBL_SASA_TIPOCONTACTO_C'] = 'Tipo de Contacto (eliminar)';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información del Registro';
$mod_strings['LBL_SASA_INFOQUIERERECIBIR_C'] = 'Información de interés que quiere recibir';
$mod_strings['LBL_SASA_TIPOINFOPERIODICIDAD_C'] = 'Tipo de Información y Periodicidad';
$mod_strings['LBL_PREFERRED_LANGUAGE'] = 'Idioma preferido:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Codigo Postal';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa:';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
$mod_strings['LBL_SASA_FECHADEACTUALIZACIONSC'] = 'Fecha de Actualización SC';
$mod_strings['LBL_SASA_FECHADEMODIFICACIONSC_C'] = 'Fecha de Modificación SC';
$mod_strings['LBL_SASA_FECHADEVINCULACIONSC_C'] = 'Fecha de Vinculación SC';
$mod_strings['LBL_SASA_CONDICIONESPECIAL_C'] = 'Condición especial';
$mod_strings['LBL_SASA_LGBTIQ_C'] = 'LGBTIQ+';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.customcases_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_CASES_CONTACTS_1_FROM_CASES_TITLE'] = 'Quejas';
$mod_strings['LBL_CASES_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Quejas';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/en_us.AFContactsCustomFieldsv0022022111602.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_MEETING'] = 'Programar Visita';
$mod_strings['LBL_DEPARTMENT'] = 'Dependencia:';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono 1';
$mod_strings['LBL_MOBILE_PHONE'] = 'Celular:';
$mod_strings['LBL_OTHER_PHONE'] = 'Phone Other:';
$mod_strings['LBL_ASSISTANT_PHONE'] = 'Teléfono 3:';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento:';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Dirección principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Departamento/Estado de dirección principal:';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Dirección alternativa';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Departamento/Estado de dirección alternativa';
$mod_strings['LBL_EMAIL_ADDRESS'] = 'Correo Electronico:';
$mod_strings['LBL_SASA_TIPOIDENTIFICACION_C'] = 'Tipo de ID';
$mod_strings['LBL_SASA_NROIDENTIFICACION_C'] = 'Número de ID';
$mod_strings['LBL_SASA_HOBBIESAFICIONESDEPOR_C'] = 'Hobbies/Aficiones/Deportes';
$mod_strings['LBL_SASA_ESTADOCIVIL_C'] = 'Estado Civil';
$mod_strings['LBL_SASA_NROHIJOS_C'] = 'Número de Hijos';
$mod_strings['LBL_SASA_RELACIONCUENTA_C'] = 'Relación con la Cuenta';
$mod_strings['LBL_SASA_QUIENREFIRIO_C'] = 'Quien Refirió?';
$mod_strings['LBL_SASA_GENERO_C'] = 'Género';
$mod_strings['LBL_SASA_NOENVIARINFOCLIENTE_C'] = 'No Enviar Información al Cliente';
$mod_strings['LBL_SASA_FONDOPENSIONES_C'] = 'Fondo de Pensiones';
$mod_strings['LBL_SASA_FONDOCESANTIAS_C'] = 'Fondo de Cesantías';
$mod_strings['LBL_SASA_PARENTESCORELACION_C'] = 'Parentesco/Relación';
$mod_strings['LBL_SASA_TIPOPERSONA_C'] = 'Tipo de Persona (eliminar)';
$mod_strings['LBL_SASA_PERIODICIDAD_C'] = 'Periodicidad';
$mod_strings['LBL_SASA_INFOINTERES_C'] = 'Información de interés que quisiera recibir';
$mod_strings['LBL_SASA_PAIS_C'] = 'País';
$mod_strings['LBL_SASA_DEPARTAMENTO_C'] = 'Departamento';
$mod_strings['LBL_SASA_MUNICIPIO_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DESCRIPCIONCONTACTO_C'] = 'Descripción (eliminar)';
$mod_strings['LBL_SASA_TELOFICINA_C'] = 'Teléfono';
$mod_strings['LBL_SASA_TELEFONO2_C'] = 'Teléfono 2';
$mod_strings['LBL_OFFICE_PHONE'] = 'Phone Work:';
$mod_strings['LBL_SASA_TIPOCONTACTO_C'] = 'Tipo de Contacto (eliminar)';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información del Registro';
$mod_strings['LBL_SASA_INFOQUIERERECIBIR_C'] = 'Información de interés que quiere recibir';
$mod_strings['LBL_SASA_TIPOINFOPERIODICIDAD_C'] = 'Tipo de Información y Periodicidad';
$mod_strings['LBL_PREFERRED_LANGUAGE'] = 'Idioma preferido:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Codigo Postal';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'Código Postal de dirección alternativa:';
$mod_strings['LBL_SASA_TELEFONO_C'] = 'Teléfono';
$mod_strings['LBL_LEAD_SOURCE'] = 'Forma de contacto:';
$mod_strings['LBL_SASA_MUNICIPIOINTER_C'] = 'Ciudad';
$mod_strings['LBL_SASA_DEPARTAMENTOINTER_C'] = 'Departamento';
$mod_strings['LBL_SASA_FECHADEACTUALIZACIONSC'] = 'Fecha de Actualización SC';
$mod_strings['LBL_SASA_FECHADEMODIFICACIONSC_C'] = 'Fecha de Modificación SC';
$mod_strings['LBL_SASA_FECHADEVINCULACIONSC_C'] = 'Fecha de Vinculación SC';
$mod_strings['LBL_SASA_CONDICIONESPECIAL_C'] = 'Condición especial';
$mod_strings['LBL_SASA_LGBTIQ_C'] = 'LGBTIQ+';
$mod_strings['LBL_SASA_APELLIDOM4'] = 'Cuenta (Apellido) - M4';
$mod_strings['LBL_SASA_CELULARM4'] = 'Celular - M4';
$mod_strings['LBL_SASA_CORREOM4'] = 'Correo Electrónico - M4';
$mod_strings['LBL_SASA_DEPARTAMENTOM4_C'] = 'Departamento - M4';
$mod_strings['LBL_SASA_DIRECCIONPRINCM4'] = 'Dirección Principal - M4';
$mod_strings['LBL_SASA_FECHANACM4_C'] = 'Fecha de Nacimiento - M4';
$mod_strings['LBL_SASA_MUNICIPIOM4_C'] = 'Municipio - M4';
$mod_strings['LBL_SASA_NOMBREM4'] = 'Cuenta (Nombre) - M4';
$mod_strings['LBL_SASA_NROIDENTIFICACIONM4_C'] = 'Numero de ID - M4';
$mod_strings['LBL_SASA_RAZON_SOCIALM4_C'] = 'Razón Social - M4';
$mod_strings['LBL_SASA_TIPOIDENTIFICACIONM4_C'] = 'Tipo de ID - M4';

?>
