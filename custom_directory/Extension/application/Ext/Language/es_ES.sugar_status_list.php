<?php
 // created: 2023-04-10 20:40:22

$app_list_strings['status_list']=array (
  '' => '',
  10 => 'Crear Queja SFC',
  1 => 'Recibida',
  2 => 'Abierta',
  3 => 'Abierta en conciliación',
  4 => 'Cerrada',
  5 => 'Reabierta',
  6 => 'Escalada',
  7 => 'Vencida',
  8 => 'Mail to case',
);