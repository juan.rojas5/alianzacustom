<?php
// created: 2022-12-09 21:37:28
$app_list_strings['record_type_display_notes']['Accounts'] = 'Cuenta';
$app_list_strings['record_type_display_notes']['Contacts'] = 'Contacto';
$app_list_strings['record_type_display_notes']['Opportunities'] = 'Oportunidad';
$app_list_strings['record_type_display_notes']['Tasks'] = 'Tarea';
$app_list_strings['record_type_display_notes']['ProductTemplates'] = 'Product';
$app_list_strings['record_type_display_notes']['Quotes'] = 'Presupuesto';
$app_list_strings['record_type_display_notes']['Products'] = 'Lnea de la Oferta';
$app_list_strings['record_type_display_notes']['Contracts'] = 'Contrato';
$app_list_strings['record_type_display_notes']['Emails'] = 'Correo electrnico';
$app_list_strings['record_type_display_notes']['Bugs'] = 'Incidencia';
$app_list_strings['record_type_display_notes']['Project'] = 'Proyecto';
$app_list_strings['record_type_display_notes']['ProjectTask'] = 'Tarea de Proyecto';
$app_list_strings['record_type_display_notes']['Prospects'] = 'Pblico Objetivo';
$app_list_strings['record_type_display_notes']['Cases'] = 'PQR';
$app_list_strings['record_type_display_notes']['Leads'] = 'Cliente Potencial';
$app_list_strings['record_type_display_notes']['Meetings'] = 'Visita';
$app_list_strings['record_type_display_notes']['Calls'] = 'Llamada';
$app_list_strings['record_type_display_notes']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['record_type_display_notes']['PurchasedLineItems'] = 'Elemento comprado';
$app_list_strings['record_type_display_notes']['Purchases'] = 'Compra';
$app_list_strings['record_type_display_notes']['Escalations'] = 'Escalada';
$app_list_strings['record_type_display_notes']['RevenueLineItems'] = 'Líneas de Ingreso';
