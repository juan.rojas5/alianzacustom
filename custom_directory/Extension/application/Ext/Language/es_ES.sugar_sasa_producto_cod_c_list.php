<?php
 // created: 2022-03-08 08:53:05

$app_list_strings['sasa_producto_cod_c_list']=array (
  '' => '',
  401 => 'Fiducia de inversión',
  402 => 'Fiducia inmobiliaria',
  403 => 'Fiducia de administración',
  404 => 'Fiducia en garantía',
  405 => 'Negocios fiduciarios con entidades públicas',
  406 => 'Fiducia con recursos del sistema general de seguridad social y otros relacionados',
  407 => 'Fondos de Inversión Colectiva (FIC)',
  408 => 'Fondos de Capital Privado',
  409 => 'Custodia de Valores',
  410 => 'Fondos de Pensiones Voluntarias',
  498 => 'Otros productos de Fiduciarias',
);