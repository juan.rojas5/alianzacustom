<?php

require_once 'modules/Administration/Administration.php';

class ConfigClass 
{
	var $administrationObj;

	function __construct() {
		//$this->administrationObj = new Administration();
	}

	public function newConfig($config)
	{
		$this->administrationObj = new Administration();

		$this->administrationObj->retrieveSettings('PAI');
		$name = $config['name'];
		$MySetting = $this->administrationObj->settings['PAI_'.$name];

		$value = $config['value'];
		$json[] = $value;

		foreach($json as $string) {
			json_decode($string);
			switch(json_last_error()) {
				case JSON_ERROR_NONE:
					$validation = true;
				break;
				case JSON_ERROR_DEPTH:
					$validation = 'Excedido tamaño máximo de la pila';
				break;
				case JSON_ERROR_STATE_MISMATCH:
					$validation = 'Desbordamiento de buffer o los modos no coinciden';
				break;
				case JSON_ERROR_CTRL_CHAR:
					$validation = 'Encontrado carácter de control no esperado';
				break;
				case JSON_ERROR_SYNTAX:
					$validation = 'Error de sintaxis, JSON mal formado';
				break;
				case JSON_ERROR_UTF8:
					$validation = 'Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
				break;
				default:
					$validation = 'Error desconocido';
				break;
			}
		}

		if (!empty($MySetting)) {
			return array (
				'status' => 'ERROR',
				'code' => 204,
				'message' => 'No se creo el Registro, Dato Duplicado'
			);
		} else if ($validation === true) {
			$this->administrationObj->saveSetting('PAI', $config['name'], $config['value'], 'base');
			return array (
				'status' => 'CREATED',
				'code' => 201,
				'message' => 'Registro '.$config['name'].' Creado con Exito'
			);
		} else {
			return array (
				'status' => 'ERROR',
				'code' => 400,
				'message' => $validation
			);
		}
	}

	public function editConfig($config)
	{
		$this->administrationObj = new Administration();

		$this->administrationObj->retrieveSettings('PAI');
		$name = $config['name'];

		$MySetting = $this->administrationObj->settings['PAI_'.$name];

		$value = $config['value'];
		$json[] = $value;

		foreach($json as $string) {
			json_decode($string);
			switch(json_last_error()) {
				case JSON_ERROR_NONE:
					$validation = true;
				break;
				case JSON_ERROR_DEPTH:
					$validation = 'Excedido tamaño máximo de la pila';
				break;
				case JSON_ERROR_STATE_MISMATCH:
					$validation = 'Desbordamiento de buffer o los modos no coinciden';
				break;
				case JSON_ERROR_CTRL_CHAR:
					$validation = 'Encontrado carácter de control no esperado';
				break;
				case JSON_ERROR_SYNTAX:
					$validation = 'Error de sintaxis, JSON mal formado';
				break;
				case JSON_ERROR_UTF8:
					$validation = 'Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
				break;
				default:
					$validation = 'Error desconocido';
				break;
			}
		}

		if (!empty($MySetting) and $validation === true) {
		//return $config['value'];
			$this->administrationObj->saveSetting('PAI', $config['name'], $config['value'], 'base');
			//$this->administrationObj->saveSetting('PAI','Pruebaname', $config['value'], 'base');

			return array (
				'status' => 'OK',
				'code' => 200,
				'message' => 'Registro '.$config['name'].' Actualizado con Exito'
			);
		} else {
			return array (
				'status' => 'ERROR',
				'code' => 400,
				'message' => $validation
			);
		}
	}

	public function deleteConfig($config)
	{
		global $db;

		$query = "DELETE FROM config WHERE category = 'PAI' AND name = '".$config['name']."' AND platform = 'base'";
		$result = $db->query($query);

		return array (
			'status' => 'Ok',
			'code' => 200,
			'message' => 'Registro '.$config['name'].' Eliminado con Exito'
		);
	}
}
